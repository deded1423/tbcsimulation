﻿namespace WowHeadCrawler
{
    public class Spell
    {
        public int CastTime;
        public bool Channeled;
        public int CooldownTime;
        public int Id;

        public bool IsGCD;
        public int Level;

        public int Mana;
        public string Name;
        public int Range;
        public int Rank;

        public string School;
        public string SecondarySchool;


        public int TickTime;

        // Name = "Banish";
        // Id = 18647;
        // CastTime = 1500;
        // Channeled = false;
        // CooldownTime = 0;
        // IsGCD = true;
        // TickTime = 0;
        //
        // Mana = 200;
        // Rank = 2;
        //
        // *Level* 
        // *Range* 
        // 
        // School = EnumSim.SchoolType.Shadow;
        // SecondarySchool = EnumSim.SecondarySchoolType.Warlock_Demonology;
        // ActionDelegate = delegate { throw new NotImplementedException(); };
        public string Url;
    }
}