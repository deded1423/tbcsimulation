﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using HtmlAgilityPack;
using Newtonsoft.Json;
using PuppeteerSharp;
using TBCsimulation.Data.Items;
using TBCsimulation.Data.Utils;

namespace WowHeadCrawler
{
    public class GemCrawler
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        private static string baseUrl = @"https://tbc.wowhead.com";
        private static Page basePage;
        public static Browser browser;
        private static string FolderName;

        public static List<Gem> items = new();

        public static void DownloadHtml(string url)
        {
            var cont = true;
            var addedUrl = "";
            while (cont)
            {
                log.Info(url + addedUrl);

                HtmlDocument htmlDocument;
                try
                {
                    basePage = Task.WhenAll(browser.NewPageAsync()).Result[0];
                    Task.WaitAll(basePage.GoToAsync(url + addedUrl));
                    htmlDocument = new HtmlDocument();
                    htmlDocument.LoadHtml(Task.WhenAll(basePage.GetContentAsync()).Result[0]);
                    Task.WaitAll(basePage.CloseAsync());
                }
                catch (Exception)
                {
                    Thread.Sleep(500);
                    try
                    {
                        Task.WaitAll(basePage.CloseAsync());
                    }
                    catch (Exception)
                    {
                        // ignored
                    }

                    basePage = Task.WhenAll(browser.NewPageAsync()).Result[0];
                    Task.WaitAll(basePage.GoToAsync(url + addedUrl));
                    htmlDocument = new HtmlDocument();
                    htmlDocument.LoadHtml(Task.WhenAll(basePage.GetContentAsync()).Result[0]);
                    Task.WaitAll(basePage.CloseAsync());
                }

                FolderName =
                    SelectNodes(htmlDocument, @"/html/body/div[4]/div/div[1]/div[2]/div[4]/div[2]/form/div[1]/h1")[0]
                        .InnerText.Replace(" ", "");
                log.Info(FolderName);


                var exists = Directory.Exists(Directory.GetCurrentDirectory() + @"\Data\" + FolderName);

                if (!exists)
                    Directory.CreateDirectory(Directory.GetCurrentDirectory() + @"\Data\" + FolderName);
                HtmlNode table = null;

                try
                {
                    table = SelectNodes(htmlDocument,
                        @"/html/body/div[4]/div/div[1]/div[2]/div[4]/div[5]/div[1]/div[2]/div/table/tbody")[0];
                }
                catch (Exception)
                {
                    table = SelectNodes(htmlDocument,
                        @"/html/body/div[4]/div/div[1]/div[2]/div[4]/div[4]/div[2]/div/table/tbody")[0];
                }

                log.Fatal(table.ChildNodes.Count);
                DownloadHtmlTable(table);

                HtmlNode continueButton;

                try
                {
                    continueButton = SelectNodes(htmlDocument,
                        @"/html/body/div[4]/div/div[1]/div[2]/div[4]/div[5]/div[1]/div[1]/div[1]/a[4]")[0];
                }
                catch (Exception)
                {
                    continueButton = SelectNodes(htmlDocument,
                        @"/html/body/div[4]/div/div[1]/div[2]/div[4]/div[4]/div[1]/div[1]/a[4]")[0];
                }

                if (continueButton.Attributes["data-active"].Value.Equals("yes"))
                {
                    if (!addedUrl.Equals(""))
                    {
                        var i = int.Parse(addedUrl.Replace("#", ""));
                        addedUrl = "#" + (i + 50);
                        if (i >= 900) throw new AggregateException();
                    }
                    else
                    {
                        addedUrl = "#50";
                    }
                }
                else
                {
                    cont = false;
                }
            }

            log.Info("END");
        }

        public static async Task Initialize()
        {
            Task.WaitAll(new BrowserFetcher().DownloadAsync(BrowserFetcher.DefaultChromiumRevision));

            // Create an instance of the browser and configure launch options
            browser = await Puppeteer.LaunchAsync(new LaunchOptions
            {
                Headless = true
            });

            // Create a new page and go to Bing Maps
            basePage = await browser.NewPageAsync();
        }

        private static HtmlNodeCollection SelectNodes(HtmlDocument htmlDocument, string xpath)
        {
            var node = htmlDocument.DocumentNode.SelectNodes(xpath);
            if (node == null) log.Info("node == null - " + xpath);

            return node;
        }

        private static void DownloadHtmlTable(HtmlNode table)
        {
            var options = new ParallelOptions();
            options.MaxDegreeOfParallelism = 5;
            Parallel.ForEach(table.ChildNodes, options, childNode =>
                    // foreach (var childNode in table.ChildNodes)
                {
                    try
                    {
                        var itemUrl = baseUrl +
                                      childNode.ChildNodes[2].ChildNodes[0].ChildNodes["a"].Attributes["href"].Value;
                        var itemName = childNode.ChildNodes[2].ChildNodes[0].ChildNodes["a"].InnerText;
                        log.Info(itemName + " - " + itemUrl);
                        HtmlDocument htmlDocument;
                        Page page = null;
                        try
                        {
                            page = Task.WhenAll(browser.NewPageAsync()).Result[0];
                            Task.WaitAll(page.GoToAsync(itemUrl));
                            htmlDocument = new HtmlDocument();
                            htmlDocument.LoadHtml(Task.WhenAll(page.GetContentAsync()).Result[0]);
                            Task.WaitAll(page.CloseAsync());
                        }
                        catch (Exception)
                        {
                            Thread.Sleep(500);
                            try
                            {
                                Task.WaitAll(page.CloseAsync());
                            }
                            catch (Exception)
                            {
                                // ignored
                            }

                            page = Task.WhenAll(browser.NewPageAsync()).Result[0];
                            Task.WaitAll(page.GoToAsync(itemUrl));
                            htmlDocument = new HtmlDocument();
                            htmlDocument.LoadHtml(Task.WhenAll(page.GetContentAsync()).Result[0]);
                            Task.WaitAll(page.CloseAsync());
                        }

                        try
                        {
                            htmlDocument.Save(Directory.GetCurrentDirectory() + @"\Data\" + FolderName + @"\" +
                                              HttpUtility.UrlEncode(itemName) + ".html");
                        }
                        catch (Exception e)
                        {
                            log.Info(e);
                        }
                    }
                    catch (Exception e)
                    {
                        log.Info(e);
                        throw;
                    }
                }
            );
        }


        public static void ParseHtml(string FolderName)
        {
            var filePaths = Directory.GetFiles(FolderName);
            foreach (var filePath in filePaths) ParseHtmlItem(filePath);
        }

        public static void ParseHtmlItem(string filePath)
        {
            try
            {
                log.Info(filePath);
                var htmlDocument = new HtmlDocument();
                htmlDocument.Load(filePath);
                HtmlNode table;
                try
                {
                    table = SelectNodes(htmlDocument,
                        @"/html/body/div[4]/div/div[1]/div[2]/div[4]/div[2]/div[5]/table/tbody/tr[1]/td")[0];
                }
                catch (Exception)
                {
                    table = SelectNodes(htmlDocument,
                        @"/html/body/div[4]/div[1]/div/div[2]/div[5]/div[5]/table/tbody/tr[1]/td")[0];
                }

                var gem = new Gem();

                var link = SelectNodes(htmlDocument, @"/html/head/link[1]")[0].Attributes["href"].Value;
                int id = Int32.Parse(Regex.Match(link, @"item=(?<id>\d+)/").Groups["id"].Value);
                gem.Id = id;


                var upperTable = table.ChildNodes[0].ChildNodes["tbody"].ChildNodes["tr"].ChildNodes["td"];
                // var lowerTable = table.ChildNodes[1].ChildNodes["tbody"].ChildNodes["tr"].ChildNodes["td"];

                HtmlNode nameNode;
                if (upperTable.ChildNodes[0].Name.Equals("table"))
                    nameNode = upperTable.ChildNodes[0].ChildNodes["tbody"].ChildNodes["tr"].ChildNodes["td"]
                        .ChildNodes["b"];
                else
                    nameNode = upperTable.ChildNodes["b"];
                gem.Name = nameNode.InnerText;
                if (gem.Name.Equals("Brian's Bryanite of Extended Cost Copying") ||
                    gem.Name.Equals("zzDEPRECATEDHeart of the Sky"))
                {
                    return;
                }

                foreach (var node in upperTable.ChildNodes)
                {
                    if (node.GetType().Equals(typeof(HtmlCommentNode)) || node.InnerText.Equals("") ||
                        node.InnerText.Equals(gem.Name))
                        continue;
                    if (node.InnerText.Contains("Item Level"))
                    {
                        gem.ILevel = int.Parse(node.InnerText.Replace("Item Level", ""));
                        continue;
                    }

                    if (node.InnerText.Contains("Binds"))
                    {
                        switch (node.InnerText)
                        {
                            case "":
                                gem.Binds = EnumSim.Binds.None;
                                break;
                            case "Binds when equipped":
                                gem.Binds = EnumSim.Binds.OnEquip;
                                break;
                            case "Binds when picked up":
                                gem.Binds = EnumSim.Binds.OnPickup;
                                break;
                            case "Binds when used":
                                gem.Binds = EnumSim.Binds.OnUse;
                                break;
                            default:
                                throw new ArgumentException();
                        }

                        continue;
                    }


                    if (node.InnerText.Contains("Unique"))
                    {
                        if (node.InnerText.Equals("Unique") || node.InnerText.Equals("Unique-Equipped"))
                            gem.Unique = true;
                        else
                            log.Warn(node.InnerText + " in " + gem.Name);
                        continue;
                    }

                    string[] split = null;
                    if (node.InnerText.Contains("and"))
                    {
                        split = node.InnerText.Split(new[] {"and"}, StringSplitOptions.RemoveEmptyEntries);
                    }

                    if (node.InnerText.Contains(","))
                    {
                        split = node.InnerText.Split(new[] {","}, StringSplitOptions.RemoveEmptyEntries);
                    }

                    if (node.InnerText.Contains("&"))
                    {
                        split = node.InnerText.Split(new[] {"&"}, StringSplitOptions.RemoveEmptyEntries);
                    }

                    if (split == null)
                    {
                        split = new[] {node.InnerText};
                    }

                    foreach (var s in split)
                    {
                        TBCsimulation.Data.Items.Stat stat = new();
                        if (!Regex.IsMatch(s, @"(?<Number>[+-]?\d+) (?<Key>.+)"))
                        {
                            if (s.Equals("amp; 3% Increased Critical Damage"))
                            {
                                stat.Value = 3;
                                stat.Key = EnumSim.StatEnum.CriticalDamage;
                                gem.Stats.Add(stat);
                                continue;
                            }

                            if (s.Equals(" -10% Speed"))
                            {
                                stat.Value = -10;
                                stat.Key = EnumSim.StatEnum.MovementSpeed;
                                gem.Stats.Add(stat);
                                continue;
                            }

                            gem.ExtraStats = s;
                            continue;
                        }

                        var match = Regex.Match(s, @"(?<Number>[+-]?\d+) (?<Key>.+)");
                        stat.Value = int.Parse(match.Groups["Number"].Value);

                        if (Regex.IsMatch(s,
                            @"(?<Number>[+-]?\d+) Healing (Spells )?(and )?(?<Number>[+-]?\d+) (Spell )?Damage( Spells)?"))
                        {
                            stat.Key = EnumSim.StatEnum.HealingThirdSpellDamage;
                            gem.Stats.Add(stat);
                            continue;
                        }

                        switch (match.Groups["Key"].Value)
                        {
                            case "Critical Strike Rating":
                            case "Critical Strike Rating ":
                            case "Critical Rating":
                            case "Critical Rating ":
                            case "Crit Rating":
                            case "Crit Rating ":
                                stat.Key = EnumSim.StatEnum.CriticalStrikeRating;
                                break;
                            case "Attack Power":
                            case "Attack Power ":
                                stat.Key = EnumSim.StatEnum.AttackPower;
                                break;
                            case "Dodge Rating ":
                            case "Dodge Rating":
                                stat.Key = EnumSim.StatEnum.DodgeRating;
                                break;
                            case "Stamina":
                            case "Stamina ":
                                stat.Key = EnumSim.StatEnum.Stamina;
                                break;
                            case "Intellect":
                            case "Intellect ":
                                stat.Key = EnumSim.StatEnum.Intellect;
                                break;
                            case "Spirit":
                            case "Spirit ":
                                stat.Key = EnumSim.StatEnum.Spirit;
                                break;
                            case "Agility":
                            case "Agility ":
                                stat.Key = EnumSim.StatEnum.Agility;
                                break;
                            case "Strength":
                            case "Strength ":
                                stat.Key = EnumSim.StatEnum.Strength;
                                break;
                            case "Resilience Rating":
                            case "Resilience Rating ":
                                stat.Key = EnumSim.StatEnum.ResilienceRating;
                                break;
                            case "Spell Critical":
                            case "Spell Critical ":
                            case "Spell Critical Rating":
                            case "Spell Critical Rating ":
                            case "Spell Critical Strike Rating":
                            case "Spell Critical Strike Rating ":
                            case "Spell Crit Rating":
                            case "Spell Crit Rating ":
                                stat.Key = EnumSim.StatEnum.SpellCriticalStrikeRating;
                                break;
                            case "Spell Power":
                            case "Spell Power ":
                            case "Spell Damage":
                            case "Spell Damage ":
                                stat.Key = EnumSim.StatEnum.SpellDamage;
                                break;
                            case "Melee Damage":
                            case "Melee Damage ":
                                stat.Key = EnumSim.StatEnum.MeleeDamage;
                                break;
                            case "Defense Rating":
                            case "Defense Rating ":
                                stat.Key = EnumSim.StatEnum.DefenseRating;
                                break;
                            case "Mana per 5 Seconds":
                            case "Mana every 5 seconds":
                            case "Mana every 5 Sec ":
                            case "mana per 5 sec.":
                                stat.Key = EnumSim.StatEnum.Manaper5sec;
                                break;
                            case "Parry Rating":
                            case "Parry Rating ":
                                stat.Key = EnumSim.StatEnum.ParryRating;
                                break;
                            case "Hit Rating":
                            case "Hit Rating ":
                                stat.Key = EnumSim.StatEnum.HitRating;
                                break;
                            case "Spell Haste Rating":
                            case "Spell Haste Rating ":
                                stat.Key = EnumSim.StatEnum.SpellHasteRating;
                                break;
                            case "Spell Hit Rating":
                            case "Spell Hit Rating ":
                                stat.Key = EnumSim.StatEnum.SpellHitRating;
                                break;
                            case "Armor":
                            case "Armor ":
                                stat.Key = EnumSim.StatEnum.Armor;
                                break;
                            case "Healing":
                            case "Healing ":
                                stat.Key = EnumSim.StatEnum.Healing;
                                break;
                            case "Spell Penetration":
                            case "Spell Penetration ":
                                stat.Key = EnumSim.StatEnum.SpellPenetration;
                                break;
                            case "Resist All":
                                stat.Key = EnumSim.StatEnum.AllResistance;
                                break;

                            default:
                                log.Fatal(match.Groups["Key"].Value);
                                throw new NotSupportedException();
                        }

                        gem.Stats.Add(stat);
                        log.Info(s);
                    }

                    log.Info(node.InnerText);
                }

                if (gem.Stats.Count != 0)
                {
                    items.Add(gem);
                }
            }
            catch (Exception e)
            {
                log.Fatal(e);
            }
        }

        public static void PrintDictionary()
        {
            var reader =
                new StreamReader(@"F:\Proyectos\TBCsimulation\WowHeadCrawler\Data_WoWHead\Gem.json");
            var foo = reader.ReadToEnd();
            var items = JsonConvert.DeserializeObject<List<TBCsimulation.Data.Items.Item>>(foo);

            var writerClass = new StreamWriter(Directory.GetCurrentDirectory() + @"\Data\GemId.cs");


            writerClass.WriteLine("using System.Collections.Generic;");
            writerClass.WriteLine("using System.Linq;");

            writerClass.WriteLine("namespace TBCsimulation.Data.Items");
            writerClass.WriteLine("{");
            writerClass.WriteLine("public static class GemId");
            writerClass.WriteLine("{");
            writerClass.WriteLine("public static int GetId(string Name)");
            writerClass.WriteLine("{");
            writerClass.WriteLine("    return NameToId[Name];");
            writerClass.WriteLine("}");
            writerClass.WriteLine("private static Dictionary<string, int> NameToId = new()");
            writerClass.WriteLine("{");
            foreach (var item in items)
            {
                writerClass.WriteLine("    {\"" + item.Name.Replace("\"", "\\\"") +
                                      "\", " + item.Id + "},  ");
            }

            writerClass.WriteLine(" };");
            writerClass.WriteLine("}");
            writerClass.WriteLine("}");

            writerClass.Close();
        }
    }
}