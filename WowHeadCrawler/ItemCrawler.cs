﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using HtmlAgilityPack;
using Newtonsoft.Json;
using PuppeteerSharp;
using TBCsimulation.Data.Items;
using TBCsimulation.Data.Utils;

namespace WowHeadCrawler
{
    public class ItemCrawler
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        private static string baseUrl = @"https://tbc.wowhead.com";
        private static Page basePage;
        public static Browser browser;
        private static string FolderName;

        public static List<Item> items = new();

        public static void DownloadHtml(string url)
        {
            var cont = true;
            var addedUrl = "";
            while (cont)
            {
                log.Info(url + addedUrl);

                HtmlDocument htmlDocument;
                try
                {
                    basePage = Task.WhenAll(browser.NewPageAsync()).Result[0];
                    Task.WaitAll(basePage.GoToAsync(url + addedUrl));
                    htmlDocument = new HtmlDocument();
                    htmlDocument.LoadHtml(Task.WhenAll(basePage.GetContentAsync()).Result[0]);
                    Task.WaitAll(basePage.CloseAsync());
                }
                catch (Exception)
                {
                    Thread.Sleep(500);
                    try
                    {
                        Task.WaitAll(basePage.CloseAsync());
                    }
                    catch (Exception)
                    {
                        // ignored
                    }

                    basePage = Task.WhenAll(browser.NewPageAsync()).Result[0];
                    Task.WaitAll(basePage.GoToAsync(url + addedUrl));
                    htmlDocument = new HtmlDocument();
                    htmlDocument.LoadHtml(Task.WhenAll(basePage.GetContentAsync()).Result[0]);
                    Task.WaitAll(basePage.CloseAsync());
                }

                FolderName =
                    SelectNodes(htmlDocument, @"/html/body/div[4]/div/div[1]/div[2]/div[4]/div[2]/form/div[1]/h1")[0]
                        .InnerText.Replace(" ", "");
                log.Info(FolderName);


                var exists = Directory.Exists(Directory.GetCurrentDirectory() + @"\Data\" + FolderName);

                if (!exists)
                    Directory.CreateDirectory(Directory.GetCurrentDirectory() + @"\Data\" + FolderName);
                HtmlNode table = null;

                try
                {
                    table = SelectNodes(htmlDocument,
                        @"/html/body/div[4]/div/div[1]/div[2]/div[4]/div[5]/div[1]/div[2]/div/table/tbody")[0];
                }
                catch (Exception)
                {
                    table = SelectNodes(htmlDocument,
                        @"/html/body/div[4]/div/div[1]/div[2]/div[4]/div[4]/div[2]/div/table/tbody")[0];
                }

                DownloadHtmlTable(table);

                HtmlNode continueButton;

                try
                {
                    continueButton = SelectNodes(htmlDocument,
                        @"/html/body/div[4]/div/div[1]/div[2]/div[4]/div[5]/div[1]/div[1]/div[1]/a[4]")[0];
                }
                catch (Exception)
                {
                    continueButton = SelectNodes(htmlDocument,
                        @"/html/body/div[4]/div/div[1]/div[2]/div[4]/div[4]/div[1]/div[1]/a[4]")[0];
                }

                if (continueButton.Attributes["data-active"].Value.Equals("yes"))
                {
                    if (!addedUrl.Equals(""))
                    {
                        var i = int.Parse(addedUrl.Replace(";", ""));
                        addedUrl = ";" + (i + 50);
                        if (i >= 900) throw new AggregateException();
                    }
                    else
                    {
                        addedUrl = ";50";
                    }
                }
                else
                {
                    cont = false;
                }
            }

            log.Info("END");
        }

        private static void DownloadHtmlTable(HtmlNode table)
        {
            var options = new ParallelOptions();
            options.MaxDegreeOfParallelism = 5;
            Parallel.ForEach(table.ChildNodes, options, childNode =>
                // foreach (var childNode in table.ChildNodes)
            {
                try
                {
                    var itemUrl = baseUrl +
                                  childNode.ChildNodes[2].ChildNodes[0].ChildNodes["a"].Attributes["href"].Value;
                    var itemName = childNode.ChildNodes[2].ChildNodes[0].ChildNodes["a"].InnerText;
                    log.Info(itemName + " - " + itemUrl);
                    HtmlDocument htmlDocument;
                    Page page = null;
                    try
                    {
                        page = Task.WhenAll(browser.NewPageAsync()).Result[0];
                        Task.WaitAll(page.GoToAsync(itemUrl));
                        htmlDocument = new HtmlDocument();
                        htmlDocument.LoadHtml(Task.WhenAll(page.GetContentAsync()).Result[0]);
                        Task.WaitAll(page.CloseAsync());
                    }
                    catch (Exception)
                    {
                        Thread.Sleep(500);
                        try
                        {
                            Task.WaitAll(page.CloseAsync());
                        }
                        catch (Exception)
                        {
                            // ignored
                        }

                        page = Task.WhenAll(browser.NewPageAsync()).Result[0];
                        Task.WaitAll(page.GoToAsync(itemUrl));
                        htmlDocument = new HtmlDocument();
                        htmlDocument.LoadHtml(Task.WhenAll(page.GetContentAsync()).Result[0]);
                        Task.WaitAll(page.CloseAsync());
                    }

                    try
                    {
                        htmlDocument.Save(Directory.GetCurrentDirectory() + @"\Data\" + FolderName + @"\" +
                                          HttpUtility.UrlEncode(itemName) + ".html");
                    }
                    catch (Exception e)
                    {
                        log.Info(e);
                    }
                }
                catch (Exception e)
                {
                    log.Info(e);
                    throw;
                }
            });
        }

        public static async Task Initialize()
        {
            Task.WaitAll(new BrowserFetcher().DownloadAsync(BrowserFetcher.DefaultChromiumRevision));

            // Create an instance of the browser and configure launch options
            browser = await Puppeteer.LaunchAsync(new LaunchOptions
            {
                Headless = true
            });

            // Create a new page and go to Bing Maps
            basePage = await browser.NewPageAsync();
        }

        private static HtmlNodeCollection SelectNodes(HtmlDocument htmlDocument, string xpath)
        {
            var node = htmlDocument.DocumentNode.SelectNodes(xpath);
            if (node == null) log.Info("node == null - " + xpath);

            return node;
        }

        public static void ConvertFiles(string FolderName)
        {
            var filePaths = Directory.GetFiles(FolderName);
            foreach (var filePath in filePaths)
            {
                var fileName = Path.GetFileName(filePath);
                // log.Info(fileName);
                if (fileName.Contains("+")) throw new ApplicationException();

                var encoded = HttpUtility.UrlEncode(fileName);
                // log.Info(encoded);
                File.Move(filePath, FolderName + @"\" + encoded);
            }
        }

        public static void ParseHtml(string FolderName)
        {
            var filePaths = Directory.GetFiles(FolderName);
            foreach (var filePath in filePaths) ParseHtmlItem(filePath);
        }

        public static void ParseHtmlItem(string filePath)
        {
            try
            {
                log.Info(filePath);
                var htmlDocument = new HtmlDocument();
                htmlDocument.Load(filePath);
                HtmlNode table;
                try
                {
                    table = SelectNodes(htmlDocument,
                        @"/html/body/div[4]/div/div[1]/div[2]/div[4]/div[2]/div[5]/table/tbody/tr[1]/td")[0];
                }
                catch (Exception)
                {
                    table = SelectNodes(htmlDocument,
                        @"/html/body/div[4]/div[1]/div/div[2]/div[5]/div[5]/table/tbody/tr[1]/td")[0];
                }

                var item = new Item(table, htmlDocument);
                if (item.Name.Equals(""))
                    return;

                var link = SelectNodes(htmlDocument, @"/html/head/link[1]")[0].Attributes["href"].Value;
                int id = Int32.Parse(Regex.Match(link, @"item=(?<id>\d+)/").Groups["id"].Value);
                item.File = filePath;
                item.Id = id;

                if (item.RandomStats.Count != 0)
                    foreach (var randomStat in item.RandomStats)
                    {
                        var newItem = item.Clone();
                        newItem.Name = item.Name + randomStat.Key;
                        var stats = randomStat.Value.Split(',');
                        foreach (var s in stats)
                        {
                            var matchRegex = @"\((?<min>\d+) - (?<max>\d+)\)";
                            var match = Regex.Match(s, matchRegex);
                            if (!match.Value.Equals(""))
                                newItem.Stats_Raw.Add(s.Replace(match.Value, match.Groups["max"].ToString()));
                            else
                                newItem.Stats_Raw.Add(s);
                        }

                        items.Add(newItem);
                    }
                else
                    items.Add(item);
            }
            catch (Exception e)
            {
                log.Fatal(e);
            }
        }

        private static void PrintInfoNotParsed()
        {
            var reader =
                new StreamReader(@"F:\Proyectos\TBCsimulation\WowHeadCrawler\Data_WoWHead\Armor.json");
            var foo = reader.ReadToEnd();
            var items = JsonConvert.DeserializeObject<List<Item>>(foo);
            reader =
                new StreamReader(@"F:\Proyectos\TBCsimulation\WowHeadCrawler\Data_WoWHead\Weapon.json");
            foo = reader.ReadToEnd();
            items.AddRange(JsonConvert.DeserializeObject<List<Item>>(foo));
            StreamWriter writerUse = new StreamWriter(@"Data/Use.log");
            StreamWriter writerEquip = new StreamWriter(@"Data/Equip.log");
            StreamWriter writerExtraLower = new StreamWriter(@"Data/ExtraLower.log");
            StreamWriter writerExtraUpper = new StreamWriter(@"Data/ExtraUpper.log");
            StreamWriter writerChanceOnHit = new StreamWriter(@"Data/ChanceOnHit.log");
            StreamWriter writerSetBonuses = new StreamWriter(@"Data/SetBonuses.log");
            foreach (var item in items)
            {
                if (item.Use.Count != 0)
                {
                    writerUse.WriteLine(item.ToString());
                    foreach (var s in item.Use)
                    {
                        writerUse.WriteLine("\t" + s);
                    }
                }

                if (item.Equip.Count != 0)
                {
                    writerEquip.WriteLine(item.ToString());
                    foreach (var s in item.Equip)
                    {
                        writerEquip.WriteLine("\t" + s);
                    }
                }

                if (item.ExtraLower.Count != 0)
                {
                    writerExtraLower.WriteLine(item.ToString());
                    foreach (var s in item.ExtraLower)
                    {
                        writerExtraLower.WriteLine("\t" + s);
                    }
                }

                if (item.ExtraUpper.Count != 0)
                {
                    writerExtraUpper.WriteLine(item.ToString());
                    foreach (var s in item.ExtraUpper)
                    {
                        writerExtraUpper.WriteLine("\t" + s);
                    }
                }

                if (item.ChanceOnHit.Count != 0)
                {
                    writerChanceOnHit.WriteLine(item.ToString());
                    foreach (var s in item.ChanceOnHit)
                    {
                        writerChanceOnHit.WriteLine("\t" + s);
                    }
                }

                if (item.SetBonuses.Count != 0)
                {
                    writerSetBonuses.WriteLine(item.ToString());
                    foreach (var s in item.SetBonuses)
                    {
                        writerSetBonuses.WriteLine("\t" + s.Key + " - " + s.Value);
                    }
                }
            }

            writerUse.Close();
            writerEquip.Close();
            writerExtraLower.Close();
            writerExtraUpper.Close();
            writerChanceOnHit.Close();
            writerSetBonuses.Close();
        }

        public static void PrintDictionary()
        {
            var reader =
                new StreamReader(@"F:\Proyectos\TBCsimulation\WowHeadCrawler\Data_WoWHead\Armor.json");
            var foo = reader.ReadToEnd();
            var items = JsonConvert.DeserializeObject<List<TBCsimulation.Data.Items.Item>>(foo);
            reader =
                new StreamReader(@"F:\Proyectos\TBCsimulation\WowHeadCrawler\Data_WoWHead\Weapon.json");
            foo = reader.ReadToEnd();
            Console.WriteLine(items.Count);
            var items2 = JsonConvert.DeserializeObject<List<TBCsimulation.Data.Items.Item>>(foo);
            Console.WriteLine(items2.Count);
            items.AddRange(items2);

            Console.WriteLine(items.Count);
            Dictionary<ItemKey, TBCsimulation.Data.Items.Item> dictionary = new();
            foreach (var item in items)
            {
                dictionary.Add(new ItemKey(item.Id, item.Name), item);
            }

            Console.WriteLine(dictionary.Count);

            var jsonArmor = JsonConvert.SerializeObject(items, Formatting.Indented);
            var writerArmor = new StreamWriter(Directory.GetCurrentDirectory() + @"\Data\Items.json");
            writerArmor.Write(jsonArmor);
            writerArmor.Close();
            log.Debug(dictionary.Count);

            var writerClass = new StreamWriter(Directory.GetCurrentDirectory() + @"\Data\ItemInfo.cs");
            writerClass.WriteLine("using System;");
            writerClass.WriteLine("namespace TBCsimulation.Data.Items");
            writerClass.WriteLine("{");
            writerClass.WriteLine("public partial class Item");
            writerClass.WriteLine("{");

            writerClass.WriteLine("private static void SetUpItems()");
            writerClass.WriteLine("{");
            writerClass.WriteLine("Item item;");
            foreach (var item in items)
            {
                if (item.Equip.Count == 0 && item.Use.Count == 0 && item.ChanceOnHit.Count == 0)
                    continue;
                writerClass.WriteLine("            // " + item.Id + " - " + item.Name);
                writerClass.WriteLine("item = GetItem(" + item.Id + ", \"" + item.Name + "\");");

                if (item.Equip.Count != 0)
                {
                    writerClass.WriteLine("item.OnEquip = (sim) =>");
                    writerClass.WriteLine("{");
                    foreach (var s in item.Equip)
                    {
                        writerClass.WriteLine("            //" + s);
                    }

                    writerClass.WriteLine("throw new NotImplementedException();");

                    writerClass.WriteLine("};");
                }

                if (item.Use.Count != 0)
                {
                    writerClass.WriteLine("item.OnUse = (sim) =>");
                    writerClass.WriteLine("{");
                    foreach (var s in item.Use)
                    {
                        writerClass.WriteLine("            //" + s);
                    }

                    writerClass.WriteLine("throw new NotImplementedException();");

                    writerClass.WriteLine("};");
                }

                if (item.ChanceOnHit.Count != 0)
                {
                    writerClass.WriteLine("item.OnChanceOnHit = (sim) =>");
                    writerClass.WriteLine("{");
                    foreach (var s in item.ChanceOnHit)
                    {
                        writerClass.WriteLine("            //" + s);
                    }

                    writerClass.WriteLine("throw new NotImplementedException();");

                    writerClass.WriteLine("};");
                }
            }

            writerClass.WriteLine("}");
            writerClass.WriteLine("}");
            writerClass.WriteLine("}");

            writerClass.Close();


            writerClass = new StreamWriter(Directory.GetCurrentDirectory() + @"\Data\ItemId.cs");


            writerClass.WriteLine("using System.Collections.Generic;");
            writerClass.WriteLine("using System.Linq;");

            writerClass.WriteLine("namespace TBCsimulation.Data.Items");
            writerClass.WriteLine("{");
            writerClass.WriteLine("public static class ItemId");
            writerClass.WriteLine("{");
            writerClass.WriteLine("public static int GetId(string Name)");
            writerClass.WriteLine("{");
            writerClass.WriteLine("    return NameToId.Find((pair => pair.Key.Equals(Name))).Value;");
            writerClass.WriteLine("}");
            writerClass.WriteLine("private static List<KeyValuePair<string,int>> NameToId = new()");
            writerClass.WriteLine("{");
            foreach (var item in items)
            {
                writerClass.WriteLine("    new KeyValuePair<string, int>(\"" + item.Name.Replace("\"", "\\\"") +
                                      "\", " + item.Id + "),  ");
            }

            writerClass.WriteLine(" };");
            writerClass.WriteLine("}");
            writerClass.WriteLine("}");

            writerClass.Close();
        }

        private static void SeparateJson()
        {
            var reader =
                new StreamReader(@"F:\Proyectos\TBCsimulation\WowHeadCrawler\Data_WoWHead\Armor.json");
            var foo = reader.ReadToEnd();
            var items = JsonConvert.DeserializeObject<List<Item>>(foo);

            Dictionary<EnumSim.Slot, List<Item>> separated = new Dictionary<EnumSim.Slot, List<Item>>();

            foreach (var item in items)
            {
                if (separated.ContainsKey(item.Slot))
                {
                    separated[item.Slot].Add(item);
                }
                else
                {
                    separated.Add(item.Slot, new List<Item>());
                    separated[item.Slot].Add(item);
                }
            }

            foreach (var pair in separated)
            {
                var jsonArmor = JsonConvert.SerializeObject(pair.Value, Formatting.Indented);
                var writerArmor = new StreamWriter(@"Data\" + Enum.GetName(typeof(EnumSim.Slot), pair.Key) + ".json");
                writerArmor.Write(jsonArmor);
                writerArmor.Close();
            }

            reader =
                new StreamReader(@"F:\Proyectos\TBCsimulation\WowHeadCrawler\Data_WoWHead\Weapon.json");
            foo = reader.ReadToEnd();
            items = JsonConvert.DeserializeObject<List<Item>>(foo);

            separated = new Dictionary<EnumSim.Slot, List<Item>>();

            foreach (var item in items)
            {
                if (separated.ContainsKey(item.Slot))
                {
                    separated[item.Slot].Add(item);
                }
                else
                {
                    separated.Add(item.Slot, new List<Item>());
                    separated[item.Slot].Add(item);
                }
            }

            foreach (var pair in separated)
            {
                var jsonArmor = JsonConvert.SerializeObject(pair.Value, Formatting.Indented);
                var writerArmor = new StreamWriter(@"Data\" + Enum.GetName(typeof(EnumSim.Slot), pair.Key) + ".json");
                writerArmor.Write(jsonArmor);
                writerArmor.Close();
            }
        }

        private static void ConvertJson()
        {
            var reader =
                new StreamReader(@"F:\Proyectos\TBCsimulation\WowHeadCrawler\Data_WoWHead\Armor_Raw.json");
            var foo = reader.ReadToEnd();
            var items = JsonConvert.DeserializeObject<List<Item>>(foo);
            Parallel.ForEach(items, new ParallelOptions() {MaxDegreeOfParallelism = 40}, item =>
                    // foreach (var item in items)
                {
                    log.Debug(item.Name);
                    item.Convert();
                }
            );
            var jsonArmor = JsonConvert.SerializeObject(items, Formatting.Indented);
            var writerArmor = new StreamWriter(@"Data\Armor.json");
            writerArmor.Write(jsonArmor);
            writerArmor.Close();

            reader =
                new StreamReader(@"F:\Proyectos\TBCsimulation\WowHeadCrawler\Data_WoWHead\Weapon_Raw.json");
            foo = reader.ReadToEnd();

            items = JsonConvert.DeserializeObject<List<Item>>(foo);
            Parallel.ForEach(items, new ParallelOptions() {MaxDegreeOfParallelism = 40}, item =>
                    // foreach (var item in items)
                {
                    log.Debug(item.Name);
                    item.Convert();
                }
            );
            log.Debug(items.Count);
            var jsonWeapon = JsonConvert.SerializeObject(items, Formatting.Indented);
            var writerWeapon = new StreamWriter(@"Data\Weapon.json");
            writerWeapon.Write(jsonWeapon);
            writerWeapon.Close();
        }

        private static void HtmlToJson()
        {
            //         ItemCrawler.ParseHtmlItem(
            // @"F:\Proyectos\TBCsimulation\WowHeadCrawler\Data_WoWHead\Armor\Amulets\Arctic+Pendant.html");

            var filePathsArmor =
                Directory.GetDirectories(@"F:\Proyectos\TBCsimulation\WowHeadCrawler\Data_WoWHead\Armor");

            var excludeArmor = new List<string>()
            {
                //     "Amulets", 
                //     "Cloaks", 
                //     "ClothChestArmor", 
                //     "ClothFootArmor", 
                //     "ClothHandArmor", 
                //     "ClothFootArmor", 
                //     "ClothHeadArmor", 
                //     "ClothLegArmor", 
                //     "ClothShoulderArmor", 
                //     "ClothWaistArmor", 
                //     "ClothWristArmor", 
                //     "Idols", 
                //     "LeatherChestArmor", 
                //     "LeatherFootArmor", 
                //     "LeatherHandArmor", 
                //     "LeatherFootArmor", 
                //     "LeatherHeadArmor", 
                //     "LeatherLegArmor", 
                //     "LeatherShoulderArmor", 
                //     "LeatherWaistArmor", 
                //     "LeatherWristArmor",
                //     "Librams", 
                //     "MailChestArmor", 
                //     "MailFootArmor", 
                //     "MailHandArmor", 
                //     "MailFootArmor", 
                //     "MailHeadArmor", 
                //     "MailLegArmor", 
                //     "MailShoulderArmor", 
                //     "MailWaistArmor", 
                //     "MailWristArmor",
                //     "Off-handFrills", 
                //     "PlateChestArmor", 
                //     "PlateFootArmor", 
                //     "PlateHandArmor", 
                //     "PlateFootArmor", 
                //     "PlateHeadArmor", 
                //     "PlateLegArmor", 
                //     "PlateShoulderArmor", 
                //     "PlateWaistArmor", 
                //     "PlateWristArmor",
            };
            Parallel.ForEach(filePathsArmor, filePath =>

                    // foreach (var filePath in filePathsArmor)
                {
                    var found = false;
                    foreach (var name in excludeArmor)
                        if (filePath.Contains(name))
                            found = true;
                    if (found)
                        return;
                    // continue;
                    log.Info(filePath);
                    try
                    {
                        ItemCrawler.ParseHtml(filePath);
                    }
                    catch (Exception e)
                    {
                        log.Fatal(e);
                        throw;
                    }
                }
            );

            var jsonArmor = JsonConvert.SerializeObject(ItemCrawler.items, Formatting.Indented);
            var writerArmor = new StreamWriter(@"Data\Armor_Raw.json");
            writerArmor.Write(jsonArmor);
            writerArmor.Close();
            ItemCrawler.items = new List<Item>();

            var filePathsWeapon =
                Directory.GetDirectories(@"F:\Proyectos\TBCsimulation\WowHeadCrawler\Data_WoWHead\Weapon");

            Parallel.ForEach(filePathsWeapon, filePath =>

                    // foreach (var filePath in filePathsWeapon)
                {
                    log.Info(filePath);
                    try
                    {
                        ItemCrawler.ParseHtml(filePath);
                    }
                    catch (Exception e)
                    {
                        log.Fatal(e);
                        throw;
                    }
                }
            );

            var jsonWeapon = JsonConvert.SerializeObject(ItemCrawler.items, Formatting.Indented);
            var writerWeapon = new StreamWriter(@"Data\Weapon_Raw.json");
            writerWeapon.Write(jsonWeapon);
            writerWeapon.Close();
        }
    }
}