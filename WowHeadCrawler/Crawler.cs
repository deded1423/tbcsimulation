﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using HtmlAgilityPack;
using PuppeteerSharp;

namespace WowHeadCrawler
{
    public static class Crawler
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        private static Page basePage;
        private static Page talentPage;
        private static Browser browser;
        public static List<Talent> talents = new();
        public static List<Spell> spells = new();
        public static List<Spell> spellsFinal = new();

        public static void DownloadTalents(string FileName, string wowclass)
        {
            var talentUrl = "http://tbc.wowhead.com/talent-calc/" + wowclass.ToLower();
            var wowheadBaseUrl = "http://tbc.wowhead.com";
            Task.WaitAll(Initialize());
            Task.WaitAll(basePage.GoToAsync(talentUrl));
            talents = new List<Talent>();
            var htmlDocument = new HtmlDocument();
            htmlDocument.LoadHtml(Task.WhenAll(basePage.GetContentAsync()).Result[0]);
            var talentTree = SelectNodes(htmlDocument, @"//*[@id=""main-contents""]/div[3]/div[2]/div[2]");

            foreach (var node in talentTree.Nodes())
            {
                var SecondarySchoolType = node.ChildNodes[0].ChildNodes["b"].InnerText;
                foreach (var talentDiv in node.ChildNodes[1].ChildNodes)
                {
                    if (!talentDiv.Attributes["class"].Value.Equals("ctc-tree-talent"))
                    {
                        log.Info("continue");
                        continue;
                    }

                    try
                    {
                        log.Info("new");
                        log.Info(talentDiv);
                        log.Info(talentDiv.Attributes);
                        var row = int.Parse(talentDiv.Attributes["data-row"].Value);
                        var col = int.Parse(talentDiv.Attributes["data-col"].Value);
                        var maxRank = int.Parse(talentDiv.Attributes["data-max-points"].Value);
                        var pathTalent = talentDiv.ChildNodes["div"].ChildNodes["a"].Attributes["href"].Value;
                        log.Info(pathTalent);
                        var id = int.Parse(pathTalent.Replace("/spell=", ""));

                        log.Info(wowheadBaseUrl + pathTalent);

                        try
                        {
                            talentPage.CloseAsync();
                        }
                        catch (Exception e)
                        {
                            log.Info("talentPage");
                            log.Info(e);
                        }

                        talentPage = Task.WhenAll(browser.NewPageAsync()).Result[0];
                        Task.WaitAll(talentPage.GoToAsync(wowheadBaseUrl + pathTalent));
                        var htmlTalentDocument = new HtmlDocument();
                        var foo = Task.WhenAll(talentPage.GetContentAsync()).Result[0];
                        log.Info("After");
                        log.Info(foo.Length);
                        htmlTalentDocument.LoadHtml(foo);
                        var spellTable = SelectNodes(htmlTalentDocument,
                            @"/html/body/div[4]/div/div[1]/div[2]/div[4]/div[3]/div[4]/table/tbody/tr[1]/td")[0];
                        log.Info(spellTable.ChildNodes.Count);
                        var spellDescription = spellTable.ChildNodes[1].ChildNodes["tbody"].ChildNodes["tr"]
                            .ChildNodes["td"].ChildNodes["div"].InnerText;
                        log.Info(spellDescription);
                        log.Info("spellDescription");
                        var t = SelectNodes(htmlTalentDocument,
                            @"/html/body/div[4]/div/div[1]/div[2]/div[4]/div[3]/div[4]/table/tbody/tr[1]/td/table[1]/tbody/tr/td/table/tbody/tr/td/a/b");

                        if (t == null)
                            t = SelectNodes(htmlTalentDocument,
                                @"/html/body/div[4]/div/div[1]/div[2]/div[4]/div[3]/div[4]/table/tbody/tr[1]/td/table[1]/tbody/tr/td/a/b");

                        log.Info(t[0].InnerText);
                        var spellName = t[0].InnerText;
                        log.Info(spellName);

                        var talent = new Talent()
                        {
                            Id = id,
                            Row = row,
                            Col = col,
                            MaxRank = maxRank,
                            Desc = spellDescription,
                            Name = spellName,
                            School = SecondarySchoolType,
                            Wowclass = wowclass
                        };
                        talents.Add(talent);
                    }
                    catch (Exception e)
                    {
                        log.Info("ERROR");
                        log.Info(e);
                    }
                }
            }

            basePage.CloseAsync();
            PrintTalents(FileName, wowclass);
        }


        public static void PrintTalents(string FileName, string wowClass)
        {
            var writer = new StreamWriter(Directory.GetCurrentDirectory() + FileName);
            writer.WriteLine("using System.Collections.Generic;");
            writer.WriteLine("using TBCsimulation.Data.Utils;");
            writer.WriteLine("using TBCsimulation.Simulation;");

            writer.WriteLine("namespace TBCsimulation.Data.TalentsInfo");
            writer.WriteLine("{");
            writer.WriteLine("public static partial class Talents");
            writer.WriteLine("{");
            writer.WriteLine("public static readonly List<Talent> " + wowClass + " = new List<Talent>()");
            writer.WriteLine("{");
            foreach (var talent in talents)
            {
                writer.WriteLine("new Talent()");
                writer.WriteLine("{");
                writer.WriteLine("Id = " + talent.Id + ",");
                writer.WriteLine("Name = \"" + talent.Name + "\",");
                writer.WriteLine("WowClass = EnumSim.WowClass." + talent.Wowclass + ",");
                var secondary = talent.School.Replace(" ", "");
                if (secondary.Equals("Shadow")) secondary = "ShadowMagic";

                if (secondary.Equals("Elemental")) secondary = "ElementalCombat";

                writer.WriteLine("SecondarySchoolType = EnumSim.SecondarySchoolType." + talent.Wowclass + "_" +
                                 secondary + ",");
                writer.WriteLine("MaxRank = " + talent.MaxRank + ",");
                writer.WriteLine("TalentRequired = -1,");
                writer.WriteLine("Tooltip = \"" + talent.Desc + "\",");
                writer.WriteLine("Row = " + talent.Row + ",");
                writer.WriteLine("Column = " + talent.Col + "");

                writer.WriteLine("},");
            }

            writer.WriteLine(" };");
            writer.WriteLine("}");
            writer.WriteLine("}");
            writer.Close();
        }

        public static void DownloadSpells(string FileName, string wowclass)
        {
            DownloadSpells(FileName, wowclass, "abilities/");
        }

        public static void DownloadSpells(string FileName, string wowclass, string extraUrl)
        {
            var talentUrl = "https://tbc.wowhead.com/spells/" + extraUrl + wowclass.ToLower();
            var wowheadBaseUrl = "http://tbc.wowhead.com";
            spells = new List<Spell>();
            Task.WaitAll(Initialize());
            var cont = true;
            var addedUrl = "";
            while (cont)
            {
                log.Info(talentUrl + addedUrl);
                basePage = Task.WhenAll(browser.NewPageAsync()).Result[0];
                Task.WaitAll(basePage.GoToAsync(talentUrl + addedUrl));
                var htmlDocument = new HtmlDocument();
                htmlDocument.LoadHtml(Task.WhenAll(basePage.GetContentAsync()).Result[0]);
                Task.WaitAll(basePage.CloseAsync());
                var spellTable = SelectNodes(htmlDocument,
                    @"/html/body/div[4]/div/div[1]/div[2]/div[4]/div[4]/div[2]/div/table/tbody")[0];
                var continueButton = SelectNodes(htmlDocument,
                    @"/html/body/div[4]/div/div[1]/div[2]/div[4]/div[4]/div[1]/div[1]/a[4]")[0];

                if (continueButton.Attributes["data-active"].Value.Equals("yes"))
                {
                    if (!addedUrl.Equals(""))
                    {
                        if (!addedUrl.Equals("#200"))
                        {
                            if (!addedUrl.Equals("#300"))
                                addedUrl = "#400";
                            else
                                addedUrl = "#300";
                        }
                        else
                        {
                            addedUrl = "#200";
                        }
                    }
                    else
                    {
                        addedUrl = "#100";
                    }
                }
                else
                {
                    cont = false;
                }

                Download(spellTable, wowheadBaseUrl);
                log.Info("spells.Count = " + spells.Count);
            }

            CleanSpells();
            log.Info("spells.Count = " + spells.Count);
            PrintSpellsTxt(FileName, wowclass);
            PrintSpells(FileName, wowclass);

            void Download(HtmlNode htmlNode, string s)
            {
                var options = new ParallelOptions();
                options.MaxDegreeOfParallelism = 5;
                Parallel.ForEach(htmlNode.ChildNodes, options, node =>
                    // foreach (var node in htmlNode.ChildNodes)
                {
                    // CastTime = 1500;
                    // Channeled = false;
                    // CooldownTime = 0;
                    // IsGCD = true;
                    // TickTime = 0;
                    //
                    // Mana = 200;
                    // Rank = 2;
                    //
                    // *Range* 
                    // 
                    // School = EnumSim.SchoolType.Shadow;
                    // SecondarySchool = EnumSim.SecondarySchoolType.Warlock_Demonology;
                    // ActionDelegate = delegate { throw new NotImplementedException(); };

                    var name = node.ChildNodes[1].ChildNodes["div"].ChildNodes["a"].InnerText;
                    log.Info("name: " + name);

                    string rank;

                    try
                    {
                        rank = node.ChildNodes[1].ChildNodes["div"].ChildNodes["div"].InnerText;
                    }
                    catch (Exception)
                    {
                        log.Info("No rank found");
                        rank = "";
                    }

                    log.Info("rank: " + rank);

                    var url = node.ChildNodes[1].ChildNodes["div"].ChildNodes["a"].Attributes["href"].Value;
                    log.Info("url: " + url);
                    var id = int.Parse(url.Split('/')[1].Replace("spell=", ""));
                    log.Info("id: " + id);

                    if (node.ChildNodes[2].InnerText.Equals("") && !wowclass.Equals("")) return;

                    var level = 0;
                    try
                    {
                        level = int.Parse(node.ChildNodes[2].InnerText);
                    }
                    catch (Exception)
                    {
                        level = 0;
                    }

                    log.Info("level: " + level);
                    var school = "";
                    try
                    {
                        school = node.ChildNodes[4].InnerText;
                    }
                    catch (ArgumentOutOfRangeException)
                    {
                        school = "Racial";
                    }

                    log.Info("school: " + school);

                    basePage = Task.WhenAll(browser.NewPageAsync()).Result[0];
                    log.Info(s + url);
                    try
                    {
                        Task.WaitAll(basePage.GoToAsync(s + url));
                    }
                    catch (Exception)
                    {
                        log.Info("FAIL");
                        Task.WaitAll(basePage.CloseAsync());
                        Thread.Sleep(1000);
                        basePage = Task.WhenAll(browser.NewPageAsync()).Result[0];
                        log.Info(s + url);
                        Task.WaitAll(basePage.GoToAsync(s + url));
                    }

                    log.Info(basePage.GetTitleAsync().Result);
                    var foo = Task.WhenAll(basePage.GetContentAsync()).Result[0];
                    var htmlDocumentSpell = new HtmlDocument();
                    htmlDocumentSpell.LoadHtml(foo);
                    Task.WaitAll(basePage.CloseAsync());
                    Thread.Sleep(1000);

                    log.Info("3");

                    var mana = SelectNodes(htmlDocumentSpell,
                        @"/html/body/div[4]/div/div[1]/div[2]/div[4]/div[3]/table/tbody/tr[2]/td")[0].InnerText;
                    log.Info("mana: " + mana);
                    var casttime = SelectNodes(htmlDocumentSpell,
                        @"/html/body/div[4]/div/div[1]/div[2]/div[4]/div[3]/table/tbody/tr[4]/td")[0].InnerText;
                    log.Info("casttime: " + casttime);
                    var range = SelectNodes(htmlDocumentSpell,
                        @"/html/body/div[4]/div/div[1]/div[2]/div[4]/div[3]/table/tbody/tr[3]/td")[0].InnerText;
                    log.Info("range: " + range);
                    var gcd = SelectNodes(htmlDocumentSpell,
                        @"/html/body/div[4]/div/div[1]/div[2]/div[4]/div[3]/table/tbody/tr[6]/td")[0].InnerText;
                    log.Info("gcd: " + gcd);
                    var duration = SelectNodes(htmlDocumentSpell,
                            @"/html/body/div[4]/div/div[1]/div[2]/div[4]/div[3]/table/tbody/tr[1]/td[2]/table/tbody/tr[2]/td")
                        [0]
                        .InnerText;
                    log.Info("duration: " + duration);
                    var secondary = "";
                    try
                    {
                        secondary = SelectNodes(htmlDocumentSpell,
                                @"/html/body/div[4]/div/div[1]/div[2]/div[3]/div/span[5]/a")[0]
                            .InnerText;
                    }
                    catch (NullReferenceException)
                    {
                        secondary = "Racial";
                    }

                    log.Info("secondary: " + secondary);
                    var cooldown = SelectNodes(htmlDocumentSpell,
                        @"/html/body/div[4]/div/div[1]/div[2]/div[4]/div[3]/table/tbody/tr[5]/td")[0].InnerText;
                    log.Info("cooldown: " + cooldown);

                    if (secondary.Equals("Internal")) return;

                    var spell = new Spell();
                    spell.Url = s + url;
                    spell.Name = name;
                    spell.Id = id;
                    spell.Level = level;
                    spell.School = school;
                    spell.SecondarySchool = secondary;
                    try
                    {
                        spell.Mana = int.Parse(mana.Replace("Mana", "").Replace(" ", ""));
                    }
                    catch (Exception)
                    {
                        spell.Mana = -1;
                        if (school.Equals("Racial")) spell.Mana = 0;
                    }

                    spell.Range = int.Parse(range.Split(' ')[0]);

                    if (gcd.Contains("0"))
                        spell.IsGCD = false;
                    else
                        spell.IsGCD = true;

                    var MatchPhraseRank = @"(Rank )(?<i>\d*)";
                    if (Regex.IsMatch(rank, MatchPhraseRank))
                    {
                        var groups = Regex.Matches(rank, MatchPhraseRank)[0].Groups;
                        spell.Rank = int.Parse(groups["i"].Value);
                    }
                    else
                    {
                        log.Info("Rank error - " + rank);
                        spell.Rank = 1;
                    }

                    if (cooldown.Equals("n/a"))
                    {
                        spell.CooldownTime = 0;
                    }
                    else
                    {
                        if (cooldown.Contains("minutes"))
                            spell.CooldownTime = (int) (float.Parse(cooldown.Split(' ')[0]) * 60 * 1000);
                        else
                            spell.CooldownTime = (int) (float.Parse(cooldown.Split(' ')[0]) * 1000);
                    }

                    if (casttime.Equals("Channeled"))
                    {
                        spell.Channeled = true;
                        spell.CastTime = int.Parse(duration.Split(' ')[0]) * 1000;
                    }
                    else
                    {
                        spell.Channeled = false;
                        if (casttime.Equals("Instant"))
                            spell.CastTime = 0;
                        else
                            spell.CastTime = (int) (float.Parse(casttime.Split(' ')[0]) * 1000);
                    }

                    log.Info("Add " + spell.Name);
                    spells.Add(spell);
                });
            }
        }

        public static void DownloadSpellById(string FileName, string wowclass, int[] ids)
        {
            var url = "https://tbc.wowhead.com/spell=";
            var wowheadBaseUrl = "http://tbc.wowhead.com";
            spells = new List<Spell>();
            Task.WaitAll(Initialize());
            var cont = true;
            var addedUrl = "";

            foreach (var i in ids) GetValue(i);


            log.Info("spells.Count = " + spells.Count);

            CleanSpells();
            PrintSpellsTxt(FileName, wowclass);
            PrintSpells(FileName, wowclass);

            void GetValue(int id)
            {
                basePage = Task.WhenAll(browser.NewPageAsync()).Result[0];
                log.Info(url + id);
                try
                {
                    Task.WaitAll(basePage.GoToAsync(url + id));
                }
                catch (Exception)
                {
                    log.Info("FAIL");
                    Task.WaitAll(basePage.CloseAsync());
                    Thread.Sleep(1000);
                    basePage = Task.WhenAll(browser.NewPageAsync()).Result[0];
                    log.Info(url + id);
                    Task.WaitAll(basePage.GoToAsync(url + id));
                }

                log.Info(basePage.GetTitleAsync().Result);
                var foo = Task.WhenAll(basePage.GetContentAsync()).Result[0];
                var htmlDocumentSpell = new HtmlDocument();
                htmlDocumentSpell.LoadHtml(foo);
                Task.WaitAll(basePage.CloseAsync());
                Thread.Sleep(1000);

                string name = null;
                try
                {
                    name = SelectNodes(htmlDocumentSpell,
                            @"/html/body/div[4]/div/div[1]/div[2]/div[4]/div[3]/div[4]/table/tbody/tr[1]/td/table[1]/tbody/tr/td/a/b")
                        [0]
                        .InnerText;
                }
                catch (Exception)
                {
                    log.Info("No name found");
                    name = SelectNodes(htmlDocumentSpell,
                            @"/html/body/div[4]/div/div[1]/div[2]/div[4]/div[3]/div[4]/table/tbody/tr[1]/td/table[1]/tbody/tr/td/table[1]/tbody/tr/td/a/b")
                        [0]
                        .InnerText;
                }

                log.Info("name: " + name);

                string rank;

                try
                {
                    rank = SelectNodes(htmlDocumentSpell,
                            @"/html/body/div[4]/div/div[1]/div[2]/div[4]/div[3]/div[4]/table/tbody/tr[1]/td/table[1]/tbody/tr/td/table[1]/tbody/tr/th/b")
                        [0].InnerText;
                }
                catch (Exception)
                {
                    log.Info("No rank found");
                    rank = "";
                }

                log.Info("rank: " + rank);

                log.Info("id: " + id);

                var level = 0;
                try
                {
                    level = int.Parse(SelectNodes(htmlDocumentSpell,
                            @"/html/body/div[4]/div/div[1]/div[2]/div[4]/div[3]/div[4]/table/tbody/tr[1]/td/table[1]/tbody/tr/td/span")
                        [0]
                        .InnerText.Replace("Requires level ", ""));
                }
                catch (Exception)
                {
                    log.Info("No level found");
                    level = 0;
                }

                log.Info("level: " + level);
                var school = SelectNodes(htmlDocumentSpell,
                        @"/html/body/div[4]/div/div[1]/div[2]/div[4]/div[3]/table/tbody/tr[1]/td[2]/table/tbody/tr[3]/td")
                    [0]
                    .InnerText;
                log.Info("school: " + school);


                log.Info("3");

                var mana = SelectNodes(htmlDocumentSpell,
                    @"/html/body/div[4]/div/div[1]/div[2]/div[4]/div[3]/table/tbody/tr[2]/td")[0].InnerText;
                log.Info("mana: " + mana);
                var casttime = SelectNodes(htmlDocumentSpell,
                    @"/html/body/div[4]/div/div[1]/div[2]/div[4]/div[3]/table/tbody/tr[4]/td")[0].InnerText;
                log.Info("casttime: " + casttime);
                var range = SelectNodes(htmlDocumentSpell,
                    @"/html/body/div[4]/div/div[1]/div[2]/div[4]/div[3]/table/tbody/tr[3]/td")[0].InnerText;
                log.Info("range: " + range);
                var gcd = SelectNodes(htmlDocumentSpell,
                    @"/html/body/div[4]/div/div[1]/div[2]/div[4]/div[3]/table/tbody/tr[6]/td")[0].InnerText;
                log.Info("gcd: " + gcd);
                var duration = SelectNodes(htmlDocumentSpell,
                        @"/html/body/div[4]/div/div[1]/div[2]/div[4]/div[3]/table/tbody/tr[1]/td[2]/table/tbody/tr[2]/td")
                    [0]
                    .InnerText;
                log.Info("duration: " + duration);
                var secondary = SelectNodes(htmlDocumentSpell,
                        @"/html/body/div[4]/div/div[1]/div[2]/div[4]/div[3]/div[4]/table/tbody/tr[1]/td/table[1]/tbody/tr/td/div[2]")
                    [0].InnerText.Split('(')[1].Replace(")", "");
                log.Info("secondary: " + secondary);
                var cooldown = SelectNodes(htmlDocumentSpell,
                    @"/html/body/div[4]/div/div[1]/div[2]/div[4]/div[3]/table/tbody/tr[5]/td")[0].InnerText;
                log.Info("cooldown: " + cooldown);

                if (secondary.Equals("Internal")) return;

                var spell = new Spell();
                spell.Url = url + id;
                spell.Name = name;
                spell.Id = id;
                spell.Level = level;
                spell.School = school;
                spell.SecondarySchool = secondary;
                try
                {
                    spell.Mana = int.Parse(mana.Replace("Mana", "").Replace(" ", ""));
                }
                catch (Exception)
                {
                    spell.Mana = -1;
                }

                spell.Range = int.Parse(range.Split(' ')[0]);

                if (gcd.Contains("0"))
                    spell.IsGCD = false;
                else
                    spell.IsGCD = true;

                var MatchPhraseRank = @"(Rank )(?<i>\d*)";
                if (Regex.IsMatch(rank, MatchPhraseRank))
                {
                    var groups = Regex.Matches(rank, MatchPhraseRank)[0].Groups;
                    spell.Rank = int.Parse(groups["i"].Value);
                }
                else
                {
                    log.Info("Rank error - " + rank);
                    spell.Rank = 1;
                }

                if (cooldown.Equals("n/a"))
                {
                    spell.CooldownTime = 0;
                }
                else
                {
                    if (cooldown.Contains("minutes"))
                        spell.CooldownTime = int.Parse(cooldown.Split(' ')[0]) * 60 * 1000;
                    else
                        spell.CooldownTime = int.Parse(cooldown.Split(' ')[0]) * 1000;
                }

                if (casttime.Equals("Channeled"))
                {
                    spell.Channeled = true;
                    spell.CastTime = int.Parse(duration.Split(' ')[0]) * 1000;
                }
                else
                {
                    spell.Channeled = false;
                    if (casttime.Equals("Instant"))
                        spell.CastTime = 0;
                    else
                        spell.CastTime = (int) (float.Parse(casttime.Split(' ')[0]) * 1000);
                }

                spells.Add(spell);
                return;
            }
        }

        public static void PrintSpellsTxt(string FileName, string wowClass)
        {
            var writer =
                new StreamWriter(Directory.GetCurrentDirectory() + FileName + "Spell_" + wowClass + "Extra.txt");
            foreach (var spell in spellsFinal)
            {
                var nameformatted = spell.Name
                    .Replace(" ", "")
                    .Replace("'", "")
                    .Replace(":", "")
                    .Replace("-", "")
                    .Replace("(", "")
                    .Replace(")", "");

                writer.WriteLine("Url = " + spell.Url);
                writer.WriteLine("Name = " + spell.Name);
                writer.WriteLine("Nameformatted = " + nameformatted);
                writer.WriteLine("Id = " + spell.Id);
                writer.WriteLine("Level = " + spell.Level);
                writer.WriteLine("School = " + spell.School);
                writer.WriteLine("SecondarySchool = " + spell.SecondarySchool);
                writer.WriteLine("Mana = " + spell.Mana);
                writer.WriteLine("Range = " + spell.Range);
                writer.WriteLine("IsGCD = " + spell.IsGCD);
                writer.WriteLine("Rank = " + spell.Rank);
                writer.WriteLine("Channeled = " + spell.Channeled);
                writer.WriteLine("CooldownTime = " + spell.CooldownTime);
                writer.WriteLine("CastTime = " + spell.CastTime);
                writer.WriteLine("TickTime = " + spell.TickTime);
                writer.WriteLine();
                writer.WriteLine();
            }

            writer.Close();
        }

        public static void PrintSpells(string FileName, string wowClass)
        {
            var exists = Directory.Exists(Directory.GetCurrentDirectory() + @"\Data" + @"\" + wowClass);

            if (!exists)
                Directory.CreateDirectory(Directory.GetCurrentDirectory() + @"\Data" + @"\" + wowClass);

            exists = Directory.Exists(Directory.GetCurrentDirectory() + @"\Data" + @"\Info\" + wowClass + @"\Info");

            if (!exists)
                Directory.CreateDirectory(Directory.GetCurrentDirectory() + @"\Data" + @"\" + wowClass + @"\Info");

            foreach (var spell in spellsFinal)
            {
                var nameformatted = spell.Name
                    .Replace(" ", "")
                    .Replace("'", "")
                    .Replace(":", "")
                    .Replace("-", "")
                    .Replace("(", "")
                    .Replace(")", "");
                var writer = new StreamWriter(Directory.GetCurrentDirectory() + FileName + @"\" + wowClass +
                                              @"\Info\" + nameformatted + "SpellInfo.cs");

                writer.WriteLine("using System;");
                writer.WriteLine("using TBCsimulation.Data.Utils;");
                writer.WriteLine("using TBCsimulation.Simulation;");
                writer.WriteLine("");
                writer.WriteLine("");
                if (wowClass.Equals(""))
                    writer.WriteLine("namespace TBCsimulation.Data." + "Racial" + ".Spell");
                else
                    writer.WriteLine("namespace TBCsimulation.Data." + wowClass + ".Spell");

                writer.WriteLine("{");
                writer.WriteLine("public partial class " + nameformatted + "Spell : SpellsInfo.Spell");
                writer.WriteLine("{");
                writer.WriteLine("private static readonly log4net.ILog log =");
                writer.WriteLine(
                    "    log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);");
                writer.WriteLine("public " + nameformatted + "Spell(FightSim sim)");
                writer.WriteLine("");
                writer.WriteLine("{");
                writer.WriteLine("");
                writer.WriteLine("//" + spell.Url);
                writer.WriteLine("Name = \"" + spell.Name + "\";");
                writer.WriteLine("Id = " + spell.Id + ";");
                writer.WriteLine("Level = " + spell.Level + ";");
                writer.WriteLine("Rank = " + spell.Rank + ";");
                writer.WriteLine("Range = " + spell.Range + ";");
                writer.WriteLine("");

                writer.WriteLine("Mana = " + spell.Mana + ";");
                writer.WriteLine("");
                writer.WriteLine("IsGCD = " + spell.IsGCD.ToString().ToLower() + ";");
                writer.WriteLine("Channeled = " + spell.Channeled.ToString().ToLower() + ";");
                writer.WriteLine("CastTime = " + spell.CastTime + ";");
                writer.WriteLine("CooldownTime = " + spell.CooldownTime + ";");
                writer.WriteLine("");
                writer.WriteLine("School = EnumSim.SchoolType." + spell.School + ";");
                if (wowClass.Equals(""))
                    writer.WriteLine("SecondarySchool = EnumSim.SecondarySchoolType." +
                                     spell.SecondarySchool.Replace(" ", "") + ";");
                else
                    writer.WriteLine("SecondarySchool = EnumSim.SecondarySchoolType." + wowClass + "_" +
                                     spell.SecondarySchool.Replace(" ", "") + ";");

                writer.WriteLine("Init(sim);");
                writer.WriteLine("}");
                writer.WriteLine("}");
                writer.WriteLine("}");
                writer.Close();

                writer = new StreamWriter(Directory.GetCurrentDirectory() + FileName + @"\" + wowClass + @"\" +
                                          nameformatted + "Spell.cs");

                writer.WriteLine("using System;");
                writer.WriteLine("using TBCsimulation.Data.Utils;");
                writer.WriteLine("using TBCsimulation.Simulation;");

                if (wowClass.Equals(""))
                    writer.WriteLine("namespace TBCsimulation.Data." + "Racial" + ".Spell");
                else
                    writer.WriteLine("namespace TBCsimulation.Data." + wowClass + ".Spell");

                writer.WriteLine("{");
                writer.WriteLine("public partial class " + nameformatted + "Spell : SpellsInfo.Spell");
                writer.WriteLine("{");
                writer.WriteLine("private void Init(FightSim sim)");
                writer.WriteLine("{");
                writer.WriteLine("    ActionDelegate = delegate { throw new NotImplementedException(); };");
                writer.WriteLine("}");
                writer.WriteLine("}");
                writer.WriteLine("}");

                writer.Close();
            }
        }

        public static void CleanSpells()
        {
            spellsFinal = new List<Spell>();
            foreach (var spell in spells)
            {
                var before = spellsFinal.Find(s => spell.Name.Equals(s.Name));
                if (before != null)
                {
                    if (before.Id <= spell.Id)
                    {
                        spellsFinal.Remove(before);
                        spellsFinal.Add(spell);
                    }
                }
                else
                {
                    spellsFinal.Add(spell);
                }
            }
        }

        private static async Task Initialize()
        {
            Task.WaitAll(new BrowserFetcher().DownloadAsync(BrowserFetcher.DefaultChromiumRevision));

            // Create an instance of the browser and configure launch options
            browser = await Puppeteer.LaunchAsync(new LaunchOptions
            {
                Headless = true
            });

            // Create a new page and go to Bing Maps
            basePage = await browser.NewPageAsync();
        }

        private static HtmlNodeCollection SelectNodes(HtmlDocument htmlDocument, string xpath)
        {
            var node = htmlDocument.DocumentNode.SelectNodes(xpath);
            if (node == null) log.Info("node == null - " + xpath);

            return node;
        }
    }
}