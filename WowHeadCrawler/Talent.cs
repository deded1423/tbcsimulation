﻿namespace WowHeadCrawler
{
    public class Talent
    {
        public int Col;
        public string Desc;
        public int Id;
        public int MaxRank;
        public string Name;
        public int Row;
        public string School;
        public string Wowclass;
    }
}