﻿using System;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace WowHeadCrawler
{
    internal static class Program
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        private static void Main(string[] args)
        {
            var exists = Directory.Exists(Directory.GetCurrentDirectory() + @"\Data");

            if (!exists)
                Directory.CreateDirectory(Directory.GetCurrentDirectory() + @"\Data");

            GemCrawler.PrintDictionary();
            Thread.Sleep(1000);
        }

        public static void ParseGems()
        {
            GemCrawler.ParseHtml(@"F:\Proyectos\TBCsimulation\WowHeadCrawler\Data_WoWHead\Gems");

            var jsonWeapon = JsonConvert.SerializeObject(GemCrawler.items, Formatting.Indented);
            var writerWeapon = new StreamWriter(@"Data\Gem.json");
            writerWeapon.Write(jsonWeapon);
            writerWeapon.Close();
            Console.ReadKey();
        }

        public static void DownloadGems()
        {
            Task.WaitAll(GemCrawler.Initialize());
            GemCrawler.DownloadHtml(@"https://tbc.wowhead.com/items/gems");
            Task.WaitAll(GemCrawler.browser.CloseAsync());
            Console.ReadKey();
        }

        private static void ConvertFileNames()
        {
            ItemCrawler.ConvertFiles(Directory.GetCurrentDirectory() + @"\Data\ClothChestArmor");
            ItemCrawler.ConvertFiles(Directory.GetCurrentDirectory() + @"\Data\ClothFootArmor");
            ItemCrawler.ConvertFiles(Directory.GetCurrentDirectory() + @"\Data\ClothHandArmor");
            ItemCrawler.ConvertFiles(Directory.GetCurrentDirectory() + @"\Data\ClothHeadArmor");
            ItemCrawler.ConvertFiles(Directory.GetCurrentDirectory() + @"\Data\ClothLegArmor");
            ItemCrawler.ConvertFiles(Directory.GetCurrentDirectory() + @"\Data\ClothShoulderArmor");
            ItemCrawler.ConvertFiles(Directory.GetCurrentDirectory() + @"\Data\ClothWaistArmor");
            ItemCrawler.ConvertFiles(Directory.GetCurrentDirectory() + @"\Data\ClothWristArmor");

            ItemCrawler.ConvertFiles(Directory.GetCurrentDirectory() + @"\Data\LeatherChestArmor");
            ItemCrawler.ConvertFiles(Directory.GetCurrentDirectory() + @"\Data\LeatherFootArmor");
            ItemCrawler.ConvertFiles(Directory.GetCurrentDirectory() + @"\Data\LeatherHandArmor");
            ItemCrawler.ConvertFiles(Directory.GetCurrentDirectory() + @"\Data\LeatherHeadArmor");
            ItemCrawler.ConvertFiles(Directory.GetCurrentDirectory() + @"\Data\LeatherLegArmor");
            ItemCrawler.ConvertFiles(Directory.GetCurrentDirectory() + @"\Data\LeatherShoulderArmor");
            ItemCrawler.ConvertFiles(Directory.GetCurrentDirectory() + @"\Data\LeatherWaistArmor");
            ItemCrawler.ConvertFiles(Directory.GetCurrentDirectory() + @"\Data\LeatherWristArmor");

            ItemCrawler.ConvertFiles(Directory.GetCurrentDirectory() + @"\Data\MailChestArmor");
            ItemCrawler.ConvertFiles(Directory.GetCurrentDirectory() + @"\Data\MailFootArmor");
            ItemCrawler.ConvertFiles(Directory.GetCurrentDirectory() + @"\Data\MailHandArmor");
            ItemCrawler.ConvertFiles(Directory.GetCurrentDirectory() + @"\Data\MailHeadArmor");
            ItemCrawler.ConvertFiles(Directory.GetCurrentDirectory() + @"\Data\MailLegArmor");
            ItemCrawler.ConvertFiles(Directory.GetCurrentDirectory() + @"\Data\MailShoulderArmor");
            ItemCrawler.ConvertFiles(Directory.GetCurrentDirectory() + @"\Data\MailWaistArmor");
            ItemCrawler.ConvertFiles(Directory.GetCurrentDirectory() + @"\Data\MailWristArmor");

            ItemCrawler.ConvertFiles(Directory.GetCurrentDirectory() + @"\Data\PlateChestArmor");
            ItemCrawler.ConvertFiles(Directory.GetCurrentDirectory() + @"\Data\PlateFootArmor");
            ItemCrawler.ConvertFiles(Directory.GetCurrentDirectory() + @"\Data\PlateHandArmor");
            ItemCrawler.ConvertFiles(Directory.GetCurrentDirectory() + @"\Data\PlateHeadArmor");
            ItemCrawler.ConvertFiles(Directory.GetCurrentDirectory() + @"\Data\PlateLegArmor");
            ItemCrawler.ConvertFiles(Directory.GetCurrentDirectory() + @"\Data\PlateShoulderArmor");
            ItemCrawler.ConvertFiles(Directory.GetCurrentDirectory() + @"\Data\PlateWaistArmor");
            ItemCrawler.ConvertFiles(Directory.GetCurrentDirectory() + @"\Data\PlateWristArmor");
        }

        private static void DownloadSpells()
        {
            Crawler.DownloadSpells(@"\Data\", "", "racial-traits/");
            try
            {
                Crawler.DownloadSpells(@"\Data\", "Druid");
            }
            catch (Exception e)
            {
                log.Info(e);
            }

            try
            {
                Crawler.DownloadSpells(@"\Data\", "Warlock");
            }
            catch (Exception e)
            {
                log.Info(e);
            }

            try
            {
                Crawler.DownloadSpells(@"\Data\", "Warrior");
            }
            catch (Exception e)
            {
                log.Info(e);
            }

            try
            {
                Crawler.DownloadSpells(@"\Data\", "Rogue");
            }
            catch (Exception e)
            {
                log.Info(e);
            }

            try
            {
                Crawler.DownloadSpells(@"\Data\", "Priest");
            }
            catch (Exception e)
            {
                log.Info(e);
            }

            try
            {
                Crawler.DownloadSpells(@"\Data\", "Paladin");
            }
            catch (Exception e)
            {
                log.Info(e);
            }

            try
            {
                Crawler.DownloadSpells(@"\Data\", "Mage");
            }
            catch (Exception e)
            {
                log.Info(e);
            }

            try
            {
                Crawler.DownloadSpells(@"\Data\", "Hunter");
            }
            catch (Exception e)
            {
                log.Info(e);
            }

            try
            {
                Crawler.DownloadSpells(@"\Data\", "Shaman");
            }
            catch (Exception e)
            {
                log.Info(e);
            }

            Task.WaitAll(ItemCrawler.browser.CloseAsync());
            Console.ReadKey();
        }

        private static void DownloadTalents()
        {
            //////////////////////////////////////////////////////////////////////////////
            // Warlock Talents
            int[] ids = {18288, 18223, 18265, 30108, 18220, 18708, 18788, 19028, 30146, 17877, 17962, 30283};
            Crawler.DownloadSpellById(@"\Data\", "Warlock", ids);

            //////////////////////////////////////////////////////////////////////////////
            Crawler.DownloadTalents(@"\Data\WarlockTalents.cs", "Warlock");
            Crawler.DownloadTalents(@"\Data\DruidTalents.cs", "Druid");
            Crawler.DownloadTalents(@"\Data\WarriorTalents.cs", "Warrior");
            Crawler.DownloadTalents(@"\Data\RogueTalents.cs", "Rogue");
            Crawler.DownloadTalents(@"\Data\PriestTalents.cs", "Priest");
            Crawler.DownloadTalents(@"\Data\PaladinTalents.cs", "Paladin");
            Crawler.DownloadTalents(@"\Data\MageTalents.cs", "Mage");
            Crawler.DownloadTalents(@"\Data\HunterTalents.cs", "Hunter");
            Crawler.DownloadTalents(@"\Data\ShamanTalents.cs", "Shaman");

            Task.WaitAll(ItemCrawler.browser.CloseAsync());
            Console.ReadKey();
        }

        private static void DownloadItems()
        {
            ////////////////////////////////////////////////////////////////////////////////

            // int[] num = {5,8,10,1,7,3,6,9};
            int[] num = {6};
            string[] t = {"plate"};
            // string[] t = {"cloth", "leather", "mail", "plate"};
            Task.WaitAll(ItemCrawler.Initialize());
            foreach (var i in num)
            foreach (var j in t)
                ItemCrawler.DownloadHtml(@"https://tbc.wowhead.com/items/armor/" + j + "/slot:" + i + "#items");

            ItemCrawler.DownloadHtml(@"https://tbc.wowhead.com/items/armor/amulets/#items");
            ItemCrawler.DownloadHtml(@"https://tbc.wowhead.com/items/armor/rings/#items");
            ItemCrawler.DownloadHtml(@"https://tbc.wowhead.com/items/armor/trinkets/#items");
            ItemCrawler.DownloadHtml(@"https://tbc.wowhead.com/items/armor/librams/#items");
            ItemCrawler.DownloadHtml(@"https://tbc.wowhead.com/items/armor/idols/#items");
            ItemCrawler.DownloadHtml(@"https://tbc.wowhead.com/items/armor/totems/#items");
            ItemCrawler.DownloadHtml(@"https://tbc.wowhead.com/items/armor/cloaks/#items");
            ItemCrawler.DownloadHtml(@"https://tbc.wowhead.com/items/armor/shields/#items");
            ItemCrawler.DownloadHtml(@"https://tbc.wowhead.com/items/armor/off-hand-frills/#items");


            ItemCrawler.DownloadHtml(@"https://tbc.wowhead.com/items/weapons/daggers/#items");
            ItemCrawler.DownloadHtml(@"https://tbc.wowhead.com/items/weapons/fist-weapons/#items");
            ItemCrawler.DownloadHtml(@"https://tbc.wowhead.com/items/weapons/one-handed-axes/#items");
            ItemCrawler.DownloadHtml(@"https://tbc.wowhead.com/items/weapons/one-handed-maces/#items");
            ItemCrawler.DownloadHtml(@"https://tbc.wowhead.com/items/weapons/one-handed-swords/#items");
            ItemCrawler.DownloadHtml(@"https://tbc.wowhead.com/items/weapons/polearms/#items");
            ItemCrawler.DownloadHtml(@"https://tbc.wowhead.com/items/weapons/staves/#items");
            ItemCrawler.DownloadHtml(@"https://tbc.wowhead.com/items/weapons/two-handed-axes/#items");
            ItemCrawler.DownloadHtml(@"https://tbc.wowhead.com/items/weapons/two-handed-maces/#items");
            ItemCrawler.DownloadHtml(@"https://tbc.wowhead.com/items/weapons/two-handed-swords/#items");
            ItemCrawler.DownloadHtml(@"https://tbc.wowhead.com/items/weapons/bows/#items");
            ItemCrawler.DownloadHtml(@"https://tbc.wowhead.com/items/weapons/crossbows/#items");
            ItemCrawler.DownloadHtml(@"https://tbc.wowhead.com/items/weapons/guns/#items");
            ItemCrawler.DownloadHtml(@"https://tbc.wowhead.com/items/weapons/wands/#items");
            ItemCrawler.DownloadHtml(@"https://tbc.wowhead.com/items/weapons/thrown/#items");
            ItemCrawler.DownloadHtml(@"https://tbc.wowhead.com/items/weapons/fishing-poles/#items");

            Task.WaitAll(ItemCrawler.browser.CloseAsync());
        }
    }
}