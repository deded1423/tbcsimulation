﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text.RegularExpressions;
using HtmlAgilityPack;
using TBCsimulation.Data.Utils;

namespace WowHeadCrawler
{
    [Serializable]
    public class Stat
    {
        public EnumSim.StatEnum Key = EnumSim.StatEnum.None;
        public int Value = 0;
    }

    [Serializable]
    public class Item
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public EnumSim.Binds Binds;
        public string Binds_Raw = "";
        public List<string> ChanceOnHit = new();
        public List<EnumSim.WowClass> Classes = new();
        public string Classes_Raw = "";
        public int[] Damage = new int[2];
        public string Damage_Raw = "";

        public float DamageperSecond = -1f;
        public EnumSim.SchoolType DamageSchool;
        public float DropChance = -1;

        public string DroppedBy = "";
        public int Durability = -1;
        public List<string> Equip = new();

        public List<string> Equip_Raw = new();
        public List<string> ExtraLower = new();

        public List<string> ExtraUpper = new();

        public string File = "";

        public int Id = -1;
        public string Icon = "";
        public int ItemLevel = -1;
        public EnumSim.ItemType ItemType;
        public string ItemType_Raw = "";
        public int LevelRequired = -1;
        public string Name = "";
        public int Quality = -1;
        public List<EnumSim.RaceType> Races = new();

        public string Races_Raw = "";
        public Dictionary<string, string> RandomStats = new();

        public string Requires = "";
        public string RequiresProfession = "";
        public string RequiresProfessionSpecialization = "";
        public int RequiresProfessionValue = -1;
        public string RequiresReputation = "";
        public string RequiresReputationValue = "";

        public string Set = "";
        public List<KeyValuePair<int, string>> SetBonuses = new();
        public List<string> SetPieces = new();
        public int SetPiecesCount = -1;
        public string SetRequires = "";
        public EnumSim.Slot Slot;
        public string Slot_Raw = "";
        public Stat SocketBonus = null;
        public string SocketBonus_Raw = "";
        public Dictionary<EnumSim.Socket, int> Sockets = new();

        public List<string> Sockets_Raw = new();
        public float Speed = -1f;
        public string Speed_Raw = "";
        public List<Stat> Stats = new();

        public List<string> Stats_Raw = new();
        public bool Unique = false;
        public List<string> Use = new();

        public int VendorPrice = -1;

        public Item()
        {
        }

        public Item(HtmlNode node, HtmlDocument htmlDocument)
        {
            var upperTable = node.ChildNodes[0].ChildNodes["tbody"].ChildNodes["tr"].ChildNodes["td"];
            var lowerTable = node.ChildNodes[1].ChildNodes["tbody"].ChildNodes["tr"].ChildNodes["td"];

            HtmlNode nameNode;
            if (upperTable.ChildNodes[0].Name.Equals("table"))
                nameNode = upperTable.ChildNodes[0].ChildNodes["tbody"].ChildNodes["tr"].ChildNodes["td"]
                    .ChildNodes["b"];
            else
                nameNode = upperTable.ChildNodes["b"];

            Name = nameNode.InnerText;
            log.Info("Name : " + Name);
            if (Name.Equals("Astral Guard") ||
                Name.Equals("Vermilion Necklace") ||
                Name.Equals("Ragnaros Core") ||
                Name.Equals("Prismatic Signet") ||
                Name.Equals("Vermilion Band") ||
                Name.Equals("Vermilion Band") ||
                Name.Equals("Viridian Band") ||
                Name.Equals("Ivory Band"))
            {
                Name = "";
                return;
            }

            Quality = ToQuality(nameNode.Attributes["class"].Value);
            log.Info("Quality : " + Quality);

            var skipFirst = true;
            var slotDone = false;
            foreach (var childNode in upperTable.ChildNodes)
            {
                if (skipFirst)
                {
                    if (childNode.GetType() != typeof(HtmlCommentNode)) skipFirst = false;

                    continue;
                }

                if (childNode.InnerText.Contains("Item Level"))
                {
                    ItemLevel = int.Parse(childNode.ChildNodes[3].InnerText);
                    continue;
                }

                if (childNode.InnerText.Contains("Binds"))
                {
                    Binds_Raw = childNode.InnerText;
                    continue;
                }

                if (childNode.InnerText.Contains("Unique"))
                {
                    if (childNode.InnerText.Equals("Unique") || childNode.InnerText.Equals("Unique-Equipped"))
                        Unique = true;
                    else
                        log.Warn(childNode.InnerText + " in " + Name);

                    continue;
                }

                if (childNode.Name.Equals("table") && childNode.Attributes["width"] != null &&
                    childNode.Attributes["width"].Value.Equals("100%"))
                {
                    if (!slotDone)
                    {
                        slotDone = true;
                        Slot_Raw = childNode.ChildNodes["tbody"].ChildNodes["tr"].ChildNodes["td"].InnerText;
                        if (childNode.ChildNodes["tbody"].ChildNodes["tr"].ChildNodes["th"] != null)
                            ItemType_Raw = childNode.ChildNodes["tbody"].ChildNodes["tr"].ChildNodes["th"].InnerText;

                        continue;
                    }
                    else
                    {
                        Damage_Raw = childNode.ChildNodes["tbody"].ChildNodes["tr"].ChildNodes["td"].InnerText;
                        if (childNode.ChildNodes["tbody"].ChildNodes["tr"].ChildNodes["th"] != null)
                            Speed_Raw = childNode.ChildNodes["tbody"].ChildNodes["tr"].ChildNodes["th"].InnerText;

                        continue;
                    }
                }

                if (childNode.InnerText.Contains("Socket Bonus"))
                {
                    SocketBonus_Raw = childNode.InnerText;
                    continue;
                }

                if (childNode.InnerText.Contains("Socket"))
                {
                    Sockets_Raw.Add(childNode.InnerText);
                    continue;
                }

                if (childNode.InnerText.Contains("Classes:"))
                {
                    Classes_Raw = childNode.InnerText.Replace("Classes: ", "");
                    continue;
                }

                if (childNode.InnerText.Contains("Races:"))
                {
                    Races_Raw = childNode.InnerText.Replace("Races: ", "");
                    continue;
                }

                if (childNode.InnerText.Contains("Durability"))
                {
                    Durability = int.Parse(childNode.InnerText.Split(' ')[1]);
                    continue;
                }

                if (childNode.InnerText.Contains("damage per second"))
                {
                    DamageperSecond = float.Parse(childNode.InnerText.Split(' ')[0].Replace("(", ""));
                    continue;
                }

                if (childNode.InnerText.Contains("+") ||
                    childNode.InnerText.Contains("-") ||
                    childNode.InnerText.Contains("Armor") ||
                    childNode.InnerText.Contains("Block"))
                {
                    Stats_Raw.Add(childNode.InnerText);
                    continue;
                }

                if (childNode.InnerText.Contains(@"&lt;Random enchantment&gt;"))
                {
                    HtmlNodeCollection htmlNode = null;
                    var found = false;
                    for (var i = 0; i < 30; i++)
                    {
                        htmlNode = htmlDocument.DocumentNode.SelectNodes(
                            @"/html/body/div[4]/div/div[1]/div[2]/div[4]/div[2]/div[" + i + "]/ul");
                        if (htmlNode != null)
                        {
                            ParseRandomStats(htmlNode);
                            found = true;
                        }
                    }

                    if (found)
                        continue;

                    htmlNode = htmlDocument.DocumentNode.SelectNodes(
                        @"/html/body/div[4]/div/div[1]/div[2]/div[4]/div[2]/ul[2]");
                    if (htmlNode != null)
                    {
                        ParseRandomStats(htmlNode);
                        continue;
                    }

                    htmlNode = htmlDocument.DocumentNode.SelectNodes(
                        @"/html/body/div[4]/div/div[1]/div[2]/div[4]/div[2]/ul");
                    if (htmlNode != null)
                    {
                        ParseRandomStats(htmlNode);
                        continue;
                    }


                    if (RandomStats.Count == 0)
                    {
                    }

                    continue;
                }

                if (childNode.InnerText.Equals("Quest Item") ||
                    childNode.InnerText.Contains("Duration: ") ||
                    childNode.InnerText.Equals("This Item Begins a Quest") ||
                    childNode.InnerText.Equals("Tempest Keep") ||
                    childNode.InnerText.Equals("Stratholme") ||
                    childNode.InnerText.Equals("Conjured Item"))
                {
                    ExtraUpper.Add(childNode.InnerText);
                    continue;
                }

                if (!childNode.InnerText.Equals("")) log.Fatal("Extra : " + childNode.InnerText);
            }

            log.Info("ItemLevel : " + ItemLevel);
            log.Info("Binds : " + Binds_Raw);
            log.Info("Unique : " + Unique);
            log.Info("Slot : " + Slot_Raw);
            log.Info("ItemType : " + ItemType_Raw);
            log.Info("Damage : " + Damage_Raw);
            log.Info("Speed : " + Speed_Raw);
            log.Info("DamageperSecond : " + DamageperSecond);
            log.Info("Stats : " + Stats_Raw.Count);
            log.Info("Sockets : " + Sockets_Raw.Count);
            log.Info("SocketBonus : " + SocketBonus_Raw);
            log.Info("Classes : " + Classes_Raw);
            log.Info("Races : " + Races_Raw);
            log.Info("Durability : " + Durability);
            log.Info("ExtraUpper : " + ExtraUpper.Count);

            var setRegex = @"(?<name>[\w ]+) \((?<min>\d+)\/(?<max>\d+)\)";
            var setBonusRegex = @"\((?<number>\d)\)( Set :) (?<description>.+)";
            skipFirst = false;
            HtmlNode skipUntil = null;
            foreach (var childNode in lowerTable.ChildNodes)
            {
                if (childNode.GetType() == typeof(HtmlCommentNode))
                    continue;


                if (skipFirst)
                {
                    if (childNode.Name.Equals("br") || childNode.NextSibling.GetType() == typeof(HtmlCommentNode))
                        skipFirst = false;
                    continue;
                }

                if (childNode.Name.Equals("br"))
                    continue;

                if (skipUntil != null)
                {
                    if (childNode.Equals(skipUntil))
                        skipUntil = null;
                    continue;
                }

                if (childNode.InnerText.Contains("Requires Level"))
                {
                    LevelRequired = int.Parse(childNode.NextSibling.NextSibling.InnerText);
                    skipFirst = true;
                    continue;
                }

                if (childNode.Attributes["class"] != null &&
                    childNode.Attributes["class"].Value.Equals("whtt-sellprice"))
                {
                    var money = 0;
                    foreach (var moneyNode in childNode.SelectNodes(@"span"))
                        switch (moneyNode.Attributes["class"].Value)
                        {
                            case "moneygold":
                                money += int.Parse(moneyNode.InnerText) * 100 * 100;
                                break;
                            case "moneysilver":
                                money += int.Parse(moneyNode.InnerText) * 100;
                                break;
                            case "moneycopper":
                                money += int.Parse(moneyNode.InnerText);
                                break;
                            default:
                                throw new NotSupportedException();
                        }

                    VendorPrice = money;
                    continue;
                }

                if (childNode.InnerText.StartsWith("Requires"))
                {
                    if (childNode.NextSibling.NextSibling.Name.Equals("br"))
                        Requires = childNode.NextSibling.InnerText;
                    else
                        Requires = childNode.NextSibling.InnerText + " " + childNode.NextSibling.NextSibling.InnerText;

                    skipFirst = true;
                    continue;
                }

                if (childNode.InnerText.StartsWith("Equip:"))
                {
                    Equip_Raw.Add(childNode.InnerText);
                    continue;
                }

                if (childNode.InnerText.StartsWith("Use:"))
                {
                    Use.Add(childNode.InnerText);
                    continue;
                }

                if (childNode.InnerText.StartsWith("Chance on hit:"))
                {
                    ChanceOnHit.Add(childNode.InnerText);
                    continue;
                }

                if (childNode.InnerText.StartsWith("Dropped by: "))
                {
                    DroppedBy = childNode.InnerText.Replace("Dropped by: ", "");
                    continue;
                }

                if (childNode.InnerText.StartsWith("Drop Chance: "))
                {
                    DropChance = float.Parse(childNode.InnerText.Replace("Drop Chance: ", "").Replace("%", ""));
                    continue;
                }

                if (childNode.Attributes["class"] != null && childNode.Attributes["class"].Value.Equals("q") &&
                    Regex.IsMatch(childNode.InnerText, setRegex))
                {
                    var match = Regex.Match(childNode.InnerText, setRegex);
                    Set = match.Groups["name"].Value;
                    SetPiecesCount = int.Parse(match.Groups["max"].Value);
                    var currentNode = childNode;

                    if (currentNode.NextSibling.Name.Equals("br"))
                        while (!currentNode.NextSibling.Name.Equals("div"))
                        {
                            if (!currentNode.InnerText.Contains("Requires ")) SetRequires += currentNode.InnerText;

                            currentNode = currentNode.NextSibling;
                        }

                    foreach (var setNode in currentNode.NextSibling.ChildNodes)
                        if (setNode.GetType() != typeof(HtmlCommentNode) && setNode.Name.Equals("span"))
                            SetPieces.Add(setNode.InnerText);

                    var setBonusTable = currentNode.NextSibling.NextSibling.NextSibling;
                    foreach (var setBonusNode in setBonusTable.ChildNodes)
                        if (Regex.IsMatch(setBonusNode.InnerText, setBonusRegex))
                        {
                            match = Regex.Match(setBonusNode.InnerText, setBonusRegex);
                            SetBonuses.Add(new KeyValuePair<int, string>(int.Parse(match.Groups["number"].Value),
                                match.Groups["description"].Value));
                        }

                    skipFirst = true;
                    skipUntil = setBonusTable;
                    continue;
                }


                if (childNode.InnerText.Contains("Charge") ||
                    childNode.Attributes["class"] != null && childNode.Attributes["class"].Value.Equals("q") &&
                    childNode.InnerText.Contains("\"") ||
                    childNode.InnerText.Contains("Max Stack:") ||
                    childNode.InnerText.Contains("&lt;Right Click to Read&gt;"))
                {
                    ExtraLower.Add(childNode.InnerText);
                    continue;
                }

                if (!childNode.InnerText.Equals(""))
                {
                    log.Fatal("Extra : " + childNode.InnerText);
                    continue;
                }

                log.Fatal("Extra : " + childNode.InnerText);
            }

            log.Info("LevelRequired : " + LevelRequired);
            log.Info("Requires : " + Requires);
            log.Info("Equip : " + Equip_Raw.Count);
            log.Info("Use : " + Use.Count);
            log.Info("ChanceOnHit : " + ChanceOnHit.Count);
            log.Info("Set : " + Set);
            log.Info("SetRequires : " + SetRequires);
            log.Info("SetPiecesCount : " + SetPiecesCount);
            log.Info("SetBonuses : " + SetBonuses.Count);
            log.Info("VendorPrice : " + VendorPrice);
            log.Info("DroppedBy : " + DroppedBy);
            log.Info("DropChance : " + DropChance);
            log.Info("ExtraLower : " + ExtraLower.Count);


            HtmlNodeCollection iconNode = null;
            for (var i = 10 - 1; i >= 0; i--)
            {
                var xpath =
                    @"/html/body/div[4]/div/div[1]/div[2]/div[4]/div[2]/div[14]/table/tbody/tr/td[1]/table/tbody/tr[2]/td/div[2]/ul/li[" +
                    i + "]/div";
                iconNode = htmlDocument.DocumentNode.SelectNodes(xpath);
                if (iconNode != null && iconNode[0].InnerText.Contains("Icon")) break;

                xpath =
                    @"/html/body/div[4]/div/div[1]/div[2]/div[4]/div[2]/div[15]/table/tbody/tr/td[1]/table/tbody/tr[2]/td/div[2]/ul/li[" +
                    i + "]/div";
                iconNode = htmlDocument.DocumentNode.SelectNodes(xpath);
                if (iconNode != null && iconNode[0].InnerText.Contains("Icon")) break;

                xpath =
                    @"/html/body/div[4]/div/div[1]/div[2]/div[4]/div[2]/div[16]/table/tbody/tr/td[1]/table/tbody/tr[2]/td/div[2]/ul/li[" +
                    i + "]/div";
                iconNode = htmlDocument.DocumentNode.SelectNodes(xpath);
                if (iconNode != null && iconNode[0].InnerText.Contains("Icon")) break;

                xpath =
                    @"/html/body/div[4]/div[1]/div/div[2]/div[5]/div[14]/table/tbody/tr/td[1]/table/tbody/tr[2]/td/div[2]/ul/li[" +
                    i + "]/div";
                iconNode = htmlDocument.DocumentNode.SelectNodes(xpath);
                if (iconNode != null && iconNode[0].InnerText.Contains("Icon")) break;
            }

            if (Name.Equals("Red Pointy Hat"))
                Icon = "";
            else
                Icon = iconNode[0].ChildNodes["a"].InnerText;


            log.Info("Icon : " + Icon);
        }

        public void Convert()
        {
            try
            {
                ConvertBinds();
                ConvertSlot();
                ConvertItemType();
                ConvertDamage();
                ConvertStats();
                ConvertSockets();
                ConvertRaces();
                ConvertClasses();
                ConvertRequires();
            }
            catch (Exception e)
            {
                log.Fatal(e);
                throw;
            }
        }

        private void ConvertBinds()
        {
            switch (Binds_Raw)
            {
                case "":
                    Binds = EnumSim.Binds.None;
                    break;
                case "Binds when equipped":
                    Binds = EnumSim.Binds.OnEquip;
                    break;
                case "Binds when picked up":
                    Binds = EnumSim.Binds.OnPickup;
                    break;
                case "Binds when used":
                    Binds = EnumSim.Binds.OnUse;
                    break;
                default:
                    throw new ArgumentException();
            }
        }

        private void ConvertSlot()
        {
            switch (Slot_Raw)
            {
                case "":
                    Slot = EnumSim.Slot.None;
                    break;
                case "Waist":
                    Slot = EnumSim.Slot.Waist;
                    break;
                case "Wrist":
                    Slot = EnumSim.Slot.Wrist;
                    break;
                case "Head":
                    Slot = EnumSim.Slot.Head;
                    break;
                case "Chest":
                    Slot = EnumSim.Slot.Chest;
                    break;
                case "Feet":
                    Slot = EnumSim.Slot.Feet;
                    break;
                case "Hands":
                    Slot = EnumSim.Slot.Hands;
                    break;
                case "Neck":
                    Slot = EnumSim.Slot.Neck;
                    break;
                case "Shoulder":
                    Slot = EnumSim.Slot.Shoulder;
                    break;
                case "Legs":
                    Slot = EnumSim.Slot.Legs;
                    break;
                case "Relic":
                    Slot = EnumSim.Slot.Relic;
                    break;
                case "Held In Off-hand":
                    Slot = EnumSim.Slot.OffHand;
                    break;
                case "Off Hand":
                    Slot = EnumSim.Slot.OffHand;
                    break;
                case "Trinket":
                    Slot = EnumSim.Slot.Trinket;
                    break;
                case "Back":
                    Slot = EnumSim.Slot.Back;
                    break;
                case "Finger":
                    Slot = EnumSim.Slot.Finger;
                    break;
                case "Thrown":
                    Slot = EnumSim.Slot.Thrown;
                    break;
                case "Ranged":
                    Slot = EnumSim.Slot.Ranged;
                    break;
                case "Two-Hand":
                    Slot = EnumSim.Slot.TwoHand;
                    break;
                case "One-Hand":
                    Slot = EnumSim.Slot.OneHand;
                    break;
                case "Main Hand":
                    Slot = EnumSim.Slot.MainHand;
                    break;
                default:
                    throw new ArgumentException();
            }
        }

        private void ConvertItemType()
        {
            switch (ItemType_Raw)
            {
                case "":
                    ItemType = EnumSim.ItemType.None;
                    break;
                case "Cloth":
                    ItemType = EnumSim.ItemType.Cloth;
                    break;
                case "Leather":
                    ItemType = EnumSim.ItemType.Leather;
                    break;
                case "Mail":
                    ItemType = EnumSim.ItemType.Mail;
                    break;
                case "Plate":
                    ItemType = EnumSim.ItemType.Plate;
                    break;
                case "Shield":
                    ItemType = EnumSim.ItemType.Shield;
                    break;
                case "Totem":
                    ItemType = EnumSim.ItemType.Totem;
                    break;
                case "Idol":
                    ItemType = EnumSim.ItemType.Idol;
                    break;
                case "Libram":
                    ItemType = EnumSim.ItemType.Libram;
                    break;
                case "Thrown":
                    ItemType = EnumSim.ItemType.Thrown;
                    break;
                case "Crossbow":
                    ItemType = EnumSim.ItemType.Crossbow;
                    break;
                case "Axe":
                    ItemType = EnumSim.ItemType.Axe;
                    break;
                case "Mace":
                    ItemType = EnumSim.ItemType.Mace;
                    break;
                case "Sword":
                    ItemType = EnumSim.ItemType.Sword;
                    break;
                case "Bow":
                    ItemType = EnumSim.ItemType.Bow;
                    break;
                case "Polearm":
                    ItemType = EnumSim.ItemType.Polearm;
                    break;
                case "Staff":
                    ItemType = EnumSim.ItemType.Staff;
                    break;
                case "Fishing Pole":
                    ItemType = EnumSim.ItemType.FishingPole;
                    break;
                case "Wand":
                    ItemType = EnumSim.ItemType.Wand;
                    break;
                case "Fist Weapon":
                    ItemType = EnumSim.ItemType.FistWeapon;
                    break;
                case "Gun":
                    ItemType = EnumSim.ItemType.Gun;
                    break;
                case "Dagger":
                    ItemType = EnumSim.ItemType.Dagger;
                    break;
                default:
                    throw new ArgumentException();
            }
        }

        private void ConvertDamage()
        {
            if (!Damage_Raw.Equals(""))
            {
                var split = Damage_Raw.Split(' ');
                Damage[0] = int.Parse(split[0]);
                if (split.Length > 3)
                    Damage[1] = int.Parse(split[2]);
                else if (split.Length == 2) Damage[1] = int.Parse(split[0]);

                DamageSchool = EnumSim.SchoolType.Physical;
                if (Damage_Raw.Contains("Arcane")) DamageSchool = EnumSim.SchoolType.Arcane;
                if (Damage_Raw.Contains("Fire")) DamageSchool = EnumSim.SchoolType.Fire;

                if (Damage_Raw.Contains("Frost")) DamageSchool = EnumSim.SchoolType.Frost;
                if (Damage_Raw.Contains("Holy")) DamageSchool = EnumSim.SchoolType.Holy;
                if (Damage_Raw.Contains("Nature")) DamageSchool = EnumSim.SchoolType.Nature;
                if (Damage_Raw.Contains("Shadow")) DamageSchool = EnumSim.SchoolType.Shadow;

                Speed = float.Parse(Speed_Raw.Replace("Speed ", ""));
            }
        }

        private void ConvertStats()
        {
            foreach (var str in Stats_Raw) Stats.Add(ConvertStat(str));

            foreach (var str in Equip_Raw)
            {
                var stat = ConvertEquip(str.Replace("Equip: ", ""));
                if (stat != null) Stats.Add(stat);
            }
        }

        private void ConvertSockets()
        {
            foreach (var socket in Sockets_Raw)
                if (socket.Contains("Red"))
                {
                    if (Sockets.ContainsKey(EnumSim.Socket.Red))
                        Sockets[EnumSim.Socket.Red] += 1;
                    else
                        Sockets.Add(EnumSim.Socket.Red, 1);
                }
                else if (socket.Contains("Blue"))
                {
                    if (Sockets.ContainsKey(EnumSim.Socket.Blue))
                        Sockets[EnumSim.Socket.Blue] += 1;
                    else
                        Sockets.Add(EnumSim.Socket.Blue, 1);
                }
                else if (socket.Contains("Yellow"))
                {
                    if (Sockets.ContainsKey(EnumSim.Socket.Yellow))
                        Sockets[EnumSim.Socket.Yellow] += 1;
                    else
                        Sockets.Add(EnumSim.Socket.Yellow, 1);
                }
                else if (socket.Contains("Meta"))
                {
                    if (Sockets.ContainsKey(EnumSim.Socket.Meta))
                        Sockets[EnumSim.Socket.Meta] += 1;
                    else
                        Sockets.Add(EnumSim.Socket.Meta, 1);
                }
                else if (socket.Contains("Prismatic"))
                {
                    if (Sockets.ContainsKey(EnumSim.Socket.Prismatic))
                        Sockets[EnumSim.Socket.Prismatic] += 1;
                    else
                        Sockets.Add(EnumSim.Socket.Prismatic, 1);
                }
                else
                {
                }

            if (!SocketBonus_Raw.Equals("")) SocketBonus = ConvertStat(SocketBonus_Raw.Replace("Socket Bonus: ", ""));
        }

        private void ConvertRaces()
        {
            var split = Races_Raw.Replace(" ", "").Split(',');
            foreach (var s in split)
                switch (s)
                {
                    case "":
                        break;
                    case "Orc":
                        Races.Add(EnumSim.RaceType.Orc);
                        break;
                    case "Undead":
                        Races.Add(EnumSim.RaceType.Undead);
                        break;
                    case "Tauren":
                        Races.Add(EnumSim.RaceType.Tauren);
                        break;
                    case "Troll":
                        Races.Add(EnumSim.RaceType.Troll);
                        break;
                    case "BloodElf":
                        Races.Add(EnumSim.RaceType.BloodElf);
                        break;
                    case "Human":
                        Races.Add(EnumSim.RaceType.Human);
                        break;
                    case "Gnome":
                        Races.Add(EnumSim.RaceType.Gnome);
                        break;
                    case "Dwarf":
                        Races.Add(EnumSim.RaceType.Dwarf);
                        break;
                    case "NightElf":
                        Races.Add(EnumSim.RaceType.NightElf);
                        break;
                    case "Draenei":
                        Races.Add(EnumSim.RaceType.Draenei);
                        break;
                    default:
                        throw new ArgumentException();
                }
        }

        private void ConvertClasses()
        {
            var split = Classes_Raw.Replace(" ", "").Split(',');
            foreach (var s in split)
                switch (s)
                {
                    case "":
                        break;
                    case "Mage":
                        Classes.Add(EnumSim.WowClass.Mage);
                        break;
                    case "Warlock":
                        Classes.Add(EnumSim.WowClass.Warlock);
                        break;
                    case "Druid":
                        Classes.Add(EnumSim.WowClass.Druid);
                        break;
                    case "Warrior":
                        Classes.Add(EnumSim.WowClass.Warrior);
                        break;
                    case "Paladin":
                        Classes.Add(EnumSim.WowClass.Paladin);
                        break;
                    case "Shaman":
                        Classes.Add(EnumSim.WowClass.Shaman);
                        break;
                    case "Priest":
                        Classes.Add(EnumSim.WowClass.Priest);
                        break;
                    case "Rogue":
                        Classes.Add(EnumSim.WowClass.Rogue);
                        break;
                    case "Hunter":
                        Classes.Add(EnumSim.WowClass.Hunter);
                        break;

                    default:
                        throw new ArgumentException();
                }
        }

        private void ConvertRequires()
        {
            if (!Requires.Equals(""))
            {
                if (Regex.IsMatch(Requires, @"([\w\s]*)-([\w\s]*)"))
                {
                    var match = Regex.Match(Requires, @"(?<Faction>[\w\s]*)-(?<Level>[\w\s]*)").Groups;
                    RequiresReputation = match["Faction"].Value;
                    RequiresReputationValue = match["Level"].Value;
                }
                else if (Requires.Contains("Tailoring"))
                {
                    RequiresProfession = "Tailoring";
                    if (Regex.IsMatch(Requires, @"\((?<Number>\d*)\)"))
                    {
                        RequiresProfessionValue =
                            int.Parse(Regex.Match(Requires, @"\((?<Number>\d*)\)").Groups["Number"].Value);
                    }
                    else if (Requires.Contains("Shadoweave"))
                    {
                        RequiresProfessionSpecialization = "Shadoweave";
                    }
                    else if (Requires.Contains("Mooncloth"))
                    {
                        RequiresProfessionSpecialization = "Mooncloth";
                    }
                    else if (Requires.Contains("Spellfire"))
                    {
                        RequiresProfessionSpecialization = "Spellfire";
                    }
                    else
                    {
                    }
                }
                else if (Requires.Contains("Alchemy"))
                {
                    RequiresProfession = "Alchemy";

                    if (Regex.IsMatch(Requires, @"\((?<Number>\d*)\)"))
                    {
                        RequiresProfessionValue =
                            int.Parse(Regex.Match(Requires, @"\((?<Number>\d*)\)").Groups["Number"].Value);
                    }
                    else
                    {
                    }
                }
                else if (Requires.Contains("Armorsmith") ||
                         Requires.Contains("Blacksmithing") ||
                         Requires.Contains("Master Axesmith") ||
                         Requires.Contains("Master Hammersmith") ||
                         Requires.Contains("Master Swordsmith") ||
                         Requires.Contains("Weaponsmith"))
                {
                    RequiresProfession = "Blacksmithing";

                    if (Regex.IsMatch(Requires, @"\((?<Number>\d*)\)"))
                    {
                        RequiresProfessionValue =
                            int.Parse(Regex.Match(Requires, @"\((?<Number>\d*)\)").Groups["Number"].Value);
                    }
                    else if (Requires.Contains("Armorsmith"))
                    {
                        RequiresProfessionSpecialization = "Armorsmith";
                    }
                    else if (Requires.Contains("Master Axesmith"))
                    {
                        RequiresProfessionSpecialization = "Master Axesmith";
                    }
                    else if (Requires.Contains("Master Hammersmith"))
                    {
                        RequiresProfessionSpecialization = "Master Hammersmith";
                    }
                    else if (Requires.Contains("Master Swordsmith"))
                    {
                        RequiresProfessionSpecialization = "Master Swordsmith";
                    }
                    else if (Requires.Contains("Weaponsmith"))
                    {
                        RequiresProfessionSpecialization = "Weaponsmith";
                    }
                    else
                    {
                    }
                }
                else if (Requires.Contains("Fishing"))
                {
                    RequiresProfession = "Fishing";

                    if (Regex.IsMatch(Requires, @"\((?<Number>\d*)\)"))
                    {
                        RequiresProfessionValue =
                            int.Parse(Regex.Match(Requires, @"\((?<Number>\d*)\)").Groups["Number"].Value);
                    }
                    else
                    {
                    }
                }
                else if (Requires.Contains("Jewelcrafting"))
                {
                    RequiresProfession = "Jewelcrafting";

                    if (Regex.IsMatch(Requires, @"\((?<Number>\d*)\)"))
                    {
                        RequiresProfessionValue =
                            int.Parse(Regex.Match(Requires, @"\((?<Number>\d*)\)").Groups["Number"].Value);
                    }
                    else
                    {
                    }
                }
                else if (Requires.Contains("Mining"))
                {
                    RequiresProfession = "Mining";

                    if (Regex.IsMatch(Requires, @"\((?<Number>\d*)\)"))
                    {
                        RequiresProfessionValue =
                            int.Parse(Regex.Match(Requires, @"\((?<Number>\d*)\)").Groups["Number"].Value);
                    }
                    else
                    {
                    }
                }
                else if (Requires.Contains("Herbalism"))
                {
                    RequiresProfession = "Herbalism";

                    if (Regex.IsMatch(Requires, @"\((?<Number>\d*)\)"))
                    {
                        RequiresProfessionValue =
                            int.Parse(Regex.Match(Requires, @"\((?<Number>\d*)\)").Groups["Number"].Value);
                    }
                    else
                    {
                    }
                }
                else if (Requires.Contains("Engineering") || Requires.Contains("Engineer"))
                {
                    RequiresProfession = "Engineering";

                    if (Regex.IsMatch(Requires, @"\((?<Number>\d*)\)"))
                    {
                        RequiresProfessionValue =
                            int.Parse(Regex.Match(Requires, @"\((?<Number>\d*)\)").Groups["Number"].Value);
                    }
                    else if (Requires.Contains("Goblin"))
                    {
                        RequiresProfessionSpecialization = "Goblin";
                    }
                    else if (Requires.Contains("Gnomish"))
                    {
                        RequiresProfessionSpecialization = "Gnomish";
                    }
                    else
                    {
                    }
                }
                else if (Requires.Contains("Leatherworking"))
                {
                    RequiresProfession = "Leatherworking";

                    if (Regex.IsMatch(Requires, @"\((?<Number>\d*)\)"))
                    {
                        RequiresProfessionValue =
                            int.Parse(Regex.Match(Requires, @"\((?<Number>\d*)\)").Groups["Number"].Value);
                    }
                    else if (Requires.Contains("Dragonscale"))
                    {
                        RequiresProfessionSpecialization = "Dragonscale";
                    }
                    else if (Requires.Contains("Elemental"))
                    {
                        RequiresProfessionSpecialization = "Elemental";
                    }
                    else if (Requires.Contains("Tribal"))
                    {
                        RequiresProfessionSpecialization = "Tribal";
                    }
                    else
                    {
                    }
                }
                else if (Requires.Contains("Leatherworking"))
                {
                    RequiresProfession = "Leatherworking";

                    if (Regex.IsMatch(Requires, @"\((?<Number>\d*)\)"))
                    {
                        RequiresProfessionValue =
                            int.Parse(Regex.Match(Requires, @"\((?<Number>\d*)\)").Groups["Number"].Value);
                    }
                    else if (Requires.Contains("Dragonscale"))
                    {
                        RequiresProfessionSpecialization = "Dragonscale";
                    }
                    else
                    {
                    }
                }
                else
                {
                    throw new NotSupportedException();
                }
            }
        }

        private Stat ConvertStat(string str)
        {
            var stat = new Stat();
            try
            {
                var matchRegex = @"\((?<min>\d+) - (?<max>\d+)\)";
                var matchParse = Regex.Matches(str, matchRegex);
                foreach (Match m in matchParse)
                    if (!m.Value.Equals(""))
                        str = str.Replace(m.Value, m.Groups["max"].ToString());

                var match = Regex.Match(str, @"(?<Number>[+-]?\d+) (?<Key>.+)");
                stat.Value = int.Parse(match.Groups["Number"].Value);

                if (Regex.IsMatch(str,
                    @"(?<Number>[+-]?\d+) Healing (Spells )?(and )?(?<Number>[+-]?\d+) (Spell )?Damage( Spells)?"))
                {
                    stat.Key = EnumSim.StatEnum.HealingThirdSpellDamage;
                    return stat;
                }

                switch (match.Groups["Key"].Value)
                {
                    case "Spell Critical Strike Rating":
                        stat.Key = EnumSim.StatEnum.SpellCriticalStrikeRating;
                        break;
                    case "Spell Damage and Healing":
                        stat.Key = EnumSim.StatEnum.SpellDamageandHealing;
                        break;
                    case "Stamina":
                        stat.Key = EnumSim.StatEnum.Stamina;
                        break;
                    case "mana per 5 sec.":
                        stat.Key = EnumSim.StatEnum.Manaper5sec;
                        break;
                    case "Mana per 5 sec.":
                        stat.Key = EnumSim.StatEnum.Manaper5sec;
                        break;
                    case "mana every 5 sec.":
                        stat.Key = EnumSim.StatEnum.Manaper5sec;
                        break;
                    case "Spell Hit Rating":
                        stat.Key = EnumSim.StatEnum.SpellHitRating;
                        break;
                    case "Parry Rating":
                        stat.Key = EnumSim.StatEnum.ParryRating;
                        break;
                    case "Strength":
                        stat.Key = EnumSim.StatEnum.Strength;
                        break;
                    case "Agility":
                        stat.Key = EnumSim.StatEnum.Agility;
                        break;
                    case "Spirit":
                        stat.Key = EnumSim.StatEnum.Spirit;
                        break;
                    case "Intellect":
                        stat.Key = EnumSim.StatEnum.Intellect;
                        break;
                    case "Critical Strike Rating":
                        stat.Key = EnumSim.StatEnum.CriticalStrikeRating;
                        break;
                    case "Hit Rating":
                        stat.Key = EnumSim.StatEnum.HitRating;
                        break;
                    case "Resilience Rating":
                        stat.Key = EnumSim.StatEnum.ResilienceRating;
                        break;
                    case "Attack Power":
                        stat.Key = EnumSim.StatEnum.AttackPower;
                        break;
                    case "Defense Rating":
                        stat.Key = EnumSim.StatEnum.DefenseRating;
                        break;
                    case "Dodge Rating":
                        stat.Key = EnumSim.StatEnum.DodgeRating;
                        break;
                    case "Block Rating":
                        stat.Key = EnumSim.StatEnum.BlockRating;
                        break;
                    case "Block Value":
                        stat.Key = EnumSim.StatEnum.BlockValue;
                        break;
                    case "Spell Damage":
                        stat.Key = EnumSim.StatEnum.SpellDamage;
                        break;
                    case "Haste Rating":
                        stat.Key = EnumSim.StatEnum.HasteRating;
                        break;
                    case "Expertise Rating":
                        stat.Key = EnumSim.StatEnum.ExpertiseRating;
                        break;
                    case "Armor":
                        stat.Key = EnumSim.StatEnum.Armor;
                        break;
                    case "Arcane Spell Damage":
                        stat.Key = EnumSim.StatEnum.ArcaneSpellDamage;
                        break;
                    case "Frost Spell Damage":
                        stat.Key = EnumSim.StatEnum.FrostSpellDamage;
                        break;
                    case "Fire Spell Damage":
                        stat.Key = EnumSim.StatEnum.FireSpellDamage;
                        break;
                    case "Holy Spell Damage":
                        stat.Key = EnumSim.StatEnum.HolySpellDamage;
                        break;
                    case "Nature Spell Damage":
                        stat.Key = EnumSim.StatEnum.NatureSpellDamage;
                        break;
                    case "Shadow Spell Damage":
                        stat.Key = EnumSim.StatEnum.ShadowSpellDamage;
                        break;
                    case "Arcane Resistance":
                        stat.Key = EnumSim.StatEnum.ArcaneResistance;
                        break;
                    case "Frost Resistance":
                        stat.Key = EnumSim.StatEnum.FrostResistance;
                        break;
                    case "Fire Resistance":
                        stat.Key = EnumSim.StatEnum.FireResistance;
                        break;
                    case "Nature Resistance":
                        stat.Key = EnumSim.StatEnum.NatureResistance;
                        break;
                    case "Shadow Resistance":
                        stat.Key = EnumSim.StatEnum.ShadowResistance;
                        break;
                    case "Resist Shadow":
                        stat.Key = EnumSim.StatEnum.ShadowResistance;
                        break;
                    case "Block":
                        stat.Key = EnumSim.StatEnum.Block;
                        break;
                    case "Damage and Healing Spells":
                        stat.Key = EnumSim.StatEnum.SpellDamageandHealing;
                        break;
                    case "health every 5 sec.":
                        stat.Key = EnumSim.StatEnum.Healthper5sec;
                        break;
                    case "Ranged Attack Power":
                        stat.Key = EnumSim.StatEnum.RangedAttackPower;
                        break;

                    default:
                        log.Fatal(match.Groups["Key"].Value);
                        throw new NotSupportedException();
                }
            }
            catch (Exception e)
            {
                log.Error(e);
            }

            return stat;
        }

        private Stat ConvertEquip(string str)
        {
            var stat = new Stat();

            var regexCritRating = @"Improves critical strike rating by (?<Number>-?\d+).";
            var regexCritRating2 = @"Increases critical strike rating by (?<Number>-?\d+).";
            var regexDamageHealing =
                @"Increases damage and healing done by magical spells and effects by up to (?<Number>\d+).";
            var regexHitRating = @"Improves hit rating by (?<Number>-?\d+).";
            var regexHitRating2 = @"Increases your hit rating by (?<Number>-?\d+).";
            var regexSpellHitRating = @"Improves spell hit rating by (?<Number>\d+).";
            var regexSpellCritRating = @"Improves spell critical strike rating by (?<Number>-?\d+).";
            var regexArcaneSpellDamage = @"Increases damage done by Arcane spells and effects by up to (?<Number>\d+).";
            var regexFireSpellDamage = @"Increases damage done by Fire spells and effects by up to (?<Number>\d+).";
            var regexFrostSpellDamage = @"Increases damage done by Frost spells and effects by up to (?<Number>\d+).";
            var regexNatureSpellDamage = @"Increases damage done by Nature spells and effects by up to (?<Number>\d+).";
            var regexShadowSpellDamage = @"Increases damage done by Shadow spells and effects by up to (?<Number>\d+).";
            var regexSpellHasteRating = @"Improves spell haste rating by (?<Number>-?\d+).";
            var regexAttackPower = @"Increases attack power by (?<Number>\d+).";
            var regexManaPer5Sec = @"Restores (?<Number>\d+) mana per 5 sec.";
            var regexDefenseRating = @"Increases defense rating by (?<Number>-?\d+).";
            var regexDefense = @"Increases Defense by (?<Number>-?\d+).";
            var regexDodgeRating = @"Increases( your)? dodge rating by (?<Number>-?\d+).";
            var regexBlockValue = @"Increases the block value of your shield by (?<Number>\d+).";
            var regexParryRating = @"Increases your parry rating by (?<Number>\d+).";
            var regexResilienceRating = @"Improves your resilience rating by (?<Number>\d+).";
            var regexIgnoreArmor = @"Your attacks ignore (?<Number>\d+) of your opponent's armor.";
            var regexHealingThirdSpellDamage =
                @"Increases healing done by up to (?<Number>\d+) and damage done by up to (?<NumberDamage>\d+) for all magical spells and effects.";
            var regexSpellPen = @"Increases your spell penetration by (?<Number>\d+).";
            var regexExpertiseRating = @"Increases your expertise rating by (?<Number>\d+).";
            var regexHasteRating = @"Improves haste rating( by)? (?<Number>-?\d+).";
            var regexBlockRating = @"Increases your( shield)? block rating by (?<Number>\d+).";
            var regexHealthPer5Sec = @"Restores (?<Number>\d+) health per 5 sec.";
            var regexWeaponDamage = @"\+(?<Number>\d+) Weapon Damage.";
            var regexAllResistances = @"\+(?<Number>\d+) All Resistances.";
            var regexSwordSkillRating = @"Increases sword skill rating by (?<Number>\d+).";
            var regexRangedAttackPower = @"Increases ranged attack power by (?<Number>\d+).";
            var regexFishing = @"Increased Fishing \+(?<Number>\d+).";
            var regexArmor = @"\+(?<Number>\d+) Armor.";
            var regexSpeed = @"Run speed increased slightly.";

            if (Regex.IsMatch(str, regexCritRating))
            {
                var match = Regex.Match(str, regexCritRating);
                stat.Key = EnumSim.StatEnum.CriticalStrikeRating;
                stat.Value = int.Parse(match.Groups["Number"].Value);
                return stat;
            }

            if (Regex.IsMatch(str, regexCritRating2))
            {
                var match = Regex.Match(str, regexCritRating2);
                stat.Key = EnumSim.StatEnum.CriticalStrikeRating;
                stat.Value = int.Parse(match.Groups["Number"].Value);
                return stat;
            }

            if (Regex.IsMatch(str, regexDamageHealing))
            {
                var match = Regex.Match(str, regexDamageHealing);
                stat.Key = EnumSim.StatEnum.SpellDamageandHealing;
                stat.Value = int.Parse(match.Groups["Number"].Value);
                return stat;
            }

            if (Regex.IsMatch(str, regexHitRating))
            {
                var match = Regex.Match(str, regexHitRating);
                stat.Key = EnumSim.StatEnum.HitRating;
                stat.Value = int.Parse(match.Groups["Number"].Value);
                return stat;
            }

            if (Regex.IsMatch(str, regexHitRating2))
            {
                var match = Regex.Match(str, regexHitRating2);
                stat.Key = EnumSim.StatEnum.HitRating;
                stat.Value = int.Parse(match.Groups["Number"].Value);
                return stat;
            }

            if (Regex.IsMatch(str, regexSpellHitRating))
            {
                var match = Regex.Match(str, regexSpellHitRating);
                stat.Key = EnumSim.StatEnum.SpellHitRating;
                stat.Value = int.Parse(match.Groups["Number"].Value);
                return stat;
            }

            if (Regex.IsMatch(str, regexSpellCritRating))
            {
                var match = Regex.Match(str, regexSpellCritRating);
                stat.Key = EnumSim.StatEnum.SpellCriticalStrikeRating;
                stat.Value = int.Parse(match.Groups["Number"].Value);
                return stat;
            }

            if (Regex.IsMatch(str, regexArcaneSpellDamage))
            {
                var match = Regex.Match(str, regexArcaneSpellDamage);
                stat.Key = EnumSim.StatEnum.ArcaneSpellDamage;
                stat.Value = int.Parse(match.Groups["Number"].Value);
                return stat;
            }

            if (Regex.IsMatch(str, regexFireSpellDamage))
            {
                var match = Regex.Match(str, regexFireSpellDamage);
                stat.Key = EnumSim.StatEnum.FireSpellDamage;
                stat.Value = int.Parse(match.Groups["Number"].Value);
                return stat;
            }

            if (Regex.IsMatch(str, regexFrostSpellDamage))
            {
                var match = Regex.Match(str, regexFrostSpellDamage);
                stat.Key = EnumSim.StatEnum.FrostSpellDamage;
                stat.Value = int.Parse(match.Groups["Number"].Value);
                return stat;
            }

            if (Regex.IsMatch(str, regexNatureSpellDamage))
            {
                var match = Regex.Match(str, regexNatureSpellDamage);
                stat.Key = EnumSim.StatEnum.NatureSpellDamage;
                stat.Value = int.Parse(match.Groups["Number"].Value);
                return stat;
            }

            if (Regex.IsMatch(str, regexShadowSpellDamage))
            {
                var match = Regex.Match(str, regexShadowSpellDamage);
                stat.Key = EnumSim.StatEnum.ShadowSpellDamage;
                stat.Value = int.Parse(match.Groups["Number"].Value);
                return stat;
            }

            if (Regex.IsMatch(str, regexSpellHasteRating))
            {
                var match = Regex.Match(str, regexSpellHasteRating);
                stat.Key = EnumSim.StatEnum.SpellHasteRating;
                stat.Value = int.Parse(match.Groups["Number"].Value);
                return stat;
            }

            if (Regex.IsMatch(str, regexAttackPower))
            {
                var match = Regex.Match(str, regexAttackPower);
                stat.Key = EnumSim.StatEnum.AttackPower;
                stat.Value = int.Parse(match.Groups["Number"].Value);
                return stat;
            }

            if (Regex.IsMatch(str, regexManaPer5Sec))
            {
                var match = Regex.Match(str, regexManaPer5Sec);
                stat.Key = EnumSim.StatEnum.Manaper5sec;
                stat.Value = int.Parse(match.Groups["Number"].Value);
                return stat;
            }

            if (Regex.IsMatch(str, regexDefenseRating))
            {
                var match = Regex.Match(str, regexDefenseRating);
                stat.Key = EnumSim.StatEnum.DefenseRating;
                stat.Value = int.Parse(match.Groups["Number"].Value);
                return stat;
            }

            if (Regex.IsMatch(str, regexDefense))
            {
                var match = Regex.Match(str, regexDefense);
                stat.Key = EnumSim.StatEnum.DefenseRating;
                stat.Value = int.Parse(match.Groups["Number"].Value);
                return stat;
            }

            if (Regex.IsMatch(str, regexDodgeRating))
            {
                var match = Regex.Match(str, regexDodgeRating);
                stat.Key = EnumSim.StatEnum.DodgeRating;
                stat.Value = int.Parse(match.Groups["Number"].Value);
                return stat;
            }

            if (Regex.IsMatch(str, regexBlockValue))
            {
                var match = Regex.Match(str, regexBlockValue);
                stat.Key = EnumSim.StatEnum.BlockValue;
                stat.Value = int.Parse(match.Groups["Number"].Value);
                return stat;
            }

            if (Regex.IsMatch(str, regexParryRating))
            {
                var match = Regex.Match(str, regexParryRating);
                stat.Key = EnumSim.StatEnum.ParryRating;
                stat.Value = int.Parse(match.Groups["Number"].Value);
                return stat;
            }

            if (Regex.IsMatch(str, regexResilienceRating))
            {
                var match = Regex.Match(str, regexResilienceRating);
                stat.Key = EnumSim.StatEnum.ResilienceRating;
                stat.Value = int.Parse(match.Groups["Number"].Value);
                return stat;
            }

            if (Regex.IsMatch(str, regexIgnoreArmor))
            {
                var match = Regex.Match(str, regexIgnoreArmor);
                stat.Key = EnumSim.StatEnum.IgnoreArmor;
                stat.Value = int.Parse(match.Groups["Number"].Value);
                return stat;
            }

            if (Regex.IsMatch(str, regexHealingThirdSpellDamage))
            {
                var match = Regex.Match(str, regexHealingThirdSpellDamage);
                stat.Key = EnumSim.StatEnum.HealingThirdSpellDamage;
                stat.Value = int.Parse(match.Groups["Number"].Value);
                return stat;
            }

            if (Regex.IsMatch(str, regexSpellPen))
            {
                var match = Regex.Match(str, regexSpellPen);
                stat.Key = EnumSim.StatEnum.SpellPenetration;
                stat.Value = int.Parse(match.Groups["Number"].Value);
                return stat;
            }

            if (Regex.IsMatch(str, regexExpertiseRating))
            {
                var match = Regex.Match(str, regexExpertiseRating);
                stat.Key = EnumSim.StatEnum.ExpertiseRating;
                stat.Value = int.Parse(match.Groups["Number"].Value);
                return stat;
            }

            if (Regex.IsMatch(str, regexHasteRating))
            {
                var match = Regex.Match(str, regexHasteRating);
                stat.Key = EnumSim.StatEnum.HasteRating;
                stat.Value = int.Parse(match.Groups["Number"].Value);
                return stat;
            }

            if (Regex.IsMatch(str, regexBlockRating))
            {
                var match = Regex.Match(str, regexBlockRating);
                stat.Key = EnumSim.StatEnum.BlockRating;
                stat.Value = int.Parse(match.Groups["Number"].Value);
                return stat;
            }

            if (Regex.IsMatch(str, regexHealthPer5Sec))
            {
                var match = Regex.Match(str, regexHealthPer5Sec);
                stat.Key = EnumSim.StatEnum.Healthper5sec;
                stat.Value = int.Parse(match.Groups["Number"].Value);
                return stat;
            }

            if (Regex.IsMatch(str, regexWeaponDamage))
            {
                var match = Regex.Match(str, regexWeaponDamage);
                stat.Key = EnumSim.StatEnum.WeaponDamage;
                stat.Value = int.Parse(match.Groups["Number"].Value);
                return stat;
            }

            if (Regex.IsMatch(str, regexAllResistances))
            {
                var match = Regex.Match(str, regexAllResistances);
                stat.Key = EnumSim.StatEnum.AllResistance;
                stat.Value = int.Parse(match.Groups["Number"].Value);
                return stat;
            }

            if (Regex.IsMatch(str, regexSwordSkillRating))
            {
                var match = Regex.Match(str, regexSwordSkillRating);
                stat.Key = EnumSim.StatEnum.SwordSkillRating;
                stat.Value = int.Parse(match.Groups["Number"].Value);
                return stat;
            }

            if (Regex.IsMatch(str, regexFishing))
            {
                var match = Regex.Match(str, regexFishing);
                stat.Key = EnumSim.StatEnum.Fishing;
                stat.Value = int.Parse(match.Groups["Number"].Value);
                return stat;
            }

            if (Regex.IsMatch(str, regexRangedAttackPower))
            {
                var match = Regex.Match(str, regexRangedAttackPower);
                stat.Key = EnumSim.StatEnum.RangedAttackPower;
                stat.Value = int.Parse(match.Groups["Number"].Value);
                return stat;
            }

            if (Regex.IsMatch(str, regexArmor))
            {
                var match = Regex.Match(str, regexArmor);
                stat.Key = EnumSim.StatEnum.Armor;
                stat.Value = int.Parse(match.Groups["Number"].Value);
                return stat;
            }

            if (Regex.IsMatch(str, regexSpeed))
            {
                var match = Regex.Match(str, regexSpeed);
                stat.Key = EnumSim.StatEnum.MovementSpeed;
                stat.Value = 1;
                return stat;
            }

            Equip.Add(str);
            return null;
        }

        public Item Clone()
        {
            using (var stream = new MemoryStream())
            {
                var formatter = new BinaryFormatter();
                formatter.Serialize(stream, this);
                stream.Position = 0;

                return (Item) formatter.Deserialize(stream);
            }
        }

        private void ParseRandomStats(HtmlNodeCollection htmlNode)
        {
            foreach (var randomNode in htmlNode[0].ChildNodes)
                if (randomNode.GetType() != typeof(HtmlTextNode))
                {
                    string name = null;
                    string stat = null;
                    foreach (var statNode in randomNode.ChildNodes["div"].ChildNodes)
                    {
                        if (statNode.Name.Equals("span")) name = statNode.InnerText.Replace("...", " ");

                        if (statNode.Name.Equals("br")) stat = statNode.NextSibling.InnerText.Trim().Replace("  ", "");
                    }

                    RandomStats.Add(name, stat);
                }
        }

        private int ToQuality(string str)
        {
            switch (str)
            {
                case "q0":
                    return 0;
                case "q1":
                    return 1;
                case "q2":
                    return 2;
                case "q3":
                    return 3;
                case "q4":
                    return 4;
                case "q5":
                    return 5;
                default:
                    throw new NotImplementedException();
            }
        }

        public override string ToString()
        {
            return Name + " " + Id;
        }
    }
}