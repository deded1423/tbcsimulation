﻿using System;

namespace TBCsimulation.Data.Utils
{
    public static class EnumSim
    {
        public enum DamageType
        {
            Attack,
            Spell,
            Other
        }

        public enum NpcType
        {
            Aberration,
            Beast,
            Critter,
            Demon,
            Dragon,
            Elemental,
            Giant,
            Humanoid,
            Mechanical,
            Undead,
            Uncategorized
        }

        public enum RaceType
        {
            BloodElf,
            Draenei,
            Human,
            NightElf,
            Orc,
            Dwarf,
            Troll,
            Gnome,
            Undead,
            Tauren
        }

        public enum ResourceType
        {
            Mana,
            Energy,
            Rage,
            Focus
        }

        public enum SchoolType
        {
            None,
            Arcane,
            Fire,
            Frost,
            Holy,
            Nature,
            Physical,
            Shadow,
            Racial
        }

        public enum SecondarySchoolType
        {
            None,
            Racial,
            Druid_Balance,
            Druid_FeralCombat,
            Druid_Restoration,
            Hunter_BeastMastery,
            Hunter_Marksmanship,
            Hunter_Survival,
            Mage_Arcane,
            Mage_Fire,
            Mage_Frost,
            Paladin_Holy,
            Paladin_Protection,
            Paladin_Retribution,
            Priest_Discipline,
            Priest_Holy,
            Priest_ShadowMagic,
            Rogue_Assassination,
            Rogue_Combat,
            Rogue_Subtlety,
            Shaman_ElementalCombat,
            Shaman_Enhancement,
            Shaman_Restoration,
            Warlock_Affliction,
            Warlock_Demonology,
            Warlock_Destruction,
            Warrior_Arms,
            Warrior_Fury,
            Warrior_Protection,
            Hunter_BeastTraining,
            Rogue_Poisons,
            Rogue_Lockpicking
        }

        public enum WowClass
        {
            Druid,
            Hunter,
            Mage,
            Paladin,
            Priest,
            Rogue,
            Shaman,
            Warrior,
            Warlock
        }

        public enum Socket
        {
            None,
            Blue,
            Meta,
            Red,
            Yellow,
            Green,
            Purple,
            Orange,
            Prismatic
        }

        public enum Binds
        {
            None,
            OnEquip,
            OnPickup,
            OnUse
        }

        public enum Slot
        {
            None,
            Waist,
            Wrist,
            Head,
            Chest,
            Feet,
            Hands,
            Neck,
            Shoulder,
            Legs,
            Relic,
            OffHand,
            Trinket,
            Back,
            Finger,
            Thrown,
            Ranged,
            TwoHand,
            MainHand,
            OneHand
        }

        public enum ItemType
        {
            None,
            Cloth,
            Leather,
            Mail,
            Plate,

            OffHand,
            Shield,
            Totem,
            Idol,
            Libram,
            Thrown,
            Crossbow,
            Axe,
            Mace,
            Sword,
            Bow,
            Polearm,
            Staff,
            FishingPole,
            Wand,
            FistWeapon,
            Gun,
            Dagger
        }

        public enum StatEnum
        {
            None,
            SpellCriticalStrikeRating,
            SpellDamageandHealing,
            Stamina,
            Manaper5sec,
            SpellHitRating,
            ParryRating,
            Strength,
            Agility,
            Spirit,
            Intellect,
            CriticalStrikeRating,
            HitRating,
            HealingThirdSpellDamage,
            ResilienceRating,
            AttackPower,
            DefenseRating,
            DodgeRating,
            BlockRating,
            BlockValue,
            SpellDamage,
            HasteRating,
            ExpertiseRating,
            Armor,
            ArcaneSpellDamage,
            ShadowSpellDamage,
            NatureSpellDamage,
            FireSpellDamage,
            FrostSpellDamage,
            ShadowResistance,
            NatureResistance,
            FireResistance,
            FrostResistance,
            ArcaneResistance,
            Block,
            Healthper5sec,
            HolySpellDamage,
            RangedAttackPower,
            SpellHasteRating,
            IgnoreArmor,
            SpellPenetration,
            WeaponDamage,
            AllResistance,
            SwordSkillRating,
            Fishing,
            MovementSpeed,
            MeleeDamage,
            CriticalDamage,
            Healing
        }

        public static string ParseWowClassToString(WowClass wowClass)
        {
            switch (wowClass)
            {
                case WowClass.Druid:
                    return "Druid";
                case WowClass.Hunter:
                    return "Hunter";
                case WowClass.Mage:
                    return "Mage";
                case WowClass.Paladin:
                    return "Paladin";
                case WowClass.Priest:
                    return "Priest";
                case WowClass.Rogue:
                    return "Rogue";
                case WowClass.Shaman:
                    return "Shaman";
                case WowClass.Warrior:
                    return "Warrior";
                case WowClass.Warlock:
                    return "Warlock";
                default:
                    throw new ArgumentOutOfRangeException(nameof(wowClass), wowClass, null);
            }
        }

        public static WowClass ParseStringToWowClass(string name)
        {
            switch (name)
            {
                case "Druid":
                    return WowClass.Druid;
                case "Hunter":
                    return WowClass.Hunter;
                case "Mage":
                    return WowClass.Mage;
                case "Paladin":
                    return WowClass.Paladin;
                case "Priest":
                    return WowClass.Priest;
                case "Rogue":
                    return WowClass.Rogue;
                case "Shaman":
                    return WowClass.Shaman;
                case "Warrior":
                    return WowClass.Warrior;
                case "Warlock":
                    return WowClass.Warlock;
                default:
                    throw new ArgumentOutOfRangeException(name);
            }
        }
    }
}