﻿using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;
using TBCsimulation.Data.TalentsInfo;

namespace TBCsimulation.Data.Utils
{
    public static class DataParser
    {
        public static void CreateDataFiles()
        {
            var exists = Directory.Exists(Directory.GetCurrentDirectory() + @"\Data");

            if (!exists)
                Directory.CreateDirectory(Directory.GetCurrentDirectory() + @"\Data");

            // PrintTalentId(Directory.GetCurrentDirectory() + @"\Data\DruidTalentsId.cs", Talents.Druid, "Druid_");
            // PrintTalentDicionary(Directory.GetCurrentDirectory() + @"\Data\DruidTalentsDic.cs", Talents.Druid,
            //     "Druid");
            // PrintTalentId(Directory.GetCurrentDirectory() + @"\Data\HunterTalentsId.cs", Talents.Hunter, "Hunter_");
            // PrintTalentDicionary(Directory.GetCurrentDirectory() + @"\Data\HunterTalentsDic.cs", Talents.Hunter,
            //     "Hunter");
            // PrintTalentId(Directory.GetCurrentDirectory() + @"\Data\MageTalentsId.cs", Talents.Mage, "Mage_");
            // PrintTalentDicionary(Directory.GetCurrentDirectory() + @"\Data\MageTalentsDic.cs", Talents.Mage,
            //     "Mage");
            // PrintTalentId(Directory.GetCurrentDirectory() + @"\Data\PaladinTalentsId.cs", Talents.Paladin, "Paladin_");
            // PrintTalentDicionary(Directory.GetCurrentDirectory() + @"\Data\PaladinTalentsDic.cs", Talents.Paladin,
            //     "Paladin");
            // PrintTalentId(Directory.GetCurrentDirectory() + @"\Data\PriestTalentsId.cs", Talents.Priest, "Priest_");
            // PrintTalentDicionary(Directory.GetCurrentDirectory() + @"\Data\PriestTalentsDic.cs", Talents.Priest,
            //     "Priest");
            // PrintTalentId(Directory.GetCurrentDirectory() + @"\Data\RogueTalentsId.cs", Talents.Rogue, "Rogue_");
            // PrintTalentDicionary(Directory.GetCurrentDirectory() + @"\Data\RogueTalentsDic.cs", Talents.Rogue,
            //     "Rogue");
            // PrintTalentId(Directory.GetCurrentDirectory() + @"\Data\ShamanTalentsId.cs", Talents.Shaman, "Shaman_");
            // PrintTalentDicionary(Directory.GetCurrentDirectory() + @"\Data\ShamankTalentsDic.cs", Talents.Shaman,
            //     "Shaman");
            // PrintTalentId(Directory.GetCurrentDirectory() + @"\Data\WarlockTalentsId.cs", Talents.Warlock, "Warlock_");
            // PrintTalentDicionary(Directory.GetCurrentDirectory() + @"\Data\WarlockTalentsDic.cs", Talents.Warlock,
            //     "Warlock");
            // PrintTalentId(Directory.GetCurrentDirectory() + @"\Data\WarriorTalentsId.cs", Talents.Warrior, "Warrior_");
            // PrintTalentDicionary(Directory.GetCurrentDirectory() + @"\Data\WarriorTalentsDic.cs", Talents.Warrior,
            //     "Warrior");


            // PrintSpellId(Directory.GetCurrentDirectory() + @"\Data\DruidSpellId.cs",
            //     @"F:\Proyectos\TBCsimulation\TBCsimulation\Data\Druid\Spell\Info", "Druid_");
            // PrintSpellDicionaries(Directory.GetCurrentDirectory() + @"\Data\DruidSpellDic.cs",
            //     @"F:\Proyectos\TBCsimulation\TBCsimulation\Data\Druid\Spell\Info",
            //     "Druid");
            // PrintSpellId(Directory.GetCurrentDirectory() + @"\Data\HunterSpellId.cs",
            //     @"F:\Proyectos\TBCsimulation\TBCsimulation\Data\Hunter\Spell\Info", "Hunter_");
            // PrintSpellDicionaries(Directory.GetCurrentDirectory() + @"\Data\HunterSpellDic.cs",
            //     @"F:\Proyectos\TBCsimulation\TBCsimulation\Data\Hunter\Spell\Info",
            //     "Hunter");
            // PrintSpellId(Directory.GetCurrentDirectory() + @"\Data\MageSpellId.cs",
            //     @"F:\Proyectos\TBCsimulation\TBCsimulation\Data\Mage\Spell\Info", "Mage_");
            // PrintSpellDicionaries(Directory.GetCurrentDirectory() + @"\Data\MageSpellDic.cs",
            //     @"F:\Proyectos\TBCsimulation\TBCsimulation\Data\Mage\Spell\Info",
            //     "Mage");
            // PrintSpellId(Directory.GetCurrentDirectory() + @"\Data\PaladinSpellId.cs",
            //     @"F:\Proyectos\TBCsimulation\TBCsimulation\Data\Paladin\Spell\Info", "Paladin_");
            // PrintSpellDicionaries(Directory.GetCurrentDirectory() + @"\Data\PaladinSpellDic.cs",
            //     @"F:\Proyectos\TBCsimulation\TBCsimulation\Data\Paladin\Spell\Info",
            //     "Paladin");
            // PrintSpellId(Directory.GetCurrentDirectory() + @"\Data\PriestSpellId.cs",
            //     @"F:\Proyectos\TBCsimulation\TBCsimulation\Data\Priest\Spell\Info", "Priest_");
            // PrintSpellDicionaries(Directory.GetCurrentDirectory() + @"\Data\PriestSpellDic.cs",
            //     @"F:\Proyectos\TBCsimulation\TBCsimulation\Data\Priest\Spell\Info",
            //     "Priest");
            // PrintSpellId(Directory.GetCurrentDirectory() + @"\Data\RogueSpellId.cs",
            //     @"F:\Proyectos\TBCsimulation\TBCsimulation\Data\Rogue\Spell\Info", "Rogue_");
            // PrintSpellDicionaries(Directory.GetCurrentDirectory() + @"\Data\RogueSpellDic.cs",
            //     @"F:\Proyectos\TBCsimulation\TBCsimulation\Data\Rogue\Spell\Info",
            //     "Rogue");
            // PrintSpellId(Directory.GetCurrentDirectory() + @"\Data\ShamanSpellId.cs",
            //     @"F:\Proyectos\TBCsimulation\TBCsimulation\Data\Shaman\Spell\Info", "Shaman_");
            // PrintSpellDicionaries(Directory.GetCurrentDirectory() + @"\Data\ShamanSpellDic.cs",
            //     @"F:\Proyectos\TBCsimulation\TBCsimulation\Data\Shaman\Spell\Info",
            //     "Shaman");
            // PrintSpellId(Directory.GetCurrentDirectory() + @"\Data\WarlockSpellId.cs",
            //     @"F:\Proyectos\TBCsimulation\TBCsimulation\Data\Warlock\Spell\Info", "Warlock_");
            // PrintSpellDicionaries(Directory.GetCurrentDirectory() + @"\Data\WarlockSpellDic.cs",
            //     @"F:\Proyectos\TBCsimulation\TBCsimulation\Data\Warlock\Spell\Info",
            //     "Warlock");
            // PrintSpellId(Directory.GetCurrentDirectory() + @"\Data\WarriorSpellId.cs",
            //     @"F:\Proyectos\TBCsimulation\TBCsimulation\Data\Warrior\Spell\Info", "Warrior_");
            // PrintSpellDicionaries(Directory.GetCurrentDirectory() + @"\Data\WarriorSpellDic.cs",
            //     @"F:\Proyectos\TBCsimulation\TBCsimulation\Data\Warrior\Spell\Info",
            //     "Warrior");
            PrintSpellId(Directory.GetCurrentDirectory() + @"\Data\RacialSpellId.cs",
                @"F:\Proyectos\TBCsimulation\TBCsimulation\Data\Racial\Spell\Info", "Racial_");
            PrintSpellDicionaries(Directory.GetCurrentDirectory() + @"\Data\RacialSpellDic.cs",
                @"F:\Proyectos\TBCsimulation\TBCsimulation\Data\Racial\Spell\Info",
                "Racial");
        }

        /// <summary>
        /// Print the talents from a list of talents into an int 
        /// </summary>
        /// <param name="FileName">Filename that will be created with the dicionary</param>
        /// <param name="talents">List to be printed</param>
        /// <param name="extraName">Prefix that will have the variable</param>
        public static void PrintTalentId(string FileName, List<Talent> talents, string extraName)
        {
            var writer = new StreamWriter(FileName);
            writer.WriteLine("//####### AUTOGENERATED #######");
            writer.WriteLine("using System.Collections.Generic;");
            writer.WriteLine("using TBCsimulation.Data.Utils;");
            writer.WriteLine("using TBCsimulation.Simulation;");

            writer.WriteLine("namespace TBCsimulation.Data.TalentsInfo");
            writer.WriteLine("{");
            writer.WriteLine("public static partial class Talents");
            writer.WriteLine(" {");
            foreach (var talent in talents)
            {
                writer.WriteLine("//####### AUTOGENERATED #######");
                writer.WriteLine("/// <summary>");
                writer.WriteLine("/// " + talent.Tooltip);
                writer.WriteLine("/// </summary>");
                writer.WriteLine("public const int " + extraName +
                                 talent.Name
                                     .Replace(" ", "")
                                     .Replace("'", "")
                                     .Replace(":", "")
                                     .Replace("-", "")
                                     .Replace("(", "")
                                     .Replace(")", "")
                                 + " = " + talent.Id +
                                 "; \n");
                writer.Flush();
            }

            writer.WriteLine(" }");
            writer.WriteLine(" }");
            writer.Close();
        }

        public static void PrintTalentDicionary(string FileName, List<Talent> talents, string extraName)
        {
            var writer = new StreamWriter(FileName);
            writer.WriteLine("//####### AUTOGENERATED #######");
            writer.WriteLine("using System.Collections.Generic;");
            writer.WriteLine("using TBCsimulation.Data.Utils;");
            writer.WriteLine("using TBCsimulation.Simulation;");

            writer.WriteLine("namespace TBCsimulation.Data.TalentsInfo");
            writer.WriteLine("{");
            writer.WriteLine("public static partial class Talents");
            writer.WriteLine(" {");
            writer.WriteLine("//####### AUTOGENERATED #######");
            writer.WriteLine("public static readonly Dictionary<int, string> " + extraName +
                             "DictionaryIdToName = new Dictionary<int, string>()");
            writer.WriteLine("{");
            foreach (var talent in talents)
            {
                writer.WriteLine("{" + talent.Id + ", \"" + talent.Name + "\"},\n");
                writer.Flush();
            }

            writer.WriteLine("};");
            writer.WriteLine("");
            writer.WriteLine("");
            writer.WriteLine("");
            writer.WriteLine("");

            writer.WriteLine("//####### AUTOGENERATED #######");
            writer.WriteLine("public static readonly Dictionary<string, int> " + extraName +
                             "DictionaryNameToId = new Dictionary<string, int>()");
            writer.WriteLine("{");
            foreach (var talent in talents)
            {
                writer.WriteLine("{\"" + talent.Name + "\", " + talent.Id + "},\n");
                writer.Flush();
            }

            writer.WriteLine("};");
            writer.WriteLine("}");
            writer.WriteLine("}");
            writer.Close();
        }

        /// <summary>
        /// Print ints that contain the id of the spells in the folder
        /// </summary>
        /// <param name="FolderName">Folder that contains all Spell classes</param>
        /// <param name="FileName">Filename that will be created with the dicionary</param>
        /// <param name="extraName">Prefix that will have the variable</param>
        public static void PrintSpellId(string FileName, string FolderName, string extraName)
        {
            var filePaths = Directory.GetFiles(FolderName);
            var MatchPhrase = @"(Id = )(?<id>\d*);";

            var writer = new StreamWriter(FileName);
            writer.WriteLine("//####### AUTOGENERATED #######");
            writer.WriteLine("using System.Collections.Generic;");
            writer.WriteLine("using System.Diagnostics.CodeAnalysis;");

            writer.WriteLine("namespace TBCsimulation.Data.SpellsInfo");
            writer.WriteLine("{");
            writer.WriteLine("public static partial class SpellId");
            writer.WriteLine("{");
            foreach (var path in filePaths)
            {
                var name = Path.GetFileNameWithoutExtension(path);
                name = name.Replace("SpellInfo", "");
                using (var reader = new StreamReader(path))
                {
                    var line = reader.ReadLine();
                    while (line != null)
                    {
                        if (Regex.IsMatch(line, MatchPhrase))
                        {
                            var groups = Regex.Matches(line, MatchPhrase)[0].Groups;
                            var id = int.Parse(groups["id"].Value);
                            writer.WriteLine("//####### AUTOGENERATED #######");
                            writer.WriteLine("public const int " + extraName + name + " = " + id + "; \n");
                        }

                        line = reader.ReadLine();
                    }
                }
            }

            writer.WriteLine("}");
            writer.WriteLine("}");
            writer.Close();
        }

        /// <summary>
        /// Print spell dicionaries with id and name of spells
        /// </summary>
        /// <param name="FolderName">Folder that contains all Spell classes</param>
        /// <param name="FileName">Filename that will be created with the dicionary</param>
        /// <param name="extra">Prefix before the DicionaryName</param>
        /// 
        public static void PrintSpellDicionaries(string FileName, string FolderName, string extra)
        {
            var filePaths = Directory.GetFiles(FolderName);
            var MatchPhraseId = @"(Id = )(?<id>\d*);";
            var MatchPhraseName = "(Name = )\"(?<name>.*)\";";

            var writer = new StreamWriter(FileName);
            writer.WriteLine("//####### AUTOGENERATED #######");
            writer.WriteLine("using System.Collections.Generic;");
            writer.WriteLine("using System.Diagnostics.CodeAnalysis;");

            writer.WriteLine("namespace TBCsimulation.Data.SpellsInfo");
            writer.WriteLine("{");
            writer.WriteLine("public static partial class SpellId");
            writer.WriteLine("{");
            writer.WriteLine("//####### AUTOGENERATED #######");
            writer.WriteLine("public static readonly Dictionary<int, string> " + extra +
                             "DictionaryIdToName = new Dictionary<int, string>()");
            writer.WriteLine("{");
            foreach (var path in filePaths)
            {
                var name = "";
                var id = 0;
                using (var reader = new StreamReader(path))
                {
                    var line = reader.ReadLine();
                    while (line != null)
                    {
                        if (Regex.IsMatch(line, MatchPhraseId))
                        {
                            var groups = Regex.Matches(line, MatchPhraseId)[0].Groups;
                            id = int.Parse(groups["id"].Value);
                        }

                        if (Regex.IsMatch(line, MatchPhraseName))
                        {
                            var groups = Regex.Matches(line, MatchPhraseName)[0].Groups;
                            name = groups["name"].Value;
                        }

                        line = reader.ReadLine();
                    }

                    writer.WriteLine("{" + id + ", \"" + name + "\"},\n");
                }
            }

            writer.WriteLine("};");
            writer.WriteLine("");
            writer.WriteLine("");
            writer.WriteLine("");
            writer.WriteLine("");

            writer.WriteLine("//####### AUTOGENERATED #######");
            writer.WriteLine("public static readonly Dictionary<string, int> " + extra +
                             "DictionaryNameToId = new Dictionary<string, int>()");
            writer.WriteLine("{");
            foreach (var path in filePaths)
            {
                var name = "";
                var id = 0;
                using (var reader = new StreamReader(path))
                {
                    var line = reader.ReadLine();
                    while (line != null)
                    {
                        if (Regex.IsMatch(line, MatchPhraseId))
                        {
                            var groups = Regex.Matches(line, MatchPhraseId)[0].Groups;
                            id = int.Parse(groups["id"].Value);
                        }

                        if (Regex.IsMatch(line, MatchPhraseName))
                        {
                            var groups = Regex.Matches(line, MatchPhraseName)[0].Groups;
                            name = groups["name"].Value;
                        }

                        line = reader.ReadLine();
                    }

                    writer.WriteLine("{\"" + name + "\", " + id + "},\n");
                }
            }

            writer.WriteLine("};");
            writer.WriteLine("}");
            writer.WriteLine("}");
            writer.Close();
        }
    }
}