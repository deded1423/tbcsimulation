﻿using TBCsimulation.Data.SpellsInfo;
using TBCsimulation.Data.TalentsInfo;

namespace TBCsimulation.Data.Utils
{
    public static class IdUtils
    {
        public static int GetIdFromName(string name)
        {
            var id = SpellId.GetIdFromName(name);

            if (id != 0)
                return id;
            return Talents.GetIdFromName(name);
            ;
        }

        public static string GetNameFromId(int id)
        {
            var name = SpellId.GetNameFromId(id);
            if (name != null)
                return name;
            return Talents.GetNameFromId(id);
        }
    }
}