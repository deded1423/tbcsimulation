using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Priest.Spell
{
    public partial class MindBlastSpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public MindBlastSpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=25375/mind-blast
            Name = "Mind Blast";
            Id = 25375;
            Level = 69;
            Rank = 11;
            Range = 30;

            Mana = 450;

            IsGCD = true;
            Channeled = false;
            CastTime = 1500;
            CooldownTime = 8000;

            School = EnumSim.SchoolType.Shadow;
            SecondarySchool = EnumSim.SecondarySchoolType.Priest_ShadowMagic;
            Init(sim);
        }
    }
}