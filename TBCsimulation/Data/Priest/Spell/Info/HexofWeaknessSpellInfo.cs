using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Priest.Spell
{
    public partial class HexofWeaknessSpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public HexofWeaknessSpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=25470/hex-of-weakness
            Name = "Hex of Weakness";
            Id = 25470;
            Level = 70;
            Rank = 1;
            Range = 30;

            Mana = 295;

            IsGCD = true;
            Channeled = false;
            CastTime = 0;
            CooldownTime = 0;

            School = EnumSim.SchoolType.Shadow;
            SecondarySchool = EnumSim.SecondarySchoolType.Priest_ShadowMagic;
            Init(sim);
        }
    }
}