using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Priest.Spell
{
    public partial class PowerWordFortitudeSpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public PowerWordFortitudeSpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=25389/power-word-fortitude
            Name = "Power Word: Fortitude";
            Id = 25389;
            Level = 70;
            Rank = 7;
            Range = 30;

            Mana = 700;

            IsGCD = true;
            Channeled = false;
            CastTime = 0;
            CooldownTime = 0;

            School = EnumSim.SchoolType.Holy;
            SecondarySchool = EnumSim.SecondarySchoolType.Priest_Discipline;
            Init(sim);
        }
    }
}