using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Priest.Spell
{
    public partial class PrayerofMendingSpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public PrayerofMendingSpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=33076/prayer-of-mending
            Name = "Prayer of Mending";
            Id = 33076;
            Level = 68;
            Rank = 1;
            Range = 40;

            Mana = 390;

            IsGCD = true;
            Channeled = false;
            CastTime = 0;
            CooldownTime = 10000;

            School = EnumSim.SchoolType.Holy;
            SecondarySchool = EnumSim.SecondarySchoolType.Priest_Holy;
            Init(sim);
        }
    }
}