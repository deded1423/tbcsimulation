using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Priest.Spell
{
    public partial class MindControlSpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public MindControlSpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=10912/mind-control
            Name = "Mind Control";
            Id = 10912;
            Level = 58;
            Rank = 3;
            Range = 20;

            Mana = 750;

            IsGCD = true;
            Channeled = false;
            CastTime = 3000;
            CooldownTime = 0;

            School = EnumSim.SchoolType.Shadow;
            SecondarySchool = EnumSim.SecondarySchoolType.Priest_ShadowMagic;
            Init(sim);
        }
    }
}