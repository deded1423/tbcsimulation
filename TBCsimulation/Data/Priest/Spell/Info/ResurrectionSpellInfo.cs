using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Priest.Spell
{
    public partial class ResurrectionSpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public ResurrectionSpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=25435/resurrection
            Name = "Resurrection";
            Id = 25435;
            Level = 68;
            Rank = 6;
            Range = 30;

            Mana = -1;

            IsGCD = true;
            Channeled = false;
            CastTime = 10000;
            CooldownTime = 0;

            School = EnumSim.SchoolType.Holy;
            SecondarySchool = EnumSim.SecondarySchoolType.Priest_Holy;
            Init(sim);
        }
    }
}