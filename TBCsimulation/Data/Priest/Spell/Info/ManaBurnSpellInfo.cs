using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Priest.Spell
{
    public partial class ManaBurnSpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public ManaBurnSpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=25380/mana-burn
            Name = "Mana Burn";
            Id = 25380;
            Level = 70;
            Rank = 7;
            Range = 30;

            Mana = 355;

            IsGCD = true;
            Channeled = false;
            CastTime = 3000;
            CooldownTime = 0;

            School = EnumSim.SchoolType.Shadow;
            SecondarySchool = EnumSim.SecondarySchoolType.Priest_Discipline;
            Init(sim);
        }
    }
}