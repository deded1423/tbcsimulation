using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Priest.Spell
{
    public partial class AbolishDiseaseSpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public AbolishDiseaseSpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=552/abolish-disease
            Name = "Abolish Disease";
            Id = 552;
            Level = 32;
            Rank = 1;
            Range = 40;

            Mana = -1;

            IsGCD = true;
            Channeled = false;
            CastTime = 0;
            CooldownTime = 0;

            School = EnumSim.SchoolType.Holy;
            SecondarySchool = EnumSim.SecondarySchoolType.Priest_Holy;
            Init(sim);
        }
    }
}