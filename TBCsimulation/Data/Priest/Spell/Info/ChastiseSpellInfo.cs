using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Priest.Spell
{
    public partial class ChastiseSpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public ChastiseSpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=44047/chastise
            Name = "Chastise";
            Id = 44047;
            Level = 70;
            Rank = 1;
            Range = 20;

            Mana = 300;

            IsGCD = false;
            Channeled = false;
            CastTime = 0;
            CooldownTime = 30000;

            School = EnumSim.SchoolType.Holy;
            SecondarySchool = EnumSim.SecondarySchoolType.Priest_Holy;
            Init(sim);
        }
    }
}