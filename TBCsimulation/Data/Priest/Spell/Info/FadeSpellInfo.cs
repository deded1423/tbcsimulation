using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Priest.Spell
{
    public partial class FadeSpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public FadeSpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=25429/fade
            Name = "Fade";
            Id = 25429;
            Level = 66;
            Rank = 7;
            Range = 0;

            Mana = 330;

            IsGCD = true;
            Channeled = false;
            CastTime = 0;
            CooldownTime = 30000;

            School = EnumSim.SchoolType.Shadow;
            SecondarySchool = EnumSim.SecondarySchoolType.Priest_ShadowMagic;
            Init(sim);
        }
    }
}