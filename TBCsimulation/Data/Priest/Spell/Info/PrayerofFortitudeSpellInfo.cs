using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Priest.Spell
{
    public partial class PrayerofFortitudeSpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public PrayerofFortitudeSpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=25392/prayer-of-fortitude
            Name = "Prayer of Fortitude";
            Id = 25392;
            Level = 70;
            Rank = 3;
            Range = 40;

            Mana = 1800;

            IsGCD = true;
            Channeled = false;
            CastTime = 0;
            CooldownTime = 0;

            School = EnumSim.SchoolType.Holy;
            SecondarySchool = EnumSim.SecondarySchoolType.Priest_Discipline;
            Init(sim);
        }
    }
}