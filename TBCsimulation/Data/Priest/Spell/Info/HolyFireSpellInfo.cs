using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Priest.Spell
{
    public partial class HolyFireSpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public HolyFireSpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=25384/holy-fire
            Name = "Holy Fire";
            Id = 25384;
            Level = 66;
            Rank = 9;
            Range = 30;

            Mana = 290;

            IsGCD = true;
            Channeled = false;
            CastTime = 3500;
            CooldownTime = 0;

            School = EnumSim.SchoolType.Holy;
            SecondarySchool = EnumSim.SecondarySchoolType.Priest_Holy;
            Init(sim);
        }
    }
}