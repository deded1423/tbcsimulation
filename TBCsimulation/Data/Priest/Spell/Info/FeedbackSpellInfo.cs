using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Priest.Spell
{
    public partial class FeedbackSpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public FeedbackSpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=25441/feedback
            Name = "Feedback";
            Id = 25441;
            Level = 70;
            Rank = 1;
            Range = 0;

            Mana = 705;

            IsGCD = true;
            Channeled = false;
            CastTime = 0;
            CooldownTime = 180000;

            School = EnumSim.SchoolType.Shadow;
            SecondarySchool = EnumSim.SecondarySchoolType.Priest_Discipline;
            Init(sim);
        }
    }
}