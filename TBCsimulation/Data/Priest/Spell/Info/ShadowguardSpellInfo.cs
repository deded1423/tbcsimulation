using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Priest.Spell
{
    public partial class ShadowguardSpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public ShadowguardSpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=25477/shadowguard
            Name = "Shadowguard";
            Id = 25477;
            Level = 68;
            Rank = 1;
            Range = 0;

            Mana = 270;

            IsGCD = true;
            Channeled = false;
            CastTime = 0;
            CooldownTime = 0;

            School = EnumSim.SchoolType.Shadow;
            SecondarySchool = EnumSim.SecondarySchoolType.Priest_ShadowMagic;
            Init(sim);
        }
    }
}