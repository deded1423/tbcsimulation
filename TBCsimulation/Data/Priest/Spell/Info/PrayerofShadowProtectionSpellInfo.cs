using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Priest.Spell
{
    public partial class PrayerofShadowProtectionSpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public PrayerofShadowProtectionSpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=39374/prayer-of-shadow-protection
            Name = "Prayer of Shadow Protection";
            Id = 39374;
            Level = 70;
            Rank = 2;
            Range = 40;

            Mana = 1620;

            IsGCD = true;
            Channeled = false;
            CastTime = 0;
            CooldownTime = 0;

            School = EnumSim.SchoolType.Holy;
            SecondarySchool = EnumSim.SecondarySchoolType.Priest_ShadowMagic;
            Init(sim);
        }
    }
}