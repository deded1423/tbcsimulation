using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Priest.Spell
{
    public partial class SymbolofHopeSpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public SymbolofHopeSpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=32548/symbol-of-hope
            Name = "Symbol of Hope";
            Id = 32548;
            Level = 10;
            Rank = 1;
            Range = 0;

            Mana = 15;

            IsGCD = true;
            Channeled = false;
            CastTime = 0;
            CooldownTime = 300000;

            School = EnumSim.SchoolType.Holy;
            SecondarySchool = EnumSim.SecondarySchoolType.Priest_Discipline;
            Init(sim);
        }
    }
}