using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Priest.Spell
{
    public partial class PowerWordShieldSpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public PowerWordShieldSpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=25218/power-word-shield
            Name = "Power Word: Shield";
            Id = 25218;
            Level = 70;
            Rank = 12;
            Range = 40;

            Mana = 600;

            IsGCD = true;
            Channeled = false;
            CastTime = 0;
            CooldownTime = 4000;

            School = EnumSim.SchoolType.Holy;
            SecondarySchool = EnumSim.SecondarySchoolType.Priest_Discipline;
            Init(sim);
        }
    }
}