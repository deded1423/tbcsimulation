using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Priest.Spell
{
    public partial class ShadowWordDeathSpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public ShadowWordDeathSpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=32996/shadow-word-death
            Name = "Shadow Word: Death";
            Id = 32996;
            Level = 70;
            Rank = 2;
            Range = 30;

            Mana = 309;

            IsGCD = true;
            Channeled = false;
            CastTime = 0;
            CooldownTime = 12000;

            School = EnumSim.SchoolType.Shadow;
            SecondarySchool = EnumSim.SecondarySchoolType.Priest_ShadowMagic;
            Init(sim);
        }
    }
}