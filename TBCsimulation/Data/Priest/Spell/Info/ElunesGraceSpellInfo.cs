using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Priest.Spell
{
    public partial class ElunesGraceSpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public ElunesGraceSpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=2651/elunes-grace
            Name = "Elune's Grace";
            Id = 2651;
            Level = 20;
            Rank = 1;
            Range = 0;

            Mana = -1;

            IsGCD = true;
            Channeled = false;
            CastTime = 0;
            CooldownTime = 180000;

            School = EnumSim.SchoolType.Holy;
            SecondarySchool = EnumSim.SecondarySchoolType.Priest_Discipline;
            Init(sim);
        }
    }
}