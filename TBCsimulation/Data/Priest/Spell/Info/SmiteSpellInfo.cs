using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Priest.Spell
{
    public partial class SmiteSpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public SmiteSpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=25364/smite
            Name = "Smite";
            Id = 25364;
            Level = 69;
            Rank = 10;
            Range = 30;

            Mana = 385;

            IsGCD = true;
            Channeled = false;
            CastTime = 2500;
            CooldownTime = 0;

            School = EnumSim.SchoolType.Holy;
            SecondarySchool = EnumSim.SecondarySchoolType.Priest_Holy;
            Init(sim);
        }
    }
}