using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Priest.Spell
{
    public partial class PrayerofHealingSpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public PrayerofHealingSpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=25316/prayer-of-healing
            Name = "Prayer of Healing";
            Id = 25316;
            Level = 60;
            Rank = 5;
            Range = 0;

            Mana = 1070;

            IsGCD = true;
            Channeled = false;
            CastTime = 3000;
            CooldownTime = 0;

            School = EnumSim.SchoolType.Holy;
            SecondarySchool = EnumSim.SecondarySchoolType.Priest_Holy;
            Init(sim);
        }
    }
}