using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Priest.Spell
{
    public partial class InnerFireSpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public InnerFireSpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=25431/inner-fire
            Name = "Inner Fire";
            Id = 25431;
            Level = 69;
            Rank = 7;
            Range = 0;

            Mana = 375;

            IsGCD = true;
            Channeled = false;
            CastTime = 0;
            CooldownTime = 0;

            School = EnumSim.SchoolType.Holy;
            SecondarySchool = EnumSim.SecondarySchoolType.Priest_Discipline;
            Init(sim);
        }
    }
}