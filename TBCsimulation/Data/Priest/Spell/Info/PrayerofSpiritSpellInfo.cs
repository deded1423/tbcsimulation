using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Priest.Spell
{
    public partial class PrayerofSpiritSpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public PrayerofSpiritSpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=32999/prayer-of-spirit
            Name = "Prayer of Spirit";
            Id = 32999;
            Level = 70;
            Rank = 2;
            Range = 40;

            Mana = 1800;

            IsGCD = true;
            Channeled = false;
            CastTime = 0;
            CooldownTime = 0;

            School = EnumSim.SchoolType.Holy;
            SecondarySchool = EnumSim.SecondarySchoolType.Priest_Discipline;
            Init(sim);
        }
    }
}