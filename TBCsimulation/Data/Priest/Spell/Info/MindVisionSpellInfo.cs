using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Priest.Spell
{
    public partial class MindVisionSpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public MindVisionSpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=10909/mind-vision
            Name = "Mind Vision";
            Id = 10909;
            Level = 44;
            Rank = 2;
            Range = 50000;

            Mana = 150;

            IsGCD = true;
            Channeled = true;
            CastTime = 1000;
            CooldownTime = 0;

            School = EnumSim.SchoolType.Shadow;
            SecondarySchool = EnumSim.SecondarySchoolType.Priest_ShadowMagic;
            Init(sim);
        }
    }
}