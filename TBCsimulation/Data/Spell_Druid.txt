Url = http://tbc.wowhead.com/spell=40120/swift-flight-form
Name = Swift Flight Form
Nameformatted = SwiftFlightForm
Id = 40120
Level = 70
School = Physical
SecondarySchool = Feral Combat
Mana = -1
Range = 0
IsGCD = True
Rank = 1
Channeled = False
CooldownTime = 0
CastTime = 0
TickTime = 0


Url = http://tbc.wowhead.com/spell=33943/flight-form
Name = Flight Form
Nameformatted = FlightForm
Id = 33943
Level = 68
School = Physical
SecondarySchool = Feral Combat
Mana = -1
Range = 0
IsGCD = True
Rank = 1
Channeled = False
CooldownTime = 0
CastTime = 0
TickTime = 0


Url = http://tbc.wowhead.com/spell=33763/lifebloom
Name = Lifebloom
Nameformatted = Lifebloom
Id = 33763
Level = 64
School = Nature
SecondarySchool = Restoration
Mana = 220
Range = 40
IsGCD = True
Rank = 1
Channeled = False
CooldownTime = 0
CastTime = 0
TickTime = 0


Url = http://tbc.wowhead.com/spell=33745/lacerate
Name = Lacerate
Nameformatted = Lacerate
Id = 33745
Level = 66
School = Physical
SecondarySchool = Feral Combat
Mana = -1
Range = 5
IsGCD = True
Rank = 1
Channeled = False
CooldownTime = 0
CastTime = 0
TickTime = 0


Url = http://tbc.wowhead.com/spell=33786/cyclone
Name = Cyclone
Nameformatted = Cyclone
Id = 33786
Level = 70
School = Nature
SecondarySchool = Balance
Mana = -1
Range = 20
IsGCD = True
Rank = 1
Channeled = False
CooldownTime = 0
CastTime = 1500
TickTime = 0


Url = http://tbc.wowhead.com/spell=768/cat-form
Name = Cat Form
Nameformatted = CatForm
Id = 768
Level = 20
School = Physical
SecondarySchool = Feral Combat
Mana = -1
Range = 0
IsGCD = True
Rank = 1
Channeled = False
CooldownTime = 0
CastTime = 0
TickTime = 0


Url = http://tbc.wowhead.com/spell=29166/innervate
Name = Innervate
Nameformatted = Innervate
Id = 29166
Level = 40
School = Nature
SecondarySchool = Balance
Mana = -1
Range = 30
IsGCD = True
Rank = 1
Channeled = False
CooldownTime = 360000
CastTime = 0
TickTime = 0


Url = http://tbc.wowhead.com/spell=27012/hurricane
Name = Hurricane
Nameformatted = Hurricane
Id = 27012
Level = 70
School = Nature
SecondarySchool = Balance
Mana = 1905
Range = 30
IsGCD = True
Rank = 4
Channeled = True
CooldownTime = 1000
CastTime = 10000
TickTime = 0


Url = http://tbc.wowhead.com/spell=9634/dire-bear-form
Name = Dire Bear Form
Nameformatted = DireBearForm
Id = 9634
Level = 40
School = Physical
SecondarySchool = Feral Combat
Mana = -1
Range = 0
IsGCD = True
Rank = 1
Channeled = False
CooldownTime = 0
CastTime = 0
TickTime = 0


Url = http://tbc.wowhead.com/spell=26986/starfire
Name = Starfire
Nameformatted = Starfire
Id = 26986
Level = 67
School = Arcane
SecondarySchool = Balance
Mana = 370
Range = 30
IsGCD = True
Rank = 8
Channeled = False
CooldownTime = 0
CastTime = 3500
TickTime = 0


Url = http://tbc.wowhead.com/spell=33987/mangle-bear
Name = Mangle (Bear)
Nameformatted = MangleBear
Id = 33987
Level = 68
School = Physical
SecondarySchool = Feral Combat
Mana = -1
Range = 5
IsGCD = True
Rank = 3
Channeled = False
CooldownTime = 6000
CastTime = 0
TickTime = 0


Url = http://tbc.wowhead.com/spell=5487/bear-form
Name = Bear Form
Nameformatted = BearForm
Id = 5487
Level = 10
School = Physical
SecondarySchool = Feral Combat
Mana = -1
Range = 0
IsGCD = True
Rank = 1
Channeled = False
CooldownTime = 0
CastTime = 0
TickTime = 0


Url = http://tbc.wowhead.com/spell=26985/wrath
Name = Wrath
Nameformatted = Wrath
Id = 26985
Level = 69
School = Nature
SecondarySchool = Balance
Mana = 255
Range = 30
IsGCD = True
Rank = 10
Channeled = False
CooldownTime = 0
CastTime = 2000
TickTime = 0


Url = http://tbc.wowhead.com/spell=26997/swipe
Name = Swipe
Nameformatted = Swipe
Id = 26997
Level = 64
School = Physical
SecondarySchool = Feral Combat
Mana = -1
Range = 5
IsGCD = True
Rank = 6
Channeled = False
CooldownTime = 0
CastTime = 0
TickTime = 0


Url = http://tbc.wowhead.com/spell=27002/shred
Name = Shred
Nameformatted = Shred
Id = 27002
Level = 70
School = Physical
SecondarySchool = Feral Combat
Mana = -1
Range = 5
IsGCD = True
Rank = 7
Channeled = False
CooldownTime = 0
CastTime = 0
TickTime = 0


Url = http://tbc.wowhead.com/spell=9846/tigers-fury
Name = Tiger's Fury
Nameformatted = TigersFury
Id = 9846
Level = 60
School = Physical
SecondarySchool = Feral Combat
Mana = -1
Range = 0
IsGCD = False
Rank = 4
Channeled = False
CooldownTime = 1000
CastTime = 0
TickTime = 0


Url = http://tbc.wowhead.com/spell=26982/rejuvenation
Name = Rejuvenation
Nameformatted = Rejuvenation
Id = 26982
Level = 69
School = Nature
SecondarySchool = Restoration
Mana = 415
Range = 40
IsGCD = True
Rank = 13
Channeled = False
CooldownTime = 0
CastTime = 0
TickTime = 0


Url = http://tbc.wowhead.com/spell=26993/faerie-fire
Name = Faerie Fire
Nameformatted = FaerieFire
Id = 26993
Level = 66
School = Nature
SecondarySchool = Balance
Mana = 145
Range = 30
IsGCD = True
Rank = 5
Channeled = False
CooldownTime = 0
CastTime = 0
TickTime = 0


Url = http://tbc.wowhead.com/spell=783/travel-form
Name = Travel Form
Nameformatted = TravelForm
Id = 783
Level = 30
School = Physical
SecondarySchool = Feral Combat
Mana = -1
Range = 0
IsGCD = True
Rank = 1
Channeled = False
CooldownTime = 0
CastTime = 0
TickTime = 0


Url = http://tbc.wowhead.com/spell=6795/growl
Name = Growl
Nameformatted = Growl
Id = 6795
Level = 10
School = Physical
SecondarySchool = Feral Combat
Mana = -1
Range = 5
IsGCD = False
Rank = 1
Channeled = False
CooldownTime = 10000
CastTime = 0
TickTime = 0


Url = http://tbc.wowhead.com/spell=26979/healing-touch
Name = Healing Touch
Nameformatted = HealingTouch
Id = 26979
Level = 69
School = Nature
SecondarySchool = Restoration
Mana = 935
Range = 40
IsGCD = True
Rank = 13
Channeled = False
CooldownTime = 0
CastTime = 3500
TickTime = 0


Url = http://tbc.wowhead.com/spell=26980/regrowth
Name = Regrowth
Nameformatted = Regrowth
Id = 26980
Level = 65
School = Nature
SecondarySchool = Restoration
Mana = 675
Range = 40
IsGCD = True
Rank = 10
Channeled = False
CooldownTime = 0
CastTime = 2000
TickTime = 0


Url = http://tbc.wowhead.com/spell=27006/pounce
Name = Pounce
Nameformatted = Pounce
Id = 27006
Level = 66
School = Physical
SecondarySchool = Feral Combat
Mana = -1
Range = 5
IsGCD = True
Rank = 4
Channeled = False
CooldownTime = 0
CastTime = 0
TickTime = 0


Url = http://tbc.wowhead.com/spell=27008/rip
Name = Rip
Nameformatted = Rip
Id = 27008
Level = 67
School = Physical
SecondarySchool = Feral Combat
Mana = -1
Range = 5
IsGCD = True
Rank = 7
Channeled = False
CooldownTime = 0
CastTime = 0
TickTime = 0


Url = http://tbc.wowhead.com/spell=26983/tranquility
Name = Tranquility
Nameformatted = Tranquility
Id = 26983
Level = 70
School = Nature
SecondarySchool = Restoration
Mana = 1650
Range = 0
IsGCD = True
Rank = 5
Channeled = True
CooldownTime = 600000
CastTime = 8000
TickTime = 0


Url = http://tbc.wowhead.com/spell=26991/gift-of-the-wild
Name = Gift of the Wild
Nameformatted = GiftoftheWild
Id = 26991
Level = 70
School = Nature
SecondarySchool = Restoration
Mana = 1515
Range = 40
IsGCD = True
Rank = 3
Channeled = False
CooldownTime = 0
CastTime = 0
TickTime = 0


Url = http://tbc.wowhead.com/spell=2782/remove-curse
Name = Remove Curse
Nameformatted = RemoveCurse
Id = 2782
Level = 24
School = Arcane
SecondarySchool = Restoration
Mana = -1
Range = 40
IsGCD = True
Rank = 1
Channeled = False
CooldownTime = 0
CastTime = 0
TickTime = 0


Url = http://tbc.wowhead.com/spell=22812/barkskin
Name = Barkskin
Nameformatted = Barkskin
Id = 22812
Level = 44
School = Nature
SecondarySchool = Balance
Mana = -1
Range = 0
IsGCD = True
Rank = 1
Channeled = False
CooldownTime = 1000
CastTime = 0
TickTime = 0


Url = http://tbc.wowhead.com/spell=33983/mangle-cat
Name = Mangle (Cat)
Nameformatted = MangleCat
Id = 33983
Level = 68
School = Physical
SecondarySchool = Feral Combat
Mana = -1
Range = 5
IsGCD = True
Rank = 3
Channeled = False
CooldownTime = 0
CastTime = 0
TickTime = 0


Url = http://tbc.wowhead.com/spell=5229/enrage
Name = Enrage
Nameformatted = Enrage
Id = 5229
Level = 12
School = Physical
SecondarySchool = Feral Combat
Mana = -1
Range = 0
IsGCD = False
Rank = 1
Channeled = False
CooldownTime = 1000
CastTime = 0
TickTime = 0


Url = http://tbc.wowhead.com/spell=26988/moonfire
Name = Moonfire
Nameformatted = Moonfire
Id = 26988
Level = 70
School = Arcane
SecondarySchool = Balance
Mana = 495
Range = 30
IsGCD = True
Rank = 12
Channeled = False
CooldownTime = 0
CastTime = 0
TickTime = 0


Url = http://tbc.wowhead.com/spell=27005/ravage
Name = Ravage
Nameformatted = Ravage
Id = 27005
Level = 66
School = Physical
SecondarySchool = Feral Combat
Mana = -1
Range = 5
IsGCD = True
Rank = 5
Channeled = False
CooldownTime = 0
CastTime = 0
TickTime = 0


Url = http://tbc.wowhead.com/spell=26994/rebirth
Name = Rebirth
Nameformatted = Rebirth
Id = 26994
Level = 69
School = Nature
SecondarySchool = Restoration
Mana = -1
Range = 30
IsGCD = True
Rank = 6
Channeled = False
CooldownTime = 1200000
CastTime = 2000
TickTime = 0


Url = http://tbc.wowhead.com/spell=1066/aquatic-form
Name = Aquatic Form
Nameformatted = AquaticForm
Id = 1066
Level = 16
School = Physical
SecondarySchool = Feral Combat
Mana = -1
Range = 0
IsGCD = True
Rank = 1
Channeled = False
CooldownTime = 0
CastTime = 0
TickTime = 0


Url = http://tbc.wowhead.com/spell=2893/abolish-poison
Name = Abolish Poison
Nameformatted = AbolishPoison
Id = 2893
Level = 26
School = Nature
SecondarySchool = Restoration
Mana = -1
Range = 40
IsGCD = True
Rank = 1
Channeled = False
CooldownTime = 0
CastTime = 0
TickTime = 0


Url = http://tbc.wowhead.com/spell=26990/mark-of-the-wild
Name = Mark of the Wild
Nameformatted = MarkoftheWild
Id = 26990
Level = 70
School = Nature
SecondarySchool = Restoration
Mana = 565
Range = 30
IsGCD = True
Rank = 8
Channeled = False
CooldownTime = 0
CastTime = 0
TickTime = 0


Url = http://tbc.wowhead.com/spell=26998/demoralizing-roar
Name = Demoralizing Roar
Nameformatted = DemoralizingRoar
Id = 26998
Level = 62
School = Physical
SecondarySchool = Feral Combat
Mana = -1
Range = 0
IsGCD = True
Rank = 6
Channeled = False
CooldownTime = 0
CastTime = 0
TickTime = 0


Url = http://tbc.wowhead.com/spell=8983/bash
Name = Bash
Nameformatted = Bash
Id = 8983
Level = 46
School = Physical
SecondarySchool = Feral Combat
Mana = -1
Range = 5
IsGCD = True
Rank = 3
Channeled = False
CooldownTime = 1000
CastTime = 0
TickTime = 0


Url = http://tbc.wowhead.com/spell=9913/prowl
Name = Prowl
Nameformatted = Prowl
Id = 9913
Level = 60
School = Physical
SecondarySchool = Feral Combat
Mana = -1
Range = 0
IsGCD = False
Rank = 3
Channeled = False
CooldownTime = 10000
CastTime = 0
TickTime = 0


Url = http://tbc.wowhead.com/spell=20719/feline-grace
Name = Feline Grace
Nameformatted = FelineGrace
Id = 20719
Level = 40
School = Physical
SecondarySchool = Feral Combat
Mana = -1
Range = 0
IsGCD = False
Rank = 1
Channeled = False
CooldownTime = 0
CastTime = 0
TickTime = 0


Url = http://tbc.wowhead.com/spell=26992/thorns
Name = Thorns
Nameformatted = Thorns
Id = 26992
Level = 64
School = Nature
SecondarySchool = Balance
Mana = 400
Range = 30
IsGCD = True
Rank = 7
Channeled = False
CooldownTime = 0
CastTime = 0
TickTime = 0


Url = http://tbc.wowhead.com/spell=26996/maul
Name = Maul
Nameformatted = Maul
Id = 26996
Level = 67
School = Physical
SecondarySchool = Feral Combat
Mana = -1
Range = 5
IsGCD = False
Rank = 8
Channeled = False
CooldownTime = 0
CastTime = 0
TickTime = 0


Url = http://tbc.wowhead.com/spell=27000/claw
Name = Claw
Nameformatted = Claw
Id = 27000
Level = 67
School = Physical
SecondarySchool = Feral Combat
Mana = -1
Range = 5
IsGCD = True
Rank = 6
Channeled = False
CooldownTime = 0
CastTime = 0
TickTime = 0


Url = http://tbc.wowhead.com/spell=31018/ferocious-bite
Name = Ferocious Bite
Nameformatted = FerociousBite
Id = 31018
Level = 60
School = Physical
SecondarySchool = Feral Combat
Mana = -1
Range = 5
IsGCD = True
Rank = 5
Channeled = False
CooldownTime = 0
CastTime = 0
TickTime = 0


Url = http://tbc.wowhead.com/spell=8946/cure-poison
Name = Cure Poison
Nameformatted = CurePoison
Id = 8946
Level = 14
School = Nature
SecondarySchool = Restoration
Mana = -1
Range = 40
IsGCD = True
Rank = 1
Channeled = False
CooldownTime = 0
CastTime = 0
TickTime = 0


Url = http://tbc.wowhead.com/spell=26989/entangling-roots
Name = Entangling Roots
Nameformatted = EntanglingRoots
Id = 26989
Level = 68
School = Nature
SecondarySchool = Balance
Mana = 160
Range = 30
IsGCD = True
Rank = 7
Channeled = False
CooldownTime = 0
CastTime = 1500
TickTime = 0


Url = http://tbc.wowhead.com/spell=26995/soothe-animal
Name = Soothe Animal
Nameformatted = SootheAnimal
Id = 26995
Level = 70
School = Nature
SecondarySchool = Balance
Mana = 140
Range = 40
IsGCD = True
Rank = 4
Channeled = False
CooldownTime = 0
CastTime = 1500
TickTime = 0


Url = http://tbc.wowhead.com/spell=26999/frenzied-regeneration
Name = Frenzied Regeneration
Nameformatted = FrenziedRegeneration
Id = 26999
Level = 65
School = Physical
SecondarySchool = Feral Combat
Mana = -1
Range = 0
IsGCD = True
Rank = 4
Channeled = False
CooldownTime = 180000
CastTime = 0
TickTime = 0


Url = http://tbc.wowhead.com/spell=5209/challenging-roar
Name = Challenging Roar
Nameformatted = ChallengingRoar
Id = 5209
Level = 28
School = Physical
SecondarySchool = Feral Combat
Mana = -1
Range = 0
IsGCD = True
Rank = 1
Channeled = False
CooldownTime = 600000
CastTime = 0
TickTime = 0


Url = http://tbc.wowhead.com/spell=5225/track-humanoids
Name = Track Humanoids
Nameformatted = TrackHumanoids
Id = 5225
Level = 32
School = Physical
SecondarySchool = Feral Combat
Mana = -1
Range = 0
IsGCD = True
Rank = 1
Channeled = False
CooldownTime = 0
CastTime = 0
TickTime = 0


Url = http://tbc.wowhead.com/spell=33357/dash
Name = Dash
Nameformatted = Dash
Id = 33357
Level = 65
School = Physical
SecondarySchool = Feral Combat
Mana = -1
Range = 0
IsGCD = False
Rank = 3
Channeled = False
CooldownTime = 300000
CastTime = 0
TickTime = 0


Url = http://tbc.wowhead.com/spell=18960/teleport-moonglade
Name = Teleport: Moonglade
Nameformatted = TeleportMoonglade
Id = 18960
Level = 10
School = Arcane
SecondarySchool = Balance
Mana = 120
Range = 0
IsGCD = True
Rank = 1
Channeled = False
CooldownTime = 0
CastTime = 10000
TickTime = 0


Url = http://tbc.wowhead.com/spell=27003/rake
Name = Rake
Nameformatted = Rake
Id = 27003
Level = 64
School = Physical
SecondarySchool = Feral Combat
Mana = -1
Range = 5
IsGCD = True
Rank = 5
Channeled = False
CooldownTime = 0
CastTime = 0
TickTime = 0


Url = http://tbc.wowhead.com/spell=31709/cower
Name = Cower
Nameformatted = Cower
Id = 31709
Level = 60
School = Physical
SecondarySchool = Feral Combat
Mana = -1
Range = 5
IsGCD = True
Rank = 4
Channeled = False
CooldownTime = 10000
CastTime = 0
TickTime = 0


Url = http://tbc.wowhead.com/spell=18658/hibernate
Name = Hibernate
Nameformatted = Hibernate
Id = 18658
Level = 58
School = Nature
SecondarySchool = Balance
Mana = 150
Range = 30
IsGCD = True
Rank = 3
Channeled = False
CooldownTime = 0
CastTime = 1500
TickTime = 0


