Url = http://tbc.wowhead.com/spell=33776/spiritual-attunement
Name = Spiritual Attunement
Nameformatted = SpiritualAttunement
Id = 33776
Level = 66
School = Holy
SecondarySchool = Protection
Mana = -1
Range = 0
IsGCD = False
Rank = 2
Channeled = False
CooldownTime = 0
CastTime = 0
TickTime = 0


Url = http://tbc.wowhead.com/spell=31789/righteous-defense
Name = Righteous Defense
Nameformatted = RighteousDefense
Id = 31789
Level = 14
School = Holy
SecondarySchool = Protection
Mana = -1
Range = 40
IsGCD = True
Rank = 1
Channeled = False
CooldownTime = 15000
CastTime = 0
TickTime = 0


Url = http://tbc.wowhead.com/spell=31892/seal-of-blood
Name = Seal of Blood
Nameformatted = SealofBlood
Id = 31892
Level = 64
School = Holy
SecondarySchool = Retribution
Mana = 210
Range = 0
IsGCD = True
Rank = 1
Channeled = False
CooldownTime = 0
CastTime = 0
TickTime = 0


Url = http://tbc.wowhead.com/spell=348700/seal-of-the-martyr
Name = Seal of the Martyr
Nameformatted = SealoftheMartyr
Id = 348700
Level = 70
School = Holy
SecondarySchool = Retribution
Mana = 210
Range = 0
IsGCD = True
Rank = 1
Channeled = False
CooldownTime = 0
CastTime = 0
TickTime = 0


Url = http://tbc.wowhead.com/spell=27173/consecration
Name = Consecration
Nameformatted = Consecration
Id = 27173
Level = 70
School = Holy
SecondarySchool = Holy
Mana = 660
Range = 0
IsGCD = True
Rank = 6
Channeled = False
CooldownTime = 8000
CastTime = 0
TickTime = 0


Url = http://tbc.wowhead.com/spell=348704/seal-of-vengeance
Name = Seal of Vengeance
Nameformatted = SealofVengeance
Id = 348704
Level = 70
School = Holy
SecondarySchool = Retribution
Mana = 250
Range = 0
IsGCD = True
Rank = 1
Channeled = False
CooldownTime = 0
CastTime = 0
TickTime = 0


Url = http://tbc.wowhead.com/spell=34767/summon-charger
Name = Summon Charger
Nameformatted = SummonCharger
Id = 34767
Level = 60
School = Holy
SecondarySchool = Holy
Mana = 150
Range = 0
IsGCD = True
Rank = 1
Channeled = False
CooldownTime = 0
CastTime = 3000
TickTime = 0


Url = http://tbc.wowhead.com/spell=31884/avenging-wrath
Name = Avenging Wrath
Nameformatted = AvengingWrath
Id = 31884
Level = 70
School = Holy
SecondarySchool = Retribution
Mana = -1
Range = 0
IsGCD = False
Rank = 1
Channeled = False
CooldownTime = 180000
CastTime = 0
TickTime = 0


Url = http://tbc.wowhead.com/spell=20271/judgement
Name = Judgement
Nameformatted = Judgement
Id = 20271
Level = 4
School = Holy
SecondarySchool = Retribution
Mana = -1
Range = 10
IsGCD = False
Rank = 1
Channeled = False
CooldownTime = 10000
CastTime = 0
TickTime = 0


Url = http://tbc.wowhead.com/spell=27155/seal-of-righteousness
Name = Seal of Righteousness
Nameformatted = SealofRighteousness
Id = 27155
Level = 66
School = Holy
SecondarySchool = Holy
Mana = 260
Range = 0
IsGCD = True
Rank = 9
Channeled = False
CooldownTime = 0
CastTime = 0
TickTime = 0


Url = http://tbc.wowhead.com/spell=32223/crusader-aura
Name = Crusader Aura
Nameformatted = CrusaderAura
Id = 32223
Level = 62
School = Holy
SecondarySchool = Retribution
Mana = -1
Range = 0
IsGCD = True
Rank = 1
Channeled = False
CooldownTime = 0
CastTime = 0
TickTime = 0


Url = http://tbc.wowhead.com/spell=27138/exorcism
Name = Exorcism
Nameformatted = Exorcism
Id = 27138
Level = 68
School = Holy
SecondarySchool = Holy
Mana = 340
Range = 30
IsGCD = True
Rank = 7
Channeled = False
CooldownTime = 15000
CastTime = 0
TickTime = 0


Url = http://tbc.wowhead.com/spell=21183/judgement-of-the-crusader
Name = Judgement of the Crusader
Nameformatted = JudgementoftheCrusader
Id = 21183
Level = 6
School = Holy
SecondarySchool = Retribution
Mana = -1
Range = 100
IsGCD = False
Rank = 1
Channeled = False
CooldownTime = 0
CastTime = 0
TickTime = 0


Url = http://tbc.wowhead.com/spell=10308/hammer-of-justice
Name = Hammer of Justice
Nameformatted = HammerofJustice
Id = 10308
Level = 54
School = Holy
SecondarySchool = Protection
Mana = 100
Range = 10
IsGCD = True
Rank = 4
Channeled = False
CooldownTime = 1000
CastTime = 0
TickTime = 0


Url = http://tbc.wowhead.com/spell=25780/righteous-fury
Name = Righteous Fury
Nameformatted = RighteousFury
Id = 25780
Level = 16
School = Holy
SecondarySchool = Protection
Mana = -1
Range = 0
IsGCD = True
Rank = 1
Channeled = False
CooldownTime = 0
CastTime = 0
TickTime = 0


Url = http://tbc.wowhead.com/spell=31895/seal-of-justice
Name = Seal of Justice
Nameformatted = SealofJustice
Id = 31895
Level = 48
School = Holy
SecondarySchool = Protection
Mana = -1
Range = 0
IsGCD = True
Rank = 2
Channeled = False
CooldownTime = 0
CastTime = 0
TickTime = 0


Url = http://tbc.wowhead.com/spell=27180/hammer-of-wrath
Name = Hammer of Wrath
Nameformatted = HammerofWrath
Id = 27180
Level = 68
School = Holy
SecondarySchool = Holy
Mana = 440
Range = 30
IsGCD = False
Rank = 4
Channeled = False
CooldownTime = 6000
CastTime = 500000
TickTime = 0


Url = http://tbc.wowhead.com/spell=25898/greater-blessing-of-kings
Name = Greater Blessing of Kings
Nameformatted = GreaterBlessingofKings
Id = 25898
Level = 60
School = Holy
SecondarySchool = Protection
Mana = -1
Range = 40
IsGCD = True
Rank = 1
Channeled = False
CooldownTime = 0
CastTime = 0
TickTime = 0


Url = http://tbc.wowhead.com/spell=27169/greater-blessing-of-sanctuary
Name = Greater Blessing of Sanctuary
Nameformatted = GreaterBlessingofSanctuary
Id = 27169
Level = 70
School = Holy
SecondarySchool = Protection
Mana = 360
Range = 40
IsGCD = True
Rank = 2
Channeled = False
CooldownTime = 0
CastTime = 0
TickTime = 0


Url = http://tbc.wowhead.com/spell=1038/blessing-of-salvation
Name = Blessing of Salvation
Nameformatted = BlessingofSalvation
Id = 1038
Level = 26
School = Holy
SecondarySchool = Protection
Mana = -1
Range = 30
IsGCD = True
Rank = 1
Channeled = False
CooldownTime = 0
CastTime = 0
TickTime = 0


Url = http://tbc.wowhead.com/spell=19746/concentration-aura
Name = Concentration Aura
Nameformatted = ConcentrationAura
Id = 19746
Level = 22
School = Holy
SecondarySchool = Protection
Mana = -1
Range = 0
IsGCD = True
Rank = 1
Channeled = False
CooldownTime = 0
CastTime = 0
TickTime = 0


Url = http://tbc.wowhead.com/spell=27145/greater-blessing-of-light
Name = Greater Blessing of Light
Nameformatted = GreaterBlessingofLight
Id = 27145
Level = 69
School = Holy
SecondarySchool = Holy
Mana = 360
Range = 40
IsGCD = True
Rank = 2
Channeled = False
CooldownTime = 0
CastTime = 0
TickTime = 0


Url = http://tbc.wowhead.com/spell=27150/retribution-aura
Name = Retribution Aura
Nameformatted = RetributionAura
Id = 27150
Level = 66
School = Holy
SecondarySchool = Retribution
Mana = -1
Range = 0
IsGCD = True
Rank = 6
Channeled = False
CooldownTime = 0
CastTime = 0
TickTime = 0


Url = http://tbc.wowhead.com/spell=1044/blessing-of-freedom
Name = Blessing of Freedom
Nameformatted = BlessingofFreedom
Id = 1044
Level = 18
School = Holy
SecondarySchool = Protection
Mana = -1
Range = 30
IsGCD = True
Rank = 1
Channeled = False
CooldownTime = 25000
CastTime = 0
TickTime = 0


Url = http://tbc.wowhead.com/spell=19752/divine-intervention
Name = Divine Intervention
Nameformatted = DivineIntervention
Id = 19752
Level = 30
School = Holy
SecondarySchool = Protection
Mana = -1
Range = 40
IsGCD = True
Rank = 1
Channeled = False
CooldownTime = 1000
CastTime = 0
TickTime = 0


Url = http://tbc.wowhead.com/spell=27143/greater-blessing-of-wisdom
Name = Greater Blessing of Wisdom
Nameformatted = GreaterBlessingofWisdom
Id = 27143
Level = 65
School = Holy
SecondarySchool = Holy
Mana = 310
Range = 40
IsGCD = True
Rank = 3
Channeled = False
CooldownTime = 0
CastTime = 0
TickTime = 0


Url = http://tbc.wowhead.com/spell=27148/blessing-of-sacrifice
Name = Blessing of Sacrifice
Nameformatted = BlessingofSacrifice
Id = 27148
Level = 70
School = Holy
SecondarySchool = Protection
Mana = 135
Range = 30
IsGCD = True
Rank = 4
Channeled = False
CooldownTime = 30000
CastTime = 0
TickTime = 0


Url = http://tbc.wowhead.com/spell=34769/summon-warhorse
Name = Summon Warhorse
Nameformatted = SummonWarhorse
Id = 34769
Level = 30
School = Holy
SecondarySchool = Holy
Mana = 100
Range = 0
IsGCD = True
Rank = 1
Channeled = False
CooldownTime = 0
CastTime = 3000
TickTime = 0


Url = http://tbc.wowhead.com/spell=5627/turn-undead
Name = Turn Undead
Nameformatted = TurnUndead
Id = 5627
Level = 38
School = Holy
SecondarySchool = Holy
Mana = 50
Range = 20
IsGCD = True
Rank = 2
Channeled = False
CooldownTime = 30000
CastTime = 1500
TickTime = 0


Url = http://tbc.wowhead.com/spell=27160/seal-of-light
Name = Seal of Light
Nameformatted = SealofLight
Id = 27160
Level = 69
School = Holy
SecondarySchool = Holy
Mana = 280
Range = 0
IsGCD = True
Rank = 5
Channeled = False
CooldownTime = 0
CastTime = 0
TickTime = 0


Url = http://tbc.wowhead.com/spell=1020/divine-shield
Name = Divine Shield
Nameformatted = DivineShield
Id = 1020
Level = 50
School = Holy
SecondarySchool = Protection
Mana = 110
Range = 0
IsGCD = True
Rank = 2
Channeled = False
CooldownTime = 300000
CastTime = 0
TickTime = 0


Url = http://tbc.wowhead.com/spell=27141/greater-blessing-of-might
Name = Greater Blessing of Might
Nameformatted = GreaterBlessingofMight
Id = 27141
Level = 70
School = Holy
SecondarySchool = Retribution
Mana = 295
Range = 40
IsGCD = True
Rank = 3
Channeled = False
CooldownTime = 0
CastTime = 0
TickTime = 0


Url = http://tbc.wowhead.com/spell=27149/devotion-aura
Name = Devotion Aura
Nameformatted = DevotionAura
Id = 27149
Level = 70
School = Holy
SecondarySchool = Protection
Mana = -1
Range = 0
IsGCD = True
Rank = 8
Channeled = False
CooldownTime = 0
CastTime = 0
TickTime = 0


Url = http://tbc.wowhead.com/spell=10278/blessing-of-protection
Name = Blessing of Protection
Nameformatted = BlessingofProtection
Id = 10278
Level = 38
School = Holy
SecondarySchool = Protection
Mana = -1
Range = 30
IsGCD = True
Rank = 3
Channeled = False
CooldownTime = 300000
CastTime = 0
TickTime = 0


Url = http://tbc.wowhead.com/spell=25895/greater-blessing-of-salvation
Name = Greater Blessing of Salvation
Nameformatted = GreaterBlessingofSalvation
Id = 25895
Level = 60
School = Holy
SecondarySchool = Protection
Mana = -1
Range = 40
IsGCD = True
Rank = 1
Channeled = False
CooldownTime = 0
CastTime = 0
TickTime = 0


Url = http://tbc.wowhead.com/spell=27136/holy-light
Name = Holy Light
Nameformatted = HolyLight
Id = 27136
Level = 70
School = Holy
SecondarySchool = Holy
Mana = 840
Range = 40
IsGCD = True
Rank = 11
Channeled = False
CooldownTime = 0
CastTime = 2500
TickTime = 0


Url = http://tbc.wowhead.com/spell=27139/holy-wrath
Name = Holy Wrath
Nameformatted = HolyWrath
Id = 27139
Level = 69
School = Holy
SecondarySchool = Holy
Mana = 825
Range = 0
IsGCD = True
Rank = 3
Channeled = False
CooldownTime = 1000
CastTime = 2000
TickTime = 0


Url = http://tbc.wowhead.com/spell=27142/blessing-of-wisdom
Name = Blessing of Wisdom
Nameformatted = BlessingofWisdom
Id = 27142
Level = 65
School = Holy
SecondarySchool = Holy
Mana = 150
Range = 30
IsGCD = True
Rank = 7
Channeled = False
CooldownTime = 0
CastTime = 0
TickTime = 0


Url = http://tbc.wowhead.com/spell=4987/cleanse
Name = Cleanse
Nameformatted = Cleanse
Id = 4987
Level = 42
School = Holy
SecondarySchool = Holy
Mana = -1
Range = 40
IsGCD = True
Rank = 1
Channeled = False
CooldownTime = 0
CastTime = 0
TickTime = 0


Url = http://tbc.wowhead.com/spell=27140/blessing-of-might
Name = Blessing of Might
Nameformatted = BlessingofMight
Id = 27140
Level = 70
School = Holy
SecondarySchool = Retribution
Mana = 145
Range = 30
IsGCD = True
Rank = 8
Channeled = False
CooldownTime = 0
CastTime = 0
TickTime = 0


Url = http://tbc.wowhead.com/spell=27151/shadow-resistance-aura
Name = Shadow Resistance Aura
Nameformatted = ShadowResistanceAura
Id = 27151
Level = 63
School = Holy
SecondarySchool = Protection
Mana = -1
Range = 0
IsGCD = True
Rank = 4
Channeled = False
CooldownTime = 0
CastTime = 0
TickTime = 0


Url = http://tbc.wowhead.com/spell=27152/frost-resistance-aura
Name = Frost Resistance Aura
Nameformatted = FrostResistanceAura
Id = 27152
Level = 68
School = Holy
SecondarySchool = Protection
Mana = -1
Range = 0
IsGCD = True
Rank = 4
Channeled = False
CooldownTime = 0
CastTime = 0
TickTime = 0


Url = http://tbc.wowhead.com/spell=27153/fire-resistance-aura
Name = Fire Resistance Aura
Nameformatted = FireResistanceAura
Id = 27153
Level = 70
School = Holy
SecondarySchool = Protection
Mana = -1
Range = 0
IsGCD = True
Rank = 4
Channeled = False
CooldownTime = 0
CastTime = 0
TickTime = 0


Url = http://tbc.wowhead.com/spell=27158/seal-of-the-crusader
Name = Seal of the Crusader
Nameformatted = SealoftheCrusader
Id = 27158
Level = 61
School = Holy
SecondarySchool = Retribution
Mana = 210
Range = 0
IsGCD = True
Rank = 7
Channeled = False
CooldownTime = 0
CastTime = 0
TickTime = 0


Url = http://tbc.wowhead.com/spell=27144/blessing-of-light
Name = Blessing of Light
Nameformatted = BlessingofLight
Id = 27144
Level = 69
School = Holy
SecondarySchool = Holy
Mana = 180
Range = 30
IsGCD = True
Rank = 4
Channeled = False
CooldownTime = 0
CastTime = 0
TickTime = 0


Url = http://tbc.wowhead.com/spell=10326/turn-evil
Name = Turn Evil
Nameformatted = TurnEvil
Id = 10326
Level = 52
School = Holy
SecondarySchool = Holy
Mana = 75
Range = 20
IsGCD = True
Rank = 1
Channeled = False
CooldownTime = 30000
CastTime = 1500
TickTime = 0


Url = http://tbc.wowhead.com/spell=27137/flash-of-light
Name = Flash of Light
Nameformatted = FlashofLight
Id = 27137
Level = 66
School = Holy
SecondarySchool = Holy
Mana = 180
Range = 40
IsGCD = True
Rank = 7
Channeled = False
CooldownTime = 0
CastTime = 1500
TickTime = 0


Url = http://tbc.wowhead.com/spell=27154/lay-on-hands
Name = Lay on Hands
Nameformatted = LayonHands
Id = 27154
Level = 69
School = Holy
SecondarySchool = Holy
Mana = -1
Range = 40
IsGCD = True
Rank = 4
Channeled = False
CooldownTime = 1000
CastTime = 0
TickTime = 0


Url = http://tbc.wowhead.com/spell=5573/divine-protection
Name = Divine Protection
Nameformatted = DivineProtection
Id = 5573
Level = 18
School = Holy
SecondarySchool = Protection
Mana = 35
Range = 0
IsGCD = True
Rank = 2
Channeled = False
CooldownTime = 300000
CastTime = 0
TickTime = 0


Url = http://tbc.wowhead.com/spell=20773/redemption
Name = Redemption
Nameformatted = Redemption
Id = 20773
Level = 60
School = Holy
SecondarySchool = Holy
Mana = -1
Range = 30
IsGCD = True
Rank = 5
Channeled = False
CooldownTime = 0
CastTime = 10000
TickTime = 0


Url = http://tbc.wowhead.com/spell=27166/seal-of-wisdom
Name = Seal of Wisdom
Nameformatted = SealofWisdom
Id = 27166
Level = 67
School = Holy
SecondarySchool = Holy
Mana = 270
Range = 0
IsGCD = True
Rank = 4
Channeled = False
CooldownTime = 0
CastTime = 0
TickTime = 0


Url = http://tbc.wowhead.com/spell=1152/purify
Name = Purify
Nameformatted = Purify
Id = 1152
Level = 8
School = Holy
SecondarySchool = Holy
Mana = -1
Range = 40
IsGCD = True
Rank = 1
Channeled = False
CooldownTime = 0
CastTime = 0
TickTime = 0


Url = http://tbc.wowhead.com/spell=5502/sense-undead
Name = Sense Undead
Nameformatted = SenseUndead
Id = 5502
Level = 20
School = Holy
SecondarySchool = Holy
Mana = -1
Range = 0
IsGCD = True
Rank = 1
Channeled = False
CooldownTime = 0
CastTime = 0
TickTime = 0


