using System;
using TBCsimulation.Simulation;

namespace TBCsimulation.Data.Racial.Spell
{
    public partial class BerserkingSpell : SpellsInfo.Spell
    {
        private float baseHasteValue = 10;
        private int totalDuration = 10;

        private void Init(FightSim sim)
        {
            IsGCD = false;
            ActionDelegate = delegate
            {
                //Spell haste = 10% + 33% * ((total health - current health) / total health), but not higher than 30%
                var haste = Math.Min(30f,
                    baseHasteValue + (float) sim.Player.GetCurrentHealth() / (float) sim.Player.GetMaxHealth() * 100f /
                    3f);
                var buff = new BuffSim(Name, Id);
                buff.DurationTotal = totalDuration * 1000;
                buff.DurationRemaining = buff.DurationTotal;
                buff.TickActionDelegate = simBuff =>
                {
                    simBuff.LoggerSim.TickDebuff(simBuff.Time, buff);

                    simBuff.Player.FlatHasteMod -= haste;

                    simBuff.LoggerSim.FadeDebuff(simBuff.Time, buff);
                    simBuff.RemoveDebuff(Id);
                };

                var hit = sim.Buff(buff, School, Id, false);
                if (hit) sim.Player.FlatHasteMod += haste;
            };
        }
    }
}