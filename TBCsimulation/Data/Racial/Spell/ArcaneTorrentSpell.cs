using System;
using TBCsimulation.Data.SpellsInfo;
using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;

namespace TBCsimulation.Data.Racial.Spell
{
    public partial class ArcaneTorrentSpell : SpellsInfo.Spell
    {
        private int energyGain = 10;
        private int manaGain = 161;

        private void Init(FightSim sim)
        {
            ActionDelegate = delegate
            {
                var stacks = 0;
                if (sim.HasBuff(SpellId.Racial_ManaTap)) stacks = sim.GetBuff(SpellId.Racial_ManaTap).Stacks;


                switch (sim.Player.GetResource())
                {
                    case EnumSim.ResourceType.Mana:
                        sim.ManaGained(Id, manaGain * stacks);
                        break;
                    case EnumSim.ResourceType.Energy:
                        throw new NotImplementedException();
                    case EnumSim.ResourceType.Rage:
                        throw new NotSupportedException();
                    case EnumSim.ResourceType.Focus:
                        throw new NotSupportedException();
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            };
        }
    }
}