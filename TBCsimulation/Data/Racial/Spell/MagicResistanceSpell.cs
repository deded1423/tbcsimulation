using TBCsimulation.Simulation;

namespace TBCsimulation.Data.Racial.Spell
{
    public partial class MagicResistanceSpell : SpellsInfo.Spell
    {
        private int resistanceValue = 10;

        private void Init(FightSim sim)
        {
            ActionDelegate = delegate
            {
                sim.Player.ResistanceArcaneMod += resistanceValue;
                sim.Player.ResistanceFireMod += resistanceValue;
                sim.Player.ResistanceFrostMod += resistanceValue;
                sim.Player.ResistanceHolyMod += resistanceValue;
                sim.Player.ResistanceNatureMod += resistanceValue;
                sim.Player.ResistanceShadowMod += resistanceValue;
            };
        }
    }
}