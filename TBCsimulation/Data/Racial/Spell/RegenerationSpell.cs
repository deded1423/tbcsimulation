using TBCsimulation.Simulation;

namespace TBCsimulation.Data.Racial.Spell
{
    public partial class RegenerationSpell : SpellsInfo.Spell
    {
        private float regenValue = 0.1f;

        private void Init(FightSim sim)
        {
            ActionDelegate = delegate { sim.Player.CombatHealthRegenPercentageMod += regenValue; };
        }
    }
}