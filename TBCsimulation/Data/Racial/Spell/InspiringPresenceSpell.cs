using TBCsimulation.Simulation;

namespace TBCsimulation.Data.Racial.Spell
{
    public partial class InspiringPresenceSpell : SpellsInfo.Spell
    {
        private void Init(FightSim sim)
        {
            ActionDelegate = delegate { sim.Player.FlatSpellHitMod += 1; };
        }
    }
}