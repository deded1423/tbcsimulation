using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Racial.Spell
{
    public partial class WilloftheForsakenSpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public WilloftheForsakenSpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=7744/will-of-the-forsaken
            Name = "Will of the Forsaken";
            Id = 7744;
            Level = 0;
            Rank = 1;
            Range = 0;

            Mana = 0;

            IsGCD = true;
            Channeled = false;
            CastTime = 0;
            CooldownTime = 120000;

            School = EnumSim.SchoolType.Racial;
            SecondarySchool = EnumSim.SecondarySchoolType.Racial;
            Init(sim);
        }
    }
}