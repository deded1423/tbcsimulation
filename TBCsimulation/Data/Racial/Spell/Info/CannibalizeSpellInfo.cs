using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Racial.Spell
{
    public partial class CannibalizeSpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public CannibalizeSpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=20577/cannibalize
            Name = "Cannibalize";
            Id = 20577;
            Level = 0;
            Rank = 1;
            Range = 5;

            Mana = 0;

            IsGCD = true;
            Channeled = false;
            CastTime = 0;
            CooldownTime = 120000;

            School = EnumSim.SchoolType.Racial;
            SecondarySchool = EnumSim.SecondarySchoolType.Racial;
            Init(sim);
        }
    }
}