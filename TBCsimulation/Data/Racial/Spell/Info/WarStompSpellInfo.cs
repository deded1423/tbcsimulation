using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Racial.Spell
{
    public partial class WarStompSpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public WarStompSpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=20549/war-stomp
            Name = "War Stomp";
            Id = 20549;
            Level = 0;
            Rank = 1;
            Range = 0;

            Mana = 0;

            IsGCD = false;
            Channeled = false;
            CastTime = 500000;
            CooldownTime = 120000;

            School = EnumSim.SchoolType.Racial;
            SecondarySchool = EnumSim.SecondarySchoolType.Racial;
            Init(sim);
        }
    }
}