using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Racial.Spell
{
    public partial class EscapeArtistSpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public EscapeArtistSpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=20589/escape-artist
            Name = "Escape Artist";
            Id = 20589;
            Level = 0;
            Rank = 1;
            Range = 0;

            Mana = 0;

            IsGCD = true;
            Channeled = false;
            CastTime = 0;
            CooldownTime = 105000;

            School = EnumSim.SchoolType.Racial;
            SecondarySchool = EnumSim.SecondarySchoolType.Racial;
            Init(sim);
        }
    }
}