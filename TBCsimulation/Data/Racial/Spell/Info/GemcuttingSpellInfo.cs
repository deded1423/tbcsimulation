using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Racial.Spell
{
    public partial class GemcuttingSpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public GemcuttingSpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=28875/gemcutting
            Name = "Gemcutting";
            Id = 28875;
            Level = 0;
            Rank = 1;
            Range = 0;

            Mana = 0;

            IsGCD = false;
            Channeled = false;
            CastTime = 0;
            CooldownTime = 0;

            School = EnumSim.SchoolType.Racial;
            SecondarySchool = EnumSim.SecondarySchoolType.Racial;
            Init(sim);
        }
    }
}