using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Racial.Spell
{
    public partial class StoneformSpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public StoneformSpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=20594/stoneform
            Name = "Stoneform";
            Id = 20594;
            Level = 0;
            Rank = 1;
            Range = 0;

            Mana = 0;

            IsGCD = true;
            Channeled = false;
            CastTime = 0;
            CooldownTime = 180000;

            School = EnumSim.SchoolType.Racial;
            SecondarySchool = EnumSim.SecondarySchoolType.Racial;
            Init(sim);
        }
    }
}