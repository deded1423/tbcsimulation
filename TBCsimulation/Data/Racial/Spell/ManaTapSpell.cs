using System;
using TBCsimulation.Data.SpellsInfo;
using TBCsimulation.Simulation;

namespace TBCsimulation.Data.Racial.Spell
{
    public partial class ManaTapSpell : SpellsInfo.Spell
    {
        private int totalDuration = 30;

        private void Init(FightSim sim)
        {
            ActionDelegate = delegate
            {
                var buff = new BuffSim(Name, Id);
                buff.DurationTotal = totalDuration * 1000;
                buff.DurationRemaining = buff.DurationTotal;

                var stacks = 0;
                if (sim.HasBuff(SpellId.Racial_ManaTap))
                    stacks = Math.Min(sim.GetBuff(SpellId.Racial_ManaTap).Stacks, 3);

                buff.Stacks = stacks;

                buff.TickActionDelegate = simBuff =>
                {
                    simBuff.LoggerSim.TickDebuff(simBuff.Time, buff);

                    simBuff.LoggerSim.FadeDebuff(simBuff.Time, buff);
                    simBuff.RemoveDebuff(Id);
                };

                var hit = sim.Buff(buff, School, Id, false);
            };
        }
    }
}