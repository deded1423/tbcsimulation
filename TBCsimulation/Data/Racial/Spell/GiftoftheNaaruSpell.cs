using TBCsimulation.Simulation;

namespace TBCsimulation.Data.Racial.Spell
{
    public partial class GiftoftheNaaruSpell : SpellsInfo.Spell
    {
        private int healValue = 1085;
        private int totalDuration = 15;

        private void Init(FightSim sim)
        {
            ActionDelegate = delegate
            {
                var health = healValue / 5;
                var buff = new BuffSim(Name, Id);
                buff.DurationTotal = totalDuration * 1000;
                buff.DurationRemaining = buff.DurationTotal;
                buff.TickInterval = 3000;
                buff.TickActionDelegate = simBuff =>
                {
                    simBuff.LoggerSim.TickDebuff(simBuff.Time, buff);

                    simBuff.Heal(Id, health);

                    if (buff.DurationRemaining > 0)
                    {
                        simBuff.EnqueueHeap(new HeapEventSim("Tick " + Name, buff.TickInterval, buff));
                    }
                    else
                    {
                        simBuff.LoggerSim.FadeDebuff(simBuff.Time, buff);
                        simBuff.RemoveDebuff(Id);
                    }
                };

                var hit = sim.Buff(buff, School, Id, false);
            };
        }
    }
}