using TBCsimulation.Simulation;

namespace TBCsimulation.Data.Racial.Spell
{
    public partial class FrostResistanceSpell : SpellsInfo.Spell
    {
        private int resistanceValue = 10;

        private void Init(FightSim sim)
        {
            ActionDelegate = delegate { sim.Player.ResistanceFrostMod += resistanceValue; };
        }
    }
}