using TBCsimulation.Simulation;

namespace TBCsimulation.Data.Racial.Spell
{
    public partial class BloodFurySpell : SpellsInfo.Spell
    {
        private int apValue = 282;
        private int spellValue = 143;
        private int totalDuration = 15;

        private void Init(FightSim sim)
        {
            IsGCD = false;
            ActionDelegate = delegate
            {
                var buff = new BuffSim(Name, Id);
                buff.DurationTotal = totalDuration * 1000;
                buff.DurationRemaining = buff.DurationTotal;
                buff.TickActionDelegate = simBuff =>
                {
                    simBuff.LoggerSim.TickDebuff(simBuff.Time, buff);

                    simBuff.Player.AttackPowerMod -= apValue;
                    simBuff.Player.RangedAttackPowerMod -= apValue;
                    simBuff.Player.SpellDamage -= spellValue;

                    simBuff.LoggerSim.FadeDebuff(simBuff.Time, buff);
                    simBuff.RemoveDebuff(Id);
                };

                var hit = sim.Buff(buff, School, Id, false);

                if (hit)
                {
                    sim.Player.AttackPowerMod += apValue;
                    sim.Player.RangedAttackPowerMod += apValue;
                    sim.Player.SpellDamage += spellValue;
                }
            };
        }
    }
}