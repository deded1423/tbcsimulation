using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Druid.Spell
{
    public partial class SootheAnimalSpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public SootheAnimalSpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=26995/soothe-animal
            Name = "Soothe Animal";
            Id = 26995;
            Level = 70;
            Rank = 4;
            Range = 40;

            Mana = 140;

            IsGCD = true;
            Channeled = false;
            CastTime = 1500;
            CooldownTime = 0;

            School = EnumSim.SchoolType.Nature;
            SecondarySchool = EnumSim.SecondarySchoolType.Druid_Balance;
            Init(sim);
        }
    }
}