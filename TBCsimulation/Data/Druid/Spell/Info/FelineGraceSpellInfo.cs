using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Druid.Spell
{
    public partial class FelineGraceSpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public FelineGraceSpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=20719/feline-grace
            Name = "Feline Grace";
            Id = 20719;
            Level = 40;
            Rank = 1;
            Range = 0;

            Mana = -1;

            IsGCD = false;
            Channeled = false;
            CastTime = 0;
            CooldownTime = 0;

            School = EnumSim.SchoolType.Physical;
            SecondarySchool = EnumSim.SecondarySchoolType.Druid_FeralCombat;
            Init(sim);
        }
    }
}