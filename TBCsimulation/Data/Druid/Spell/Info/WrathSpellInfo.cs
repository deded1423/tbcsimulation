using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Druid.Spell
{
    public partial class WrathSpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public WrathSpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=26985/wrath
            Name = "Wrath";
            Id = 26985;
            Level = 69;
            Rank = 10;
            Range = 30;

            Mana = 255;

            IsGCD = true;
            Channeled = false;
            CastTime = 2000;
            CooldownTime = 0;

            School = EnumSim.SchoolType.Nature;
            SecondarySchool = EnumSim.SecondarySchoolType.Druid_Balance;
            Init(sim);
        }
    }
}