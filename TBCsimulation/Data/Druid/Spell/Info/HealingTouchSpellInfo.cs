using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Druid.Spell
{
    public partial class HealingTouchSpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public HealingTouchSpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=26979/healing-touch
            Name = "Healing Touch";
            Id = 26979;
            Level = 69;
            Rank = 13;
            Range = 40;

            Mana = 935;

            IsGCD = true;
            Channeled = false;
            CastTime = 3500;
            CooldownTime = 0;

            School = EnumSim.SchoolType.Nature;
            SecondarySchool = EnumSim.SecondarySchoolType.Druid_Restoration;
            Init(sim);
        }
    }
}