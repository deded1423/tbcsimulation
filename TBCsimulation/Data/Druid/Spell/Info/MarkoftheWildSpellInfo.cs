using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Druid.Spell
{
    public partial class MarkoftheWildSpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public MarkoftheWildSpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=26990/mark-of-the-wild
            Name = "Mark of the Wild";
            Id = 26990;
            Level = 70;
            Rank = 8;
            Range = 30;

            Mana = 565;

            IsGCD = true;
            Channeled = false;
            CastTime = 0;
            CooldownTime = 0;

            School = EnumSim.SchoolType.Nature;
            SecondarySchool = EnumSim.SecondarySchoolType.Druid_Restoration;
            Init(sim);
        }
    }
}