using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Druid.Spell
{
    public partial class SwiftFlightFormSpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public SwiftFlightFormSpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=40120/swift-flight-form
            Name = "Swift Flight Form";
            Id = 40120;
            Level = 70;
            Rank = 1;
            Range = 0;

            Mana = -1;

            IsGCD = true;
            Channeled = false;
            CastTime = 0;
            CooldownTime = 0;

            School = EnumSim.SchoolType.Physical;
            SecondarySchool = EnumSim.SecondarySchoolType.Druid_FeralCombat;
            Init(sim);
        }
    }
}