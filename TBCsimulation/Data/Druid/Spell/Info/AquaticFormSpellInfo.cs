using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Druid.Spell
{
    public partial class AquaticFormSpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public AquaticFormSpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=1066/aquatic-form
            Name = "Aquatic Form";
            Id = 1066;
            Level = 16;
            Rank = 1;
            Range = 0;

            Mana = -1;

            IsGCD = true;
            Channeled = false;
            CastTime = 0;
            CooldownTime = 0;

            School = EnumSim.SchoolType.Physical;
            SecondarySchool = EnumSim.SecondarySchoolType.Druid_FeralCombat;
            Init(sim);
        }
    }
}