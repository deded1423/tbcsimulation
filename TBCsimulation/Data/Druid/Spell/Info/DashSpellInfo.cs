using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Druid.Spell
{
    public partial class DashSpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public DashSpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=33357/dash
            Name = "Dash";
            Id = 33357;
            Level = 65;
            Rank = 3;
            Range = 0;

            Mana = -1;

            IsGCD = false;
            Channeled = false;
            CastTime = 0;
            CooldownTime = 300000;

            School = EnumSim.SchoolType.Physical;
            SecondarySchool = EnumSim.SecondarySchoolType.Druid_FeralCombat;
            Init(sim);
        }
    }
}