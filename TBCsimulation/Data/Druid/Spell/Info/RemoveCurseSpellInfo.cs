using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Druid.Spell
{
    public partial class RemoveCurseSpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public RemoveCurseSpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=2782/remove-curse
            Name = "Remove Curse";
            Id = 2782;
            Level = 24;
            Rank = 1;
            Range = 40;

            Mana = -1;

            IsGCD = true;
            Channeled = false;
            CastTime = 0;
            CooldownTime = 0;

            School = EnumSim.SchoolType.Arcane;
            SecondarySchool = EnumSim.SecondarySchoolType.Druid_Restoration;
            Init(sim);
        }
    }
}