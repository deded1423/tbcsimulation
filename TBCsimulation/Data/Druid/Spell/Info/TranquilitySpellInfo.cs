using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Druid.Spell
{
    public partial class TranquilitySpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public TranquilitySpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=26983/tranquility
            Name = "Tranquility";
            Id = 26983;
            Level = 70;
            Rank = 5;
            Range = 0;

            Mana = 1650;

            IsGCD = true;
            Channeled = true;
            CastTime = 8000;
            CooldownTime = 600000;

            School = EnumSim.SchoolType.Nature;
            SecondarySchool = EnumSim.SecondarySchoolType.Druid_Restoration;
            Init(sim);
        }
    }
}