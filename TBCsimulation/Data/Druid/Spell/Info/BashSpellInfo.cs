using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Druid.Spell
{
    public partial class BashSpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public BashSpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=8983/bash
            Name = "Bash";
            Id = 8983;
            Level = 46;
            Rank = 3;
            Range = 5;

            Mana = -1;

            IsGCD = true;
            Channeled = false;
            CastTime = 0;
            CooldownTime = 1000;

            School = EnumSim.SchoolType.Physical;
            SecondarySchool = EnumSim.SecondarySchoolType.Druid_FeralCombat;
            Init(sim);
        }
    }
}