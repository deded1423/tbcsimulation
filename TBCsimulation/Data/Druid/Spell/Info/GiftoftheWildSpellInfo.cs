using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Druid.Spell
{
    public partial class GiftoftheWildSpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public GiftoftheWildSpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=26991/gift-of-the-wild
            Name = "Gift of the Wild";
            Id = 26991;
            Level = 70;
            Rank = 3;
            Range = 40;

            Mana = 1515;

            IsGCD = true;
            Channeled = false;
            CastTime = 0;
            CooldownTime = 0;

            School = EnumSim.SchoolType.Nature;
            SecondarySchool = EnumSim.SecondarySchoolType.Druid_Restoration;
            Init(sim);
        }
    }
}