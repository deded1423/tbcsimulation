using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Druid.Spell
{
    public partial class TeleportMoongladeSpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public TeleportMoongladeSpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=18960/teleport-moonglade
            Name = "Teleport: Moonglade";
            Id = 18960;
            Level = 10;
            Rank = 1;
            Range = 0;

            Mana = 120;

            IsGCD = true;
            Channeled = false;
            CastTime = 10000;
            CooldownTime = 0;

            School = EnumSim.SchoolType.Arcane;
            SecondarySchool = EnumSim.SecondarySchoolType.Druid_Balance;
            Init(sim);
        }
    }
}