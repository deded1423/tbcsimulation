using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Druid.Spell
{
    public partial class HurricaneSpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public HurricaneSpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=27012/hurricane
            Name = "Hurricane";
            Id = 27012;
            Level = 70;
            Rank = 4;
            Range = 30;

            Mana = 1905;

            IsGCD = true;
            Channeled = true;
            CastTime = 10000;
            CooldownTime = 1000;

            School = EnumSim.SchoolType.Nature;
            SecondarySchool = EnumSim.SecondarySchoolType.Druid_Balance;
            Init(sim);
        }
    }
}