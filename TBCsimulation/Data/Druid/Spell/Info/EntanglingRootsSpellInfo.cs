using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Druid.Spell
{
    public partial class EntanglingRootsSpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public EntanglingRootsSpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=26989/entangling-roots
            Name = "Entangling Roots";
            Id = 26989;
            Level = 68;
            Rank = 7;
            Range = 30;

            Mana = 160;

            IsGCD = true;
            Channeled = false;
            CastTime = 1500;
            CooldownTime = 0;

            School = EnumSim.SchoolType.Nature;
            SecondarySchool = EnumSim.SecondarySchoolType.Druid_Balance;
            Init(sim);
        }
    }
}