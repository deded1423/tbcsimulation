using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Druid.Spell
{
    public partial class StarfireSpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public StarfireSpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=26986/starfire
            Name = "Starfire";
            Id = 26986;
            Level = 67;
            Rank = 8;
            Range = 30;

            Mana = 370;

            IsGCD = true;
            Channeled = false;
            CastTime = 3500;
            CooldownTime = 0;

            School = EnumSim.SchoolType.Arcane;
            SecondarySchool = EnumSim.SecondarySchoolType.Druid_Balance;
            Init(sim);
        }
    }
}