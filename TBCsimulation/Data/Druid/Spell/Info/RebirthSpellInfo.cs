using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Druid.Spell
{
    public partial class RebirthSpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public RebirthSpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=26994/rebirth
            Name = "Rebirth";
            Id = 26994;
            Level = 69;
            Rank = 6;
            Range = 30;

            Mana = -1;

            IsGCD = true;
            Channeled = false;
            CastTime = 2000;
            CooldownTime = 1200000;

            School = EnumSim.SchoolType.Nature;
            SecondarySchool = EnumSim.SecondarySchoolType.Druid_Restoration;
            Init(sim);
        }
    }
}