using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Druid.Spell
{
    public partial class LifebloomSpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public LifebloomSpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=33763/lifebloom
            Name = "Lifebloom";
            Id = 33763;
            Level = 64;
            Rank = 1;
            Range = 40;

            Mana = 220;

            IsGCD = true;
            Channeled = false;
            CastTime = 0;
            CooldownTime = 0;

            School = EnumSim.SchoolType.Nature;
            SecondarySchool = EnumSim.SecondarySchoolType.Druid_Restoration;
            Init(sim);
        }
    }
}