using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Druid.Spell
{
    public partial class MoonfireSpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public MoonfireSpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=26988/moonfire
            Name = "Moonfire";
            Id = 26988;
            Level = 70;
            Rank = 12;
            Range = 30;

            Mana = 495;

            IsGCD = true;
            Channeled = false;
            CastTime = 0;
            CooldownTime = 0;

            School = EnumSim.SchoolType.Arcane;
            SecondarySchool = EnumSim.SecondarySchoolType.Druid_Balance;
            Init(sim);
        }
    }
}