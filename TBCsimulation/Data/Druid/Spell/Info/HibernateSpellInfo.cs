using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Druid.Spell
{
    public partial class HibernateSpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public HibernateSpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=18658/hibernate
            Name = "Hibernate";
            Id = 18658;
            Level = 58;
            Rank = 3;
            Range = 30;

            Mana = 150;

            IsGCD = true;
            Channeled = false;
            CastTime = 1500;
            CooldownTime = 0;

            School = EnumSim.SchoolType.Nature;
            SecondarySchool = EnumSim.SecondarySchoolType.Druid_Balance;
            Init(sim);
        }
    }
}