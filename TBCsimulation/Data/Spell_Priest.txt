Url = http://tbc.wowhead.com/spell=32548/symbol-of-hope
Name = Symbol of Hope
Nameformatted = SymbolofHope
Id = 32548
Level = 10
School = Holy
SecondarySchool = Discipline
Mana = 15
Range = 0
IsGCD = True
Rank = 1
Channeled = False
CooldownTime = 300000
CastTime = 0
TickTime = 0


Url = http://tbc.wowhead.com/spell=6346/fear-ward
Name = Fear Ward
Nameformatted = FearWard
Id = 6346
Level = 20
School = Holy
SecondarySchool = Discipline
Mana = -1
Range = 30
IsGCD = True
Rank = 1
Channeled = False
CooldownTime = 180000
CastTime = 0
TickTime = 0


Url = http://tbc.wowhead.com/spell=33076/prayer-of-mending
Name = Prayer of Mending
Nameformatted = PrayerofMending
Id = 33076
Level = 68
School = Holy
SecondarySchool = Holy
Mana = 390
Range = 40
IsGCD = True
Rank = 1
Channeled = False
CooldownTime = 10000
CastTime = 0
TickTime = 0


Url = http://tbc.wowhead.com/spell=25467/devouring-plague
Name = Devouring Plague
Nameformatted = DevouringPlague
Id = 25467
Level = 68
School = Shadow
SecondarySchool = Shadow Magic
Mana = 1145
Range = 30
IsGCD = True
Rank = 1
Channeled = False
CooldownTime = 180000
CastTime = 0
TickTime = 0


Url = http://tbc.wowhead.com/spell=44047/chastise
Name = Chastise
Nameformatted = Chastise
Id = 44047
Level = 70
School = Holy
SecondarySchool = Holy
Mana = 300
Range = 20
IsGCD = False
Rank = 1
Channeled = False
CooldownTime = 30000
CastTime = 0
TickTime = 0


Url = http://tbc.wowhead.com/spell=32676/consume-magic
Name = Consume Magic
Nameformatted = ConsumeMagic
Id = 32676
Level = 20
School = Arcane
SecondarySchool = Discipline
Mana = -1
Range = 0
IsGCD = True
Rank = 1
Channeled = False
CooldownTime = 120000
CastTime = 0
TickTime = 0


Url = http://tbc.wowhead.com/spell=25470/hex-of-weakness
Name = Hex of Weakness
Nameformatted = HexofWeakness
Id = 25470
Level = 70
School = Shadow
SecondarySchool = Shadow Magic
Mana = 295
Range = 30
IsGCD = True
Rank = 1
Channeled = False
CooldownTime = 0
CastTime = 0
TickTime = 0


Url = http://tbc.wowhead.com/spell=25446/starshards
Name = Starshards
Nameformatted = Starshards
Id = 25446
Level = 66
School = Arcane
SecondarySchool = Discipline
Mana = -1
Range = 30
IsGCD = True
Rank = 1
Channeled = False
CooldownTime = 30000
CastTime = 0
TickTime = 0


Url = http://tbc.wowhead.com/spell=32546/binding-heal
Name = Binding Heal
Nameformatted = BindingHeal
Id = 32546
Level = 64
School = Holy
SecondarySchool = Holy
Mana = 705
Range = 40
IsGCD = True
Rank = 1
Channeled = False
CooldownTime = 0
CastTime = 1500
TickTime = 0


Url = http://tbc.wowhead.com/spell=25375/mind-blast
Name = Mind Blast
Nameformatted = MindBlast
Id = 25375
Level = 69
School = Shadow
SecondarySchool = Shadow Magic
Mana = 450
Range = 30
IsGCD = True
Rank = 11
Channeled = False
CooldownTime = 8000
CastTime = 1500
TickTime = 0


Url = http://tbc.wowhead.com/spell=34433/shadowfiend
Name = Shadowfiend
Nameformatted = Shadowfiend
Id = 34433
Level = 66
School = Shadow
SecondarySchool = Shadow Magic
Mana = -1
Range = 30
IsGCD = True
Rank = 1
Channeled = False
CooldownTime = 300000
CastTime = 0
TickTime = 0


Url = http://tbc.wowhead.com/spell=2651/elunes-grace
Name = Elune's Grace
Nameformatted = ElunesGrace
Id = 2651
Level = 20
School = Holy
SecondarySchool = Discipline
Mana = -1
Range = 0
IsGCD = True
Rank = 1
Channeled = False
CooldownTime = 180000
CastTime = 0
TickTime = 0


Url = http://tbc.wowhead.com/spell=32996/shadow-word-death
Name = Shadow Word: Death
Nameformatted = ShadowWordDeath
Id = 32996
Level = 70
School = Shadow
SecondarySchool = Shadow Magic
Mana = 309
Range = 30
IsGCD = True
Rank = 2
Channeled = False
CooldownTime = 12000
CastTime = 0
TickTime = 0


Url = http://tbc.wowhead.com/spell=10955/shackle-undead
Name = Shackle Undead
Nameformatted = ShackleUndead
Id = 10955
Level = 60
School = Holy
SecondarySchool = Discipline
Mana = 150
Range = 30
IsGCD = True
Rank = 3
Channeled = False
CooldownTime = 0
CastTime = 1500
TickTime = 0


Url = http://tbc.wowhead.com/spell=25437/desperate-prayer
Name = Desperate Prayer
Nameformatted = DesperatePrayer
Id = 25437
Level = 66
School = Holy
SecondarySchool = Holy
Mana = -1
Range = 0
IsGCD = True
Rank = 1
Channeled = False
CooldownTime = 600000
CastTime = 0
TickTime = 0


Url = http://tbc.wowhead.com/spell=25441/feedback
Name = Feedback
Nameformatted = Feedback
Id = 25441
Level = 70
School = Shadow
SecondarySchool = Discipline
Mana = 705
Range = 0
IsGCD = True
Rank = 1
Channeled = False
CooldownTime = 180000
CastTime = 0
TickTime = 0


Url = http://tbc.wowhead.com/spell=25461/touch-of-weakness
Name = Touch of Weakness
Nameformatted = TouchofWeakness
Id = 25461
Level = 70
School = Shadow
SecondarySchool = Shadow Magic
Mana = 235
Range = 0
IsGCD = True
Rank = 1
Channeled = False
CooldownTime = 0
CastTime = 0
TickTime = 0


Url = http://tbc.wowhead.com/spell=25380/mana-burn
Name = Mana Burn
Nameformatted = ManaBurn
Id = 25380
Level = 70
School = Shadow
SecondarySchool = Discipline
Mana = 355
Range = 30
IsGCD = True
Rank = 7
Channeled = False
CooldownTime = 0
CastTime = 3000
TickTime = 0


Url = http://tbc.wowhead.com/spell=25477/shadowguard
Name = Shadowguard
Nameformatted = Shadowguard
Id = 25477
Level = 68
School = Shadow
SecondarySchool = Shadow Magic
Mana = 270
Range = 0
IsGCD = True
Rank = 1
Channeled = False
CooldownTime = 0
CastTime = 0
TickTime = 0


Url = http://tbc.wowhead.com/spell=528/cure-disease
Name = Cure Disease
Nameformatted = CureDisease
Id = 528
Level = 14
School = Holy
SecondarySchool = Holy
Mana = -1
Range = 40
IsGCD = True
Rank = 1
Channeled = False
CooldownTime = 0
CastTime = 0
TickTime = 0


Url = http://tbc.wowhead.com/spell=988/dispel-magic
Name = Dispel Magic
Nameformatted = DispelMagic
Id = 988
Level = 36
School = Holy
SecondarySchool = Discipline
Mana = -1
Range = 30
IsGCD = True
Rank = 2
Channeled = False
CooldownTime = 0
CastTime = 0
TickTime = 0


Url = http://tbc.wowhead.com/spell=25368/shadow-word-pain
Name = Shadow Word: Pain
Nameformatted = ShadowWordPain
Id = 25368
Level = 70
School = Shadow
SecondarySchool = Shadow Magic
Mana = 575
Range = 30
IsGCD = True
Rank = 10
Channeled = False
CooldownTime = 0
CastTime = 0
TickTime = 0


Url = http://tbc.wowhead.com/spell=32375/mass-dispel
Name = Mass Dispel
Nameformatted = MassDispel
Id = 32375
Level = 70
School = Holy
SecondarySchool = Discipline
Mana = -1
Range = 30
IsGCD = True
Rank = 1
Channeled = False
CooldownTime = 0
CastTime = 1500
TickTime = 0


Url = http://tbc.wowhead.com/spell=552/abolish-disease
Name = Abolish Disease
Nameformatted = AbolishDisease
Id = 552
Level = 32
School = Holy
SecondarySchool = Holy
Mana = -1
Range = 40
IsGCD = True
Rank = 1
Channeled = False
CooldownTime = 0
CastTime = 0
TickTime = 0


Url = http://tbc.wowhead.com/spell=25218/power-word-shield
Name = Power Word: Shield
Nameformatted = PowerWordShield
Id = 25218
Level = 70
School = Holy
SecondarySchool = Discipline
Mana = 600
Range = 40
IsGCD = True
Rank = 12
Channeled = False
CooldownTime = 4000
CastTime = 0
TickTime = 0


Url = http://tbc.wowhead.com/spell=25235/flash-heal
Name = Flash Heal
Nameformatted = FlashHeal
Id = 25235
Level = 67
School = Holy
SecondarySchool = Holy
Mana = 470
Range = 40
IsGCD = True
Rank = 9
Channeled = False
CooldownTime = 0
CastTime = 1500
TickTime = 0


Url = http://tbc.wowhead.com/spell=39374/prayer-of-shadow-protection
Name = Prayer of Shadow Protection
Nameformatted = PrayerofShadowProtection
Id = 39374
Level = 70
School = Holy
SecondarySchool = Shadow Magic
Mana = 1620
Range = 40
IsGCD = True
Rank = 2
Channeled = False
CooldownTime = 0
CastTime = 0
TickTime = 0


Url = http://tbc.wowhead.com/spell=25392/prayer-of-fortitude
Name = Prayer of Fortitude
Nameformatted = PrayerofFortitude
Id = 25392
Level = 70
School = Holy
SecondarySchool = Discipline
Mana = 1800
Range = 40
IsGCD = True
Rank = 3
Channeled = False
CooldownTime = 0
CastTime = 0
TickTime = 0


Url = http://tbc.wowhead.com/spell=25364/smite
Name = Smite
Nameformatted = Smite
Id = 25364
Level = 69
School = Holy
SecondarySchool = Holy
Mana = 385
Range = 30
IsGCD = True
Rank = 10
Channeled = False
CooldownTime = 0
CastTime = 2500
TickTime = 0


Url = http://tbc.wowhead.com/spell=25431/inner-fire
Name = Inner Fire
Nameformatted = InnerFire
Id = 25431
Level = 69
School = Holy
SecondarySchool = Discipline
Mana = 375
Range = 0
IsGCD = True
Rank = 7
Channeled = False
CooldownTime = 0
CastTime = 0
TickTime = 0


Url = http://tbc.wowhead.com/spell=1706/levitate
Name = Levitate
Nameformatted = Levitate
Id = 1706
Level = 34
School = Holy
SecondarySchool = Discipline
Mana = 100
Range = 0
IsGCD = True
Rank = 1
Channeled = False
CooldownTime = 0
CastTime = 0
TickTime = 0


Url = http://tbc.wowhead.com/spell=25315/renew
Name = Renew
Nameformatted = Renew
Id = 25315
Level = 60
School = Holy
SecondarySchool = Holy
Mana = 410
Range = 40
IsGCD = True
Rank = 10
Channeled = False
CooldownTime = 0
CastTime = 0
TickTime = 0


Url = http://tbc.wowhead.com/spell=25316/prayer-of-healing
Name = Prayer of Healing
Nameformatted = PrayerofHealing
Id = 25316
Level = 60
School = Holy
SecondarySchool = Holy
Mana = 1070
Range = 0
IsGCD = True
Rank = 5
Channeled = False
CooldownTime = 0
CastTime = 3000
TickTime = 0


Url = http://tbc.wowhead.com/spell=25429/fade
Name = Fade
Nameformatted = Fade
Id = 25429
Level = 66
School = Shadow
SecondarySchool = Shadow Magic
Mana = 330
Range = 0
IsGCD = True
Rank = 7
Channeled = False
CooldownTime = 30000
CastTime = 0
TickTime = 0


Url = http://tbc.wowhead.com/spell=25384/holy-fire
Name = Holy Fire
Nameformatted = HolyFire
Id = 25384
Level = 66
School = Holy
SecondarySchool = Holy
Mana = 290
Range = 30
IsGCD = True
Rank = 9
Channeled = False
CooldownTime = 0
CastTime = 3500
TickTime = 0


Url = http://tbc.wowhead.com/spell=6064/heal
Name = Heal
Nameformatted = Heal
Id = 6064
Level = 34
School = Holy
SecondarySchool = Holy
Mana = 305
Range = 40
IsGCD = True
Rank = 4
Channeled = False
CooldownTime = 0
CastTime = 3000
TickTime = 0


Url = http://tbc.wowhead.com/spell=10890/psychic-scream
Name = Psychic Scream
Nameformatted = PsychicScream
Id = 10890
Level = 56
School = Shadow
SecondarySchool = Shadow Magic
Mana = 210
Range = 0
IsGCD = True
Rank = 4
Channeled = False
CooldownTime = 30000
CastTime = 0
TickTime = 0


Url = http://tbc.wowhead.com/spell=25433/shadow-protection
Name = Shadow Protection
Nameformatted = ShadowProtection
Id = 25433
Level = 68
School = Shadow
SecondarySchool = Shadow Magic
Mana = 810
Range = 30
IsGCD = True
Rank = 4
Channeled = False
CooldownTime = 0
CastTime = 0
TickTime = 0


Url = http://tbc.wowhead.com/spell=25314/greater-heal
Name = Greater Heal
Nameformatted = GreaterHeal
Id = 25314
Level = 60
School = Holy
SecondarySchool = Holy
Mana = 710
Range = 40
IsGCD = True
Rank = 5
Channeled = False
CooldownTime = 0
CastTime = 3000
TickTime = 0


Url = http://tbc.wowhead.com/spell=25389/power-word-fortitude
Name = Power Word: Fortitude
Nameformatted = PowerWordFortitude
Id = 25389
Level = 70
School = Holy
SecondarySchool = Discipline
Mana = 700
Range = 30
IsGCD = True
Rank = 7
Channeled = False
CooldownTime = 0
CastTime = 0
TickTime = 0


Url = http://tbc.wowhead.com/spell=25596/mind-soothe
Name = Mind Soothe
Nameformatted = MindSoothe
Id = 25596
Level = 67
School = Shadow
SecondarySchool = Shadow Magic
Mana = 120
Range = 40
IsGCD = True
Rank = 4
Channeled = False
CooldownTime = 0
CastTime = 0
TickTime = 0


Url = http://tbc.wowhead.com/spell=32999/prayer-of-spirit
Name = Prayer of Spirit
Nameformatted = PrayerofSpirit
Id = 32999
Level = 70
School = Holy
SecondarySchool = Discipline
Mana = 1800
Range = 40
IsGCD = True
Rank = 2
Channeled = False
CooldownTime = 0
CastTime = 0
TickTime = 0


Url = http://tbc.wowhead.com/spell=10909/mind-vision
Name = Mind Vision
Nameformatted = MindVision
Id = 10909
Level = 44
School = Shadow
SecondarySchool = Shadow Magic
Mana = 150
Range = 50000
IsGCD = True
Rank = 2
Channeled = True
CooldownTime = 0
CastTime = 1000
TickTime = 0


Url = http://tbc.wowhead.com/spell=10912/mind-control
Name = Mind Control
Nameformatted = MindControl
Id = 10912
Level = 58
School = Shadow
SecondarySchool = Shadow Magic
Mana = 750
Range = 20
IsGCD = True
Rank = 3
Channeled = False
CooldownTime = 0
CastTime = 3000
TickTime = 0


Url = http://tbc.wowhead.com/spell=2053/lesser-heal
Name = Lesser Heal
Nameformatted = LesserHeal
Id = 2053
Level = 10
School = Holy
SecondarySchool = Holy
Mana = 75
Range = 40
IsGCD = True
Rank = 3
Channeled = False
CooldownTime = 0
CastTime = 2500
TickTime = 0


Url = http://tbc.wowhead.com/spell=25435/resurrection
Name = Resurrection
Nameformatted = Resurrection
Id = 25435
Level = 68
School = Holy
SecondarySchool = Holy
Mana = -1
Range = 30
IsGCD = True
Rank = 6
Channeled = False
CooldownTime = 0
CastTime = 10000
TickTime = 0


