using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Rogue.Spell
{
    public partial class DeadlyPoisonVISpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public DeadlyPoisonVISpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=26969/deadly-poison-vi
            Name = "Deadly Poison VI";
            Id = 26969;
            Level = 62;
            Rank = 6;
            Range = 0;

            Mana = -1;

            IsGCD = false;
            Channeled = false;
            CastTime = 3000;
            CooldownTime = 0;

            School = EnumSim.SchoolType.Physical;
            SecondarySchool = EnumSim.SecondarySchoolType.Rogue_Poisons;
            Init(sim);
        }
    }
}