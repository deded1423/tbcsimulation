using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Rogue.Spell
{
    public partial class SinisterStrikeSpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public SinisterStrikeSpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=26862/sinister-strike
            Name = "Sinister Strike";
            Id = 26862;
            Level = 70;
            Rank = 10;
            Range = 5;

            Mana = -1;

            IsGCD = true;
            Channeled = false;
            CastTime = 0;
            CooldownTime = 0;

            School = EnumSim.SchoolType.Physical;
            SecondarySchool = EnumSim.SecondarySchoolType.Rogue_Combat;
            Init(sim);
        }
    }
}