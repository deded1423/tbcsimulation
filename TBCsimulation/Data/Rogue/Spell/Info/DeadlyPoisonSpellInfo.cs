using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Rogue.Spell
{
    public partial class DeadlyPoisonSpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public DeadlyPoisonSpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=2835/deadly-poison
            Name = "Deadly Poison";
            Id = 2835;
            Level = 30;
            Rank = 1;
            Range = 0;

            Mana = -1;

            IsGCD = false;
            Channeled = false;
            CastTime = 3000;
            CooldownTime = 0;

            School = EnumSim.SchoolType.Physical;
            SecondarySchool = EnumSim.SecondarySchoolType.Rogue_Poisons;
            Init(sim);
        }
    }
}