using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Rogue.Spell
{
    public partial class InstantPoisonVSpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public InstantPoisonVSpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=11342/instant-poison-v
            Name = "Instant Poison V";
            Id = 11342;
            Level = 52;
            Rank = 5;
            Range = 0;

            Mana = -1;

            IsGCD = false;
            Channeled = false;
            CastTime = 3000;
            CooldownTime = 0;

            School = EnumSim.SchoolType.Physical;
            SecondarySchool = EnumSim.SecondarySchoolType.Rogue_Poisons;
            Init(sim);
        }
    }
}