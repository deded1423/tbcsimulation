using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Rogue.Spell
{
    public partial class InstantPoisonIISpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public InstantPoisonIISpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=8687/instant-poison-ii
            Name = "Instant Poison II";
            Id = 8687;
            Level = 28;
            Rank = 2;
            Range = 0;

            Mana = -1;

            IsGCD = false;
            Channeled = false;
            CastTime = 3000;
            CooldownTime = 0;

            School = EnumSim.SchoolType.Physical;
            SecondarySchool = EnumSim.SecondarySchoolType.Rogue_Poisons;
            Init(sim);
        }
    }
}