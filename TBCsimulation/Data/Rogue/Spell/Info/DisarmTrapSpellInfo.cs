using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Rogue.Spell
{
    public partial class DisarmTrapSpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public DisarmTrapSpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=1842/disarm-trap
            Name = "Disarm Trap";
            Id = 1842;
            Level = 30;
            Rank = 1;
            Range = 5;

            Mana = -1;

            IsGCD = false;
            Channeled = false;
            CastTime = 2000;
            CooldownTime = 0;

            School = EnumSim.SchoolType.Physical;
            SecondarySchool = EnumSim.SecondarySchoolType.Rogue_Subtlety;
            Init(sim);
        }
    }
}