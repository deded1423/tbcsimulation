using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Rogue.Spell
{
    public partial class PickLockSpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public PickLockSpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=1804/pick-lock
            Name = "Pick Lock";
            Id = 1804;
            Level = 1;
            Rank = 1;
            Range = 5;

            Mana = -1;

            IsGCD = false;
            Channeled = false;
            CastTime = 5000;
            CooldownTime = 0;

            School = EnumSim.SchoolType.Physical;
            SecondarySchool = EnumSim.SecondarySchoolType.Rogue_Lockpicking;
            Init(sim);
        }
    }
}