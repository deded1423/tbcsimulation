using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Rogue.Spell
{
    public partial class SliceandDiceSpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public SliceandDiceSpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=6774/slice-and-dice
            Name = "Slice and Dice";
            Id = 6774;
            Level = 42;
            Rank = 2;
            Range = 100;

            Mana = -1;

            IsGCD = true;
            Channeled = false;
            CastTime = 0;
            CooldownTime = 0;

            School = EnumSim.SchoolType.Physical;
            SecondarySchool = EnumSim.SecondarySchoolType.Rogue_Assassination;
            Init(sim);
        }
    }
}