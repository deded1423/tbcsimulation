using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Rogue.Spell
{
    public partial class KidneyShotSpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public KidneyShotSpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=8643/kidney-shot
            Name = "Kidney Shot";
            Id = 8643;
            Level = 50;
            Rank = 2;
            Range = 5;

            Mana = -1;

            IsGCD = true;
            Channeled = false;
            CastTime = 0;
            CooldownTime = 20000;

            School = EnumSim.SchoolType.Physical;
            SecondarySchool = EnumSim.SecondarySchoolType.Rogue_Assassination;
            Init(sim);
        }
    }
}