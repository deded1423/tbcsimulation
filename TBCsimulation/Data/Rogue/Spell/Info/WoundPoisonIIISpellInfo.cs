using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Rogue.Spell
{
    public partial class WoundPoisonIIISpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public WoundPoisonIIISpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=13229/wound-poison-iii
            Name = "Wound Poison III";
            Id = 13229;
            Level = 48;
            Rank = 3;
            Range = 0;

            Mana = -1;

            IsGCD = false;
            Channeled = false;
            CastTime = 3000;
            CooldownTime = 0;

            School = EnumSim.SchoolType.Physical;
            SecondarySchool = EnumSim.SecondarySchoolType.Rogue_Poisons;
            Init(sim);
        }
    }
}