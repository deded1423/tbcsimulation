using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Rogue.Spell
{
    public partial class EnvenomSpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public EnvenomSpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=32684/envenom
            Name = "Envenom";
            Id = 32684;
            Level = 69;
            Rank = 2;
            Range = 5;

            Mana = -1;

            IsGCD = true;
            Channeled = false;
            CastTime = 0;
            CooldownTime = 0;

            School = EnumSim.SchoolType.Nature;
            SecondarySchool = EnumSim.SecondarySchoolType.Rogue_Assassination;
            Init(sim);
        }
    }
}