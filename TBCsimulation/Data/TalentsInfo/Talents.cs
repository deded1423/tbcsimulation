﻿using System;
using TBCsimulation.Data.Utils;

namespace TBCsimulation.Data.TalentsInfo
{
    public static partial class Talents
    {
        public static Talent GetTalent(int Id)
        {
            var talent = GetDruidTalent(Id);
            if (talent != null)
                return talent;
            talent = GetHunterTalent(Id);
            if (talent != null)
                return talent;
            talent = GetMageTalent(Id);
            if (talent != null)
                return talent;
            talent = GetPaladinTalent(Id);
            if (talent != null)
                return talent;
            talent = GetPriestTalent(Id);
            if (talent != null)
                return talent;
            talent = GetRogueTalent(Id);
            if (talent != null)
                return talent;
            talent = GetShamanTalent(Id);
            if (talent != null)
                return talent;
            talent = GetWarlockTalent(Id);
            if (talent != null)
                return talent;
            talent = GetWarriorTalent(Id);
            if (talent != null)
                return talent;
            return null;
        }

        public static Talent GetTalent(EnumSim.WowClass wowClass, int Id)
        {
            switch (wowClass)
            {
                case EnumSim.WowClass.Druid:
                    return GetDruidTalent(Id);
                case EnumSim.WowClass.Hunter:
                    return GetHunterTalent(Id);
                case EnumSim.WowClass.Mage:
                    return GetMageTalent(Id);
                case EnumSim.WowClass.Paladin:
                    return GetPaladinTalent(Id);
                case EnumSim.WowClass.Priest:
                    return GetPriestTalent(Id);
                case EnumSim.WowClass.Rogue:
                    return GetRogueTalent(Id);
                case EnumSim.WowClass.Shaman:
                    return GetShamanTalent(Id);
                case EnumSim.WowClass.Warrior:
                    return GetWarriorTalent(Id);
                case EnumSim.WowClass.Warlock:
                    return GetWarlockTalent(Id);
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        public static Talent GetDruidTalent(int Id)
        {
            return Druid.Find(talent => { return talent.GetId() == Id; });
        }

        public static Talent GetHunterTalent(int Id)
        {
            return Hunter.Find(talent => { return talent.GetId() == Id; });
        }

        public static Talent GetMageTalent(int Id)
        {
            return Mage.Find(talent => { return talent.GetId() == Id; });
        }

        public static Talent GetPaladinTalent(int Id)
        {
            return Paladin.Find(talent => { return talent.GetId() == Id; });
        }

        public static Talent GetPriestTalent(int Id)
        {
            return Priest.Find(talent => { return talent.GetId() == Id; });
        }

        public static Talent GetRogueTalent(int Id)
        {
            return Rogue.Find(talent => { return talent.GetId() == Id; });
        }

        public static Talent GetShamanTalent(int Id)
        {
            return Shaman.Find(talent => { return talent.GetId() == Id; });
        }

        public static Talent GetWarlockTalent(int Id)
        {
            return Warlock.Find(talent => { return talent.GetId() == Id; });
        }

        public static Talent GetWarriorTalent(int Id)
        {
            return Warrior.Find(talent => { return talent.GetId() == Id; });
        }

        public static int GetIdFromName(string name)
        {
            var id = 0;

            if (DruidDictionaryNameToId.TryGetValue(name, out id))
                return id;
            if (HunterDictionaryNameToId.TryGetValue(name, out id))
                return id;
            if (MageDictionaryNameToId.TryGetValue(name, out id))
                return id;
            if (PaladinDictionaryNameToId.TryGetValue(name, out id))
                return id;
            if (PriestDictionaryNameToId.TryGetValue(name, out id))
                return id;
            if (RogueDictionaryNameToId.TryGetValue(name, out id))
                return id;
            if (ShamanDictionaryNameToId.TryGetValue(name, out id))
                return id;
            if (WarlockDictionaryNameToId.TryGetValue(name, out id))
                return id;
            if (WarriorDictionaryNameToId.TryGetValue(name, out id))
                return id;
            return id;
        }

        public static string GetNameFromId(int id)
        {
            string name = null;
            if (DruidDictionaryIdToName.TryGetValue(id, out name))
                return name;
            if (HunterDictionaryIdToName.TryGetValue(id, out name))
                return name;
            if (MageDictionaryIdToName.TryGetValue(id, out name))
                return name;
            if (PaladinDictionaryIdToName.TryGetValue(id, out name))
                return name;
            if (PriestDictionaryIdToName.TryGetValue(id, out name))
                return name;
            if (RogueDictionaryIdToName.TryGetValue(id, out name))
                return name;
            if (ShamanDictionaryIdToName.TryGetValue(id, out name))
                return name;
            if (WarlockDictionaryIdToName.TryGetValue(id, out name))
                return name;
            if (WarriorDictionaryIdToName.TryGetValue(id, out name))
                return name;
            return name;
        }
    }
}