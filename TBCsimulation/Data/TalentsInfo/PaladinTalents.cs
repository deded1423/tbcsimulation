using System.Collections.Generic;
using TBCsimulation.Data.Utils;

namespace TBCsimulation.Data.TalentsInfo
{
    public static partial class Talents
    {
        public static readonly List<Talent> Paladin = new()
        {
            new()
            {
                Id = 20205,
                Name = "Spiritual Focus",
                WowClass = EnumSim.WowClass.Paladin,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Paladin_Holy,
                MaxRank = 5,
                TalentRequired = -1,
                Tooltip =
                    "Gives your Flash of Light and Holy Light spells a 14% chance to not lose casting time when you take damage.",
                Row = 1,
                Column = 1
            },
            new()
            {
                Id = 20216,
                Name = "Divine Favor",
                WowClass = EnumSim.WowClass.Paladin,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Paladin_Holy,
                MaxRank = 1,
                TalentRequired = -1,
                Tooltip =
                    "When activated, gives your next Flash of Light, Holy Light, or Holy Shock spell a 100% critical effect chance.",
                Row = 4,
                Column = 1
            },
            new()
            {
                Id = 31821,
                Name = "Aura Mastery",
                WowClass = EnumSim.WowClass.Paladin,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Paladin_Holy,
                MaxRank = 1,
                TalentRequired = -1,
                Tooltip = "Increases the radius of your Auras to 40 yards.",
                Row = 2,
                Column = 1
            },
            new()
            {
                Id = 20234,
                Name = "Improved Lay on Hands",
                WowClass = EnumSim.WowClass.Paladin,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Paladin_Holy,
                MaxRank = 2,
                TalentRequired = -1,
                Tooltip =
                    "Gives the target of your Lay on Hands spell a 15% bonus to their armor value from items for 2 min. &nbsp;In addition, the cooldown for your Lay on Hands spell is reduced by 10 min.",
                Row = 2,
                Column = 2
            },
            new()
            {
                Id = 20237,
                Name = "Healing Light",
                WowClass = EnumSim.WowClass.Paladin,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Paladin_Holy,
                MaxRank = 3,
                TalentRequired = -1,
                Tooltip = "Increases the amount healed by your Holy Light and Flash of Light spells by 4%.",
                Row = 2,
                Column = 0
            },
            new()
            {
                Id = 20244,
                Name = "Improved Blessing of Wisdom",
                WowClass = EnumSim.WowClass.Paladin,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Paladin_Holy,
                MaxRank = 2,
                TalentRequired = -1,
                Tooltip = "Increases the effect of your Blessing of Wisdom spell by 10%.",
                Row = 3,
                Column = 2
            },
            new()
            {
                Id = 20257,
                Name = "Divine Intellect",
                WowClass = EnumSim.WowClass.Paladin,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Paladin_Holy,
                MaxRank = 5,
                TalentRequired = -1,
                Tooltip = "Increases your total Intellect by 2%.",
                Row = 0,
                Column = 2
            },
            new()
            {
                Id = 20262,
                Name = "Divine Strength",
                WowClass = EnumSim.WowClass.Paladin,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Paladin_Holy,
                MaxRank = 5,
                TalentRequired = -1,
                Tooltip = "Increases your total Strength by 2%.",
                Row = 0,
                Column = 1
            },
            new()
            {
                Id = 20210,
                Name = "Illumination",
                WowClass = EnumSim.WowClass.Paladin,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Paladin_Holy,
                MaxRank = 5,
                TalentRequired = -1,
                Tooltip =
                    "After getting a critical effect from your Flash of Light, Holy Light, or Holy Shock heal spell, gives you a 20% chance to gain mana equal to 60% of the base cost of the spell.",
                Row = 3,
                Column = 1
            },
            new()
            {
                Id = 20224,
                Name = "Improved Seal of Righteousness",
                WowClass = EnumSim.WowClass.Paladin,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Paladin_Holy,
                MaxRank = 5,
                TalentRequired = -1,
                Tooltip =
                    "Increases the damage done by your Seal of Righteousness and Judgement of Righteousness by 3%.",
                Row = 1,
                Column = 2
            },
            new()
            {
                Id = 20359,
                Name = "Sanctified Light",
                WowClass = EnumSim.WowClass.Paladin,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Paladin_Holy,
                MaxRank = 3,
                TalentRequired = -1,
                Tooltip = "Increases the critical effect chance of your Holy Light spell by 2%.",
                Row = 4,
                Column = 2
            },
            new()
            {
                Id = 20473,
                Name = "Holy Shock",
                WowClass = EnumSim.WowClass.Paladin,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Paladin_Holy,
                MaxRank = 1,
                TalentRequired = -1,
                Tooltip =
                    "Blasts the target with Holy energy, causing 277 to 299 Holy damage to an enemy, or 351 to 379 healing to an ally.",
                Row = 6,
                Column = 1
            },
            new()
            {
                Id = 5923,
                Name = "Holy Power",
                WowClass = EnumSim.WowClass.Paladin,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Paladin_Holy,
                MaxRank = 5,
                TalentRequired = -1,
                Tooltip = "Increases the critical effect chance of your Holy spells by 1%.",
                Row = 5,
                Column = 2
            },
            new()
            {
                Id = 9453,
                Name = "Unyielding Faith",
                WowClass = EnumSim.WowClass.Paladin,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Paladin_Holy,
                MaxRank = 2,
                TalentRequired = -1,
                Tooltip = "Increases your chance to resist Fear and Disorient effects by an additional 5%.",
                Row = 2,
                Column = 3
            },
            new()
            {
                Id = 31822,
                Name = "Pure of Heart",
                WowClass = EnumSim.WowClass.Paladin,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Paladin_Holy,
                MaxRank = 3,
                TalentRequired = -1,
                Tooltip = "Increases your resistance to Curse and Disease effects by 5%.",
                Row = 4,
                Column = 0
            },
            new()
            {
                Id = 31825,
                Name = "Purifying Power",
                WowClass = EnumSim.WowClass.Paladin,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Paladin_Holy,
                MaxRank = 2,
                TalentRequired = -1,
                Tooltip =
                    "Reduces the mana cost of your Cleanse, Purify and Consecration spells by 5% and increases the critical strike chance of your Exorcism and Holy Wrath spells by 10%.",
                Row = 5,
                Column = 0
            },
            new()
            {
                Id = 31828,
                Name = "Blessed Life",
                WowClass = EnumSim.WowClass.Paladin,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Paladin_Holy,
                MaxRank = 3,
                TalentRequired = -1,
                Tooltip = "All attacks against you have a 4% chance to cause half damage.",
                Row = 6,
                Column = 2
            },
            new()
            {
                Id = 31833,
                Name = "Light's Grace",
                WowClass = EnumSim.WowClass.Paladin,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Paladin_Holy,
                MaxRank = 3,
                TalentRequired = -1,
                Tooltip =
                    "Gives your Holy Light spell a 33% chance to reduce the cast time of your next Holy Light spell by 0.5 sec. &nbsp;This effect lasts 15 sec.",
                Row = 6,
                Column = 0
            },
            new()
            {
                Id = 31837,
                Name = "Holy Guidance",
                WowClass = EnumSim.WowClass.Paladin,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Paladin_Holy,
                MaxRank = 5,
                TalentRequired = -1,
                Tooltip = "Increases your spell damage and healing by 7% of your total Intellect.",
                Row = 7,
                Column = 1
            },
            new()
            {
                Id = 31842,
                Name = "Divine Illumination",
                WowClass = EnumSim.WowClass.Paladin,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Paladin_Holy,
                MaxRank = 1,
                TalentRequired = -1,
                Tooltip = "Reduces the mana cost of all spells by 50% for 15 sec.",
                Row = 8,
                Column = 1
            },
            new()
            {
                Id = 20127,
                Name = "Redoubt",
                WowClass = EnumSim.WowClass.Paladin,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Paladin_Protection,
                MaxRank = 5,
                TalentRequired = -1,
                Tooltip =
                    "Damaging melee and ranged attacks against you have a 10% chance to increase your chance to block by 6%. &nbsp;Lasts 10 sec or 5 blocks.",
                Row = 0,
                Column = 2
            },
            new()
            {
                Id = 20138,
                Name = "Improved Devotion Aura",
                WowClass = EnumSim.WowClass.Paladin,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Paladin_Protection,
                MaxRank = 5,
                TalentRequired = -1,
                Tooltip = "Increases the armor bonus of your Devotion Aura by 8%.",
                Row = 0,
                Column = 1
            },
            new()
            {
                Id = 20143,
                Name = "Toughness",
                WowClass = EnumSim.WowClass.Paladin,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Paladin_Protection,
                MaxRank = 5,
                TalentRequired = -1,
                Tooltip = "Increases your armor value from items by 2%.",
                Row = 1,
                Column = 3
            },
            new()
            {
                Id = 20148,
                Name = "Shield Specialization",
                WowClass = EnumSim.WowClass.Paladin,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Paladin_Protection,
                MaxRank = 3,
                TalentRequired = -1,
                Tooltip = "Increases the amount of damage absorbed by your shield by 10%.",
                Row = 2,
                Column = 2
            },
            new()
            {
                Id = 20174,
                Name = "Guardian's Favor",
                WowClass = EnumSim.WowClass.Paladin,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Paladin_Protection,
                MaxRank = 2,
                TalentRequired = -1,
                Tooltip =
                    "Reduces the cooldown of your Blessing of Protection by 60 sec and increases the duration of your Blessing of Freedom by 2 sec.",
                Row = 1,
                Column = 1
            },
            new()
            {
                Id = 20177,
                Name = "Reckoning",
                WowClass = EnumSim.WowClass.Paladin,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Paladin_Protection,
                MaxRank = 5,
                TalentRequired = -1,
                Tooltip =
                    "Gives you a 2% chance after being hit by any damaging attack that the next 4 weapon swings within 8 sec will generate an additional attack.",
                Row = 4,
                Column = 2
            },
            new()
            {
                Id = 20196,
                Name = "One-Handed Weapon Specialization",
                WowClass = EnumSim.WowClass.Paladin,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Paladin_Protection,
                MaxRank = 5,
                TalentRequired = -1,
                Tooltip = "Increases all damage you deal when a one-handed melee weapon is equipped by 1%.",
                Row = 5,
                Column = 2
            },
            new()
            {
                Id = 20925,
                Name = "Holy Shield",
                WowClass = EnumSim.WowClass.Paladin,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Paladin_Protection,
                MaxRank = 1,
                TalentRequired = -1,
                Tooltip =
                    "Increases chance to block by 30% for 10 sec and deals 59 Holy damage for each attack blocked while active. &nbsp;Damage caused by Holy Shield causes 35% additional threat. &nbsp;Each block expends a charge. &nbsp;4 charges.",
                Row = 6,
                Column = 1
            },
            new()
            {
                Id = 20911,
                Name = "Blessing of Sanctuary",
                WowClass = EnumSim.WowClass.Paladin,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Paladin_Protection,
                MaxRank = 1,
                TalentRequired = -1,
                Tooltip =
                    "Places a Blessing on the friendly target, reducing damage dealt from all sources by up to 10 for 10 min. &nbsp;In addition, when the target blocks a melee attack the attacker will take 14 Holy damage. &nbsp;Players may only have one Blessing on them per Paladin at any one time.",
                Row = 4,
                Column = 1
            },
            new()
            {
                Id = 20217,
                Name = "Blessing of Kings",
                WowClass = EnumSim.WowClass.Paladin,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Paladin_Protection,
                MaxRank = 1,
                TalentRequired = -1,
                Tooltip =
                    "Places a Blessing on the friendly target, increasing total stats by 10% for 10 min. &nbsp;Players may only have one Blessing on them per Paladin at any one time.",
                Row = 2,
                Column = 0
            },
            new()
            {
                Id = 20468,
                Name = "Improved Righteous Fury",
                WowClass = EnumSim.WowClass.Paladin,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Paladin_Protection,
                MaxRank = 3,
                TalentRequired = -1,
                Tooltip =
                    "While Righteous Fury is active, all damage taken is reduced by 2% and increases the amount of threat generated by your Righteous Fury spell by 16%.",
                Row = 2,
                Column = 1
            },
            new()
            {
                Id = 20487,
                Name = "Improved Hammer of Justice",
                WowClass = EnumSim.WowClass.Paladin,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Paladin_Protection,
                MaxRank = 3,
                TalentRequired = -1,
                Tooltip = "Decreases the cooldown of your Hammer of Justice spell by 5 sec.",
                Row = 3,
                Column = 1
            },
            new()
            {
                Id = 20254,
                Name = "Improved Concentration Aura",
                WowClass = EnumSim.WowClass.Paladin,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Paladin_Protection,
                MaxRank = 3,
                TalentRequired = -1,
                Tooltip =
                    "Increases the effect of your Concentration Aura by an additional 5% and reduces the duration of any Silence or Interrupt effect used against an affected group member by 10%. &nbsp;The duration reduction does not stack with any other effects.",
                Row = 3,
                Column = 2
            },
            new()
            {
                Id = 20096,
                Name = "Anticipation",
                WowClass = EnumSim.WowClass.Paladin,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Paladin_Protection,
                MaxRank = 5,
                TalentRequired = -1,
                Tooltip = "Increases your Defense skill by 4.",
                Row = 2,
                Column = 3
            },
            new()
            {
                Id = 20189,
                Name = "Precision",
                WowClass = EnumSim.WowClass.Paladin,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Paladin_Protection,
                MaxRank = 3,
                TalentRequired = -1,
                Tooltip = "Increases your chance to hit with melee weapons and spells by 1%.",
                Row = 1,
                Column = 0
            },
            new()
            {
                Id = 31844,
                Name = "Stoicism",
                WowClass = EnumSim.WowClass.Paladin,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Paladin_Protection,
                MaxRank = 2,
                TalentRequired = -1,
                Tooltip =
                    "Increases your resistance to Stun effects by an additional 5% and reduces the chance your spells will be dispelled by an additional 15%.",
                Row = 3,
                Column = 0
            },
            new()
            {
                Id = 31846,
                Name = "Spell Warding",
                WowClass = EnumSim.WowClass.Paladin,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Paladin_Protection,
                MaxRank = 2,
                TalentRequired = -1,
                Tooltip = "All spell damage taken is reduced by 2%.",
                Row = 4,
                Column = 0
            },
            new()
            {
                Id = 31848,
                Name = "Sacred Duty",
                WowClass = EnumSim.WowClass.Paladin,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Paladin_Protection,
                MaxRank = 2,
                TalentRequired = -1,
                Tooltip =
                    "Increases your total Stamina by 3%, reduces the cooldown of your Divine Shield spell by 30 sec and reduces the attack speed penalty by 50%.",
                Row = 5,
                Column = 0
            },
            new()
            {
                Id = 31850,
                Name = "Ardent Defender",
                WowClass = EnumSim.WowClass.Paladin,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Paladin_Protection,
                MaxRank = 5,
                TalentRequired = -1,
                Tooltip = "When you have less than 35% health, all damage taken is reduced by 6%.",
                Row = 6,
                Column = 2
            },
            new()
            {
                Id = 31858,
                Name = "Combat Expertise",
                WowClass = EnumSim.WowClass.Paladin,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Paladin_Protection,
                MaxRank = 5,
                TalentRequired = -1,
                Tooltip = "Increases your expertise by 1 and your total Stamina by 2%.",
                Row = 7,
                Column = 2
            },
            new()
            {
                Id = 31935,
                Name = "Avenger's Shield",
                WowClass = EnumSim.WowClass.Paladin,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Paladin_Protection,
                MaxRank = 1,
                TalentRequired = -1,
                Tooltip =
                    "Hurls a holy shield at the enemy, dealing 270 to 330 Holy damage, Dazing them and then jumping to additional nearby enemies. &nbsp;Affects 3 total targets. &nbsp;Lasts 6 sec.",
                Row = 8,
                Column = 1
            },
            new()
            {
                Id = 41021,
                Name = "Improved Holy Shield",
                WowClass = EnumSim.WowClass.Paladin,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Paladin_Protection,
                MaxRank = 2,
                TalentRequired = -1,
                Tooltip =
                    "Increases damage caused by your Holy Shield by 10% and increases the number of charges of your Holy Shield by 2.",
                Row = 6,
                Column = 0
            },
            new()
            {
                Id = 20042,
                Name = "Improved Blessing of Might",
                WowClass = EnumSim.WowClass.Paladin,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Paladin_Retribution,
                MaxRank = 5,
                TalentRequired = -1,
                Tooltip = "Increases the attack power bonus of your Blessing of Might by 4%.",
                Row = 0,
                Column = 1
            },
            new()
            {
                Id = 20049,
                Name = "Vengeance",
                WowClass = EnumSim.WowClass.Paladin,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Paladin_Retribution,
                MaxRank = 5,
                TalentRequired = -1,
                Tooltip =
                    "Gives you a 1% bonus to Physical and Holy damage you deal for 30 sec after dealing a critical strike from a weapon swing, spell, or ability. &nbsp;This effect stacks up to 3 times.",
                Row = 5,
                Column = 1
            },
            new()
            {
                Id = 20060,
                Name = "Deflection",
                WowClass = EnumSim.WowClass.Paladin,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Paladin_Retribution,
                MaxRank = 5,
                TalentRequired = -1,
                Tooltip = "Increases your Parry chance by 1%.",
                Row = 1,
                Column = 2
            },
            new()
            {
                Id = 20091,
                Name = "Improved Retribution Aura",
                WowClass = EnumSim.WowClass.Paladin,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Paladin_Retribution,
                MaxRank = 2,
                TalentRequired = -1,
                Tooltip = "Increases the damage done by your Retribution Aura by 25%.",
                Row = 3,
                Column = 2
            },
            new()
            {
                Id = 20101,
                Name = "Benediction",
                WowClass = EnumSim.WowClass.Paladin,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Paladin_Retribution,
                MaxRank = 5,
                TalentRequired = -1,
                Tooltip = "Reduces the mana cost of your Judgement and Seal spells by 3%.",
                Row = 0,
                Column = 2
            },
            new()
            {
                Id = 20218,
                Name = "Sanctity Aura",
                WowClass = EnumSim.WowClass.Paladin,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Paladin_Retribution,
                MaxRank = 1,
                TalentRequired = -1,
                Tooltip =
                    "Increases Holy damage done by party members within 30 yards by 10%. &nbsp;Players may only have one Aura on them per Paladin at any one time.",
                Row = 4,
                Column = 2
            },
            new()
            {
                Id = 20111,
                Name = "Two-Handed Weapon Specialization",
                WowClass = EnumSim.WowClass.Paladin,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Paladin_Retribution,
                MaxRank = 3,
                TalentRequired = -1,
                Tooltip = "Increases the damage you deal with two-handed melee weapons by 2%.",
                Row = 4,
                Column = 0
            },
            new()
            {
                Id = 20117,
                Name = "Conviction",
                WowClass = EnumSim.WowClass.Paladin,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Paladin_Retribution,
                MaxRank = 5,
                TalentRequired = -1,
                Tooltip = "Increases your chance to get a critical strike with melee weapons by 1%.",
                Row = 2,
                Column = 1
            },
            new()
            {
                Id = 20066,
                Name = "Repentance",
                WowClass = EnumSim.WowClass.Paladin,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Paladin_Retribution,
                MaxRank = 1,
                TalentRequired = -1,
                Tooltip =
                    "Puts the enemy target in a state of meditation, incapacitating them for up to 6 sec. &nbsp;Any damage caused will awaken the target. &nbsp;Only works against Humanoids.",
                Row = 6,
                Column = 1
            },
            new()
            {
                Id = 20335,
                Name = "Improved Seal of the Crusader",
                WowClass = EnumSim.WowClass.Paladin,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Paladin_Retribution,
                MaxRank = 3,
                TalentRequired = -1,
                Tooltip =
                    "In addition to the normal effect, your Judgement of the Crusader spell will also increase the critical strike chance of all attacks made against that target by an additional 1%.",
                Row = 1,
                Column = 1
            },
            new()
            {
                Id = 20375,
                Name = "Seal of Command",
                WowClass = EnumSim.WowClass.Paladin,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Paladin_Retribution,
                MaxRank = 1,
                TalentRequired = -1,
                Tooltip =
                    "Gives the Paladin a chance to deal additional Holy damage equal to 70% of normal weapon damage. &nbsp;Only one Seal can be active on the Paladin at any one time. &nbsp;Lasts 30 sec.Unleashing this Seal's energy will judge an enemy, instantly causing 68 to 73 Holy damage, 137 to 146 if the target is stunned or incapacitated.",
                Row = 2,
                Column = 2
            },
            new()
            {
                Id = 25956,
                Name = "Improved Judgement",
                WowClass = EnumSim.WowClass.Paladin,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Paladin_Retribution,
                MaxRank = 2,
                TalentRequired = -1,
                Tooltip = "Decreases the cooldown of your Judgement spell by 1 sec.",
                Row = 1,
                Column = 0
            },
            new()
            {
                Id = 9799,
                Name = "Eye for an Eye",
                WowClass = EnumSim.WowClass.Paladin,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Paladin_Retribution,
                MaxRank = 2,
                TalentRequired = -1,
                Tooltip =
                    "All spell criticals against you cause 15% of the damage taken to the caster as well. &nbsp;The damage caused by Eye for an Eye will not exceed 50% of the Paladin's total health.",
                Row = 3,
                Column = 0
            },
            new()
            {
                Id = 9452,
                Name = "Vindication",
                WowClass = EnumSim.WowClass.Paladin,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Paladin_Retribution,
                MaxRank = 3,
                TalentRequired = -1,
                Tooltip =
                    "Gives the Paladin's damaging melee attacks a chance to reduce the target's attributes by 5% for 15 sec.",
                Row = 2,
                Column = 0
            },
            new()
            {
                Id = 26022,
                Name = "Pursuit of Justice",
                WowClass = EnumSim.WowClass.Paladin,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Paladin_Retribution,
                MaxRank = 3,
                TalentRequired = -1,
                Tooltip =
                    "Reduces the chance you'll be hit by spells by 1% and increases movement and mounted movement speed by 5%. &nbsp;This does not stack with other movement speed increasing effects.",
                Row = 2,
                Column = 3
            },
            new()
            {
                Id = 31866,
                Name = "Crusade",
                WowClass = EnumSim.WowClass.Paladin,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Paladin_Retribution,
                MaxRank = 3,
                TalentRequired = -1,
                Tooltip = "Increases all damage caused against Humanoids, Demons, Undead and Elementals by 1%.",
                Row = 3,
                Column = 3
            },
            new()
            {
                Id = 31869,
                Name = "Improved Sanctity Aura",
                WowClass = EnumSim.WowClass.Paladin,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Paladin_Retribution,
                MaxRank = 2,
                TalentRequired = -1,
                Tooltip = "The amount of damage caused by targets affected by Sanctity Aura is increased by 1%.",
                Row = 4,
                Column = 3
            },
            new()
            {
                Id = 31871,
                Name = "Divine Purpose",
                WowClass = EnumSim.WowClass.Paladin,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Paladin_Retribution,
                MaxRank = 3,
                TalentRequired = -1,
                Tooltip = "Melee and ranged critical strikes against you cause 4% less damage.",
                Row = 6,
                Column = 2
            },
            new()
            {
                Id = 31876,
                Name = "Sanctified Judgement",
                WowClass = EnumSim.WowClass.Paladin,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Paladin_Retribution,
                MaxRank = 3,
                TalentRequired = -1,
                Tooltip = "Gives your Judgement spell a 33% chance to return 80% of the mana cost of the judged seal.",
                Row = 5,
                Column = 2
            },
            new()
            {
                Id = 31879,
                Name = "Fanaticism",
                WowClass = EnumSim.WowClass.Paladin,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Paladin_Retribution,
                MaxRank = 5,
                TalentRequired = -1,
                Tooltip =
                    "Increases the critical strike chance of all Judgements capable of a critical hit by 3% and reduces threat caused by all actions by 6% except when under the effects of Righteous Fury.",
                Row = 7,
                Column = 1
            },
            new()
            {
                Id = 32043,
                Name = "Sanctified Seals",
                WowClass = EnumSim.WowClass.Paladin,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Paladin_Retribution,
                MaxRank = 3,
                TalentRequired = -1,
                Tooltip =
                    "Increases your chance to critically hit with all spells and melee attacks by 1% and reduces the chance your Seals will be dispelled by 33%.",
                Row = 6,
                Column = 0
            },
            new()
            {
                Id = 35395,
                Name = "Crusader Strike",
                WowClass = EnumSim.WowClass.Paladin,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Paladin_Retribution,
                MaxRank = 1,
                TalentRequired = -1,
                Tooltip =
                    "An instant strike that causes 110% weapon damage and refreshes all Judgements on the target.",
                Row = 8,
                Column = 1
            }
        };
    }
}