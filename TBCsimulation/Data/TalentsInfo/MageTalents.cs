using System.Collections.Generic;
using TBCsimulation.Data.Utils;

namespace TBCsimulation.Data.TalentsInfo
{
    public static partial class Talents
    {
        public static readonly List<Talent> Mage = new()
        {
            new()
            {
                Id = 11210,
                Name = "Arcane Subtlety",
                WowClass = EnumSim.WowClass.Mage,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Mage_Arcane,
                MaxRank = 2,
                TalentRequired = -1,
                Tooltip =
                    "Reduces your target's resistance to all your spells by 5 and reduces the threat caused by your Arcane spells by 20%.",
                Row = 0,
                Column = 0
            },
            new()
            {
                Id = 11213,
                Name = "Arcane Concentration",
                WowClass = EnumSim.WowClass.Mage,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Mage_Arcane,
                MaxRank = 5,
                TalentRequired = -1,
                Tooltip =
                    "Gives you a 2% chance of entering a Clearcasting state after any damage spell hits a target. &nbsp;The Clearcasting state reduces the mana cost of your next damage spell by 100%.",
                Row = 1,
                Column = 2
            },
            new()
            {
                Id = 11222,
                Name = "Arcane Focus",
                WowClass = EnumSim.WowClass.Mage,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Mage_Arcane,
                MaxRank = 5,
                TalentRequired = -1,
                Tooltip = "Reduces the chance that the opponent can resist your Arcane spells by 2%.",
                Row = 0,
                Column = 1
            },
            new()
            {
                Id = 11232,
                Name = "Arcane Mind",
                WowClass = EnumSim.WowClass.Mage,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Mage_Arcane,
                MaxRank = 5,
                TalentRequired = -1,
                Tooltip = "Increases your total Intellect by 3%.",
                Row = 4,
                Column = 3
            },
            new()
            {
                Id = 6057,
                Name = "Wand Specialization",
                WowClass = EnumSim.WowClass.Mage,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Mage_Arcane,
                MaxRank = 2,
                TalentRequired = -1,
                Tooltip = "Increases your damage with Wands by 13%.",
                Row = 1,
                Column = 0
            },
            new()
            {
                Id = 11237,
                Name = "Improved Arcane Missiles",
                WowClass = EnumSim.WowClass.Mage,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Mage_Arcane,
                MaxRank = 5,
                TalentRequired = -1,
                Tooltip =
                    "Gives you a 20% chance to avoid interruption caused by damage while channeling Arcane Missiles.",
                Row = 0,
                Column = 2
            },
            new()
            {
                Id = 11242,
                Name = "Arcane Impact",
                WowClass = EnumSim.WowClass.Mage,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Mage_Arcane,
                MaxRank = 3,
                TalentRequired = -1,
                Tooltip =
                    "Increases the critical strike chance of your Arcane Explosion and Arcane Blast spells by an additional 2%.",
                Row = 2,
                Column = 1
            },
            new()
            {
                Id = 11247,
                Name = "Magic Attunement",
                WowClass = EnumSim.WowClass.Mage,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Mage_Arcane,
                MaxRank = 2,
                TalentRequired = -1,
                Tooltip = "Increases the effect of your Amplify Magic and Dampen Magic spells by 25%.",
                Row = 2,
                Column = 0
            },
            new()
            {
                Id = 11252,
                Name = "Improved Mana Shield",
                WowClass = EnumSim.WowClass.Mage,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Mage_Arcane,
                MaxRank = 2,
                TalentRequired = -1,
                Tooltip = "Decreases the mana lost per point of damage taken when Mana Shield is active by 10%.",
                Row = 3,
                Column = 0
            },
            new()
            {
                Id = 28574,
                Name = "Arcane Fortitude",
                WowClass = EnumSim.WowClass.Mage,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Mage_Arcane,
                MaxRank = 1,
                TalentRequired = -1,
                Tooltip = "Increases your armor by an amount equal to 100% of your Intellect.",
                Row = 2,
                Column = 3
            },
            new()
            {
                Id = 12043,
                Name = "Presence of Mind",
                WowClass = EnumSim.WowClass.Mage,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Mage_Arcane,
                MaxRank = 1,
                TalentRequired = -1,
                Tooltip =
                    "When activated, your next Mage spell with a casting time less than 10 sec becomes an instant cast spell.",
                Row = 4,
                Column = 1
            },
            new()
            {
                Id = 12042,
                Name = "Arcane Power",
                WowClass = EnumSim.WowClass.Mage,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Mage_Arcane,
                MaxRank = 1,
                TalentRequired = -1,
                Tooltip =
                    "When activated, your spells deal 30% more damage while costing 30% more mana to cast. &nbsp;This effect lasts 15 sec.",
                Row = 6,
                Column = 1
            },
            new()
            {
                Id = 11255,
                Name = "Improved Counterspell",
                WowClass = EnumSim.WowClass.Mage,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Mage_Arcane,
                MaxRank = 2,
                TalentRequired = -1,
                Tooltip = "Gives your Counterspell a 50% chance to silence the target for 4 sec.",
                Row = 3,
                Column = 1
            },
            new()
            {
                Id = 15058,
                Name = "Arcane Instability",
                WowClass = EnumSim.WowClass.Mage,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Mage_Arcane,
                MaxRank = 3,
                TalentRequired = -1,
                Tooltip = "Increases your spell damage and critical strike chance by 1%.",
                Row = 5,
                Column = 1
            },
            new()
            {
                Id = 18462,
                Name = "Arcane Meditation",
                WowClass = EnumSim.WowClass.Mage,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Mage_Arcane,
                MaxRank = 3,
                TalentRequired = -1,
                Tooltip = "Allows 10% of your mana regeneration to continue while casting.",
                Row = 3,
                Column = 3
            },
            new()
            {
                Id = 29441,
                Name = "Magic Absorption",
                WowClass = EnumSim.WowClass.Mage,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Mage_Arcane,
                MaxRank = 5,
                TalentRequired = -1,
                Tooltip =
                    "Increases all resistances by 2 and causes all spells you fully resist to restore 1% of your total mana. &nbsp;1 sec. cooldown.",
                Row = 1,
                Column = 1
            },
            new()
            {
                Id = 31569,
                Name = "Improved Blink",
                WowClass = EnumSim.WowClass.Mage,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Mage_Arcane,
                MaxRank = 2,
                TalentRequired = -1,
                Tooltip =
                    "For 4 sec after casting Blink, your chance to be hit by all attacks and spells is reduced by 13%.",
                Row = 4,
                Column = 0
            },
            new()
            {
                Id = 31571,
                Name = "Arcane Potency",
                WowClass = EnumSim.WowClass.Mage,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Mage_Arcane,
                MaxRank = 3,
                TalentRequired = -1,
                Tooltip = "Increases the critical strike chance of any spell cast while Clearcasting by 10%.",
                Row = 5,
                Column = 2
            },
            new()
            {
                Id = 31574,
                Name = "Prismatic Cloak",
                WowClass = EnumSim.WowClass.Mage,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Mage_Arcane,
                MaxRank = 2,
                TalentRequired = -1,
                Tooltip = "Reduces all damage taken by 2%.",
                Row = 5,
                Column = 0
            },
            new()
            {
                Id = 31579,
                Name = "Empowered Arcane Missiles",
                WowClass = EnumSim.WowClass.Mage,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Mage_Arcane,
                MaxRank = 3,
                TalentRequired = -1,
                Tooltip =
                    "Your Arcane Missiles spell gains an additional 15% of your bonus spell damage effects, but mana cost is increased by 2%.",
                Row = 6,
                Column = 0
            },
            new()
            {
                Id = 31584,
                Name = "Mind Mastery",
                WowClass = EnumSim.WowClass.Mage,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Mage_Arcane,
                MaxRank = 5,
                TalentRequired = -1,
                Tooltip = "Increases spell damage by up to 5% of your total Intellect.",
                Row = 7,
                Column = 1
            },
            new()
            {
                Id = 31589,
                Name = "Slow",
                WowClass = EnumSim.WowClass.Mage,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Mage_Arcane,
                MaxRank = 1,
                TalentRequired = -1,
                Tooltip =
                    "Reduces target's movement speed by 50%, increases the time between ranged attacks by 50% and increases casting time by 50%. &nbsp;Lasts 15 sec. &nbsp;Slow can only affect one target at a time.",
                Row = 8,
                Column = 1
            },
            new()
            {
                Id = 35578,
                Name = "Spell Power",
                WowClass = EnumSim.WowClass.Mage,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Mage_Arcane,
                MaxRank = 2,
                TalentRequired = -1,
                Tooltip = "Increases critical strike damage bonus of all spells by 25%.",
                Row = 6,
                Column = 2
            },
            new()
            {
                Id = 11083,
                Name = "Burning Soul",
                WowClass = EnumSim.WowClass.Mage,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Mage_Fire,
                MaxRank = 2,
                TalentRequired = -1,
                Tooltip =
                    "Gives your Fire spells a 35% chance to not lose casting time when you take damage and reduces the threat caused by your Fire spells by 5%.",
                Row = 2,
                Column = 3
            },
            new()
            {
                Id = 11094,
                Name = "Molten Shields",
                WowClass = EnumSim.WowClass.Mage,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Mage_Fire,
                MaxRank = 2,
                TalentRequired = -1,
                Tooltip =
                    "Causes your Fire Ward to have a 10% chance to reflect Fire spells while active. In addition, your Molten Armor has a 50% chance to affect ranged and spell attacks.",
                Row = 3,
                Column = 1
            },
            new()
            {
                Id = 11095,
                Name = "Improved Scorch",
                WowClass = EnumSim.WowClass.Mage,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Mage_Fire,
                MaxRank = 3,
                TalentRequired = -1,
                Tooltip =
                    "Your Scorch spells have a 33% chance to cause your target to be vulnerable to Fire damage. &nbsp;This vulnerability increases the Fire damage dealt to your target by 3% and lasts 30 sec. &nbsp;Stacks up to 5 times.",
                Row = 3,
                Column = 0
            },
            new()
            {
                Id = 11069,
                Name = "Improved Fireball",
                WowClass = EnumSim.WowClass.Mage,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Mage_Fire,
                MaxRank = 5,
                TalentRequired = -1,
                Tooltip = "Reduces the casting time of your Fireball spell by 0.1 sec.",
                Row = 0,
                Column = 1
            },
            new()
            {
                Id = 11078,
                Name = "Improved Fire Blast",
                WowClass = EnumSim.WowClass.Mage,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Mage_Fire,
                MaxRank = 3,
                TalentRequired = -1,
                Tooltip = "Reduces the cooldown of your Fire Blast spell by 0.5 sec.",
                Row = 1,
                Column = 2
            },
            new()
            {
                Id = 11100,
                Name = "Flame Throwing",
                WowClass = EnumSim.WowClass.Mage,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Mage_Fire,
                MaxRank = 2,
                TalentRequired = -1,
                Tooltip = "Increases the range of your Fire spells by 3 yards.",
                Row = 1,
                Column = 1
            },
            new()
            {
                Id = 11366,
                Name = "Pyroblast",
                WowClass = EnumSim.WowClass.Mage,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Mage_Fire,
                MaxRank = 1,
                TalentRequired = -1,
                Tooltip =
                    "Hurls an immense fiery boulder that causes 148 to 195 Fire damage and an additional 56 Fire damage over 12 sec.",
                Row = 2,
                Column = 2
            },
            new()
            {
                Id = 11103,
                Name = "Impact",
                WowClass = EnumSim.WowClass.Mage,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Mage_Fire,
                MaxRank = 5,
                TalentRequired = -1,
                Tooltip = "Gives your Fire spells a 2% chance to stun the target for 2 sec.",
                Row = 0,
                Column = 2
            },
            new()
            {
                Id = 11108,
                Name = "Improved Flamestrike",
                WowClass = EnumSim.WowClass.Mage,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Mage_Fire,
                MaxRank = 3,
                TalentRequired = -1,
                Tooltip = "Increases the critical strike chance of your Flamestrike spell by 5%.",
                Row = 2,
                Column = 1
            },
            new()
            {
                Id = 11113,
                Name = "Blast Wave",
                WowClass = EnumSim.WowClass.Mage,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Mage_Fire,
                MaxRank = 1,
                TalentRequired = -1,
                Tooltip =
                    "A wave of flame radiates outward from the caster, damaging all enemies caught within the blast for 160 to 192 Fire damage, and Dazing them for 6 sec.",
                Row = 4,
                Column = 2
            },
            new()
            {
                Id = 11115,
                Name = "Critical Mass",
                WowClass = EnumSim.WowClass.Mage,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Mage_Fire,
                MaxRank = 3,
                TalentRequired = -1,
                Tooltip = "Increases the critical strike chance of your Fire spells by 2%.",
                Row = 4,
                Column = 1
            },
            new()
            {
                Id = 11119,
                Name = "Ignite",
                WowClass = EnumSim.WowClass.Mage,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Mage_Fire,
                MaxRank = 5,
                TalentRequired = -1,
                Tooltip =
                    "Your critical strikes from Fire damage spells cause the target to burn for an additional 8% of your spell's damage over 4 sec.",
                Row = 1,
                Column = 0
            },
            new()
            {
                Id = 11124,
                Name = "Fire Power",
                WowClass = EnumSim.WowClass.Mage,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Mage_Fire,
                MaxRank = 5,
                TalentRequired = -1,
                Tooltip = "Increases the damage done by your Fire spells by 2%.",
                Row = 5,
                Column = 2
            },
            new()
            {
                Id = 11129,
                Name = "Combustion",
                WowClass = EnumSim.WowClass.Mage,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Mage_Fire,
                MaxRank = 1,
                TalentRequired = -1,
                Tooltip =
                    "When activated, this spell causes each of your Fire damage spell hits to increase your critical strike chance with Fire damage spells by 10%. &nbsp;This effect lasts until you have caused 3 critical strikes with Fire spells.",
                Row = 6,
                Column = 1
            },
            new()
            {
                Id = 18459,
                Name = "Incineration",
                WowClass = EnumSim.WowClass.Mage,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Mage_Fire,
                MaxRank = 2,
                TalentRequired = -1,
                Tooltip = "Increases the critical strike chance of your Fire Blast and Scorch spells by 2%.",
                Row = 2,
                Column = 0
            },
            new()
            {
                Id = 29074,
                Name = "Master of Elements",
                WowClass = EnumSim.WowClass.Mage,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Mage_Fire,
                MaxRank = 3,
                TalentRequired = -1,
                Tooltip = "Your Fire and Frost spell criticals will refund 10% of their base mana cost.",
                Row = 3,
                Column = 3
            },
            new()
            {
                Id = 31638,
                Name = "Playing with Fire",
                WowClass = EnumSim.WowClass.Mage,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Mage_Fire,
                MaxRank = 3,
                TalentRequired = -1,
                Tooltip = "Increases all spell damage caused by 1% and all spell damage taken by 1%.",
                Row = 4,
                Column = 0
            },
            new()
            {
                Id = 31641,
                Name = "Blazing Speed",
                WowClass = EnumSim.WowClass.Mage,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Mage_Fire,
                MaxRank = 2,
                TalentRequired = -1,
                Tooltip =
                    "Gives you a 5% chance when hit by a melee or ranged attack to increase your movement speed by 50% and dispel all movement impairing effects. &nbsp;This effect lasts 8 sec.",
                Row = 5,
                Column = 0
            },
            new()
            {
                Id = 31679,
                Name = "Molten Fury",
                WowClass = EnumSim.WowClass.Mage,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Mage_Fire,
                MaxRank = 2,
                TalentRequired = -1,
                Tooltip = "Increases damage of all spells against targets with less than 20% health by 10%.",
                Row = 6,
                Column = 2
            },
            new()
            {
                Id = 34293,
                Name = "Pyromaniac",
                WowClass = EnumSim.WowClass.Mage,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Mage_Fire,
                MaxRank = 3,
                TalentRequired = -1,
                Tooltip =
                    "Increases chance to critically hit and reduces the mana cost of all Fire spells by an additional 1%.",
                Row = 6,
                Column = 0
            },
            new()
            {
                Id = 31656,
                Name = "Empowered Fireball",
                WowClass = EnumSim.WowClass.Mage,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Mage_Fire,
                MaxRank = 5,
                TalentRequired = -1,
                Tooltip = "Your Fireball spell gains an additional 3% of your bonus spell damage effects.",
                Row = 7,
                Column = 2
            },
            new()
            {
                Id = 31661,
                Name = "Dragon's Breath",
                WowClass = EnumSim.WowClass.Mage,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Mage_Fire,
                MaxRank = 1,
                TalentRequired = -1,
                Tooltip =
                    "Targets in a cone in front of the caster take 382 to 442 Fire damage and are Disoriented for 3 sec. &nbsp;Any direct damaging attack will revive targets. &nbsp;Turns off your attack when used.",
                Row = 8,
                Column = 1
            },
            new()
            {
                Id = 11070,
                Name = "Improved Frostbolt",
                WowClass = EnumSim.WowClass.Mage,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Mage_Frost,
                MaxRank = 5,
                TalentRequired = -1,
                Tooltip = "Reduces the casting time of your Frostbolt spell by 0.1 sec.",
                Row = 0,
                Column = 1
            },
            new()
            {
                Id = 11071,
                Name = "Frostbite",
                WowClass = EnumSim.WowClass.Mage,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Mage_Frost,
                MaxRank = 3,
                TalentRequired = -1,
                Tooltip = "Gives your Chill effects a 5% chance to freeze the target for 5 sec.",
                Row = 1,
                Column = 1
            },
            new()
            {
                Id = 11151,
                Name = "Piercing Ice",
                WowClass = EnumSim.WowClass.Mage,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Mage_Frost,
                MaxRank = 3,
                TalentRequired = -1,
                Tooltip = "Increases the damage done by your Frost spells by 2%.",
                Row = 2,
                Column = 0
            },
            new()
            {
                Id = 11165,
                Name = "Improved Frost Nova",
                WowClass = EnumSim.WowClass.Mage,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Mage_Frost,
                MaxRank = 2,
                TalentRequired = -1,
                Tooltip = "Reduces the cooldown of your Frost Nova spell by 2 sec.",
                Row = 1,
                Column = 2
            },
            new()
            {
                Id = 11185,
                Name = "Improved Blizzard",
                WowClass = EnumSim.WowClass.Mage,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Mage_Frost,
                MaxRank = 3,
                TalentRequired = -1,
                Tooltip =
                    "Adds a chill effect to your Blizzard spell. &nbsp;This effect lowers the target's movement speed by 30%. &nbsp;Lasts 1.50 sec.",
                Row = 2,
                Column = 3
            },
            new()
            {
                Id = 11190,
                Name = "Improved Cone of Cold",
                WowClass = EnumSim.WowClass.Mage,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Mage_Frost,
                MaxRank = 3,
                TalentRequired = -1,
                Tooltip = "Increases the damage dealt by your Cone of Cold spell by 15%.",
                Row = 4,
                Column = 2
            },
            new()
            {
                Id = 11175,
                Name = "Permafrost",
                WowClass = EnumSim.WowClass.Mage,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Mage_Frost,
                MaxRank = 3,
                TalentRequired = -1,
                Tooltip =
                    "Increases the duration of your Chill effects by 1 sec and reduces the target's speed by an additional 4%.",
                Row = 1,
                Column = 3
            },
            new()
            {
                Id = 11160,
                Name = "Frost Channeling",
                WowClass = EnumSim.WowClass.Mage,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Mage_Frost,
                MaxRank = 3,
                TalentRequired = -1,
                Tooltip =
                    "Reduces the mana cost of your Frost spells by 5% and reduces the threat caused by your Frost spells by 4%.",
                Row = 3,
                Column = 1
            },
            new()
            {
                Id = 11170,
                Name = "Shatter",
                WowClass = EnumSim.WowClass.Mage,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Mage_Frost,
                MaxRank = 5,
                TalentRequired = -1,
                Tooltip = "Increases the critical strike chance of all your spells against frozen targets by 10%.",
                Row = 3,
                Column = 2
            },
            new()
            {
                Id = 11180,
                Name = "Winter's Chill",
                WowClass = EnumSim.WowClass.Mage,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Mage_Frost,
                MaxRank = 5,
                TalentRequired = -1,
                Tooltip =
                    "Gives your Frost damage spells a 20% chance to apply the Winter's Chill effect, which increases the chance a Frost spell will critically hit the target by 2% for 15 sec. &nbsp;Stacks up to 5 times.",
                Row = 5,
                Column = 2
            },
            new()
            {
                Id = 12472,
                Name = "Icy Veins",
                WowClass = EnumSim.WowClass.Mage,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Mage_Frost,
                MaxRank = 1,
                TalentRequired = -1,
                Tooltip =
                    "Hastens your spellcasting, increasing spell casting speed by 20% and gives you 100% chance to avoid interruption caused by damage while casting. &nbsp;Lasts 20 sec.",
                Row = 2,
                Column = 1
            },
            new()
            {
                Id = 11189,
                Name = "Frost Warding",
                WowClass = EnumSim.WowClass.Mage,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Mage_Frost,
                MaxRank = 2,
                TalentRequired = -1,
                Tooltip =
                    "Increases the armor and resistances given by your Frost Armor and Ice Armor spells by 15%. &nbsp;In addition, gives your Frost Ward a 10% chance to reflect Frost spells and effects while active.",
                Row = 0,
                Column = 0
            },
            new()
            {
                Id = 11426,
                Name = "Ice Barrier",
                WowClass = EnumSim.WowClass.Mage,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Mage_Frost,
                MaxRank = 1,
                TalentRequired = -1,
                Tooltip =
                    "Instantly shields you, absorbing 455 damage. &nbsp;Lasts 1 min. &nbsp;While the shield holds, spells will not be interrupted.",
                Row = 6,
                Column = 1
            },
            new()
            {
                Id = 11958,
                Name = "Cold Snap",
                WowClass = EnumSim.WowClass.Mage,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Mage_Frost,
                MaxRank = 1,
                TalentRequired = -1,
                Tooltip = "When activated, this spell finishes the cooldown on all Frost spells you recently cast.",
                Row = 4,
                Column = 1
            },
            new()
            {
                Id = 11207,
                Name = "Ice Shards",
                WowClass = EnumSim.WowClass.Mage,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Mage_Frost,
                MaxRank = 5,
                TalentRequired = -1,
                Tooltip = "Increases the critical strike damage bonus of your Frost spells by 20%.",
                Row = 1,
                Column = 0
            },
            new()
            {
                Id = 16757,
                Name = "Arctic Reach",
                WowClass = EnumSim.WowClass.Mage,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Mage_Frost,
                MaxRank = 2,
                TalentRequired = -1,
                Tooltip =
                    "Increases the range of your Frostbolt, Ice Lance and Blizzard spells and the radius of your Frost Nova and Cone of Cold spells by 10%.",
                Row = 3,
                Column = 0
            },
            new()
            {
                Id = 29438,
                Name = "Elemental Precision",
                WowClass = EnumSim.WowClass.Mage,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Mage_Frost,
                MaxRank = 3,
                TalentRequired = -1,
                Tooltip = "Reduces the mana cost and chance targets resist your Frost and Fire spells by 1%.",
                Row = 0,
                Column = 2
            },
            new()
            {
                Id = 31667,
                Name = "Frozen Core",
                WowClass = EnumSim.WowClass.Mage,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Mage_Frost,
                MaxRank = 3,
                TalentRequired = -1,
                Tooltip = "Reduces the damage taken by Frost and Fire effects by 2%.",
                Row = 4,
                Column = 0
            },
            new()
            {
                Id = 31670,
                Name = "Ice Floes",
                WowClass = EnumSim.WowClass.Mage,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Mage_Frost,
                MaxRank = 2,
                TalentRequired = -1,
                Tooltip =
                    "Reduces the cooldown of your Cone of Cold, Cold Snap, Ice Barrier and Ice Block spells by 10%.",
                Row = 5,
                Column = 0
            },
            new()
            {
                Id = 31674,
                Name = "Arctic Winds",
                WowClass = EnumSim.WowClass.Mage,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Mage_Frost,
                MaxRank = 5,
                TalentRequired = -1,
                Tooltip =
                    "Increases all Frost damage you cause by 1% and reduces the chance melee and ranged attacks will hit you by 1%.",
                Row = 6,
                Column = 2
            },
            new()
            {
                Id = 31682,
                Name = "Empowered Frostbolt",
                WowClass = EnumSim.WowClass.Mage,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Mage_Frost,
                MaxRank = 5,
                TalentRequired = -1,
                Tooltip =
                    "Your Frostbolt spell gains an additional 2% of your bonus spell damage effects and an additional 1% chance to critically strike.",
                Row = 7,
                Column = 1
            },
            new()
            {
                Id = 31687,
                Name = "Summon Water Elemental",
                WowClass = EnumSim.WowClass.Mage,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Mage_Frost,
                MaxRank = 1,
                TalentRequired = -1,
                Tooltip = "Summon a Water Elemental to fight for the caster for 45 sec.",
                Row = 8,
                Column = 1
            }
        };
    }
}