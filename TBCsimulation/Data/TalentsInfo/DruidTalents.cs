using System.Collections.Generic;
using TBCsimulation.Data.Utils;

namespace TBCsimulation.Data.TalentsInfo
{
    public static partial class Talents
    {
        public static readonly List<Talent> Druid = new()
        {
            new()
            {
                Id = 16689,
                Name = "Nature's Grasp",
                WowClass = EnumSim.WowClass.Druid,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Druid_Balance,
                MaxRank = 1,
                TalentRequired = -1,
                Tooltip =
                    "While active, any time an enemy strikes the caster they have a 35% chance to become afflicted by Entangling Roots (Rank 1). &nbsp;Only useable outdoors. &nbsp;1 charge. &nbsp;Lasts 45 sec.",
                Row = 0,
                Column = 1
            },
            new()
            {
                Id = 16814,
                Name = "Starlight Wrath",
                WowClass = EnumSim.WowClass.Druid,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Druid_Balance,
                MaxRank = 5,
                TalentRequired = -1,
                Tooltip = "Reduces the cast time of your Wrath and Starfire spells by 0.1 sec.",
                Row = 0,
                Column = 0
            },
            new()
            {
                Id = 16821,
                Name = "Improved Moonfire",
                WowClass = EnumSim.WowClass.Druid,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Druid_Balance,
                MaxRank = 2,
                TalentRequired = -1,
                Tooltip = "Increases the damage and critical strike chance of your Moonfire spell by 5%.",
                Row = 1,
                Column = 2
            },
            new()
            {
                Id = 16819,
                Name = "Nature's Reach",
                WowClass = EnumSim.WowClass.Druid,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Druid_Balance,
                MaxRank = 2,
                TalentRequired = -1,
                Tooltip = "Increases the range of your Balance spells and Faerie Fire (Feral) ability by 10%.",
                Row = 2,
                Column = 3
            },
            new()
            {
                Id = 16836,
                Name = "Brambles",
                WowClass = EnumSim.WowClass.Druid,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Druid_Balance,
                MaxRank = 3,
                TalentRequired = -1,
                Tooltip = "Increases damage caused by your Thorns and Entangling Roots spells by 25%.",
                Row = 2,
                Column = 0
            },
            new()
            {
                Id = 16845,
                Name = "Moonglow",
                WowClass = EnumSim.WowClass.Druid,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Druid_Balance,
                MaxRank = 3,
                TalentRequired = -1,
                Tooltip =
                    "Reduces the Mana cost of your Moonfire, Starfire, Wrath, Healing Touch, Regrowth and Rejuvenation spells by 3%.",
                Row = 4,
                Column = 2
            },
            new()
            {
                Id = 16850,
                Name = "Celestial Focus",
                WowClass = EnumSim.WowClass.Druid,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Druid_Balance,
                MaxRank = 3,
                TalentRequired = -1,
                Tooltip =
                    "Gives your Starfire spell a 5% chance to stun the target for 3 sec and increases the chance you'll resist spell interruption when casting your Wrath spell by 25%.",
                Row = 3,
                Column = 2
            },
            new()
            {
                Id = 16918,
                Name = "Control of Nature",
                WowClass = EnumSim.WowClass.Druid,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Druid_Balance,
                MaxRank = 3,
                TalentRequired = -1,
                Tooltip =
                    "Gives you a 40% chance to avoid interruption caused by damage while casting Entangling Roots and Cyclone.",
                Row = 1,
                Column = 0
            },
            new()
            {
                Id = 5570,
                Name = "Insect Swarm",
                WowClass = EnumSim.WowClass.Druid,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Druid_Balance,
                MaxRank = 1,
                TalentRequired = -1,
                Tooltip =
                    "The enemy target is swarmed by insects, decreasing their chance to hit by 2% and causing 108 Nature damage over 12 sec.",
                Row = 2,
                Column = 2
            },
            new()
            {
                Id = 16880,
                Name = "Nature's Grace",
                WowClass = EnumSim.WowClass.Druid,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Druid_Balance,
                MaxRank = 1,
                TalentRequired = -1,
                Tooltip =
                    "All spell criticals grace you with a blessing of nature, reducing the casting time of your next spell by 0.5 sec.",
                Row = 4,
                Column = 1
            },
            new()
            {
                Id = 16896,
                Name = "Moonfury",
                WowClass = EnumSim.WowClass.Druid,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Druid_Balance,
                MaxRank = 5,
                TalentRequired = -1,
                Tooltip = "Increases the damage done by your Starfire, Moonfire and Wrath spells by 2%.",
                Row = 5,
                Column = 1
            },
            new()
            {
                Id = 16909,
                Name = "Vengeance",
                WowClass = EnumSim.WowClass.Druid,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Druid_Balance,
                MaxRank = 5,
                TalentRequired = -1,
                Tooltip =
                    "Increases the critical strike damage bonus of your Starfire, Moonfire, and Wrath spells by 20%.",
                Row = 3,
                Column = 1
            },
            new()
            {
                Id = 24858,
                Name = "Moonkin Form",
                WowClass = EnumSim.WowClass.Druid,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Druid_Balance,
                MaxRank = 1,
                TalentRequired = -1,
                Tooltip =
                    "Shapeshift into Moonkin Form. &nbsp;While in this form the armor contribution from items is increased by 400%, attack power is increased by 150% of your level and all party members within 30 yards have their spell critical chance increased by 5%. &nbsp;Melee attacks in this form have a chance on hit to regenerate mana based on attack power. &nbsp;The Moonkin can only cast Balance and Remove Curse spells while shapeshifted.The act of shapeshifting frees the caster of Polymorph and Movement Impairing effects.",
                Row = 6,
                Column = 1
            },
            new()
            {
                Id = 17245,
                Name = "Improved Nature's Grasp",
                WowClass = EnumSim.WowClass.Druid,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Druid_Balance,
                MaxRank = 4,
                TalentRequired = -1,
                Tooltip = "Increases the chance for your Nature's Grasp to entangle an enemy by 15%.",
                Row = 0,
                Column = 2
            },
            new()
            {
                Id = 33589,
                Name = "Lunar Guidance",
                WowClass = EnumSim.WowClass.Druid,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Druid_Balance,
                MaxRank = 3,
                TalentRequired = -1,
                Tooltip = "Increases your spell damage and healing by 8% of your total Intellect.",
                Row = 4,
                Column = 0
            },
            new()
            {
                Id = 33592,
                Name = "Balance of Power",
                WowClass = EnumSim.WowClass.Druid,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Druid_Balance,
                MaxRank = 2,
                TalentRequired = -1,
                Tooltip =
                    "Increases your chance to hit with all spells and reduces the chance you'll be hit by spells by 2%.",
                Row = 5,
                Column = 2
            },
            new()
            {
                Id = 33597,
                Name = "Dreamstate",
                WowClass = EnumSim.WowClass.Druid,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Druid_Balance,
                MaxRank = 3,
                TalentRequired = -1,
                Tooltip = "Regenerate mana equal to 4% of your Intellect every 5 sec, even while casting.",
                Row = 6,
                Column = 0
            },
            new()
            {
                Id = 33600,
                Name = "Improved Faerie Fire",
                WowClass = EnumSim.WowClass.Druid,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Druid_Balance,
                MaxRank = 3,
                TalentRequired = -1,
                Tooltip =
                    "Your Faerie Fire spell also increases the chance the target will be hit by melee and ranged attacks by 1%.",
                Row = 6,
                Column = 2
            },
            new()
            {
                Id = 33603,
                Name = "Wrath of Cenarius",
                WowClass = EnumSim.WowClass.Druid,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Druid_Balance,
                MaxRank = 5,
                TalentRequired = -1,
                Tooltip =
                    "Your Starfire spell gains an additional 4% and your Wrath gains an additional 2% of your bonus damage effects.",
                Row = 7,
                Column = 1
            },
            new()
            {
                Id = 33831,
                Name = "Force of Nature",
                WowClass = EnumSim.WowClass.Druid,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Druid_Balance,
                MaxRank = 1,
                TalentRequired = -1,
                Tooltip = "Summons 3 treants to attack enemy targets for 30 sec.",
                Row = 8,
                Column = 1
            },
            new()
            {
                Id = 35363,
                Name = "Focused Starlight",
                WowClass = EnumSim.WowClass.Druid,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Druid_Balance,
                MaxRank = 2,
                TalentRequired = -1,
                Tooltip = "Increases the critical strike chance of your Wrath and Starfire spells by 2%.",
                Row = 1,
                Column = 1
            },
            new()
            {
                Id = 16929,
                Name = "Thick Hide",
                WowClass = EnumSim.WowClass.Druid,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Druid_FeralCombat,
                MaxRank = 3,
                TalentRequired = -1,
                Tooltip = "Increases your Armor contribution from items by 4%.",
                Row = 1,
                Column = 2
            },
            new()
            {
                Id = 16858,
                Name = "Feral Aggression",
                WowClass = EnumSim.WowClass.Druid,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Druid_FeralCombat,
                MaxRank = 5,
                TalentRequired = -1,
                Tooltip =
                    "Increases the attack power reduction of your Demoralizing Roar by 8% and the damage caused by your Ferocious Bite by 3%.",
                Row = 0,
                Column = 2
            },
            new()
            {
                Id = 16934,
                Name = "Ferocity",
                WowClass = EnumSim.WowClass.Druid,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Druid_FeralCombat,
                MaxRank = 5,
                TalentRequired = -1,
                Tooltip = "Reduces the cost of your Maul, Swipe, Claw, Rake and Mangle abilities by 1 Rage or Energy.",
                Row = 0,
                Column = 1
            },
            new()
            {
                Id = 16940,
                Name = "Brutal Impact",
                WowClass = EnumSim.WowClass.Druid,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Druid_FeralCombat,
                MaxRank = 2,
                TalentRequired = -1,
                Tooltip = "Increases the stun duration of your Bash and Pounce abilities by 0.5 sec.",
                Row = 1,
                Column = 1
            },
            new()
            {
                Id = 16942,
                Name = "Sharpened Claws",
                WowClass = EnumSim.WowClass.Druid,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Druid_FeralCombat,
                MaxRank = 3,
                TalentRequired = -1,
                Tooltip = "Increases your critical strike chance while in Bear, Dire Bear or Cat Form by 2%.",
                Row = 2,
                Column = 2
            },
            new()
            {
                Id = 16947,
                Name = "Feral Instinct",
                WowClass = EnumSim.WowClass.Druid,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Druid_FeralCombat,
                MaxRank = 3,
                TalentRequired = -1,
                Tooltip =
                    "Increases threat caused in Bear and Dire Bear Form by 5% and reduces the chance enemies have to detect you while Prowling.",
                Row = 1,
                Column = 0
            },
            new()
            {
                Id = 37116,
                Name = "Primal Fury",
                WowClass = EnumSim.WowClass.Druid,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Druid_FeralCombat,
                MaxRank = 2,
                TalentRequired = -1,
                Tooltip =
                    "Gives you a 50% chance to gain an additional 5 Rage anytime you get a critical strike while in Bear and Dire Bear Form and your critical strikes from Cat Form abilities that add combo points &nbsp;have a 50% chance to add an additional combo point.",
                Row = 3,
                Column = 2
            },
            new()
            {
                Id = 16966,
                Name = "Shredding Attacks",
                WowClass = EnumSim.WowClass.Druid,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Druid_FeralCombat,
                MaxRank = 2,
                TalentRequired = -1,
                Tooltip =
                    "Reduces the energy cost of your Shred ability by 9 and the rage cost of your Lacerate ability by 1.",
                Row = 3,
                Column = 0
            },
            new()
            {
                Id = 16972,
                Name = "Predatory Strikes",
                WowClass = EnumSim.WowClass.Druid,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Druid_FeralCombat,
                MaxRank = 3,
                TalentRequired = -1,
                Tooltip =
                    "Increases your melee attack power in Cat, Bear, Dire Bear and Moonkin Forms by 50% of your level.",
                Row = 3,
                Column = 1
            },
            new()
            {
                Id = 16979,
                Name = "Feral Charge",
                WowClass = EnumSim.WowClass.Druid,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Druid_FeralCombat,
                MaxRank = 1,
                TalentRequired = -1,
                Tooltip =
                    "Causes you to charge an enemy, immobilizing and interrupting any spell being cast for 4 sec.",
                Row = 2,
                Column = 1
            },
            new()
            {
                Id = 16998,
                Name = "Savage Fury",
                WowClass = EnumSim.WowClass.Druid,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Druid_FeralCombat,
                MaxRank = 2,
                TalentRequired = -1,
                Tooltip = "Increases the damage caused by your Claw, Rake, and Mangle (Cat) abilities by 10%.",
                Row = 4,
                Column = 0
            },
            new()
            {
                Id = 17002,
                Name = "Feral Swiftness",
                WowClass = EnumSim.WowClass.Druid,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Druid_FeralCombat,
                MaxRank = 2,
                TalentRequired = -1,
                Tooltip =
                    "Increases your movement speed by 15% while outdoors in Cat Form and increases your chance to dodge while in Cat Form, Bear Form and Dire Bear Form by 2%.",
                Row = 2,
                Column = 0
            },
            new()
            {
                Id = 17003,
                Name = "Heart of the Wild",
                WowClass = EnumSim.WowClass.Druid,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Druid_FeralCombat,
                MaxRank = 5,
                TalentRequired = -1,
                Tooltip =
                    "Increases your Intellect by 4%. &nbsp;In addition, while in Bear or Dire Bear Form your Stamina is increased by 4% and while in Cat Form your attack power is increased by 2%.",
                Row = 5,
                Column = 1
            },
            new()
            {
                Id = 17007,
                Name = "Leader of the Pack",
                WowClass = EnumSim.WowClass.Druid,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Druid_FeralCombat,
                MaxRank = 1,
                TalentRequired = -1,
                Tooltip =
                    "While in Cat, Bear or Dire Bear Form, the Leader of the Pack increases ranged and melee critical chance of all party members within 45 yards by 5%.",
                Row = 6,
                Column = 1
            },
            new()
            {
                Id = 16857,
                Name = "Faerie Fire (Feral)",
                WowClass = EnumSim.WowClass.Druid,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Druid_FeralCombat,
                MaxRank = 1,
                TalentRequired = -1,
                Tooltip =
                    "Decrease the armor of the target by 175 for 40 sec. &nbsp;While affected, the target cannot stealth or turn invisible.",
                Row = 4,
                Column = 2
            },
            new()
            {
                Id = 33872,
                Name = "Nurturing Instinct",
                WowClass = EnumSim.WowClass.Druid,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Druid_FeralCombat,
                MaxRank = 2,
                TalentRequired = -1,
                Tooltip =
                    "Increases your healing spells by up to 50% of your Agility, and increases healing done to you by 10% while in Cat form.",
                Row = 4,
                Column = 3
            },
            new()
            {
                Id = 33851,
                Name = "Primal Tenacity",
                WowClass = EnumSim.WowClass.Druid,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Druid_FeralCombat,
                MaxRank = 3,
                TalentRequired = -1,
                Tooltip = "Increases your chance to resist Stun and Fear mechanics by 5%.",
                Row = 6,
                Column = 0
            },
            new()
            {
                Id = 33853,
                Name = "Survival of the Fittest",
                WowClass = EnumSim.WowClass.Druid,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Druid_FeralCombat,
                MaxRank = 3,
                TalentRequired = -1,
                Tooltip =
                    "Increases all attributes by 1% and reduces the chance you'll be critically hit by melee attacks by 1%.",
                Row = 5,
                Column = 2
            },
            new()
            {
                Id = 33859,
                Name = "Predatory Instincts",
                WowClass = EnumSim.WowClass.Druid,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Druid_FeralCombat,
                MaxRank = 5,
                TalentRequired = -1,
                Tooltip =
                    "While in Cat Form, Bear Form, or Dire Bear Form, increases your damage from melee critical strikes by 2% and your chance to avoid area effect attacks by 3%.",
                Row = 7,
                Column = 2
            },
            new()
            {
                Id = 33917,
                Name = "Mangle",
                WowClass = EnumSim.WowClass.Druid,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Druid_FeralCombat,
                MaxRank = 1,
                TalentRequired = -1,
                Tooltip =
                    "Mangle the target, inflicting damage and causing the target to take additional damage from bleed effects for 12 sec. &nbsp;This ability can be used in Cat Form or Dire Bear Form.",
                Row = 8,
                Column = 1
            },
            new()
            {
                Id = 34297,
                Name = "Improved Leader of the Pack",
                WowClass = EnumSim.WowClass.Druid,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Druid_FeralCombat,
                MaxRank = 2,
                TalentRequired = -1,
                Tooltip =
                    "Your Leader of the Pack ability also causes affected targets to have a 100% chance to heal themselves for 2% of their total health when they critically hit with a melee or ranged attack. &nbsp;The healing effect cannot occur more than once every 6 sec.",
                Row = 6,
                Column = 2
            },
            new()
            {
                Id = 17050,
                Name = "Improved Mark of the Wild",
                WowClass = EnumSim.WowClass.Druid,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Druid_Restoration,
                MaxRank = 5,
                TalentRequired = -1,
                Tooltip = "Increases the effects of your Mark of the Wild and Gift of the Wild spells by 7%.",
                Row = 0,
                Column = 1
            },
            new()
            {
                Id = 17056,
                Name = "Furor",
                WowClass = EnumSim.WowClass.Druid,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Druid_Restoration,
                MaxRank = 5,
                TalentRequired = -1,
                Tooltip =
                    "Gives you 20% chance to gain 10 Rage when you shapeshift into Bear and Dire Bear Form or 40 Energy when you shapeshift into Cat Form.",
                Row = 0,
                Column = 2
            },
            new()
            {
                Id = 17063,
                Name = "Nature's Focus",
                WowClass = EnumSim.WowClass.Druid,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Druid_Restoration,
                MaxRank = 5,
                TalentRequired = -1,
                Tooltip =
                    "Gives you a 14% chance to avoid interruption caused by damage while casting the Healing Touch, Regrowth and Tranquility spells.",
                Row = 1,
                Column = 1
            },
            new()
            {
                Id = 17069,
                Name = "Naturalist",
                WowClass = EnumSim.WowClass.Druid,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Druid_Restoration,
                MaxRank = 5,
                TalentRequired = -1,
                Tooltip =
                    "Reduces the cast time of your Healing Touch spell by 0.1 sec and increases the damage you deal with physical attacks in all forms by 2%.",
                Row = 1,
                Column = 0
            },
            new()
            {
                Id = 17074,
                Name = "Improved Regrowth",
                WowClass = EnumSim.WowClass.Druid,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Druid_Restoration,
                MaxRank = 5,
                TalentRequired = -1,
                Tooltip = "Increases the critical effect chance of your Regrowth spell by 10%.",
                Row = 5,
                Column = 2
            },
            new()
            {
                Id = 16833,
                Name = "Natural Shapeshifter",
                WowClass = EnumSim.WowClass.Druid,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Druid_Restoration,
                MaxRank = 3,
                TalentRequired = -1,
                Tooltip = "Reduces the mana cost of all shapeshifting by 10%.",
                Row = 1,
                Column = 2
            },
            new()
            {
                Id = 16864,
                Name = "Omen of Clarity",
                WowClass = EnumSim.WowClass.Druid,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Druid_Restoration,
                MaxRank = 1,
                TalentRequired = -1,
                Tooltip =
                    "Imbues the Druid with natural energy. &nbsp;Each of the Druid's melee attacks has a chance of causing the caster to enter a Clearcasting state. &nbsp;The Clearcasting state reduces the Mana, Rage or Energy cost of your next damage or healing spell or offensive ability by 100%. &nbsp;Lasts 30 min.",
                Row = 2,
                Column = 2
            },
            new()
            {
                Id = 17104,
                Name = "Gift of Nature",
                WowClass = EnumSim.WowClass.Druid,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Druid_Restoration,
                MaxRank = 5,
                TalentRequired = -1,
                Tooltip = "Increases the effect of all healing spells by 2%.",
                Row = 4,
                Column = 1
            },
            new()
            {
                Id = 17106,
                Name = "Intensity",
                WowClass = EnumSim.WowClass.Druid,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Druid_Restoration,
                MaxRank = 3,
                TalentRequired = -1,
                Tooltip =
                    "Allows 10% of your Mana regeneration to continue while casting and causes your Enrage ability to instantly generate 4 rage.",
                Row = 2,
                Column = 0
            },
            new()
            {
                Id = 17111,
                Name = "Improved Rejuvenation",
                WowClass = EnumSim.WowClass.Druid,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Druid_Restoration,
                MaxRank = 3,
                TalentRequired = -1,
                Tooltip = "Increases the effect of your Rejuvenation spell by 5%.",
                Row = 3,
                Column = 2
            },
            new()
            {
                Id = 17116,
                Name = "Nature's Swiftness",
                WowClass = EnumSim.WowClass.Druid,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Druid_Restoration,
                MaxRank = 1,
                TalentRequired = -1,
                Tooltip = "When activated, your next Nature spell becomes an instant cast spell.",
                Row = 4,
                Column = 0
            },
            new()
            {
                Id = 17118,
                Name = "Subtlety",
                WowClass = EnumSim.WowClass.Druid,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Druid_Restoration,
                MaxRank = 5,
                TalentRequired = -1,
                Tooltip =
                    "Reduces the threat generated by your spells by 4% and reduces the chance your spells will be dispelled by 6%.",
                Row = 2,
                Column = 1
            },
            new()
            {
                Id = 17123,
                Name = "Improved Tranquility",
                WowClass = EnumSim.WowClass.Druid,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Druid_Restoration,
                MaxRank = 2,
                TalentRequired = -1,
                Tooltip = "Reduces threat caused by Tranquility by 50%.",
                Row = 4,
                Column = 3
            },
            new()
            {
                Id = 24968,
                Name = "Tranquil Spirit",
                WowClass = EnumSim.WowClass.Druid,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Druid_Restoration,
                MaxRank = 5,
                TalentRequired = -1,
                Tooltip = "Reduces the mana cost of your Healing Touch and Tranquility spells by 2%.",
                Row = 3,
                Column = 1
            },
            new()
            {
                Id = 18562,
                Name = "Swiftmend",
                WowClass = EnumSim.WowClass.Druid,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Druid_Restoration,
                MaxRank = 1,
                TalentRequired = -1,
                Tooltip =
                    "Consumes a Rejuvenation or Regrowth effect on a friendly target to instantly heal them an amount equal to 12 sec. of Rejuvenation or 18 sec. of Regrowth.",
                Row = 6,
                Column = 1
            },
            new()
            {
                Id = 33879,
                Name = "Empowered Touch",
                WowClass = EnumSim.WowClass.Druid,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Druid_Restoration,
                MaxRank = 2,
                TalentRequired = -1,
                Tooltip = "Your Healing Touch spell gains an additional 10% of your bonus healing effects.",
                Row = 5,
                Column = 0
            },
            new()
            {
                Id = 33886,
                Name = "Empowered Rejuvenation",
                WowClass = EnumSim.WowClass.Druid,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Druid_Restoration,
                MaxRank = 5,
                TalentRequired = -1,
                Tooltip = "The bonus healing effects of your healing over time spells is increased by 4%.",
                Row = 7,
                Column = 1
            },
            new()
            {
                Id = 33881,
                Name = "Natural Perfection",
                WowClass = EnumSim.WowClass.Druid,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Druid_Restoration,
                MaxRank = 3,
                TalentRequired = -1,
                Tooltip =
                    "Your critical strike chance with all spells is increased by 1% and critical strikes against you give you the Natural Perfection effect reducing all damage taken by 2%. &nbsp;Stacks up to 3 times. &nbsp;Lasts 8 sec.",
                Row = 6,
                Column = 2
            },
            new()
            {
                Id = 33891,
                Name = "Tree of Life",
                WowClass = EnumSim.WowClass.Druid,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Druid_Restoration,
                MaxRank = 1,
                TalentRequired = -1,
                Tooltip =
                    "Shapeshift into the Tree of Life. &nbsp;While in this form you increase healing received by 25% of your total Spirit for all party members within 45 yards, your movement speed is reduced by 20%, and you can only cast Swiftmend, Innervate, Nature's Swiftness, Rebirth, Barkskin, poison removing and healing over time spells, but the mana cost of these spells is reduced by 20%.The act of shapeshifting frees the caster of Polymorph and Movement Impairing effects.",
                Row = 8,
                Column = 1
            },
            new()
            {
                Id = 34151,
                Name = "Living Spirit",
                WowClass = EnumSim.WowClass.Druid,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Druid_Restoration,
                MaxRank = 3,
                TalentRequired = -1,
                Tooltip = "Increases your total Spirit by 5%.",
                Row = 6,
                Column = 0
            }
        };
    }
}