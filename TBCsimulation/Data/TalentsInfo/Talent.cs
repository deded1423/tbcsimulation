﻿using TBCsimulation.Data.Utils;

namespace TBCsimulation.Data.TalentsInfo
{
    public class Talent
    {
        internal int Column;
        internal int Id;
        internal int MaxRank;
        internal string Name;
        internal int Row;
        internal EnumSim.SecondarySchoolType SecondarySchoolType;
        internal int TalentRequired = -1;
        internal string Tooltip;
        internal EnumSim.WowClass WowClass;

        public int GetId()
        {
            return Id;
        }

        public string GetName()
        {
            return Name;
        }

        public EnumSim.WowClass GetWowClass()
        {
            return WowClass;
        }

        public EnumSim.SecondarySchoolType GetSecondarySchoolType()
        {
            return SecondarySchoolType;
        }

        public int GetMaxRank()
        {
            return MaxRank;
        }

        public int GetTalentRequired()
        {
            return TalentRequired;
        }

        public bool HasTalentRequired()
        {
            return TalentRequired != -1;
        }

        public string GetTooltip()
        {
            return Tooltip;
        }

        public int GetRow()
        {
            return Row;
        }

        public int GetColumn()
        {
            return Column;
        }
    }
}