using System.Collections.Generic;
using TBCsimulation.Data.Utils;

namespace TBCsimulation.Data.TalentsInfo
{
    public static partial class Talents
    {
        public static readonly List<Talent> Hunter = new()
        {
            new()
            {
                Id = 19549,
                Name = "Improved Aspect of the Monkey",
                WowClass = EnumSim.WowClass.Hunter,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Hunter_BeastMastery,
                MaxRank = 3,
                TalentRequired = -1,
                Tooltip = "Increases the Dodge bonus of your Aspect of the Monkey by 2%.",
                Row = 1,
                Column = 1
            },
            new()
            {
                Id = 19552,
                Name = "Improved Aspect of the Hawk",
                WowClass = EnumSim.WowClass.Hunter,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Hunter_BeastMastery,
                MaxRank = 5,
                TalentRequired = -1,
                Tooltip =
                    "While Aspect of the Hawk is active, all normal ranged attacks have a 10% chance of increasing ranged attack speed by 3% for 12 sec.",
                Row = 0,
                Column = 1
            },
            new()
            {
                Id = 19559,
                Name = "Pathfinding",
                WowClass = EnumSim.WowClass.Hunter,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Hunter_BeastMastery,
                MaxRank = 2,
                TalentRequired = -1,
                Tooltip = "Increases the speed bonus of your Aspect of the Cheetah and Aspect of the Pack by 4%.",
                Row = 2,
                Column = 0
            },
            new()
            {
                Id = 19572,
                Name = "Improved Mend Pet",
                WowClass = EnumSim.WowClass.Hunter,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Hunter_BeastMastery,
                MaxRank = 2,
                TalentRequired = -1,
                Tooltip =
                    "Reduces the mana cost of your Mend Pet spell by 10% and gives the Mend Pet spell a 25% chance of cleansing 1 Curse, Disease, Magic or Poison effect from the pet each tick.",
                Row = 3,
                Column = 1
            },
            new()
            {
                Id = 19574,
                Name = "Bestial Wrath",
                WowClass = EnumSim.WowClass.Hunter,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Hunter_BeastMastery,
                MaxRank = 1,
                TalentRequired = -1,
                Tooltip =
                    "Send your pet into a rage causing 50% additional damage for 18 sec. &nbsp;While enraged, the beast does not feel pity or remorse or fear and it cannot be stopped unless killed.",
                Row = 6,
                Column = 1
            },
            new()
            {
                Id = 19577,
                Name = "Intimidation",
                WowClass = EnumSim.WowClass.Hunter,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Hunter_BeastMastery,
                MaxRank = 1,
                TalentRequired = -1,
                Tooltip =
                    "Command your pet to intimidate the target on the next successful melee attack, causing a high amount of threat and stunning the target for 3 sec.",
                Row = 4,
                Column = 1
            },
            new()
            {
                Id = 19578,
                Name = "Spirit Bond",
                WowClass = EnumSim.WowClass.Hunter,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Hunter_BeastMastery,
                MaxRank = 2,
                TalentRequired = -1,
                Tooltip = "While your pet is active, you and your pet will regenerate 1% of total health every 10 sec.",
                Row = 4,
                Column = 0
            },
            new()
            {
                Id = 19583,
                Name = "Endurance Training",
                WowClass = EnumSim.WowClass.Hunter,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Hunter_BeastMastery,
                MaxRank = 5,
                TalentRequired = -1,
                Tooltip = "Increases the Health of your pet by 2% and your total health by 1%.",
                Row = 0,
                Column = 2
            },
            new()
            {
                Id = 19590,
                Name = "Bestial Discipline",
                WowClass = EnumSim.WowClass.Hunter,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Hunter_BeastMastery,
                MaxRank = 2,
                TalentRequired = -1,
                Tooltip = "Increases the Focus regeneration of your pets by 50%.",
                Row = 4,
                Column = 3
            },
            new()
            {
                Id = 19596,
                Name = "Bestial Swiftness",
                WowClass = EnumSim.WowClass.Hunter,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Hunter_BeastMastery,
                MaxRank = 1,
                TalentRequired = -1,
                Tooltip = "Increases the outdoor movement speed of your pets by 30%.",
                Row = 2,
                Column = 1
            },
            new()
            {
                Id = 19598,
                Name = "Ferocity",
                WowClass = EnumSim.WowClass.Hunter,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Hunter_BeastMastery,
                MaxRank = 5,
                TalentRequired = -1,
                Tooltip = "Increases the critical strike chance of your pet by 2%.",
                Row = 3,
                Column = 2
            },
            new()
            {
                Id = 19609,
                Name = "Thick Hide",
                WowClass = EnumSim.WowClass.Hunter,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Hunter_BeastMastery,
                MaxRank = 3,
                TalentRequired = -1,
                Tooltip = "Increases the armor rating of your pets by 7% and your armor contribution from items by 4%.",
                Row = 1,
                Column = 2
            },
            new()
            {
                Id = 19616,
                Name = "Unleashed Fury",
                WowClass = EnumSim.WowClass.Hunter,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Hunter_BeastMastery,
                MaxRank = 5,
                TalentRequired = -1,
                Tooltip = "Increases the damage done by your pets by 4%.",
                Row = 2,
                Column = 2
            },
            new()
            {
                Id = 19621,
                Name = "Frenzy",
                WowClass = EnumSim.WowClass.Hunter,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Hunter_BeastMastery,
                MaxRank = 5,
                TalentRequired = -1,
                Tooltip =
                    "Gives your pet a 20% chance to gain a 30% attack speed increase for 8 sec after dealing a critical strike.",
                Row = 5,
                Column = 2
            },
            new()
            {
                Id = 35029,
                Name = "Focused Fire",
                WowClass = EnumSim.WowClass.Hunter,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Hunter_BeastMastery,
                MaxRank = 2,
                TalentRequired = -1,
                Tooltip =
                    "All damage caused by you is increased by 1% while your pet is active and the critical strike chance of your Kill Command ability is increased by 10%.",
                Row = 1,
                Column = 0
            },
            new()
            {
                Id = 24443,
                Name = "Improved Revive Pet",
                WowClass = EnumSim.WowClass.Hunter,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Hunter_BeastMastery,
                MaxRank = 2,
                TalentRequired = -1,
                Tooltip =
                    "Revive Pet's casting time is reduced by 3 sec, mana cost is reduced by 20%, and increases the health your pet returns with by an additional 15%.",
                Row = 1,
                Column = 3
            },
            new()
            {
                Id = 34453,
                Name = "Animal Handler",
                WowClass = EnumSim.WowClass.Hunter,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Hunter_BeastMastery,
                MaxRank = 2,
                TalentRequired = -1,
                Tooltip =
                    "Increases your speed while mounted by 4% and your pet's chance to hit by 2%. &nbsp;The mounted movement speed increase does not stack with other effects.",
                Row = 5,
                Column = 0
            },
            new()
            {
                Id = 34455,
                Name = "Ferocious Inspiration",
                WowClass = EnumSim.WowClass.Hunter,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Hunter_BeastMastery,
                MaxRank = 3,
                TalentRequired = -1,
                Tooltip =
                    "When your pet scores a critical hit, all party members have all damage increased by 1% for 10 sec.",
                Row = 6,
                Column = 0
            },
            new()
            {
                Id = 34462,
                Name = "Catlike Reflexes",
                WowClass = EnumSim.WowClass.Hunter,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Hunter_BeastMastery,
                MaxRank = 3,
                TalentRequired = -1,
                Tooltip = "Increases your chance to dodge by 1% and your pet's chance to dodge by an additional 3%.",
                Row = 6,
                Column = 2
            },
            new()
            {
                Id = 34466,
                Name = "Serpent's Swiftness",
                WowClass = EnumSim.WowClass.Hunter,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Hunter_BeastMastery,
                MaxRank = 5,
                TalentRequired = -1,
                Tooltip = "Increases ranged combat attack speed by 4% and your pet's melee attack speed by 4%.",
                Row = 7,
                Column = 2
            },
            new()
            {
                Id = 34692,
                Name = "The Beast Within",
                WowClass = EnumSim.WowClass.Hunter,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Hunter_BeastMastery,
                MaxRank = 1,
                TalentRequired = -1,
                Tooltip =
                    "When your pet is under the effects of Bestial Wrath, you also go into a rage causing 10% additional damage and reducing mana costs of all spells by 20% for 18 sec. &nbsp;While enraged, you do not feel pity or remorse or fear and you cannot be stopped unless killed.",
                Row = 8,
                Column = 1
            },
            new()
            {
                Id = 19407,
                Name = "Improved Concussive Shot",
                WowClass = EnumSim.WowClass.Hunter,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Hunter_Marksmanship,
                MaxRank = 5,
                TalentRequired = -1,
                Tooltip = "Gives your Concussive Shot a 4% chance to stun the target for 3 sec.",
                Row = 0,
                Column = 1
            },
            new()
            {
                Id = 19416,
                Name = "Efficiency",
                WowClass = EnumSim.WowClass.Hunter,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Hunter_Marksmanship,
                MaxRank = 5,
                TalentRequired = -1,
                Tooltip = "Reduces the Mana cost of your Shots and Stings by 2%.",
                Row = 1,
                Column = 2
            },
            new()
            {
                Id = 19421,
                Name = "Improved Hunter's Mark",
                WowClass = EnumSim.WowClass.Hunter,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Hunter_Marksmanship,
                MaxRank = 5,
                TalentRequired = -1,
                Tooltip =
                    "Causes 20% of your Hunter's Mark ability's base attack power to apply to melee attack power as well.",
                Row = 1,
                Column = 1
            },
            new()
            {
                Id = 19426,
                Name = "Lethal Shots",
                WowClass = EnumSim.WowClass.Hunter,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Hunter_Marksmanship,
                MaxRank = 5,
                TalentRequired = -1,
                Tooltip = "Increases your critical strike chance with ranged weapons by 1%.",
                Row = 0,
                Column = 2
            },
            new()
            {
                Id = 19434,
                Name = "Aimed Shot",
                WowClass = EnumSim.WowClass.Hunter,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Hunter_Marksmanship,
                MaxRank = 1,
                TalentRequired = -1,
                Tooltip =
                    "An aimed shot that increases ranged damage by 70 and reduces healing done to that target by 50%. &nbsp;Lasts 10 sec.",
                Row = 2,
                Column = 2
            },
            new()
            {
                Id = 19454,
                Name = "Improved Arcane Shot",
                WowClass = EnumSim.WowClass.Hunter,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Hunter_Marksmanship,
                MaxRank = 5,
                TalentRequired = -1,
                Tooltip = "Reduces the cooldown of your Arcane Shot by 0.2 sec.",
                Row = 2,
                Column = 1
            },
            new()
            {
                Id = 19461,
                Name = "Barrage",
                WowClass = EnumSim.WowClass.Hunter,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Hunter_Marksmanship,
                MaxRank = 3,
                TalentRequired = -1,
                Tooltip = "Increases the damage done by your Multi-Shot and Volley spells by 4%.",
                Row = 4,
                Column = 2
            },
            new()
            {
                Id = 19464,
                Name = "Improved Stings",
                WowClass = EnumSim.WowClass.Hunter,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Hunter_Marksmanship,
                MaxRank = 5,
                TalentRequired = -1,
                Tooltip =
                    "Increases the damage done by your Serpent Sting and Wyvern Sting by 6% and the mana drained by your Viper Sting by 6%. &nbsp;In addition, reduces the chance your Stings will be dispelled by 6%.",
                Row = 3,
                Column = 1
            },
            new()
            {
                Id = 19485,
                Name = "Mortal Shots",
                WowClass = EnumSim.WowClass.Hunter,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Hunter_Marksmanship,
                MaxRank = 5,
                TalentRequired = -1,
                Tooltip = "Increases your ranged weapon critical strike damage bonus by 6%.",
                Row = 3,
                Column = 2
            },
            new()
            {
                Id = 35100,
                Name = "Concussive Barrage",
                WowClass = EnumSim.WowClass.Hunter,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Hunter_Marksmanship,
                MaxRank = 3,
                TalentRequired = -1,
                Tooltip = "Your successful Auto Shot attacks have a 2% chance to Daze the target for 4 sec.",
                Row = 4,
                Column = 0
            },
            new()
            {
                Id = 19503,
                Name = "Scatter Shot",
                WowClass = EnumSim.WowClass.Hunter,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Hunter_Marksmanship,
                MaxRank = 1,
                TalentRequired = -1,
                Tooltip =
                    "A short-range shot that deals 50% weapon damage and disorients the target for 4 sec. &nbsp;Any damage caused will remove the effect. &nbsp;Turns off your attack when used.",
                Row = 4,
                Column = 1
            },
            new()
            {
                Id = 19506,
                Name = "Trueshot Aura",
                WowClass = EnumSim.WowClass.Hunter,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Hunter_Marksmanship,
                MaxRank = 1,
                TalentRequired = -1,
                Tooltip =
                    "Increases the attack power of party members within 45 yards by 50. &nbsp;Lasts until cancelled.",
                Row = 6,
                Column = 1
            },
            new()
            {
                Id = 19507,
                Name = "Ranged Weapon Specialization",
                WowClass = EnumSim.WowClass.Hunter,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Hunter_Marksmanship,
                MaxRank = 5,
                TalentRequired = -1,
                Tooltip = "Increases the damage you deal with ranged weapons by 1%.",
                Row = 5,
                Column = 3
            },
            new()
            {
                Id = 34475,
                Name = "Combat Experience",
                WowClass = EnumSim.WowClass.Hunter,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Hunter_Marksmanship,
                MaxRank = 2,
                TalentRequired = -1,
                Tooltip = "Increases your total Agility by 1% and your total Intellect by 3%.",
                Row = 5,
                Column = 0
            },
            new()
            {
                Id = 34482,
                Name = "Careful Aim",
                WowClass = EnumSim.WowClass.Hunter,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Hunter_Marksmanship,
                MaxRank = 3,
                TalentRequired = -1,
                Tooltip = "Increases your ranged attack power by an amount equal to 15% of your total Intellect.",
                Row = 6,
                Column = 0
            },
            new()
            {
                Id = 34485,
                Name = "Master Marksman",
                WowClass = EnumSim.WowClass.Hunter,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Hunter_Marksmanship,
                MaxRank = 5,
                TalentRequired = -1,
                Tooltip = "Increases your ranged attack power by 2%.",
                Row = 7,
                Column = 1
            },
            new()
            {
                Id = 34490,
                Name = "Silencing Shot",
                WowClass = EnumSim.WowClass.Hunter,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Hunter_Marksmanship,
                MaxRank = 1,
                TalentRequired = -1,
                Tooltip = "A shot that deals 50% weapon damage and Silences the target for 3 sec.",
                Row = 8,
                Column = 1
            },
            new()
            {
                Id = 34950,
                Name = "Go for the Throat",
                WowClass = EnumSim.WowClass.Hunter,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Hunter_Marksmanship,
                MaxRank = 2,
                TalentRequired = -1,
                Tooltip = "Your ranged critical hits cause your pet to generate 25 Focus.",
                Row = 2,
                Column = 0
            },
            new()
            {
                Id = 34948,
                Name = "Rapid Killing",
                WowClass = EnumSim.WowClass.Hunter,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Hunter_Marksmanship,
                MaxRank = 2,
                TalentRequired = -1,
                Tooltip =
                    "Reduces the cooldown of your Rapid Fire ability by 1 min. &nbsp;In addition, after killing an opponent that yields experience or honor, your next Aimed Shot, Arcane Shot or Auto Shot causes 10% additional damage. &nbsp;Lasts 20 sec.",
                Row = 2,
                Column = 3
            },
            new()
            {
                Id = 35104,
                Name = "Improved Barrage",
                WowClass = EnumSim.WowClass.Hunter,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Hunter_Marksmanship,
                MaxRank = 3,
                TalentRequired = -1,
                Tooltip =
                    "Increases the critical strike chance of your Multi-Shot ability by 4% and gives you a 33% chance to avoid interruption caused by damage while channeling Volley.",
                Row = 6,
                Column = 2
            },
            new()
            {
                Id = 19151,
                Name = "Humanoid Slaying",
                WowClass = EnumSim.WowClass.Hunter,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Hunter_Survival,
                MaxRank = 3,
                TalentRequired = -1,
                Tooltip =
                    "Increases all damage caused against Humanoid targets by 1% and increases critical damage caused against Humanoid targets by an additional 1%.",
                Row = 0,
                Column = 1
            },
            new()
            {
                Id = 19168,
                Name = "Lightning Reflexes",
                WowClass = EnumSim.WowClass.Hunter,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Hunter_Survival,
                MaxRank = 5,
                TalentRequired = -1,
                Tooltip = "Increases your Agility by 3%.",
                Row = 5,
                Column = 2
            },
            new()
            {
                Id = 19184,
                Name = "Entrapment",
                WowClass = EnumSim.WowClass.Hunter,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Hunter_Survival,
                MaxRank = 3,
                TalentRequired = -1,
                Tooltip =
                    "Gives your Immolation Trap, Frost Trap, Explosive Trap, and Snake Trap a 8% chance to entrap the target, preventing them from moving for 4 sec.",
                Row = 1,
                Column = 0
            },
            new()
            {
                Id = 19228,
                Name = "Improved Wing Clip",
                WowClass = EnumSim.WowClass.Hunter,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Hunter_Survival,
                MaxRank = 3,
                TalentRequired = -1,
                Tooltip = "Gives your Wing Clip ability a 7% chance to immobilize the target for 5 sec.",
                Row = 1,
                Column = 2
            },
            new()
            {
                Id = 19239,
                Name = "Clever Traps",
                WowClass = EnumSim.WowClass.Hunter,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Hunter_Survival,
                MaxRank = 2,
                TalentRequired = -1,
                Tooltip =
                    "Increases the duration of Freezing and Frost Trap effects by 15%, the damage of Immolation and Explosive Trap effects by 15%, and the number of snakes summoned from Snake Traps by 15%.",
                Row = 2,
                Column = 0
            },
            new()
            {
                Id = 19263,
                Name = "Deterrence",
                WowClass = EnumSim.WowClass.Hunter,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Hunter_Survival,
                MaxRank = 1,
                TalentRequired = -1,
                Tooltip = "When activated, increases your Dodge and Parry chance by 25% for 10 sec.",
                Row = 2,
                Column = 2
            },
            new()
            {
                Id = 19286,
                Name = "Improved Feign Death",
                WowClass = EnumSim.WowClass.Hunter,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Hunter_Survival,
                MaxRank = 2,
                TalentRequired = -1,
                Tooltip = "Reduces the chance your Feign Death ability will be resisted by 2%.",
                Row = 3,
                Column = 3
            },
            new()
            {
                Id = 19290,
                Name = "Surefooted",
                WowClass = EnumSim.WowClass.Hunter,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Hunter_Survival,
                MaxRank = 3,
                TalentRequired = -1,
                Tooltip =
                    "Increases hit chance by 1% and increases the chance movement impairing effects will be resisted by an additional 5%.",
                Row = 3,
                Column = 1
            },
            new()
            {
                Id = 19295,
                Name = "Deflection",
                WowClass = EnumSim.WowClass.Hunter,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Hunter_Survival,
                MaxRank = 5,
                TalentRequired = -1,
                Tooltip = "Increases your Parry chance by 1%.",
                Row = 1,
                Column = 1
            },
            new()
            {
                Id = 19306,
                Name = "Counterattack",
                WowClass = EnumSim.WowClass.Hunter,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Hunter_Survival,
                MaxRank = 1,
                TalentRequired = -1,
                Tooltip =
                    "A strike that becomes active after parrying an opponent's attack. &nbsp;This attack deals 40 damage and immobilizes the target for 5 sec. &nbsp;Counterattack cannot be blocked, dodged, or parried.",
                Row = 4,
                Column = 2
            },
            new()
            {
                Id = 19370,
                Name = "Killer Instinct",
                WowClass = EnumSim.WowClass.Hunter,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Hunter_Survival,
                MaxRank = 3,
                TalentRequired = -1,
                Tooltip = "Increases your critical strike chance with all attacks by 1%.",
                Row = 4,
                Column = 1
            },
            new()
            {
                Id = 19376,
                Name = "Trap Mastery",
                WowClass = EnumSim.WowClass.Hunter,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Hunter_Survival,
                MaxRank = 2,
                TalentRequired = -1,
                Tooltip = "Decreases the chance enemies will resist trap effects by 5%.",
                Row = 3,
                Column = 0
            },
            new()
            {
                Id = 19386,
                Name = "Wyvern Sting",
                WowClass = EnumSim.WowClass.Hunter,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Hunter_Survival,
                MaxRank = 1,
                TalentRequired = -1,
                Tooltip =
                    "A stinging shot that puts the target to sleep for 12 sec. &nbsp;Any damage will cancel the effect. &nbsp;When the target wakes up, the Sting causes 300 Nature damage over 12 sec. &nbsp;Only one Sting per Hunter can be active on the target at a time.",
                Row = 6,
                Column = 1
            },
            new()
            {
                Id = 19159,
                Name = "Savage Strikes",
                WowClass = EnumSim.WowClass.Hunter,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Hunter_Survival,
                MaxRank = 2,
                TalentRequired = -1,
                Tooltip = "Increases the critical strike chance of Raptor Strike and Mongoose Bite by 10%.",
                Row = 0,
                Column = 3
            },
            new()
            {
                Id = 19255,
                Name = "Survivalist",
                WowClass = EnumSim.WowClass.Hunter,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Hunter_Survival,
                MaxRank = 5,
                TalentRequired = -1,
                Tooltip = "Increases total health by 2%.",
                Row = 2,
                Column = 1
            },
            new()
            {
                Id = 24293,
                Name = "Monster Slaying",
                WowClass = EnumSim.WowClass.Hunter,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Hunter_Survival,
                MaxRank = 3,
                TalentRequired = -1,
                Tooltip =
                    "Increases all damage caused against Beasts, Giants and Dragonkin targets by 1% and increases critical damage caused against Beasts, Giants and Dragonkin targets by an additional 1%.",
                Row = 0,
                Column = 0
            },
            new()
            {
                Id = 34491,
                Name = "Resourcefulness",
                WowClass = EnumSim.WowClass.Hunter,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Hunter_Survival,
                MaxRank = 3,
                TalentRequired = -1,
                Tooltip =
                    "Reduces the mana cost of all traps and melee abilities by 20% and reduces the cooldown of all traps by 2 sec.",
                Row = 5,
                Column = 0
            },
            new()
            {
                Id = 34494,
                Name = "Survival Instincts",
                WowClass = EnumSim.WowClass.Hunter,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Hunter_Survival,
                MaxRank = 2,
                TalentRequired = -1,
                Tooltip = "Reduces all damage taken by 2% and increases attack power by 2%.",
                Row = 4,
                Column = 0
            },
            new()
            {
                Id = 34497,
                Name = "Thrill of the Hunt",
                WowClass = EnumSim.WowClass.Hunter,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Hunter_Survival,
                MaxRank = 3,
                TalentRequired = -1,
                Tooltip = "Gives you a 33% chance to regain 40% of the mana cost of any shot when it critically hits.",
                Row = 6,
                Column = 0
            },
            new()
            {
                Id = 34500,
                Name = "Expose Weakness",
                WowClass = EnumSim.WowClass.Hunter,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Hunter_Survival,
                MaxRank = 3,
                TalentRequired = -1,
                Tooltip =
                    "Your ranged criticals have a 33% chance to apply an Expose Weakness effect to the target. Expose Weakness increases the attack power of all attackers against that target by 25% of your Agility for 7 sec.",
                Row = 6,
                Column = 2
            },
            new()
            {
                Id = 34506,
                Name = "Master Tactician",
                WowClass = EnumSim.WowClass.Hunter,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Hunter_Survival,
                MaxRank = 5,
                TalentRequired = -1,
                Tooltip =
                    "Your successful ranged attacks have a 6% chance to increase your critical strike chance with all attacks by 2% for 8 sec.",
                Row = 7,
                Column = 1
            },
            new()
            {
                Id = 23989,
                Name = "Readiness",
                WowClass = EnumSim.WowClass.Hunter,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Hunter_Survival,
                MaxRank = 1,
                TalentRequired = -1,
                Tooltip =
                    "When activated, this ability immediately finishes the cooldown on your other Hunter abilities.",
                Row = 8,
                Column = 1
            },
            new()
            {
                Id = 19498,
                Name = "Hawk Eye",
                WowClass = EnumSim.WowClass.Hunter,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Hunter_Survival,
                MaxRank = 3,
                TalentRequired = -1,
                Tooltip = "Increases the range of your ranged weapons by 2 yards.",
                Row = 0,
                Column = 2
            }
        };
    }
}