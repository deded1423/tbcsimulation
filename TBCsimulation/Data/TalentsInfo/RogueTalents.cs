using System.Collections.Generic;
using TBCsimulation.Data.Utils;

namespace TBCsimulation.Data.TalentsInfo
{
    public static partial class Talents
    {
        public static readonly List<Talent> Rogue = new()
        {
            new()
            {
                Id = 14113,
                Name = "Improved Poisons",
                WowClass = EnumSim.WowClass.Rogue,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Rogue_Assassination,
                MaxRank = 5,
                TalentRequired = -1,
                Tooltip = "Increases the chance to apply poisons to your target by 2%.",
                Row = 3,
                Column = 2
            },
            new()
            {
                Id = 14128,
                Name = "Lethality",
                WowClass = EnumSim.WowClass.Rogue,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Rogue_Assassination,
                MaxRank = 5,
                TalentRequired = -1,
                Tooltip =
                    "Increases the critical strike damage bonus of your Sinister Strike, Gouge, Backstab, Ghostly Strike, Mutilate, Shiv, and Hemorrhage abilities by 6%.",
                Row = 2,
                Column = 2
            },
            new()
            {
                Id = 14138,
                Name = "Malice",
                WowClass = EnumSim.WowClass.Rogue,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Rogue_Assassination,
                MaxRank = 5,
                TalentRequired = -1,
                Tooltip = "Increases your critical strike chance by 1%.",
                Row = 0,
                Column = 2
            },
            new()
            {
                Id = 14144,
                Name = "Remorseless Attacks",
                WowClass = EnumSim.WowClass.Rogue,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Rogue_Assassination,
                MaxRank = 2,
                TalentRequired = -1,
                Tooltip =
                    "After killing an opponent that yields experience or honor, gives you a 20% increased critical strike chance on your next Sinister Strike, Hemorrhage, Backstab, Mutilate, Ambush, or Ghostly Strike. &nbsp;Lasts 20 sec.",
                Row = 0,
                Column = 1
            },
            new()
            {
                Id = 14156,
                Name = "Ruthlessness",
                WowClass = EnumSim.WowClass.Rogue,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Rogue_Assassination,
                MaxRank = 3,
                TalentRequired = -1,
                Tooltip = "Gives your melee finishing moves a 20% chance to add a combo point to your target.",
                Row = 1,
                Column = 0
            },
            new()
            {
                Id = 14158,
                Name = "Murder",
                WowClass = EnumSim.WowClass.Rogue,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Rogue_Assassination,
                MaxRank = 2,
                TalentRequired = -1,
                Tooltip = "Increases all damage caused against Humanoid, Giant, Beast and Dragonkin targets by 1%.",
                Row = 1,
                Column = 1
            },
            new()
            {
                Id = 14162,
                Name = "Improved Eviscerate",
                WowClass = EnumSim.WowClass.Rogue,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Rogue_Assassination,
                MaxRank = 3,
                TalentRequired = -1,
                Tooltip = "Increases the damage done by your Eviscerate ability by 5%.",
                Row = 0,
                Column = 0
            },
            new()
            {
                Id = 13733,
                Name = "Puncturing Wounds",
                WowClass = EnumSim.WowClass.Rogue,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Rogue_Assassination,
                MaxRank = 3,
                TalentRequired = -1,
                Tooltip =
                    "Increases the critical strike chance of your Backstab ability by 10%, and the critical strike chance of your Mutilate ability by 5%.",
                Row = 1,
                Column = 3
            },
            new()
            {
                Id = 14168,
                Name = "Improved Expose Armor",
                WowClass = EnumSim.WowClass.Rogue,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Rogue_Assassination,
                MaxRank = 2,
                TalentRequired = -1,
                Tooltip = "Increases the armor reduced by your Expose Armor ability by 25%.",
                Row = 2,
                Column = 1
            },
            new()
            {
                Id = 14174,
                Name = "Improved Kidney Shot",
                WowClass = EnumSim.WowClass.Rogue,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Rogue_Assassination,
                MaxRank = 3,
                TalentRequired = -1,
                Tooltip =
                    "While affected by your Kidney Shot ability, the target receives an additional 3% damage from all sources.",
                Row = 4,
                Column = 2
            },
            new()
            {
                Id = 14177,
                Name = "Cold Blood",
                WowClass = EnumSim.WowClass.Rogue,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Rogue_Assassination,
                MaxRank = 1,
                TalentRequired = -1,
                Tooltip =
                    "When activated, increases the critical strike chance of your next offensive ability by 100%.",
                Row = 4,
                Column = 1
            },
            new()
            {
                Id = 14179,
                Name = "Relentless Strikes",
                WowClass = EnumSim.WowClass.Rogue,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Rogue_Assassination,
                MaxRank = 1,
                TalentRequired = -1,
                Tooltip = "Your finishing moves have a 20% chance per combo point to restore 25 energy.",
                Row = 2,
                Column = 0
            },
            new()
            {
                Id = 14186,
                Name = "Seal Fate",
                WowClass = EnumSim.WowClass.Rogue,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Rogue_Assassination,
                MaxRank = 5,
                TalentRequired = -1,
                Tooltip =
                    "Your critical strikes from abilities that add combo points have a 20% chance to add an additional combo point.",
                Row = 5,
                Column = 1
            },
            new()
            {
                Id = 14983,
                Name = "Vigor",
                WowClass = EnumSim.WowClass.Rogue,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Rogue_Assassination,
                MaxRank = 1,
                TalentRequired = -1,
                Tooltip = "Increases your maximum Energy by 10.",
                Row = 6,
                Column = 1
            },
            new()
            {
                Id = 16513,
                Name = "Vile Poisons",
                WowClass = EnumSim.WowClass.Rogue,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Rogue_Assassination,
                MaxRank = 5,
                TalentRequired = -1,
                Tooltip =
                    "Increases the damage dealt by your poisons and Envenom ability by 4% and gives your poisons an additional 8% chance to resist dispel effects.",
                Row = 3,
                Column = 1
            },
            new()
            {
                Id = 31226,
                Name = "Master Poisoner",
                WowClass = EnumSim.WowClass.Rogue,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Rogue_Assassination,
                MaxRank = 2,
                TalentRequired = -1,
                Tooltip =
                    "Reduces the chance your poisons will be resisted by 5% and increases your chance to resist Poison effects by an additional 15%.",
                Row = 5,
                Column = 2
            },
            new()
            {
                Id = 31233,
                Name = "Find Weakness",
                WowClass = EnumSim.WowClass.Rogue,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Rogue_Assassination,
                MaxRank = 5,
                TalentRequired = -1,
                Tooltip = "Your finishing moves increase the damage of all your offensive abilities by 2% for 10 sec.",
                Row = 7,
                Column = 2
            },
            new()
            {
                Id = 1329,
                Name = "Mutilate",
                WowClass = EnumSim.WowClass.Rogue,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Rogue_Assassination,
                MaxRank = 1,
                TalentRequired = -1,
                Tooltip =
                    "Instantly attacks with both weapons for an additional 44 with each weapon. &nbsp;Damage is increased by 50% against Poisoned targets. &nbsp;Must be behind the target. &nbsp;Awards 2 combo points.",
                Row = 8,
                Column = 1
            },
            new()
            {
                Id = 31208,
                Name = "Fleet Footed",
                WowClass = EnumSim.WowClass.Rogue,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Rogue_Assassination,
                MaxRank = 2,
                TalentRequired = -1,
                Tooltip =
                    "Increases your chance to resist movement impairing effects by 5% and increases your movement speed by 8%. &nbsp;This does not stack with other movement speed increasing effects.",
                Row = 4,
                Column = 0
            },
            new()
            {
                Id = 31380,
                Name = "Deadened Nerves",
                WowClass = EnumSim.WowClass.Rogue,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Rogue_Assassination,
                MaxRank = 5,
                TalentRequired = -1,
                Tooltip = "Decreases all physical damage taken by 1%.",
                Row = 6,
                Column = 2
            },
            new()
            {
                Id = 31244,
                Name = "Quick Recovery",
                WowClass = EnumSim.WowClass.Rogue,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Rogue_Assassination,
                MaxRank = 2,
                TalentRequired = -1,
                Tooltip =
                    "All healing effects on you are increased by 10%. &nbsp;In addition, your finishing moves cost 40% less Energy when they fail to hit.",
                Row = 4,
                Column = 3
            },
            new()
            {
                Id = 13705,
                Name = "Precision",
                WowClass = EnumSim.WowClass.Rogue,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Rogue_Combat,
                MaxRank = 5,
                TalentRequired = -1,
                Tooltip = "Increases your chance to hit with weapons by 1%.",
                Row = 1,
                Column = 2
            },
            new()
            {
                Id = 13706,
                Name = "Dagger Specialization",
                WowClass = EnumSim.WowClass.Rogue,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Rogue_Combat,
                MaxRank = 5,
                TalentRequired = -1,
                Tooltip = "Increases your chance to get a critical strike with Daggers by 1%.",
                Row = 3,
                Column = 1
            },
            new()
            {
                Id = 13707,
                Name = "Fist Weapon Specialization",
                WowClass = EnumSim.WowClass.Rogue,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Rogue_Combat,
                MaxRank = 5,
                TalentRequired = -1,
                Tooltip = "Increases your chance to get a critical strike with Fist Weapons by 1%.",
                Row = 4,
                Column = 3
            },
            new()
            {
                Id = 13709,
                Name = "Mace Specialization",
                WowClass = EnumSim.WowClass.Rogue,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Rogue_Combat,
                MaxRank = 5,
                TalentRequired = -1,
                Tooltip =
                    "Increases the damage dealt by your critical strikes with maces by 1%, and gives you a 1% chance to stun your target for 3 sec with a mace.",
                Row = 4,
                Column = 0
            },
            new()
            {
                Id = 13712,
                Name = "Lightning Reflexes",
                WowClass = EnumSim.WowClass.Rogue,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Rogue_Combat,
                MaxRank = 5,
                TalentRequired = -1,
                Tooltip = "Increases your Dodge chance by 1%.",
                Row = 0,
                Column = 2
            },
            new()
            {
                Id = 13713,
                Name = "Deflection",
                WowClass = EnumSim.WowClass.Rogue,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Rogue_Combat,
                MaxRank = 5,
                TalentRequired = -1,
                Tooltip = "Increases your Parry chance by 1%.",
                Row = 1,
                Column = 1
            },
            new()
            {
                Id = 13732,
                Name = "Improved Sinister Strike",
                WowClass = EnumSim.WowClass.Rogue,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Rogue_Combat,
                MaxRank = 2,
                TalentRequired = -1,
                Tooltip = "Reduces the Energy cost of your Sinister Strike ability by 3.",
                Row = 0,
                Column = 1
            },
            new()
            {
                Id = 13741,
                Name = "Improved Gouge",
                WowClass = EnumSim.WowClass.Rogue,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Rogue_Combat,
                MaxRank = 3,
                TalentRequired = -1,
                Tooltip = "Increases the effect duration of your Gouge ability by 0.5 sec.",
                Row = 0,
                Column = 0
            },
            new()
            {
                Id = 13742,
                Name = "Endurance",
                WowClass = EnumSim.WowClass.Rogue,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Rogue_Combat,
                MaxRank = 2,
                TalentRequired = -1,
                Tooltip = "Reduces the cooldown of your Sprint and Evasion abilities by 45 sec.",
                Row = 2,
                Column = 0
            },
            new()
            {
                Id = 13750,
                Name = "Adrenaline Rush",
                WowClass = EnumSim.WowClass.Rogue,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Rogue_Combat,
                MaxRank = 1,
                TalentRequired = -1,
                Tooltip = "Increases your Energy regeneration rate by 100% for 15 sec.",
                Row = 6,
                Column = 1
            },
            new()
            {
                Id = 13754,
                Name = "Improved Kick",
                WowClass = EnumSim.WowClass.Rogue,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Rogue_Combat,
                MaxRank = 2,
                TalentRequired = -1,
                Tooltip = "Gives your Kick ability a 50% chance to silence the target for 2 sec.",
                Row = 3,
                Column = 0
            },
            new()
            {
                Id = 13715,
                Name = "Dual Wield Specialization",
                WowClass = EnumSim.WowClass.Rogue,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Rogue_Combat,
                MaxRank = 5,
                TalentRequired = -1,
                Tooltip = "Increases the damage done by your offhand weapon by 10%.",
                Row = 3,
                Column = 2
            },
            new()
            {
                Id = 13743,
                Name = "Improved Sprint",
                WowClass = EnumSim.WowClass.Rogue,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Rogue_Combat,
                MaxRank = 2,
                TalentRequired = -1,
                Tooltip =
                    "Gives a 50% chance to remove all Movement Impairing effects when you activate your Sprint ability.",
                Row = 2,
                Column = 3
            },
            new()
            {
                Id = 13877,
                Name = "Blade Flurry",
                WowClass = EnumSim.WowClass.Rogue,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Rogue_Combat,
                MaxRank = 1,
                TalentRequired = -1,
                Tooltip =
                    "Increases your attack speed by 20%. &nbsp;In addition, attacks strike an additional nearby opponent. &nbsp;Lasts 15 sec.",
                Row = 4,
                Column = 1
            },
            new()
            {
                Id = 13960,
                Name = "Sword Specialization",
                WowClass = EnumSim.WowClass.Rogue,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Rogue_Combat,
                MaxRank = 5,
                TalentRequired = -1,
                Tooltip =
                    "Gives you a 1% chance to get an extra attack on the same target after hitting your target with your Sword.",
                Row = 4,
                Column = 2
            },
            new()
            {
                Id = 14251,
                Name = "Riposte",
                WowClass = EnumSim.WowClass.Rogue,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Rogue_Combat,
                MaxRank = 1,
                TalentRequired = -1,
                Tooltip =
                    "A strike that becomes active after parrying an opponent's attack. &nbsp;This attack deals 150% weapon damage and disarms the target for 6 sec.",
                Row = 2,
                Column = 1
            },
            new()
            {
                Id = 18427,
                Name = "Aggression",
                WowClass = EnumSim.WowClass.Rogue,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Rogue_Combat,
                MaxRank = 3,
                TalentRequired = -1,
                Tooltip = "Increases the damage of your Sinister Strike, Backstab, and Eviscerate abilities by 2%.",
                Row = 5,
                Column = 2
            },
            new()
            {
                Id = 30919,
                Name = "Weapon Expertise",
                WowClass = EnumSim.WowClass.Rogue,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Rogue_Combat,
                MaxRank = 2,
                TalentRequired = -1,
                Tooltip = "Increases your expertise by 5.",
                Row = 5,
                Column = 1
            },
            new()
            {
                Id = 31122,
                Name = "Vitality",
                WowClass = EnumSim.WowClass.Rogue,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Rogue_Combat,
                MaxRank = 2,
                TalentRequired = -1,
                Tooltip = "Increases your total Stamina by 2% and your total Agility by 1%.",
                Row = 6,
                Column = 0
            },
            new()
            {
                Id = 31124,
                Name = "Blade Twisting",
                WowClass = EnumSim.WowClass.Rogue,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Rogue_Combat,
                MaxRank = 2,
                TalentRequired = -1,
                Tooltip =
                    "Gives your Sinister Strike, Backstab, Gouge and Shiv abilities a 10% chance to Daze the target for 8 sec.",
                Row = 5,
                Column = 0
            },
            new()
            {
                Id = 31130,
                Name = "Nerves of Steel",
                WowClass = EnumSim.WowClass.Rogue,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Rogue_Combat,
                MaxRank = 2,
                TalentRequired = -1,
                Tooltip = "Increases your chance to resist Stun and Fear effects by an additional 5%.",
                Row = 6,
                Column = 2
            },
            new()
            {
                Id = 32601,
                Name = "Surprise Attacks",
                WowClass = EnumSim.WowClass.Rogue,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Rogue_Combat,
                MaxRank = 1,
                TalentRequired = -1,
                Tooltip =
                    "Your finishing moves can no longer be dodged, and the damage dealt by your Sinister Strike, Backstab, Shiv and Gouge abilities is increased by 10%.",
                Row = 8,
                Column = 1
            },
            new()
            {
                Id = 35541,
                Name = "Combat Potency",
                WowClass = EnumSim.WowClass.Rogue,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Rogue_Combat,
                MaxRank = 5,
                TalentRequired = -1,
                Tooltip = "Gives your successful off-hand melee attacks a 20% chance to generate 3 Energy.",
                Row = 7,
                Column = 2
            },
            new()
            {
                Id = 14165,
                Name = "Improved Slice and Dice",
                WowClass = EnumSim.WowClass.Rogue,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Rogue_Combat,
                MaxRank = 3,
                TalentRequired = -1,
                Tooltip = "Increases the duration of your Slice and Dice ability by 15%.",
                Row = 1,
                Column = 0
            },
            new()
            {
                Id = 13958,
                Name = "Master of Deception",
                WowClass = EnumSim.WowClass.Rogue,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Rogue_Subtlety,
                MaxRank = 5,
                TalentRequired = -1,
                Tooltip = "Reduces the chance enemies have to detect you while in Stealth mode.",
                Row = 0,
                Column = 1
            },
            new()
            {
                Id = 13975,
                Name = "Camouflage",
                WowClass = EnumSim.WowClass.Rogue,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Rogue_Subtlety,
                MaxRank = 5,
                TalentRequired = -1,
                Tooltip =
                    "Increases your speed while stealthed by 3% and reduces the cooldown of your Stealth ability by 1 sec.",
                Row = 1,
                Column = 2
            },
            new()
            {
                Id = 13976,
                Name = "Initiative",
                WowClass = EnumSim.WowClass.Rogue,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Rogue_Subtlety,
                MaxRank = 3,
                TalentRequired = -1,
                Tooltip =
                    "Gives you a 25% chance to add an additional combo point to your target when using your Ambush, Garrote, or Cheap Shot ability.",
                Row = 2,
                Column = 0
            },
            new()
            {
                Id = 13983,
                Name = "Setup",
                WowClass = EnumSim.WowClass.Rogue,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Rogue_Subtlety,
                MaxRank = 3,
                TalentRequired = -1,
                Tooltip =
                    "Gives you a 15% chance to add a combo point to your target after dodging their attack or fully resisting one of their spells.",
                Row = 3,
                Column = 0
            },
            new()
            {
                Id = 13981,
                Name = "Elusiveness",
                WowClass = EnumSim.WowClass.Rogue,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Rogue_Subtlety,
                MaxRank = 2,
                TalentRequired = -1,
                Tooltip = "Reduces the cooldown of your Vanish and Blind abilities by 45 sec.",
                Row = 3,
                Column = 1
            },
            new()
            {
                Id = 14057,
                Name = "Opportunity",
                WowClass = EnumSim.WowClass.Rogue,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Rogue_Subtlety,
                MaxRank = 5,
                TalentRequired = -1,
                Tooltip =
                    "Increases the damage dealt when striking from behind with your Backstab, Mutilate, Garrote and Ambush abilities by 4%.",
                Row = 0,
                Column = 2
            },
            new()
            {
                Id = 14076,
                Name = "Dirty Tricks",
                WowClass = EnumSim.WowClass.Rogue,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Rogue_Subtlety,
                MaxRank = 2,
                TalentRequired = -1,
                Tooltip =
                    "Increases the range of your Blind and Sap abilities by 2 yards and reduces the energy cost of your Blind and Sap abilities by 25%.",
                Row = 1,
                Column = 1
            },
            new()
            {
                Id = 14079,
                Name = "Improved Ambush",
                WowClass = EnumSim.WowClass.Rogue,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Rogue_Subtlety,
                MaxRank = 3,
                TalentRequired = -1,
                Tooltip = "Increases the critical strike chance of your Ambush ability by 15%.",
                Row = 2,
                Column = 2
            },
            new()
            {
                Id = 14082,
                Name = "Dirty Deeds",
                WowClass = EnumSim.WowClass.Rogue,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Rogue_Subtlety,
                MaxRank = 2,
                TalentRequired = -1,
                Tooltip =
                    "Reduces the Energy cost of your Cheap Shot and Garrote abilities by 10. &nbsp;Additionally, your special abilities cause 10% more damage against targets below 35% health.",
                Row = 4,
                Column = 2
            },
            new()
            {
                Id = 14185,
                Name = "Preparation",
                WowClass = EnumSim.WowClass.Rogue,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Rogue_Subtlety,
                MaxRank = 1,
                TalentRequired = -1,
                Tooltip =
                    "When activated, this ability immediately finishes the cooldown on your Evasion, Sprint, Vanish, Cold Blood, Shadowstep and Premeditation abilities.",
                Row = 4,
                Column = 1
            },
            new()
            {
                Id = 14278,
                Name = "Ghostly Strike",
                WowClass = EnumSim.WowClass.Rogue,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Rogue_Subtlety,
                MaxRank = 1,
                TalentRequired = -1,
                Tooltip =
                    "A strike that deals 125% weapon damage and increases your chance to dodge by 15% for 7 sec. &nbsp;Awards 1 combo point.",
                Row = 2,
                Column = 1
            },
            new()
            {
                Id = 14183,
                Name = "Premeditation",
                WowClass = EnumSim.WowClass.Rogue,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Rogue_Subtlety,
                MaxRank = 1,
                TalentRequired = -1,
                Tooltip =
                    "When used, adds 2 combo points to your target. &nbsp;You must add to or use those combo points within 10 sec or the combo points are lost.",
                Row = 6,
                Column = 1
            },
            new()
            {
                Id = 16511,
                Name = "Hemorrhage",
                WowClass = EnumSim.WowClass.Rogue,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Rogue_Subtlety,
                MaxRank = 1,
                TalentRequired = -1,
                Tooltip =
                    "An instant strike that deals 110% weapon damage and causes the target to hemorrhage, increasing any Physical damage dealt to the target by up to 13. &nbsp;Lasts 10 charges or 15 sec. &nbsp;Awards 1 combo point.",
                Row = 4,
                Column = 3
            },
            new()
            {
                Id = 14171,
                Name = "Serrated Blades",
                WowClass = EnumSim.WowClass.Rogue,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Rogue_Subtlety,
                MaxRank = 3,
                TalentRequired = -1,
                Tooltip =
                    "Causes your attacks to ignore 0 of your target's Armor and increases the damage dealt by your Rupture ability by 10%. &nbsp;The amount of Armor reduced increases with your level.",
                Row = 3,
                Column = 2
            },
            new()
            {
                Id = 30892,
                Name = "Sleight of Hand",
                WowClass = EnumSim.WowClass.Rogue,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Rogue_Subtlety,
                MaxRank = 2,
                TalentRequired = -1,
                Tooltip =
                    "Reduces the chance you are critically hit by melee and ranged attacks by 1% and increases the threat reduction of your Feint ability by 10%.",
                Row = 1,
                Column = 0
            },
            new()
            {
                Id = 30894,
                Name = "Heightened Senses",
                WowClass = EnumSim.WowClass.Rogue,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Rogue_Subtlety,
                MaxRank = 2,
                TalentRequired = -1,
                Tooltip =
                    "Increases your Stealth detection and reduces the chance you are hit by spells and ranged attacks by 2%.",
                Row = 4,
                Column = 0
            },
            new()
            {
                Id = 30902,
                Name = "Deadliness",
                WowClass = EnumSim.WowClass.Rogue,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Rogue_Subtlety,
                MaxRank = 5,
                TalentRequired = -1,
                Tooltip = "Increases your attack power by 2%.",
                Row = 5,
                Column = 2
            },
            new()
            {
                Id = 31211,
                Name = "Enveloping Shadows",
                WowClass = EnumSim.WowClass.Rogue,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Rogue_Subtlety,
                MaxRank = 3,
                TalentRequired = -1,
                Tooltip = "Increases your chance to avoid area of effect attacks by an additional 5%.",
                Row = 6,
                Column = 0
            },
            new()
            {
                Id = 31216,
                Name = "Sinister Calling",
                WowClass = EnumSim.WowClass.Rogue,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Rogue_Subtlety,
                MaxRank = 5,
                TalentRequired = -1,
                Tooltip =
                    "Increases your total Agility by 3% and increases the percentage damage bonus of Backstab and Hemorrhage by an additional 1%.",
                Row = 7,
                Column = 1
            },
            new()
            {
                Id = 31221,
                Name = "Master of Subtlety",
                WowClass = EnumSim.WowClass.Rogue,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Rogue_Subtlety,
                MaxRank = 3,
                TalentRequired = -1,
                Tooltip =
                    "Attacks made while stealthed and for 6 seconds after breaking stealth cause an additional 4% damage.",
                Row = 5,
                Column = 0
            },
            new()
            {
                Id = 36554,
                Name = "Shadowstep",
                WowClass = EnumSim.WowClass.Rogue,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Rogue_Subtlety,
                MaxRank = 1,
                TalentRequired = -1,
                Tooltip =
                    "Attempts to step through the shadows and reappear behind your enemy and increases movement speed by 70% for 3 sec. &nbsp;The damage of your next ability is increased by 20% and the threat caused is reduced by 50%. &nbsp;Lasts 10 sec.",
                Row = 8,
                Column = 1
            },
            new()
            {
                Id = 31228,
                Name = "Cheat Death",
                WowClass = EnumSim.WowClass.Rogue,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Rogue_Subtlety,
                MaxRank = 3,
                TalentRequired = -1,
                Tooltip =
                    "You have a 33% chance that an attack which would otherwise kill you will instead reduce you to 10% of your maximum health. In addition, all damage taken will be reduced by up to 90% for 3 sec (modified by resilience). &nbsp;This effect cannot occur more than once per minute.",
                Row = 6,
                Column = 2
            }
        };
    }
}