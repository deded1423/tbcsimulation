using System.Collections.Generic;
using TBCsimulation.Data.Utils;

namespace TBCsimulation.Data.TalentsInfo
{
    public static partial class Talents
    {
        public static readonly List<Talent> Warlock = new()
        {
            new()
            {
                Id = 17783,
                Name = "Fel Concentration",
                WowClass = EnumSim.WowClass.Warlock,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Warlock_Affliction,
                MaxRank = 5,
                TalentRequired = -1,
                Tooltip =
                    "Gives you a 14% chance to avoid interruption caused by damage while channeling the Drain Life, Drain Mana, or Drain Soul spell.",
                Row = 2,
                Column = 1
            },
            new()
            {
                Id = 18094,
                Name = "Nightfall",
                WowClass = EnumSim.WowClass.Warlock,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Warlock_Affliction,
                MaxRank = 2,
                TalentRequired = -1,
                Tooltip =
                    "Gives your Corruption and Drain Life spells a 2% chance to cause you to enter a Shadow Trance state after damaging the opponent. &nbsp;The Shadow Trance state reduces the casting time of your next Shadow Bolt spell by 100%.",
                Row = 3,
                Column = 1
            },
            new()
            {
                Id = 17810,
                Name = "Improved Corruption",
                WowClass = EnumSim.WowClass.Warlock,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Warlock_Affliction,
                MaxRank = 5,
                TalentRequired = -1,
                Tooltip = "Reduces the casting time of your Corruption spell by 0.4 sec.",
                Row = 0,
                Column = 2
            },
            new()
            {
                Id = 17804,
                Name = "Soul Siphon",
                WowClass = EnumSim.WowClass.Warlock,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Warlock_Affliction,
                MaxRank = 2,
                TalentRequired = -1,
                Tooltip =
                    "Increases the amount drained by your Drain Life spell by an additional 2% for each Affliction effect on the target, up to a maximum of 24% additional effect.",
                Row = 1,
                Column = 3
            },
            new()
            {
                Id = 18174,
                Name = "Suppression",
                WowClass = EnumSim.WowClass.Warlock,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Warlock_Affliction,
                MaxRank = 5,
                TalentRequired = -1,
                Tooltip = "Reduces the chance for enemies to resist your Affliction spells by 2%.",
                Row = 0,
                Column = 1
            },
            new()
            {
                Id = 18179,
                Name = "Improved Curse of Weakness",
                WowClass = EnumSim.WowClass.Warlock,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Warlock_Affliction,
                MaxRank = 2,
                TalentRequired = -1,
                Tooltip = "Increases the effect of your Curse of Weakness by 10%.",
                Row = 1,
                Column = 0
            },
            new()
            {
                Id = 18182,
                Name = "Improved Life Tap",
                WowClass = EnumSim.WowClass.Warlock,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Warlock_Affliction,
                MaxRank = 2,
                TalentRequired = -1,
                Tooltip = "Increases the amount of Mana awarded by your Life Tap spell by 10%.",
                Row = 1,
                Column = 2
            },
            new()
            {
                Id = 18218,
                Name = "Grim Reach",
                WowClass = EnumSim.WowClass.Warlock,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Warlock_Affliction,
                MaxRank = 2,
                TalentRequired = -1,
                Tooltip = "Increases the range of your Affliction spells by 10%.",
                Row = 3,
                Column = 0
            },
            new()
            {
                Id = 18220,
                Name = "Dark Pact",
                WowClass = EnumSim.WowClass.Warlock,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Warlock_Affliction,
                MaxRank = 1,
                TalentRequired = -1,
                Tooltip = "Drains 305 of your pet's Mana, returning 100% to you.",
                Row = 6,
                Column = 2
            },
            new()
            {
                Id = 18265,
                Name = "Siphon Life",
                WowClass = EnumSim.WowClass.Warlock,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Warlock_Affliction,
                MaxRank = 1,
                TalentRequired = -1,
                Tooltip = "Transfers 15 health from the target to the caster every 3 sec. &nbsp;Lasts 30 sec.",
                Row = 4,
                Column = 1
            },
            new()
            {
                Id = 18271,
                Name = "Shadow Mastery",
                WowClass = EnumSim.WowClass.Warlock,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Warlock_Affliction,
                MaxRank = 5,
                TalentRequired = -1,
                Tooltip = "Increases the damage dealt or life drained by your Shadow spells by 2%.",
                Row = 5,
                Column = 1
            },
            new()
            {
                Id = 18288,
                Name = "Amplify Curse",
                WowClass = EnumSim.WowClass.Warlock,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Warlock_Affliction,
                MaxRank = 1,
                TalentRequired = -1,
                Tooltip =
                    "Increases the effect of your next Curse of Doom or Curse of Agony by 50%, or your next Curse of Exhaustion by an additional 20%. &nbsp;Lasts 30 sec.",
                Row = 2,
                Column = 2
            },
            new()
            {
                Id = 18223,
                Name = "Curse of Exhaustion",
                WowClass = EnumSim.WowClass.Warlock,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Warlock_Affliction,
                MaxRank = 1,
                TalentRequired = -1,
                Tooltip =
                    "Reduces the target's movement speed by 30% for 12 sec. &nbsp;Only one Curse per Warlock can be active on any one target.",
                Row = 4,
                Column = 2
            },
            new()
            {
                Id = 18213,
                Name = "Improved Drain Soul",
                WowClass = EnumSim.WowClass.Warlock,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Warlock_Affliction,
                MaxRank = 2,
                TalentRequired = -1,
                Tooltip =
                    "Returns 7% of your maximum mana if the target is killed by you while you drain its soul. &nbsp;In addition, your Affliction spells generate 5% less threat.",
                Row = 1,
                Column = 1
            },
            new()
            {
                Id = 18827,
                Name = "Improved Curse of Agony",
                WowClass = EnumSim.WowClass.Warlock,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Warlock_Affliction,
                MaxRank = 2,
                TalentRequired = -1,
                Tooltip = "Increases the damage done by your Curse of Agony by 5%.",
                Row = 2,
                Column = 0
            },
            new()
            {
                Id = 32477,
                Name = "Malediction",
                WowClass = EnumSim.WowClass.Warlock,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Warlock_Affliction,
                MaxRank = 3,
                TalentRequired = -1,
                Tooltip = "Increases the damage bonus effect of your Curse of the Elements spell by an additional 1%.",
                Row = 7,
                Column = 2
            },
            new()
            {
                Id = 30054,
                Name = "Improved Howl of Terror",
                WowClass = EnumSim.WowClass.Warlock,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Warlock_Affliction,
                MaxRank = 2,
                TalentRequired = -1,
                Tooltip = "Reduces the casting time of your Howl of Terror spell by 0.8 sec.",
                Row = 7,
                Column = 0
            },
            new()
            {
                Id = 30060,
                Name = "Contagion",
                WowClass = EnumSim.WowClass.Warlock,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Warlock_Affliction,
                MaxRank = 5,
                TalentRequired = -1,
                Tooltip =
                    "Increases the damage of Curse of Agony, Corruption and Seed of Corruption by 1% and reduces the chance your Affliction spells will be dispelled by an additional 6%.",
                Row = 6,
                Column = 1
            },
            new()
            {
                Id = 30108,
                Name = "Unstable Affliction",
                WowClass = EnumSim.WowClass.Warlock,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Warlock_Affliction,
                MaxRank = 1,
                TalentRequired = -1,
                Tooltip =
                    "Shadow energy slowly destroys the target, causing 660 damage over 18 sec. &nbsp;In addition, if the Unstable Affliction is dispelled it will cause 990 damage to the dispeller and silence them for 5 sec.",
                Row = 8,
                Column = 1
            },
            new()
            {
                Id = 32385,
                Name = "Shadow Embrace",
                WowClass = EnumSim.WowClass.Warlock,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Warlock_Affliction,
                MaxRank = 5,
                TalentRequired = -1,
                Tooltip =
                    "Your Corruption, Curse of Agony, Siphon Life and Seed of Corruption spells also cause the Shadow Embrace effect, which reduces physical damage caused by 1%.",
                Row = 4,
                Column = 0
            },
            new()
            {
                Id = 32381,
                Name = "Empowered Corruption",
                WowClass = EnumSim.WowClass.Warlock,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Warlock_Affliction,
                MaxRank = 3,
                TalentRequired = -1,
                Tooltip = "Your Corruption spell gains an additional 12% of your bonus spell damage effects.",
                Row = 3,
                Column = 3
            },
            new()
            {
                Id = 18692,
                Name = "Improved Healthstone",
                WowClass = EnumSim.WowClass.Warlock,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Warlock_Demonology,
                MaxRank = 2,
                TalentRequired = -1,
                Tooltip = "Increases the amount of Health restored by your Healthstone by 10%.",
                Row = 0,
                Column = 0
            },
            new()
            {
                Id = 18694,
                Name = "Improved Imp",
                WowClass = EnumSim.WowClass.Warlock,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Warlock_Demonology,
                MaxRank = 3,
                TalentRequired = -1,
                Tooltip = "Increases the effect of your Imp's Firebolt, Fire Shield, and Blood Pact spells by 10%.",
                Row = 0,
                Column = 1
            },
            new()
            {
                Id = 18697,
                Name = "Demonic Embrace",
                WowClass = EnumSim.WowClass.Warlock,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Warlock_Demonology,
                MaxRank = 5,
                TalentRequired = -1,
                Tooltip = "Increases your total Stamina by 3% but reduces your total Spirit by 1%.",
                Row = 0,
                Column = 2
            },
            new()
            {
                Id = 18703,
                Name = "Improved Health Funnel",
                WowClass = EnumSim.WowClass.Warlock,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Warlock_Demonology,
                MaxRank = 2,
                TalentRequired = -1,
                Tooltip =
                    "Increases the amount of Health transferred by your Health Funnel spell by 10% and reduces the initial health cost by 10%.",
                Row = 1,
                Column = 0
            },
            new()
            {
                Id = 18705,
                Name = "Improved Voidwalker",
                WowClass = EnumSim.WowClass.Warlock,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Warlock_Demonology,
                MaxRank = 3,
                TalentRequired = -1,
                Tooltip =
                    "Increases the effectiveness of your Voidwalker's Torment, Consume Shadows, Sacrifice and Suffering spells by 10%.",
                Row = 1,
                Column = 1
            },
            new()
            {
                Id = 18708,
                Name = "Fel Domination",
                WowClass = EnumSim.WowClass.Warlock,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Warlock_Demonology,
                MaxRank = 1,
                TalentRequired = -1,
                Tooltip =
                    "Your next Imp, Voidwalker, Succubus, Felhunter or Felguard Summon spell has its casting time reduced by 5.5 sec and its Mana cost reduced by 50%.",
                Row = 2,
                Column = 1
            },
            new()
            {
                Id = 18709,
                Name = "Master Summoner",
                WowClass = EnumSim.WowClass.Warlock,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Warlock_Demonology,
                MaxRank = 2,
                TalentRequired = -1,
                Tooltip =
                    "Reduces the casting time of your Imp, Voidwalker, Succubus, Felhunter and Fel Guard Summoning spells by 2 sec and the Mana cost by 20%.",
                Row = 3,
                Column = 1
            },
            new()
            {
                Id = 18748,
                Name = "Fel Stamina",
                WowClass = EnumSim.WowClass.Warlock,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Warlock_Demonology,
                MaxRank = 3,
                TalentRequired = -1,
                Tooltip =
                    "Increases the Stamina of your Imp, Voidwalker, Succubus, Felhunter and Felguard by 5% and increases your maximum health by 1%.",
                Row = 2,
                Column = 2
            },
            new()
            {
                Id = 18731,
                Name = "Fel Intellect",
                WowClass = EnumSim.WowClass.Warlock,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Warlock_Demonology,
                MaxRank = 3,
                TalentRequired = -1,
                Tooltip =
                    "Increases the Intellect of your Imp, Voidwalker, Succubus, Felhunter and Felguard by 5% and increases your maximum mana by 1%.",
                Row = 1,
                Column = 2
            },
            new()
            {
                Id = 18754,
                Name = "Improved Succubus",
                WowClass = EnumSim.WowClass.Warlock,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Warlock_Demonology,
                MaxRank = 3,
                TalentRequired = -1,
                Tooltip =
                    "Increases the effect of your Succubus' Lash of Pain and Soothing Kiss spells by 10%, and increases the duration of your Succubus' Seduction and Lesser Invisibility spells by 10%.",
                Row = 2,
                Column = 0
            },
            new()
            {
                Id = 23785,
                Name = "Master Demonologist",
                WowClass = EnumSim.WowClass.Warlock,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Warlock_Demonology,
                MaxRank = 5,
                TalentRequired = -1,
                Tooltip =
                    "Grants both the Warlock and the summoned demon an effect as long as that demon is active.Imp - Reduces threat caused by 4%.Voidwalker - Reduces physical damage taken by 2%.Succubus - Increases all damage caused by 2%.Felhunter - Increases all resistances by .2 per level.Felguard - Increases all damage caused by 1% and all resistances by .1 per level.",
                Row = 5,
                Column = 2
            },
            new()
            {
                Id = 18767,
                Name = "Master Conjuror",
                WowClass = EnumSim.WowClass.Warlock,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Warlock_Demonology,
                MaxRank = 2,
                TalentRequired = -1,
                Tooltip =
                    "Increases the bonus Fire damage from Firestones and the Firestone effect by 15% and increases the spell critical strike rating bonus of your Spellstone by 15%.",
                Row = 4,
                Column = 3
            },
            new()
            {
                Id = 18769,
                Name = "Unholy Power",
                WowClass = EnumSim.WowClass.Warlock,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Warlock_Demonology,
                MaxRank = 5,
                TalentRequired = -1,
                Tooltip =
                    "Increases the damage done by your Voidwalker, Succubus, Felhunter and Felguard's melee attacks and your Imp's Firebolt by 4%.",
                Row = 3,
                Column = 2
            },
            new()
            {
                Id = 35691,
                Name = "Demonic Knowledge",
                WowClass = EnumSim.WowClass.Warlock,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Warlock_Demonology,
                MaxRank = 3,
                TalentRequired = -1,
                Tooltip =
                    "Increases your spell damage by an amount equal to 4% of the total of your active demon's Stamina plus Intellect.",
                Row = 6,
                Column = 2
            },
            new()
            {
                Id = 18788,
                Name = "Demonic Sacrifice",
                WowClass = EnumSim.WowClass.Warlock,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Warlock_Demonology,
                MaxRank = 1,
                TalentRequired = -1,
                Tooltip =
                    "When activated, sacrifices your summoned demon to grant you an effect that lasts 30 min. &nbsp;The effect is canceled if any Demon is summoned.Imp: Increases your Fire damage by 15%.Voidwalker: Restores 2% of total health every 4 sec.Succubus: Increases your Shadow damage by 15%.Felhunter: Restores 3% of total mana every 4 sec.Felguard: Increases your Shadow damage by 10% and restores 2% of total mana every 4 sec.",
                Row = 4,
                Column = 1
            },
            new()
            {
                Id = 19028,
                Name = "Soul Link",
                WowClass = EnumSim.WowClass.Warlock,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Warlock_Demonology,
                MaxRank = 1,
                TalentRequired = -1,
                Tooltip =
                    "When active, 20% of all damage taken by the caster is taken by your Imp, Voidwalker, Succubus, Felhunter, Felguard, or enslaved demon instead. &nbsp;That damage cannot be prevented. &nbsp;In addition, both the demon and master will inflict 5% more damage. &nbsp;Lasts as long as the demon is active and controlled.",
                Row = 6,
                Column = 1
            },
            new()
            {
                Id = 18821,
                Name = "Improved Enslave Demon",
                WowClass = EnumSim.WowClass.Warlock,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Warlock_Demonology,
                MaxRank = 2,
                TalentRequired = -1,
                Tooltip =
                    "Reduces the Attack Speed and Casting Speed penalty of your Enslave Demon spell by 5% and reduces the resist chance by 5%.",
                Row = 4,
                Column = 0
            },
            new()
            {
                Id = 30143,
                Name = "Demonic Aegis",
                WowClass = EnumSim.WowClass.Warlock,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Warlock_Demonology,
                MaxRank = 3,
                TalentRequired = -1,
                Tooltip = "Increases the effectiveness of your Demon Armor and Fel Armor spells by 10%.",
                Row = 2,
                Column = 3
            },
            new()
            {
                Id = 30146,
                Name = "Summon Felguard",
                WowClass = EnumSim.WowClass.Warlock,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Warlock_Demonology,
                MaxRank = 1,
                TalentRequired = -1,
                Tooltip = "Soul Shard",
                Row = 8,
                Column = 1
            },
            new()
            {
                Id = 30242,
                Name = "Demonic Tactics",
                WowClass = EnumSim.WowClass.Warlock,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Warlock_Demonology,
                MaxRank = 5,
                TalentRequired = -1,
                Tooltip = "Increases melee and spell critical strike chance for you and your summoned demon by 1%.",
                Row = 7,
                Column = 1
            },
            new()
            {
                Id = 30319,
                Name = "Demonic Resilience",
                WowClass = EnumSim.WowClass.Warlock,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Warlock_Demonology,
                MaxRank = 3,
                TalentRequired = -1,
                Tooltip =
                    "Reduces the chance you'll be critically hit by melee and spells by 1% and reduces all damage your summoned demon takes by 5%.",
                Row = 6,
                Column = 0
            },
            new()
            {
                Id = 30326,
                Name = "Mana Feed",
                WowClass = EnumSim.WowClass.Warlock,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Warlock_Demonology,
                MaxRank = 3,
                TalentRequired = -1,
                Tooltip =
                    "When you gain mana from Drain Mana or Life Tap spells, your pet gains 33% of the mana you gain.",
                Row = 5,
                Column = 0
            },
            new()
            {
                Id = 17778,
                Name = "Cataclysm",
                WowClass = EnumSim.WowClass.Warlock,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Warlock_Destruction,
                MaxRank = 5,
                TalentRequired = -1,
                Tooltip = "Reduces the Mana cost of your Destruction spells by 1%.",
                Row = 0,
                Column = 2
            },
            new()
            {
                Id = 17788,
                Name = "Bane",
                WowClass = EnumSim.WowClass.Warlock,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Warlock_Destruction,
                MaxRank = 5,
                TalentRequired = -1,
                Tooltip =
                    "Reduces the casting time of your Shadow Bolt and Immolate spells by 0.1 sec and your Soul Fire spell by 0.4 sec.",
                Row = 1,
                Column = 1
            },
            new()
            {
                Id = 17793,
                Name = "Improved Shadow Bolt",
                WowClass = EnumSim.WowClass.Warlock,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Warlock_Destruction,
                MaxRank = 5,
                TalentRequired = -1,
                Tooltip =
                    "Your Shadow Bolt critical strikes increase Shadow damage dealt to the target by 4% until 4 non-periodic damage sources are applied. &nbsp;Effect lasts a maximum of 12 sec.",
                Row = 0,
                Column = 1
            },
            new()
            {
                Id = 17815,
                Name = "Improved Immolate",
                WowClass = EnumSim.WowClass.Warlock,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Warlock_Destruction,
                MaxRank = 5,
                TalentRequired = -1,
                Tooltip = "Increases the initial damage of your Immolate spell by 5%.",
                Row = 4,
                Column = 1
            },
            new()
            {
                Id = 17877,
                Name = "Shadowburn",
                WowClass = EnumSim.WowClass.Warlock,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Warlock_Destruction,
                MaxRank = 1,
                TalentRequired = -1,
                Tooltip = "Soul Shard",
                Row = 2,
                Column = 3
            },
            new()
            {
                Id = 17917,
                Name = "Destructive Reach",
                WowClass = EnumSim.WowClass.Warlock,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Warlock_Destruction,
                MaxRank = 2,
                TalentRequired = -1,
                Tooltip =
                    "Increases the range of your Destruction spells by 10% and reduces threat caused by Destruction spells by 5%.",
                Row = 3,
                Column = 1
            },
            new()
            {
                Id = 17927,
                Name = "Improved Searing Pain",
                WowClass = EnumSim.WowClass.Warlock,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Warlock_Destruction,
                MaxRank = 3,
                TalentRequired = -1,
                Tooltip = "Increases the critical strike chance of your Searing Pain spell by 4%.",
                Row = 3,
                Column = 3
            },
            new()
            {
                Id = 17954,
                Name = "Emberstorm",
                WowClass = EnumSim.WowClass.Warlock,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Warlock_Destruction,
                MaxRank = 5,
                TalentRequired = -1,
                Tooltip =
                    "Increases the damage done by your Fire spells by 2% and reduces the cast time of your Incinerate spell by 2%.",
                Row = 5,
                Column = 2
            },
            new()
            {
                Id = 17959,
                Name = "Ruin",
                WowClass = EnumSim.WowClass.Warlock,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Warlock_Destruction,
                MaxRank = 1,
                TalentRequired = -1,
                Tooltip = "Increases the critical strike damage bonus of your Destruction spells by 100%.",
                Row = 4,
                Column = 2
            },
            new()
            {
                Id = 17962,
                Name = "Conflagrate",
                WowClass = EnumSim.WowClass.Warlock,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Warlock_Destruction,
                MaxRank = 1,
                TalentRequired = -1,
                Tooltip =
                    "Ignites a target that is already afflicted by your Immolate, dealing 249 to 316 Fire damage and consuming the Immolate spell.",
                Row = 6,
                Column = 1
            },
            new()
            {
                Id = 18130,
                Name = "Devastation",
                WowClass = EnumSim.WowClass.Warlock,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Warlock_Destruction,
                MaxRank = 5,
                TalentRequired = -1,
                Tooltip = "Increases the critical strike chance of your Destruction spells by 1%.",
                Row = 2,
                Column = 2
            },
            new()
            {
                Id = 18119,
                Name = "Aftermath",
                WowClass = EnumSim.WowClass.Warlock,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Warlock_Destruction,
                MaxRank = 5,
                TalentRequired = -1,
                Tooltip = "Gives your Destruction spells a 2% chance to daze the target for 5 sec.",
                Row = 1,
                Column = 2
            },
            new()
            {
                Id = 18126,
                Name = "Improved Firebolt",
                WowClass = EnumSim.WowClass.Warlock,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Warlock_Destruction,
                MaxRank = 2,
                TalentRequired = -1,
                Tooltip = "Reduces the casting time of your Imp's Firebolt spell by 0.25 sec.",
                Row = 2,
                Column = 0
            },
            new()
            {
                Id = 18128,
                Name = "Improved Lash of Pain",
                WowClass = EnumSim.WowClass.Warlock,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Warlock_Destruction,
                MaxRank = 2,
                TalentRequired = -1,
                Tooltip = "Reduces the cooldown of your Succubus' Lash of Pain spell by 3 sec.",
                Row = 2,
                Column = 1
            },
            new()
            {
                Id = 18135,
                Name = "Intensity",
                WowClass = EnumSim.WowClass.Warlock,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Warlock_Destruction,
                MaxRank = 2,
                TalentRequired = -1,
                Tooltip =
                    "Gives you a 35% chance to resist interruption caused by damage while casting or channeling any Destruction spell.",
                Row = 3,
                Column = 0
            },
            new()
            {
                Id = 18096,
                Name = "Pyroclasm",
                WowClass = EnumSim.WowClass.Warlock,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Warlock_Destruction,
                MaxRank = 2,
                TalentRequired = -1,
                Tooltip =
                    "Gives your Rain of Fire, Hellfire, and Soul Fire spells a 13% chance to stun the target for 3 sec.",
                Row = 4,
                Column = 0
            },
            new()
            {
                Id = 30283,
                Name = "Shadowfury",
                WowClass = EnumSim.WowClass.Warlock,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Warlock_Destruction,
                MaxRank = 1,
                TalentRequired = -1,
                Tooltip =
                    "Shadowfury is unleashed, causing 357 to 422 Shadow damage and stunning all enemies within 8 yds for 2 sec.",
                Row = 8,
                Column = 1
            },
            new()
            {
                Id = 30288,
                Name = "Shadow and Flame",
                WowClass = EnumSim.WowClass.Warlock,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Warlock_Destruction,
                MaxRank = 5,
                TalentRequired = -1,
                Tooltip =
                    "Your Shadow Bolt and Incinerate spells gain an additional 4% of your bonus spell damage effects.",
                Row = 7,
                Column = 1
            },
            new()
            {
                Id = 30293,
                Name = "Soul Leech",
                WowClass = EnumSim.WowClass.Warlock,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Warlock_Destruction,
                MaxRank = 3,
                TalentRequired = -1,
                Tooltip =
                    "Gives your Shadow Bolt, Shadowburn, Soul Fire, Incinerate, Searing Pain and Conflagrate spells a 10% chance to return health equal to 20% of the damage caused.",
                Row = 6,
                Column = 2
            },
            new()
            {
                Id = 30299,
                Name = "Nether Protection",
                WowClass = EnumSim.WowClass.Warlock,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Warlock_Destruction,
                MaxRank = 3,
                TalentRequired = -1,
                Tooltip =
                    "After being hit with a Shadow or Fire spell, you have a 10% chance to become immune to Shadow and Fire spells for 4 sec.",
                Row = 5,
                Column = 0
            },
            new()
            {
                Id = 34935,
                Name = "Backlash",
                WowClass = EnumSim.WowClass.Warlock,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Warlock_Destruction,
                MaxRank = 3,
                TalentRequired = -1,
                Tooltip =
                    "Increases your critical strike chance with spells by an additional 1% and gives you a 8% chance when hit by a physical attack to reduce the cast time of your next Shadow Bolt or Incinerate spell by 100%. &nbsp;This effect lasts 8 sec and will not occur more than once every 8 seconds.",
                Row = 6,
                Column = 0
            }
        };
    }
}