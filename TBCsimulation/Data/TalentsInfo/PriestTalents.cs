using System.Collections.Generic;
using TBCsimulation.Data.Utils;

namespace TBCsimulation.Data.TalentsInfo
{
    public static partial class Talents
    {
        public static readonly List<Talent> Priest = new()
        {
            new()
            {
                Id = 14531,
                Name = "Martyrdom",
                WowClass = EnumSim.WowClass.Priest,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Priest_Discipline,
                MaxRank = 2,
                TalentRequired = -1,
                Tooltip =
                    "Gives you a 50% chance to gain the Focused Casting effect that lasts for 6 sec after being the victim of a melee or ranged critical strike. &nbsp;The Focused Casting effect prevents you from losing casting time when taking damage while casting Priest spells and increases resistance to Interrupt effects by 10%.",
                Row = 1,
                Column = 3
            },
            new()
            {
                Id = 10060,
                Name = "Power Infusion",
                WowClass = EnumSim.WowClass.Priest,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Priest_Discipline,
                MaxRank = 1,
                TalentRequired = -1,
                Tooltip =
                    "Infuses the target with power, increasing spell casting speed by 20% and reducing the mana cost of all spells by 20%. &nbsp;Lasts 15 sec.",
                Row = 6,
                Column = 1
            },
            new()
            {
                Id = 14520,
                Name = "Mental Agility",
                WowClass = EnumSim.WowClass.Priest,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Priest_Discipline,
                MaxRank = 5,
                TalentRequired = -1,
                Tooltip = "Reduces the mana cost of your instant cast spells by 2%.",
                Row = 3,
                Column = 1
            },
            new()
            {
                Id = 14522,
                Name = "Unbreakable Will",
                WowClass = EnumSim.WowClass.Priest,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Priest_Discipline,
                MaxRank = 5,
                TalentRequired = -1,
                Tooltip = "Increases your chance to resist Stun, Fear, and Silence effects by an additional 3%.",
                Row = 0,
                Column = 1
            },
            new()
            {
                Id = 14748,
                Name = "Improved Power Word: Shield",
                WowClass = EnumSim.WowClass.Priest,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Priest_Discipline,
                MaxRank = 3,
                TalentRequired = -1,
                Tooltip = "Increases the damage absorbed by your Power Word: Shield by 5%.",
                Row = 1,
                Column = 2
            },
            new()
            {
                Id = 14749,
                Name = "Improved Power Word: Fortitude",
                WowClass = EnumSim.WowClass.Priest,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Priest_Discipline,
                MaxRank = 2,
                TalentRequired = -1,
                Tooltip = "Increases the effect of your Power Word: Fortitude and Prayer of Fortitude spells by 15%.",
                Row = 1,
                Column = 1
            },
            new()
            {
                Id = 14524,
                Name = "Wand Specialization",
                WowClass = EnumSim.WowClass.Priest,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Priest_Discipline,
                MaxRank = 5,
                TalentRequired = -1,
                Tooltip = "Increases your damage with Wands by 5%.",
                Row = 0,
                Column = 2
            },
            new()
            {
                Id = 14747,
                Name = "Improved Inner Fire",
                WowClass = EnumSim.WowClass.Priest,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Priest_Discipline,
                MaxRank = 3,
                TalentRequired = -1,
                Tooltip = "Increases the armor bonus of your Inner Fire spell by 10%.",
                Row = 3,
                Column = 0
            },
            new()
            {
                Id = 14521,
                Name = "Meditation",
                WowClass = EnumSim.WowClass.Priest,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Priest_Discipline,
                MaxRank = 3,
                TalentRequired = -1,
                Tooltip = "Allows 10% of your mana regeneration to continue while casting.",
                Row = 2,
                Column = 2
            },
            new()
            {
                Id = 14751,
                Name = "Inner Focus",
                WowClass = EnumSim.WowClass.Priest,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Priest_Discipline,
                MaxRank = 1,
                TalentRequired = -1,
                Tooltip =
                    "When activated, reduces the mana cost of your next spell by 100% and increases its critical effect chance by 25% if it is capable of a critical effect.",
                Row = 2,
                Column = 1
            },
            new()
            {
                Id = 14750,
                Name = "Improved Mana Burn",
                WowClass = EnumSim.WowClass.Priest,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Priest_Discipline,
                MaxRank = 2,
                TalentRequired = -1,
                Tooltip = "Reduces the casting time of your Mana Burn spell by 0.5 sec.",
                Row = 3,
                Column = 3
            },
            new()
            {
                Id = 14752,
                Name = "Divine Spirit",
                WowClass = EnumSim.WowClass.Priest,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Priest_Discipline,
                MaxRank = 1,
                TalentRequired = -1,
                Tooltip = "Holy power infuses the target, increasing their Spirit by 17 for 30 min.",
                Row = 4,
                Column = 2
            },
            new()
            {
                Id = 14523,
                Name = "Silent Resolve",
                WowClass = EnumSim.WowClass.Priest,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Priest_Discipline,
                MaxRank = 5,
                TalentRequired = -1,
                Tooltip =
                    "Reduces the threat generated by your Holy and Discipline spells by 4% and reduces the chance your spells will be dispelled by 4%.",
                Row = 1,
                Column = 0
            },
            new()
            {
                Id = 18551,
                Name = "Mental Strength",
                WowClass = EnumSim.WowClass.Priest,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Priest_Discipline,
                MaxRank = 5,
                TalentRequired = -1,
                Tooltip = "Increases your maximum mana by 2%.",
                Row = 4,
                Column = 1
            },
            new()
            {
                Id = 18544,
                Name = "Force of Will",
                WowClass = EnumSim.WowClass.Priest,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Priest_Discipline,
                MaxRank = 5,
                TalentRequired = -1,
                Tooltip =
                    "Increases your spell damage by 1% and the critical strike chance of your offensive spells by 1%.",
                Row = 5,
                Column = 2
            },
            new()
            {
                Id = 33167,
                Name = "Absolution",
                WowClass = EnumSim.WowClass.Priest,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Priest_Discipline,
                MaxRank = 3,
                TalentRequired = -1,
                Tooltip =
                    "Reduces the mana cost of your Dispel Magic, Cure Disease, Abolish Disease and Mass Dispel spells by 5%.",
                Row = 2,
                Column = 0
            },
            new()
            {
                Id = 33174,
                Name = "Improved Divine Spirit",
                WowClass = EnumSim.WowClass.Priest,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Priest_Discipline,
                MaxRank = 2,
                TalentRequired = -1,
                Tooltip =
                    "Your Divine Spirit and Prayer of Spirit spells also increase the target's spell damage and healing by an amount equal to 5% of their total Spirit.",
                Row = 4,
                Column = 3
            },
            new()
            {
                Id = 33186,
                Name = "Focused Power",
                WowClass = EnumSim.WowClass.Priest,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Priest_Discipline,
                MaxRank = 2,
                TalentRequired = -1,
                Tooltip =
                    "Your Smite, Mind Blast and Mass Dispel spells have an additional 2% chance to hit. &nbsp;In addition, your Mass Dispel cast time is reduced by 0.5 sec.",
                Row = 5,
                Column = 0
            },
            new()
            {
                Id = 34908,
                Name = "Enlightenment",
                WowClass = EnumSim.WowClass.Priest,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Priest_Discipline,
                MaxRank = 5,
                TalentRequired = -1,
                Tooltip = "Increases your total Stamina, Intellect and Spirit by 1%.",
                Row = 7,
                Column = 1
            },
            new()
            {
                Id = 33201,
                Name = "Reflective Shield",
                WowClass = EnumSim.WowClass.Priest,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Priest_Discipline,
                MaxRank = 5,
                TalentRequired = -1,
                Tooltip =
                    "Causes 10% of the damage absorbed by your Power Word: Shield to reflect back at the attacker. &nbsp;This damage causes no threat.",
                Row = 6,
                Column = 2
            },
            new()
            {
                Id = 33206,
                Name = "Pain Suppression",
                WowClass = EnumSim.WowClass.Priest,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Priest_Discipline,
                MaxRank = 1,
                TalentRequired = -1,
                Tooltip =
                    "Instantly reduces a friendly target's threat by 5%, reduces all damage taken by 40% and increases resistance to Dispel mechanics by 65% for 8 sec.",
                Row = 8,
                Column = 1
            },
            new()
            {
                Id = 45234,
                Name = "Focused Will",
                WowClass = EnumSim.WowClass.Priest,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Priest_Discipline,
                MaxRank = 3,
                TalentRequired = -1,
                Tooltip =
                    "After taking a critical hit you gain the Focused Will effect, reducing all damage taken by 2% and increasing healing effects on you by 4%. &nbsp;Stacks up to 3 times. &nbsp;Lasts 8 sec.",
                Row = 6,
                Column = 0
            },
            new()
            {
                Id = 14892,
                Name = "Inspiration",
                WowClass = EnumSim.WowClass.Priest,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Priest_Holy,
                MaxRank = 3,
                TalentRequired = -1,
                Tooltip =
                    "Increases your target's armor by 8% for 15 sec after getting a critical effect from your Flash Heal, Heal, Greater Heal, Binding Heal, Prayer of Healing, or Circle of Healing spell.",
                Row = 2,
                Column = 3
            },
            new()
            {
                Id = 14889,
                Name = "Holy Specialization",
                WowClass = EnumSim.WowClass.Priest,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Priest_Holy,
                MaxRank = 5,
                TalentRequired = -1,
                Tooltip = "Increases the critical effect chance of your Holy spells by 1%.",
                Row = 0,
                Column = 2
            },
            new()
            {
                Id = 14901,
                Name = "Spiritual Guidance",
                WowClass = EnumSim.WowClass.Priest,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Priest_Holy,
                MaxRank = 5,
                TalentRequired = -1,
                Tooltip = "Increases spell damage and healing by up to 5% of your total Spirit.",
                Row = 4,
                Column = 2
            },
            new()
            {
                Id = 14909,
                Name = "Searing Light",
                WowClass = EnumSim.WowClass.Priest,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Priest_Holy,
                MaxRank = 2,
                TalentRequired = -1,
                Tooltip = "Increases the damage of your Smite and Holy Fire spells by 5%.",
                Row = 3,
                Column = 2
            },
            new()
            {
                Id = 14898,
                Name = "Spiritual Healing",
                WowClass = EnumSim.WowClass.Priest,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Priest_Holy,
                MaxRank = 5,
                TalentRequired = -1,
                Tooltip = "Increases the amount healed by your healing spells by 2%.",
                Row = 5,
                Column = 2
            },
            new()
            {
                Id = 14908,
                Name = "Improved Renew",
                WowClass = EnumSim.WowClass.Priest,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Priest_Holy,
                MaxRank = 3,
                TalentRequired = -1,
                Tooltip = "Increases the amount healed by your Renew spell by 5%.",
                Row = 0,
                Column = 1
            },
            new()
            {
                Id = 14912,
                Name = "Improved Healing",
                WowClass = EnumSim.WowClass.Priest,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Priest_Holy,
                MaxRank = 3,
                TalentRequired = -1,
                Tooltip = "Reduces the mana cost of your Lesser Heal, Heal, and Greater Heal spells by 5%.",
                Row = 3,
                Column = 1
            },
            new()
            {
                Id = 14913,
                Name = "Healing Focus",
                WowClass = EnumSim.WowClass.Priest,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Priest_Holy,
                MaxRank = 2,
                TalentRequired = -1,
                Tooltip =
                    "Gives you a 35% chance to avoid interruption caused by damage while casting any healing spell.",
                Row = 0,
                Column = 0
            },
            new()
            {
                Id = 27900,
                Name = "Spell Warding",
                WowClass = EnumSim.WowClass.Priest,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Priest_Holy,
                MaxRank = 5,
                TalentRequired = -1,
                Tooltip = "Reduces all spell damage taken by 2%.",
                Row = 1,
                Column = 1
            },
            new()
            {
                Id = 14911,
                Name = "Healing Prayers",
                WowClass = EnumSim.WowClass.Priest,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Priest_Holy,
                MaxRank = 2,
                TalentRequired = -1,
                Tooltip = "Reduces the mana cost of your Prayer of Healing and Prayer of Mending spell by 10%.",
                Row = 4,
                Column = 0
            },
            new()
            {
                Id = 15237,
                Name = "Holy Nova",
                WowClass = EnumSim.WowClass.Priest,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Priest_Holy,
                MaxRank = 1,
                TalentRequired = -1,
                Tooltip =
                    "Causes an explosion of holy light around the caster, causing 29 to 34 Holy damage to all enemy targets within 10 yards and healing all party members within 10 yards for 54 to 63. &nbsp;These effects cause no threat.",
                Row = 2,
                Column = 0
            },
            new()
            {
                Id = 18530,
                Name = "Divine Fury",
                WowClass = EnumSim.WowClass.Priest,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Priest_Holy,
                MaxRank = 5,
                TalentRequired = -1,
                Tooltip = "Reduces the casting time of your Smite, Holy Fire, Heal and Greater Heal spells by 0.1 sec.",
                Row = 1,
                Column = 2
            },
            new()
            {
                Id = 20711,
                Name = "Spirit of Redemption",
                WowClass = EnumSim.WowClass.Priest,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Priest_Holy,
                MaxRank = 1,
                TalentRequired = -1,
                Tooltip =
                    "Increases total Spirit by 5% and upon death, the priest becomes the Spirit of Redemption for 15 sec. &nbsp;The Spirit of Redemption cannot move, attack, be attacked or targeted by any spells or effects. &nbsp;While in this form the priest can cast any healing spell free of cost. &nbsp;When the effect ends, the priest dies.",
                Row = 4,
                Column = 1
            },
            new()
            {
                Id = 27789,
                Name = "Holy Reach",
                WowClass = EnumSim.WowClass.Priest,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Priest_Holy,
                MaxRank = 2,
                TalentRequired = -1,
                Tooltip =
                    "Increases the range of your Smite and Holy Fire spells and the radius of your Prayer of Healing, Holy Nova and Circle of Healing spells by 10%.",
                Row = 3,
                Column = 0
            },
            new()
            {
                Id = 27811,
                Name = "Blessed Recovery",
                WowClass = EnumSim.WowClass.Priest,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Priest_Holy,
                MaxRank = 3,
                TalentRequired = -1,
                Tooltip =
                    "After being struck by a melee or ranged critical hit, heal 8% of the damage taken over 6 sec.",
                Row = 2,
                Column = 1
            },
            new()
            {
                Id = 724,
                Name = "Lightwell",
                WowClass = EnumSim.WowClass.Priest,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Priest_Holy,
                MaxRank = 1,
                TalentRequired = -1,
                Tooltip =
                    "Creates a Holy Lightwell. &nbsp;Members of your raid or party can click the Lightwell to restore 801 health over 6 sec. &nbsp;Any damage taken will cancel the effect. &nbsp;Lightwell lasts for 3 min or 5 charges.",
                Row = 6,
                Column = 1
            },
            new()
            {
                Id = 33142,
                Name = "Blessed Resilience",
                WowClass = EnumSim.WowClass.Priest,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Priest_Holy,
                MaxRank = 3,
                TalentRequired = -1,
                Tooltip =
                    "Critical hits made against you have a 20% chance to prevent you from being critically hit again for 6 sec.",
                Row = 6,
                Column = 2
            },
            new()
            {
                Id = 33150,
                Name = "Surge of Light",
                WowClass = EnumSim.WowClass.Priest,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Priest_Holy,
                MaxRank = 2,
                TalentRequired = -1,
                Tooltip =
                    "Your spell criticals have a 25% chance to cause your next Smite spell to be instant cast, cost no mana but be incapable of a critical hit. &nbsp;This effect lasts 10 sec.",
                Row = 5,
                Column = 0
            },
            new()
            {
                Id = 33158,
                Name = "Empowered Healing",
                WowClass = EnumSim.WowClass.Priest,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Priest_Holy,
                MaxRank = 5,
                TalentRequired = -1,
                Tooltip =
                    "Your Greater Heal spell gains an additional 4% and your Flash Heal and Binding Heal gain an additional 2% of your bonus healing effects.",
                Row = 7,
                Column = 1
            },
            new()
            {
                Id = 34753,
                Name = "Holy Concentration",
                WowClass = EnumSim.WowClass.Priest,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Priest_Holy,
                MaxRank = 3,
                TalentRequired = -1,
                Tooltip =
                    "Gives you a 2% chance to enter a Clearcasting state after casting any Flash Heal, Binding Heal, or Greater Heal spell. &nbsp;The Clearcasting state reduces the mana cost of your next Flash Heal, Binding Heal, or Greater Heal spell by 100%.",
                Row = 6,
                Column = 0
            },
            new()
            {
                Id = 34861,
                Name = "Circle of Healing",
                WowClass = EnumSim.WowClass.Priest,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Priest_Holy,
                MaxRank = 1,
                TalentRequired = -1,
                Tooltip =
                    "Heals friendly target and that target's party members within 15 yards of the target for 250 to 274.",
                Row = 8,
                Column = 1
            },
            new()
            {
                Id = 15257,
                Name = "Shadow Weaving",
                WowClass = EnumSim.WowClass.Priest,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Priest_ShadowMagic,
                MaxRank = 5,
                TalentRequired = -1,
                Tooltip =
                    "Your Shadow damage spells have a 20% chance to cause your target to be vulnerable to Shadow damage. &nbsp;This vulnerability increases the Shadow damage dealt to your target by 2% and lasts 15 sec. &nbsp;Stacks up to 5 times.",
                Row = 3,
                Column = 3
            },
            new()
            {
                Id = 15259,
                Name = "Darkness",
                WowClass = EnumSim.WowClass.Priest,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Priest_ShadowMagic,
                MaxRank = 5,
                TalentRequired = -1,
                Tooltip = "Increases your Shadow spell damage by 2%.",
                Row = 5,
                Column = 2
            },
            new()
            {
                Id = 15260,
                Name = "Shadow Focus",
                WowClass = EnumSim.WowClass.Priest,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Priest_ShadowMagic,
                MaxRank = 5,
                TalentRequired = -1,
                Tooltip = "Reduces your target's chance to resist your Shadow spells by 2%.",
                Row = 1,
                Column = 2
            },
            new()
            {
                Id = 15268,
                Name = "Blackout",
                WowClass = EnumSim.WowClass.Priest,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Priest_ShadowMagic,
                MaxRank = 5,
                TalentRequired = -1,
                Tooltip = "Gives your Shadow damage spells a 2% chance to stun the target for 3 sec.",
                Row = 0,
                Column = 2
            },
            new()
            {
                Id = 15270,
                Name = "Spirit Tap",
                WowClass = EnumSim.WowClass.Priest,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Priest_ShadowMagic,
                MaxRank = 5,
                TalentRequired = -1,
                Tooltip =
                    "Gives you a 20% chance to gain a 100% bonus to your Spirit after killing a target that yields experience or honor. &nbsp;For the duration, your mana will regenerate at a 50% rate while casting. &nbsp;Lasts 15 sec.",
                Row = 0,
                Column = 1
            },
            new()
            {
                Id = 15318,
                Name = "Shadow Affinity",
                WowClass = EnumSim.WowClass.Priest,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Priest_ShadowMagic,
                MaxRank = 3,
                TalentRequired = -1,
                Tooltip = "Reduces the threat generated by your Shadow spells by 8%.",
                Row = 1,
                Column = 0
            },
            new()
            {
                Id = 15273,
                Name = "Improved Mind Blast",
                WowClass = EnumSim.WowClass.Priest,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Priest_ShadowMagic,
                MaxRank = 5,
                TalentRequired = -1,
                Tooltip = "Reduces the cooldown of your Mind Blast spell by 0.5 sec.",
                Row = 2,
                Column = 1
            },
            new()
            {
                Id = 15275,
                Name = "Improved Shadow Word: Pain",
                WowClass = EnumSim.WowClass.Priest,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Priest_ShadowMagic,
                MaxRank = 2,
                TalentRequired = -1,
                Tooltip = "Increases the duration of your Shadow Word: Pain spell by 3 sec.",
                Row = 1,
                Column = 1
            },
            new()
            {
                Id = 15274,
                Name = "Improved Fade",
                WowClass = EnumSim.WowClass.Priest,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Priest_ShadowMagic,
                MaxRank = 2,
                TalentRequired = -1,
                Tooltip = "Decreases the cooldown of your Fade ability by 3 sec.",
                Row = 3,
                Column = 1
            },
            new()
            {
                Id = 15286,
                Name = "Vampiric Embrace",
                WowClass = EnumSim.WowClass.Priest,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Priest_ShadowMagic,
                MaxRank = 1,
                TalentRequired = -1,
                Tooltip =
                    "Afflicts your target with Shadow energy that causes all party members to be healed for 15% of any Shadow spell damage you deal for 1 min.",
                Row = 4,
                Column = 1
            },
            new()
            {
                Id = 15407,
                Name = "Mind Flay",
                WowClass = EnumSim.WowClass.Priest,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Priest_ShadowMagic,
                MaxRank = 1,
                TalentRequired = -1,
                Tooltip =
                    "Assault the target's mind with Shadow energy, causing 75 Shadow damage over 3 sec and slowing their movement speed by 50%.",
                Row = 2,
                Column = 2
            },
            new()
            {
                Id = 15473,
                Name = "Shadowform",
                WowClass = EnumSim.WowClass.Priest,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Priest_ShadowMagic,
                MaxRank = 1,
                TalentRequired = -1,
                Tooltip =
                    "Assume a Shadowform, increasing your Shadow damage by 15% and reducing Physical damage done to you by 15%. &nbsp;However, you may not cast Holy spells while in this form.",
                Row = 6,
                Column = 1
            },
            new()
            {
                Id = 15487,
                Name = "Silence",
                WowClass = EnumSim.WowClass.Priest,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Priest_ShadowMagic,
                MaxRank = 1,
                TalentRequired = -1,
                Tooltip = "Silences the target, preventing them from casting spells for 5 sec.",
                Row = 4,
                Column = 0
            },
            new()
            {
                Id = 15392,
                Name = "Improved Psychic Scream",
                WowClass = EnumSim.WowClass.Priest,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Priest_ShadowMagic,
                MaxRank = 2,
                TalentRequired = -1,
                Tooltip = "Reduces the cooldown of your Psychic Scream spell by 2 sec.",
                Row = 2,
                Column = 0
            },
            new()
            {
                Id = 17322,
                Name = "Shadow Reach",
                WowClass = EnumSim.WowClass.Priest,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Priest_ShadowMagic,
                MaxRank = 2,
                TalentRequired = -1,
                Tooltip = "Increases the range of your offensive Shadow spells by 10%.",
                Row = 3,
                Column = 2
            },
            new()
            {
                Id = 27839,
                Name = "Improved Vampiric Embrace",
                WowClass = EnumSim.WowClass.Priest,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Priest_ShadowMagic,
                MaxRank = 2,
                TalentRequired = -1,
                Tooltip = "Increases the percentage healed by Vampiric Embrace by an additional 5%.",
                Row = 4,
                Column = 2
            },
            new()
            {
                Id = 33213,
                Name = "Focused Mind",
                WowClass = EnumSim.WowClass.Priest,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Priest_ShadowMagic,
                MaxRank = 3,
                TalentRequired = -1,
                Tooltip = "Reduces the mana cost of your Mind Blast, Mind Control and Mind Flay spells by 5%.",
                Row = 4,
                Column = 3
            },
            new()
            {
                Id = 33221,
                Name = "Shadow Power",
                WowClass = EnumSim.WowClass.Priest,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Priest_ShadowMagic,
                MaxRank = 5,
                TalentRequired = -1,
                Tooltip =
                    "Increases the critical strike chance of your Mind Blast and Shadow Word: Death spells by 3%.",
                Row = 6,
                Column = 2
            },
            new()
            {
                Id = 34914,
                Name = "Vampiric Touch",
                WowClass = EnumSim.WowClass.Priest,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Priest_ShadowMagic,
                MaxRank = 1,
                TalentRequired = -1,
                Tooltip =
                    "Causes 450 Shadow damage over 15 sec to your target and causes all party members to gain mana equal to 5% of any Shadow spell damage you deal.",
                Row = 8,
                Column = 1
            },
            new()
            {
                Id = 14910,
                Name = "Shadow Resilience",
                WowClass = EnumSim.WowClass.Priest,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Priest_ShadowMagic,
                MaxRank = 2,
                TalentRequired = -1,
                Tooltip = "Reduces the chance you'll be critically hit by all spells by 2%.",
                Row = 5,
                Column = 0
            },
            new()
            {
                Id = 33191,
                Name = "Misery",
                WowClass = EnumSim.WowClass.Priest,
                SecondarySchoolType = EnumSim.SecondarySchoolType.Priest_ShadowMagic,
                MaxRank = 5,
                TalentRequired = -1,
                Tooltip =
                    "Your Shadow Word: Pain, Mind Flay and Vampiric Touch spells also cause the target to take an additional 1% spell damage.",
                Row = 7,
                Column = 2
            }
        };
    }
}