//####### AUTOGENERATED #######

using System.Collections.Generic;

namespace TBCsimulation.Data.TalentsInfo
{
    public static partial class Talents
    {
//####### AUTOGENERATED #######
        public static readonly Dictionary<int, string> WarriorDictionaryIdToName = new()
        {
            {12834, "Deep Wounds"},

            {12281, "Sword Specialization"},

            {12282, "Improved Heroic Strike"},

            {12284, "Mace Specialization"},

            {12285, "Improved Charge"},

            {12286, "Improved Rend"},

            {12287, "Improved Thunder Clap"},

            {12289, "Improved Hamstring"},

            {16462, "Deflection"},

            {12290, "Improved Overpower"},

            {12700, "Poleaxe Specialization"},

            {12292, "Death Wish"},

            {29888, "Improved Intercept"},

            {12294, "Mortal Strike"},

            {12163, "Two-Handed Weapon Specialization"},

            {12296, "Anger Management"},

            {12300, "Iron Will"},

            {16493, "Impale"},

            {29623, "Endless Rage"},

            {29723, "Improved Disciplines"},

            {29834, "Second Wind"},

            {29836, "Blood Frenzy"},

            {35446, "Improved Mortal Strike"},

            {12318, "Commanding Presence"},

            {12317, "Enrage"},

            {12319, "Flurry"},

            {12320, "Cruelty"},

            {12321, "Booming Voice"},

            {12322, "Unbridled Wrath"},

            {12323, "Piercing Howl"},

            {12324, "Improved Demoralizing Shout"},

            {12328, "Sweeping Strikes"},

            {12329, "Improved Cleave"},

            {23881, "Bloodthirst"},

            {12862, "Improved Slam"},

            {16487, "Blood Craze"},

            {20500, "Improved Berserker Rage"},

            {20502, "Improved Execute"},

            {20504, "Weapon Mastery"},

            {23584, "Dual Wield Specialization"},

            {29721, "Improved Whirlwind"},

            {29590, "Precision"},

            {29759, "Improved Berserker Stance"},

            {29801, "Rampage"},

            {12297, "Anticipation"},

            {12299, "Toughness"},

            {12295, "Tactical Mastery"},

            {12301, "Improved Bloodrage"},

            {12302, "Improved Taunt"},

            {12303, "Defiance"},

            {12945, "Improved Shield Block"},

            {12308, "Improved Sunder Armor"},

            {12797, "Improved Revenge"},

            {23922, "Shield Slam"},

            {12311, "Improved Shield Bash"},

            {12312, "Improved Shield Wall"},

            {12313, "Improved Disarm"},

            {12809, "Concussion Blow"},

            {12975, "Last Stand"},

            {16538, "One-Handed Weapon Specialization"},

            {12298, "Shield Specialization"},

            {29593, "Improved Defensive Stance"},

            {29140, "Vitality"},

            {29598, "Shield Mastery"},

            {29787, "Focused Rage"},

            {20243, "Devastate"}
        };


//####### AUTOGENERATED #######
        public static readonly Dictionary<string, int> WarriorDictionaryNameToId = new()
        {
            {"Deep Wounds", 12834},

            {"Sword Specialization", 12281},

            {"Improved Heroic Strike", 12282},

            {"Mace Specialization", 12284},

            {"Improved Charge", 12285},

            {"Improved Rend", 12286},

            {"Improved Thunder Clap", 12287},

            {"Improved Hamstring", 12289},

            {"Deflection", 16462},

            {"Improved Overpower", 12290},

            {"Poleaxe Specialization", 12700},

            {"Death Wish", 12292},

            {"Improved Intercept", 29888},

            {"Mortal Strike", 12294},

            {"Two-Handed Weapon Specialization", 12163},

            {"Anger Management", 12296},

            {"Iron Will", 12300},

            {"Impale", 16493},

            {"Endless Rage", 29623},

            {"Improved Disciplines", 29723},

            {"Second Wind", 29834},

            {"Blood Frenzy", 29836},

            {"Improved Mortal Strike", 35446},

            {"Commanding Presence", 12318},

            {"Enrage", 12317},

            {"Flurry", 12319},

            {"Cruelty", 12320},

            {"Booming Voice", 12321},

            {"Unbridled Wrath", 12322},

            {"Piercing Howl", 12323},

            {"Improved Demoralizing Shout", 12324},

            {"Sweeping Strikes", 12328},

            {"Improved Cleave", 12329},

            {"Bloodthirst", 23881},

            {"Improved Slam", 12862},

            {"Blood Craze", 16487},

            {"Improved Berserker Rage", 20500},

            {"Improved Execute", 20502},

            {"Weapon Mastery", 20504},

            {"Dual Wield Specialization", 23584},

            {"Improved Whirlwind", 29721},

            {"Precision", 29590},

            {"Improved Berserker Stance", 29759},

            {"Rampage", 29801},

            {"Anticipation", 12297},

            {"Toughness", 12299},

            {"Tactical Mastery", 12295},

            {"Improved Bloodrage", 12301},

            {"Improved Taunt", 12302},

            {"Defiance", 12303},

            {"Improved Shield Block", 12945},

            {"Improved Sunder Armor", 12308},

            {"Improved Revenge", 12797},

            {"Shield Slam", 23922},

            {"Improved Shield Bash", 12311},

            {"Improved Shield Wall", 12312},

            {"Improved Disarm", 12313},

            {"Concussion Blow", 12809},

            {"Last Stand", 12975},

            {"One-Handed Weapon Specialization", 16538},

            {"Shield Specialization", 12298},

            {"Improved Defensive Stance", 29593},

            {"Vitality", 29140},

            {"Shield Mastery", 29598},

            {"Focused Rage", 29787},

            {"Devastate", 20243}
        };
    }
}