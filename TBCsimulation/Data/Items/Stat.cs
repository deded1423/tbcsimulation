﻿using System;
using TBCsimulation.Data.Utils;

namespace TBCsimulation.Data.Items
{
    [Serializable]
    public class Stat
    {
        public EnumSim.StatEnum Key = EnumSim.StatEnum.None;
        public int Value = 0;

        public override string ToString()
        {
            return Enum.GetName(typeof(EnumSim.StatEnum), Key) + " - " + Value;
        }
    }
}