﻿using System;

namespace TBCsimulation.Data.Items
{
    [Serializable]
    public struct ItemKey
    {
        public readonly int Id;
        public readonly string Name;

        public ItemKey(int Id, string Name)
        {
            this.Id = Id;
            this.Name = Name;
        }

        public override string ToString()
        {
            return Id + " - " + Name;
        }
    }
}