﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Newtonsoft.Json;
using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;

namespace TBCsimulation.Data.Items
{
    [Serializable]
    public partial class Item
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);


        public EnumSim.Binds Binds;
        public List<EnumSim.WowClass> Classes = new();
        public int[] Damage = new int[2];

        public float DamageperSecond = -1f;
        public EnumSim.SchoolType DamageSchool;
        public float DropChance = -1;

        public string DroppedBy = "";
        public int Durability = -1;

        // public string File = "";

        public string Icon = "";
        public int Id = -1;
        public int ItemLevel = -1;
        public EnumSim.ItemType ItemType;
        public int LevelRequired = -1;
        public string Name = "";
        public int Quality = -1;
        public List<EnumSim.RaceType> Races = new();

        public string Requires = "";
        public string RequiresProfession = "";
        public string RequiresProfessionSpecialization = "";
        public int RequiresProfessionValue = -1;
        public string RequiresReputation = "";
        public string RequiresReputationValue = "";

        public string Set = "";
        public List<string> SetPieces = new();
        public int SetPiecesCount = -1;
        public string SetRequires = "";
        public EnumSim.Slot Slot;

        public Stat SocketBonus = null;
        public Dictionary<EnumSim.Socket, int> Sockets = new();

        public float Speed = -1f;
        public List<Stat> Stats = new();

        public bool Unique = false;

        public int VendorPrice = -1;

        public List<string> Use = new();
        public Action<FightSim> OnUse = null;
        public List<string> Equip = new();
        public Action<FightSim> OnEquip = null;
        public List<string> ExtraLower = new();
        public List<string> ExtraUpper = new();
        public List<string> ChanceOnHit = new();

        public Action<FightSim> OnChanceOnHit = null;
        // public List<KeyValuePair<int, string>> SetBonuses = new();

        public static Dictionary<ItemKey, Item> Items = null;

        public int GetSocketCount()
        {
            int count = 0;
            foreach (var i in Sockets)
            {
                count += i.Value;
            }

            return count;
        }

        public bool HasMetaSocket()
        {
            return Sockets.ContainsKey(EnumSim.Socket.Meta);
        }

        public bool HasSocketBonus(List<Gem> gems)
        {
            if (Sockets.Count == 0)
            {
                return false;
            }

            int blue = gems.Count((gem => gem.Socket == EnumSim.Socket.Blue));
            int red = gems.Count((gem => gem.Socket == EnumSim.Socket.Red));
            int green = gems.Count((gem => gem.Socket == EnumSim.Socket.Green));
            int purple = gems.Count((gem => gem.Socket == EnumSim.Socket.Purple));
            int orange = gems.Count((gem => gem.Socket == EnumSim.Socket.Orange));
            int yellow = gems.Count((gem => gem.Socket == EnumSim.Socket.Yellow));
            int meta = gems.Count((gem => gem.Socket == EnumSim.Socket.Meta));
            int prismatic = gems.Count((gem => gem.Socket == EnumSim.Socket.Prismatic));

            if (blue > GetNumberofSockets(EnumSim.Socket.Blue) ||
                red > GetNumberofSockets(EnumSim.Socket.Red) ||
                yellow > GetNumberofSockets(EnumSim.Socket.Yellow) ||
                meta != GetNumberofSockets(EnumSim.Socket.Meta))
            {
                return false;
            }

            //TODO Comprobar que si son multicolor este correctamente engarzadas

            return true;

            int GetNumberofSockets(EnumSim.Socket color)
            {
                if (!Sockets.ContainsKey(color))
                {
                    return 0;
                }

                return Sockets[color];
            }
        }

        static Item()
        {
            try
            {
                var reader =
                    new StreamReader(@"Items.json");
                var items = JsonConvert.DeserializeObject<List<Item>>(reader.ReadToEnd());

                Items = new();
                foreach (var item in items)
                {
                    Items.Add(new ItemKey(item.Id, item.Name), item);
                }

                reader.Close();
                SetUpItems();
            }
            catch (Exception e)
            {
                log.Fatal(e);
            }
        }

        public static Item GetItem(int Id, string Name)
        {
            return Items[new ItemKey(Id, Name)];
        }
    }
}