﻿using System;
using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json;
using TBCsimulation.Data.Utils;

namespace TBCsimulation.Data.Items
{
    public class Gem
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public EnumSim.Binds Binds;
        public EnumSim.Socket Socket;
        public int ILevel;
        public int Id;
        public List<Stat> Stats = new();
        public string ExtraStats = null;
        public string Name;
        public int VendorPrice = -1;
        public bool Unique;

        public static Dictionary<int, Gem> Gems = null;

        static Gem()
        {
            try
            {
                var reader =
                    new StreamReader(@"Gem.json");
                var gems = JsonConvert.DeserializeObject<List<Gem>>(reader.ReadToEnd());

                Gems = new();
                foreach (var item in gems)
                {
                    Gems.Add(item.Id, item);
                }

                reader.Close();
            }
            catch (Exception e)
            {
                log.Fatal(e);
            }
        }

        public override string ToString()
        {
            return Id + " - " + Name;
        }
    }
}