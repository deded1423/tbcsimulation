using System;

namespace TBCsimulation.Data.Items
{
    public partial class Item
    {
        private static void SetUpItems()
        {
            Item item;
            // 35019 - Brutal Gladiator's Idol of Resolve
            item = GetItem(35019, "Brutal Gladiator's Idol of Resolve");
            item.OnEquip = (sim) =>
            {
                //Your Mangle ability also grants you 39 resilience rating for 6 sec.
                throw new NotImplementedException();
            };
            // 35020 - Brutal Gladiator's Idol of Steadfastness
            item = GetItem(35020, "Brutal Gladiator's Idol of Steadfastness");
            item.OnEquip = (sim) =>
            {
                //Your Moonfire ability also grants you 39 resilience rating for 6 sec.
                throw new NotImplementedException();
            };
            // 35021 - Brutal Gladiator's Idol of Tenacity
            item = GetItem(35021, "Brutal Gladiator's Idol of Tenacity");
            item.OnEquip = (sim) =>
            {
                //Increases the final healing value of your Lifebloom by 131.
                throw new NotImplementedException();
            };
            // 186054 - Communal Idol of Life
            item = GetItem(186054, "Communal Idol of Life");
            item.OnEquip = (sim) =>
            {
                //Increases the periodic healing of your Rejuvenation by up to 15.
                throw new NotImplementedException();
            };
            // 186053 - Communal Idol of the Wild
            item = GetItem(186053, "Communal Idol of the Wild");
            item.OnEquip = (sim) =>
            {
                //Increases the damage of your Claw and Rake abilites by 5.
                throw new NotImplementedException();
            };
            // 186052 - Communal Idol of Wrath
            item = GetItem(186052, "Communal Idol of Wrath");
            item.OnEquip = (sim) =>
            {
                //Increases the damage of your Moonfire spell by up to 10.
                throw new NotImplementedException();
            };
            // 29390 - Everbloom Idol
            item = GetItem(29390, "Everbloom Idol");
            item.OnEquip = (sim) =>
            {
                //Increases the damage dealt by Shred by 88.
                throw new NotImplementedException();
            };
            // 33945 - Gladiator's Idol of Resolve
            item = GetItem(33945, "Gladiator's Idol of Resolve");
            item.OnEquip = (sim) =>
            {
                //Your Mangle ability also grants you 26 resilience rating for 6 sec.
                throw new NotImplementedException();
            };
            // 33942 - Gladiator's Idol of Steadfastness
            item = GetItem(33942, "Gladiator's Idol of Steadfastness");
            item.OnEquip = (sim) =>
            {
                //Your Moonfire ability also grants you 26 resilience rating for 6 sec.
                throw new NotImplementedException();
            };
            // 28355 - Gladiator's Idol of Tenacity
            item = GetItem(28355, "Gladiator's Idol of Tenacity");
            item.OnEquip = (sim) =>
            {
                //Increases the final healing value of your Lifebloom by 87.
                throw new NotImplementedException();
            };
            // 25643 - Harold's Rejuvenating Broach
            item = GetItem(25643, "Harold's Rejuvenating Broach");
            item.OnEquip = (sim) =>
            {
                //Increases healing done by Rejuvenation by up to 86.
                throw new NotImplementedException();
            };
            // 23198 - Idol of Brutality
            item = GetItem(23198, "Idol of Brutality");
            item.OnEquip = (sim) =>
            {
                //Increases the damage dealt by your Maul ability by 50 and Swipe ability by 10.
                throw new NotImplementedException();
            };
            // 33508 - Idol of Budding Life
            item = GetItem(33508, "Idol of Budding Life");
            item.OnEquip = (sim) =>
            {
                //Reduces the mana cost of Rejuvenation by 36.
                throw new NotImplementedException();
            };
            // 28372 - Idol of Feral Shadows
            item = GetItem(28372, "Idol of Feral Shadows");
            item.OnEquip = (sim) =>
            {
                //Increases periodic damage done by Rip by 7 per combo point.
                throw new NotImplementedException();
            };
            // 22397 - Idol of Ferocity
            item = GetItem(22397, "Idol of Ferocity");
            item.OnEquip = (sim) =>
            {
                //Increases the damage of your Claw and Rake abilites by 20.
                throw new NotImplementedException();
            };
            // 22399 - Idol of Health
            item = GetItem(22399, "Idol of Health");
            item.OnEquip = (sim) =>
            {
                //Increases the amount healed by Healing Touch by 100.
                throw new NotImplementedException();
            };
            // 23004 - Idol of Longevity
            item = GetItem(23004, "Idol of Longevity");
            item.OnEquip = (sim) =>
            {
                //Gain up to 25 mana each time you cast Healing Touch.
                throw new NotImplementedException();
            };
            // 22398 - Idol of Rejuvenation
            item = GetItem(22398, "Idol of Rejuvenation");
            item.OnEquip = (sim) =>
            {
                //Increases healing done by Rejuvenation by up to 50.
                throw new NotImplementedException();
            };
            // 27990 - Idol of Savagery
            item = GetItem(27990, "Idol of Savagery");
            item.OnEquip = (sim) =>
            {
                //Increases the damage of your Claw and Rake abilites by 30.
                throw new NotImplementedException();
            };
            // 33509 - Idol of Terror
            item = GetItem(33509, "Idol of Terror");
            item.OnEquip = (sim) =>
            {
                //Your Mangle ability has a chance to grant 65 agility for 10 sec.
                throw new NotImplementedException();
            };
            // 31025 - Idol of the Avenger
            item = GetItem(31025, "Idol of the Avenger");
            item.OnEquip = (sim) =>
            {
                //Increases the damage dealt by Wrath by 25.
                throw new NotImplementedException();
            };
            // 28568 - Idol of the Avian Heart
            item = GetItem(28568, "Idol of the Avian Heart");
            item.OnEquip = (sim) =>
            {
                //Increases the amount healed by Healing Touch by 136.
                throw new NotImplementedException();
            };
            // 25667 - Idol of the Beast
            item = GetItem(25667, "Idol of the Beast");
            item.OnEquip = (sim) =>
            {
                //Increases damage done by Ferocious Bite by 14 per combo point.
                throw new NotImplementedException();
            };
            // 25940 - Idol of the Claw
            item = GetItem(25940, "Idol of the Claw");
            item.OnEquip = (sim) =>
            {
                //20% chance per combo point to heal yourself for 90 to 110 each time you land a finishing move.
                throw new NotImplementedException();
            };
            // 30051 - Idol of the Crescent Goddess
            item = GetItem(30051, "Idol of the Crescent Goddess");
            item.OnEquip = (sim) =>
            {
                //Reduces the mana cost of Regrowth by 65.
                throw new NotImplementedException();
            };
            // 27886 - Idol of the Emerald Queen
            item = GetItem(27886, "Idol of the Emerald Queen");
            item.OnEquip = (sim) =>
            {
                //Increases the periodic healing of your Lifebloom by up to 88.
                throw new NotImplementedException();
            };
            // 23197 - Idol of the Moon
            item = GetItem(23197, "Idol of the Moon");
            item.OnEquip = (sim) =>
            {
                //Increases the damage of your Moonfire spell by up to 33.
                throw new NotImplementedException();
            };
            // 32387 - Idol of the Raven Goddess
            item = GetItem(32387, "Idol of the Raven Goddess");
            item.OnEquip = (sim) =>
            {
                //Increases the healing granted by the Tree of Life form aura by 44, adds 20 critical strike rating to the Leader of the Pack aura, and adds 20 spell critical strike rating to the Moonkin form aura.
                throw new NotImplementedException();
            };
            // 33510 - Idol of the Unseen Moon
            item = GetItem(33510, "Idol of the Unseen Moon");
            item.OnEquip = (sim) =>
            {
                //Your Moonfire ability has a chance to grant up to 140 spell damage and healing for 10 sec.
                throw new NotImplementedException();
            };
            // 32257 - Idol of the White Stag
            item = GetItem(32257, "Idol of the White Stag");
            item.OnEquip = (sim) =>
            {
                //Your Mangle ability also increases your attack power by 94 for 20 sec.
                throw new NotImplementedException();
            };
            // 28064 - Idol of the Wild
            item = GetItem(28064, "Idol of the Wild");
            item.OnEquip = (sim) =>
            {
                //Increases the damage dealt by Mangle (Cat) by 24 and the damage dealt by Mangle (Bear) by 52.
                throw new NotImplementedException();
            };
            // 27744 - Idol of Ursoc
            item = GetItem(27744, "Idol of Ursoc");
            item.OnEquip = (sim) =>
            {
                //Increases initial and per application periodic damage done by Lacerate by 8.
                throw new NotImplementedException();
            };
            // 27518 - Ivory Idol of the Moongoddess
            item = GetItem(27518, "Ivory Idol of the Moongoddess");
            item.OnEquip = (sim) =>
            {
                //Increases the damage of your Starfire spell by up to 55.
                throw new NotImplementedException();
            };
            // 33946 - Merciless Gladiator's Idol of Resolve
            item = GetItem(33946, "Merciless Gladiator's Idol of Resolve");
            item.OnEquip = (sim) =>
            {
                //Your Mangle ability also grants you 31 resilience rating for 6 sec.
                throw new NotImplementedException();
            };
            // 25798 - 130 Epic Warrior Trinket
            item = GetItem(25798, "130 Epic Warrior Trinket");
            item.OnUse = (sim) =>
            {
                //Use: Increases attack power by 260 for 20 sec. (2 Min Cooldown)
                throw new NotImplementedException();
            };
            // 19998 - Bloodvine Lens
            item = GetItem(19998, "Bloodvine Lens");
            item.OnEquip = (sim) =>
            {
                //Slightly increases your stealth detection.
                throw new NotImplementedException();
            };
            // 33943 - Merciless Gladiator's Idol of Steadfastness
            item = GetItem(33943, "Merciless Gladiator's Idol of Steadfastness");
            item.OnEquip = (sim) =>
            {
                //Your Moonfire ability also grants you 31 resilience rating for 6 sec.
                throw new NotImplementedException();
            };
            // 25801 - 130 Test Caster Trinket
            item = GetItem(25801, "130 Test Caster Trinket");
            item.OnUse = (sim) =>
            {
                //Use: Increases damage and healing done by magical spells and effects by up to 187 for 20 sec. (2 Min Cooldown)
                throw new NotImplementedException();
            };
            // 7052 - Azure Silk Belt
            item = GetItem(7052, "Azure Silk Belt");
            item.OnEquip = (sim) =>
            {
                //Increases swim speed by 15%.
                throw new NotImplementedException();
            };
            // 33076 - Merciless Gladiator's Idol of Tenacity
            item = GetItem(33076, "Merciless Gladiator's Idol of Tenacity");
            item.OnEquip = (sim) =>
            {
                //Increases the final healing value of your Lifebloom by 105.
                throw new NotImplementedException();
            };
            // 28288 - Abacus of Violent Odds
            item = GetItem(28288, "Abacus of Violent Odds");
            item.OnUse = (sim) =>
            {
                //Use: Increases haste rating by 260 for 10 sec. (2 Min Cooldown)
                throw new NotImplementedException();
            };
            // 33947 - Vengeful Gladiator's Idol of Resolve
            item = GetItem(33947, "Vengeful Gladiator's Idol of Resolve");
            item.OnEquip = (sim) =>
            {
                //Your Mangle ability also grants you 34 resilience rating for 6 sec.
                throw new NotImplementedException();
            };
            // 20534 - Abyss Shard
            item = GetItem(20534, "Abyss Shard");
            item.OnUse = (sim) =>
            {
                //Use: Casts your Summon Voidwalker spell with no mana or Soul Shard requirements. (30 Min Cooldown)
                throw new NotImplementedException();
            };
            // 33944 - Vengeful Gladiator's Idol of Steadfastness
            item = GetItem(33944, "Vengeful Gladiator's Idol of Steadfastness");
            item.OnEquip = (sim) =>
            {
                //Your Moonfire ability also grants you 34 resilience rating for 6 sec.
                throw new NotImplementedException();
            };
            // 27891 - Adamantine Figurine
            item = GetItem(27891, "Adamantine Figurine");
            item.OnUse = (sim) =>
            {
                //Use: Increases armor by 1280 for 20 sec. (2 Min Cooldown)
                throw new NotImplementedException();
            };
            // 33841 - Vengeful Gladiator's Idol of Tenacity
            item = GetItem(33841, "Vengeful Gladiator's Idol of Tenacity");
            item.OnEquip = (sim) =>
            {
                //Increases the final healing value of your Lifebloom by 116.
                throw new NotImplementedException();
            };
            // 19345 - Aegis of Preservation
            item = GetItem(19345, "Aegis of Preservation");
            item.OnUse = (sim) =>
            {
                //Use: Increases armor by 500, and heals 35 damage every time you take ranged or melee damage for 20 sec. (5 Min Cooldown)
                throw new NotImplementedException();
            };
            // 17067 - Ancient Cornerstone Grimoire
            item = GetItem(17067, "Ancient Cornerstone Grimoire");
            item.OnUse = (sim) =>
            {
                //Use: Summons a Skeleton that will protect you for 1 min. (15 Min Cooldown)
                throw new NotImplementedException();
            };
            // 32771 - Airman's Ribbon of Gallantry
            item = GetItem(32771, "Airman's Ribbon of Gallantry");
            item.OnEquip = (sim) =>
            {
                //50% chance to increase your spell damage and healing by 80 for 30 sec when you kill a target that gives experience or honor. This effect cannot occur more than once every 10 seconds.
                throw new NotImplementedException();
            };
            // 27896 - Alembic of Infernal Power
            item = GetItem(27896, "Alembic of Infernal Power");
            item.OnEquip = (sim) =>
            {
                //When struck in combat has a 2% chance of gaining 260 mana.
                throw new NotImplementedException();
            };
            // 33830 - Ancient Aqir Artifact
            item = GetItem(33830, "Ancient Aqir Artifact");
            item.OnUse = (sim) =>
            {
                //Use: Increases armor by 2500 for 20 sec. (2 Min Cooldown)
                throw new NotImplementedException();
            };
            // 22015 - Beastmaster's Gloves
            item = GetItem(22015, "Beastmaster's Gloves");
            item.OnEquip = (sim) =>
            {
                //Increases your pet's critical strike chance by 2%.
                throw new NotImplementedException();
            };
            // 17577 - Blood Guard's Dreadweave Gloves
            item = GetItem(17577, "Blood Guard's Dreadweave Gloves");
            item.OnEquip = (sim) =>
            {
                //Gives you a 50% chance to avoid interruption caused by damage while casting Searing Pain.
                throw new NotImplementedException();
            };
            // 25620 - Ancient Crystal Talisman
            item = GetItem(25620, "Ancient Crystal Talisman");
            item.OnUse = (sim) =>
            {
                //Use: Increases damage and healing done by magical spells and effects by up to 104 for 20 sec. (2 Min Cooldown)
                throw new NotImplementedException();
            };
            // 7507 - Arcane Orb
            item = GetItem(7507, "Arcane Orb");
            item.OnUse = (sim) =>
            {
                //Use: Restores 140 to 180 mana. (30 Min Cooldown)
                throw new NotImplementedException();
            };
            // 22194 - Black Grasp of the Destroyer
            item = GetItem(22194, "Black Grasp of the Destroyer");
            item.OnEquip = (sim) =>
            {
                //On successful melee or ranged attack gain 8 mana and if possible drain 8 mana from the target.
                throw new NotImplementedException();
            };
            // 22865 - Blood Guard's Dreadweave Handwraps
            item = GetItem(22865, "Blood Guard's Dreadweave Handwraps");
            item.OnEquip = (sim) =>
            {
                //Gives you a 50% chance to avoid interruption caused by damage while casting Searing Pain.
                throw new NotImplementedException();
            };
            // 31615 - Ancient Draenei Arcane Relic
            item = GetItem(31615, "Ancient Draenei Arcane Relic");
            item.OnUse = (sim) =>
            {
                //Use: Increases spell damage done by up to 120 and healing done by up to 220 for 15 sec. (1 Min, 30 Sec Cooldown)
                throw new NotImplementedException();
            };
            // 17617 - Blood Guard's Satin Gloves
            item = GetItem(17617, "Blood Guard's Satin Gloves");
            item.OnEquip = (sim) =>
            {
                //Gives you a 50% chance to avoid interruption caused by damage while casting Mind Blast.
                throw new NotImplementedException();
            };
            // 31617 - Ancient Draenei War Talisman
            item = GetItem(31617, "Ancient Draenei War Talisman");
            item.OnUse = (sim) =>
            {
                //Use: Increases attack power by 200 for 15 sec. (1 Min, 30 Sec Cooldown)
                throw new NotImplementedException();
            };
            // 22869 - Blood Guard's Satin Handwraps
            item = GetItem(22869, "Blood Guard's Satin Handwraps");
            item.OnEquip = (sim) =>
            {
                //Gives you a 50% chance to avoid interruption caused by damage while casting Mind Blast.
                throw new NotImplementedException();
            };
            // 16498 - Blood Guard's Leather Treads
            item = GetItem(16498, "Blood Guard's Leather Treads");
            item.OnEquip = (sim) =>
            {
                //Increases the duration of your Sprint ability by 3 sec.
                throw new NotImplementedException();
            };
            // 1713 - Ankh of Life
            item = GetItem(1713, "Ankh of Life");
            item.OnUse = (sim) =>
            {
                //Use: Heal your target for 135 to 165. (5 Min Cooldown)
                throw new NotImplementedException();
            };
            // 16530 - Blood Guard's Chain Gauntlets
            item = GetItem(16530, "Blood Guard's Chain Gauntlets");
            item.OnEquip = (sim) =>
            {
                //Reduces the mana cost of your Arcane Shot by 15.
                throw new NotImplementedException();
            };
            // 16487 - Blood Guard's Silk Gloves
            item = GetItem(16487, "Blood Guard's Silk Gloves");
            item.OnEquip = (sim) =>
            {
                //Increases the damage absorbed by your Mana Shield by 285.
                throw new NotImplementedException();
            };
            // 22856 - Blood Guard's Leather Walkers
            item = GetItem(22856, "Blood Guard's Leather Walkers");
            item.OnEquip = (sim) =>
            {
                //Increases the duration of your Sprint ability by 3 sec.
                throw new NotImplementedException();
            };
            // 19336 - Arcane Infused Gem
            item = GetItem(19336, "Arcane Infused Gem");
            item.OnUse = (sim) =>
            {
                //Use: Infuses you with Arcane energy, causing your next Arcane Shot fired within 10 sec to detonate at the target. The Arcane Detonation will deal 185 to 215 damage to enemies near the target. (2 Min Cooldown)
                throw new NotImplementedException();
            };
            // 22862 - Blood Guard's Chain Vices
            item = GetItem(22862, "Blood Guard's Chain Vices");
            item.OnEquip = (sim) =>
            {
                //Increases the damage done by your Multi-Shot by 5%.
                throw new NotImplementedException();
            };
            // 22870 - Blood Guard's Silk Handwraps
            item = GetItem(22870, "Blood Guard's Silk Handwraps");
            item.OnEquip = (sim) =>
            {
                //Increases the damage absorbed by your Mana Shield by 285.
                throw new NotImplementedException();
            };
            // 13243 - Argent Defender
            item = GetItem(13243, "Argent Defender");
            item.OnEquip = (sim) =>
            {
                //Has a 1% chance when struck in combat of increasing block rating by 250 for 10 sec.
                throw new NotImplementedException();
            };
            // 22060 - Beastmaster's Tunic
            item = GetItem(22060, "Beastmaster's Tunic");
            item.OnEquip = (sim) =>
            {
                //Increases your pet's armor by 10%.
                throw new NotImplementedException();
            };
            // 28223 - Arcanist's Stone
            item = GetItem(28223, "Arcanist's Stone");
            item.OnUse = (sim) =>
            {
                //Use: Increases damage and healing done by magical spells and effects by up to 167 for 20 sec. (2 Min Cooldown)
                throw new NotImplementedException();
            };
            // 16022 - Arcanite Dragonling
            item = GetItem(16022, "Arcanite Dragonling");
            item.OnUse = (sim) =>
            {
                //Use: Activates your Arcanite Dragonling to fight for you for 1 min. (20 Min Cooldown)
                throw new NotImplementedException();
            };
            // 19024 - Arena Grand Master
            item = GetItem(19024, "Arena Grand Master");
            item.OnUse = (sim) =>
            {
                //Use: Absorbs 750 to 1250 damage. &nbsp;Lasts 20 sec. (15 Min Cooldown)
                throw new NotImplementedException();
            };
            // 23073 - Boots of Displacement
            item = GetItem(23073, "Boots of Displacement");
            item.OnEquip = (sim) =>
            {
                //Increases your effective stealth level.
                throw new NotImplementedException();
            };
            // 27770 - Argussian Compass
            item = GetItem(27770, "Argussian Compass");
            item.OnUse = (sim) =>
            {
                //Use: Reduces damage from each attack by 68, up to a total of 1150 damage absorbed. &nbsp;Lasts 20 sec. (2 Min Cooldown)
                throw new NotImplementedException();
            };
            // 32490 - Ashtongue Talisman of Acumen
            item = GetItem(32490, "Ashtongue Talisman of Acumen");
            item.OnEquip = (sim) =>
            {
                //Each time your Shadow Word: Pain deals damage, it has a 10% chance to grant you 220 spell damage for 10 sec and each time your Renew heals, it has a 10% chance to grant you 220 healing for 5 sec.
                throw new NotImplementedException();
            };
            // 32486 - Ashtongue Talisman of Equilibrium
            item = GetItem(32486, "Ashtongue Talisman of Equilibrium");
            item.OnEquip = (sim) =>
            {
                //Mangle has a 40% chance to grant 140 Strength for 8 sec, Starfire has a 25% chance to grant up to 150 spell damage for 8 sec, and Rejuvenation has a 25% chance to grant up to 210 healing for 8 sec.
                throw new NotImplementedException();
            };
            // 32488 - Ashtongue Talisman of Insight
            item = GetItem(32488, "Ashtongue Talisman of Insight");
            item.OnEquip = (sim) =>
            {
                //Your spell critical strikes have a 50% chance to grant you 145 spell haste rating for 5 sec.
                throw new NotImplementedException();
            };
            // 35003 - Brutal Gladiator's Dreadweave Gloves
            item = GetItem(35003, "Brutal Gladiator's Dreadweave Gloves");
            item.OnEquip = (sim) =>
            {
                //Gives you a 50% chance to avoid interruption caused by damage while casting Fear.
                throw new NotImplementedException();
            };
            // 32492 - Ashtongue Talisman of Lethality
            item = GetItem(32492, "Ashtongue Talisman of Lethality");
            item.OnEquip = (sim) =>
            {
                //20% chance per combo point for your finishing moves to grant 145 critical strike rating for 10 sec.
                throw new NotImplementedException();
            };
            // 35011 - Brutal Gladiator's Felweave Handguards
            item = GetItem(35011, "Brutal Gladiator's Felweave Handguards");
            item.OnEquip = (sim) =>
            {
                //Gives you a 50% chance to avoid interruption caused by damage while casting Fear.
                throw new NotImplementedException();
            };
            // 32493 - Ashtongue Talisman of Shadows
            item = GetItem(32493, "Ashtongue Talisman of Shadows");
            item.OnEquip = (sim) =>
            {
                //Each time your Corruption deals damage, it has a 20% chance to grant you 220 spell damage for 5 sec.
                throw new NotImplementedException();
            };
            // 35053 - Brutal Gladiator's Mooncloth Gloves
            item = GetItem(35053, "Brutal Gladiator's Mooncloth Gloves");
            item.OnEquip = (sim) =>
            {
                //Reduces the cooldown of your Psychic Scream ability by 3 sec.
                throw new NotImplementedException();
            };
            // 32487 - Ashtongue Talisman of Swiftness
            item = GetItem(32487, "Ashtongue Talisman of Swiftness");
            item.OnEquip = (sim) =>
            {
                //Your Steady Shot has a 15% chance to grant you 275 attack power for 8 sec.
                throw new NotImplementedException();
            };
            // 35083 - Brutal Gladiator's Satin Gloves
            item = GetItem(35083, "Brutal Gladiator's Satin Gloves");
            item.OnEquip = (sim) =>
            {
                //Reduces the cooldown of your Psychic Scream ability by 3 sec.
                throw new NotImplementedException();
            };
            // 35098 - Brutal Gladiator's Silk Handguards
            item = GetItem(35098, "Brutal Gladiator's Silk Handguards");
            item.OnEquip = (sim) =>
            {
                //Gives you a 50% chance to avoid interruption caused by damage while casting Polymorph.
                throw new NotImplementedException();
            };
            // 32485 - Ashtongue Talisman of Valor
            item = GetItem(32485, "Ashtongue Talisman of Valor");
            item.OnEquip = (sim) =>
            {
                //Your Mortal Strike, Bloodthirst, and Shield Slam attacks have a 25% chance to heal you for 330 and grant 55 Strength for 12 sec.
                throw new NotImplementedException();
            };
            // 32491 - Ashtongue Talisman of Vision
            item = GetItem(32491, "Ashtongue Talisman of Vision");
            item.OnEquip = (sim) =>
            {
                //Lesser Healing Wave has a 10% chance to grant 170 mana, Lightning Bolt has a 15% chance to grant up to 170 mana, and Stormstrike has a 50% chance to grant up to 275 attack power for 10 sec.
                throw new NotImplementedException();
            };
            // 13353 - Book of the Dead
            item = GetItem(13353, "Book of the Dead");
            item.OnUse = (sim) =>
            {
                //Use: Summons a Skeleton that will protect you for 1 min. (15 Min Cooldown)
                throw new NotImplementedException();
            };
            // 32489 - Ashtongue Talisman of Zeal
            item = GetItem(32489, "Ashtongue Talisman of Zeal");
            item.OnEquip = (sim) =>
            {
                //Flash of Light and Holy Light have a 15% chance to grant your target 760 healing over 12 sec, and your Judgements have a 50% chance to inflict 480 damage on their target over 8 sec.
                throw new NotImplementedException();
            };
            // 35751 - Assassin's Alchemist Stone
            item = GetItem(35751, "Assassin's Alchemist Stone");
            item.OnEquip = (sim) =>
            {
                //Increases the effect that healing and mana potions have on the wearer by 40%. &nbsp;This effect does not stack.
                throw new NotImplementedException();
            };
            // 22206 - Bouquet of Red Roses
            item = GetItem(22206, "Bouquet of Red Roses");
            item.OnUse = (sim) =>
            {
                //Use: Shower a nearby target with a cascade of rose petals! (5 Min Cooldown)
                throw new NotImplementedException();
            };
            // 34991 - Brutal Gladiator's Chain Gauntlets
            item = GetItem(34991, "Brutal Gladiator's Chain Gauntlets");
            item.OnEquip = (sim) =>
            {
                //Increases the damage done by your Multi-Shot by 5%.
                throw new NotImplementedException();
            };
            // 24390 - Auslese's Light Channeler
            item = GetItem(24390, "Auslese's Light Channeler");
            item.OnUse = (sim) =>
            {
                //Use: Reduces the cost of your next spell cast within 10 sec by up to 215 mana. (3 Min Cooldown)
                throw new NotImplementedException();
            };
            // 32093 - Chancellor's Dreadweave Gloves
            item = GetItem(32093, "Chancellor's Dreadweave Gloves");
            item.OnEquip = (sim) =>
            {
                //Gives you a 50% chance to avoid interruption caused by damage while casting Fear.
                throw new NotImplementedException();
            };
            // 35043 - Brutal Gladiator's Linked Gauntlets
            item = GetItem(35043, "Brutal Gladiator's Linked Gauntlets");
            item.OnEquip = (sim) =>
            {
                //Improves the range of all Shock spells by 5 yards.
                throw new NotImplementedException();
            };
            // 32658 - Badge of Tenacity
            item = GetItem(32658, "Badge of Tenacity");
            item.OnUse = (sim) =>
            {
                //Use: Increases agility by 150 for 20 sec. (2 Min Cooldown)
                throw new NotImplementedException();
            };
            // 32100 - Chancellor's Mooncloth Mitts
            item = GetItem(32100, "Chancellor's Mooncloth Mitts");
            item.OnEquip = (sim) =>
            {
                //Increases the duration of your Psychic Scream spell by 1 sec.
                throw new NotImplementedException();
            };
            // 35049 - Brutal Gladiator's Mail Gauntlets
            item = GetItem(35049, "Brutal Gladiator's Mail Gauntlets");
            item.OnEquip = (sim) =>
            {
                //Improves the range of all Shock spells by 5 yards.
                throw new NotImplementedException();
            };
            // 28798 - Badge of the Protector
            item = GetItem(28798, "Badge of the Protector");
            item.OnUse = (sim) =>
            {
                //Use: Show how heroic you are! (5 Min Cooldown)
                throw new NotImplementedException();
            };
            // 32103 - Chancellor's Satin Gloves
            item = GetItem(32103, "Chancellor's Satin Gloves");
            item.OnEquip = (sim) =>
            {
                //Increases the duration of your Psychic Scream spell by 1 sec.
                throw new NotImplementedException();
            };
            // 35078 - Brutal Gladiator's Ringmail Gauntlets
            item = GetItem(35078, "Brutal Gladiator's Ringmail Gauntlets");
            item.OnEquip = (sim) =>
            {
                //Improves the range of all Shock spells by 5 yards.
                throw new NotImplementedException();
            };
            // 4444 - Black Husk Shield
            item = GetItem(4444, "Black Husk Shield");
            item.OnUse = (sim) =>
            {
                //Use: Attempts to cure 1 poison effect on the target, and 1 more poison effect every 2 seconds for 8 sec. (5 Min Cooldown)
                throw new NotImplementedException();
            };
            // 32110 - Chancellor's Silk Handguards
            item = GetItem(32110, "Chancellor's Silk Handguards");
            item.OnEquip = (sim) =>
            {
                //Improves the range of your Fire Blast spell by 5 yards.
                throw new NotImplementedException();
            };
            // 21670 - Badge of the Swarmguard
            item = GetItem(21670, "Badge of the Swarmguard");
            item.OnUse = (sim) =>
            {
                //Use: Gives a chance on melee or ranged attack to apply an armor penetration effect on you for 30 sec, lowering the target's physical armor by 200 to your own attacks. The armor penetration effect can be applied up to 6 times. (3 Min Cooldown)
                throw new NotImplementedException();
            };
            // 37128 - Balebrew Charm
            item = GetItem(37128, "Balebrew Charm");
            item.OnUse = (sim) =>
            {
                //Use: Right Click to summon the Black Brewmaiden, who will smite your foes with your empty tankards. Wave at her for Brewfest Brew! (10 Min Cooldown)
                throw new NotImplementedException();
            };
            // 28370 - Bangle of Endless Blessings
            item = GetItem(28370, "Bangle of Endless Blessings");
            item.OnEquip = (sim) =>
            {
                //Your spell casts have a chance to allow [15 - max(Level - 70, 0) / 2]% of your mana regeneration to continue while casting for 15 sec.
                throw new NotImplementedException();
            };
            item.OnUse = (sim) =>
            {
                //Use: Increases your Spirit by 130 for 20 sec. (2 Min Cooldown)
                throw new NotImplementedException();
            };
            // 7133 - Brutal Hauberk
            item = GetItem(7133, "Brutal Hauberk");
            item.OnUse = (sim) =>
            {
                //Use: Increase Rage by 30. (1 Hour Cooldown)
                throw new NotImplementedException();
            };
            // 14023 - Barov Peasant Caller
            item = GetItem(14023, "Barov Peasant Caller");
            item.OnUse = (sim) =>
            {
                //Use: Calls forth 3 servants of the House Barov that will fight, cook, and clean for you. (10 Min Cooldown)
                throw new NotImplementedException();
            };
            // 32235 - Cursed Vision of Sargeras
            item = GetItem(32235, "Cursed Vision of Sargeras");
            item.OnUse = (sim) =>
            {
                //Use: Shows the location of all nearby demons on the minimap until cancelled. &nbsp;Only one form of tracking can be active at a time. (10 Min Cooldown)
                throw new NotImplementedException();
            };
            // 35326 - Battlemaster's Alacrity
            item = GetItem(35326, "Battlemaster's Alacrity");
            item.OnUse = (sim) =>
            {
                //Use: Increases maximum health by 1750 for 15 sec. Shares cooldown with other Battlemaster's trinkets. (3 Min Cooldown)
                throw new NotImplementedException();
            };
            // 34579 - Battlemaster's Audacity
            item = GetItem(34579, "Battlemaster's Audacity");
            item.OnUse = (sim) =>
            {
                //Use: Increases maximum health by 1750 for 15 sec. Shares cooldown with other Battlemaster's trinkets. (3 Min Cooldown)
                throw new NotImplementedException();
            };
            // 34163 - Battlemaster's Cruelty
            item = GetItem(34163, "Battlemaster's Cruelty");
            item.OnUse = (sim) =>
            {
                //Use: Increases maximum health by 1750 for 15 sec. Shares cooldown with other Battlemaster's trinkets. (3 Min Cooldown)
                throw new NotImplementedException();
            };
            // 7515 - Celestial Orb
            item = GetItem(7515, "Celestial Orb");
            item.OnUse = (sim) =>
            {
                //Use: Restores 400 to 1200 mana. (30 Min Cooldown)
                throw new NotImplementedException();
            };
            // 34162 - Battlemaster's Depravity
            item = GetItem(34162, "Battlemaster's Depravity");
            item.OnUse = (sim) =>
            {
                //Use: Increases maximum health by 1750 for 15 sec. Shares cooldown with other Battlemaster's trinkets. (3 Min Cooldown)
                throw new NotImplementedException();
            };
            // 16787 - Amulet of Draconic Subversion
            item = GetItem(16787, "Amulet of Draconic Subversion");
            item.OnUse = (sim) =>
            {
                //Use: Use to disguise yourself as a member of the Black Dragonflight.
                throw new NotImplementedException();
            };
            // 32478 - Deathblow X11 Goggles
            item = GetItem(32478, "Deathblow X11 Goggles");
            item.OnEquip = (sim) =>
            {
                //Shows the location of all nearby gas clouds on the minimap.
                //Slightly increases your stealth detection.
                throw new NotImplementedException();
            };
            item.OnUse = (sim) =>
            {
                //Use: Allows you to look far into the distance. (3 Sec Cooldown)
                throw new NotImplementedException();
            };
            // 32134 - Chancellor's Chain Gauntlets
            item = GetItem(32134, "Chancellor's Chain Gauntlets");
            item.OnEquip = (sim) =>
            {
                //Increases the damage done by your Multi-Shot by 5%.
                throw new NotImplementedException();
            };
            // 10030 - Admiral's Hat
            item = GetItem(10030, "Admiral's Hat");
            item.OnUse = (sim) =>
            {
                //Use: Gives 10 additional stamina to party members within 30 yards. (1 Min Cooldown)
                throw new NotImplementedException();
            };
            // 34578 - Battlemaster's Determination
            item = GetItem(34578, "Battlemaster's Determination");
            item.OnUse = (sim) =>
            {
                //Use: Increases maximum health by 1750 for 15 sec. Shares cooldown with other Battlemaster's trinkets. (3 Min Cooldown)
                throw new NotImplementedException();
            };
            // 32139 - Chancellor's Linked Gauntlets
            item = GetItem(32139, "Chancellor's Linked Gauntlets");
            item.OnEquip = (sim) =>
            {
                //Increases the damage done by your Lightning Shield by 8%.
                throw new NotImplementedException();
            };
            // 32144 - Chancellor's Mail Gauntlets
            item = GetItem(32144, "Chancellor's Mail Gauntlets");
            item.OnEquip = (sim) =>
            {
                //Improves the range of all Shock spells by 5 yards.
                throw new NotImplementedException();
            };
            // 34580 - Battlemaster's Perseverance
            item = GetItem(34580, "Battlemaster's Perseverance");
            item.OnUse = (sim) =>
            {
                //Use: Increases maximum health by 1750 for 15 sec. Shares cooldown with other Battlemaster's trinkets. (3 Min Cooldown)
                throw new NotImplementedException();
            };
            // 32149 - Chancellor's Ringmail Gloves
            item = GetItem(32149, "Chancellor's Ringmail Gloves");
            item.OnEquip = (sim) =>
            {
                //Improves the range of all Shock spells by 5 yards.
                throw new NotImplementedException();
            };
            // 33831 - Berserker's Call
            item = GetItem(33831, "Berserker's Call");
            item.OnUse = (sim) =>
            {
                //Use: Increases attack power by 360 for 20 sec. (2 Min Cooldown)
                throw new NotImplementedException();
            };
            // 34427 - Blackened Naaru Sliver
            item = GetItem(34427, "Blackened Naaru Sliver");
            item.OnEquip = (sim) =>
            {
                //Chance on hit to enter a Battle Trance, during which your melee or ranged attacks will each grant 44 attack power, stacking up to 10 times. &nbsp;Expires after 20 sec.
                throw new NotImplementedException();
            };
            // 34847 - Annihilator Holo-Gogs
            item = GetItem(34847, "Annihilator Holo-Gogs");
            item.OnEquip = (sim) =>
            {
                //Shows the location of all nearby gas clouds on the minimap.
                //Slightly increases your stealth detection.
                throw new NotImplementedException();
            };
            item.OnUse = (sim) =>
            {
                //Use: Allows you to look far into the distance. (3 Sec Cooldown)
                throw new NotImplementedException();
            };
            // 23691 - Corrupted Mark of the Lightbringer
            item = GetItem(23691, "Corrupted Mark of the Lightbringer");
            item.OnUse = (sim) =>
            {
                //Use: Defiles Uther's Tomb. (1 Min Cooldown)
                throw new NotImplementedException();
            };
            // 28041 - Bladefist's Breadth
            item = GetItem(28041, "Bladefist's Breadth");
            item.OnUse = (sim) =>
            {
                //Use: Increases attack power by 200 for 15 sec. (1 Min, 30 Sec Cooldown)
                throw new NotImplementedException();
            };
            // 2802 - Blazing Emblem
            item = GetItem(2802, "Blazing Emblem");
            item.OnUse = (sim) =>
            {
                //Use: Increases Fire resistance by 50 and reduces all Fire damage taken by up to 25 for 15 sec. (10 Min Cooldown)
                throw new NotImplementedException();
            };
            // 20830 - Amulet of the Moon
            item = GetItem(20830, "Amulet of the Moon");
            item.OnUse = (sim) =>
            {
                //Use: Gives 10 additional spirit to nearby party members for 30 min.
                throw new NotImplementedException();
            };
            // 19990 - Blessed Prayer Beads
            item = GetItem(19990, "Blessed Prayer Beads");
            item.OnUse = (sim) =>
            {
                //Use: Increases healing done by spells and effects by up to 190 and damage done by spells by up to 64 for 20 sec. (2 Min Cooldown)
                throw new NotImplementedException();
            };
            // 34140 - Dark Iron Tankard
            item = GetItem(34140, "Dark Iron Tankard");
            item.OnUse = (sim) =>
            {
                //Use: An extremely potent alcoholic beverage. (1 Hour Cooldown)
                throw new NotImplementedException();
            };
            // 25644 - Blessed Book of Nagrand
            item = GetItem(25644, "Blessed Book of Nagrand");
            item.OnEquip = (sim) =>
            {
                //Increases healing done by Flash of Light by up to 79.
                throw new NotImplementedException();
            };
            // 31110 - Druidic Helmet of Second Sight
            item = GetItem(31110, "Druidic Helmet of Second Sight");
            item.OnEquip = (sim) =>
            {
                //Allows the bearer to see into the ghost world while in Shadowmoon Valley.
                throw new NotImplementedException();
            };
            // 29383 - Bloodlust Brooch
            item = GetItem(29383, "Bloodlust Brooch");
            item.OnUse = (sim) =>
            {
                //Use: Increases attack power by 278 for 20 sec. (2 Min Cooldown)
                throw new NotImplementedException();
            };
            // 22003 - Darkmantle Boots
            item = GetItem(22003, "Darkmantle Boots");
            item.OnEquip = (sim) =>
            {
                //Increases your effective stealth level.
                throw new NotImplementedException();
            };
            // 35328 - Dreadweave Gloves
            item = GetItem(35328, "Dreadweave Gloves");
            item.OnEquip = (sim) =>
            {
                //Gives you a 50% chance to avoid interruption caused by damage while casting Fear.
                throw new NotImplementedException();
            };
            // 35039 - Brutal Gladiator's Libram of Fortitude
            item = GetItem(35039, "Brutal Gladiator's Libram of Fortitude");
            item.OnEquip = (sim) =>
            {
                //Your Judgement ability also grants you 39 resilience rating for 6 sec.
                throw new NotImplementedException();
            };
            // 37127 - Brightbrew Charm
            item = GetItem(37127, "Brightbrew Charm");
            item.OnUse = (sim) =>
            {
                //Use: Right Click to summon the Brewmaiden, whose very presence bolsters a party's vigor. Wave at her for Brewfest Brew! (10 Min Cooldown)
                throw new NotImplementedException();
            };
            // 32534 - Brooch of the Immortal King
            item = GetItem(32534, "Brooch of the Immortal King");
            item.OnUse = (sim) =>
            {
                //Use: Increases maximum health by 1250 for 15 sec. (5 Min Cooldown)
                throw new NotImplementedException();
            };
            // 35040 - Brutal Gladiator's Libram of Justice
            item = GetItem(35040, "Brutal Gladiator's Libram of Justice");
            item.OnEquip = (sim) =>
            {
                //Causes your Flash of Light to increase the target's Resilience rating by 39 for 6 sec.
                throw new NotImplementedException();
            };
            // 32375 - Bulwark of Azzinoth
            item = GetItem(32375, "Bulwark of Azzinoth");
            item.OnEquip = (sim) =>
            {
                //When struck in combat has a 2% chance of increasing your Armor by 2000 for 10 secs.
                throw new NotImplementedException();
            };
            // 11832 - Burst of Knowledge
            item = GetItem(11832, "Burst of Knowledge");
            item.OnUse = (sim) =>
            {
                //Use: Reduces mana cost of all spells by 100 for 10 sec. (15 Min Cooldown)
                throw new NotImplementedException();
            };
            // 6182 - Dim Torch
            item = GetItem(6182, "Dim Torch");
            item.OnEquip = (sim) =>
            {
                //Increase the Spirit of nearby party members by 4.
                throw new NotImplementedException();
            };
            // 13382 - Cannonball Runner
            item = GetItem(13382, "Cannonball Runner");
            item.OnUse = (sim) =>
            {
                //Use: Summons a cannon that will fire at enemies in front of it that are attacking you. (5 Min Cooldown)
                throw new NotImplementedException();
            };
            // 35041 - Brutal Gladiator's Libram of Vengeance
            item = GetItem(35041, "Brutal Gladiator's Libram of Vengeance");
            item.OnEquip = (sim) =>
            {
                //Your Holy Shield ability also grants you 39 resilience rating for 6 sec.
                throw new NotImplementedException();
            };
            // 21755 - Aquamarine Pendant of the Warrior
            item = GetItem(21755, "Aquamarine Pendant of the Warrior");
            item.OnUse = (sim) =>
            {
                //Use: Gives 15 additional stamina to party members within 40 yards. &nbsp;Lasts 30 min.
                throw new NotImplementedException();
            };
            // 32695 - Captain's Badge
            item = GetItem(32695, "Captain's Badge");
            item.OnUse = (sim) =>
            {
                //Use: Calls forth a Netherwing Ally to fight at your side. (10 Min Cooldown)
                throw new NotImplementedException();
            };
            // 11122 - Carrot on a Stick
            item = GetItem(11122, "Carrot on a Stick");
            item.OnEquip = (sim) =>
            {
                //Increases mount speed by 3%.
                throw new NotImplementedException();
            };
            // 186065 - Communal Book of Healing
            item = GetItem(186065, "Communal Book of Healing");
            item.OnEquip = (sim) =>
            {
                //Increases healing done by Flash of Light by up to 10.
                throw new NotImplementedException();
            };
            // 23716 - Carved Ogre Idol
            item = GetItem(23716, "Carved Ogre Idol");
            item.OnUse = (sim) =>
            {
                //Use: Become a hulking red ogre for 10 min! (10 Min Cooldown)
                throw new NotImplementedException();
            };
            // 32461 - Furious Gizmatic Goggles
            item = GetItem(32461, "Furious Gizmatic Goggles");
            item.OnEquip = (sim) =>
            {
                //Shows the location of all nearby gas clouds on the minimap.
                //Slightly increases your stealth detection.
                throw new NotImplementedException();
            };
            item.OnUse = (sim) =>
            {
                //Use: Allows you to look far into the distance. (3 Sec Cooldown)
                throw new NotImplementedException();
            };
            // 186066 - Communal Book of Protection
            item = GetItem(186066, "Communal Book of Protection");
            item.OnEquip = (sim) =>
            {
                //Increases the armor from your Devotion Aura by 50.
                throw new NotImplementedException();
            };
            // 10455 - Chained Essence of Eranikus
            item = GetItem(10455, "Chained Essence of Eranikus");
            item.OnUse = (sim) =>
            {
                //Use: Poisons all enemies in an 8 yard radius around the caster. &nbsp;Victims of the poison suffer 50 Nature damage every 5 sec for 45 sec. (15 Min Cooldown)
                throw new NotImplementedException();
            };
            // 25787 - Charm of Alacrity
            item = GetItem(25787, "Charm of Alacrity");
            item.OnUse = (sim) =>
            {
                //Use: Increases dodge rating by 192 for 10 sec. (1 Min, 30 Sec Cooldown)
                throw new NotImplementedException();
            };
            // 21780 - Blood Crown
            item = GetItem(21780, "Blood Crown");
            item.OnEquip = (sim) =>
            {
                //Heals for 12 to 18 damage when you get a critical hit.
                throw new NotImplementedException();
            };
            // 186067 - Communal Book of Righteousness
            item = GetItem(186067, "Communal Book of Righteousness");
            item.OnEquip = (sim) =>
            {
                //Reduces the base mana cost of your Seal spells by 5.
                throw new NotImplementedException();
            };
            // 32481 - Charm of Swift Flight
            item = GetItem(32481, "Charm of Swift Flight");
            item.OnEquip = (sim) =>
            {
                //Increases speed in Flight Form and Swift Flight Form by 10%.
                throw new NotImplementedException();
            };
            // 12185 - Bloodsail Admiral's Hat
            item = GetItem(12185, "Bloodsail Admiral's Hat");
            item.OnUse = (sim) =>
            {
                //Use: Right Click to summon and dismiss your bird.
                throw new NotImplementedException();
            };
            // 5079 - Cold Basilisk Eye
            item = GetItem(5079, "Cold Basilisk Eye");
            item.OnUse = (sim) =>
            {
                //Use: Increases time between target's attacks by 5% for 15 sec. (5 Min Cooldown)
                throw new NotImplementedException();
            };
            // 33936 - Gladiator's Libram of Fortitude
            item = GetItem(33936, "Gladiator's Libram of Fortitude");
            item.OnEquip = (sim) =>
            {
                //Your Judgement ability also grants you 26 resilience rating for 6 sec.
                throw new NotImplementedException();
            };
            // 32864 - Commander's Badge
            item = GetItem(32864, "Commander's Badge");
            item.OnUse = (sim) =>
            {
                //Use: Calls forth a Netherwing Ally to fight at your side. (10 Min Cooldown)
                throw new NotImplementedException();
            };
            // 28356 - Gladiator's Libram of Justice
            item = GetItem(28356, "Gladiator's Libram of Justice");
            item.OnEquip = (sim) =>
            {
                //Causes your Flash of Light to increase the target's Resilience rating by 26 for 6 sec.
                throw new NotImplementedException();
            };
            // 34473 - Commendation of Kael'thas
            item = GetItem(34473, "Commendation of Kael'thas");
            item.OnEquip = (sim) =>
            {
                //Melee attacks which reduce you below 35% health cause you to gain 152 dodge rating for 10 sec. &nbsp;Cannot occur more than once every 30 sec.
                throw new NotImplementedException();
            };
            // 185986 - Communal Stone of Durability
            item = GetItem(185986, "Communal Stone of Durability");
            item.OnEquip = (sim) =>
            {
                //Have a 2% chance when struck in combat of increasing armor by 350 for 15 sec.
                throw new NotImplementedException();
            };
            // 17111 - Blazefury Medallion
            item = GetItem(17111, "Blazefury Medallion");
            item.OnEquip = (sim) =>
            {
                //Adds 2 fire damage to your melee attacks.
                throw new NotImplementedException();
            };
            // 33948 - Gladiator's Libram of Vengeance
            item = GetItem(33948, "Gladiator's Libram of Vengeance");
            item.OnEquip = (sim) =>
            {
                //Your Holy Shield ability also grants you 26 resilience rating for 6 sec.
                throw new NotImplementedException();
            };
            // 32757 - Blessed Medallion of Karabor
            item = GetItem(32757, "Blessed Medallion of Karabor");
            item.OnUse = (sim) =>
            {
                //Use: Teleports the bearer to the footsteps of Illidan's fortress. (15 Min Cooldown)
                throw new NotImplementedException();
            };
            // 185988 - Communal Stone of Stoicism
            item = GetItem(185988, "Communal Stone of Stoicism");
            item.OnEquip = (sim) =>
            {
                //Restores 8 health every 6 sec.
                throw new NotImplementedException();
            };
            item.OnUse = (sim) =>
            {
                //Use: Restores 200 to 600 health. (10 Min Cooldown)
                throw new NotImplementedException();
            };
            // 30063 - Libram of Absolute Truth
            item = GetItem(30063, "Libram of Absolute Truth");
            item.OnEquip = (sim) =>
            {
                //Reduces the mana cost of Holy Light by 34.
                throw new NotImplementedException();
            };
            // 35345 - Evoker's Silk Handguards
            item = GetItem(35345, "Evoker's Silk Handguards");
            item.OnEquip = (sim) =>
            {
                //Improves the range of your Fire Blast spell by 5 yards.
                throw new NotImplementedException();
            };
            // 24114 - Braided Eternium Chain
            item = GetItem(24114, "Braided Eternium Chain");
            item.OnUse = (sim) =>
            {
                //Use: Increases the critical strike rating of nearby party members by 28 for 30 min. (1 Hour Cooldown)
                throw new NotImplementedException();
            };
            // 27484 - Libram of Avengement
            item = GetItem(27484, "Libram of Avengement");
            item.OnEquip = (sim) =>
            {
                //Causes your Judgement of Command, Judgement of Righteousness, Judgement of Blood, and Judgement of Vengeance to increase your Critical Strike rating by 53 for 5 sec.
                throw new NotImplementedException();
            };
            // 29776 - Core of Ar'kelos
            item = GetItem(29776, "Core of Ar'kelos");
            item.OnUse = (sim) =>
            {
                //Use: Increases your melee and ranged attack power by 200. &nbsp;Effect lasts for 20 sec. (2 Min Cooldown)
                throw new NotImplementedException();
            };
            // 8367 - Dragonscale Breastplate
            item = GetItem(8367, "Dragonscale Breastplate");
            item.OnUse = (sim) =>
            {
                //Use: Absorbs 600 magical damage. &nbsp;Lasts 2 min. (1 Hour Cooldown)
                throw new NotImplementedException();
            };
            // 31154 - Bronze Torc
            item = GetItem(31154, "Bronze Torc");
            item.OnEquip = (sim) =>
            {
                //Reduces melee damage taken by 2.
                throw new NotImplementedException();
            };
            // 38289 - Coren's Lucky Coin
            item = GetItem(38289, "Coren's Lucky Coin");
            item.OnUse = (sim) =>
            {
                //Use: Increases the block value of your shield by 200 for 20 sec. (2 Min Cooldown)
                throw new NotImplementedException();
            };
            // 33503 - Libram of Divine Judgement
            item = GetItem(33503, "Libram of Divine Judgement");
            item.OnEquip = (sim) =>
            {
                //Your Judgement of Command ability has a chance to grant 200 attack power for 10 sec.
                throw new NotImplementedException();
            };
            // 5323 - Everglow Lantern
            item = GetItem(5323, "Everglow Lantern");
            item.OnUse = (sim) =>
            {
                //Use: Heal your target for 135 to 165. (30 Min Cooldown)
                throw new NotImplementedException();
            };
            // 33504 - Libram of Divine Purpose
            item = GetItem(33504, "Libram of Divine Purpose");
            item.OnEquip = (sim) =>
            {
                //Increases the damage done by your Seal of Righteousness and Judgement of Righteousness abilities by up to 94.
                throw new NotImplementedException();
            };
            // 13347 - Crystal of Zin-Malor
            item = GetItem(13347, "Crystal of Zin-Malor");
            item.OnEquip = (sim) =>
            {
                //Deals damage and drains 100 to 500 mana every second if you are not worthy.
                throw new NotImplementedException();
            };
            // 32654 - Crystalforged Trinket
            item = GetItem(32654, "Crystalforged Trinket");
            item.OnUse = (sim) =>
            {
                //Use: Increases attack power by 216 for 10 sec. (1 Min Cooldown)
                throw new NotImplementedException();
            };
            // 34357 - Hard Khorium Goggles
            item = GetItem(34357, "Hard Khorium Goggles");
            item.OnEquip = (sim) =>
            {
                //Shows the location of all nearby gas clouds on the minimap.
                //Slightly increases your stealth detection.
                throw new NotImplementedException();
            };
            item.OnUse = (sim) =>
            {
                //Use: Allows you to look far into the distance. (3 Sec Cooldown)
                throw new NotImplementedException();
            };
            // 23201 - Libram of Divinity
            item = GetItem(23201, "Libram of Divinity");
            item.OnEquip = (sim) =>
            {
                //Increases healing done by Flash of Light by up to 53.
                throw new NotImplementedException();
            };
            // 30300 - Dabiri's Enigma
            item = GetItem(30300, "Dabiri's Enigma");
            item.OnUse = (sim) =>
            {
                //Use: Increases block rating by 125 for 15 sec. (1 Min, 30 Sec Cooldown)
                throw new NotImplementedException();
            };
            // 10501 - Catseye Ultra Goggles
            item = GetItem(10501, "Catseye Ultra Goggles");
            item.OnEquip = (sim) =>
            {
                //Moderately increases your stealth detection.
                throw new NotImplementedException();
            };
            // 38290 - Dark Iron Smoking Pipe
            item = GetItem(38290, "Dark Iron Smoking Pipe");
            item.OnUse = (sim) =>
            {
                //Use: Increases damage and healing done by magical spells and effects by up to 155 for 20 sec. (2 Min Cooldown)
                throw new NotImplementedException();
            };
            // 24121 - Chain of the Twilight Owl
            item = GetItem(24121, "Chain of the Twilight Owl");
            item.OnUse = (sim) =>
            {
                //Use: Increases the spell critical hit chance of nearby party members by 2% for 30 min. (1 Hour Cooldown)
                throw new NotImplementedException();
            };
            // 23203 - Libram of Fervor
            item = GetItem(23203, "Libram of Fervor");
            item.OnEquip = (sim) =>
            {
                //Increases the melee attack power bonus of your Seal of the Crusader by 48 and the Holy damage increase of your Judgement of the Crusader by 33.
                throw new NotImplementedException();
            };
            // 19288 - Darkmoon Card: Blue Dragon
            item = GetItem(19288, "Darkmoon Card: Blue Dragon");
            item.OnEquip = (sim) =>
            {
                //2% chance on successful spellcast to allow 100% of your Mana regeneration to continue while casting for 15 sec.
                throw new NotImplementedException();
            };
            // 31856 - Darkmoon Card: Crusade
            item = GetItem(31856, "Darkmoon Card: Crusade");
            item.OnEquip = (sim) =>
            {
                //Each time you deal melee or ranged damage to an opponent, you gain 6 attack power for the next 10 sec., stacking up to 20 times. &nbsp;Each time you land a harmful spell on an opponent, you gain 8 spell damage for the next 10 sec., stacking up to 10 times.
                throw new NotImplementedException();
            };
            // 22402 - Libram of Grace
            item = GetItem(22402, "Libram of Grace");
            item.OnEquip = (sim) =>
            {
                //Reduces the mana cost of your Cleanse spell by 25.
                throw new NotImplementedException();
            };
            // 13375 - Crest of Retribution
            item = GetItem(13375, "Crest of Retribution");
            item.OnEquip = (sim) =>
            {
                //Deals 5 to 35 damage every time you block.
                throw new NotImplementedException();
            };
            // 19287 - Darkmoon Card: Heroism
            item = GetItem(19287, "Darkmoon Card: Heroism");
            item.OnEquip = (sim) =>
            {
                //Sometimes heals bearer of 120 to 180 damage when damaging an enemy in melee.
                throw new NotImplementedException();
            };
            // 31859 - Darkmoon Card: Madness
            item = GetItem(31859, "Darkmoon Card: Madness");
            item.OnEquip = (sim) =>
            {
                //Each time you land a killing blow on an enemy that yields experience or honor, you gain the Power of Madness.
                throw new NotImplementedException();
            };
            // 22401 - Libram of Hope
            item = GetItem(22401, "Libram of Hope");
            item.OnEquip = (sim) =>
            {
                //Reduces the base mana cost of your Seal spells by 20.
                throw new NotImplementedException();
            };
            // 19289 - Darkmoon Card: Maelstrom
            item = GetItem(19289, "Darkmoon Card: Maelstrom");
            item.OnEquip = (sim) =>
            {
                //Chance to strike your melee target with lightning for 200 to 300 Nature damage.
                throw new NotImplementedException();
            };
            // 19290 - Darkmoon Card: Twisting Nether
            item = GetItem(19290, "Darkmoon Card: Twisting Nether");
            item.OnEquip = (sim) =>
            {
                //Gives the wearer a 10% chance of being able to resurrect with 20% health and mana.
                throw new NotImplementedException();
            };
            // 23006 - Libram of Light
            item = GetItem(23006, "Libram of Light");
            item.OnEquip = (sim) =>
            {
                //Increases healing done by Flash of Light by up to 83.
                throw new NotImplementedException();
            };
            // 23565 - Embrace of the Twisting Nether
            item = GetItem(23565, "Embrace of the Twisting Nether");
            item.OnUse = (sim) =>
            {
                //Use: You are protected from all physical attacks for 6 sec, but cannot attack or use physical abilities. (30 Min Cooldown)
                throw new NotImplementedException();
            };
            // 17588 - General's Dreadweave Gloves
            item = GetItem(17588, "General's Dreadweave Gloves");
            item.OnEquip = (sim) =>
            {
                //Gives you a 50% chance to avoid interruption caused by damage while casting Searing Pain.
                throw new NotImplementedException();
            };
            // 16768 - Furbolg Medicine Pouch
            item = GetItem(16768, "Furbolg Medicine Pouch");
            item.OnUse = (sim) =>
            {
                //Use: Restores 100 health every 1 sec for 10 sec. (20 Min Cooldown)
                throw new NotImplementedException();
            };
            // 31858 - Darkmoon Card: Vengeance
            item = GetItem(31858, "Darkmoon Card: Vengeance");
            item.OnEquip = (sim) =>
            {
                //You have a 10% chance when hit by an attack or harmful spell to deal 95 to 115 holy damage to your attacker.
                throw new NotImplementedException();
            };
            // 17620 - General's Satin Gloves
            item = GetItem(17620, "General's Satin Gloves");
            item.OnEquip = (sim) =>
            {
                //Gives you a 50% chance to avoid interruption caused by damage while casting Mind Blast.
                throw new NotImplementedException();
            };
            // 16540 - General's Silk Handguards
            item = GetItem(16540, "General's Silk Handguards");
            item.OnEquip = (sim) =>
            {
                //Increases the damage absorbed by your Mana Shield by 285.
                throw new NotImplementedException();
            };
            // 31857 - Darkmoon Card: Wrath
            item = GetItem(31857, "Darkmoon Card: Wrath");
            item.OnEquip = (sim) =>
            {
                //Each time one of your direct damage attacks does not critically strike, you gain 17 critical strike rating and 17 spell critical strike rating for the next 10 sec. &nbsp;This effect is consumed when you deal a critical strike.
                throw new NotImplementedException();
            };
            // 33502 - Libram of Mending
            item = GetItem(33502, "Libram of Mending");
            item.OnEquip = (sim) =>
            {
                //Your Holy Light spell grants 22 mana per 5 sec. for 30 sec.
                throw new NotImplementedException();
            };
            // 8348 - Helm of Fire
            item = GetItem(8348, "Helm of Fire");
            item.OnUse = (sim) =>
            {
                //Use: Hurls a fiery ball that causes 286 to 376 Fire damage and an additional 40 damage over 8 sec. (15 Min Cooldown)
                throw new NotImplementedException();
            };
            // 21326 - Defender of the Timbermaw
            item = GetItem(21326, "Defender of the Timbermaw");
            item.OnUse = (sim) =>
            {
                //Use: Calls forth a Timbermaw Ancestor to fight at your side and heal you. (10 Min Cooldown)
                throw new NotImplementedException();
            };
            // 24556 - Gladiator's Dreadweave Gloves
            item = GetItem(24556, "Gladiator's Dreadweave Gloves");
            item.OnEquip = (sim) =>
            {
                //Gives you a 50% chance to avoid interruption caused by damage while casting Fear.
                throw new NotImplementedException();
            };
            // 16558 - General's Leather Treads
            item = GetItem(16558, "General's Leather Treads");
            item.OnEquip = (sim) =>
            {
                //Increases the duration of your Sprint ability by 3 sec.
                throw new NotImplementedException();
            };
            // 21120 - Defiler's Talisman
            item = GetItem(21120, "Defiler's Talisman");
            item.OnUse = (sim) =>
            {
                //Use: Absorbs 248 to 302 physical damage. &nbsp;Lasts 15 sec. (3 Min Cooldown)
                throw new NotImplementedException();
            };
            // 20967 - Citrine Pendant of Golden Healing
            item = GetItem(20967, "Citrine Pendant of Golden Healing");
            item.OnUse = (sim) =>
            {
                //Use: Restores 3 health to all nearby party members every 5 seconds for 30 min.
                throw new NotImplementedException();
            };
            // 30188 - Gladiator's Felweave Handguards
            item = GetItem(30188, "Gladiator's Felweave Handguards");
            item.OnEquip = (sim) =>
            {
                //Gives you a 50% chance to avoid interruption caused by damage while casting Fear.
                throw new NotImplementedException();
            };
            // 31033 - Libram of Righteous Power
            item = GetItem(31033, "Libram of Righteous Power");
            item.OnEquip = (sim) =>
            {
                //Increases the damage dealt by Crusader Strike by 36.
                throw new NotImplementedException();
            };
            // 31409 - Gladiator's Mooncloth Gloves
            item = GetItem(31409, "Gladiator's Mooncloth Gloves");
            item.OnEquip = (sim) =>
            {
                //Increases the duration of your Psychic Scream spell by 1 sec.
                throw new NotImplementedException();
            };
            // 11861 - Girdle of Reprisal
            item = GetItem(11861, "Girdle of Reprisal");
            item.OnEquip = (sim) =>
            {
                //When struck in combat has a 1% chance of inflicting 75 to 125 Shadow damage to the attacker.
                throw new NotImplementedException();
            };
            // 27707 - Gladiator's Satin Gloves
            item = GetItem(27707, "Gladiator's Satin Gloves");
            item.OnEquip = (sim) =>
            {
                //Increases the duration of your Psychic Scream spell by 1 sec.
                throw new NotImplementedException();
            };
            // 19991 - Devilsaur Eye
            item = GetItem(19991, "Devilsaur Eye");
            item.OnUse = (sim) =>
            {
                //Use: Increases your attack power by 150 and your hit rating by 20. &nbsp;Effect lasts for 20 sec. (2 Min Cooldown)
                throw new NotImplementedException();
            };
            // 25857 - Gladiator's Silk Handguards
            item = GetItem(25857, "Gladiator's Silk Handguards");
            item.OnEquip = (sim) =>
            {
                //Improves the range of your Fire Blast spell by 5 yards.
                throw new NotImplementedException();
            };
            // 24386 - Libram of Saints Departed
            item = GetItem(24386, "Libram of Saints Departed");
            item.OnEquip = (sim) =>
            {
                //Causes your Judgements to heal you for 41 to 49.
                throw new NotImplementedException();
            };
            // 19992 - Devilsaur Tooth
            item = GetItem(19992, "Devilsaur Tooth");
            item.OnUse = (sim) =>
            {
                //Use: Your pet's next attack is guaranteed to critically strike if that attack is capable of striking critically. (2 Min Cooldown)
                throw new NotImplementedException();
            };
            // 16496 - Blood Guard's Dragonhide Gauntlets
            item = GetItem(16496, "Blood Guard's Dragonhide Gauntlets");
            item.OnEquip = (sim) =>
            {
                //Slightly increases your stealth detection.
                throw new NotImplementedException();
            };
            // 20130 - Diamond Flask
            item = GetItem(20130, "Diamond Flask");
            item.OnUse = (sim) =>
            {
                //Use: Restores 9 health every 5 sec and increases your Strength by 75. &nbsp;Lasts 1 min. (6 Min Cooldown)
                throw new NotImplementedException();
            };
            // 28592 - Libram of Souls Redeemed
            item = GetItem(28592, "Libram of Souls Redeemed");
            item.OnEquip = (sim) =>
            {
                //Increases the benefit your Flash of Light spell receives from Blessing of Light by 60 and Holy Light spell receives from Blessing of Light by 120.
                throw new NotImplementedException();
            };
            // 22863 - Blood Guard's Dragonhide Grips
            item = GetItem(22863, "Blood Guard's Dragonhide Grips");
            item.OnEquip = (sim) =>
            {
                //Slightly increases your stealth detection.
                throw new NotImplementedException();
            };
            // 11808 - Circle of Flame
            item = GetItem(11808, "Circle of Flame");
            item.OnUse = (sim) =>
            {
                //Use: Channels 75 health into mana every 1 sec for 10 sec. (5 Min Cooldown)
                throw new NotImplementedException();
            };
            // 30542 - Dimensional Ripper - Area 52
            item = GetItem(30542, "Dimensional Ripper - Area 52");
            item.OnUse = (sim) =>
            {
                //Use: Rips the dimensional walls asunder and transports you to Area 52 in Netherstorm. &nbsp; There are technical problems that sometimes occur, but that's what Goblin Engineering is all about! (4 Hrs Cooldown)
                throw new NotImplementedException();
            };
            // 16571 - General's Chain Gloves
            item = GetItem(16571, "General's Chain Gloves");
            item.OnEquip = (sim) =>
            {
                //Increases the damage done by your Multi-Shot by 5%.
                throw new NotImplementedException();
            };
            // 6972 - Fire Hardened Hauberk
            item = GetItem(6972, "Fire Hardened Hauberk");
            item.OnUse = (sim) =>
            {
                //Use: Increase Rage by 30. (1 Hour Cooldown)
                throw new NotImplementedException();
            };
            // 24123 - Circlet of Arcane Might
            item = GetItem(24123, "Circlet of Arcane Might");
            item.OnEquip = (sim) =>
            {
                //2% chance on successful spellcast to increase your spell damage by up to 120 for 15 sec.
                throw new NotImplementedException();
            };
            // 18984 - Dimensional Ripper - Everlook
            item = GetItem(18984, "Dimensional Ripper - Everlook");
            item.OnUse = (sim) =>
            {
                //Use: Rips the dimensional walls asunder and transports you to Everlook in Winterspring. &nbsp; There are technical problems that sometimes occur, but that's what Goblin Engineering is all about! (4 Hrs Cooldown)
                throw new NotImplementedException();
            };
            // 27917 - Libram of the Eternal Rest
            item = GetItem(27917, "Libram of the Eternal Rest");
            item.OnEquip = (sim) =>
            {
                //Increases the damage of your Consecration spell by up to 47.
                throw new NotImplementedException();
            };
            // 35182 - Hyper-Magnified Moon Specs
            item = GetItem(35182, "Hyper-Magnified Moon Specs");
            item.OnEquip = (sim) =>
            {
                //Shows the location of all nearby gas clouds on the minimap.
                //Slightly increases your stealth detection.
                throw new NotImplementedException();
            };
            item.OnUse = (sim) =>
            {
                //Use: Allows you to look far into the distance. (3 Sec Cooldown)
                throw new NotImplementedException();
            };
            // 38288 - Direbrew Hops
            item = GetItem(38288, "Direbrew Hops");
            item.OnUse = (sim) =>
            {
                //Use: Increases healing done by spells by up to 297 and damage done by spells by up to 99 for 20 sec. (2 Min Cooldown)
                throw new NotImplementedException();
            };
            // 23763 - Hyper-Vision Goggles
            item = GetItem(23763, "Hyper-Vision Goggles");
            item.OnUse = (sim) =>
            {
                //Use: Increases your stealth detection for 15 sec. (2 Min Cooldown)
                throw new NotImplementedException();
            };
            // 28296 - Libram of the Lightbringer
            item = GetItem(28296, "Libram of the Lightbringer");
            item.OnEquip = (sim) =>
            {
                //Increases healing done by Holy Light by up to 87.
                throw new NotImplementedException();
            };
            // 22268 - Draconic Infused Emblem
            item = GetItem(22268, "Draconic Infused Emblem");
            item.OnUse = (sim) =>
            {
                //Use: Increases your spell damage by up to 100 and your healing by up to 190 for 15 sec. (1 Min, 15 Sec Cooldown)
                throw new NotImplementedException();
            };
            // 28335 - Gladiator's Chain Gauntlets
            item = GetItem(28335, "Gladiator's Chain Gauntlets");
            item.OnEquip = (sim) =>
            {
                //Increases the damage done by your Multi-Shot by 5%.
                throw new NotImplementedException();
            };
            // 28830 - Dragonspine Trophy
            item = GetItem(28830, "Dragonspine Trophy");
            item.OnEquip = (sim) =>
            {
                //Your melee and ranged attacks have a chance to increase your haste rating by 325 for 10 sec.
                throw new NotImplementedException();
            };
            // 26000 - Gladiator's Linked Gauntlets
            item = GetItem(26000, "Gladiator's Linked Gauntlets");
            item.OnEquip = (sim) =>
            {
                //Increases the damage done by your Lightning Shield by 8%.
                throw new NotImplementedException();
            };
            // 22400 - Libram of Truth
            item = GetItem(22400, "Libram of Truth");
            item.OnEquip = (sim) =>
            {
                //Increases the armor from your Devotion Aura by 110.
                throw new NotImplementedException();
            };
            // 27470 - Gladiator's Mail Gauntlets
            item = GetItem(27470, "Gladiator's Mail Gauntlets");
            item.OnEquip = (sim) =>
            {
                //Improves the range of all Shock spells by 5 yards.
                throw new NotImplementedException();
            };
            // 16907 - Bloodfang Gloves
            item = GetItem(16907, "Bloodfang Gloves");
            item.OnEquip = (sim) =>
            {
                //Disarm duration reduced by 50%.
                throw new NotImplementedException();
            };
            // 31397 - Gladiator's Ringmail Gauntlets
            item = GetItem(31397, "Gladiator's Ringmail Gauntlets");
            item.OnEquip = (sim) =>
            {
                //Increases the critical effect chance of your Lesser Healing Wave by 2%.
                throw new NotImplementedException();
            };
            // 30665 - Earring of Soulful Meditation
            item = GetItem(30665, "Earring of Soulful Meditation");
            item.OnUse = (sim) =>
            {
                //Use: Increases your Spirit by +300 for 20 sec. (2 Min Cooldown)
                throw new NotImplementedException();
            };
            // 28065 - Libram of Wracking
            item = GetItem(28065, "Libram of Wracking");
            item.OnEquip = (sim) =>
            {
                //Increases the damage done by your Exorcism and Holy Wrath spells by up to 120.
                throw new NotImplementedException();
            };
            // 20525 - Earthen Sigil
            item = GetItem(20525, "Earthen Sigil");
            item.OnUse = (sim) =>
            {
                //Use: Restores 400 mana over 10 sec. (3 Min Cooldown)
                throw new NotImplementedException();
            };
            // 9394 - Horned Viking Helmet
            item = GetItem(9394, "Horned Viking Helmet");
            item.OnUse = (sim) =>
            {
                //Use: Charge an enemy, knocking it silly for 30 seconds. Also knocks you down, stunning you for a short period of time. Any damage caused will revive the target. &nbsp;Chance to fizzle when used against targets over level 60. (30 Min Cooldown)
                throw new NotImplementedException();
            };
            // 21180 - Earthstrike
            item = GetItem(21180, "Earthstrike");
            item.OnUse = (sim) =>
            {
                //Use: Increases your melee and ranged attack power by 280. &nbsp;Effect lasts for 20 sec. (2 Min Cooldown)
                throw new NotImplementedException();
            };
            // 27983 - Libram of Zeal
            item = GetItem(27983, "Libram of Zeal");
            item.OnEquip = (sim) =>
            {
                //Increases the melee attack power bonus of your Seal of the Crusader by 68 and the Holy damage increase of your Judgement of the Crusader by 47.
                throw new NotImplementedException();
            };
            // 25996 - Emblem of Perseverance
            item = GetItem(25996, "Emblem of Perseverance");
            item.OnUse = (sim) =>
            {
                //Use: Increases defense rating by 80 for 15 sec. (2 Min Cooldown)
                throw new NotImplementedException();
            };
            // 4264 - Barbaric Belt
            item = GetItem(4264, "Barbaric Belt");
            item.OnUse = (sim) =>
            {
                //Use: Increase Rage by 30. (1 Hour Cooldown)
                throw new NotImplementedException();
            };
            // 38287 - Empty Mug of Direbrew
            item = GetItem(38287, "Empty Mug of Direbrew");
            item.OnUse = (sim) =>
            {
                //Use: Increases attack power by 278 for 20 sec. (2 Min Cooldown)
                throw new NotImplementedException();
            };
            // 33937 - Merciless Gladiator's Libram of Fortitude
            item = GetItem(33937, "Merciless Gladiator's Libram of Fortitude");
            item.OnEquip = (sim) =>
            {
                //Your Judgement ability also grants you 31 resilience rating for 6 sec.
                throw new NotImplementedException();
            };
            // 20503 - Enamored Water Spirit
            item = GetItem(20503, "Enamored Water Spirit");
            item.OnUse = (sim) =>
            {
                //Use: Summons a Mana Spring Totem with 5 health at the feet of the caster for 24 sec that restores 27 mana every 2 seconds to group members within 20 yards. (3 Min Cooldown)
                throw new NotImplementedException();
            };
            // 28109 - Essence Infused Mushroom
            item = GetItem(28109, "Essence Infused Mushroom");
            item.OnEquip = (sim) =>
            {
                //Restores 200 health when you kill a target that gives experience or honor. This effect cannot occur more than once every 10 seconds.
                throw new NotImplementedException();
            };
            // 33077 - Merciless Gladiator's Libram of Justice
            item = GetItem(33077, "Merciless Gladiator's Libram of Justice");
            item.OnEquip = (sim) =>
            {
                //Causes your Flash of Light to increase the target's Resilience rating by 31 for 6 sec.
                throw new NotImplementedException();
            };
            // 28614 - Grand Marshal's Chain Gauntlets
            item = GetItem(28614, "Grand Marshal's Chain Gauntlets");
            item.OnEquip = (sim) =>
            {
                //Increases the damage done by your Multi-Shot by 5%.
                throw new NotImplementedException();
            };
            // 17066 - Drillborer Disk
            item = GetItem(17066, "Drillborer Disk");
            item.OnEquip = (sim) =>
            {
                //When struck in combat inflicts 3 Arcane damage to the attacker.
                throw new NotImplementedException();
            };
            // 24122 - Coronet of Verdant Flame
            item = GetItem(24122, "Coronet of Verdant Flame");
            item.OnEquip = (sim) =>
            {
                //Chance on successful spellcast to restore 90 Mana over 10 sec.
                throw new NotImplementedException();
            };
            // 29376 - Essence of the Martyr
            item = GetItem(29376, "Essence of the Martyr");
            item.OnUse = (sim) =>
            {
                //Use: Increases healing done by spells by up to 297 and damage done by spells by up to 99 for 20 sec. (2 Min Cooldown)
                throw new NotImplementedException();
            };
            // 28690 - Grand Marshal's Linked Gauntlets
            item = GetItem(28690, "Grand Marshal's Linked Gauntlets");
            item.OnEquip = (sim) =>
            {
                //Increases the damage done by your Lightning Shield by 8%.
                throw new NotImplementedException();
            };
            // 32480 - Magnified Moon Specs
            item = GetItem(32480, "Magnified Moon Specs");
            item.OnEquip = (sim) =>
            {
                //Shows the location of all nearby gas clouds on the minimap.
                //Slightly increases your stealth detection.
                throw new NotImplementedException();
            };
            item.OnUse = (sim) =>
            {
                //Use: Allows you to look far into the distance. (3 Sec Cooldown)
                throw new NotImplementedException();
            };
            // 18815 - Essence of the Pure Flame
            item = GetItem(18815, "Essence of the Pure Flame");
            item.OnEquip = (sim) =>
            {
                //When struck in combat inflicts 13 Fire damage to the attacker.
                throw new NotImplementedException();
            };
            // 33949 - Merciless Gladiator's Libram of Vengeance
            item = GetItem(33949, "Merciless Gladiator's Libram of Vengeance");
            item.OnEquip = (sim) =>
            {
                //Your Holy Shield ability also grants you 31 resilience rating for 6 sec.
                throw new NotImplementedException();
            };
            // 28695 - Grand Marshal's Mail Gauntlets
            item = GetItem(28695, "Grand Marshal's Mail Gauntlets");
            item.OnEquip = (sim) =>
            {
                //Improves the range of all Shock spells by 5 yards.
                throw new NotImplementedException();
            };
            // 23001 - Eye of Diminution
            item = GetItem(23001, "Eye of Diminution");
            item.OnUse = (sim) =>
            {
                //Use: Reduces the threat you generate by [35 - max(0, Level - 60)]% for 20 sec. (2 Min Cooldown)
                throw new NotImplementedException();
            };
            // 31641 - Grand Marshal's Ringmail Gloves
            item = GetItem(31641, "Grand Marshal's Ringmail Gloves");
            item.OnEquip = (sim) =>
            {
                //Improves the range of all Shock spells by 5 yards.
                throw new NotImplementedException();
            };
            // 24117 - Embrace of the Dawn
            item = GetItem(24117, "Embrace of the Dawn");
            item.OnUse = (sim) =>
            {
                //Use: All stats of nearby party members increased by 10 for 30 min. (1 Hour Cooldown)
                throw new NotImplementedException();
            };
            // 28823 - Eye of Gruul
            item = GetItem(28823, "Eye of Gruul");
            item.OnEquip = (sim) =>
            {
                //Each healing spell you cast has a 2% chance to make your next heal cast within 15 sec cost 450 less mana.
                throw new NotImplementedException();
            };
            // 32368 - Tome of the Lightbringer
            item = GetItem(32368, "Tome of the Lightbringer");
            item.OnEquip = (sim) =>
            {
                //Your Judgement ability also increases your shield block value by 186 for 5 sec.
                throw new NotImplementedException();
            };
            // 28789 - Eye of Magtheridon
            item = GetItem(28789, "Eye of Magtheridon");
            item.OnEquip = (sim) =>
            {
                //Grants 170 increased spell damage for 10 sec when one of your spells is resisted.
                throw new NotImplementedException();
            };
            // 21473 - Eye of Moam
            item = GetItem(21473, "Eye of Moam");
            item.OnUse = (sim) =>
            {
                //Use: Increases damage done by magical spells and effects by up to 50, and decreases the magical resistances of your spell targets by 100 for 30 sec. (3 Min Cooldown)
                throw new NotImplementedException();
            };
            // 33938 - Vengeful Gladiator's Libram of Fortitude
            item = GetItem(33938, "Vengeful Gladiator's Libram of Fortitude");
            item.OnEquip = (sim) =>
            {
                //Your Judgement ability also grants you 34 resilience rating for 6 sec.
                throw new NotImplementedException();
            };
            // 33842 - Vengeful Gladiator's Libram of Justice
            item = GetItem(33842, "Vengeful Gladiator's Libram of Justice");
            item.OnEquip = (sim) =>
            {
                //Causes your Flash of Light to increase the target's Resilience rating by 34 for 6 sec.
                throw new NotImplementedException();
            };
            // 4696 - Lapidis Tankard of Tidesippe
            item = GetItem(4696, "Lapidis Tankard of Tidesippe");
            item.OnUse = (sim) =>
            {
                //Use: Restores 1992 mana over 30 sec. &nbsp;Must remain seated while drinking. (10 Min Cooldown)
                throw new NotImplementedException();
            };
            // 23047 - Eye of the Dead
            item = GetItem(23047, "Eye of the Dead");
            item.OnUse = (sim) =>
            {
                //Use: Increases healing done by the next 5 spells by up to 450 and damage done by up to 150 for 30 sec. (2 Min Cooldown)
                throw new NotImplementedException();
            };
            // 23084 - Gloves of Undead Cleansing
            item = GetItem(23084, "Gloves of Undead Cleansing");
            item.OnEquip = (sim) =>
            {
                //Increases damage done to Undead by magical spells and effects by up to 35.
                throw new NotImplementedException();
            };
            // 30663 - Fathom-Brooch of the Tidewalker
            item = GetItem(30663, "Fathom-Brooch of the Tidewalker");
            item.OnEquip = (sim) =>
            {
                //Your Nature spells have a chance to restore 335 mana.
                throw new NotImplementedException();
            };
            // 32472 - Justicebringer 2000 Specs
            item = GetItem(32472, "Justicebringer 2000 Specs");
            item.OnEquip = (sim) =>
            {
                //Shows the location of all nearby gas clouds on the minimap.
                //Slightly increases your stealth detection.
                throw new NotImplementedException();
            };
            item.OnUse = (sim) =>
            {
                //Use: Allows you to look far into the distance. (3 Sec Cooldown)
                throw new NotImplementedException();
            };
            // 34998 - Brutal Gladiator's Dragonhide Gloves
            item = GetItem(34998, "Brutal Gladiator's Dragonhide Gloves");
            item.OnEquip = (sim) =>
            {
                //Causes your Maim ability to interrupt spellcasting and prevent any spell in that school from being cast for 3 sec.
                throw new NotImplementedException();
            };
            // 33950 - Vengeful Gladiator's Libram of Vengeance
            item = GetItem(33950, "Vengeful Gladiator's Libram of Vengeance");
            item.OnEquip = (sim) =>
            {
                //Your Holy Shield ability also grants you 34 resilience rating for 6 sec.
                throw new NotImplementedException();
            };
            // 30619 - Fel Reaver's Piston
            item = GetItem(30619, "Fel Reaver's Piston");
            item.OnEquip = (sim) =>
            {
                //Your direct healing spells have a chance to place a heal over time on your target, healing 500 over 12 sec.
                throw new NotImplementedException();
            };
            // 35185 - Justicebringer 3000 Specs
            item = GetItem(35185, "Justicebringer 3000 Specs");
            item.OnEquip = (sim) =>
            {
                //Shows the location of all nearby gas clouds on the minimap.
                //Slightly increases your stealth detection.
                throw new NotImplementedException();
            };
            item.OnUse = (sim) =>
            {
                //Use: Allows you to look far into the distance. (3 Sec Cooldown)
                throw new NotImplementedException();
            };
            // 7508 - Ley Orb
            item = GetItem(7508, "Ley Orb");
            item.OnUse = (sim) =>
            {
                //Use: Restores 140 to 180 mana. (15 Min Cooldown)
                throw new NotImplementedException();
            };
            // 21488 - Fetish of Chitinous Spikes
            item = GetItem(21488, "Fetish of Chitinous Spikes");
            item.OnUse = (sim) =>
            {
                //Use: Spikes sprout from you causing 25 Nature damage to attackers when hit. &nbsp;Lasts 30 sec. (3 Min Cooldown)
                throw new NotImplementedException();
            };
            // 35022 - Brutal Gladiator's Kodohide Gloves
            item = GetItem(35022, "Brutal Gladiator's Kodohide Gloves");
            item.OnEquip = (sim) =>
            {
                //Reduces the cast time of your Cyclone spell by 0.1 sec.
                throw new NotImplementedException();
            };
            // 35104 - Brutal Gladiator's Totem of Indomitability
            item = GetItem(35104, "Brutal Gladiator's Totem of Indomitability");
            item.OnEquip = (sim) =>
            {
                //Your Stormstrike ability also grants you 39 resilience rating for 6 sec.
                throw new NotImplementedException();
            };
            // 24116 - Eye of the Night
            item = GetItem(24116, "Eye of the Night");
            item.OnUse = (sim) =>
            {
                //Use: Increases spell damage by up to 34 for all nearby party members. &nbsp;Lasts 30 min. (1 Hour Cooldown)
                throw new NotImplementedException();
            };
            // 27416 - Fetish of the Fallen
            item = GetItem(27416, "Fetish of the Fallen");
            item.OnUse = (sim) =>
            {
                //Use: The next opponent killed within 15 sec that yields experience or honor will restore 900 health. (2 Min Cooldown)
                throw new NotImplementedException();
            };
            // 35032 - Brutal Gladiator's Leather Gloves
            item = GetItem(35032, "Brutal Gladiator's Leather Gloves");
            item.OnEquip = (sim) =>
            {
                //Causes your Deadly Throw ability to interrupt spellcasting and prevent any spell in that school from being cast for 3 sec.
                throw new NotImplementedException();
            };
            // 28624 - Grand Marshal's Dreadweave Gloves
            item = GetItem(28624, "Grand Marshal's Dreadweave Gloves");
            item.OnEquip = (sim) =>
            {
                //Gives you a 50% chance to avoid interruption caused by damage while casting Fear.
                throw new NotImplementedException();
            };
            // 35105 - Brutal Gladiator's Totem of Survival
            item = GetItem(35105, "Brutal Gladiator's Totem of Survival");
            item.OnEquip = (sim) =>
            {
                //Your Earth Shock, Flame Shock, and Frost Shock abilities also grant you 39 resilience rating for 6 sec.
                throw new NotImplementedException();
            };
            // 21647 - Fetish of the Sand Reaver
            item = GetItem(21647, "Fetish of the Sand Reaver");
            item.OnUse = (sim) =>
            {
                //Use: Reduces the threat you generate by [70 - 2 * max(0, Level - 60)]% for 20 sec. (3 Min Cooldown)
                throw new NotImplementedException();
            };
            // 31620 - Grand Marshal's Mooncloth Mitts
            item = GetItem(31620, "Grand Marshal's Mooncloth Mitts");
            item.OnEquip = (sim) =>
            {
                //Increases the duration of your Psychic Scream spell by 1 sec.
                throw new NotImplementedException();
            };
            // 23091 - Bracers of Undead Cleansing
            item = GetItem(23091, "Bracers of Undead Cleansing");
            item.OnEquip = (sim) =>
            {
                //Increases damage done to Undead by magical spells and effects by up to 26.
                throw new NotImplementedException();
            };
            // 35111 - Brutal Gladiator's Wyrmhide Gloves
            item = GetItem(35111, "Brutal Gladiator's Wyrmhide Gloves");
            item.OnEquip = (sim) =>
            {
                //Reduces the cast time of your Cyclone spell by 0.1 sec.
                throw new NotImplementedException();
            };
            // 21784 - Figurine - Black Diamond Crab
            item = GetItem(21784, "Figurine - Black Diamond Crab");
            item.OnUse = (sim) =>
            {
                //Use: Reduces melee damage taken by 35 for 20 sec. (5 Min Cooldown)
                throw new NotImplementedException();
            };
            // 28704 - Grand Marshal's Satin Gloves
            item = GetItem(28704, "Grand Marshal's Satin Gloves");
            item.OnEquip = (sim) =>
            {
                //Increases the duration of your Psychic Scream spell by 1 sec.
                throw new NotImplementedException();
            };
            // 35106 - Brutal Gladiator's Totem of the Third Wind
            item = GetItem(35106, "Brutal Gladiator's Totem of the Third Wind");
            item.OnEquip = (sim) =>
            {
                //Causes your Lesser Healing Wave to increase the target's Resilience rating by 39 for 6 sec.
                throw new NotImplementedException();
            };
            // 28716 - Grand Marshal's Silk Handguards
            item = GetItem(28716, "Grand Marshal's Silk Handguards");
            item.OnEquip = (sim) =>
            {
                //Improves the range of your Fire Blast spell by 5 yards.
                throw new NotImplementedException();
            };
            // 21758 - Figurine - Black Pearl Panther
            item = GetItem(21758, "Figurine - Black Pearl Panther");
            item.OnEquip = (sim) =>
            {
                //Increases your stealth slightly.
                throw new NotImplementedException();
            };
            item.OnUse = (sim) =>
            {
                //Use: Increases attack power by 90 for 15 sec. (5 Min Cooldown)
                throw new NotImplementedException();
            };
            // 28806 - High Warlord's Chain Gauntlets
            item = GetItem(28806, "High Warlord's Chain Gauntlets");
            item.OnEquip = (sim) =>
            {
                //Increases the damage done by your Multi-Shot by 5%.
                throw new NotImplementedException();
            };
            // 16392 - Knight-Lieutenant's Leather Boots
            item = GetItem(16392, "Knight-Lieutenant's Leather Boots");
            item.OnEquip = (sim) =>
            {
                //Increases the duration of your Sprint ability by 3 sec.
                throw new NotImplementedException();
            };
            // 35700 - Figurine - Crimson Serpent
            item = GetItem(35700, "Figurine - Crimson Serpent");
            item.OnUse = (sim) =>
            {
                //Use: Increases damage and healing done by magical spells and effects by up to 150 for 20 sec. (2 Min Cooldown)
                throw new NotImplementedException();
            };
            // 28842 - High Warlord's Linked Gauntlets
            item = GetItem(28842, "High Warlord's Linked Gauntlets");
            item.OnEquip = (sim) =>
            {
                //Increases the damage done by your Lightning Shield by 8%.
                throw new NotImplementedException();
            };
            // 23285 - Knight-Lieutenant's Leather Walkers
            item = GetItem(23285, "Knight-Lieutenant's Leather Walkers");
            item.OnEquip = (sim) =>
            {
                //Increases the duration of your Sprint ability by 3 sec.
                throw new NotImplementedException();
            };
            // 186071 - Communal Totem of Lightning
            item = GetItem(186071, "Communal Totem of Lightning");
            item.OnEquip = (sim) =>
            {
                //Increases damage done by Chain Lightning and Lightning Bolt by up to 10.
                throw new NotImplementedException();
            };
            // 28847 - High Warlord's Mail Gauntlets
            item = GetItem(28847, "High Warlord's Mail Gauntlets");
            item.OnEquip = (sim) =>
            {
                //Improves the range of all Shock spells by 5 yards.
                throw new NotImplementedException();
            };
            // 21789 - Figurine - Dark Iron Scorpid
            item = GetItem(21789, "Figurine - Dark Iron Scorpid");
            item.OnUse = (sim) =>
            {
                //Use: Every swing poisons your foe for 15 damage every second for 10 sec. (5 Min Cooldown)
                throw new NotImplementedException();
            };
            // 23825 - Nigh Invulnerability Belt
            item = GetItem(23825, "Nigh Invulnerability Belt");
            item.OnUse = (sim) =>
            {
                //Use: Protects you with a shield of force that stops 4000 damage for 8 sec. (5 Min Cooldown)
                throw new NotImplementedException();
            };
            // 31647 - High Warlord's Ringmail Gloves
            item = GetItem(31647, "High Warlord's Ringmail Gloves");
            item.OnEquip = (sim) =>
            {
                //Improves the range of all Shock spells by 5 yards.
                throw new NotImplementedException();
            };
            // 24125 - Figurine - Dawnstone Crab
            item = GetItem(24125, "Figurine - Dawnstone Crab");
            item.OnUse = (sim) =>
            {
                //Use: Increases dodge rating by 125 for 20 sec. (2 Min Cooldown)
                throw new NotImplementedException();
            };
            // 186072 - Communal Totem of Restoration
            item = GetItem(186072, "Communal Totem of Restoration");
            item.OnEquip = (sim) =>
            {
                //Increases healing done by Lesser Healing Wave by up to 10.
                throw new NotImplementedException();
            };
            // 21777 - Figurine - Emerald Owl
            item = GetItem(21777, "Figurine - Emerald Owl");
            item.OnUse = (sim) =>
            {
                //Use: Restores 60 mana every second for 12 sec. (5 Min Cooldown)
                throw new NotImplementedException();
            };
            // 35693 - Figurine - Empyrean Tortoise
            item = GetItem(35693, "Figurine - Empyrean Tortoise");
            item.OnUse = (sim) =>
            {
                //Use: Increases dodge rating by 165 for 20 sec. (2 Min Cooldown)
                throw new NotImplementedException();
            };
            // 186073 - Communal Totem of the Storm
            item = GetItem(186073, "Communal Totem of the Storm");
            item.OnEquip = (sim) =>
            {
                //Reduces the mana cost of Stormstrike by 5.
                throw new NotImplementedException();
            };
            // 24124 - Figurine - Felsteel Boar
            item = GetItem(24124, "Figurine - Felsteel Boar");
            item.OnUse = (sim) =>
            {
                //Use: Summons the Felsteel Boar to fight for you for 30 seconds. (5 Min Cooldown)
                throw new NotImplementedException();
            };
            // 31328 - Leggings of Beast Mastery
            item = GetItem(31328, "Leggings of Beast Mastery");
            item.OnEquip = (sim) =>
            {
                //Increases your pet's attack power by 70, armor by 490 and Stamina by 52.
                throw new NotImplementedException();
            };
            // 21756 - Figurine - Golden Hare
            item = GetItem(21756, "Figurine - Golden Hare");
            item.OnUse = (sim) =>
            {
                //Use: Increased speed by 30% and prevents new snares from landing on the user for 6 sec. (20 Min Cooldown)
                throw new NotImplementedException();
            };
            // 16446 - Marshal's Leather Footguards
            item = GetItem(16446, "Marshal's Leather Footguards");
            item.OnEquip = (sim) =>
            {
                //Increases the duration of your Sprint ability by 3 sec.
                throw new NotImplementedException();
            };
            // 7936 - Ornate Mithril Boots
            item = GetItem(7936, "Ornate Mithril Boots");
            item.OnUse = (sim) =>
            {
                //Use: Removes existing Immobilizing effects and makes you immune to Immobilizing effects for 5 sec. (30 Min Cooldown)
                throw new NotImplementedException();
            };
            // 33939 - Gladiator's Totem of Indomitability
            item = GetItem(33939, "Gladiator's Totem of Indomitability");
            item.OnEquip = (sim) =>
            {
                //Your Stormstrike ability also grants you 26 resilience rating for 6 sec.
                throw new NotImplementedException();
            };
            // 21748 - Figurine - Jade Owl
            item = GetItem(21748, "Figurine - Jade Owl");
            item.OnUse = (sim) =>
            {
                //Use: Restores 30 mana every second for 12 sec. (5 Min Cooldown)
                throw new NotImplementedException();
            };
            // 34354 - Mayhem Projection Goggles
            item = GetItem(34354, "Mayhem Projection Goggles");
            item.OnEquip = (sim) =>
            {
                //Shows the location of all nearby gas clouds on the minimap.
                //Slightly increases your stealth detection.
                throw new NotImplementedException();
            };
            item.OnUse = (sim) =>
            {
                //Use: Allows you to look far into the distance. (3 Sec Cooldown)
                throw new NotImplementedException();
            };
            // 35694 - Figurine - Khorium Boar
            item = GetItem(35694, "Figurine - Khorium Boar");
            item.OnUse = (sim) =>
            {
                //Use: Summons the Khorium Boar to fight for you for 30 sec. (5 Min Cooldown)
                throw new NotImplementedException();
            };
            // 7297 - Morbent's Bane
            item = GetItem(7297, "Morbent's Bane");
            item.OnUse = (sim) =>
            {
                //Use: Removes the protective enchantments around Morbent Fel. (3 Min Cooldown)
                throw new NotImplementedException();
            };
            // 33951 - Gladiator's Totem of Survival
            item = GetItem(33951, "Gladiator's Totem of Survival");
            item.OnEquip = (sim) =>
            {
                //Your Earth Shock, Flame Shock, and Frost Shock abilities also grant you 26 resilience rating for 6 sec.
                throw new NotImplementedException();
            };
            // 24126 - Figurine - Living Ruby Serpent
            item = GetItem(24126, "Figurine - Living Ruby Serpent");
            item.OnUse = (sim) =>
            {
                //Use: Increases damage and healing done by magical spells and effects by up to 150 for 20 sec. (5 Min Cooldown)
                throw new NotImplementedException();
            };
            // 22061 - Beastmaster's Boots
            item = GetItem(22061, "Beastmaster's Boots");
            item.OnEquip = (sim) =>
            {
                //Increases damage dealt by your pet by 3%.
                throw new NotImplementedException();
            };
            // 32113 - Chancellor's Dragonhide Gloves
            item = GetItem(32113, "Chancellor's Dragonhide Gloves");
            item.OnEquip = (sim) =>
            {
                //Reduces the cast time of your Cyclone spell by 0.1 sec.
                throw new NotImplementedException();
            };
            // 24128 - Figurine - Nightseye Panther
            item = GetItem(24128, "Figurine - Nightseye Panther");
            item.OnEquip = (sim) =>
            {
                //Increases your effective stealth level by 1.
                throw new NotImplementedException();
            };
            item.OnUse = (sim) =>
            {
                //Use: Increases attack power by 320 for 12 sec. (3 Min Cooldown)
                throw new NotImplementedException();
            };
            // 28357 - Gladiator's Totem of the Third Wind
            item = GetItem(28357, "Gladiator's Totem of the Third Wind");
            item.OnEquip = (sim) =>
            {
                //Causes your Lesser Healing Wave to increase the target's Resilience rating by 26 for 6 sec.
                throw new NotImplementedException();
            };
            // 21769 - Figurine - Ruby Serpent
            item = GetItem(21769, "Figurine - Ruby Serpent");
            item.OnUse = (sim) =>
            {
                //Use: Increases damage and healing done by magical spells and effects by up to 100 for 20 sec. (5 Min Cooldown)
                throw new NotImplementedException();
            };
            // 16403 - Knight-Lieutenant's Chain Gauntlets
            item = GetItem(16403, "Knight-Lieutenant's Chain Gauntlets");
            item.OnEquip = (sim) =>
            {
                //Reduces the mana cost of your Arcane Shot by 15.
                throw new NotImplementedException();
            };
            // 32118 - Chancellor's Kodohide Gloves
            item = GetItem(32118, "Chancellor's Kodohide Gloves");
            item.OnEquip = (sim) =>
            {
                //Reduces the cast time of your Cyclone spell by 0.1 sec.
                throw new NotImplementedException();
            };
            // 35703 - Figurine - Seaspray Albatross
            item = GetItem(35703, "Figurine - Seaspray Albatross");
            item.OnUse = (sim) =>
            {
                //Use: Restores 900 mana over 12 sec. (3 Min Cooldown)
                throw new NotImplementedException();
            };
            // 33940 - Merciless Gladiator's Totem of Indomitability
            item = GetItem(33940, "Merciless Gladiator's Totem of Indomitability");
            item.OnEquip = (sim) =>
            {
                //Your Stormstrike ability also grants you 31 resilience rating for 6 sec.
                throw new NotImplementedException();
            };
            // 23279 - Knight-Lieutenant's Chain Vices
            item = GetItem(23279, "Knight-Lieutenant's Chain Vices");
            item.OnEquip = (sim) =>
            {
                //Increases the damage done by your Multi-Shot by 5%.
                throw new NotImplementedException();
            };
            // 7375 - Green Whelp Armor
            item = GetItem(7375, "Green Whelp Armor");
            item.OnEquip = (sim) =>
            {
                //When struck by a melee attacker, that attacker has a 5% chance of being put to sleep for 10 sec. &nbsp;Only affects enemies level 50 and below.
                throw new NotImplementedException();
            };
            // 35702 - Figurine - Shadowsong Panther
            item = GetItem(35702, "Figurine - Shadowsong Panther");
            item.OnEquip = (sim) =>
            {
                //Increases your effective stealth level by 1.
                throw new NotImplementedException();
            };
            item.OnUse = (sim) =>
            {
                //Use: Increases attack power by 320 for 15 sec. (1 Min, 30 Sec Cooldown)
                throw new NotImplementedException();
            };
            // 32124 - Chancellor's Leather Gloves
            item = GetItem(32124, "Chancellor's Leather Gloves");
            item.OnEquip = (sim) =>
            {
                //Causes your Deadly Throw ability to interrupt spellcasting and prevent any spell in that school from being cast for 3 sec.
                throw new NotImplementedException();
            };
            // 18168 - Force Reactive Disk
            item = GetItem(18168, "Force Reactive Disk");
            item.OnEquip = (sim) =>
            {
                //When the shield blocks it releases an electrical charge that damages all nearby enemies. &nbsp; This also has a chance of damaging the shield.
                throw new NotImplementedException();
            };
            // 34353 - Quad Deathblow X44 Goggles
            item = GetItem(34353, "Quad Deathblow X44 Goggles");
            item.OnEquip = (sim) =>
            {
                //Shows the location of all nearby gas clouds on the minimap.
                //Slightly increases your stealth detection.
                throw new NotImplementedException();
            };
            item.OnUse = (sim) =>
            {
                //Use: Allows you to look far into the distance. (3 Sec Cooldown)
                throw new NotImplementedException();
            };
            // 24127 - Figurine - Talasite Owl
            item = GetItem(24127, "Figurine - Talasite Owl");
            item.OnUse = (sim) =>
            {
                //Use: Restores 900 mana over 12 sec. (5 Min Cooldown)
                throw new NotImplementedException();
            };
            // 12641 - Invulnerable Mail
            item = GetItem(12641, "Invulnerable Mail");
            item.OnEquip = (sim) =>
            {
                //When struck in combat has a 5% chance to make you invulnerable to melee damage for 3 sec. This effect can only occur once every 30 sec.
                throw new NotImplementedException();
            };
            // 33952 - Merciless Gladiator's Totem of Survival
            item = GetItem(33952, "Merciless Gladiator's Totem of Survival");
            item.OnEquip = (sim) =>
            {
                //Your Earth Shock, Flame Shock, and Frost Shock abilities also grant you 31 resilience rating for 6 sec.
                throw new NotImplementedException();
            };
            // 32128 - Chancellor's Wyrmhide Gloves
            item = GetItem(32128, "Chancellor's Wyrmhide Gloves");
            item.OnEquip = (sim) =>
            {
                //Reduces the cast time of your Cyclone spell by 0.1 sec.
                throw new NotImplementedException();
            };
            // 21763 - Figurine - Truesilver Boar
            item = GetItem(21763, "Figurine - Truesilver Boar");
            item.OnUse = (sim) =>
            {
                //Use: Summons the Truesilver Boar to fight for you for 30 seconds. (15 Min Cooldown)
                throw new NotImplementedException();
            };
            // 8197 - Nightscape Boots
            item = GetItem(8197, "Nightscape Boots");
            item.OnEquip = (sim) =>
            {
                //Increases your effective stealth level by 1.
                throw new NotImplementedException();
            };
            // 21760 - Figurine - Truesilver Crab
            item = GetItem(21760, "Figurine - Truesilver Crab");
            item.OnUse = (sim) =>
            {
                //Use: Reduces melee damage taken by 20 for 20 sec. (5 Min Cooldown)
                throw new NotImplementedException();
            };
            // 33078 - Merciless Gladiator's Totem of the Third Wind
            item = GetItem(33078, "Merciless Gladiator's Totem of the Third Wind");
            item.OnEquip = (sim) =>
            {
                //Causes your Lesser Healing Wave to increase the target's Resilience rating by 31 for 6 sec.
                throw new NotImplementedException();
            };
            // 28817 - High Warlord's Dreadweave Gloves
            item = GetItem(28817, "High Warlord's Dreadweave Gloves");
            item.OnEquip = (sim) =>
            {
                //Gives you a 50% chance to avoid interruption caused by damage while casting Fear.
                throw new NotImplementedException();
            };
            // 27529 - Figurine of the Colossus
            item = GetItem(27529, "Figurine of the Colossus");
            item.OnUse = (sim) =>
            {
                //Use: Each successful block heals you for 120. Effect lasts 20 sec. (2 Min Cooldown)
                throw new NotImplementedException();
            };
            // 31621 - High Warlord's Mooncloth Mitts
            item = GetItem(31621, "High Warlord's Mooncloth Mitts");
            item.OnEquip = (sim) =>
            {
                //Increases the duration of your Psychic Scream spell by 1 sec.
                throw new NotImplementedException();
            };
            // 10506 - Deepdive Helmet
            item = GetItem(10506, "Deepdive Helmet");
            item.OnEquip = (sim) =>
            {
                //Allows underwater breathing.
                throw new NotImplementedException();
            };
            // 31105 - Overlord's Helmet of Second Sight
            item = GetItem(31105, "Overlord's Helmet of Second Sight");
            item.OnEquip = (sim) =>
            {
                //Allows the bearer to see into the ghost world while in Shadowmoon Valley.
                throw new NotImplementedException();
            };
            // 28856 - High Warlord's Satin Gloves
            item = GetItem(28856, "High Warlord's Satin Gloves");
            item.OnEquip = (sim) =>
            {
                //Increases the duration of your Psychic Scream spell by 1 sec.
                throw new NotImplementedException();
            };
            // 20036 - Fire Ruby
            item = GetItem(20036, "Fire Ruby");
            item.OnUse = (sim) =>
            {
                //Use: Restores 1 to 500 mana and increases the damage of your next Fire spell by up to 100. &nbsp;Effect lasts for 1 min. (3 Min Cooldown)
                throw new NotImplementedException();
            };
            // 33506 - Skycall Totem
            item = GetItem(33506, "Skycall Totem");
            item.OnEquip = (sim) =>
            {
                //Your Lightning Bolt spell has a chance to grant 100 spell haste rating for 10 sec.
                throw new NotImplementedException();
            };
            // 28868 - High Warlord's Silk Handguards
            item = GetItem(28868, "High Warlord's Silk Handguards");
            item.OnEquip = (sim) =>
            {
                //Improves the range of your Fire Blast spell by 5 yards.
                throw new NotImplementedException();
            };
            // 11810 - Force of Will
            item = GetItem(11810, "Force of Will");
            item.OnEquip = (sim) =>
            {
                //When struck in combat has a 1% chance of reducing all melee damage taken by 25 for 10 sec.
                throw new NotImplementedException();
            };
            // 32494 - Destruction Holo-gogs
            item = GetItem(32494, "Destruction Holo-gogs");
            item.OnEquip = (sim) =>
            {
                //Shows the location of all nearby gas clouds on the minimap.
                //Slightly increases your stealth detection.
                throw new NotImplementedException();
            };
            item.OnUse = (sim) =>
            {
                //Use: Allows you to look far into the distance. (3 Sec Cooldown)
                throw new NotImplementedException();
            };
            // 17690 - Frostwolf Insignia Rank 1
            item = GetItem(17690, "Frostwolf Insignia Rank 1");
            item.OnUse = (sim) =>
            {
                //Use: Returns you to the sanctuary of Frostwolf Keep. (2 Min Cooldown)
                throw new NotImplementedException();
            };
            // 33507 - Stonebreaker's Totem
            item = GetItem(33507, "Stonebreaker's Totem");
            item.OnEquip = (sim) =>
            {
                //Your Shock spells have a chance to grant 110 attack power for 10 sec.
                throw new NotImplementedException();
            };
            // 17905 - Frostwolf Insignia Rank 2
            item = GetItem(17905, "Frostwolf Insignia Rank 2");
            item.OnUse = (sim) =>
            {
                //Use: Returns you to the sanctuary of Frostwolf Keep. (2 Min Cooldown)
                throw new NotImplementedException();
            };
            // 15108 - Orb of Dar'Orahil
            item = GetItem(15108, "Orb of Dar'Orahil");
            item.OnUse = (sim) =>
            {
                //Use: Restores 40 health every 3 sec for 30 sec. (30 Min Cooldown)
                throw new NotImplementedException();
            };
            // 22013 - Beastmaster's Cap
            item = GetItem(22013, "Beastmaster's Cap");
            item.OnEquip = (sim) =>
            {
                //Increases your pet's maximum health by 3%.
                throw new NotImplementedException();
            };
            // 17906 - Frostwolf Insignia Rank 3
            item = GetItem(17906, "Frostwolf Insignia Rank 3");
            item.OnUse = (sim) =>
            {
                //Use: Returns you to the sanctuary of Frostwolf Keep. (2 Min Cooldown)
                throw new NotImplementedException();
            };
            // 31031 - Stormfury Totem
            item = GetItem(31031, "Stormfury Totem");
            item.OnEquip = (sim) =>
            {
                //Reduces the mana cost of Stormstrike by 22.
                throw new NotImplementedException();
            };
            // 20831 - Heavy Golden Necklace of Battle
            item = GetItem(20831, "Heavy Golden Necklace of Battle");
            item.OnUse = (sim) =>
            {
                //Use: Gives 10 additional strength to nearby party members for 30 min.
                throw new NotImplementedException();
            };
            // 17907 - Frostwolf Insignia Rank 4
            item = GetItem(17907, "Frostwolf Insignia Rank 4");
            item.OnUse = (sim) =>
            {
                //Use: Returns you to the sanctuary of Frostwolf Keep. (2 Min Cooldown)
                throw new NotImplementedException();
            };
            // 16463 - Marshal's Chain Grips
            item = GetItem(16463, "Marshal's Chain Grips");
            item.OnEquip = (sim) =>
            {
                //Increases the damage done by your Multi-Shot by 5%.
                throw new NotImplementedException();
            };
            // 38506 - Don Carlos' Famous Hat
            item = GetItem(38506, "Don Carlos' Famous Hat");
            item.OnUse = (sim) =>
            {
                //Use: Right Click to summon an incorporeal coyote spirit to accompany you. The spirit will remain until sent away or you remove the hat.
                throw new NotImplementedException();
            };
            // 15107 - Orb of Noh'Orahil
            item = GetItem(15107, "Orb of Noh'Orahil");
            item.OnUse = (sim) =>
            {
                //Use: Restores 40 health every 3 sec for 30 sec. (30 Min Cooldown)
                throw new NotImplementedException();
            };
            // 17908 - Frostwolf Insignia Rank 5
            item = GetItem(17908, "Frostwolf Insignia Rank 5");
            item.OnUse = (sim) =>
            {
                //Use: Returns you to the sanctuary of Frostwolf Keep. (2 Min Cooldown)
                throw new NotImplementedException();
            };
            // 19588 - Hero's Brand
            item = GetItem(19588, "Hero's Brand");
            item.OnEquip = (sim) =>
            {
                //Increases the duration of Hammer of Justice by 0.5 sec.
                throw new NotImplementedException();
            };
            // 32330 - Totem of Ancestral Guidance
            item = GetItem(32330, "Totem of Ancestral Guidance");
            item.OnEquip = (sim) =>
            {
                //Increases damage done by Chain Lightning and Lightning Bolt by up to 85.
                throw new NotImplementedException();
            };
            // 17909 - Frostwolf Insignia Rank 6
            item = GetItem(17909, "Frostwolf Insignia Rank 6");
            item.OnUse = (sim) =>
            {
                //Use: Returns you to the sanctuary of Frostwolf Keep.
                throw new NotImplementedException();
            };
            // 6898 - Orb of Soran'ruk
            item = GetItem(6898, "Orb of Soran'ruk");
            item.OnUse = (sim) =>
            {
                //Use: Restores 25 health every 3 sec for 30 sec. (30 Min Cooldown)
                throw new NotImplementedException();
            };
            // 10418 - Glimmering Mithril Insignia
            item = GetItem(10418, "Glimmering Mithril Insignia");
            item.OnUse = (sim) =>
            {
                //Use: Increases armor by 50, all resistances by 10 and grants immunity to Fear for 30 sec. &nbsp;This device has a chance to be resisted when used by players over level 60. (20 Min Cooldown)
                throw new NotImplementedException();
            };
            // 23005 - Totem of Flowing Water
            item = GetItem(23005, "Totem of Flowing Water");
            item.OnEquip = (sim) =>
            {
                //Regain up to 10 mana each time you cast Lesser Healing Wave.
                throw new NotImplementedException();
            };
            // 34430 - Glimmering Naaru Sliver
            item = GetItem(34430, "Glimmering Naaru Sliver");
            item.OnUse = (sim) =>
            {
                //Use: Gain 250 mana each sec. for 8 sec. &nbsp;Channeled. (5 Min Cooldown)
                throw new NotImplementedException();
            };
            // 31961 - Merciless Gladiator's Chain Gauntlets
            item = GetItem(31961, "Merciless Gladiator's Chain Gauntlets");
            item.OnEquip = (sim) =>
            {
                //Increases the damage done by your Multi-Shot by 5%.
                throw new NotImplementedException();
            };
            // 25619 - Glowing Crystal Insignia
            item = GetItem(25619, "Glowing Crystal Insignia");
            item.OnUse = (sim) =>
            {
                //Use: Increases damage and healing done by magical spells and effects by up to 104 for 20 sec. (2 Min Cooldown)
                throw new NotImplementedException();
            };
            // 32005 - Merciless Gladiator's Linked Gauntlets
            item = GetItem(32005, "Merciless Gladiator's Linked Gauntlets");
            item.OnEquip = (sim) =>
            {
                //Increases the damage done by your Lightning Shield by 8%.
                throw new NotImplementedException();
            };
            // 22857 - Blood Guard's Mail Greaves
            item = GetItem(22857, "Blood Guard's Mail Greaves");
            item.OnEquip = (sim) =>
            {
                //Increases the speed of your Ghost Wolf ability by 15%. &nbsp;Does not function for players higher than level 60.
                throw new NotImplementedException();
            };
            // 18825 - Grand Marshal's Aegis
            item = GetItem(18825, "Grand Marshal's Aegis");
            item.OnEquip = (sim) =>
            {
                //When struck in combat has a 5% chance of inflicting 35 to 65 Nature damage to the attacker.
                throw new NotImplementedException();
            };
            // 28523 - Totem of Healing Rains
            item = GetItem(28523, "Totem of Healing Rains");
            item.OnEquip = (sim) =>
            {
                //Increases the base amount healed by Chain Heal by 87.
                throw new NotImplementedException();
            };
            // 32010 - Merciless Gladiator's Mail Gauntlets
            item = GetItem(32010, "Merciless Gladiator's Mail Gauntlets");
            item.OnEquip = (sim) =>
            {
                //Improves the range of all Shock spells by 5 yards.
                throw new NotImplementedException();
            };
            // 23040 - Glyph of Deflection
            item = GetItem(23040, "Glyph of Deflection");
            item.OnUse = (sim) =>
            {
                //Use: Increases the block value of your shield by 235 for 20 sec. (2 Min Cooldown)
                throw new NotImplementedException();
            };
            // 32030 - Merciless Gladiator's Ringmail Gauntlets
            item = GetItem(32030, "Merciless Gladiator's Ringmail Gauntlets");
            item.OnEquip = (sim) =>
            {
                //Increases the critical effect chance of your Lesser Healing Wave by 2%.
                throw new NotImplementedException();
            };
            // 17564 - Knight-Lieutenant's Dreadweave Gloves
            item = GetItem(17564, "Knight-Lieutenant's Dreadweave Gloves");
            item.OnEquip = (sim) =>
            {
                //Gives you a 50% chance to avoid interruption caused by damage while casting Searing Pain.
                throw new NotImplementedException();
            };
            // 29387 - Gnomeregan Auto-Blocker 600
            item = GetItem(29387, "Gnomeregan Auto-Blocker 600");
            item.OnUse = (sim) =>
            {
                //Use: Increases the block value of your shield by 200 for 20 sec. (2 Min Cooldown)
                throw new NotImplementedException();
            };
            // 27984 - Totem of Impact
            item = GetItem(27984, "Totem of Impact");
            item.OnEquip = (sim) =>
            {
                //Increases damage done by Earth Shock, Flame Shock, and Frost Shock by up to 46.
                throw new NotImplementedException();
            };
            // 16518 - Blood Guard's Mail Walkers
            item = GetItem(16518, "Blood Guard's Mail Walkers");
            item.OnEquip = (sim) =>
            {
                //Increases the speed of your Ghost Wolf ability by 15%. &nbsp;Does not function for players higher than level 60.
                throw new NotImplementedException();
            };
            // 23282 - Knight-Lieutenant's Dreadweave Handwraps
            item = GetItem(23282, "Knight-Lieutenant's Dreadweave Handwraps");
            item.OnEquip = (sim) =>
            {
                //Gives you a 50% chance to avoid interruption caused by damage while casting Searing Pain.
                throw new NotImplementedException();
            };
            // 17596 - Knight-Lieutenant's Satin Gloves
            item = GetItem(17596, "Knight-Lieutenant's Satin Gloves");
            item.OnEquip = (sim) =>
            {
                //Gives you a 50% chance to avoid interruption caused by damage while casting Mind Blast.
                throw new NotImplementedException();
            };
            // 10725 - Gnomish Battle Chicken
            item = GetItem(10725, "Gnomish Battle Chicken");
            item.OnUse = (sim) =>
            {
                //Use: Creates a Battle Chicken that will fight for you for 1.50 min or until it is destroyed. (20 Min Cooldown)
                throw new NotImplementedException();
            };
            // 23288 - Knight-Lieutenant's Satin Handwraps
            item = GetItem(23288, "Knight-Lieutenant's Satin Handwraps");
            item.OnEquip = (sim) =>
            {
                //Gives you a 50% chance to avoid interruption caused by damage while casting Mind Blast.
                throw new NotImplementedException();
            };
            // 20966 - Jade Pendant of Blasting
            item = GetItem(20966, "Jade Pendant of Blasting");
            item.OnUse = (sim) =>
            {
                //Use: Increases spell damage done by nearby party members by up to 15 for 30 min.
                throw new NotImplementedException();
            };
            // 4248 - Dark Leather Gloves
            item = GetItem(4248, "Dark Leather Gloves");
            item.OnEquip = (sim) =>
            {
                //Increases your lockpicking skill slightly.
                throw new NotImplementedException();
            };
            // 4397 - Gnomish Cloaking Device
            item = GetItem(4397, "Gnomish Cloaking Device");
            item.OnUse = (sim) =>
            {
                //Use: Gives invisibility for 10 sec. &nbsp;It can only be used every 60 minutes. (1 Hour Cooldown)
                throw new NotImplementedException();
            };
            // 22396 - Totem of Life
            item = GetItem(22396, "Totem of Life");
            item.OnEquip = (sim) =>
            {
                //Increases healing done by Lesser Healing Wave by up to 80.
                throw new NotImplementedException();
            };
            // 16391 - Knight-Lieutenant's Silk Gloves
            item = GetItem(16391, "Knight-Lieutenant's Silk Gloves");
            item.OnEquip = (sim) =>
            {
                //Increases the damage absorbed by your Mana Shield by 285.
                throw new NotImplementedException();
            };
            // 10645 - Gnomish Death Ray
            item = GetItem(10645, "Gnomish Death Ray");
            item.OnUse = (sim) =>
            {
                //Use: The device charges over time using your life force and then directs a burst of energy at your opponent. (5 Min Cooldown)
                throw new NotImplementedException();
            };
            // 23290 - Knight-Lieutenant's Silk Handwraps
            item = GetItem(23290, "Knight-Lieutenant's Silk Handwraps");
            item.OnEquip = (sim) =>
            {
                //Increases the damage absorbed by your Mana Shield by 285.
                throw new NotImplementedException();
            };
            // 28066 - Totem of Lightning
            item = GetItem(28066, "Totem of Lightning");
            item.OnEquip = (sim) =>
            {
                //Reduces the mana cost of your Lightning Bolt spells by 15.
                throw new NotImplementedException();
            };
            // 10720 - Gnomish Net-o-Matic Projector
            item = GetItem(10720, "Gnomish Net-o-Matic Projector");
            item.OnUse = (sim) =>
            {
                //Use: Captures the target in a net for 20 sec. &nbsp;The net has a lot of hooks however and sometimes gets caught in the user's clothing when fired...... (10 Min Cooldown)
                throw new NotImplementedException();
            };
            // 31109 - Stealther's Helmet of Second Sight
            item = GetItem(31109, "Stealther's Helmet of Second Sight");
            item.OnEquip = (sim) =>
            {
                //Allows the bearer to see into the ghost world while in Shadowmoon Valley.
                throw new NotImplementedException();
            };
            // 19601 - Jewel of Kajaro
            item = GetItem(19601, "Jewel of Kajaro");
            item.OnEquip = (sim) =>
            {
                //Reduces the cooldown of Counterspell by 2 sec.
                throw new NotImplementedException();
            };
            // 4328 - Spider Belt
            item = GetItem(4328, "Spider Belt");
            item.OnUse = (sim) =>
            {
                //Use: Removes existing Immobilizing effects and makes you immune to Immobilizing effects for 5 sec. (30 Min Cooldown)
                throw new NotImplementedException();
            };
            // 33505 - Totem of Living Water
            item = GetItem(33505, "Totem of Living Water");
            item.OnEquip = (sim) =>
            {
                //Reduces the base mana cost of Chain Heal by 20.
                throw new NotImplementedException();
            };
            // 23835 - Gnomish Poultryizer
            item = GetItem(23835, "Gnomish Poultryizer");
            item.OnUse = (sim) =>
            {
                //Use: Turns the target into a chicken for 15 sec. &nbsp; Well, that is assuming the transmogrification polarity has not been reversed... (5 Min Cooldown)
                throw new NotImplementedException();
            };
            // 10716 - Gnomish Shrink Ray
            item = GetItem(10716, "Gnomish Shrink Ray");
            item.OnUse = (sim) =>
            {
                //Use: Shrinks the target reducing their attack power by 250. &nbsp;That's what it usually does anyway..... (5 Min Cooldown)
                throw new NotImplementedException();
            };
            // 22395 - Totem of Rage
            item = GetItem(22395, "Totem of Rage");
            item.OnEquip = (sim) =>
            {
                //Increases damage done by Earth Shock, Flame Shock, and Frost Shock by up to 30.
                throw new NotImplementedException();
            };
            // 7506 - Gnomish Universal Remote
            item = GetItem(7506, "Gnomish Universal Remote");
            item.OnUse = (sim) =>
            {
                //Use: Allows control of a mechanical target for a short time. &nbsp;It may not always work and may just root the machine or make it very very angry. &nbsp;Gnomish engineering at its finest. (3 Min Cooldown)
                throw new NotImplementedException();
            };
            // 23824 - Rocket Boots Xtreme
            item = GetItem(23824, "Rocket Boots Xtreme");
            item.OnUse = (sim) =>
            {
                //Use: Engage the rocket boots to greatly increase your speed... most of the time. (5 Min Cooldown)
                throw new NotImplementedException();
            };
            // 32473 - Tankatronic Goggles
            item = GetItem(32473, "Tankatronic Goggles");
            item.OnEquip = (sim) =>
            {
                //Shows the location of all nearby gas clouds on the minimap.
                //Slightly increases your stealth detection.
                throw new NotImplementedException();
            };
            item.OnUse = (sim) =>
            {
                //Use: Allows you to look far into the distance. (3 Sec Cooldown)
                throw new NotImplementedException();
            };
            // 10587 - Goblin Bomb Dispenser
            item = GetItem(10587, "Goblin Bomb Dispenser");
            item.OnUse = (sim) =>
            {
                //Use: Creates a mobile bomb that charges the nearest enemy and explodes for 315 to 385 fire damage. (30 Min Cooldown)
                throw new NotImplementedException();
            };
            // 22345 - Totem of Rebirth
            item = GetItem(22345, "Totem of Rebirth");
            item.OnEquip = (sim) =>
            {
                //Reduces the cooldown of Reincarnation by 10 minutes.
                throw new NotImplementedException();
            };
            // 10727 - Goblin Dragon Gun
            item = GetItem(10727, "Goblin Dragon Gun");
            item.OnUse = (sim) =>
            {
                //Use: Deals 61 to 69 fire damage for 10 sec to all targets in a cone in front of the engineer using the weapon. &nbsp; That is unless it explodes..... (5 Min Cooldown)
                throw new NotImplementedException();
            };
            // 19605 - Kezan's Unstoppable Taint
            item = GetItem(19605, "Kezan's Unstoppable Taint");
            item.OnEquip = (sim) =>
            {
                //Increases the radius of Rain of Fire and Hellfire by 1 yard.
                throw new NotImplementedException();
            };
            // 9492 - Electromagnetic Gigaflux Reactivator
            item = GetItem(9492, "Electromagnetic Gigaflux Reactivator");
            item.OnUse = (sim) =>
            {
                //Use: Channels a bolt of lightning and hurls it towards all enemies in front of the caster causing 152 to 172 Nature damage. The caster is then surrounded by a barrier of electricity for 10 min. (30 Min Cooldown)
                throw new NotImplementedException();
            };
            // 27544 - Totem of Spontaneous Regrowth
            item = GetItem(27544, "Totem of Spontaneous Regrowth");
            item.OnEquip = (sim) =>
            {
                //Increases healing done by Healing Wave by up to 88.
                throw new NotImplementedException();
            };
            // 10577 - Goblin Mortar
            item = GetItem(10577, "Goblin Mortar");
            item.OnUse = (sim) =>
            {
                //Use: Inflicts 383 to 517 Fire damage and stuns the targets in a 5 yard radius for 3 sec. (10 Min Cooldown)
                throw new NotImplementedException();
            };
            // 30892 - Beast-tamer's Shoulders
            item = GetItem(30892, "Beast-tamer's Shoulders");
            item.OnEquip = (sim) =>
            {
                //Increases damage dealt by your pet by 3%.
                //Increases your pet's critical strike chance by 2%.
                throw new NotImplementedException();
            };
            // 23200 - Totem of Sustaining
            item = GetItem(23200, "Totem of Sustaining");
            item.OnEquip = (sim) =>
            {
                //Increases healing done by Lesser Healing Wave by up to 53.
                throw new NotImplementedException();
            };
            // 17584 - Marshal's Dreadweave Gloves
            item = GetItem(17584, "Marshal's Dreadweave Gloves");
            item.OnEquip = (sim) =>
            {
                //Gives you a 50% chance to avoid interruption caused by damage while casting Searing Pain.
                throw new NotImplementedException();
            };
            // 31333 - The Night Watchman
            item = GetItem(31333, "The Night Watchman");
            item.OnEquip = (sim) =>
            {
                //Slightly increases your stealth detection.
                throw new NotImplementedException();
            };
            // 35485 - Goblin Rocket Launcher [PH]
            item = GetItem(35485, "Goblin Rocket Launcher [PH]");
            item.OnUse = (sim) =>
            {
                //Use: Fire a powerful rocket at the enemy that does 960 to 1440 damage and stuns them for 3 sec. &nbsp; &nbsp;This thing has quite a kick though... (2 Min Cooldown)
                throw new NotImplementedException();
            };
            // 17608 - Marshal's Satin Gloves
            item = GetItem(17608, "Marshal's Satin Gloves");
            item.OnEquip = (sim) =>
            {
                //Gives you a 50% chance to avoid interruption caused by damage while casting Mind Blast.
                throw new NotImplementedException();
            };
            // 1315 - Lei of Lilies
            item = GetItem(1315, "Lei of Lilies");
            item.OnUse = (sim) =>
            {
                //Use: Conjures a Lily Root that restores health and mana when eaten. (1 Hour Cooldown)
                throw new NotImplementedException();
            };
            // 23836 - Goblin Rocket Launcher
            item = GetItem(23836, "Goblin Rocket Launcher");
            item.OnUse = (sim) =>
            {
                //Use: Fire a powerful rocket at the enemy that does 960 to 1440 damage and stuns them for 3 sec. &nbsp; &nbsp;This thing has quite a kick though... (2 Min Cooldown)
                throw new NotImplementedException();
            };
            // 16440 - Marshal's Silk Gloves
            item = GetItem(16440, "Marshal's Silk Gloves");
            item.OnEquip = (sim) =>
            {
                //Increases the damage absorbed by your Mana Shield by 285.
                throw new NotImplementedException();
            };
            // 27815 - Totem of the Astral Winds
            item = GetItem(27815, "Totem of the Astral Winds");
            item.OnEquip = (sim) =>
            {
                //Increases the attack power bonus on Windfury Weapon attacks by 80.
                throw new NotImplementedException();
            };
            // 21181 - Grace of Earth
            item = GetItem(21181, "Grace of Earth");
            item.OnUse = (sim) =>
            {
                //Use: Reduces your threat to enemy targets within 30 yards, making them less likely to attack you. (5 Min Cooldown)
                throw new NotImplementedException();
            };
            // 33808 - The Horseman's Helm
            item = GetItem(33808, "The Horseman's Helm");
            item.OnUse = (sim) =>
            {
                //Use: Let the Horseman laugh through you. (30 Sec Cooldown)
                throw new NotImplementedException();
            };
            // 31973 - Merciless Gladiator's Dreadweave Gloves
            item = GetItem(31973, "Merciless Gladiator's Dreadweave Gloves");
            item.OnEquip = (sim) =>
            {
                //Gives you a 50% chance to avoid interruption caused by damage while casting Fear.
                throw new NotImplementedException();
            };
            // 19951 - Gri'lek's Charm of Might
            item = GetItem(19951, "Gri'lek's Charm of Might");
            item.OnUse = (sim) =>
            {
                //Use: Instantly increases your rage by [(300 - 10 * max(0, Level - 60)) / 10]. (3 Min Cooldown)
                throw new NotImplementedException();
            };
            // 24110 - Living Ruby Pendant
            item = GetItem(24110, "Living Ruby Pendant");
            item.OnUse = (sim) =>
            {
                //Use: Restores 6 health per second to nearby party members for 30 min. (1 Hour Cooldown)
                throw new NotImplementedException();
            };
            // 30023 - Totem of the Maelstrom
            item = GetItem(30023, "Totem of the Maelstrom");
            item.OnEquip = (sim) =>
            {
                //Reduces the mana cost of Healing Wave by 24.
                throw new NotImplementedException();
            };
            // 31981 - Merciless Gladiator's Felweave Handguards
            item = GetItem(31981, "Merciless Gladiator's Felweave Handguards");
            item.OnEquip = (sim) =>
            {
                //Gives you a 50% chance to avoid interruption caused by damage while casting Fear.
                throw new NotImplementedException();
            };
            // 19952 - Gri'lek's Charm of Valor
            item = GetItem(19952, "Gri'lek's Charm of Valor");
            item.OnUse = (sim) =>
            {
                //Use: Increases your spell critical strike rating by 140 for 15 sec. (3 Min Cooldown)
                throw new NotImplementedException();
            };
            // 32015 - Merciless Gladiator's Mooncloth Gloves
            item = GetItem(32015, "Merciless Gladiator's Mooncloth Gloves");
            item.OnEquip = (sim) =>
            {
                //Increases the duration of your Psychic Scream spell by 1 sec.
                throw new NotImplementedException();
            };
            // 18826 - High Warlord's Shield Wall
            item = GetItem(18826, "High Warlord's Shield Wall");
            item.OnEquip = (sim) =>
            {
                //When struck in combat has a 5% chance of inflicting 35 to 65 Nature damage to the attacker.
                throw new NotImplementedException();
            };
            // 35748 - Guardian's Alchemist Stone
            item = GetItem(35748, "Guardian's Alchemist Stone");
            item.OnEquip = (sim) =>
            {
                //Increases the effect that healing and mana potions have on the wearer by 40%. &nbsp;This effect does not stack.
                throw new NotImplementedException();
            };
            // 32034 - Merciless Gladiator's Satin Gloves
            item = GetItem(32034, "Merciless Gladiator's Satin Gloves");
            item.OnEquip = (sim) =>
            {
                //Increases the duration of your Psychic Scream spell by 1 sec.
                throw new NotImplementedException();
            };
            // 25645 - Totem of the Plains
            item = GetItem(25645, "Totem of the Plains");
            item.OnEquip = (sim) =>
            {
                //Increases healing done by Lesser Healing Wave by up to 79.
                throw new NotImplementedException();
            };
            // 32049 - Merciless Gladiator's Silk Handguards
            item = GetItem(32049, "Merciless Gladiator's Silk Handguards");
            item.OnEquip = (sim) =>
            {
                //Improves the range of your Fire Blast spell by 5 yards.
                throw new NotImplementedException();
            };
            // 1490 - Guardian Talisman
            item = GetItem(1490, "Guardian Talisman");
            item.OnEquip = (sim) =>
            {
                //Have a 2% chance when struck in combat of increasing armor by 350 for 15 sec.
                throw new NotImplementedException();
            };
            // 29389 - Totem of the Pulsing Earth
            item = GetItem(29389, "Totem of the Pulsing Earth");
            item.OnEquip = (sim) =>
            {
                //Reduces the mana cost of Lightning Bolt by 27.
                throw new NotImplementedException();
            };
            // 18634 - Gyrofreeze Ice Reflector
            item = GetItem(18634, "Gyrofreeze Ice Reflector");
            item.OnUse = (sim) =>
            {
                //Use: Reflects Frost spells back at their caster for 5 sec. &nbsp;Chance to be resisted when used by players over level 60. (5 Min Cooldown)
                throw new NotImplementedException();
            };
            // 27467 - Silent-Strider Kneeboots
            item = GetItem(27467, "Silent-Strider Kneeboots");
            item.OnEquip = (sim) =>
            {
                //Increases your effective stealth level by 1.
                throw new NotImplementedException();
            };
            // 19621 - Maelstrom's Wrath
            item = GetItem(19621, "Maelstrom's Wrath");
            item.OnEquip = (sim) =>
            {
                //Decreases the cooldown of Feign Death by 2 sec.
                throw new NotImplementedException();
            };
            // 23199 - Totem of the Storm
            item = GetItem(23199, "Totem of the Storm");
            item.OnEquip = (sim) =>
            {
                //Increases damage done by Chain Lightning and Lightning Bolt by up to 33.
                throw new NotImplementedException();
            };
            // 11815 - Hand of Justice
            item = GetItem(11815, "Hand of Justice");
            item.OnEquip = (sim) =>
            {
                //Chance on melee hit to gain 1 extra attack.
                throw new NotImplementedException();
            };
            // 19957 - Hazza'rah's Charm of Destruction
            item = GetItem(19957, "Hazza'rah's Charm of Destruction");
            item.OnUse = (sim) =>
            {
                //Use: Increases your spell critical strike rating by 140 for 20 sec. (3 Min Cooldown)
                throw new NotImplementedException();
            };
            // 35335 - Mooncloth Mitts
            item = GetItem(35335, "Mooncloth Mitts");
            item.OnEquip = (sim) =>
            {
                //Increases the duration of your Psychic Scream spell by 1 sec.
                throw new NotImplementedException();
            };
            // 24413 - Totem of the Thunderhead
            item = GetItem(24413, "Totem of the Thunderhead");
            item.OnEquip = (sim) =>
            {
                //Your Water Shield ability grants an additional 27 mana each time it triggers and an additional 2 mana per 5 sec.
                throw new NotImplementedException();
            };
            // 11522 - Silver Totem of Aquementas
            item = GetItem(11522, "Silver Totem of Aquementas");
            item.OnUse = (sim) =>
            {
                //Use: Dispels Blazerunner's Aura.
                throw new NotImplementedException();
            };
            // 31104 - Evoker's Helmet of Second Sight
            item = GetItem(31104, "Evoker's Helmet of Second Sight");
            item.OnEquip = (sim) =>
            {
                //Allows the bearer to see into the ghost world while in Shadowmoon Valley.
                throw new NotImplementedException();
            };
            // 22191 - Obsidian Mail Tunic
            item = GetItem(22191, "Obsidian Mail Tunic");
            item.OnEquip = (sim) =>
            {
                //Spell Damage received is reduced by 10.
                throw new NotImplementedException();
            };
            // 19958 - Hazza'rah's Charm of Healing
            item = GetItem(19958, "Hazza'rah's Charm of Healing");
            item.OnUse = (sim) =>
            {
                //Use: Grants 70 spell haste rating and reduces the mana cost of your healing spells by 5% for 15 sec. (3 Min Cooldown)
                throw new NotImplementedException();
            };
            // 35356 - Dragonhide Gloves
            item = GetItem(35356, "Dragonhide Gloves");
            item.OnEquip = (sim) =>
            {
                //Reduces the cast time of your Cyclone spell by 0.1 sec.
                throw new NotImplementedException();
            };
            // 19959 - Hazza'rah's Charm of Magic
            item = GetItem(19959, "Hazza'rah's Charm of Magic");
            item.OnUse = (sim) =>
            {
                //Use: Increases Arcane spell damage by 200 for 20 sec. (3 Min Cooldown)
                throw new NotImplementedException();
            };
            // 28248 - Totem of the Void
            item = GetItem(28248, "Totem of the Void");
            item.OnEquip = (sim) =>
            {
                //Increases damage done by Chain Lightning and Lightning Bolt by up to 55.
                throw new NotImplementedException();
            };
            // 17744 - Heart of Noxxion
            item = GetItem(17744, "Heart of Noxxion");
            item.OnUse = (sim) =>
            {
                //Use: Removes 1 poison effect. (5 Min Cooldown)
                throw new NotImplementedException();
            };
            // 13164 - Heart of the Scale
            item = GetItem(13164, "Heart of the Scale");
            item.OnUse = (sim) =>
            {
                //Use: Increases Fire Resistance by 20 and deals 20 Fire damage to anyone who strikes you with a melee attack for 5 min. (30 Min Cooldown)
                throw new NotImplementedException();
            };
            // 33941 - Vengeful Gladiator's Totem of Indomitability
            item = GetItem(33941, "Vengeful Gladiator's Totem of Indomitability");
            item.OnEquip = (sim) =>
            {
                //Your Stormstrike ability also grants you 34 resilience rating for 6 sec.
                throw new NotImplementedException();
            };
            // 4984 - Skull of Impending Doom
            item = GetItem(4984, "Skull of Impending Doom");
            item.OnUse = (sim) =>
            {
                //Use: Increase your run speed by 60% for 10 sec, but deals damage equal to 60% of your maximum health and drains 60% of your maximum mana over 10 seconds. (5 Min Cooldown)
                throw new NotImplementedException();
            };
            // 22321 - Heart of Wyrmthalak
            item = GetItem(22321, "Heart of Wyrmthalak");
            item.OnEquip = (sim) =>
            {
                //Chance to bathe your melee target in flames for 120 to 180 Fire damage.
                throw new NotImplementedException();
            };
            // 30293 - Heavenly Inspiration
            item = GetItem(30293, "Heavenly Inspiration");
            item.OnUse = (sim) =>
            {
                //Use: Increases healing done by up to 238 and damage done by up to 79 for all magical spells and effects for 15 sec. (1 Min, 30 Sec Cooldown)
                throw new NotImplementedException();
            };
            // 33953 - Vengeful Gladiator's Totem of Survival
            item = GetItem(33953, "Vengeful Gladiator's Totem of Survival");
            item.OnEquip = (sim) =>
            {
                //Your Earth Shock, Flame Shock, and Frost Shock abilities also grant you 34 resilience rating for 6 sec.
                throw new NotImplementedException();
            };
            // 33829 - Hex Shrunken Head
            item = GetItem(33829, "Hex Shrunken Head");
            item.OnUse = (sim) =>
            {
                //Use: Increases damage and healing done by magical spells and effects by up to 211 for 20 sec. (2 Min Cooldown)
                throw new NotImplementedException();
            };
            // 33843 - Vengeful Gladiator's Totem of the Third Wind
            item = GetItem(33843, "Vengeful Gladiator's Totem of the Third Wind");
            item.OnEquip = (sim) =>
            {
                //Causes your Lesser Healing Wave to increase the target's Resilience rating by 34 for 6 sec.
                throw new NotImplementedException();
            };
            // 20636 - Hibernation Crystal
            item = GetItem(20636, "Hibernation Crystal");
            item.OnUse = (sim) =>
            {
                //Use: Increases healing done by magical spells and effects by up to 350 and damage done by spells by up to 117 for 15 sec. (1 Min, 30 Sec Cooldown)
                throw new NotImplementedException();
            };
            // 22198 - Jagged Obsidian Shield
            item = GetItem(22198, "Jagged Obsidian Shield");
            item.OnEquip = (sim) =>
            {
                //When struck by a harmful spell, the caster of that spell has a 5% chance to be silenced for 3 sec.
                throw new NotImplementedException();
            };
            // 19979 - Hook of the Master Angler
            item = GetItem(19979, "Hook of the Master Angler");
            item.OnUse = (sim) =>
            {
                //Use: Turns you into a fish giving water breath and increasing your movement speed, but attacking or casting while in this form breaks the effect. &nbsp;Does not work out of water or at the water's surface. (5 Sec Cooldown)
                throw new NotImplementedException();
            };
            // 28578 - Masquerade Gown
            item = GetItem(28578, "Masquerade Gown");
            item.OnEquip = (sim) =>
            {
                //Chance on spell cast to increase your Spirit by 145 for 15 secs.
                throw new NotImplementedException();
            };
            // 28034 - Hourglass of the Unraveller
            item = GetItem(28034, "Hourglass of the Unraveller");
            item.OnEquip = (sim) =>
            {
                //Chance on critical hit to increase your attack power by 300 for 10 secs.
                throw new NotImplementedException();
            };
            // 20084 - Hunting Net
            item = GetItem(20084, "Hunting Net");
            item.OnUse = (sim) =>
            {
                //Use: Renders a target unable to move for 10 sec. (3 Min Cooldown)
                throw new NotImplementedException();
            };
            // 18638 - Hyper-Radiant Flame Reflector
            item = GetItem(18638, "Hyper-Radiant Flame Reflector");
            item.OnUse = (sim) =>
            {
                //Use: Reflects Fire spells back at their caster for 5 sec. &nbsp;Chance to be resisted when used by players over level 60. (5 Min Cooldown)
                throw new NotImplementedException();
            };
            // 29600 - Blood Guard's Lamellar Gauntlets
            item = GetItem(29600, "Blood Guard's Lamellar Gauntlets");
            item.OnEquip = (sim) =>
            {
                //Increases the Holy damage bonus of your Judgement of the Crusader by 20.
                throw new NotImplementedException();
            };
            // 25786 - Hypnotist's Watch
            item = GetItem(25786, "Hypnotist's Watch");
            item.OnUse = (sim) =>
            {
                //Use: Reduces your threat to enemy targets within 30 yards, making them less likely to attack you. (5 Min Cooldown)
                throw new NotImplementedException();
            };
            // 22784 - Sunwell Orb
            item = GetItem(22784, "Sunwell Orb");
            item.OnUse = (sim) =>
            {
                //Use: Use on Dar'Khan Drathir to release the energy contained in this item causing 500 Arcane damage over 5 sec and silencing the target. (10 Min Cooldown)
                throw new NotImplementedException();
            };
            // 29370 - Icon of the Silver Crescent
            item = GetItem(29370, "Icon of the Silver Crescent");
            item.OnUse = (sim) =>
            {
                //Use: Increases damage and healing done by magical spells and effects by up to 155 for 20 sec. (2 Min Cooldown)
                throw new NotImplementedException();
            };
            // 8345 - Wolfshead Helm
            item = GetItem(8345, "Wolfshead Helm");
            item.OnEquip = (sim) =>
            {
                //When shapeshifting into Cat form the Druid gains 20 energy, when shapeshifting into Bear form the Druid gains 5 rage.
                throw new NotImplementedException();
            };
            // 22868 - Blood Guard's Plate Gauntlets
            item = GetItem(22868, "Blood Guard's Plate Gauntlets");
            item.OnEquip = (sim) =>
            {
                //Hamstring Rage cost reduced by 3.
                throw new NotImplementedException();
            };
            // 28121 - Icon of Unyielding Courage
            item = GetItem(28121, "Icon of Unyielding Courage");
            item.OnUse = (sim) =>
            {
                //Use: Your attacks ignore 600 of your enemies' armor for 20 sec. (2 Min Cooldown)
                throw new NotImplementedException();
            };
            // 35382 - Seer's Linked Gauntlets
            item = GetItem(35382, "Seer's Linked Gauntlets");
            item.OnEquip = (sim) =>
            {
                //Improves the range of all Shock spells by 5 yards.
                throw new NotImplementedException();
            };
            // 32479 - Wonderheal XT40 Shades
            item = GetItem(32479, "Wonderheal XT40 Shades");
            item.OnEquip = (sim) =>
            {
                //Shows the location of all nearby gas clouds on the minimap.
                //Slightly increases your stealth detection.
                throw new NotImplementedException();
            };
            item.OnUse = (sim) =>
            {
                //Use: Allows you to look far into the distance. (3 Sec Cooldown)
                throw new NotImplementedException();
            };
            // 29877 - Indalamar's Trinket of Pwnage
            item = GetItem(29877, "Indalamar's Trinket of Pwnage");
            item.OnUse = (sim) =>
            {
                //Use: Sunders the target's armor, reducing it by 520 per Sunder Armor and causes a high amount of threat. &nbsp;Can be applied up to 5 times. &nbsp;Lasts until cancelled.
                throw new NotImplementedException();
            };
            // 23838 - Foreman's Enchanted Helmet
            item = GetItem(23838, "Foreman's Enchanted Helmet");
            item.OnEquip = (sim) =>
            {
                //+10% Stun Resistance.
                throw new NotImplementedException();
            };
            // 35183 - Wonderheal XT68 Shades
            item = GetItem(35183, "Wonderheal XT68 Shades");
            item.OnEquip = (sim) =>
            {
                //Shows the location of all nearby gas clouds on the minimap.
                //Slightly increases your stealth detection.
                throw new NotImplementedException();
            };
            item.OnUse = (sim) =>
            {
                //Use: Allows you to look far into the distance. (3 Sec Cooldown)
                throw new NotImplementedException();
            };
            // 35387 - Seer's Mail Gauntlets
            item = GetItem(35387, "Seer's Mail Gauntlets");
            item.OnEquip = (sim) =>
            {
                //Improves the range of all Shock spells by 5 yards.
                throw new NotImplementedException();
            };
            // 7391 - Swift Boots
            item = GetItem(7391, "Swift Boots");
            item.OnUse = (sim) =>
            {
                //Use: Increases run speed by 40% for 15 sec. (20 Min Cooldown)
                throw new NotImplementedException();
            };
            // 33046 - Insignia of PvP Pwn
            item = GetItem(33046, "Insignia of PvP Pwn");
            item.OnUse = (sim) =>
            {
                //Use: Removes all movement impairing effects and all effects which cause loss of control of your character. (2 Min Cooldown)
                throw new NotImplementedException();
            };
            // 16510 - Blood Guard's Plate Gloves
            item = GetItem(16510, "Blood Guard's Plate Gloves");
            item.OnEquip = (sim) =>
            {
                //Hamstring Rage cost reduced by 3.
                throw new NotImplementedException();
            };
            // 35473 - Seer's Ringmail Gloves
            item = GetItem(35473, "Seer's Ringmail Gloves");
            item.OnEquip = (sim) =>
            {
                //Improves the range of all Shock spells by 5 yards.
                throw new NotImplementedException();
            };
            // 10724 - Gnomish Rocket Boots
            item = GetItem(10724, "Gnomish Rocket Boots");
            item.OnUse = (sim) =>
            {
                //Use: These boots significantly increase your run speed for 20 sec. &nbsp;WARNING: &nbsp;Their power supply and gyros do not always function as intended. (15 Min Cooldown)
                throw new NotImplementedException();
            };
            // 18858 - Insignia of the Alliance
            item = GetItem(18858, "Insignia of the Alliance");
            item.OnUse = (sim) =>
            {
                //Use: Removes all movement impairing effects and all effects which cause loss of control of your character. (5 Min Cooldown)
                throw new NotImplementedException();
            };
            // 18849 - Insignia of the Horde
            item = GetItem(18849, "Insignia of the Horde");
            item.OnUse = (sim) =>
            {
                //Use: Removes all movement impairing effects and all effects which cause loss of control of your character. (5 Min Cooldown)
                throw new NotImplementedException();
            };
            // 7189 - Goblin Rocket Boots
            item = GetItem(7189, "Goblin Rocket Boots");
            item.OnUse = (sim) =>
            {
                //Use: These dangerous looking boots significantly increase your run speed for 20 sec. &nbsp;They are prone to explode however, so use with caution. (5 Min Cooldown)
                throw new NotImplementedException();
            };
            // 12588 - Bonespike Shoulder
            item = GetItem(12588, "Bonespike Shoulder");
            item.OnEquip = (sim) =>
            {
                //Deals 60 to 90 damage when you are the victim of a critical melee strike.
                throw new NotImplementedException();
            };
            // 27900 - Jewel of Charismatic Mystique
            item = GetItem(27900, "Jewel of Charismatic Mystique");
            item.OnUse = (sim) =>
            {
                //Use: Reduces your threat to enemy targets within 30 yards, making them less likely to attack you. (5 Min Cooldown)
                throw new NotImplementedException();
            };
            // 23570 - Jom Gabbar
            item = GetItem(23570, "Jom Gabbar");
            item.OnUse = (sim) =>
            {
                //Use: Increases attack power by 65 and an additional 65 every 2 sec. &nbsp;Lasts 20 sec. (2 Min Cooldown)
                throw new NotImplementedException();
            };
            // 22954 - Kiss of the Spider
            item = GetItem(22954, "Kiss of the Spider");
            item.OnUse = (sim) =>
            {
                //Use: Increases your haste rating by 200 for 15 sec. (2 Min Cooldown)
                throw new NotImplementedException();
            };
            // 19341 - Lifegiving Gem
            item = GetItem(19341, "Lifegiving Gem");
            item.OnUse = (sim) =>
            {
                //Use: Increases your maximum health by 1500 for 20 sec. &nbsp;After the effects wear off, you will lose the extra maximum health. (5 Min Cooldown)
                throw new NotImplementedException();
            };
            // 31288 - The Master's Treads
            item = GetItem(31288, "The Master's Treads");
            item.OnEquip = (sim) =>
            {
                //Increases your effective stealth level by 1.
                throw new NotImplementedException();
            };
            // 833 - Lifestone
            item = GetItem(833, "Lifestone");
            item.OnEquip = (sim) =>
            {
                //Restores 10 health every 5 sec.
                throw new NotImplementedException();
            };
            item.OnUse = (sim) =>
            {
                //Use: Restores 300 to 700 health. (30 Min Cooldown)
                throw new NotImplementedException();
            };
            // 11905 - Linken's Boomerang
            item = GetItem(11905, "Linken's Boomerang");
            item.OnUse = (sim) =>
            {
                //Use: Flings a magical boomerang towards target enemy dealing 113 to 187 damage and has a chance to stun or disarm them. (3 Min Cooldown)
                throw new NotImplementedException();
            };
            // 7348 - Fletcher's Gloves
            item = GetItem(7348, "Fletcher's Gloves");
            item.OnEquip = (sim) =>
            {
                //Decreases your chance to parry an attack by 1%.
                throw new NotImplementedException();
            };
            // 14134 - Cloak of Fire
            item = GetItem(14134, "Cloak of Fire");
            item.OnUse = (sim) =>
            {
                //Use: Deals 25 Fire damage every 5 sec to all nearby enemies for 15 sec. (2 Min Cooldown)
                throw new NotImplementedException();
            };
            // 30664 - Living Root of the Wildheart
            item = GetItem(30664, "Living Root of the Wildheart");
            item.OnEquip = (sim) =>
            {
                //Your spells and attacks in each form have a chance to grant you a blessing for 15 sec.
                throw new NotImplementedException();
            };
            // 23042 - Loatheb's Reflection
            item = GetItem(23042, "Loatheb's Reflection");
            item.OnUse = (sim) =>
            {
                //Use: Increases resistances to all schools of magic by 40 for 20 sec. (2 Min Cooldown)
                throw new NotImplementedException();
            };
            // 3475 - Cloak of Flames
            item = GetItem(3475, "Cloak of Flames");
            item.OnEquip = (sim) =>
            {
                //Deals 5 Fire damage to anyone who strikes you with a melee attack.
                throw new NotImplementedException();
            };
            // 30841 - Lower City Prayerbook
            item = GetItem(30841, "Lower City Prayerbook");
            item.OnUse = (sim) =>
            {
                //Use: Your heals each cost 22 less mana for the next 15 sec. (1 Min Cooldown)
                throw new NotImplementedException();
            };
            // 19141 - Luffa
            item = GetItem(19141, "Luffa");
            item.OnUse = (sim) =>
            {
                //Use: Removes one Bleed effect. &nbsp;Will not work on bleeds cast by enemies over level 60. (3 Min Cooldown)
                throw new NotImplementedException();
            };
            // 32505 - Madness of the Betrayer
            item = GetItem(32505, "Madness of the Betrayer");
            item.OnEquip = (sim) =>
            {
                //Your melee and ranged attacks have a chance allow you to ignore 300 of your enemies' armor for 10 secs.
                throw new NotImplementedException();
            };
            // 17741 - Nature's Embrace
            item = GetItem(17741, "Nature's Embrace");
            item.OnEquip = (sim) =>
            {
                //Increases damage done by Holy spells and effects by up to 29.
                throw new NotImplementedException();
            };
            // 18637 - Major Recombobulator
            item = GetItem(18637, "Major Recombobulator");
            item.OnUse = (sim) =>
            {
                //Use: Restores 375 to 625 health and mana to a friendly target and attempts to dispel any polymorph effects from them. &nbsp;Reduced effectiveness against polymorph effects from casters of level 61 and higher. (5 Min Cooldown)
                throw new NotImplementedException();
            };
            // 19930 - Mar'li's Eye
            item = GetItem(19930, "Mar'li's Eye");
            item.OnUse = (sim) =>
            {
                //Use: Restores 60 mana every 5 sec for 30 sec. (3 Min Cooldown)
                throw new NotImplementedException();
            };
            // 10726 - Gnomish Mind Control Cap
            item = GetItem(10726, "Gnomish Mind Control Cap");
            item.OnUse = (sim) =>
            {
                //Use: Engage in mental combat with a humanoid target to try and control their mind. &nbsp;If all works well, you will control the mind of the target for 20 sec ..... Increased chance of failure when used against targets over level 60. (30 Min Cooldown)
                throw new NotImplementedException();
            };
            // 35377 - Stalker's Chain Gauntlets
            item = GetItem(35377, "Stalker's Chain Gauntlets");
            item.OnEquip = (sim) =>
            {
                //Increases the damage done by your Multi-Shot by 5%.
                throw new NotImplementedException();
            };
            // 27921 - Mark of Conquest
            item = GetItem(27921, "Mark of Conquest");
            item.OnEquip = (sim) =>
            {
                //Sometimes heals bearer of 90 to 120 damage when damaging an enemy in melee and chance on ranged hit to restore 128 to 172 mana to the bearer.
                throw new NotImplementedException();
            };
            // 27924 - Mark of Defiance
            item = GetItem(27924, "Mark of Defiance");
            item.OnEquip = (sim) =>
            {
                //Chance on spell hit to restore 128 to 172 mana to the bearer.
                throw new NotImplementedException();
            };
            // 21517 - Gnomish Turban of Psychic Might
            item = GetItem(21517, "Gnomish Turban of Psychic Might");
            item.OnEquip = (sim) =>
            {
                //Reduces the duration of any Silence or Interrupt effects used against the wearer by 20%. This effect does not stack with other similar effects.
                throw new NotImplementedException();
            };
            // 8346 - Gauntlets of the Sea
            item = GetItem(8346, "Gauntlets of the Sea");
            item.OnUse = (sim) =>
            {
                //Use: Heal friendly target for 300 to 500. (30 Min Cooldown)
                throw new NotImplementedException();
            };
            // 35338 - Satin Gloves
            item = GetItem(35338, "Satin Gloves");
            item.OnEquip = (sim) =>
            {
                //Increases the duration of your Psychic Scream spell by 1 sec.
                throw new NotImplementedException();
            };
            // 17759 - Mark of Resolution
            item = GetItem(17759, "Mark of Resolution");
            item.OnEquip = (sim) =>
            {
                //Increases your chance to resist Stun and Fear effects by 1%.
                throw new NotImplementedException();
            };
            item.OnUse = (sim) =>
            {
                //Use: Absorbs 350 to 650 physical damage. &nbsp;Lasts 10 sec. (30 Min Cooldown)
                throw new NotImplementedException();
            };
            // 10543 - Goblin Construction Helmet
            item = GetItem(10543, "Goblin Construction Helmet");
            item.OnUse = (sim) =>
            {
                //Use: Absorbs 300 to 500 Fire damage. &nbsp;Lasts 1 min. (1 Hour Cooldown)
                throw new NotImplementedException();
            };
            // 16555 - General's Dragonhide Gloves
            item = GetItem(16555, "General's Dragonhide Gloves");
            item.OnEquip = (sim) =>
            {
                //Slightly increases your stealth detection.
                throw new NotImplementedException();
            };
            // 23207 - Mark of the Champion
            item = GetItem(23207, "Mark of the Champion");
            item.OnEquip = (sim) =>
            {
                //Increases damage done to Undead and Demons by magical spells and effects by up to 85. &nbsp;It also allows the acquisition of Scourgestones on behalf of the Argent Dawn.
                throw new NotImplementedException();
            };
            // 10588 - Goblin Rocket Helmet
            item = GetItem(10588, "Goblin Rocket Helmet");
            item.OnUse = (sim) =>
            {
                //Use: Charge an enemy, knocking it silly for 30 seconds. Also knocks you down, stunning you for a short period of time. Any damage caused will revive the target. &nbsp;Chance to fizzle when used against targets over level 60. (20 Min Cooldown)
                throw new NotImplementedException();
            };
            // 12632 - Storm Gauntlets
            item = GetItem(12632, "Storm Gauntlets");
            item.OnEquip = (sim) =>
            {
                //Adds 3 Lightning damage to your melee attacks.
                throw new NotImplementedException();
            };
            // 17774 - Mark of the Chosen
            item = GetItem(17774, "Mark of the Chosen");
            item.OnEquip = (sim) =>
            {
                //Has a 2% chance when struck in combat of increasing all stats by 25 for 1 min.
                throw new NotImplementedException();
            };
            // 27926 - Mark of Vindication
            item = GetItem(27926, "Mark of Vindication");
            item.OnEquip = (sim) =>
            {
                //Chance on spell hit to restore 128 to 172 mana to the bearer.
                throw new NotImplementedException();
            };
            // 4396 - Mechanical Dragonling
            item = GetItem(4396, "Mechanical Dragonling");
            item.OnUse = (sim) =>
            {
                //Use: Activates your Mechanical Dragonling to fight for you for 1 min. (20 Min Cooldown)
                throw new NotImplementedException();
            };
            // 35028 - Brutal Gladiator's Lamellar Gauntlets
            item = GetItem(35028, "Brutal Gladiator's Lamellar Gauntlets");
            item.OnEquip = (sim) =>
            {
                //Increases the critical effect chance of your Flash of Light by 2%.
                throw new NotImplementedException();
            };
            // 28126 - Gladiator's Dragonhide Gloves
            item = GetItem(28126, "Gladiator's Dragonhide Gloves");
            item.OnEquip = (sim) =>
            {
                //Reduces the cast time of your Cyclone spell by 0.1 sec.
                throw new NotImplementedException();
            };
            // 1131 - Totem of Infliction
            item = GetItem(1131, "Totem of Infliction");
            item.OnEquip = (sim) =>
            {
                //When struck in combat has a 1% chance of inflicting 75 to 125 Shadow damage to the attacker.
                throw new NotImplementedException();
            };
            // 28235 - Medallion of the Alliance
            item = GetItem(28235, "Medallion of the Alliance");
            item.OnUse = (sim) =>
            {
                //Use: Removes all movement impairing effects and all effects which cause loss of control of your character. (2 Min Cooldown)
                throw new NotImplementedException();
            };
            // 31375 - Gladiator's Kodohide Gloves
            item = GetItem(31375, "Gladiator's Kodohide Gloves");
            item.OnEquip = (sim) =>
            {
                //Reduces the cast time of your Cyclone spell by 0.1 sec.
                throw new NotImplementedException();
            };
            // 28242 - Medallion of the Horde
            item = GetItem(28242, "Medallion of the Horde");
            item.OnUse = (sim) =>
            {
                //Use: Removes all movement impairing effects and all effects which cause loss of control of your character. (2 Min Cooldown)
                throw new NotImplementedException();
            };
            // 35060 - Brutal Gladiator's Ornamented Gloves
            item = GetItem(35060, "Brutal Gladiator's Ornamented Gloves");
            item.OnEquip = (sim) =>
            {
                //Increases the critical effect chance of your Flash of Light by 2%.
                throw new NotImplementedException();
            };
            // 25834 - Gladiator's Leather Gloves
            item = GetItem(25834, "Gladiator's Leather Gloves");
            item.OnEquip = (sim) =>
            {
                //Causes your Deadly Throw ability to interrupt spellcasting and prevent any spell in that school from being cast for 3 sec.
                throw new NotImplementedException();
            };
            // 32496 - Memento of Tyrande
            item = GetItem(32496, "Memento of Tyrande");
            item.OnEquip = (sim) =>
            {
                //Each time you cast a spell, there is chance you will gain up to 76 mana per 5 for 15 sec.
                throw new NotImplementedException();
            };
            // 35067 - Brutal Gladiator's Plate Gauntlets
            item = GetItem(35067, "Brutal Gladiator's Plate Gauntlets");
            item.OnEquip = (sim) =>
            {
                //Hamstring Rage cost reduced by 3.
                throw new NotImplementedException();
            };
            // 19339 - Mind Quickening Gem
            item = GetItem(19339, "Mind Quickening Gem");
            item.OnUse = (sim) =>
            {
                //Use: Quickens the mind, increasing the Mage's spell haste rating by 330 for 20 sec. (5 Min Cooldown)
                throw new NotImplementedException();
            };
            // 28136 - Gladiator's Wyrmhide Gloves
            item = GetItem(28136, "Gladiator's Wyrmhide Gloves");
            item.OnEquip = (sim) =>
            {
                //Reduces the cast time of your Cyclone spell by 0.1 sec.
                throw new NotImplementedException();
            };
            // 35089 - Brutal Gladiator's Scaled Gauntlets
            item = GetItem(35089, "Brutal Gladiator's Scaled Gauntlets");
            item.OnEquip = (sim) =>
            {
                //Increases the damage dealt by your Crusader Strike ability by 5%.
                throw new NotImplementedException();
            };
            // 4381 - Minor Recombobulator
            item = GetItem(4381, "Minor Recombobulator");
            item.OnUse = (sim) =>
            {
                //Use: Restores 150 to 250 health and mana to a friendly target and attempts to dispel any polymorph effects from them. &nbsp;Reduced effectiveness against polymorph effects from casters of level 31 and higher. (5 Min Cooldown)
                throw new NotImplementedException();
            };
            // 24092 - Pendant of Frozen Flame
            item = GetItem(24092, "Pendant of Frozen Flame");
            item.OnUse = (sim) =>
            {
                //Use: Absorbs 900 to 2700 fire damage on all nearby party members. &nbsp;Lasts 5 min. (1 Hour Cooldown)
                throw new NotImplementedException();
            };
            // 32154 - Chancellor's Lamellar Gauntlets
            item = GetItem(32154, "Chancellor's Lamellar Gauntlets");
            item.OnEquip = (sim) =>
            {
                //Increases the Holy damage bonus of your Judgement of the Crusader by 20.
                throw new NotImplementedException();
            };
            // 10576 - Mithril Mechanical Dragonling
            item = GetItem(10576, "Mithril Mechanical Dragonling");
            item.OnUse = (sim) =>
            {
                //Use: Activates your Mithril Mechanical Dragonling to fight for you for 1 min. (20 Min Cooldown)
                throw new NotImplementedException();
            };
            // 24097 - Pendant of Shadow's End
            item = GetItem(24097, "Pendant of Shadow's End");
            item.OnUse = (sim) =>
            {
                //Use: Absorbs 900 to 2700 shadow damage on all nearby party members. &nbsp;Lasts 5 min. (1 Hour Cooldown)
                throw new NotImplementedException();
            };
            // 28528 - Moroes' Lucky Pocket Watch
            item = GetItem(28528, "Moroes' Lucky Pocket Watch");
            item.OnUse = (sim) =>
            {
                //Use: Increases dodge rating by 300 for 10 sec. (2 Min Cooldown)
                throw new NotImplementedException();
            };
            // 15866 - Veildust Medicine Bag
            item = GetItem(15866, "Veildust Medicine Bag");
            item.OnUse = (sim) =>
            {
                //Use: Restores 50 mana every 3 sec for 15 sec. (30 Min Cooldown)
                throw new NotImplementedException();
            };
            // 32159 - Chancellor's Ornamented Gloves
            item = GetItem(32159, "Chancellor's Ornamented Gloves");
            item.OnEquip = (sim) =>
            {
                //Increases the Holy damage bonus of your Judgement of the Crusader by 20.
                throw new NotImplementedException();
            };
            // 19947 - Nat Pagle's Broken Reel
            item = GetItem(19947, "Nat Pagle's Broken Reel");
            item.OnUse = (sim) =>
            {
                //Use: Increases spell hit rating by 80 for 15 sec. (1 Min, 15 Sec Cooldown)
                throw new NotImplementedException();
            };
            // 32164 - Chancellor's Plate Gauntlets
            item = GetItem(32164, "Chancellor's Plate Gauntlets");
            item.OnEquip = (sim) =>
            {
                //Hamstring Rage cost reduced by 3.
                throw new NotImplementedException();
            };
            // 24093 - Pendant of Thawing
            item = GetItem(24093, "Pendant of Thawing");
            item.OnUse = (sim) =>
            {
                //Use: Absorbs 900 to 2700 frost damage on all nearby party members. &nbsp;Lasts 5 min. (1 Hour Cooldown)
                throw new NotImplementedException();
            };
            // 19344 - Natural Alignment Crystal
            item = GetItem(19344, "Natural Alignment Crystal");
            item.OnUse = (sim) =>
            {
                //Use: Increases spell damage and healing by up to 250, and increases mana cost of spells by 20% for 20 sec. (5 Min Cooldown)
                throw new NotImplementedException();
            };
            // 20950 - Pendant of the Agate Shield
            item = GetItem(20950, "Pendant of the Agate Shield");
            item.OnUse = (sim) =>
            {
                //Use: Gives 10 additional stamina to nearby party members for 30 min.
                throw new NotImplementedException();
            };
            // 32169 - Chancellor's Scaled Gauntlets
            item = GetItem(32169, "Chancellor's Scaled Gauntlets");
            item.OnEquip = (sim) =>
            {
                //Increases the Holy damage bonus of your Judgement of the Crusader by 20.
                throw new NotImplementedException();
            };
            // 2820 - Nifty Stopwatch
            item = GetItem(2820, "Nifty Stopwatch");
            item.OnUse = (sim) =>
            {
                //Use: Increases run speed by 40% for 10 sec. (30 Min Cooldown)
                throw new NotImplementedException();
            };
            // 38579 - Venomous Tome
            item = GetItem(38579, "Venomous Tome");
            item.OnEquip = (sim) =>
            {
                //When struck in melee you have a chance of exhaling poison, inflicting 93 to 107 Nature damage on a nearby enemy.
                throw new NotImplementedException();
            };
            // 26055 - Oculus of the Hidden Eye
            item = GetItem(26055, "Oculus of the Hidden Eye");
            item.OnUse = (sim) =>
            {
                //Use: The next opponent you kill within 10 sec that yields experience or honor will restore 900 mana. (2 Min Cooldown)
                throw new NotImplementedException();
            };
            // 30008 - Pendant of the Lost Ages
            item = GetItem(30008, "Pendant of the Lost Ages");
            item.OnEquip = (sim) =>
            {
                //Reduces the duration of any Silence or Interrupt effects used against the wearer by 20%. This effect does not stack with other similar effects.
                throw new NotImplementedException();
            };
            // 25628 - Ogre Mauler's Badge
            item = GetItem(25628, "Ogre Mauler's Badge");
            item.OnUse = (sim) =>
            {
                //Use: Increases attack power by 185 for 15 sec. (2 Min Cooldown)
                throw new NotImplementedException();
            };
            // 24098 - Pendant of the Null Rune
            item = GetItem(24098, "Pendant of the Null Rune");
            item.OnUse = (sim) =>
            {
                //Use: Absorbs 900 to 2700 arcane damage on all nearby party members. &nbsp;Lasts 5 min. (1 Hour Cooldown)
                throw new NotImplementedException();
            };
            // 28484 - Bulwark of Kings
            item = GetItem(28484, "Bulwark of Kings");
            item.OnUse = (sim) =>
            {
                //Use: Temporarily Increases Health by 1500 and Strength by 150 for 15 sec. (30 Min Cooldown)
                throw new NotImplementedException();
            };
            // 4262 - Gem-studded Leather Belt
            item = GetItem(4262, "Gem-studded Leather Belt");
            item.OnUse = (sim) =>
            {
                //Use: Heal yourself for 225 to 375. (1 Hour Cooldown)
                throw new NotImplementedException();
            };
            // 1973 - Orb of Deception
            item = GetItem(1973, "Orb of Deception");
            item.OnUse = (sim) =>
            {
                //Use: Transforms caster to look like a member of the opposing faction. (30 Min Cooldown)
                throw new NotImplementedException();
            };
            // 33665 - Vengeful Gladiator's Chain Gauntlets
            item = GetItem(33665, "Vengeful Gladiator's Chain Gauntlets");
            item.OnEquip = (sim) =>
            {
                //Increases the damage done by your Multi-Shot by 5%.
                throw new NotImplementedException();
            };
            // 28485 - Bulwark of the Ancient Kings
            item = GetItem(28485, "Bulwark of the Ancient Kings");
            item.OnUse = (sim) =>
            {
                //Use: Temporarily Increases Health by 1500 and Strength by 150 for 15 sec. (30 Min Cooldown)
                throw new NotImplementedException();
            };
            // 12805 - Orb of Fire
            item = GetItem(12805, "Orb of Fire");
            item.OnEquip = (sim) =>
            {
                //Grants a chance on striking the enemy for 50 to 70 Fire damage.
                throw new NotImplementedException();
            };
            // 33707 - Vengeful Gladiator's Linked Gauntlets
            item = GetItem(33707, "Vengeful Gladiator's Linked Gauntlets");
            item.OnEquip = (sim) =>
            {
                //Improves the range of all Shock spells by 5 yards.
                throw new NotImplementedException();
            };
            // 20255 - Whisperwalk Boots
            item = GetItem(20255, "Whisperwalk Boots");
            item.OnEquip = (sim) =>
            {
                //Increases your effective stealth level by 1.
                throw new NotImplementedException();
            };
            // 25634 - Oshu'gun Relic
            item = GetItem(25634, "Oshu'gun Relic");
            item.OnUse = (sim) =>
            {
                //Use: Increases healing done by spells and effects by up to 213 and damage done by spells by up to 71 for 20 sec. (2 Min Cooldown)
                throw new NotImplementedException();
            };
            // 33712 - Vengeful Gladiator's Mail Gauntlets
            item = GetItem(33712, "Vengeful Gladiator's Mail Gauntlets");
            item.OnEquip = (sim) =>
            {
                //Improves the range of all Shock spells by 5 yards.
                throw new NotImplementedException();
            };
            // 32694 - Overseer's Badge
            item = GetItem(32694, "Overseer's Badge");
            item.OnUse = (sim) =>
            {
                //Use: Calls forth a Netherwing Ally to fight at your side in Shadowmoon Valley. (10 Min Cooldown)
                throw new NotImplementedException();
            };
            // 33739 - Vengeful Gladiator's Ringmail Gauntlets
            item = GetItem(33739, "Vengeful Gladiator's Ringmail Gauntlets");
            item.OnEquip = (sim) =>
            {
                //Improves the range of all Shock spells by 5 yards.
                throw new NotImplementedException();
            };
            // 28727 - Pendant of the Violet Eye
            item = GetItem(28727, "Pendant of the Violet Eye");
            item.OnUse = (sim) =>
            {
                //Use: Each spell cast within 20 seconds will grant a stacking bonus of 21 mana regen per 5 sec. Expires after 20 seconds. &nbsp;Abilities with no mana cost will not trigger this trinket. (2 Min Cooldown)
                throw new NotImplementedException();
            };
            // 24095 - Pendant of Withering
            item = GetItem(24095, "Pendant of Withering");
            item.OnUse = (sim) =>
            {
                //Use: Absorbs 900 to 2700 nature damage on all nearby party members. &nbsp;Lasts 5 min. (1 Hour Cooldown)
                throw new NotImplementedException();
            };
            // 23714 - Perpetual Purple Firework
            item = GetItem(23714, "Perpetual Purple Firework");
            item.OnUse = (sim) =>
            {
                //Use: Shoots a firework into the air that bursts into a thousand purple stars. (30 Sec Cooldown)
                throw new NotImplementedException();
            };
            // 35403 - Crusader's Ornamented Gloves
            item = GetItem(35403, "Crusader's Ornamented Gloves");
            item.OnEquip = (sim) =>
            {
                //Increases the Holy damage bonus of your Judgement of the Crusader by 20.
                throw new NotImplementedException();
            };
            // 21685 - Petrified Scarab
            item = GetItem(21685, "Petrified Scarab");
            item.OnUse = (sim) =>
            {
                //Use: Increases your spell resistances by 100 for 1 min. Every time a hostile spell lands on you, this bonus is reduced by 10 resistance. (3 Min Cooldown)
                throw new NotImplementedException();
            };
            // 35413 - Crusader's Scaled Gauntlets
            item = GetItem(35413, "Crusader's Scaled Gauntlets");
            item.OnEquip = (sim) =>
            {
                //Increases the Holy damage bonus of your Judgement of the Crusader by 20.
                throw new NotImplementedException();
            };
            // 13379 - Piccolo of the Flaming Fire
            item = GetItem(13379, "Piccolo of the Flaming Fire");
            item.OnUse = (sim) =>
            {
                //Use: Causes nearby players to dance. (1 Min Cooldown)
                throw new NotImplementedException();
            };
            // 18354 - Pimgib's Collar
            item = GetItem(18354, "Pimgib's Collar");
            item.OnEquip = (sim) =>
            {
                //Increases the damage of your Imp's Firebolt spell by 8.
                throw new NotImplementedException();
            };
            // 28108 - Power Infused Mushroom
            item = GetItem(28108, "Power Infused Mushroom");
            item.OnEquip = (sim) =>
            {
                //Restores 200 mana when you kill a target that gives experience or honor. This effect cannot occur more than once every 10 seconds.
                throw new NotImplementedException();
            };
            // 14554 - Cloudkeeper Legplates
            item = GetItem(14554, "Cloudkeeper Legplates");
            item.OnUse = (sim) =>
            {
                //Use: Increases attack power by 100 for 30 sec. (15 Min Cooldown)
                throw new NotImplementedException();
            };
            // 30621 - Prism of Inner Calm
            item = GetItem(30621, "Prism of Inner Calm");
            item.OnEquip = (sim) =>
            {
                //Reduces the threat from your harmful critical strikes.
                throw new NotImplementedException();
            };
            // 15867 - Prismcharm
            item = GetItem(15867, "Prismcharm");
            item.OnUse = (sim) =>
            {
                //Use: Increases all resistances by 20 for 30 sec. (30 Min Cooldown)
                throw new NotImplementedException();
            };
            // 10721 - Gnomish Harm Prevention Belt
            item = GetItem(10721, "Gnomish Harm Prevention Belt");
            item.OnUse = (sim) =>
            {
                //Use: A shield of force protects you from the next 500 damage done over the next 10 min. WARNING: &nbsp;Force Field may overload when struck temporarily removing the wearer from this dimension. (1 Hour Cooldown)
                throw new NotImplementedException();
            };
            // 34626 - Prototype Tonk Controller
            item = GetItem(34626, "Prototype Tonk Controller");
            item.OnUse = (sim) =>
            {
                //Use: Summon and control your steam tonk. (30 Sec Cooldown)
                throw new NotImplementedException();
            };
            // 19577 - Rage of Mugamba
            item = GetItem(19577, "Rage of Mugamba");
            item.OnEquip = (sim) =>
            {
                //Reduces the cost of your Hamstring ability by 2 rage points.
                throw new NotImplementedException();
            };
            // 27683 - Quagmirran's Eye
            item = GetItem(27683, "Quagmirran's Eye");
            item.OnEquip = (sim) =>
            {
                //Your harmful spells have a chance to increase your spell haste rating by 320 for 6 secs.
                throw new NotImplementedException();
            };
            // 15873 - Ragged John's Neverending Cup
            item = GetItem(15873, "Ragged John's Neverending Cup");
            item.OnUse = (sim) =>
            {
                //Use: Increases Stamina by 10 and reduces physical damage taken by 5 for 10 min. However, lowers Strength and Agility by 5 and increases magical damage taken by up to 20. (30 Min Cooldown)
                throw new NotImplementedException();
            };
            // 18722 - Death Grips
            item = GetItem(18722, "Death Grips");
            item.OnEquip = (sim) =>
            {
                //Disarm duration reduced by 50%.
                throw new NotImplementedException();
            };
            // 28618 - Grand Marshal's Dragonhide Gloves
            item = GetItem(28618, "Grand Marshal's Dragonhide Gloves");
            item.OnEquip = (sim) =>
            {
                //Reduces the cast time of your Cyclone spell by 0.1 sec.
                throw new NotImplementedException();
            };
            // 13515 - Ramstein's Lightning Bolts
            item = GetItem(13515, "Ramstein's Lightning Bolts");
            item.OnUse = (sim) =>
            {
                //Use: Harness the power of lightning to strike down all enemies around you for 200 Nature damage. (5 Min Cooldown)
                throw new NotImplementedException();
            };
            // 31589 - Grand Marshal's Kodohide Gloves
            item = GetItem(31589, "Grand Marshal's Kodohide Gloves");
            item.OnEquip = (sim) =>
            {
                //Reduces the cast time of your Cyclone spell by 0.1 sec.
                throw new NotImplementedException();
            };
            // 35750 - Redeemer's Alchemist Stone
            item = GetItem(35750, "Redeemer's Alchemist Stone");
            item.OnEquip = (sim) =>
            {
                //Increases the effect that healing and mana potions have on the wearer by 40%. &nbsp;This effect does not stack.
                throw new NotImplementedException();
            };
            // 28042 - Regal Protectorate
            item = GetItem(28042, "Regal Protectorate");
            item.OnUse = (sim) =>
            {
                //Use: Increases maximum health by 900 for 15 sec. (5 Min Cooldown)
                throw new NotImplementedException();
            };
            // 28684 - Grand Marshal's Leather Gloves
            item = GetItem(28684, "Grand Marshal's Leather Gloves");
            item.OnEquip = (sim) =>
            {
                //Causes your Deadly Throw ability to interrupt spellcasting and prevent any spell in that school from being cast for 3 sec.
                throw new NotImplementedException();
            };
            // 28719 - Grand Marshal's Wyrmhide Gloves
            item = GetItem(28719, "Grand Marshal's Wyrmhide Gloves");
            item.OnEquip = (sim) =>
            {
                //Reduces the cast time of your Cyclone spell by 0.1 sec.
                throw new NotImplementedException();
            };
            // 33676 - Vengeful Gladiator's Dreadweave Gloves
            item = GetItem(33676, "Vengeful Gladiator's Dreadweave Gloves");
            item.OnEquip = (sim) =>
            {
                //Gives you a 50% chance to avoid interruption caused by damage while casting Fear.
                throw new NotImplementedException();
            };
            // 33684 - Vengeful Gladiator's Felweave Handguards
            item = GetItem(33684, "Vengeful Gladiator's Felweave Handguards");
            item.OnEquip = (sim) =>
            {
                //Gives you a 50% chance to avoid interruption caused by damage while casting Fear.
                throw new NotImplementedException();
            };
            // 33717 - Vengeful Gladiator's Mooncloth Gloves
            item = GetItem(33717, "Vengeful Gladiator's Mooncloth Gloves");
            item.OnEquip = (sim) =>
            {
                //Reduces the cooldown of your Psychic Scream ability by 3 sec.
                throw new NotImplementedException();
            };
            // 33744 - Vengeful Gladiator's Satin Gloves
            item = GetItem(33744, "Vengeful Gladiator's Satin Gloves");
            item.OnEquip = (sim) =>
            {
                //Reduces the cooldown of your Psychic Scream ability by 3 sec.
                throw new NotImplementedException();
            };
            // 33759 - Vengeful Gladiator's Silk Handguards
            item = GetItem(33759, "Vengeful Gladiator's Silk Handguards");
            item.OnEquip = (sim) =>
            {
                //Gives you a 50% chance to avoid interruption caused by damage while casting Polymorph.
                throw new NotImplementedException();
            };
            // 34678 - Shattered Sun Pendant of Acumen
            item = GetItem(34678, "Shattered Sun Pendant of Acumen");
            item.OnEquip = (sim) =>
            {
                //Your spells have a chance to call on the power of the Arcane if you're exalted with the Scryers, or the Light if you're exalted with the Aldor.
                throw new NotImplementedException();
            };
            // 34679 - Shattered Sun Pendant of Might
            item = GetItem(34679, "Shattered Sun Pendant of Might");
            item.OnEquip = (sim) =>
            {
                //Your melee and ranged attacks have a chance to call on the power of the Arcane if you're exalted with the Scryers, or the Light if you're exalted with the Aldor.
                throw new NotImplementedException();
            };
            // 19953 - Renataki's Charm of Beasts
            item = GetItem(19953, "Renataki's Charm of Beasts");
            item.OnUse = (sim) =>
            {
                //Use: Instantly clears the cooldowns of Aimed Shot, Multishot, and Volley. (3 Min Cooldown)
                throw new NotImplementedException();
            };
            // 23839 - Foreman's Reinforced Helmet
            item = GetItem(23839, "Foreman's Reinforced Helmet");
            item.OnEquip = (sim) =>
            {
                //Increases your chance to resist Stun and Disorient effects by 10%.
                throw new NotImplementedException();
            };
            // 34680 - Shattered Sun Pendant of Resolve
            item = GetItem(34680, "Shattered Sun Pendant of Resolve");
            item.OnEquip = (sim) =>
            {
                //Your melee and ranged attacks have a chance to call on the power of the Arcane if you're exalted with the Scryers, or the Light if you're exalted with the Aldor.
                throw new NotImplementedException();
            };
            // 19954 - Renataki's Charm of Trickery
            item = GetItem(19954, "Renataki's Charm of Trickery");
            item.OnUse = (sim) =>
            {
                //Use: Instantly increases your energy by [60 - 4 * max(0, Level - 60)]. (3 Min Cooldown)
                throw new NotImplementedException();
            };
            // 34677 - Shattered Sun Pendant of Restoration
            item = GetItem(34677, "Shattered Sun Pendant of Restoration");
            item.OnEquip = (sim) =>
            {
                //Your heals have a chance to call on the power of the Arcane if you're exalted with the Scryers, or the Light if you're exalted with the Aldor.
                throw new NotImplementedException();
            };
            // 28590 - Ribbon of Sacrifice
            item = GetItem(28590, "Ribbon of Sacrifice");
            item.OnUse = (sim) =>
            {
                //Use: For the next 20 sec, your direct heals grant Fecundity to your target, increasing the healing received by the target by up to 30. &nbsp;Fecundity lasts 10 sec and stacks up to 5 times. (2 Min Cooldown)
                throw new NotImplementedException();
            };
            // 25653 - Riding Crop
            item = GetItem(25653, "Riding Crop");
            item.OnEquip = (sim) =>
            {
                //Increases mount speed by 10%.
                throw new NotImplementedException();
            };
            // 9404 - Olaf's All Purpose Shield
            item = GetItem(9404, "Olaf's All Purpose Shield");
            item.OnUse = (sim) =>
            {
                //Use: Reduces your fall speed for 10 sec. (1 Hour Cooldown)
                throw new NotImplementedException();
            };
            // 14152 - Robe of the Archmage
            item = GetItem(14152, "Robe of the Archmage");
            item.OnUse = (sim) =>
            {
                //Use: Restores 375 to 625 mana. (5 Min Cooldown)
                throw new NotImplementedException();
            };
            // 23564 - Twisting Nether Chain Shirt
            item = GetItem(23564, "Twisting Nether Chain Shirt");
            item.OnUse = (sim) =>
            {
                //Use: You are protected from all physical attacks for 6 sec, but cannot attack or use physical abilities. (30 Min Cooldown)
                throw new NotImplementedException();
            };
            // 28579 - Romulo's Poison Vial
            item = GetItem(28579, "Romulo's Poison Vial");
            item.OnEquip = (sim) =>
            {
                //Your melee and ranged attacks have a chance to inject poison into your target dealing 222 to 332 Nature damage.
                throw new NotImplementedException();
            };
            // 16573 - General's Mail Boots
            item = GetItem(16573, "General's Mail Boots");
            item.OnEquip = (sim) =>
            {
                //Increases the speed of your Ghost Wolf ability by 15%. &nbsp;Does not function for players higher than level 60.
                throw new NotImplementedException();
            };
            // 32476 - Gadgetstorm Goggles
            item = GetItem(32476, "Gadgetstorm Goggles");
            item.OnEquip = (sim) =>
            {
                //Shows the location of all nearby gas clouds on the minimap.
                //Slightly increases your stealth detection.
                throw new NotImplementedException();
            };
            item.OnUse = (sim) =>
            {
                //Use: Allows you to look far into the distance. (3 Sec Cooldown)
                throw new NotImplementedException();
            };
            // 25994 - Rune of Force
            item = GetItem(25994, "Rune of Force");
            item.OnUse = (sim) =>
            {
                //Use: Increases attack power by 120 for 15 sec. (2 Min Cooldown)
                throw new NotImplementedException();
            };
            // 20956 - Silver Rose Pendant
            item = GetItem(20956, "Silver Rose Pendant");
            item.OnEquip = (sim) =>
            {
                //When struck in combat inflicts 1 Arcane damage to the attacker.
                throw new NotImplementedException();
            };
            // 28602 - Robe of the Elder Scribes
            item = GetItem(28602, "Robe of the Elder Scribes");
            item.OnEquip = (sim) =>
            {
                //Gives a chance when your harmful spells land to increase the damage of your spells and effects by up to 130 for 10 sec.
                throw new NotImplementedException();
            };
            // 19340 - Rune of Metamorphosis
            item = GetItem(19340, "Rune of Metamorphosis");
            item.OnUse = (sim) =>
            {
                //Use: Decreases the mana cost of all Druid shapeshifting forms by 550 for 20 sec. (5 Min Cooldown)
                throw new NotImplementedException();
            };
            // 12628 - Demon Forged Breastplate
            item = GetItem(12628, "Demon Forged Breastplate");
            item.OnEquip = (sim) =>
            {
                //When struck has a 3% chance of stealing 120 life from the attacker over 4 sec.
                throw new NotImplementedException();
            };
            // 12631 - Fiery Plate Gauntlets
            item = GetItem(12631, "Fiery Plate Gauntlets");
            item.OnEquip = (sim) =>
            {
                //Adds 4 fire damage to your weapon attack.
                throw new NotImplementedException();
            };
            // 18346 - Threadbare Trousers
            item = GetItem(18346, "Threadbare Trousers");
            item.OnEquip = (sim) =>
            {
                //Increases damage done to Undead by magical spells and effects by up to 35.
                throw new NotImplementedException();
            };
            // 19812 - Rune of the Dawn
            item = GetItem(19812, "Rune of the Dawn");
            item.OnEquip = (sim) =>
            {
                //Increases damage done to Undead by magical spells and effects by up to 48. &nbsp;It also allows the acquisition of Scourgestones on behalf of the Argent Dawn.
                throw new NotImplementedException();
            };
            // 24376 - Runed Fungalcap
            item = GetItem(24376, "Runed Fungalcap");
            item.OnUse = (sim) =>
            {
                //Use: Absorbs 440 damage. &nbsp;Lasts 20 sec. (2 Min Cooldown)
                throw new NotImplementedException();
            };
            // 10542 - Goblin Mining Helmet
            item = GetItem(10542, "Goblin Mining Helmet");
            item.OnEquip = (sim) =>
            {
                //Mining +5.
                throw new NotImplementedException();
            };
            // 7349 - Herbalist's Gloves
            item = GetItem(7349, "Herbalist's Gloves");
            item.OnEquip = (sim) =>
            {
                //Herbalism +5.
                throw new NotImplementedException();
            };
            // 20512 - Sanctified Orb
            item = GetItem(20512, "Sanctified Orb");
            item.OnUse = (sim) =>
            {
                //Use: Increases your critical strike rating and spell critical strike rating by 42. &nbsp;Lasts 25 sec. (3 Min Cooldown)
                throw new NotImplementedException();
            };
            // 14153 - Robe of the Void
            item = GetItem(14153, "Robe of the Void");
            item.OnUse = (sim) =>
            {
                //Use: Heal your pet for 450 to 750. (10 Min Cooldown)
                throw new NotImplementedException();
            };
            // 28811 - High Warlord's Dragonhide Gloves
            item = GetItem(28811, "High Warlord's Dragonhide Gloves");
            item.OnEquip = (sim) =>
            {
                //Reduces the cast time of your Cyclone spell by 0.1 sec.
                throw new NotImplementedException();
            };
            // 21625 - Scarab Brooch
            item = GetItem(21625, "Scarab Brooch");
            item.OnUse = (sim) =>
            {
                //Use: Your magical heals provide the target with a shield that absorbs damage equal to 15% of the amount healed for 30 sec. (3 Min Cooldown)
                throw new NotImplementedException();
            };
            // 25828 - Petrified Lichen Guard
            item = GetItem(25828, "Petrified Lichen Guard");
            item.OnEquip = (sim) =>
            {
                //Afflicts your attacker with deadly poison spores each time you block.
                throw new NotImplementedException();
            };
            // 23085 - Robe of Undead Cleansing
            item = GetItem(23085, "Robe of Undead Cleansing");
            item.OnEquip = (sim) =>
            {
                //Increases damage done to Undead by magical spells and effects by up to 48.
                throw new NotImplementedException();
            };
            // 31584 - High Warlord's Kodohide Gloves
            item = GetItem(31584, "High Warlord's Kodohide Gloves");
            item.OnEquip = (sim) =>
            {
                //Reduces the cast time of your Cyclone spell by 0.1 sec.
                throw new NotImplementedException();
            };
            // 30629 - Scarab of Displacement
            item = GetItem(30629, "Scarab of Displacement");
            item.OnUse = (sim) =>
            {
                //Use: Increases your defense rating by 165, but decreases your melee and ranged attack power by 330. &nbsp;Effect lasts for 15 sec. (3 Min Cooldown)
                throw new NotImplementedException();
            };
            // 28836 - High Warlord's Leather Gloves
            item = GetItem(28836, "High Warlord's Leather Gloves");
            item.OnEquip = (sim) =>
            {
                //Causes your Deadly Throw ability to interrupt spellcasting and prevent any spell in that school from being cast for 3 sec.
                throw new NotImplementedException();
            };
            // 28190 - Scarab of the Infinite Cycle
            item = GetItem(28190, "Scarab of the Infinite Cycle");
            item.OnEquip = (sim) =>
            {
                //Your direct healing and heal over time spells have a chance to increase your spell haste rating by 320 for 6 secs.
                throw new NotImplementedException();
            };
            // 30314 - Phaseshift Bulwark
            item = GetItem(30314, "Phaseshift Bulwark");
            item.OnUse = (sim) =>
            {
                //Use: Shields the caster, absorbing 100000 damage and making the caster immune to Fear and Snare effects for 4 sec. (30 Sec Cooldown)
                throw new NotImplementedException();
            };
            // 28871 - High Warlord's Wyrmhide Gloves
            item = GetItem(28871, "High Warlord's Wyrmhide Gloves");
            item.OnEquip = (sim) =>
            {
                //Reduces the cast time of your Cyclone spell by 0.1 sec.
                throw new NotImplementedException();
            };
            // 30696 - Scourgebane
            item = GetItem(30696, "Scourgebane");
            item.OnUse = (sim) =>
            {
                //Use: Imbue your weapon with power, increasing attack power against undead and demons by 150. &nbsp;Lasts 5 min. (30 Min Cooldown)
                throw new NotImplementedException();
            };
            // 19343 - Scrolls of Blinding Light
            item = GetItem(19343, "Scrolls of Blinding Light");
            item.OnUse = (sim) =>
            {
                //Use: Energizes a Paladin with light, increasing haste rating by 250 and spell haste rating by 330 for 20 sec. (5 Min Cooldown)
                throw new NotImplementedException();
            };
            // 29132 - Scryer's Bloodgem
            item = GetItem(29132, "Scryer's Bloodgem");
            item.OnUse = (sim) =>
            {
                //Use: Increases spell damage by up to 150 and healing by up to 280 for 15 sec. (1 Min, 30 Sec Cooldown)
                throw new NotImplementedException();
            };
            // 11819 - Second Wind
            item = GetItem(11819, "Second Wind");
            item.OnUse = (sim) =>
            {
                //Use: Restores 30 mana every 1 sec for 10 sec. (15 Min Cooldown)
                throw new NotImplementedException();
            };
            // 18438 - Sergeant's Mark
            item = GetItem(18438, "Sergeant's Mark");
            item.OnUse = (sim) =>
            {
                //Use: Removes one Fear, Polymorph, or Sleep effect. (3 Min Cooldown)
                throw new NotImplementedException();
            };
            // 30720 - Serpent-Coil Braid
            item = GetItem(30720, "Serpent-Coil Braid");
            item.OnEquip = (sim) =>
            {
                //You gain 25% more mana when you use a mana gem. &nbsp;In addition, using a mana gem grants you 225 spell damage for 15 sec.
                throw new NotImplementedException();
            };
            // 940 - Robes of Insight
            item = GetItem(940, "Robes of Insight");
            item.OnUse = (sim) =>
            {
                //Use: Reduces the cost of your next spell cast within 10 sec by up to 500 mana. (15 Min Cooldown)
                throw new NotImplementedException();
            };
            // 23587 - Mirren's Drinking Hat
            item = GetItem(23587, "Mirren's Drinking Hat");
            item.OnUse = (sim) =>
            {
                //Use: Reach into the hat for a drink. (1 Hour Cooldown)
                throw new NotImplementedException();
            };
            // 30626 - Sextant of Unstable Currents
            item = GetItem(30626, "Sextant of Unstable Currents");
            item.OnEquip = (sim) =>
            {
                //Your spell critical strikes have a chance to increase your spell damage and healing by 190 for 15 sec.
                throw new NotImplementedException();
            };
            // 32501 - Shadowmoon Insignia
            item = GetItem(32501, "Shadowmoon Insignia");
            item.OnUse = (sim) =>
            {
                //Use: Increases your maximum health by 1750 for 20 sec. (3 Min Cooldown)
                throw new NotImplementedException();
            };
            // 34472 - Shard of Contempt
            item = GetItem(34472, "Shard of Contempt");
            item.OnEquip = (sim) =>
            {
                //Chance on hit to increase your attack power by 230 for 20 sec.
                throw new NotImplementedException();
            };
            // 21891 - Shard of the Fallen Star
            item = GetItem(21891, "Shard of the Fallen Star");
            item.OnUse = (sim) =>
            {
                //Use: Calls down a meteor, burning all enemies within the area for 400 to 442 total Fire damage. (3 Min Cooldown)
                throw new NotImplementedException();
            };
            // 16397 - Knight-Lieutenant's Dragonhide Gloves
            item = GetItem(16397, "Knight-Lieutenant's Dragonhide Gloves");
            item.OnEquip = (sim) =>
            {
                //Slightly increases your stealth detection.
                throw new NotImplementedException();
            };
            // 29347 - Talisman of the Breaker
            item = GetItem(29347, "Talisman of the Breaker");
            item.OnEquip = (sim) =>
            {
                //Reduces the duration of any Silence or Interrupt effects used against the wearer by 20%. This effect does not stack with other similar effects.
                throw new NotImplementedException();
            };
            // 28418 - Shiffar's Nexus-Horn
            item = GetItem(28418, "Shiffar's Nexus-Horn");
            item.OnEquip = (sim) =>
            {
                //Chance on spell critical hit to increase your spell damage and healing by 225 for 10 secs.
                throw new NotImplementedException();
            };
            // 23280 - Knight-Lieutenant's Dragonhide Grips
            item = GetItem(23280, "Knight-Lieutenant's Dragonhide Grips");
            item.OnEquip = (sim) =>
            {
                //Slightly increases your stealth detection.
                throw new NotImplementedException();
            };
            // 34429 - Shifting Naaru Sliver
            item = GetItem(34429, "Shifting Naaru Sliver");
            item.OnUse = (sim) =>
            {
                //Use: Conjures a Power Circle lasting for 15 sec. &nbsp;While standing in this circle, the caster gains up to 320 spell damage and healing. (1 Min, 30 Sec Cooldown)
                throw new NotImplementedException();
            };
            // 8703 - Signet of Expertise
            item = GetItem(8703, "Signet of Expertise");
            item.OnUse = (sim) =>
            {
                //Use: Summons a Hammer of Expertise. (3 Hrs Cooldown)
                throw new NotImplementedException();
            };
            // 7734 - Six Demon Bag
            item = GetItem(7734, "Six Demon Bag");
            item.OnUse = (sim) =>
            {
                //Use: Blasts enemies in front of you with the power of wind, fire, all that kind of thing! (3 Min Cooldown)
                throw new NotImplementedException();
            };
            // 35361 - Kodohide Gloves
            item = GetItem(35361, "Kodohide Gloves");
            item.OnEquip = (sim) =>
            {
                //Reduces the cast time of your Cyclone spell by 0.1 sec.
                throw new NotImplementedException();
            };
            // 30642 - Drape of the Righteous
            item = GetItem(30642, "Drape of the Righteous");
            item.OnEquip = (sim) =>
            {
                //Increases the damage done by Holy spells and effects by up to 43.
                throw new NotImplementedException();
            };
            // 32863 - Skybreaker Whip
            item = GetItem(32863, "Skybreaker Whip");
            item.OnEquip = (sim) =>
            {
                //Increases mount speed by 10%.
                throw new NotImplementedException();
            };
            // 32770 - Skyguard Silver Cross
            item = GetItem(32770, "Skyguard Silver Cross");
            item.OnEquip = (sim) =>
            {
                //50% chance to increase your attack power by 140 for 30 sec when you kill a target that gives experience or honor. This effect cannot occur more than once every 10 seconds.
                throw new NotImplementedException();
            };
            // 23041 - Slayer's Crest
            item = GetItem(23041, "Slayer's Crest");
            item.OnUse = (sim) =>
            {
                //Use: Increases attack power by 260 for 20 sec. (2 Min Cooldown)
                throw new NotImplementedException();
            };
            // 13171 - Smokey's Lighter
            item = GetItem(13171, "Smokey's Lighter");
            item.OnUse = (sim) =>
            {
                //Use: Deals 125 Fire damage to all targets in a cone in front of the caster. (5 Min Cooldown)
                throw new NotImplementedException();
            };
            // 13213 - Smolderweb's Eye
            item = GetItem(13213, "Smolderweb's Eye");
            item.OnUse = (sim) =>
            {
                //Use: Poisons target for 20 Nature damage every 2 sec for 20 sec. (2 Min Cooldown)
                throw new NotImplementedException();
            };
            // 19982 - Duskbat Drape
            item = GetItem(19982, "Duskbat Drape");
            item.OnEquip = (sim) =>
            {
                //Reduces damage from falling.
                throw new NotImplementedException();
            };
            // 29613 - General's Lamellar Gloves
            item = GetItem(29613, "General's Lamellar Gloves");
            item.OnEquip = (sim) =>
            {
                //Increases the Holy damage bonus of your Judgement of the Crusader by 20.
                throw new NotImplementedException();
            };
            // 35581 - Rocket Boots Xtreme Lite
            item = GetItem(35581, "Rocket Boots Xtreme Lite");
            item.OnUse = (sim) =>
            {
                //Use: Engage the rocket boots to greatly increase your speed... most of the time. (5 Min Cooldown)
                throw new NotImplementedException();
            };
            // 30446 - Solarian's Sapphire
            item = GetItem(30446, "Solarian's Sapphire");
            item.OnEquip = (sim) =>
            {
                //Your Battle Shout ability grants an additional 70 attack power.
                throw new NotImplementedException();
            };
            // 16548 - General's Plate Gauntlets
            item = GetItem(16548, "General's Plate Gauntlets");
            item.OnEquip = (sim) =>
            {
                //Hamstring Rage cost reduced by 3.
                throw new NotImplementedException();
            };
            // 16448 - Marshal's Dragonhide Gauntlets
            item = GetItem(16448, "Marshal's Dragonhide Gauntlets");
            item.OnEquip = (sim) =>
            {
                //Slightly increases your stealth detection.
                throw new NotImplementedException();
            };
            // 27703 - Gladiator's Lamellar Gauntlets
            item = GetItem(27703, "Gladiator's Lamellar Gauntlets");
            item.OnEquip = (sim) =>
            {
                //Increases the critical effect chance of your Flash of Light by 2%.
                throw new NotImplementedException();
            };
            // 35749 - Sorcerer's Alchemist Stone
            item = GetItem(35749, "Sorcerer's Alchemist Stone");
            item.OnEquip = (sim) =>
            {
                //Increases the effect that healing and mana potions have on the wearer by 40%. &nbsp;This effect does not stack.
                throw new NotImplementedException();
            };
            // 28585 - Ruby Slippers
            item = GetItem(28585, "Ruby Slippers");
            item.OnUse = (sim) =>
            {
                //Use: Returns you to &lt;Hearthstone Location&gt;. &nbsp;Speak to an Innkeeper in a different place to change your home location. (1 Hour Cooldown)
                throw new NotImplementedException();
            };
            // 31614 - Gladiator's Ornamented Gloves
            item = GetItem(31614, "Gladiator's Ornamented Gloves");
            item.OnEquip = (sim) =>
            {
                //Increases the critical effect chance of your Flash of Light by 2%.
                throw new NotImplementedException();
            };
            // 32495 - Powerheal 4000 Lens
            item = GetItem(32495, "Powerheal 4000 Lens");
            item.OnEquip = (sim) =>
            {
                //Shows the location of all nearby gas clouds on the minimap.
                //Slightly increases your stealth detection.
                throw new NotImplementedException();
            };
            item.OnUse = (sim) =>
            {
                //Use: Allows you to look far into the distance. (3 Sec Cooldown)
                throw new NotImplementedException();
            };
            // 29357 - Master Thief's Gloves
            item = GetItem(29357, "Master Thief's Gloves");
            item.OnEquip = (sim) =>
            {
                //Disarm duration reduced by 50%.
                throw new NotImplementedException();
            };
            // 24549 - Gladiator's Plate Gauntlets
            item = GetItem(24549, "Gladiator's Plate Gauntlets");
            item.OnEquip = (sim) =>
            {
                //Hamstring Rage cost reduced by 3.
                throw new NotImplementedException();
            };
            // 35181 - Powerheal 9000 Lens
            item = GetItem(35181, "Powerheal 9000 Lens");
            item.OnEquip = (sim) =>
            {
                //Shows the location of all nearby gas clouds on the minimap.
                //Slightly increases your stealth detection.
                throw new NotImplementedException();
            };
            item.OnUse = (sim) =>
            {
                //Use: Allows you to look far into the distance. (3 Sec Cooldown)
                throw new NotImplementedException();
            };
            // 31967 - Merciless Gladiator's Dragonhide Gloves
            item = GetItem(31967, "Merciless Gladiator's Dragonhide Gloves");
            item.OnEquip = (sim) =>
            {
                //Reduces the cast time of your Cyclone spell by 0.1 sec.
                throw new NotImplementedException();
            };
            // 27880 - Gladiator's Scaled Gauntlets
            item = GetItem(27880, "Gladiator's Scaled Gauntlets");
            item.OnEquip = (sim) =>
            {
                //Increases the Holy damage bonus of your Judgement of the Crusader by 20.
                throw new NotImplementedException();
            };
            // 30620 - Spyglass of the Hidden Fleet
            item = GetItem(30620, "Spyglass of the Hidden Fleet");
            item.OnUse = (sim) =>
            {
                //Use: Heals 1300 damage over 12 sec. (2 Min Cooldown)
                throw new NotImplementedException();
            };
            // 29594 - Knight-Lieutenant's Mail Greaves
            item = GetItem(29594, "Knight-Lieutenant's Mail Greaves");
            item.OnEquip = (sim) =>
            {
                //Increases the speed of your Ghost Wolf ability by 15%. &nbsp;Does not function for players higher than level 60.
                throw new NotImplementedException();
            };
            // 31987 - Merciless Gladiator's Kodohide Gloves
            item = GetItem(31987, "Merciless Gladiator's Kodohide Gloves");
            item.OnEquip = (sim) =>
            {
                //Reduces the cast time of your Cyclone spell by 0.1 sec.
                throw new NotImplementedException();
            };
            // 25995 - Star of Sha'naar
            item = GetItem(25995, "Star of Sha'naar");
            item.OnUse = (sim) =>
            {
                //Use: Increases spell damage by up to 70 and healing by up to 135 for 15 sec. (2 Min Cooldown)
                throw new NotImplementedException();
            };
            // 31998 - Merciless Gladiator's Leather Gloves
            item = GetItem(31998, "Merciless Gladiator's Leather Gloves");
            item.OnEquip = (sim) =>
            {
                //Causes your Deadly Throw ability to interrupt spellcasting and prevent any spell in that school from being cast for 3 sec.
                throw new NotImplementedException();
            };
            // 30340 - Starkiller's Bauble
            item = GetItem(30340, "Starkiller's Bauble");
            item.OnUse = (sim) =>
            {
                //Use: Increases spell damage by up to 125 for 15 sec. (1 Min, 30 Sec Cooldown)
                throw new NotImplementedException();
            };
            // 32056 - Merciless Gladiator's Wyrmhide Gloves
            item = GetItem(32056, "Merciless Gladiator's Wyrmhide Gloves");
            item.OnEquip = (sim) =>
            {
                //Reduces the cast time of your Cyclone spell by 0.1 sec.
                throw new NotImplementedException();
            };
            // 19594 - The All-Seeing Eye of Zuldazar
            item = GetItem(19594, "The All-Seeing Eye of Zuldazar");
            item.OnEquip = (sim) =>
            {
                //Increases the amount of damage absorbed by Power Word: Shield by 35.
                throw new NotImplementedException();
            };
            // 34428 - Steely Naaru Sliver
            item = GetItem(34428, "Steely Naaru Sliver");
            item.OnUse = (sim) =>
            {
                //Use: Increases maximum health by 2000 for 15 sec. (5 Min Cooldown)
                throw new NotImplementedException();
            };
            // 17691 - Stormpike Insignia Rank 1
            item = GetItem(17691, "Stormpike Insignia Rank 1");
            item.OnUse = (sim) =>
            {
                //Use: Returns you to the sanctuary of Dun Baldar. (2 Min Cooldown)
                throw new NotImplementedException();
            };
            // 17900 - Stormpike Insignia Rank 2
            item = GetItem(17900, "Stormpike Insignia Rank 2");
            item.OnUse = (sim) =>
            {
                //Use: Returns you to the sanctuary of Dun Baldar. (2 Min Cooldown)
                throw new NotImplementedException();
            };
            // 17901 - Stormpike Insignia Rank 3
            item = GetItem(17901, "Stormpike Insignia Rank 3");
            item.OnUse = (sim) =>
            {
                //Use: Returns you to the sanctuary of Dun Baldar. (2 Min Cooldown)
                throw new NotImplementedException();
            };
            // 17902 - Stormpike Insignia Rank 4
            item = GetItem(17902, "Stormpike Insignia Rank 4");
            item.OnUse = (sim) =>
            {
                //Use: Returns you to the sanctuary of Dun Baldar. (2 Min Cooldown)
                throw new NotImplementedException();
            };
            // 28680 - Grand Marshal's Lamellar Gauntlets
            item = GetItem(28680, "Grand Marshal's Lamellar Gauntlets");
            item.OnEquip = (sim) =>
            {
                //Increases the Holy damage bonus of your Judgement of the Crusader by 20.
                throw new NotImplementedException();
            };
            // 17903 - Stormpike Insignia Rank 5
            item = GetItem(17903, "Stormpike Insignia Rank 5");
            item.OnUse = (sim) =>
            {
                //Use: Returns you to the sanctuary of Dun Baldar. (2 Min Cooldown)
                throw new NotImplementedException();
            };
            // 31631 - Grand Marshal's Ornamented Gloves
            item = GetItem(31631, "Grand Marshal's Ornamented Gloves");
            item.OnEquip = (sim) =>
            {
                //Increases the Holy damage bonus of your Judgement of the Crusader by 20.
                throw new NotImplementedException();
            };
            // 17904 - Stormpike Insignia Rank 6
            item = GetItem(17904, "Stormpike Insignia Rank 6");
            item.OnUse = (sim) =>
            {
                //Use: Returns you to the sanctuary of Dun Baldar.
                throw new NotImplementedException();
            };
            // 28700 - Grand Marshal's Plate Gauntlets
            item = GetItem(28700, "Grand Marshal's Plate Gauntlets");
            item.OnEquip = (sim) =>
            {
                //Hamstring Rage cost reduced by 3.
                throw new NotImplementedException();
            };
            // 5387 - Enchanted Moonstalker Cloak
            item = GetItem(5387, "Enchanted Moonstalker Cloak");
            item.OnUse = (sim) =>
            {
                //Use: Transform into a Moonstalker and become invisible for 5 min.
                throw new NotImplementedException();
            };
            // 28710 - Grand Marshal's Scaled Gauntlets
            item = GetItem(28710, "Grand Marshal's Scaled Gauntlets");
            item.OnEquip = (sim) =>
            {
                //Increases the Holy damage bonus of your Judgement of the Crusader by 20.
                throw new NotImplementedException();
            };
            // 24106 - Thick Felsteel Necklace
            item = GetItem(24106, "Thick Felsteel Necklace");
            item.OnUse = (sim) =>
            {
                //Use: Increases Stamina of nearby party members by 20 for 30 min. (1 Hour Cooldown)
                throw new NotImplementedException();
            };
            // 38089 - Ruby Shades
            item = GetItem(38089, "Ruby Shades");
            item.OnEquip = (sim) =>
            {
                //Increases your chance to be the center of attention.
                throw new NotImplementedException();
            };
            // 9397 - Energy Cloak
            item = GetItem(9397, "Energy Cloak");
            item.OnUse = (sim) =>
            {
                //Use: Restores 390 to 410 mana. (1 Hour Cooldown)
                throw new NotImplementedException();
            };
            // 20071 - Talisman of Arathor
            item = GetItem(20071, "Talisman of Arathor");
            item.OnUse = (sim) =>
            {
                //Use: Absorbs 495 to 605 physical damage. &nbsp;Lasts 15 sec. (3 Min Cooldown)
                throw new NotImplementedException();
            };
            // 22678 - Talisman of Ascendance
            item = GetItem(22678, "Talisman of Ascendance");
            item.OnUse = (sim) =>
            {
                //Use: Your next 5 damage or healing spells cast within 20 seconds will grant a bonus of up to 40 damage and up to 75 healing, stacking up to 5 times. Expires after 6 damage or healing spells or 20 seconds, whichever occurs first. (1 Min Cooldown)
                throw new NotImplementedException();
            };
            // 34355 - Lightning Etched Specs
            item = GetItem(34355, "Lightning Etched Specs");
            item.OnEquip = (sim) =>
            {
                //Shows the location of all nearby gas clouds on the minimap.
                //Slightly increases your stealth detection.
                throw new NotImplementedException();
            };
            item.OnUse = (sim) =>
            {
                //Use: Allows you to look far into the distance. (3 Sec Cooldown)
                throw new NotImplementedException();
            };
            // 18820 - Talisman of Ephemeral Power
            item = GetItem(18820, "Talisman of Ephemeral Power");
            item.OnUse = (sim) =>
            {
                //Use: Increases damage and healing done by magical spells and effects by up to 175 for 15 sec. (1 Min, 30 Sec Cooldown)
                throw new NotImplementedException();
            };
            // 25829 - Talisman of the Alliance
            item = GetItem(25829, "Talisman of the Alliance");
            item.OnUse = (sim) =>
            {
                //Use: Heal self for 400 to 420 damage. (2 Min Cooldown)
                throw new NotImplementedException();
            };
            // 32475 - Living Replicator Specs
            item = GetItem(32475, "Living Replicator Specs");
            item.OnEquip = (sim) =>
            {
                //Shows the location of all nearby gas clouds on the minimap.
                //Slightly increases your stealth detection.
                throw new NotImplementedException();
            };
            item.OnUse = (sim) =>
            {
                //Use: Allows you to look far into the distance. (3 Sec Cooldown)
                throw new NotImplementedException();
            };
            // 24551 - Talisman of the Horde
            item = GetItem(24551, "Talisman of the Horde");
            item.OnUse = (sim) =>
            {
                //Use: Heal self for 877 to 969 damage. (2 Min Cooldown)
                throw new NotImplementedException();
            };
            // 30448 - Talon of Al'ar
            item = GetItem(30448, "Talon of Al'ar");
            item.OnEquip = (sim) =>
            {
                //Your Arcane Shot ability increases the damage dealt by all other damaging shots by 40 for 6 sec.
                throw new NotImplementedException();
            };
            // 25937 - Terokkar Tablet of Precision
            item = GetItem(25937, "Terokkar Tablet of Precision");
            item.OnUse = (sim) =>
            {
                //Use: Increases attack power by 140 for 15 sec. (1 Min, 30 Sec Cooldown)
                throw new NotImplementedException();
            };
            // 25936 - Terokkar Tablet of Vim
            item = GetItem(25936, "Terokkar Tablet of Vim");
            item.OnUse = (sim) =>
            {
                //Use: Increases spell damage by up to 84 and healing by up to 156 for 15 sec. (1 Min, 30 Sec Cooldown)
                throw new NotImplementedException();
            };
            // 20422 - Twilight Cultist Medallion of Station
            item = GetItem(20422, "Twilight Cultist Medallion of Station");
            item.OnEquip = (sim) =>
            {
                //When worn with the Twilight Trappings Set, allows access to a Wind Stone in Silithus.
                throw new NotImplementedException();
            };
            // 33003 - Test Sword Skill Trinket
            item = GetItem(33003, "Test Sword Skill Trinket");
            item.OnUse = (sim) =>
            {
                //Use: Increases sword skill rating by 99. (2 Min Cooldown)
                throw new NotImplementedException();
            };
            // 29606 - Marshal's Mail Boots
            item = GetItem(29606, "Marshal's Mail Boots");
            item.OnEquip = (sim) =>
            {
                //Increases the speed of your Ghost Wolf ability by 15%. &nbsp;Does not function for players higher than level 60.
                throw new NotImplementedException();
            };
            // 19609 - Unmarred Vision of Voodress
            item = GetItem(19609, "Unmarred Vision of Voodress");
            item.OnEquip = (sim) =>
            {
                //Decreases the mana cost of your Healing Stream and Mana Spring totems by 20.
                throw new NotImplementedException();
            };
            // 19337 - The Black Book
            item = GetItem(19337, "The Black Book");
            item.OnUse = (sim) =>
            {
                //Use: Empowers your pet, increasing its spell damage by 200, its attack power by 325, and its armor by 1600 for 30 sec. This spell will only affect an Imp, Succubus, Voidwalker, Felhunter, or Felguard. (5 Min Cooldown)
                throw new NotImplementedException();
            };
            // 23558 - The Burrower's Shell
            item = GetItem(23558, "The Burrower's Shell");
            item.OnUse = (sim) =>
            {
                //Use: Absorbs 900 damage. &nbsp;Lasts 20 sec. (2 Min Cooldown)
                throw new NotImplementedException();
            };
            // 28832 - High Warlord's Lamellar Gauntlets
            item = GetItem(28832, "High Warlord's Lamellar Gauntlets");
            item.OnEquip = (sim) =>
            {
                //Increases the Holy damage bonus of your Judgement of the Crusader by 20.
                throw new NotImplementedException();
            };
            // 31636 - High Warlord's Ornamented Gloves
            item = GetItem(31636, "High Warlord's Ornamented Gloves");
            item.OnEquip = (sim) =>
            {
                //Increases the Holy damage bonus of your Judgement of the Crusader by 20.
                throw new NotImplementedException();
            };
            // 28852 - High Warlord's Plate Gauntlets
            item = GetItem(28852, "High Warlord's Plate Gauntlets");
            item.OnEquip = (sim) =>
            {
                //Hamstring Rage cost reduced by 3.
                throw new NotImplementedException();
            };
            // 28862 - High Warlord's Scaled Gauntlets
            item = GetItem(28862, "High Warlord's Scaled Gauntlets");
            item.OnEquip = (sim) =>
            {
                //Increases the Holy damage bonus of your Judgement of the Crusader by 20.
                throw new NotImplementedException();
            };
            // 28785 - The Lightning Capacitor
            item = GetItem(28785, "The Lightning Capacitor");
            item.OnEquip = (sim) =>
            {
                //You gain an Electrical Charge each time you cause a damaging spell critical strike. &nbsp;When you reach 3 Electrical Charges, they will release, firing a Lightning Bolt for 694 to 806 damage. &nbsp;Electrical Charge cannot be gained more often than once every 2.5 sec.
                throw new NotImplementedException();
            };
            // 14557 - The Lion Horn of Stormwind
            item = GetItem(14557, "The Lion Horn of Stormwind");
            item.OnEquip = (sim) =>
            {
                //When struck in combat has a 1% chance of increasing all party member's armor by 250 for 30 sec.
                throw new NotImplementedException();
            };
            // 1168 - Skullflame Shield
            item = GetItem(1168, "Skullflame Shield");
            item.OnEquip = (sim) =>
            {
                //When struck in combat has a 3% chance of stealing 35 life from target enemy.
                //When struck in combat has a 1% chance of dealing 75 to 125 Fire damage to all targets around you.
                throw new NotImplementedException();
            };
            // 23046 - The Restrained Essence of Sapphiron
            item = GetItem(23046, "The Restrained Essence of Sapphiron");
            item.OnUse = (sim) =>
            {
                //Use: Increases damage and healing done by magical spells and effects by up to 130 for 20 sec. (2 Min Cooldown)
                throw new NotImplementedException();
            };
            // 35366 - Opportunist's Leather Gloves
            item = GetItem(35366, "Opportunist's Leather Gloves");
            item.OnEquip = (sim) =>
            {
                //Causes your Deadly Throw ability to interrupt spellcasting and prevent any spell in that school from being cast for 3 sec.
                throw new NotImplementedException();
            };
            // 32483 - The Skull of Gul'dan
            item = GetItem(32483, "The Skull of Gul'dan");
            item.OnUse = (sim) =>
            {
                //Use: Tap into the power of the skull, increasing spell haste rating by 175 for 20 sec. (2 Min Cooldown)
                throw new NotImplementedException();
            };
            // 744 - Thunderbrew's Boot Flask
            item = GetItem(744, "Thunderbrew's Boot Flask");
            item.OnUse = (sim) =>
            {
                //Use: Deals 50 Fire damage every 1 sec for 5 sec to all enemies in front of you. Gets you quite drunk too! (30 Min Cooldown)
                throw new NotImplementedException();
            };
            // 1404 - Tidal Charm
            item = GetItem(1404, "Tidal Charm");
            item.OnUse = (sim) =>
            {
                //Use: Stuns target for 3 sec. &nbsp;Increased chance to be resisted when used against targets over level 60. (15 Min Cooldown)
                throw new NotImplementedException();
            };
            // 34470 - Timbal's Focusing Crystal
            item = GetItem(34470, "Timbal's Focusing Crystal");
            item.OnEquip = (sim) =>
            {
                //Each time one of your spells deals periodic damage, there is a chance 285 to 475 additional damage will be dealt.
                throw new NotImplementedException();
            };
            // 32782 - Time-Lost Figurine
            item = GetItem(32782, "Time-Lost Figurine");
            item.OnUse = (sim) =>
            {
                //Use: Transforms caster to have the appearance of a Skettis arakkoa. (30 Min Cooldown)
                throw new NotImplementedException();
            };
            // 29181 - Timelapse Shard
            item = GetItem(29181, "Timelapse Shard");
            item.OnUse = (sim) =>
            {
                //Use: Reduces your threat to enemy targets within 30 yards, making them less likely to attack you. (2 Min Cooldown)
                throw new NotImplementedException();
            };
            // 34029 - Tiny Voodoo Mask
            item = GetItem(34029, "Tiny Voodoo Mask");
            item.OnUse = (sim) =>
            {
                //Use: Calls forth 3 Voodoo Gnomes to destroy your enemies. (10 Min Cooldown)
                throw new NotImplementedException();
            };
            // 16009 - Voice Amplification Modulator
            item = GetItem(16009, "Voice Amplification Modulator");
            item.OnEquip = (sim) =>
            {
                //Reduces the duration of any Silence or Interrupt effects used against the wearer by 10%. This effect does not stack with other similar effects.
                throw new NotImplementedException();
            };
            // 21526 - Band of Icy Depths
            item = GetItem(21526, "Band of Icy Depths");
            item.OnEquip = (sim) =>
            {
                //Allows underwater breathing.
                throw new NotImplementedException();
            };
            // 33828 - Tome of Diabolic Remedy
            item = GetItem(33828, "Tome of Diabolic Remedy");
            item.OnUse = (sim) =>
            {
                //Use: Increases healing done by spells by up to 396 and damage done by spells by up to 132 for 20 sec. (2 Min Cooldown)
                throw new NotImplementedException();
            };
            // 30447 - Tome of Fiery Redemption
            item = GetItem(30447, "Tome of Fiery Redemption");
            item.OnEquip = (sim) =>
            {
                //Each time you cast a spell, there is chance you will gain up to 290 spell damage and healing.
                throw new NotImplementedException();
            };
            // 30627 - Tsunami Talisman
            item = GetItem(30627, "Tsunami Talisman");
            item.OnEquip = (sim) =>
            {
                //Chance on critical hit to increase your attack power by 340 for 10 secs.
                throw new NotImplementedException();
            };
            // 18639 - Ultra-Flash Shadow Reflector
            item = GetItem(18639, "Ultra-Flash Shadow Reflector");
            item.OnUse = (sim) =>
            {
                //Use: Reflects Shadow spells back at their caster for 5 sec. &nbsp;Chance to be resisted when used by players over level 60. (5 Min Cooldown)
                throw new NotImplementedException();
            };
            // 18986 - Ultrasafe Transporter: Gadgetzan
            item = GetItem(18986, "Ultrasafe Transporter: Gadgetzan");
            item.OnUse = (sim) =>
            {
                //Use: Safely transport yourself to Gadgetzan in Tanaris! &nbsp; &nbsp;Emphasis on Safe! &nbsp; Yup, nothing bad could ever happen while using this device! (4 Hrs Cooldown)
                throw new NotImplementedException();
            };
            // 23274 - Knight-Lieutenant's Lamellar Gauntlets
            item = GetItem(23274, "Knight-Lieutenant's Lamellar Gauntlets");
            item.OnEquip = (sim) =>
            {
                //Increases the Holy damage bonus of your Judgement of the Crusader by 20.
                throw new NotImplementedException();
            };
            // 30544 - Ultrasafe Transporter: Toshley's Station
            item = GetItem(30544, "Ultrasafe Transporter: Toshley's Station");
            item.OnUse = (sim) =>
            {
                //Use: Safely transport yourself to Toshley's Station in Blade's Edge! &nbsp; &nbsp;Nothing to worry about while using this baby! &nbsp; Gnomish know-how will get you there in a safe and timely fashion! (4 Hrs Cooldown)
                throw new NotImplementedException();
            };
            // 23286 - Knight-Lieutenant's Plate Gauntlets
            item = GetItem(23286, "Knight-Lieutenant's Plate Gauntlets");
            item.OnEquip = (sim) =>
            {
                //Hamstring Rage cost reduced by 3.
                throw new NotImplementedException();
            };
            // 31360 - Unfinished Headpiece
            item = GetItem(31360, "Unfinished Headpiece");
            item.OnEquip = (sim) =>
            {
                //Allows the wearer to see echoes of the past.
                throw new NotImplementedException();
            };
            item.OnUse = (sim) =>
            {
                //Use: Imbue the unfinished headpiece with the essence of Gul'dan.
                throw new NotImplementedException();
            };
            // 24420 - Unique Equippable Test Item
            item = GetItem(24420, "Unique Equippable Test Item");
            item.OnUse = (sim) =>
            {
                //Use: Hurls a fiery ball that causes 16 to 25 Fire damage and an additional 2 Fire damage over 4 sec. (30 Sec Cooldown)
                throw new NotImplementedException();
            };
            // 19617 - Zandalarian Shadow Mastery Talisman
            item = GetItem(19617, "Zandalarian Shadow Mastery Talisman");
            item.OnEquip = (sim) =>
            {
                //Decreases the cooldown of Kick by 0.5 sec.
                throw new NotImplementedException();
            };
            // 25633 - Uniting Charm
            item = GetItem(25633, "Uniting Charm");
            item.OnUse = (sim) =>
            {
                //Use: Increases attack power by 185 for 15 sec. (2 Min Cooldown)
                throw new NotImplementedException();
            };
            // 11302 - Uther's Strength
            item = GetItem(11302, "Uther's Strength");
            item.OnEquip = (sim) =>
            {
                //Has a 2% chance when struck in combat of protecting you with a holy shield.
                throw new NotImplementedException();
            };
            // 21579 - Vanquished Tentacle of C'Thun
            item = GetItem(21579, "Vanquished Tentacle of C'Thun");
            item.OnUse = (sim) =>
            {
                //Use: Summons a Vanquished Tentacle to your aid for 30 sec. (3 Min Cooldown)
                throw new NotImplementedException();
            };
            // 28040 - Vengeance of the Illidari
            item = GetItem(28040, "Vengeance of the Illidari");
            item.OnUse = (sim) =>
            {
                //Use: Increases spell damage done by up to 120 and healing done by up to 220 for 15 sec. (1 Min, 30 Sec Cooldown)
                throw new NotImplementedException();
            };
            // 23238 - Stygian Buckler
            item = GetItem(23238, "Stygian Buckler");
            item.OnEquip = (sim) =>
            {
                //When struck has a 15% chance of reducing the attacker's movement speed by 50% for 5 secs. &nbsp;Chance to fizzle against targets over level 60.
                throw new NotImplementedException();
            };
            // 19342 - Venomous Totem
            item = GetItem(19342, "Venomous Totem");
            item.OnUse = (sim) =>
            {
                //Use: Increases the damage dealt by Instant Poison by 65 and the periodic damage dealt by Deadly Poison by 22 for 20 sec. (5 Min Cooldown)
                throw new NotImplementedException();
            };
            // 35184 - Primal-Attuned Goggles
            item = GetItem(35184, "Primal-Attuned Goggles");
            item.OnEquip = (sim) =>
            {
                //Shows the location of all nearby gas clouds on the minimap.
                //Slightly increases your stealth detection.
                throw new NotImplementedException();
            };
            item.OnUse = (sim) =>
            {
                //Use: Allows you to look far into the distance. (3 Sec Cooldown)
                throw new NotImplementedException();
            };
            // 34471 - Vial of the Sunwell
            item = GetItem(34471, "Vial of the Sunwell");
            item.OnEquip = (sim) =>
            {
                //Collects 100 Holy Energy from healing spells you cast. &nbsp;Cannot collect more than 2000 Holy Energy.
                throw new NotImplementedException();
            };
            item.OnUse = (sim) =>
            {
                //Use: Release all accumulated Holy Energy to instantly heal current friendly target by the amount of Holy Energy accumulated. (2 Min Cooldown)
                throw new NotImplementedException();
            };
            // 30449 - Void Star Talisman
            item = GetItem(30449, "Void Star Talisman");
            item.OnEquip = (sim) =>
            {
                //Increases your pet's resistances by 130 and increases your spell damage by up to 48.
                throw new NotImplementedException();
            };
            // 16471 - Marshal's Lamellar Gloves
            item = GetItem(16471, "Marshal's Lamellar Gloves");
            item.OnEquip = (sim) =>
            {
                //Increases the Holy damage bonus of your Judgement of the Crusader by 20.
                throw new NotImplementedException();
            };
            // 23762 - Ultra-Spectropic Detection Goggles
            item = GetItem(23762, "Ultra-Spectropic Detection Goggles");
            item.OnEquip = (sim) =>
            {
                //Shows the location of all nearby gas clouds on the minimap.
                throw new NotImplementedException();
            };
            item.OnUse = (sim) =>
            {
                //Use: Allows you to look far into the distance.
                throw new NotImplementedException();
            };
            // 16484 - Marshal's Plate Gauntlets
            item = GetItem(16484, "Marshal's Plate Gauntlets");
            item.OnEquip = (sim) =>
            {
                //Hamstring Rage cost reduced by 3.
                throw new NotImplementedException();
            };
            // 23027 - Warmth of Forgiveness
            item = GetItem(23027, "Warmth of Forgiveness");
            item.OnUse = (sim) =>
            {
                //Use: Restores 500 mana. (3 Min Cooldown)
                throw new NotImplementedException();
            };
            // 7284 - Red Whelp Gloves
            item = GetItem(7284, "Red Whelp Gloves");
            item.OnEquip = (sim) =>
            {
                //5% chance of dealing 15 to 25 Fire damage on a successful melee attack.
                throw new NotImplementedException();
            };
            // 31993 - Merciless Gladiator's Lamellar Gauntlets
            item = GetItem(31993, "Merciless Gladiator's Lamellar Gauntlets");
            item.OnEquip = (sim) =>
            {
                //Increases the critical effect chance of your Flash of Light by 2%.
                throw new NotImplementedException();
            };
            // 27828 - Warp-Scarab Brooch
            item = GetItem(27828, "Warp-Scarab Brooch");
            item.OnUse = (sim) =>
            {
                //Use: Increases healing done by spells and effects by up to 282 and damage done by spells by up to 94 for 20 sec. (2 Min Cooldown)
                throw new NotImplementedException();
            };
            // 32021 - Merciless Gladiator's Ornamented Gloves
            item = GetItem(32021, "Merciless Gladiator's Ornamented Gloves");
            item.OnEquip = (sim) =>
            {
                //Increases the critical effect chance of your Flash of Light by 2%.
                throw new NotImplementedException();
            };
            // 30450 - Warp-Spring Coil
            item = GetItem(30450, "Warp-Spring Coil");
            item.OnEquip = (sim) =>
            {
                //Your special attacks have a chance to give you 1000 armor penetration for 15 sec.
                throw new NotImplementedException();
            };
            // 29301 - Band of the Eternal Champion
            item = GetItem(29301, "Band of the Eternal Champion");
            item.OnEquip = (sim) =>
            {
                //Chance on hit to increase your attack power by 160 for 10 seconds.
                throw new NotImplementedException();
            };
            // 30487 - Merciless Gladiator's Plate Gauntlets
            item = GetItem(30487, "Merciless Gladiator's Plate Gauntlets");
            item.OnEquip = (sim) =>
            {
                //Hamstring Rage cost reduced by 3.
                throw new NotImplementedException();
            };
            // 19955 - Wushoolay's Charm of Nature
            item = GetItem(19955, "Wushoolay's Charm of Nature");
            item.OnUse = (sim) =>
            {
                //Use: Grants 70 spell haste rating, and reduces the mana cost of Rejuvenation, Healing Touch, Regrowth, and Tranquility by 5% for 15 sec. (3 Min Cooldown)
                throw new NotImplementedException();
            };
            // 29297 - Band of the Eternal Defender
            item = GetItem(29297, "Band of the Eternal Defender");
            item.OnEquip = (sim) =>
            {
                //When struck in combat has a chance of increasing your armor by 800 for 10 sec.
                throw new NotImplementedException();
            };
            // 19956 - Wushoolay's Charm of Spirits
            item = GetItem(19956, "Wushoolay's Charm of Spirits");
            item.OnUse = (sim) =>
            {
                //Use: Increases the damage dealt by your Lightning Shield spell by 305 for 20 sec. (3 Min Cooldown)
                throw new NotImplementedException();
            };
            // 32040 - Merciless Gladiator's Scaled Gauntlets
            item = GetItem(32040, "Merciless Gladiator's Scaled Gauntlets");
            item.OnEquip = (sim) =>
            {
                //Increases the Holy damage bonus of your Judgement of the Crusader by 20.
                throw new NotImplementedException();
            };
            // 29309 - Band of the Eternal Restorer
            item = GetItem(29309, "Band of the Eternal Restorer");
            item.OnEquip = (sim) =>
            {
                //Your healing and damage spells have a chance to increase your healing by up to 175 and damage by up to 59 for 10 secs.
                throw new NotImplementedException();
            };
            // 29179 - Xi'ri's Gift
            item = GetItem(29179, "Xi'ri's Gift");
            item.OnUse = (sim) =>
            {
                //Use: Increases spell damage by up to 150 and healing by up to 280 for 15 sec. (1 Min, 30 Sec Cooldown)
                throw new NotImplementedException();
            };
            // 29305 - Band of the Eternal Sage
            item = GetItem(29305, "Band of the Eternal Sage");
            item.OnEquip = (sim) =>
            {
                //Your offensive spells have a chance on hit to increase your spell damage by 95 for 10 secs.
                throw new NotImplementedException();
            };
            // 19948 - Zandalarian Hero Badge
            item = GetItem(19948, "Zandalarian Hero Badge");
            item.OnUse = (sim) =>
            {
                //Use: Increases your armor by 2000 and defense rating by 50 for 20 sec. Every time you take melee or ranged damage, this bonus is reduced by 200 armor and 5 defense rating. (2 Min Cooldown)
                throw new NotImplementedException();
            };
            // 14154 - Truefaith Vestments
            item = GetItem(14154, "Truefaith Vestments");
            item.OnEquip = (sim) =>
            {
                //Reduces the cooldown of your Fade ability by 2 sec.
                throw new NotImplementedException();
            };
            // 19950 - Zandalarian Hero Charm
            item = GetItem(19950, "Zandalarian Hero Charm");
            item.OnUse = (sim) =>
            {
                //Use: Increases your spell damage by up to 204 and your healing by up to 408 for 20 sec. Every time you cast a spell, the bonus is reduced by 17 spell damage and 34 healing. (2 Min Cooldown)
                throw new NotImplementedException();
            };
            // 10036 - Tuxedo Jacket
            item = GetItem(10036, "Tuxedo Jacket");
            item.OnEquip = (sim) =>
            {
                //Impress others with your fashion sense.
                throw new NotImplementedException();
            };
            // 1204 - The Green Tower
            item = GetItem(1204, "The Green Tower");
            item.OnEquip = (sim) =>
            {
                //When struck in combat has a 1% chance of raising a thorny shield that inflicts 3 Nature damage to attackers when hit and increases Nature resistance by 50 for 30 sec.
                throw new NotImplementedException();
            };
            // 19949 - Zandalarian Hero Medallion
            item = GetItem(19949, "Zandalarian Hero Medallion");
            item.OnUse = (sim) =>
            {
                //Use: Increases your melee and ranged damage by 40 for 20 sec. Every time you hit a target, this bonus is reduced by 2. (2 Min Cooldown)
                throw new NotImplementedException();
            };
            // 31107 - Shamanistic Helmet of Second Sight
            item = GetItem(31107, "Shamanistic Helmet of Second Sight");
            item.OnEquip = (sim) =>
            {
                //Allows the bearer to see into the ghost world while in Shadowmoon Valley.
                throw new NotImplementedException();
            };
            // 9458 - Thermaplugg's Central Core
            item = GetItem(9458, "Thermaplugg's Central Core");
            item.OnEquip = (sim) =>
            {
                //When struck in combat has a 5% chance of inflicting 35 to 65 Nature damage to the attacker.
                throw new NotImplementedException();
            };
            // 18326 - Razor Gauntlets
            item = GetItem(18326, "Razor Gauntlets");
            item.OnEquip = (sim) =>
            {
                //When struck in combat inflicts 3 Arcane damage to the attacker.
                throw new NotImplementedException();
            };
            // 33820 - Weather-Beaten Fishing Hat
            item = GetItem(33820, "Weather-Beaten Fishing Hat");
            item.OnUse = (sim) =>
            {
                //Use: Attach a lure to your equipped fishing pole, increasing Fishing by 75 for 10 min. (10 Min Cooldown)
                throw new NotImplementedException();
            };
            // 31106 - Stalker's Helmet of Second Sight
            item = GetItem(31106, "Stalker's Helmet of Second Sight");
            item.OnEquip = (sim) =>
            {
                //Allows the bearer to see into the ghost world while in Shadowmoon Valley.
                throw new NotImplementedException();
            };
            // 35408 - Savage Plate Gauntlets
            item = GetItem(35408, "Savage Plate Gauntlets");
            item.OnEquip = (sim) =>
            {
                //Hamstring Rage cost reduced by 3.
                throw new NotImplementedException();
            };
            // 30847 - X-52 Rocket Helmet
            item = GetItem(30847, "X-52 Rocket Helmet");
            item.OnUse = (sim) =>
            {
                //Use: Launch yourself from Outland to the stars. &nbsp;For the safety of others, please clear the launching platform before use. (30 Min Cooldown)
                throw new NotImplementedException();
            };
            // 7747 - Vile Protector
            item = GetItem(7747, "Vile Protector");
            item.OnEquip = (sim) =>
            {
                //When struck in combat has a 1% chance of inflicting 105 to 175 Shadow damage to the attacker.
                throw new NotImplementedException();
            };
            // 32474 - Surestrike Goggles v2.0
            item = GetItem(32474, "Surestrike Goggles v2.0");
            item.OnEquip = (sim) =>
            {
                //Shows the location of all nearby gas clouds on the minimap.
                //Slightly increases your stealth detection.
                throw new NotImplementedException();
            };
            item.OnUse = (sim) =>
            {
                //Use: Allows you to look far into the distance. (3 Sec Cooldown)
                throw new NotImplementedException();
            };
            // 1979 - Wall of the Dead
            item = GetItem(1979, "Wall of the Dead");
            item.OnEquip = (sim) =>
            {
                //When struck in combat has a 3% chance to encase the caster in bone, increasing armor by 150 for 20 sec.
                throw new NotImplementedException();
            };
            // 34356 - Surestrike Goggles v3.0
            item = GetItem(34356, "Surestrike Goggles v3.0");
            item.OnEquip = (sim) =>
            {
                //Shows the location of all nearby gas clouds on the minimap.
                //Slightly increases your stealth detection.
                throw new NotImplementedException();
            };
            item.OnUse = (sim) =>
            {
                //Use: Allows you to look far into the distance. (3 Sec Cooldown)
                throw new NotImplementedException();
            };
            // 23533 - Steelgrip Gauntlets
            item = GetItem(23533, "Steelgrip Gauntlets");
            item.OnEquip = (sim) =>
            {
                //Disarm duration reduced by 50%.
                throw new NotImplementedException();
            };
            // 12639 - Stronghold Gauntlets
            item = GetItem(12639, "Stronghold Gauntlets");
            item.OnEquip = (sim) =>
            {
                //Disarm duration reduced by 50%.
                throw new NotImplementedException();
            };
            // 22196 - Thick Obsidian Breastplate
            item = GetItem(22196, "Thick Obsidian Breastplate");
            item.OnEquip = (sim) =>
            {
                //When struck by a non-periodic damage spell you have a 30% chance of getting a 6 sec spell shield that absorbs 300 to 500 of that school of damage.
                throw new NotImplementedException();
            };
            // 30804 - Bronze Band of Force
            item = GetItem(30804, "Bronze Band of Force");
            item.OnEquip = (sim) =>
            {
                //Increases damage and healing done by magical spells and effects slightly.
                throw new NotImplementedException();
            };
            // 7939 - Truesilver Breastplate
            item = GetItem(7939, "Truesilver Breastplate");
            item.OnEquip = (sim) =>
            {
                //When struck in combat has a 3% chance to heal you for 60 to 100.
                throw new NotImplementedException();
            };
            // 33696 - Vengeful Gladiator's Lamellar Gauntlets
            item = GetItem(33696, "Vengeful Gladiator's Lamellar Gauntlets");
            item.OnEquip = (sim) =>
            {
                //Increases the critical effect chance of your Flash of Light by 2%.
                throw new NotImplementedException();
            };
            // 33723 - Vengeful Gladiator's Ornamented Gloves
            item = GetItem(33723, "Vengeful Gladiator's Ornamented Gloves");
            item.OnEquip = (sim) =>
            {
                //Increases the critical effect chance of your Flash of Light by 2%.
                throw new NotImplementedException();
            };
            // 33729 - Vengeful Gladiator's Plate Gauntlets
            item = GetItem(33729, "Vengeful Gladiator's Plate Gauntlets");
            item.OnEquip = (sim) =>
            {
                //Hamstring Rage cost reduced by 3.
                throw new NotImplementedException();
            };
            // 33750 - Vengeful Gladiator's Scaled Gauntlets
            item = GetItem(33750, "Vengeful Gladiator's Scaled Gauntlets");
            item.OnEquip = (sim) =>
            {
                //Increases the damage dealt by your Crusader Strike ability by 5%.
                throw new NotImplementedException();
            };
            // 24548 - zzOLDbrokenitem
            item = GetItem(24548, "zzOLDbrokenitem");
            item.OnEquip = (sim) =>
            {
                //Hamstring Rage cost reduced by 3.
                throw new NotImplementedException();
            };
            // 33671 - Vengeful Gladiator's Dragonhide Gloves
            item = GetItem(33671, "Vengeful Gladiator's Dragonhide Gloves");
            item.OnEquip = (sim) =>
            {
                //Causes your Maim ability to interrupt spellcasting and prevent any spell in that school from being cast for 3 sec.
                throw new NotImplementedException();
            };
            // 33690 - Vengeful Gladiator's Kodohide Gloves
            item = GetItem(33690, "Vengeful Gladiator's Kodohide Gloves");
            item.OnEquip = (sim) =>
            {
                //Reduces the cast time of your Cyclone spell by 0.1 sec.
                throw new NotImplementedException();
            };
            // 33700 - Vengeful Gladiator's Leather Gloves
            item = GetItem(33700, "Vengeful Gladiator's Leather Gloves");
            item.OnEquip = (sim) =>
            {
                //Causes your Deadly Throw ability to interrupt spellcasting and prevent any spell in that school from being cast for 3 sec.
                throw new NotImplementedException();
            };
            // 33767 - Vengeful Gladiator's Wyrmhide Gloves
            item = GetItem(33767, "Vengeful Gladiator's Wyrmhide Gloves");
            item.OnEquip = (sim) =>
            {
                //Reduces the cast time of your Cyclone spell by 0.1 sec.
                throw new NotImplementedException();
            };
            // 35371 - Wyrmhide Gloves
            item = GetItem(35371, "Wyrmhide Gloves");
            item.OnEquip = (sim) =>
            {
                //Reduces the cast time of your Cyclone spell by 0.1 sec.
                throw new NotImplementedException();
            };
            // 34227 - Deadman's Hand
            item = GetItem(34227, "Deadman's Hand");
            item.OnEquip = (sim) =>
            {
                //When struck in combat has a chance of freezing the attacker in place for 3 sec.
                throw new NotImplementedException();
            };
            // 25827 - Muck-Covered Drape
            item = GetItem(25827, "Muck-Covered Drape");
            item.OnUse = (sim) =>
            {
                //Use: Reduces your threat to enemy targets within 30 yards, making them less likely to attack you. (5 Min Cooldown)
                throw new NotImplementedException();
            };
            // 15138 - Onyxia Scale Cloak
            item = GetItem(15138, "Onyxia Scale Cloak");
            item.OnEquip = (sim) =>
            {
                //Protects the wearer from being fully engulfed by Shadow Flame.
                throw new NotImplementedException();
            };
            // 10518 - Parachute Cloak
            item = GetItem(10518, "Parachute Cloak");
            item.OnUse = (sim) =>
            {
                //Use: Reduces your fall speed for 10 sec. (30 Sec Cooldown)
                throw new NotImplementedException();
            };
            // 942 - Freezing Band
            item = GetItem(942, "Freezing Band");
            item.OnEquip = (sim) =>
            {
                //When struck in combat has a 1% chance of inflicting 50 Frost damage to the attacker and freezing them for 5 sec.
                throw new NotImplementedException();
            };
            // 33994 - Kjordan's Test Ring
            item = GetItem(33994, "Kjordan's Test Ring");
            item.OnEquip = (sim) =>
            {
                //Increases damage done by magical spells and effects by up to 606.
                throw new NotImplementedException();
            };
            // 13143 - Mark of the Dragon Lord
            item = GetItem(13143, "Mark of the Dragon Lord");
            item.OnUse = (sim) =>
            {
                //Use: A protective mana shield surrounds the caster absorbing 500 damage. While the shield holds, increases mana regeneration by 22 every 5 sec for 30 min. (30 Min Cooldown)
                throw new NotImplementedException();
            };
            // 32539 - Skyguard's Drape
            item = GetItem(32539, "Skyguard's Drape");
            item.OnUse = (sim) =>
            {
                //Use: Reduces your fall speed for 10 sec. (30 Min Cooldown)
                throw new NotImplementedException();
            };
            // 32538 - Skywitch's Drape
            item = GetItem(32538, "Skywitch's Drape");
            item.OnUse = (sim) =>
            {
                //Use: Reduces your fall speed for 10 sec. (30 Min Cooldown)
                throw new NotImplementedException();
            };
            // 18345 - Murmuring Ring
            item = GetItem(18345, "Murmuring Ring");
            item.OnEquip = (sim) =>
            {
                //Reduces the duration of any Silence or Interrupt effects used against the wearer by 10%. This effect does not stack with other similar effects.
                throw new NotImplementedException();
            };
            // 11669 - Naglering
            item = GetItem(11669, "Naglering");
            item.OnEquip = (sim) =>
            {
                //When struck in combat inflicts 3 Arcane damage to the attacker.
                throw new NotImplementedException();
            };
            // 23717 - Pitted Gold Band
            item = GetItem(23717, "Pitted Gold Band");
            item.OnEquip = (sim) =>
            {
                //Increases your Mojo.
                throw new NotImplementedException();
            };
            // 34776 - QA Test Ring +100% Stun Resist
            item = GetItem(34776, "QA Test Ring +100% Stun Resist");
            item.OnEquip = (sim) =>
            {
                //Increases your chance to resist Stun effects by 100%.
                throw new NotImplementedException();
            };
            // 1447 - Ring of Saviors
            item = GetItem(1447, "Ring of Saviors");
            item.OnUse = (sim) =>
            {
                //Use: Increases armor by 300 for 10 sec. (30 Min Cooldown)
                throw new NotImplementedException();
            };
            // 20451 - Twilight Cultist Ring of Lordship
            item = GetItem(20451, "Twilight Cultist Ring of Lordship");
            item.OnEquip = (sim) =>
            {
                //When worn with the Twilight Trappings Set and the Medallion of Station, allows access to a Greater Wind Stone.
                throw new NotImplementedException();
            };
            // 21190 - Wrath of Cenarius
            item = GetItem(21190, "Wrath of Cenarius");
            item.OnEquip = (sim) =>
            {
                //Gives a chance when your harmful spells land to increase the damage of your spells and effects by 132 for 10 sec.
                throw new NotImplementedException();
            };
            // 19910 - Arlokk's Grasp
            item = GetItem(19910, "Arlokk's Grasp");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Sends a shadowy bolt at the enemy causing 55 to 85 Shadow damage.
                throw new NotImplementedException();
            };
            // 12795 - Blood Talon
            item = GetItem(12795, "Blood Talon");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Wounds the target causing them to bleed for 100 damage over 30 sec.
                throw new NotImplementedException();
            };
            // 11744 - Bloodfist
            item = GetItem(11744, "Bloodfist");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Wounds the target for 20 damage.
                throw new NotImplementedException();
            };
            // 19852 - Ancient Hakkari Manslayer
            item = GetItem(19852, "Ancient Hakkari Manslayer");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Steals 48 to 54 life from target enemy.
                throw new NotImplementedException();
            };
            // 12790 - Arcanite Champion
            item = GetItem(12790, "Arcanite Champion");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Heal self for 270 to 450 and increases Strength by 120 for 30 sec.
                throw new NotImplementedException();
            };
            // 12798 - Annihilator
            item = GetItem(12798, "Annihilator");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Reduces an enemy's armor by 200. &nbsp;Stacks up to 3 times.
                throw new NotImplementedException();
            };
            // 22736 - Andonisus, Reaper of Souls
            item = GetItem(22736, "Andonisus, Reaper of Souls");
            item.OnEquip = (sim) =>
            {
                //Increases melee and ranged attack power by 600.
                throw new NotImplementedException();
            };
            // 12583 - Blackhand Doomsaw
            item = GetItem(12583, "Blackhand Doomsaw");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Wounds the target for 324 to 540 damage.
                throw new NotImplementedException();
            };
            // 2000 - Archeus
            item = GetItem(2000, "Archeus");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Blasts a target for 85 Arcane damage.
                throw new NotImplementedException();
            };
            // 7959 - Blight
            item = GetItem(7959, "Blight");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Diseases a target for 50 Nature damage and an additional 180 damage over 1 min.
                throw new NotImplementedException();
            };
            // 811 - Axe of the Deep Woods
            item = GetItem(811, "Axe of the Deep Woods");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Blasts a target for 90 to 126 Nature damage.
                throw new NotImplementedException();
            };
            // 13057 - Bloodpike
            item = GetItem(13057, "Bloodpike");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Wounds the target causing them to bleed for 110 damage over 30 sec.
                throw new NotImplementedException();
            };
            // 13246 - Argent Avenger
            item = GetItem(13246, "Argent Avenger");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Increases attack power against Undead by 200 for 10 sec.
                throw new NotImplementedException();
            };
            // 14541 - Barovian Family Sword
            item = GetItem(14541, "Barovian Family Sword");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Deals 30 Shadow damage every 3 sec for 15 sec.
                throw new NotImplementedException();
            };
            // 32336 - Black Bow of the Betrayer
            item = GetItem(32336, "Black Bow of the Betrayer");
            item.OnEquip = (sim) =>
            {
                //On successful ranged attack gain 8 mana and if possible drain 8 mana from the target.
                throw new NotImplementedException();
            };
            // 17738 - Claw of Celebras
            item = GetItem(17738, "Claw of Celebras");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Poisons target for 9 Nature damage every 2 sec for 20 sec.
                throw new NotImplementedException();
            };
            // 13148 - Chillpike
            item = GetItem(13148, "Chillpike");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Blasts a target for 160 to 250 Frost damage.
                throw new NotImplementedException();
            };
            // 3278 - Aura Proc Damage Sword
            item = GetItem(3278, "Aura Proc Damage Sword");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Surrounds you with electricity, dealing 15 Nature damage to any who strike you for 15 sec.
                throw new NotImplementedException();
            };
            // 3194 - Black Malice
            item = GetItem(3194, "Black Malice");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Sends a shadowy bolt at the enemy causing 55 to 85 Shadow damage.
                throw new NotImplementedException();
            };
            // 30418 - Darkspear (Purple Glow)
            item = GetItem(30418, "Darkspear (Purple Glow)");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Party members have a chance to increase their critical strike rating by 56. &nbsp;Lasts for 20 sec.
                throw new NotImplementedException();
            };
            // 12802 - Darkspear
            item = GetItem(12802, "Darkspear");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Party members have a chance to increase their critical strike rating by 56. &nbsp;Lasts for 20 sec.
                throw new NotImplementedException();
            };
            // 2825 - Bow of Searing Arrows
            item = GetItem(2825, "Bow of Searing Arrows");
            item.OnEquip = (sim) =>
            {
                //Chance to strike your ranged target with a Searing Arrow for 18 to 26 Fire damage.
                throw new NotImplementedException();
            };
            // 26783 - 72 TEST Green Spell Dagger
            item = GetItem(26783, "72 TEST Green Spell Dagger");
            item.OnUse = (sim) =>
            {
                //Use: Increases damage and healing done by magical spells and effects by up to 95.
                throw new NotImplementedException();
            };
            // 9475 - Diabolic Skiver
            item = GetItem(9475, "Diabolic Skiver");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Delivers a fatal wound for 160 to 180 damage.
                throw new NotImplementedException();
            };
            // 12592 - Blackblade of Shahram
            item = GetItem(12592, "Blackblade of Shahram");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Summons the infernal spirit of Shahram.
                throw new NotImplementedException();
            };
            // 14555 - Alcor's Sunrazor
            item = GetItem(14555, "Alcor's Sunrazor");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Blasts a target for 75 to 105 Fire damage.
                throw new NotImplementedException();
            };
            // 6738 - Bleeding Crescent
            item = GetItem(6738, "Bleeding Crescent");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Wounds the target causing them to bleed for 45 damage over 30 sec.
                throw new NotImplementedException();
            };
            // 11809 - Flame Wrath
            item = GetItem(11809, "Flame Wrath");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Envelops the caster with a Fire shield for 15 sec and shoots a ring of fire dealing 130 to 170 damage to all nearby enemies.
                throw new NotImplementedException();
            };
            // 12769 - Bleakwood Hew
            item = GetItem(12769, "Bleakwood Hew");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Enemy is inflicted with the Bleakwood Curse that reduces their magic resistances by 25. &nbsp;Can be applied up to 3 times.
                throw new NotImplementedException();
            };
            // 18202 - Eskhandar's Left Claw
            item = GetItem(18202, "Eskhandar's Left Claw");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Slows enemy's movement by 60% and causes them to bleed for 150 damage over 30 sec.
                throw new NotImplementedException();
            };
            // 18203 - Eskhandar's Right Claw
            item = GetItem(18203, "Eskhandar's Right Claw");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Increases your haste rating by 300 for 5 sec.
                throw new NotImplementedException();
            };
            // 28774 - Glaive of the Pit
            item = GetItem(28774, "Glaive of the Pit");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Steals 285 to 315 life from target enemy.
                throw new NotImplementedException();
            };
            // 7753 - Bloodspiller
            item = GetItem(7753, "Bloodspiller");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Wounds the target causing them to bleed for 130 damage over 30 sec.
                throw new NotImplementedException();
            };
            // 17076 - Bonereaver's Edge
            item = GetItem(17076, "Bonereaver's Edge");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Your attacks ignore 700 of your enemies' armor for 10 sec. This effect stacks up to 3 times.
                throw new NotImplementedException();
            };
            // 16004 - Dark Iron Rifle
            item = GetItem(16004, "Dark Iron Rifle");
            item.OnEquip = (sim) =>
            {
                //Chance to strike your ranged target with Shadow Shot for 18 to 26 Shadow damage.
                throw new NotImplementedException();
            };
            // 18671 - Baron Charr's Sceptre
            item = GetItem(18671, "Baron Charr's Sceptre");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Blasts a target for 35 Fire damage.
                throw new NotImplementedException();
            };
            // 13399 - Gargoyle Shredder Talons
            item = GetItem(13399, "Gargoyle Shredder Talons");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Wounds the target causing them to bleed for 110 damage over 30 sec.
                throw new NotImplementedException();
            };
            // 13054 - Grim Reaper
            item = GetItem(13054, "Grim Reaper");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Wounds the target for 130 damage.
                throw new NotImplementedException();
            };
            // 13204 - Bashguuder
            item = GetItem(13204, "Bashguuder");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Punctures target's armor lowering it by 200. Can be applied up to 3 times.
                throw new NotImplementedException();
            };
            // 1263 - Brain Hacker
            item = GetItem(1263, "Brain Hacker");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Wounds the target for 200 to 300 damage and lowers Intellect of target by 25 for 30 sec.
                throw new NotImplementedException();
            };
            // 7730 - Cobalt Crusher
            item = GetItem(7730, "Cobalt Crusher");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Blasts a target for 110 to 120 Frost damage.
                throw new NotImplementedException();
            };
            // 13040 - Heartseeking Crossbow
            item = GetItem(13040, "Heartseeking Crossbow");
            item.OnEquip = (sim) =>
            {
                //Chance to strike your ranged target with a Shadow Bolt for 13 to 19 Shadow damage.
                throw new NotImplementedException();
            };
            // 8223 - Blade of the Basilisk
            item = GetItem(8223, "Blade of the Basilisk");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Increases defense rating by 50 for 5 sec.
                throw new NotImplementedException();
            };
            // 12791 - Barman Shanker
            item = GetItem(12791, "Barman Shanker");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Wounds the target causing them to bleed for 100 damage over 30 sec.
                throw new NotImplementedException();
            };
            // 18609 - Anathema
            item = GetItem(18609, "Anathema");
            item.OnUse = (sim) =>
            {
                //Use: Calls forth Benediction. (30 Min Cooldown)
                throw new NotImplementedException();
            };
            // 31323 - Don Santos' Famous Hunting Rifle
            item = GetItem(31323, "Don Santos' Famous Hunting Rifle");
            item.OnEquip = (sim) =>
            {
                //Your ranged attacks have a chance to increase your attack power by 250 for 10 sec.
                throw new NotImplementedException();
            };
            // 19874 - Halberd of Smiting
            item = GetItem(19874, "Halberd of Smiting");
            item.OnEquip = (sim) =>
            {
                //Chance to decapitate the target on a melee swing, causing 452 to 676 damage.
                throw new NotImplementedException();
            };
            // 10803 - Blade of the Wretched
            item = GetItem(10803, "Blade of the Wretched");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Corrupts the target, causing 90 Shadow damage over 3 sec.
                throw new NotImplementedException();
            };
            // 31336 - Blade of Wizardry
            item = GetItem(31336, "Blade of Wizardry");
            item.OnEquip = (sim) =>
            {
                //Your harmful spells have a chance to increase your spell haste rating by 280 for 6 secs.
                throw new NotImplementedException();
            };
            // 6904 - Bite of Serra'kis
            item = GetItem(6904, "Bite of Serra'kis");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Poisons target for 4 Nature damage every 2 sec for 20 sec.
                throw new NotImplementedException();
            };
            // 2299 - Burning War Axe
            item = GetItem(2299, "Burning War Axe");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Hurls a fiery ball that causes 86 to 110 Fire damage and an additional 18 damage over 6 sec.
                throw new NotImplementedException();
            };
            // 2099 - Dwarven Hand Cannon
            item = GetItem(2099, "Dwarven Hand Cannon");
            item.OnEquip = (sim) =>
            {
                //Chance to strike your ranged target with a Flaming Cannonball for 33 to 49 Fire damage.
                throw new NotImplementedException();
            };
            // 19166 - Black Amnesty
            item = GetItem(19166, "Black Amnesty");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Reduce your threat to the current target making them less likely to attack you.
                throw new NotImplementedException();
            };
            // 11608 - Dark Iron Pulverizer
            item = GetItem(11608, "Dark Iron Pulverizer");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Stuns target for 8 sec.
                throw new NotImplementedException();
            };
            // 22631 - Atiesh, Greatstaff of the Guardian
            item = GetItem(22631, "Atiesh, Greatstaff of the Guardian");
            item.OnEquip = (sim) =>
            {
                //Increases healing done by magical spells and effects of all party members within 30 yards by up to 62.
                //Increases your spell damage by up to 120 and your healing by up to 300.
                throw new NotImplementedException();
            };
            item.OnUse = (sim) =>
            {
                //Use: Creates a portal, teleporting group members that use it to Karazhan. (1 Min Cooldown)
                throw new NotImplementedException();
            };
            // 27901 - Blackout Truncheon
            item = GetItem(27901, "Blackout Truncheon");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Increases your haste rating by 132 for 10 sec.
                throw new NotImplementedException();
            };
            // 28441 - Deep Thunder
            item = GetItem(28441, "Deep Thunder");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Stuns target for 4 sec.
                throw new NotImplementedException();
            };
            // 6831 - Black Menace
            item = GetItem(6831, "Black Menace");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Sends a shadowy bolt at the enemy causing 30 Shadow damage.
                throw new NotImplementedException();
            };
            // 12777 - Blazing Rapier
            item = GetItem(12777, "Blazing Rapier");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Burns the enemy for 100 damage over 30 sec.
                throw new NotImplementedException();
            };
            // 31332 - Blinkstrike
            item = GetItem(31332, "Blinkstrike");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Grants an extra attack on your next swing.
                throw new NotImplementedException();
            };
            // 13198 - Hurd Smasher
            item = GetItem(13198, "Hurd Smasher");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Knocks target silly for 2 sec.
                throw new NotImplementedException();
            };
            // 4446 - Blackvenom Blade
            item = GetItem(4446, "Blackvenom Blade");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Poisons target for 5 Nature damage every 3 sec for 15 sec.
                throw new NotImplementedException();
            };
            // 2942 - Iron Knuckles
            item = GetItem(2942, "Iron Knuckles");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Pummel the target for 4 damage and interrupt the spell being cast for 5 sec.
                throw new NotImplementedException();
            };
            // 22691 - Corrupted Ashbringer
            item = GetItem(22691, "Corrupted Ashbringer");
            item.OnEquip = (sim) =>
            {
                //Inflicts the will of the Ashbringer upon the wielder.
                throw new NotImplementedException();
            };
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Steals 185 to 215 life from target enemy.
                throw new NotImplementedException();
            };
            // 17780 - Blade of Eternal Darkness
            item = GetItem(17780, "Blade of Eternal Darkness");
            item.OnEquip = (sim) =>
            {
                //Chance on landing a direct damage spell to deal 100 Shadow damage and restore 100 mana to you.
                throw new NotImplementedException();
            };
            // 9511 - Bloodletter Scalpel
            item = GetItem(9511, "Bloodletter Scalpel");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Wounds the target for 60 to 70 damage.
                throw new NotImplementedException();
            };
            // 18608 - Benediction
            item = GetItem(18608, "Benediction");
            item.OnUse = (sim) =>
            {
                //Use: Calls forth Anathema. (30 Min Cooldown)
                throw new NotImplementedException();
            };
            // 9412 - Galgann's Fireblaster
            item = GetItem(9412, "Galgann's Fireblaster");
            item.OnEquip = (sim) =>
            {
                //Chance to strike your ranged target with a Fire Blast for 12 to 18 Fire damage.
                throw new NotImplementedException();
            };
            // 17073 - Earthshaker
            item = GetItem(17073, "Earthshaker");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Knocks down all nearby enemies for 3 sec.
                throw new NotImplementedException();
            };
            // 937 - Black Duskwood Staff
            item = GetItem(937, "Black Duskwood Staff");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Sends a shadowy bolt at the enemy causing 110 to 140 Shadow damage.
                throw new NotImplementedException();
            };
            // 809 - Bloodrazor
            item = GetItem(809, "Bloodrazor");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Wounds the target causing them to bleed for 120 damage over 30 sec.
                throw new NotImplementedException();
            };
            // 21134 - Dark Edge of Insanity
            item = GetItem(21134, "Dark Edge of Insanity");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Disorients the target, causing it to wander aimlessly for up to 3 sec.
                throw new NotImplementedException();
            };
            // 14487 - Bonechill Hammer
            item = GetItem(14487, "Bonechill Hammer");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Blasts a target for 90 Frost damage.
                throw new NotImplementedException();
            };
            // 11607 - Dark Iron Sunderer
            item = GetItem(11607, "Dark Iron Sunderer");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Reduces targets armor by 300 for 20 sec.
                throw new NotImplementedException();
            };
            // 31193 - Blade of Unquenched Thirst
            item = GetItem(31193, "Blade of Unquenched Thirst");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Steals 48 to 54 life from target enemy.
                throw new NotImplementedException();
            };
            // 30316 - Devastation
            item = GetItem(30316, "Devastation");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Increases your movement speed by 50%, and your melee attack speed by 20% for 30 sec.
                throw new NotImplementedException();
            };
            // 10627 - Bludgeon of the Grinning Dog
            item = GetItem(10627, "Bludgeon of the Grinning Dog");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Stuns target for 3 sec.
                throw new NotImplementedException();
            };
            // 10628 - Deathblow
            item = GetItem(10628, "Deathblow");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Delivers a fatal wound for 160 damage.
                throw new NotImplementedException();
            };
            // 13348 - Demonshear
            item = GetItem(13348, "Demonshear");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Sends a shadowy bolt at the enemy causing 150 Shadow damage and dealing 40 damage every 2 sec for 6 sec.
                throw new NotImplementedException();
            };
            // 19353 - Drake Talon Cleaver
            item = GetItem(19353, "Drake Talon Cleaver");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Delivers a fatal wound for 240 damage.
                throw new NotImplementedException();
            };
            // 17068 - Deathbringer
            item = GetItem(17068, "Deathbringer");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Sends a shadowy bolt at the enemy causing 110 to 140 Shadow damage.
                throw new NotImplementedException();
            };
            // 11803 - Force of Magma
            item = GetItem(11803, "Force of Magma");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Blasts a target for 150 Fire damage.
                throw new NotImplementedException();
            };
            // 28573 - Despair
            item = GetItem(28573, "Despair");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Attempts to impale the target, causing 600 damage.
                throw new NotImplementedException();
            };
            // 14531 - Frightskull Shaft
            item = GetItem(14531, "Frightskull Shaft");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Deals 8 Shadow damage every 2 sec for 30 sec and lowers their Strength for the duration of the disease.
                throw new NotImplementedException();
            };
            // 647 - Destiny
            item = GetItem(647, "Destiny");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Increases Strength by 200 for 10 sec.
                throw new NotImplementedException();
            };
            // 2824 - Hurricane
            item = GetItem(2824, "Hurricane");
            item.OnEquip = (sim) =>
            {
                //Chance to strike your target with a Frost Arrow for 31 to 45 Frost damage.
                throw new NotImplementedException();
            };
            // 12621 - Demonfork
            item = GetItem(12621, "Demonfork");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Transfers 10 health every 5 seconds from the target to the caster for 25 sec.
                throw new NotImplementedException();
            };
            // 9465 - Digmaster 5000
            item = GetItem(9465, "Digmaster 5000");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Punctures target's armor lowering it by 100.
                throw new NotImplementedException();
            };
            // 13053 - Doombringer
            item = GetItem(13053, "Doombringer");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Sends a shadowy bolt at the enemy causing 125 to 275 Shadow damage.
                throw new NotImplementedException();
            };
            // 5815 - Glacial Stone
            item = GetItem(5815, "Glacial Stone");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Blasts a target for 75 Frost damage.
                throw new NotImplementedException();
            };
            // 12463 - Drakefang Butcher
            item = GetItem(12463, "Drakefang Butcher");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Wounds the target causing them to bleed for 150 damage over 30 sec.
                throw new NotImplementedException();
            };
            // 2205 - Duskbringer
            item = GetItem(2205, "Duskbringer");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Sends a shadowy bolt at the enemy causing 60 to 100 Shadow damage.
                throw new NotImplementedException();
            };
            // 870 - Fiery War Axe
            item = GetItem(870, "Fiery War Axe");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Hurls a fiery ball that causes 155 to 197 Fire damage and an additional 24 damage over 6 sec.
                throw new NotImplementedException();
            };
            // 17704 - Edge of Winter
            item = GetItem(17704, "Edge of Winter");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Blasts a target for 30 Frost damage.
                throw new NotImplementedException();
            };
            // 17730 - Gatorbite Axe
            item = GetItem(17730, "Gatorbite Axe");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Wounds the target causing them to bleed for 230 damage over 30 sec.
                throw new NotImplementedException();
            };
            // 14145 - Cursed Felblade
            item = GetItem(14145, "Cursed Felblade");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Reduces target enemy's attack power by 25 for 30 sec.
                throw new NotImplementedException();
            };
            // 13983 - Gravestone War Axe
            item = GetItem(13983, "Gravestone War Axe");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Diseases target enemy for 55 Nature damage every 3 sec for 15 sec.
                throw new NotImplementedException();
            };
            // 871 - Flurry Axe
            item = GetItem(871, "Flurry Axe");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Grants 1 extra attack on your next swing.
                throw new NotImplementedException();
            };
            // 30317 - Cosmic Infuser
            item = GetItem(30317, "Cosmic Infuser");
            item.OnEquip = (sim) =>
            {
                //Friendly targets of the caster's heals gain an effect that reduces the damage taken by Fire and Shadow spells by 50% for 30 sec.
                throw new NotImplementedException();
            };
            // 32500 - Crystal Spire of Karabor
            item = GetItem(32500, "Crystal Spire of Karabor");
            item.OnEquip = (sim) =>
            {
                //If your target is below 50% health, your direct healing spells will cause your target to be healed for an additional 180 to 220 health.
                throw new NotImplementedException();
            };
            // 29348 - The Bladefist
            item = GetItem(29348, "The Bladefist");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Increases your haste rating by 180 for 10 sec.
                throw new NotImplementedException();
            };
            // 1726 - Poison-tipped Bone Spear
            item = GetItem(1726, "Poison-tipped Bone Spear");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Poisons target for 30 Nature damage every 6 sec for 30 sec.
                throw new NotImplementedException();
            };
            // 3854 - Frost Tiger Blade
            item = GetItem(3854, "Frost Tiger Blade");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Launches a bolt of frost at the enemy causing 20 to 30 Frost damage and slowing movement speed by 50% for 5 sec.
                throw new NotImplementedException();
            };
            // 11121 - Darkwater Talwar
            item = GetItem(11121, "Darkwater Talwar");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Sends a shadowy bolt at the enemy causing 25 Shadow damage.
                throw new NotImplementedException();
            };
            // 12796 - Hammer of the Titans
            item = GetItem(12796, "Hammer of the Titans");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Stuns target for 3 sec.
                throw new NotImplementedException();
            };
            // 2912 - Claw of the Shadowmancer
            item = GetItem(2912, "Claw of the Shadowmancer");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Sends a shadowy bolt at the enemy causing 35 Shadow damage.
                throw new NotImplementedException();
            };
            // 1387 - Ghoulfang
            item = GetItem(1387, "Ghoulfang");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Sends a shadowy bolt at the enemy causing 35 Shadow damage.
                throw new NotImplementedException();
            };
            // 869 - Dazzling Longsword
            item = GetItem(869, "Dazzling Longsword");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Decrease the armor of the target by 100 for 30 sec. &nbsp;While affected, the target cannot stealth or turn invisible.
                throw new NotImplementedException();
            };
            // 10761 - Coldrage Dagger
            item = GetItem(10761, "Coldrage Dagger");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Launches a bolt of frost at the enemy causing 20 to 30 Frost damage and slowing movement speed by 50% for 5 sec.
                throw new NotImplementedException();
            };
            // 17074 - Shadowstrike
            item = GetItem(17074, "Shadowstrike");
            item.OnUse = (sim) =>
            {
                //Use: Transforms Shadowstrike into Thunderstrike. (1 Min Cooldown)
                throw new NotImplementedException();
            };
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Steals 100 to 180 life from target enemy.
                throw new NotImplementedException();
            };
            // 10772 - Glutton's Cleaver
            item = GetItem(10772, "Glutton's Cleaver");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Wounds the target causing them to bleed for 75 damage over 30 sec.
                throw new NotImplementedException();
            };
            // 12243 - Smoldering Claw
            item = GetItem(12243, "Smoldering Claw");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Hurls a fiery ball that causes 135 Fire damage and an additional 15 damage over 6 sec.
                throw new NotImplementedException();
            };
            // 11603 - Vilerend Slicer
            item = GetItem(11603, "Vilerend Slicer");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Wounds the target for 75 damage.
                throw new NotImplementedException();
            };
            // 2291 - Kang the Decapitator
            item = GetItem(2291, "Kang the Decapitator");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Wounds the target causing them to bleed for 560 damage over 30 sec.
                throw new NotImplementedException();
            };
            // 23648 - Deprecated: Keanna's Will
            item = GetItem(23648, "Deprecated: Keanna's Will");
            item.OnUse = (sim) =>
            {
                //Use: Speak to the facet of Keanna that inhabits this weapon. (10 Min Cooldown)
                throw new NotImplementedException();
            };
            // 13016 - Killmaim
            item = GetItem(13016, "Killmaim");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Wounds the target causing them to bleed for 100 damage over 30 sec.
                throw new NotImplementedException();
            };
            // 28367 - Greatsword of Forlorn Visions
            item = GetItem(28367, "Greatsword of Forlorn Visions");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Protects the bearer against physical attacks, increasing Armor by +2750 for 10 sec.
                throw new NotImplementedException();
            };
            // 1481 - Grimclaw
            item = GetItem(1481, "Grimclaw");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Sends a shadowy bolt at the enemy causing 30 Shadow damage.
                throw new NotImplementedException();
            };
            // 28438 - Dragonmaw
            item = GetItem(28438, "Dragonmaw");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Increases your haste rating by 212 for 10 sec.
                throw new NotImplementedException();
            };
            // 1986 - Gutrender
            item = GetItem(1986, "Gutrender");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Wounds the target for 90 to 114 damage.
                throw new NotImplementedException();
            };
            // 28439 - Dragonstrike
            item = GetItem(28439, "Dragonstrike");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Increases your haste rating by 212 for 10 sec.
                throw new NotImplementedException();
            };
            // 28437 - Drakefist Hammer
            item = GetItem(28437, "Drakefist Hammer");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Increases your haste rating by 212 for 10 sec.
                throw new NotImplementedException();
            };
            // 10847 - Dragon's Call
            item = GetItem(10847, "Dragon's Call");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Calls forth an Emerald Dragon Whelp to protect you in battle for a short period of time.
                throw new NotImplementedException();
            };
            // 13060 - The Needler
            item = GetItem(13060, "The Needler");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Wounds the target for 75 damage.
                throw new NotImplementedException();
            };
            // 10698 - Enchanted Azsharite Felbane Staff
            item = GetItem(10698, "Enchanted Azsharite Felbane Staff");
            item.OnUse = (sim) =>
            {
                //Use: Weakens the servants of Razelikh the Defiler. Must be within close proximity of the target to activate.
                throw new NotImplementedException();
            };
            // 17223 - Thunderstrike
            item = GetItem(17223, "Thunderstrike");
            item.OnUse = (sim) =>
            {
                //Use: Transforms Thunderstrike into Shadowstrike. (1 Min Cooldown)
                throw new NotImplementedException();
            };
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Blasts up to 3 targets for 150 to 250 Nature damage. Each target after the first takes less damage.
                throw new NotImplementedException();
            };
            // 19170 - Ebon Hand
            item = GetItem(19170, "Ebon Hand");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Sends a shadowy bolt at the enemy causing 125 to 275 Shadow damage.
                throw new NotImplementedException();
            };
            // 14576 - Ebon Hilt of Marduk
            item = GetItem(14576, "Ebon Hilt of Marduk");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Corrupts the target, causing 210 damage over 3 sec.
                throw new NotImplementedException();
            };
            // 19918 - Jeklik's Crusher
            item = GetItem(19918, "Jeklik's Crusher");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Wounds the target for 200 to 220 damage.
                throw new NotImplementedException();
            };
            // 17112 - Empyrean Demolisher
            item = GetItem(17112, "Empyrean Demolisher");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Increases your haste rating by 212 for 10 sec.
                throw new NotImplementedException();
            };
            // 9446 - Electrocutioner Leg
            item = GetItem(9446, "Electrocutioner Leg");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Blasts a target for 10 to 20 Nature damage.
                throw new NotImplementedException();
            };
            // 13984 - Darrowspike
            item = GetItem(13984, "Darrowspike");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Blasts a target for 90 Frost damage.
                throw new NotImplementedException();
            };
            // 21679 - Kalimdor's Revenge
            item = GetItem(21679, "Kalimdor's Revenge");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Instantly lightning shocks the target for 239 to 277 Nature damage.
                throw new NotImplementedException();
            };
            // 23541 - Khorium Champion
            item = GetItem(23541, "Khorium Champion");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Heal self for 270 to 450 and increases Strength by 120 for 30 sec.
                throw new NotImplementedException();
            };
            // 9386 - Excavator's Brand
            item = GetItem(9386, "Excavator's Brand");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Hurls a fiery ball that causes 40 Fire damage and an additional 9 damage over 6 sec.
                throw new NotImplementedException();
            };
            // 20504 - Lightforged Blade
            item = GetItem(20504, "Lightforged Blade");
            item.OnEquip = (sim) =>
            {
                //Increases damage done by Holy spells and effects by up to 16.
                throw new NotImplementedException();
            };
            // 28428 - Lionheart Blade
            item = GetItem(28428, "Lionheart Blade");
            item.OnEquip = (sim) =>
            {
                //Increases your chance to resist Fear effects by 5%.
                throw new NotImplementedException();
            };
            // 13146 - Shell Launcher Shotgun
            item = GetItem(13146, "Shell Launcher Shotgun");
            item.OnEquip = (sim) =>
            {
                //Chance to strike your ranged target with a Flaming Shell for 18 to 26 Fire damage.
                throw new NotImplementedException();
            };
            // 28429 - Lionheart Champion
            item = GetItem(28429, "Lionheart Champion");
            item.OnEquip = (sim) =>
            {
                //Increases your chance to resist Fear effects by 5%.
                throw new NotImplementedException();
            };
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Increases Strength by 100 for 10 sec.
                throw new NotImplementedException();
            };
            // 12250 - Midnight Axe
            item = GetItem(12250, "Midnight Axe");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Sends a shadowy bolt at the enemy causing 30 Shadow damage.
                throw new NotImplementedException();
            };
            // 10696 - Enchanted Azsharite Felbane Sword
            item = GetItem(10696, "Enchanted Azsharite Felbane Sword");
            item.OnUse = (sim) =>
            {
                //Use: Weakens the servants of Razelikh the Defiler. Must be within close proximity of the target to activate.
                throw new NotImplementedException();
            };
            // 28430 - Lionheart Executioner
            item = GetItem(28430, "Lionheart Executioner");
            item.OnEquip = (sim) =>
            {
                //Increases your chance to resist Fear effects by 8%.
                throw new NotImplementedException();
            };
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Increases Strength by 100 for 10 sec.
                throw new NotImplementedException();
            };
            // 13393 - Malown's Slam
            item = GetItem(13393, "Malown's Slam");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Knocks target silly for 2 sec and increases Strength by 50 for 30 sec.
                throw new NotImplementedException();
            };
            // 9449 - Manual Crowd Pummeler
            item = GetItem(9449, "Manual Crowd Pummeler");
            item.OnUse = (sim) =>
            {
                //Use: Increases your haste rating by 500 for 30 sec.
                throw new NotImplementedException();
            };
            // 23911 - Deprecated: Keanna's Will
            item = GetItem(23911, "Deprecated: Keanna's Will");
            item.OnUse = (sim) =>
            {
                //Use: Speak to the facet of Keanna that inhabits this weapon. (10 Min Cooldown)
                throw new NotImplementedException();
            };
            // 17943 - Fist of Stone
            item = GetItem(17943, "Fist of Stone");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Restores 50 mana.
                throw new NotImplementedException();
            };
            // 10804 - Fist of the Damned
            item = GetItem(10804, "Fist of the Damned");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Steals 30 life from target enemy.
                throw new NotImplementedException();
            };
            // 17002 - Ichor Spitter
            item = GetItem(17002, "Ichor Spitter");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Poisons target for 8 Nature damage every 2 sec for 20 sec.
                throw new NotImplementedException();
            };
            // 30318 - Netherstrand Longbow
            item = GetItem(30318, "Netherstrand Longbow");
            item.OnEquip = (sim) =>
            {
                //Increases your ranged weapon critical strike damage bonus by 50%.
                throw new NotImplementedException();
            };
            item.OnUse = (sim) =>
            {
                //Use: Summons a bundle of Nether Spikes for use as ammo.
                throw new NotImplementedException();
            };
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Increases Physical damage taken by the target by 5%. Can be applied up to 5 times. Lasts 30 sec.
                throw new NotImplementedException();
            };
            // 37596 - Direbrew's Bottle DO NOT USE
            item = GetItem(37596, "Direbrew's Bottle DO NOT USE");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Break this on a monster.
                throw new NotImplementedException();
            };
            // 19968 - Fiery Retributer
            item = GetItem(19968, "Fiery Retributer");
            item.OnEquip = (sim) =>
            {
                //Adds 2 fire damage to your melee attacks.
                throw new NotImplementedException();
            };
            // 31082 - Monster - Mace, 2H Fathom-Lord Karathress
            item = GetItem(31082, "Monster - Mace, 2H Fathom-Lord Karathress");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Increases the critical strike rating of your next attack made within 4 seconds by 900.
                throw new NotImplementedException();
            };
            // 9419 - Galgann's Firehammer
            item = GetItem(9419, "Galgann's Firehammer");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Blasts a target for 80 to 112 Fire damage.
                throw new NotImplementedException();
            };
            // 997 - Fire Sword of Crippling
            item = GetItem(997, "Fire Sword of Crippling");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Cripples the target, reducing movement speed by 40%, increasing time between melee and ranged attacks by 45%. &nbsp;Lasts 20 sec.
                throw new NotImplementedException();
            };
            // 10797 - Firebreather
            item = GetItem(10797, "Firebreather");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Hurls a fiery ball that causes 70 Fire damage and an additional 9 damage over 6 sec.
                throw new NotImplementedException();
            };
            // 10567 - Quillshooter
            item = GetItem(10567, "Quillshooter");
            item.OnEquip = (sim) =>
            {
                //Chance to strike your ranged target with a Quill Shot for 66 to 98 Nature damage.
                throw new NotImplementedException();
            };
            // 19100 - Electrified Dagger
            item = GetItem(19100, "Electrified Dagger");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Blasts a target for 45 Nature damage.
                throw new NotImplementedException();
            };
            // 20578 - Emerald Dragonfang
            item = GetItem(20578, "Emerald Dragonfang");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Blasts the enemy with acid for 87 to 105 Nature damage.
                throw new NotImplementedException();
            };
            // 12797 - Frostguard
            item = GetItem(12797, "Frostguard");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Target's movement slowed by 30% and increasing the time between attacks by 25% for 5 sec.
                throw new NotImplementedException();
            };
            // 10697 - Enchanted Azsharite Felbane Dagger
            item = GetItem(10697, "Enchanted Azsharite Felbane Dagger");
            item.OnUse = (sim) =>
            {
                //Use: Weakens the servants of Razelikh the Defiler. Must be within close proximity of the target to activate.
                throw new NotImplementedException();
            };
            // 13937 - Headmaster's Charge
            item = GetItem(13937, "Headmaster's Charge");
            item.OnUse = (sim) =>
            {
                //Use: Gives 20 additional intellect to party members within 30 yards. (10 Min Cooldown)
                throw new NotImplementedException();
            };
            // 9452 - Hydrocane
            item = GetItem(9452, "Hydrocane");
            item.OnEquip = (sim) =>
            {
                //Allows underwater breathing.
                throw new NotImplementedException();
            };
            // 13218 - Fang of the Crystal Spider
            item = GetItem(13218, "Fang of the Crystal Spider");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Slows target enemy's casting speed and increases the time between melee and ranged attacks by 10% for 10 sec.
                throw new NotImplementedException();
            };
            // 9651 - Gryphon Rider's Stormhammer
            item = GetItem(9651, "Gryphon Rider's Stormhammer");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Blasts a target for 91 to 125 Nature damage.
                throw new NotImplementedException();
            };
            // 21856 - Neretzek, The Blood Drinker
            item = GetItem(21856, "Neretzek, The Blood Drinker");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Steals 141 to 163 life from target enemy.
                throw new NotImplementedException();
            };
            // 1318 - Night Reaver
            item = GetItem(1318, "Night Reaver");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Sends a shadowy bolt at the enemy causing 60 to 90 Shadow damage.
                throw new NotImplementedException();
            };
            // 19169 - Nightfall
            item = GetItem(19169, "Nightfall");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Spell damage taken by target increased by 15% for 5 sec. &nbsp;Chance to fail for targets above level 60.
                throw new NotImplementedException();
            };
            // 12590 - Felstriker
            item = GetItem(12590, "Felstriker");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: All attacks are guaranteed to land and will be critical strikes for the next 3 sec.
                throw new NotImplementedException();
            };
            // 15814 - Hameya's Slayer
            item = GetItem(15814, "Hameya's Slayer");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Wounds the target causing them to bleed for 80 damage over 30 sec.
                throw new NotImplementedException();
            };
            // 8190 - Hanzo Sword
            item = GetItem(8190, "Hanzo Sword");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Wounds the target for 75 damage.
                throw new NotImplementedException();
            };
            // 12709 - Finkle's Skinner
            item = GetItem(12709, "Finkle's Skinner");
            item.OnEquip = (sim) =>
            {
                //Skinning +10.
                throw new NotImplementedException();
            };
            // 9425 - Pendulum of Doom
            item = GetItem(9425, "Pendulum of Doom");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Delivers a fatal wound for 250 to 350 damage.
                throw new NotImplementedException();
            };
            // 17766 - Princess Theradras' Scepter
            item = GetItem(17766, "Princess Theradras' Scepter");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Wounds the target for 160 damage and lowers their armor by 100.
                throw new NotImplementedException();
            };
            // 10626 - Ragehammer
            item = GetItem(10626, "Ragehammer");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Increases damage done by 20 and haste rating by 50 for 15 sec.
                throw new NotImplementedException();
            };
            // 3336 - Flesh Piercer
            item = GetItem(3336, "Flesh Piercer");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Wounds the target causing them to bleed for 30 damage over 30 sec.
                throw new NotImplementedException();
            };
            // 810 - Hammer of the Northern Wind
            item = GetItem(810, "Hammer of the Northern Wind");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Launches a bolt of frost at the enemy causing 20 to 30 Frost damage and slowing movement speed by 50% for 5 sec.
                throw new NotImplementedException();
            };
            // 7717 - Ravager
            item = GetItem(7717, "Ravager");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: You attack all nearby enemies for 9 sec causing weapon damage plus an additional 5 every 3 sec.
                throw new NotImplementedException();
            };
            // 14024 - Frightalon
            item = GetItem(14024, "Frightalon");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Lowers all attributes of target by 10 for 1 min.
                throw new NotImplementedException();
            };
            // 9467 - Gahz'rilla Fang
            item = GetItem(9467, "Gahz'rilla Fang");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Surrounds you with electricity, dealing 15 Nature damage to any who strike you for 15 sec.
                throw new NotImplementedException();
            };
            // 2243 - Hand of Edward the Odd
            item = GetItem(2243, "Hand of Edward the Odd");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Increases Spell Haste Rating by 320.
                throw new NotImplementedException();
            };
            // 19099 - Glacial Blade
            item = GetItem(19099, "Glacial Blade");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Blasts a target for 45 Frost damage.
                throw new NotImplementedException();
            };
            // 20035 - Glacial Spike
            item = GetItem(20035, "Glacial Spike");
            item.OnEquip = (sim) =>
            {
                //Your Frostbolt spells have a 6% chance to restore 50 mana when cast.
                throw new NotImplementedException();
            };
            // 12969 - Seeping Willow
            item = GetItem(12969, "Seeping Willow");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Lowers all stats by 20 and deals 20 Nature damage every 3 sec to all enemies within an 8 yard radius of the caster for 30 sec.
                throw new NotImplementedException();
            };
            // 15418 - Shimmering Platinum Warhammer
            item = GetItem(15418, "Shimmering Platinum Warhammer");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Blasts a target for 180 to 250 Nature damage.
                throw new NotImplementedException();
            };
            // 34334 - Thori'dal, the Stars' Fury
            item = GetItem(34334, "Thori'dal, the Stars' Fury");
            item.OnEquip = (sim) =>
            {
                //Increases ranged attack speed by 15%. Does not stack with quiver or ammo pouch haste effects.
                //Thori'dal generates magical arrows when the bow string is drawn. Does not use ammo.
                throw new NotImplementedException();
            };
            // 11086 - Jang'thraze the Protector
            item = GetItem(11086, "Jang'thraze the Protector");
            item.OnUse = (sim) =>
            {
                //Use: Combines Jang'thraze and Sang'thraze to form the mighty sword, Sul'thraze.
                throw new NotImplementedException();
            };
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Shields the wielder from physical damage, absorbing 55 to 85 damage. Lasts 20 sec.
                throw new NotImplementedException();
            };
            // 31318 - Singing Crystal Axe
            item = GetItem(31318, "Singing Crystal Axe");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Increases your haste rating by 400 for 10 sec.
                throw new NotImplementedException();
            };
            // 17054 - Joonho's Mercy
            item = GetItem(17054, "Joonho's Mercy");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Blasts a target for 70 Arcane damage.
                throw new NotImplementedException();
            };
            // 6469 - Venomstrike
            item = GetItem(6469, "Venomstrike");
            item.OnEquip = (sim) =>
            {
                //Chance to strike your ranged target with a Venom Shot for 31 to 45 Nature damage.
                throw new NotImplementedException();
            };
            // 17753 - Verdant Keeper's Aim
            item = GetItem(17753, "Verdant Keeper's Aim");
            item.OnEquip = (sim) =>
            {
                //Chance to strike your ranged target with Keeper's Sting for 15 to 21 Nature damage.
                throw new NotImplementedException();
            };
            // 2164 - Gut Ripper
            item = GetItem(2164, "Gut Ripper");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Wounds the target for 95 to 121 damage.
                throw new NotImplementedException();
            };
            // 11684 - Ironfoe
            item = GetItem(11684, "Ironfoe");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Grants 2 extra attacks on your next swing.
                throw new NotImplementedException();
            };
            // 17071 - Gutgore Ripper
            item = GetItem(17071, "Gutgore Ripper");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Sends a shadowy bolt at the enemy causing 75 Shadow damage and lowering all stats by 25 for 30 sec.
                throw new NotImplementedException();
            };
            // 28442 - Stormherald
            item = GetItem(28442, "Stormherald");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Stuns target for 4 sec.
                throw new NotImplementedException();
            };
            // 5616 - Gutwrencher
            item = GetItem(5616, "Gutwrencher");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Wounds the target causing them to bleed for 80 damage over 30 sec.
                throw new NotImplementedException();
            };
            // 17182 - Sulfuras, Hand of Ragnaros
            item = GetItem(17182, "Sulfuras, Hand of Ragnaros");
            item.OnEquip = (sim) =>
            {
                //Deals 5 Fire damage to anyone who strikes you with a melee attack.
                throw new NotImplementedException();
            };
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Hurls a fiery ball that causes 273 to 333 Fire damage and an additional 75 damage over 10 sec.
                throw new NotImplementedException();
            };
            // 17193 - Sulfuron Hammer
            item = GetItem(17193, "Sulfuron Hammer");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Hurls a fiery ball that causes 83 to 101 Fire damage and an additional 16 damage over 8 sec.
                throw new NotImplementedException();
            };
            // 17104 - Spinal Reaper
            item = GetItem(17104, "Spinal Reaper");
            item.OnEquip = (sim) =>
            {
                //Restores 150 mana or 20 rage when you kill a target that gives experience; this effect cannot occur more than once every 10 seconds.
                throw new NotImplementedException();
            };
            // 2915 - Taran Icebreaker
            item = GetItem(2915, "Taran Icebreaker");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Hurls a fiery ball that causes 180 to 220 Fire damage and an additional 36 damage over 8 sec.
                throw new NotImplementedException();
            };
            // 29962 - Heartrazor
            item = GetItem(29962, "Heartrazor");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Increases attack power by 270 for 10 sec.
                throw new NotImplementedException();
            };
            // 12469 - Mutilator
            item = GetItem(12469, "Mutilator");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Increases damage done to target by physical attacks by 5 for 1 min. Stacks up to 5 times.
                throw new NotImplementedException();
            };
            // 31322 - The Hammer of Destiny
            item = GetItem(31322, "The Hammer of Destiny");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Restores 170 to 230 mana.
                throw new NotImplementedException();
            };
            // 9423 - The Jackhammer
            item = GetItem(9423, "The Jackhammer");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Increases your haste rating by 300 for 10 sec.
                throw new NotImplementedException();
            };
            // 11902 - Linken's Sword of Mastery
            item = GetItem(11902, "Linken's Sword of Mastery");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Blasts a target for 45 to 75 Nature damage.
                throw new NotImplementedException();
            };
            // 1982 - Nightblade
            item = GetItem(1982, "Nightblade");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Sends a shadowy bolt at the enemy causing 125 to 275 Shadow damage.
                throw new NotImplementedException();
            };
            // 12528 - The Judge's Gavel
            item = GetItem(12528, "The Judge's Gavel");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Stuns target for 3 sec.
                throw new NotImplementedException();
            };
            // 9486 - Supercharger Battle Axe
            item = GetItem(9486, "Supercharger Battle Axe");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Blasts a target for 80 to 100 Nature damage.
                throw new NotImplementedException();
            };
            // 11817 - Lord General's Sword
            item = GetItem(11817, "Lord General's Sword");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Increases attack power by 50 for 30 sec.
                throw new NotImplementedException();
            };
            // 13285 - The Blackrock Slicer
            item = GetItem(13285, "The Blackrock Slicer");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Wounds the target for 50 to 150 damage and deals an additional 6 damage every 1 sec for 25 sec.
                throw new NotImplementedException();
            };
            // 19323 - The Unstoppable Force
            item = GetItem(19323, "The Unstoppable Force");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Stuns target for 1 sec.
                throw new NotImplementedException();
            };
            // 11635 - Hookfang Shanker
            item = GetItem(11635, "Hookfang Shanker");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Corrosive acid that deals 7 Nature damage every 3 sec and lowers target's armor by 50 for 30 sec.
                throw new NotImplementedException();
            };
            // 6331 - Howling Blade
            item = GetItem(6331, "Howling Blade");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Reduces target's attack power by 30 for 30 sec.
                throw new NotImplementedException();
            };
            // 13505 - Runeblade of Baron Rivendare
            item = GetItem(13505, "Runeblade of Baron Rivendare");
            item.OnEquip = (sim) =>
            {
                //Increases movement speed and life regeneration rate.
                throw new NotImplementedException();
            };
            // 3822 - Runic Darkblade
            item = GetItem(3822, "Runic Darkblade");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Sends a shadowy bolt at the enemy causing 35 Shadow damage.
                throw new NotImplementedException();
            };
            // 12992 - Searing Blade
            item = GetItem(12992, "Searing Blade");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Hurls a fiery ball that causes 70 Fire damage and an additional 9 damage over 6 sec.
                throw new NotImplementedException();
            };
            // 30787 - Illidari-Bane Mageblade
            item = GetItem(30787, "Illidari-Bane Mageblade");
            item.OnEquip = (sim) =>
            {
                //Increases damage done to Demons by magical spells and effects by up to 185.
                throw new NotImplementedException();
            };
            // 30312 - Infinity Blade
            item = GetItem(30312, "Infinity Blade");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Increases magical damage taken by the target by 5%, and dispels Prince Kael'thas' Mind Control when a melee ability lands. Can be applied up to 5 times. Lasts 30 sec.
                throw new NotImplementedException();
            };
            // 12794 - Masterwork Stormhammer
            item = GetItem(12794, "Masterwork Stormhammer");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Blasts up to 3 targets for 105 to 145 Nature damage.
                throw new NotImplementedException();
            };
            // 5182 - Shiver Blade
            item = GetItem(5182, "Shiver Blade");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Blasts a target for 35 Frost damage.
                throw new NotImplementedException();
            };
            // 6660 - Julie's Dagger
            item = GetItem(6660, "Julie's Dagger");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Heals wielder of 78 damage over 12 sec.
                throw new NotImplementedException();
            };
            // 9478 - Ripsaw
            item = GetItem(9478, "Ripsaw");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Wounds the target for 75 damage.
                throw new NotImplementedException();
            };
            // 12582 - Keris of Zul'Serak
            item = GetItem(12582, "Keris of Zul'Serak");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Inflicts numbing pain that deals 10 Nature damage every 2 sec and increases time between target's attacks by 10% for 10 sec.
                throw new NotImplementedException();
            };
            // 13286 - Rivenspike
            item = GetItem(13286, "Rivenspike");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Punctures target's armor lowering it by 200. Can be applied up to 3 times.
                throw new NotImplementedException();
            };
            // 18410 - Sprinter's Sword
            item = GetItem(18410, "Sprinter's Sword");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Increases run speed by 30% for 10 sec.
                throw new NotImplementedException();
            };
            // 30090 - World Breaker
            item = GetItem(30090, "World Breaker");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Increases the critical strike rating of your next attack made within 4 seconds by 900.
                throw new NotImplementedException();
            };
            // 23221 - Misplaced Servo Arm
            item = GetItem(23221, "Misplaced Servo Arm");
            item.OnEquip = (sim) =>
            {
                //Chance to discharge electricity causing 100 to 150 Nature damage to your target.
                throw new NotImplementedException();
            };
            // 9418 - Stoneslayer
            item = GetItem(9418, "Stoneslayer");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Increases damage by 10 for 8 sec.
                throw new NotImplementedException();
            };
            // 6909 - Strike of the Hydra
            item = GetItem(6909, "Strike of the Hydra");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Corrosive acid that deals 7 Nature damage every 3 sec and lowers target's armor by 50 for 30 sec.
                throw new NotImplementedException();
            };
            // 5426 - Serpent's Kiss
            item = GetItem(5426, "Serpent's Kiss");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Poisons target for 7 Nature damage every 3 sec for 15 sec.
                throw new NotImplementedException();
            };
            // 9372 - Sul'thraze the Lasher
            item = GetItem(9372, "Sul'thraze the Lasher");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Strikes an enemy with the rage of Sul'thraze. Lowers target's strength by 15 and deals 90 to 210 Shadow damage with an additional 125 damage over 15 sec.
                throw new NotImplementedException();
            };
            // 9608 - Shoni's Disarming Tool
            item = GetItem(9608, "Shoni's Disarming Tool");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Disarm target's weapon for 5 sec.
                throw new NotImplementedException();
            };
            // 6220 - Meteor Shard
            item = GetItem(6220, "Meteor Shard");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Blasts a target for 35 Fire damage.
                throw new NotImplementedException();
            };
            // 13408 - Soul Breaker
            item = GetItem(13408, "Soul Breaker");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Target enemy loses 12 health and mana every 3 sec for 30 sec.
                throw new NotImplementedException();
            };
            // 19334 - The Untamed Blade
            item = GetItem(19334, "The Untamed Blade");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Increases Strength by 300 for 8 sec.
                throw new NotImplementedException();
            };
            // 934 - Stalvan's Reaper
            item = GetItem(934, "Stalvan's Reaper");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Lowers all attributes of target by 2 for 1 min.
                throw new NotImplementedException();
            };
            // 28164 - Tranquillien Flamberge
            item = GetItem(28164, "Tranquillien Flamberge");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Sends a shadowy bolt at the enemy causing 35 Shadow damage.
                throw new NotImplementedException();
            };
            // 32824 - Trashbringer
            item = GetItem(32824, "Trashbringer");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Blasts a target for 700 Fire damage.
                throw new NotImplementedException();
            };
            // 7960 - Truesilver Champion
            item = GetItem(7960, "Truesilver Champion");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Protects the caster with a holy shield.
                throw new NotImplementedException();
            };
            // 28767 - The Decapitator
            item = GetItem(28767, "The Decapitator");
            item.OnUse = (sim) =>
            {
                //Use: Hurls your axe in an attempt to decapitate your target causing 513 to 567 damage. (3 Min Cooldown)
                throw new NotImplementedException();
            };
            // 4090 - Mug O' Hurt
            item = GetItem(4090, "Mug O' Hurt");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Slows the target's movement by 50% for 10 sec.
                throw new NotImplementedException();
            };
            // 13051 - Witchfury
            item = GetItem(13051, "Witchfury");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Sends a shadowy bolt at the enemy causing 150 Shadow damage.
                throw new NotImplementedException();
            };
            // 17743 - Resurgence Rod
            item = GetItem(17743, "Resurgence Rod");
            item.OnEquip = (sim) =>
            {
                //Restores 5 health every 5 sec.
                throw new NotImplementedException();
            };
            // 9485 - Vibroblade
            item = GetItem(9485, "Vibroblade");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Punctures target's armor lowering it by 100.
                throw new NotImplementedException();
            };
            // 15853 - Windreaper
            item = GetItem(15853, "Windreaper");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Inflicts Nature damage every 2 sec for 20 sec.
                throw new NotImplementedException();
            };
            // 10623 - Winter's Bite
            item = GetItem(10623, "Winter's Bite");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Launches a bolt of frost at the enemy causing 20 to 30 Frost damage and slowing movement speed by 50% for 5 sec.
                throw new NotImplementedException();
            };
            // 11920 - Wraith Scythe
            item = GetItem(11920, "Wraith Scythe");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Steals 45 life from target enemy.
                throw new NotImplementedException();
            };
            // 29996 - Rod of the Sun King
            item = GetItem(29996, "Rod of the Sun King");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Chance on melee attack to gain 10 Energy or 5 Rage.
                throw new NotImplementedException();
            };
            // 4449 - Naraxis' Fang
            item = GetItem(4449, "Naraxis' Fang");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Poisons target for 6 Nature damage every 3 sec for 15 sec.
                throw new NotImplementedException();
            };
            // 12532 - Spire of the Stoneshaper
            item = GetItem(12532, "Spire of the Stoneshaper");
            item.OnUse = (sim) =>
            {
                //Use: Increases armor by 1000 for 10 sec but cannot cast spells or attack for the duration of the spell. (15 Min Cooldown)
                throw new NotImplementedException();
            };
            // 18816 - Perdition's Blade
            item = GetItem(18816, "Perdition's Blade");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Blasts a target for 40 to 56 Fire damage.
                throw new NotImplementedException();
            };
            // 7961 - Phantom Blade
            item = GetItem(7961, "Phantom Blade");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Decrease the armor of the target by 100 for 20 sec. &nbsp;While affected, the target cannot stealth or turn invisible.
                throw new NotImplementedException();
            };
            // 19908 - Sceptre of Smiting
            item = GetItem(19908, "Sceptre of Smiting");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Blasts the enemy with poison for 63 to 93 Nature damage.
                throw new NotImplementedException();
            };
            // 2263 - Phytoblade
            item = GetItem(2263, "Phytoblade");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Blasts a target for 35 Nature damage.
                throw new NotImplementedException();
            };
            // 12781 - Serenity
            item = GetItem(12781, "Serenity");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Dispels a magic effect on the current foe.
                throw new NotImplementedException();
            };
            // 1933 - Staff of Conjuring
            item = GetItem(1933, "Staff of Conjuring");
            item.OnUse = (sim) =>
            {
                //Use: Conjures food to eat. (5 Min Cooldown)
                throw new NotImplementedException();
            };
            // 30313 - Staff of Disintegration
            item = GetItem(30313, "Staff of Disintegration");
            item.OnUse = (sim) =>
            {
                //Use: Places a mental protection field on friendly targets within 30 yards, granting immunity to Stun, Silence, and Disorient effects.
                throw new NotImplementedException();
            };
            // 18348 - Quel'Serrar
            item = GetItem(18348, "Quel'Serrar");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: When active, grants the wielder 20 defense rating and 300 armor for 10 sec.
                throw new NotImplementedException();
            };
            // 2256 - Skeletal Club
            item = GetItem(2256, "Skeletal Club");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Sends a shadowy bolt at the enemy causing 30 Shadow damage.
                throw new NotImplementedException();
            };
            // 880 - Staff of Horrors
            item = GetItem(880, "Staff of Horrors");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Damage caused by the target is reduced by 5 for 2 min.
                throw new NotImplementedException();
            };
            // 29182 - Riftmaker
            item = GetItem(29182, "Riftmaker");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Encloses enemy in a temporal rift, increasing the time between their attacks by 10% for 10 sec.
                throw new NotImplementedException();
            };
            // 28311 - Revenger
            item = GetItem(28311, "Revenger");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Steals 105 to 125 life from target enemy.
                throw new NotImplementedException();
            };
            // 31334 - Staff of Natural Fury
            item = GetItem(31334, "Staff of Natural Fury");
            item.OnEquip = (sim) =>
            {
                //Reduces the base Mana cost of your shapeshifting spells by 200.
                throw new NotImplementedException();
            };
            // 17752 - Satyr's Lash
            item = GetItem(17752, "Satyr's Lash");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Sends a shadowy bolt at the enemy causing 55 to 85 Shadow damage.
                throw new NotImplementedException();
            };
            // 1265 - Scorpion Sting
            item = GetItem(1265, "Scorpion Sting");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Poisons target for 13 Nature damage every 5 sec for 25 sec.
                throw new NotImplementedException();
            };
            // 6472 - Stinging Viper
            item = GetItem(6472, "Stinging Viper");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Poisons target for 7 Nature damage every 3 sec for 15 sec.
                throw new NotImplementedException();
            };
            // 13035 - Serpent Slicer
            item = GetItem(13035, "Serpent Slicer");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Poisons target for 8 Nature damage every 2 sec for 20 sec.
                throw new NotImplementedException();
            };
            // 5613 - Staff of the Purifier
            item = GetItem(5613, "Staff of the Purifier");
            item.OnUse = (sim) =>
            {
                //Use: Purifies the friendly target, removing 1 disease effect and 1 poison effect. (5 Min Cooldown)
                throw new NotImplementedException();
            };
            // 1482 - Shadowfang
            item = GetItem(1482, "Shadowfang");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Sends a shadowy bolt at the enemy causing 30 Shadow damage.
                throw new NotImplementedException();
            };
            // 21128 - Staff of the Qiraji Prophets
            item = GetItem(21128, "Staff of the Qiraji Prophets");
            item.OnEquip = (sim) =>
            {
                //Gives a chance when your harmful spells land to reduce the magical resistances of your spell targets by 50 for 8 sec.
                throw new NotImplementedException();
            };
            // 12531 - Searing Needle
            item = GetItem(12531, "Searing Needle");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Blasts a target for 60 Fire damage and increases damage done to target by Fire spells by up to 10 for 30 sec.
                throw new NotImplementedException();
            };
            // 754 - Shortsword of Vengeance
            item = GetItem(754, "Shortsword of Vengeance");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Smites an enemy for 30 Holy damage.
                throw new NotImplementedException();
            };
            // 32262 - Syphon of the Nathrezim
            item = GetItem(32262, "Syphon of the Nathrezim");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Fills you with fel energy allowing all melee attacks to drain life from opponents.
                throw new NotImplementedException();
            };
            // 13953 - Silent Fang
            item = GetItem(13953, "Silent Fang");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Silences an enemy preventing it from casting spells for 6 sec.
                throw new NotImplementedException();
            };
            // 2163 - Shadowblade
            item = GetItem(2163, "Shadowblade");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Sends a shadowy bolt at the enemy causing 110 to 140 Shadow damage.
                throw new NotImplementedException();
            };
            // 8224 - Silithid Ripper
            item = GetItem(8224, "Silithid Ripper");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Wounds the target causing them to bleed for 45 damage over 30 sec.
                throw new NotImplementedException();
            };
            // 32471 - Shard of Azzinoth
            item = GetItem(32471, "Shard of Azzinoth");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Calls forth an Ember of Azzinoth to protect you in battle for a short period of time.
                throw new NotImplementedException();
            };
            // 13401 - The Cruel Hand of Timmy
            item = GetItem(13401, "The Cruel Hand of Timmy");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Lowers all attributes of target by 15 for 1 min.
                throw new NotImplementedException();
            };
            // 13361 - Skullforge Reaver
            item = GetItem(13361, "Skullforge Reaver");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Drains target for 2 Shadow damage every 1 sec and transfers it to the caster. Lasts for 30 sec.
                throw new NotImplementedException();
            };
            // 9639 - The Hand of Antu'sul
            item = GetItem(9639, "The Hand of Antu'sul");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Blasts nearby enemies with thunder increasing the time between their attacks by 11% for 10 sec and doing 7 Nature damage to them. &nbsp;Will affect up to 4 targets.
                throw new NotImplementedException();
            };
            // 7954 - The Shatterer
            item = GetItem(7954, "The Shatterer");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Disarm target's weapon for 10 sec.
                throw new NotImplementedException();
            };
            // 9477 - The Chief's Enforcer
            item = GetItem(9477, "The Chief's Enforcer");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Stuns target for 3 sec.
                throw new NotImplementedException();
            };
            // 5756 - Sliverblade
            item = GetItem(5756, "Sliverblade");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Blasts a target for 45 Frost damage.
                throw new NotImplementedException();
            };
            // 22783 - Sunwell Blade
            item = GetItem(22783, "Sunwell Blade");
            item.OnUse = (sim) =>
            {
                //Use: Use on Dar'Khan Drathir to release the energy contained in this item causing 500 Arcane damage over 5 sec and silencing the target. (10 Min Cooldown)
                throw new NotImplementedException();
            };
            // 13032 - Sword of Corruption
            item = GetItem(13032, "Sword of Corruption");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Corrupts the target, causing 30 damage over 3 sec.
                throw new NotImplementedException();
            };
            // 1727 - Sword of Decay
            item = GetItem(1727, "Sword of Decay");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Reduces target's Strength by 10 for 30 sec.
                throw new NotImplementedException();
            };
            // 6622 - Sword of Zeal
            item = GetItem(6622, "Sword of Zeal");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: A burst of energy fills the caster, increasing his damage by 10 and armor by 150 for 15 sec.
                throw new NotImplementedException();
            };
            // 13183 - Venomspitter
            item = GetItem(13183, "Venomspitter");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Poisons target for 7 Nature damage every 2 sec for 30 sec.
                throw new NotImplementedException();
            };
            // 8225 - Tainted Pierce
            item = GetItem(8225, "Tainted Pierce");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Corrupts the target, causing 45 damage over 3 sec.
                throw new NotImplementedException();
            };
            // 12792 - Volcanic Hammer
            item = GetItem(12792, "Volcanic Hammer");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Hurls a fiery ball that causes 100 to 128 Fire damage and an additional 18 damage over 6 sec.
                throw new NotImplementedException();
            };
            // 1728 - Teebu's Blazing Longsword
            item = GetItem(1728, "Teebu's Blazing Longsword");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Blasts a target for 150 Fire damage.
                throw new NotImplementedException();
            };
            // 10625 - Stealthblade
            item = GetItem(10625, "Stealthblade");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Reduces threat level on all enemies by a small amount for 10 sec.
                throw new NotImplementedException();
            };
            // 12974 - The Black Knight
            item = GetItem(12974, "The Black Knight");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Sends a shadowy bolt at the enemy causing 35 to 45 Shadow damage.
                throw new NotImplementedException();
            };
            // 38175 - The Horseman's Blade
            item = GetItem(38175, "The Horseman's Blade");
            item.OnUse = (sim) =>
            {
                //Use: Summon Pumpkin Soldiers to burn your foes. (10 Min Cooldown)
                throw new NotImplementedException();
            };
            // 17705 - Thrash Blade
            item = GetItem(17705, "Thrash Blade");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Grants an extra attack on your next swing.
                throw new NotImplementedException();
            };
            // 19019 - Thunderfury, Blessed Blade of the Windseeker
            item = GetItem(19019, "Thunderfury, Blessed Blade of the Windseeker");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Blasts your enemy with lightning, dealing 300 Nature damage and then jumping to additional nearby enemies. &nbsp;Each jump reduces that victim's Nature resistance by 25. Affects 5 targets. Your primary target is also consumed by a cyclone, slowing its attack speed by 20% for 12 sec.
                throw new NotImplementedException();
            };
            // 19324 - The Lobotomizer
            item = GetItem(19324, "The Lobotomizer");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Wounds the target for 200 to 300 damage and lowers Intellect of target by 25 for 30 sec.
                throw new NotImplementedException();
            };
            // 31331 - The Night Blade
            item = GetItem(31331, "The Night Blade");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Your attacks ignore 435 of your enemies' armor for 10 sec. This effect stacks up to 3 times.
                throw new NotImplementedException();
            };
            // 8006 - The Ziggler
            item = GetItem(8006, "The Ziggler");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Blasts a target for 10 to 20 Nature damage.
                throw new NotImplementedException();
            };
            // 17075 - Vis'kag the Bloodletter
            item = GetItem(17075, "Vis'kag the Bloodletter");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Delivers a fatal wound for 240 damage.
                throw new NotImplementedException();
            };
            // 30311 - Warp Slicer
            item = GetItem(30311, "Warp Slicer");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Increases your movement speed by 50%, and your melee attack speed by 20% for 30 sec.
                throw new NotImplementedException();
            };
            // 9453 - Toxic Revenger
            item = GetItem(9453, "Toxic Revenger");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Deals 5 Nature damage every 5 sec to any enemy in an 8 yard radius around the caster for 15 sec.
                throw new NotImplementedException();
            };
            // 899 - Venom Web Fang
            item = GetItem(899, "Venom Web Fang");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Poisons target for 3 Nature damage every 3 sec for 15 sec.
                throw new NotImplementedException();
            };
            // 19901 - Zulian Slicer
            item = GetItem(19901, "Zulian Slicer");
            item.OnEquip = (sim) =>
            {
                //Skinning +10.
                throw new NotImplementedException();
            };
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Slices the enemy for 72 to 96 Nature damage.
                throw new NotImplementedException();
            };
            // 5752 - Wyvern Tailspike
            item = GetItem(5752, "Wyvern Tailspike");
            item.OnChanceOnHit = (sim) =>
            {
                //Chance on hit: Poisons target for 6 Nature damage every 3 sec for 15 sec.
                throw new NotImplementedException();
            };
        }
    }
}