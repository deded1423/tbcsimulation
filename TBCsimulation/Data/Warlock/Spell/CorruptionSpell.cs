﻿using System;
using TBCsimulation.Data.TalentsInfo;
using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;

namespace TBCsimulation.Data.Warlock.Spell
{
    public partial class CorruptionSpell : SpellsInfo.Spell
    {
        private const int totalDuration = 18;
        private const int tickInterval = 3;
        private const int ticks = totalDuration / tickInterval;


        private void Init(FightSim sim)
        {
            CastTime -= 400 * sim.Player.GetTalentRank(Talents.Warlock_ImprovedCorruption);

            ActionDelegate = delegate
            {
                var damage = 900 +
                             (int) Math.Floor(
                                 (0.93 + 0.12 * sim.Player.GetTalentRank(Talents.Warlock_EmpoweredCorruption)) *
                                 sim.Player.GetSpellShadowDamage());
                damage /= ticks;


                var buff = new BuffSim(Name, Id);
                buff.DurationTotal = totalDuration * 1000;
                buff.DurationRemaining = buff.DurationTotal;
                buff.TickInterval = tickInterval * 1000;
                buff.TickActionDelegate = simBuff =>
                {
                    simBuff.LoggerSim.TickDebuff(simBuff.Time, buff);

                    var multiplier = 1 + 0.01f * simBuff.Player.GetTalentRank(Talents.Warlock_Contagion);

                    simBuff.Damage(EnumSim.DamageType.Spell, EnumSim.SchoolType.Shadow, Id, damage, multiplier, true);

                    if (RandomSim.CheckChance(2 * simBuff.Player.GetTalentRank(Talents.Warlock_Nightfall)))
                    {
                        var buffNF = new BuffSim("Nightfall", Talents.Warlock_Nightfall);
                        buffNF.DurationTotal = 10 * 1000;
                        buffNF.DurationRemaining = buffNF.DurationTotal;
                        buffNF.TickActionDelegate = simBuffNF =>
                        {
                            simBuffNF.LoggerSim.TickDebuff(simBuffNF.Time, buffNF);
                            simBuffNF.LoggerSim.FadeBuff(simBuffNF.Time, buffNF);
                            simBuffNF.RemoveBuff(Id);
                        };

                        simBuff.Buff(buffNF, EnumSim.SchoolType.Shadow, Id, false);
                    }

                    buff.DurationRemaining -= buff.TickInterval;

                    if (buff.DurationRemaining > 0)
                    {
                        simBuff.EnqueueHeap(new HeapEventSim("Tick " + Name, buff.TickInterval, buff));
                    }
                    else
                    {
                        simBuff.LoggerSim.FadeDebuff(simBuff.Time, buff);
                        simBuff.RemoveDebuff(Id);
                    }
                };

                var hitChance = sim.Player.GetTalentRank(Talents.Warlock_Suppression) * 2;

                var hit = sim.Debuff(buff, EnumSim.SchoolType.Shadow, Id, false, true, true,
                    sim.Player.GetSpellHitChance() + hitChance);

                if (hit)
                    if (sim.HasDebuff("Seed of Corruption"))
                    {
                        sim.RemoveBuff("Seed of Corruption");
                        sim.RemoveBuffEvents("Seed of Corruption");
                    }
            };
        }
    }
}