﻿using System;
using TBCsimulation.Data.TalentsInfo;
using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;

namespace TBCsimulation.Data.Warlock.Spell
{
    public partial class UnstableAfflictionSpell : SpellsInfo.Spell
    {
        private const int totalDuration = 18;
        private const int tickInterval = 3;
        private const int ticks = totalDuration / tickInterval;


        private void Init(FightSim sim)
        {
            ActionDelegate = delegate
            {
                var damage = 1050 + (int) Math.Floor(1.2 * sim.Player.GetSpellShadowDamage());
                damage /= ticks;

                var buff = new BuffSim(Name, Id);
                buff.DurationTotal = totalDuration * 1000;
                buff.DurationRemaining = buff.DurationTotal;
                buff.TickInterval = tickInterval * 1000;
                buff.TickActionDelegate = simBuff =>
                {
                    simBuff.LoggerSim.TickDebuff(simBuff.Time, buff);
                    simBuff.Damage(EnumSim.DamageType.Spell, EnumSim.SchoolType.Shadow, Id, damage, 1, true);

                    buff.DurationRemaining -= buff.TickInterval;

                    if (buff.DurationRemaining > 0)
                    {
                        simBuff.EnqueueHeap(new HeapEventSim("Tick " + Name, buff.TickInterval, buff));
                    }
                    else
                    {
                        simBuff.LoggerSim.FadeDebuff(simBuff.Time, buff);
                        simBuff.RemoveDebuff(Id);
                    }
                };

                var hitChance = sim.Player.GetTalentRank(Talents.Warlock_Suppression) * 2;

                sim.Debuff(buff, EnumSim.SchoolType.Shadow, Id, false, true, true,
                    sim.Player.GetSpellHitChance() + hitChance);
            };
        }
    }
}