﻿using System;
using TBCsimulation.Data.TalentsInfo;
using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;

namespace TBCsimulation.Data.Warlock.Spell
{
    public partial class SeedofCorruptionSpell : SpellsInfo.Spell
    {
        private const int totalDuration = 18;
        private const int ticks = 4;
        private const float tickInterval = totalDuration / (float) ticks;


        private void Init(FightSim sim)
        {
            ActionDelegate = delegate
            {
                //1.214
                var damage = 1044 + (int) Math.Floor(0.461 * sim.Player.GetSpellShadowDamage());
                damage /= ticks;

                damage = (int) (damage * (1 + 0.01f * sim.Player.GetTalentRank(Talents.Warlock_Contagion)));

                var buff = new BuffSim(Name, Id);
                buff.DurationTotal = totalDuration * 1000;
                buff.DurationRemaining = buff.DurationTotal;
                buff.TickInterval = (int) (tickInterval * 1000);
                buff.TickActionDelegate = simBuff =>
                {
                    simBuff.LoggerSim.TickDebuff(simBuff.Time, buff);
                    simBuff.Damage(EnumSim.DamageType.Spell, EnumSim.SchoolType.Shadow, Id, damage, 1, true);

                    buff.DurationRemaining -= buff.TickInterval;

                    if (buff.DurationRemaining > 0)
                    {
                        simBuff.EnqueueHeap(new HeapEventSim("Tick " + Name, buff.TickInterval, buff));
                    }
                    else
                    {
                        simBuff.LoggerSim.FadeDebuff(simBuff.Time, buff);
                        var damageEnd = RandomSim.GetInt(1110, 1290) +
                                        (int) Math.Floor(0.22f * simBuff.Player.GetSpellShadowDamage());

                        damageEnd =
                            (int) (damageEnd * (1 + 0.01f * simBuff.Player.GetTalentRank(Talents.Warlock_Contagion)));
                        simBuff.Damage(EnumSim.DamageType.Spell, EnumSim.SchoolType.Shadow, Id, damageEnd, 1, true,
                            false, 0, true,
                            simBuff.Player.GetSpellShadowCritChance(), simBuff.Player.GetSpellCritDamage());
                        simBuff.RemoveDebuff(Id);
                    }
                };

                var hitChance = sim.Player.GetTalentRank(Talents.Warlock_Suppression) * 2;

                var hit = sim.Debuff(buff, EnumSim.SchoolType.Shadow, Id, false, true, true,
                    sim.Player.GetSpellHitChance() + hitChance);

                if (hit)
                    if (sim.HasDebuff("Corruption"))
                    {
                        sim.RemoveBuff("Corruption");
                        sim.RemoveBuffEvents("Corruption");
                    }
            };
        }
    }
}