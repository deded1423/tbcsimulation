﻿using TBCsimulation.Data.TalentsInfo;
using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;

namespace TBCsimulation.Data.Warlock.Spell
{
    public partial class CurseofRecklessnessSpell : SpellsInfo.Spell
    {
        private const int totalDuration = 120;
        private const int armorValue = 800;
        private const int attackpowerValue = 135;


        private void Init(FightSim sim)
        {
            ActionDelegate = delegate
            {
                var buff = new BuffSim(Name, Id);
                buff.DurationTotal = totalDuration * 1000;
                buff.DurationRemaining = buff.DurationTotal;
                buff.TickActionDelegate = simBuff =>
                {
                    simBuff.LoggerSim.TickDebuff(simBuff.Time, buff);

                    simBuff.Boss.DefenseArmorMod += armorValue;
                    simBuff.Boss.MeleeAttackPowerMod -= attackpowerValue;

                    simBuff.LoggerSim.FadeDebuff(simBuff.Time, buff);
                    simBuff.RemoveDebuff(Id);
                };

                var hitChance = sim.Player.GetTalentRank(Talents.Warlock_Suppression) * 2;

                var hit = sim.Debuff(buff, EnumSim.SchoolType.Shadow, Id, false, false, true,
                    sim.Player.GetSpellHitChance() + hitChance);

                if (hit)
                {
                    sim.Boss.DefenseArmorMod -= armorValue;
                    sim.Boss.MeleeAttackPowerMod += attackpowerValue;
                }

                if (hit)
                {
                    if (sim.HasDebuff("Curse of Agony"))
                    {
                        sim.RemoveBuff("Curse of Agony");
                        sim.RemoveBuffEvents("Curse of Agony");
                    }

                    if (sim.HasDebuff("Curse of Doom"))
                    {
                        sim.RemoveBuff("Curse of Doom");
                        sim.RemoveBuffEvents("Curse of Doom");
                    }

                    if (sim.HasDebuff("Curse of Exhaustion"))
                    {
                        sim.RemoveBuff("Curse of Exhaustion");
                        sim.RemoveBuffEvents("Curse of Exhaustion");
                    }

                    if (sim.HasDebuff("Curse of the Elements"))
                    {
                        sim.RemoveBuff("Curse of the Elements");
                        sim.RemoveBuffEvents("Curse of the Elements");
                    }

                    if (sim.HasDebuff("Curse of Tongues"))
                    {
                        sim.RemoveBuff("Curse of Tongues");
                        sim.RemoveBuffEvents("Curse of Tongues");
                    }

                    if (sim.HasDebuff("Curse of Weakness"))
                    {
                        sim.RemoveBuff("Curse of Weakness");
                        sim.RemoveBuffEvents("Curse of Weakness");
                    }
                }
            };
        }
    }
}