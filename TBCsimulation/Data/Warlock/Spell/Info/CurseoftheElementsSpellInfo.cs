using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Warlock.Spell
{
    public partial class CurseoftheElementsSpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public CurseoftheElementsSpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=27228/curse-of-the-elements
            Name = "Curse of the Elements";
            Id = 27228;
            Level = 69;
            Rank = 4;
            Range = 30;

            Mana = 260;

            IsGCD = true;
            Channeled = false;
            CastTime = 0;
            CooldownTime = 0;

            School = EnumSim.SchoolType.Shadow;
            SecondarySchool = EnumSim.SecondarySchoolType.Warlock_Affliction;
            Init(sim);
        }
    }
}