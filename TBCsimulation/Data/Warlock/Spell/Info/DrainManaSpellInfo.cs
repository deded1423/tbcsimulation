using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Warlock.Spell
{
    public partial class DrainManaSpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public DrainManaSpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=30908/drain-mana
            Name = "Drain Mana";
            Id = 30908;
            Level = 70;
            Rank = 6;
            Range = 30;

            Mana = 455;

            IsGCD = true;
            Channeled = true;
            CastTime = 5000;
            CooldownTime = 0;

            School = EnumSim.SchoolType.Shadow;
            SecondarySchool = EnumSim.SecondarySchoolType.Warlock_Affliction;
            Init(sim);
        }
    }
}