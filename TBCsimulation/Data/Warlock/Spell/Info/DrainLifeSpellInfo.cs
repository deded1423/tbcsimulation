using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Warlock.Spell
{
    public partial class DrainLifeSpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public DrainLifeSpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=27220/drain-life
            Name = "Drain Life";
            Id = 27220;
            Level = 69;
            Rank = 8;
            Range = 30;

            Mana = 425;

            IsGCD = true;
            Channeled = true;
            CastTime = 5000;
            CooldownTime = 0;

            School = EnumSim.SchoolType.Shadow;
            SecondarySchool = EnumSim.SecondarySchoolType.Warlock_Affliction;
            Init(sim);
        }
    }
}