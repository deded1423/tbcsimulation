using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Warlock.Spell
{
    public partial class SearingPainSpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public SearingPainSpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=30459/searing-pain
            Name = "Searing Pain";
            Id = 30459;
            Level = 70;
            Rank = 8;
            Range = 30;

            Mana = 205;

            IsGCD = true;
            Channeled = false;
            CastTime = 1500;
            CooldownTime = 0;

            School = EnumSim.SchoolType.Fire;
            SecondarySchool = EnumSim.SecondarySchoolType.Warlock_Destruction;
            Init(sim);
        }
    }
}