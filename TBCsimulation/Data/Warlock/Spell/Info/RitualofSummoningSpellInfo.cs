using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Warlock.Spell
{
    public partial class RitualofSummoningSpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public RitualofSummoningSpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=698/ritual-of-summoning
            Name = "Ritual of Summoning";
            Id = 698;
            Level = 20;
            Rank = 1;
            Range = 30;

            Mana = 300;

            IsGCD = true;
            Channeled = false;
            CastTime = 5000;
            CooldownTime = 0;

            School = EnumSim.SchoolType.Shadow;
            SecondarySchool = EnumSim.SecondarySchoolType.Warlock_Demonology;
            Init(sim);
        }
    }
}