using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Warlock.Spell
{
    public partial class CurseofWeaknessSpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public CurseofWeaknessSpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=30909/curse-of-weakness
            Name = "Curse of Weakness";
            Id = 30909;
            Level = 69;
            Rank = 8;
            Range = 30;

            Mana = 265;

            IsGCD = true;
            Channeled = false;
            CastTime = 0;
            CooldownTime = 0;

            School = EnumSim.SchoolType.Shadow;
            SecondarySchool = EnumSim.SecondarySchoolType.Warlock_Affliction;
            Init(sim);
        }
    }
}