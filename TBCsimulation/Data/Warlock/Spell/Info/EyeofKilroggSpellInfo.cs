using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Warlock.Spell
{
    public partial class EyeofKilroggSpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public EyeofKilroggSpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=126/eye-of-kilrogg
            Name = "Eye of Kilrogg";
            Id = 126;
            Level = 22;
            Rank = 1;
            Range = 50000;

            Mana = 100;

            IsGCD = true;
            Channeled = false;
            CastTime = 5000;
            CooldownTime = 0;

            School = EnumSim.SchoolType.Shadow;
            SecondarySchool = EnumSim.SecondarySchoolType.Warlock_Demonology;
            Init(sim);
        }
    }
}