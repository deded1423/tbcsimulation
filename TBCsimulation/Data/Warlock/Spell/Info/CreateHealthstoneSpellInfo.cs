using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Warlock.Spell
{
    public partial class CreateHealthstoneSpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public CreateHealthstoneSpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=27230/create-healthstone
            Name = "Create Healthstone";
            Id = 27230;
            Level = 68;
            Rank = 6;
            Range = 0;

            Mana = 1390;

            IsGCD = true;
            Channeled = false;
            CastTime = 3000;
            CooldownTime = 0;

            School = EnumSim.SchoolType.Shadow;
            SecondarySchool = EnumSim.SecondarySchoolType.Warlock_Demonology;
            Init(sim);
        }
    }
}