using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Warlock.Spell
{
    public partial class SummonDreadsteedSpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public SummonDreadsteedSpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=23161/summon-dreadsteed
            Name = "Summon Dreadsteed";
            Id = 23161;
            Level = 60;
            Rank = 1;
            Range = 0;

            Mana = 150;

            IsGCD = true;
            Channeled = false;
            CastTime = 3000;
            CooldownTime = 0;

            School = EnumSim.SchoolType.Shadow;
            SecondarySchool = EnumSim.SecondarySchoolType.Warlock_Demonology;
            Init(sim);
        }
    }
}