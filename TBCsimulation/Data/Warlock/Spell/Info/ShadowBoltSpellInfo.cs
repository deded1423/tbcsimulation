using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Warlock.Spell
{
    public partial class ShadowBoltSpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public ShadowBoltSpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=27209/shadow-bolt
            Name = "Shadow Bolt";
            Id = 27209;
            Level = 69;
            Rank = 11;
            Range = 30;

            Mana = 420;

            IsGCD = true;
            Channeled = false;
            CastTime = 3000;
            CooldownTime = 0;

            School = EnumSim.SchoolType.Shadow;
            SecondarySchool = EnumSim.SecondarySchoolType.Warlock_Destruction;
            Init(sim);
        }
    }
}