using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Warlock.Spell
{
    public partial class BanishSpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public BanishSpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=18647/banish
            Name = "Banish";
            Id = 18647;
            Level = 48;
            Rank = 2;
            Range = 30;

            Mana = 200;

            IsGCD = true;
            Channeled = false;
            CastTime = 1500;
            CooldownTime = 0;

            School = EnumSim.SchoolType.Shadow;
            SecondarySchool = EnumSim.SecondarySchoolType.Warlock_Demonology;
            Init(sim);
        }
    }
}