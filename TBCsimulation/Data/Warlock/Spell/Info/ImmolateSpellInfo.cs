using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Warlock.Spell
{
    public partial class ImmolateSpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public ImmolateSpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=27215/immolate
            Name = "Immolate";
            Id = 27215;
            Level = 69;
            Rank = 9;
            Range = 30;

            Mana = 445;

            IsGCD = true;
            Channeled = false;
            CastTime = 2000;
            CooldownTime = 0;

            School = EnumSim.SchoolType.Fire;
            SecondarySchool = EnumSim.SecondarySchoolType.Warlock_Destruction;
            Init(sim);
        }
    }
}