using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Warlock.Spell
{
    public partial class HealthFunnelSpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public HealthFunnelSpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=27259/health-funnel
            Name = "Health Funnel";
            Id = 27259;
            Level = 67;
            Rank = 8;
            Range = 20;

            Mana = -1;

            IsGCD = true;
            Channeled = true;
            CastTime = 10000;
            CooldownTime = 0;

            School = EnumSim.SchoolType.Shadow;
            SecondarySchool = EnumSim.SecondarySchoolType.Warlock_Demonology;
            Init(sim);
        }
    }
}