using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Warlock.Spell
{
    public partial class RainofFireSpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public RainofFireSpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=27212/rain-of-fire
            Name = "Rain of Fire";
            Id = 27212;
            Level = 69;
            Rank = 5;
            Range = 30;

            Mana = 1480;

            IsGCD = true;
            Channeled = true;
            CastTime = 8000;
            CooldownTime = 0;

            School = EnumSim.SchoolType.Fire;
            SecondarySchool = EnumSim.SecondarySchoolType.Warlock_Destruction;
            Init(sim);
        }
    }
}