using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Warlock.Spell
{
    public partial class SoulFireSpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public SoulFireSpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=30545/soul-fire
            Name = "Soul Fire";
            Id = 30545;
            Level = 70;
            Rank = 4;
            Range = 30;

            Mana = 250;

            IsGCD = true;
            Channeled = false;
            CastTime = 6000;
            CooldownTime = 1000;

            School = EnumSim.SchoolType.Fire;
            SecondarySchool = EnumSim.SecondarySchoolType.Warlock_Destruction;
            Init(sim);
        }
    }
}