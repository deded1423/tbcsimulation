using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Warlock.Spell
{
    public partial class ShadowfurySpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public ShadowfurySpell(FightSim sim)

        {
//https://tbc.wowhead.com/spell=30283
            Name = "Shadowfury";
            Id = 30283;
            Level = 0;
            Rank = 1;
            Range = 30;

            Mana = 440;

            IsGCD = false;
            Channeled = false;
            CastTime = 500000;
            CooldownTime = 20000;

            School = EnumSim.SchoolType.Shadow;
            SecondarySchool = EnumSim.SecondarySchoolType.Warlock_Destruction;
            Init(sim);
        }
    }
}