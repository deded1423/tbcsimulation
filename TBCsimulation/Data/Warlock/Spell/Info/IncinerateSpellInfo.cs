using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Warlock.Spell
{
    public partial class IncinerateSpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public IncinerateSpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=32231/incinerate
            Name = "Incinerate";
            Id = 32231;
            Level = 70;
            Rank = 2;
            Range = 30;

            Mana = 355;

            IsGCD = true;
            Channeled = false;
            CastTime = 2500;
            CooldownTime = 0;

            School = EnumSim.SchoolType.Fire;
            SecondarySchool = EnumSim.SecondarySchoolType.Warlock_Destruction;
            Init(sim);
        }
    }
}