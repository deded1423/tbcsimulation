using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Warlock.Spell
{
    public partial class CurseofRecklessnessSpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public CurseofRecklessnessSpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=27226/curse-of-recklessness
            Name = "Curse of Recklessness";
            Id = 27226;
            Level = 69;
            Rank = 5;
            Range = 30;

            Mana = 160;

            IsGCD = true;
            Channeled = false;
            CastTime = 0;
            CooldownTime = 0;

            School = EnumSim.SchoolType.Shadow;
            SecondarySchool = EnumSim.SecondarySchoolType.Warlock_Affliction;
            Init(sim);
        }
    }
}