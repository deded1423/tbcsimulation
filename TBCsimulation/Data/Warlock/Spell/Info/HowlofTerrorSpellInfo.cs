using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Warlock.Spell
{
    public partial class HowlofTerrorSpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public HowlofTerrorSpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=17928/howl-of-terror
            Name = "Howl of Terror";
            Id = 17928;
            Level = 54;
            Rank = 2;
            Range = 0;

            Mana = 200;

            IsGCD = true;
            Channeled = false;
            CastTime = 1500;
            CooldownTime = 40000;

            School = EnumSim.SchoolType.Shadow;
            SecondarySchool = EnumSim.SecondarySchoolType.Warlock_Affliction;
            Init(sim);
        }
    }
}