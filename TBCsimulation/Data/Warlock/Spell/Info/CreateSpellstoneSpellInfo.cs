using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Warlock.Spell
{
    public partial class CreateSpellstoneSpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public CreateSpellstoneSpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=28172/create-spellstone
            Name = "Create Spellstone";
            Id = 28172;
            Level = 66;
            Rank = 4;
            Range = 0;

            Mana = 1150;

            IsGCD = true;
            Channeled = false;
            CastTime = 5000;
            CooldownTime = 0;

            School = EnumSim.SchoolType.Shadow;
            SecondarySchool = EnumSim.SecondarySchoolType.Warlock_Demonology;
            Init(sim);
        }
    }
}