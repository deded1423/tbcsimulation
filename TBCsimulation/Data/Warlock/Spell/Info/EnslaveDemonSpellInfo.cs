using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Warlock.Spell
{
    public partial class EnslaveDemonSpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public EnslaveDemonSpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=11726/enslave-demon
            Name = "Enslave Demon";
            Id = 11726;
            Level = 58;
            Rank = 3;
            Range = 30;

            Mana = 700;

            IsGCD = true;
            Channeled = false;
            CastTime = 3000;
            CooldownTime = 0;

            School = EnumSim.SchoolType.Shadow;
            SecondarySchool = EnumSim.SecondarySchoolType.Warlock_Demonology;
            Init(sim);
        }
    }
}