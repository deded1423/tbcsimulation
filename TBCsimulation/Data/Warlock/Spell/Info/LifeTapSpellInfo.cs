using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Warlock.Spell
{
    public partial class LifeTapSpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public LifeTapSpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=27222/life-tap
            Name = "Life Tap";
            Id = 27222;
            Level = 68;
            Rank = 7;
            Range = 0;

            Mana = -1;

            IsGCD = true;
            Channeled = false;
            CastTime = 0;
            CooldownTime = 0;

            School = EnumSim.SchoolType.Shadow;
            SecondarySchool = EnumSim.SecondarySchoolType.Warlock_Affliction;
            Init(sim);
        }
    }
}