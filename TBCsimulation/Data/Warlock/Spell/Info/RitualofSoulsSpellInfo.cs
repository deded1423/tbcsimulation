using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Warlock.Spell
{
    public partial class RitualofSoulsSpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public RitualofSoulsSpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=29893/ritual-of-souls
            Name = "Ritual of Souls";
            Id = 29893;
            Level = 68;
            Rank = 1;
            Range = 30;

            Mana = -1;

            IsGCD = true;
            Channeled = true;
            CastTime = 1000;
            CooldownTime = 300000;

            School = EnumSim.SchoolType.Shadow;
            SecondarySchool = EnumSim.SecondarySchoolType.Warlock_Demonology;
            Init(sim);
        }
    }
}