using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Warlock.Spell
{
    public partial class CurseofAgonySpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public CurseofAgonySpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=27218/curse-of-agony
            Name = "Curse of Agony";
            Id = 27218;
            Level = 67;
            Rank = 7;
            Range = 30;

            Mana = 265;

            IsGCD = true;
            Channeled = false;
            CastTime = 0;
            CooldownTime = 0;

            School = EnumSim.SchoolType.Shadow;
            SecondarySchool = EnumSim.SecondarySchoolType.Warlock_Affliction;
            Init(sim);
        }
    }
}