using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Warlock.Spell
{
    public partial class CorruptionSpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public CorruptionSpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=27216/corruption
            Name = "Corruption";
            Id = 27216;
            Level = 65;
            Rank = 8;
            Range = 30;

            Mana = 370;

            IsGCD = true;
            Channeled = false;
            CastTime = 2000;
            CooldownTime = 0;

            School = EnumSim.SchoolType.Shadow;
            SecondarySchool = EnumSim.SecondarySchoolType.Warlock_Affliction;
            Init(sim);
        }
    }
}