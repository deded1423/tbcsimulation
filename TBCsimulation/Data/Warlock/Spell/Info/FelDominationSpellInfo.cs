using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Warlock.Spell
{
    public partial class FelDominationSpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public FelDominationSpell(FightSim sim)

        {
//https://tbc.wowhead.com/spell=18708
            Name = "Fel Domination";
            Id = 18708;
            Level = 0;
            Rank = 1;
            Range = 0;

            Mana = -1;

            IsGCD = false;
            Channeled = false;
            CastTime = 0;
            CooldownTime = 900000;

            School = EnumSim.SchoolType.Shadow;
            SecondarySchool = EnumSim.SecondarySchoolType.Warlock_Demonology;
            Init(sim);
        }
    }
}