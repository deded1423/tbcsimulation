using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Warlock.Spell
{
    public partial class HellfireSpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public HellfireSpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=27213/hellfire
            Name = "Hellfire";
            Id = 27213;
            Level = 68;
            Rank = 4;
            Range = 0;

            Mana = 1665;

            IsGCD = true;
            Channeled = true;
            CastTime = 15000;
            CooldownTime = 0;

            School = EnumSim.SchoolType.Fire;
            SecondarySchool = EnumSim.SecondarySchoolType.Warlock_Destruction;
            Init(sim);
        }
    }
}