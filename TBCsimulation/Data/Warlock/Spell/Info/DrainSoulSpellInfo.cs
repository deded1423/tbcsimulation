using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Warlock.Spell
{
    public partial class DrainSoulSpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public DrainSoulSpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=27217/drain-soul
            Name = "Drain Soul";
            Id = 27217;
            Level = 67;
            Rank = 5;
            Range = 30;

            Mana = 360;

            IsGCD = true;
            Channeled = true;
            CastTime = 15000;
            CooldownTime = 0;

            School = EnumSim.SchoolType.Shadow;
            SecondarySchool = EnumSim.SecondarySchoolType.Warlock_Affliction;
            Init(sim);
        }
    }
}