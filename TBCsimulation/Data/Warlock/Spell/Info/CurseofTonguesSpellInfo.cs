using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Warlock.Spell
{
    public partial class CurseofTonguesSpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public CurseofTonguesSpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=11719/curse-of-tongues
            Name = "Curse of Tongues";
            Id = 11719;
            Level = 50;
            Rank = 2;
            Range = 30;

            Mana = 110;

            IsGCD = true;
            Channeled = false;
            CastTime = 0;
            CooldownTime = 0;

            School = EnumSim.SchoolType.Shadow;
            SecondarySchool = EnumSim.SecondarySchoolType.Warlock_Affliction;
            Init(sim);
        }
    }
}