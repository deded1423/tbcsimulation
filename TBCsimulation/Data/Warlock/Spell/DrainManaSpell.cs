﻿using TBCsimulation.Simulation;

namespace TBCsimulation.Data.Warlock.Spell
{
    public partial class DrainManaSpell : SpellsInfo.Spell
    {
        private void Init(FightSim sim)
        {
            ActionDelegate = delegate
            {
                var mana = 200;

                //int hitChance = sim.Player.GetTalentRank(Talents.Suppression) * 2;

                sim.ManaGained(Id, mana);
            };
        }
    }
}