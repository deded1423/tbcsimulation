﻿using System;
using TBCsimulation.Data.TalentsInfo;
using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;

namespace TBCsimulation.Data.Warlock.Spell
{
    public partial class ShadowfurySpell : SpellsInfo.Spell
    {
        private void Init(FightSim sim)
        {
            Mana = (int) (Mana * (1f - 0.01f * sim.Player.GetTalentRank(Talents.Warlock_Cataclysm)));

            ActionDelegate = delegate
            {
                var damage = RandomSim.GetInt(612, 728) + (int) Math.Floor(0.193 * sim.Player.GetSpellShadowDamage());
                var crit = sim.Player.GetSpellShadowCritChance() +
                           1 * sim.Player.GetTalentRank(Talents.Warlock_Devastation);
                sim.Damage(EnumSim.DamageType.Spell, EnumSim.SchoolType.Shadow, Id, damage, 1, false, true,
                    sim.Player.GetSpellHitChance(), true,
                    crit, sim.Player.GetSpellCritDamage() * 2);
            };
        }
    }
}