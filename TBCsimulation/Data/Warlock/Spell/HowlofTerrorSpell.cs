﻿using System;
using TBCsimulation.Data.TalentsInfo;
using TBCsimulation.Simulation;

namespace TBCsimulation.Data.Warlock.Spell
{
    public partial class HowlofTerrorSpell : SpellsInfo.Spell
    {
        private void Init(FightSim sim)
        {
            if (sim.Player.GetTalentRank(Talents.Warlock_ImprovedHowlofTerror) == 1)
                CastTime = 700;
            else if (sim.Player.GetTalentRank(Talents.Warlock_ImprovedHowlofTerror) == 2) CastTime = 0;

            ActionDelegate = delegate
            {
                //int hitChance = sim.Player.GetTalentRank(Talents.Suppression) * 2;

                throw new NotImplementedException();
            };
        }
    }
}