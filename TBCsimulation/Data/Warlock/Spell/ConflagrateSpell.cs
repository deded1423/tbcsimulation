﻿using System;
using TBCsimulation.Data.TalentsInfo;
using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;

namespace TBCsimulation.Data.Warlock.Spell
{
    public partial class ConflagrateSpell : SpellsInfo.Spell
    {
        private void Init(FightSim sim)
        {
            Mana = (int) (Mana * (1f - 0.01f * sim.Player.GetTalentRank(Talents.Warlock_Cataclysm)));

            ActionDelegate = delegate
            {
                if (sim.HasDebuff("Immolate"))
                {
                    var before = sim.Debuffs.Find(b => { return b.Name.Equals("Immolate"); });

                    sim.LoggerSim.FadeDebuff(sim.Time, before);
                    sim.Debuffs.Remove(before);

                    sim.RemoveBuffEvents(before);

                    var damage = RandomSim.GetInt(579, 721) + (int) Math.Floor(0.429 * sim.Player.GetSpellFireDamage());
                    var crit = sim.Player.GetSpellFireCritChance() +
                               1 * sim.Player.GetTalentRank(Talents.Warlock_Devastation);
                    damage = sim.Damage(EnumSim.DamageType.Spell, EnumSim.SchoolType.Fire, Id, damage, 1, false, true,
                        sim.Player.GetSpellHitChance(),
                        true, crit, sim.Player.GetSpellCritDamage() * 2);

                    if (RandomSim.CheckChance(10 * sim.Player.GetTalentRank(Talents.Warlock_SoulLeech)))
                        sim.Heal(Talents.Warlock_SoulLeech, (int) (damage * 0.2f));
                }
            };
        }
    }
}