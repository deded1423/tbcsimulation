﻿using System;
using TBCsimulation.Data.TalentsInfo;
using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;

namespace TBCsimulation.Data.Warlock.Spell
{
    public partial class SoulFireSpell : SpellsInfo.Spell
    {
        private void Init(FightSim sim)
        {
            CastTime -= 400 * sim.Player.GetTalentRank(Talents.Warlock_Bane);
            Mana = (int) (Mana * (1f - 0.01f * sim.Player.GetTalentRank(Talents.Warlock_Cataclysm)));

            ActionDelegate = delegate
            {
                var damage = RandomSim.GetInt(1003, 1257) + (int) Math.Floor(1.15 * sim.Player.GetSpellFireDamage());
                var crit = sim.Player.GetSpellFireCritChance() +
                           1 * sim.Player.GetTalentRank(Talents.Warlock_Devastation);
                damage = sim.Damage(EnumSim.DamageType.Spell, EnumSim.SchoolType.Fire, Id, damage, 1, false, true,
                    sim.Player.GetSpellHitChance(), true,
                    crit, sim.Player.GetSpellCritDamage() * 2);

                if (RandomSim.CheckChance(10 * sim.Player.GetTalentRank(Talents.Warlock_SoulLeech)))
                    sim.Heal(Talents.Warlock_SoulLeech, (int) (damage * 0.2f));
            };
        }
    }
}