﻿using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;

namespace TBCsimulation.Data.Warlock.Spell
{
    public partial class AmplifyCurseSpell : SpellsInfo.Spell
    {
        private const int totalDuration = 30;

        private void Init(FightSim sim)
        {
            ActionDelegate = delegate
            {
                var buff = new BuffSim(Name, Id);
                buff.DurationTotal = totalDuration * 1000;
                buff.DurationRemaining = buff.DurationTotal;
                buff.TickActionDelegate = simBuff =>
                {
                    simBuff.LoggerSim.TickBuff(simBuff.Time, buff);
                    simBuff.LoggerSim.FadeBuff(simBuff.Time, buff);
                    simBuff.RemoveBuff(Id);
                };

                sim.Buff(buff, EnumSim.SchoolType.Shadow, Id, false);
            };
        }
    }
}