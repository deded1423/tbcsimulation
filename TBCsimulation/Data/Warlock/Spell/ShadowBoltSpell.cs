﻿using System;
using TBCsimulation.Data.TalentsInfo;
using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;

namespace TBCsimulation.Data.Warlock.Spell
{
    public partial class ShadowBoltSpell : SpellsInfo.Spell
    {
        private void Init(FightSim sim)
        {
            CastTime -= 100 * sim.Player.GetTalentRank(Talents.Warlock_Bane);
            Mana = (int) (Mana * (1f - 0.01f * sim.Player.GetTalentRank(Talents.Warlock_Cataclysm)));

            ActionDelegate = delegate
            {
                var damage = RandomSim.GetInt(544, 607) +
                             (int) Math.Floor(
                                 (0.856 + 0.04 * sim.Player.GetTalentRank(Talents.Warlock_ShadowandFlame)) *
                                 sim.Player.GetSpellShadowDamage());
                var crit = sim.Player.GetSpellShadowCritChance() +
                           1 * sim.Player.GetTalentRank(Talents.Warlock_Devastation);
                bool critHit;

                damage = sim.Damage(EnumSim.DamageType.Spell, EnumSim.SchoolType.Shadow, Id, damage, 1, false, true,
                    sim.Player.GetSpellHitChance(), true,
                    crit, sim.Player.GetSpellCritDamage() * 2, out critHit);

                if (critHit)
                {
                    var buff = new BuffSim("Improved Shadow Bolt", Talents.Warlock_ImprovedShadowBolt);
                    buff.DurationTotal = 12 * 1000;
                    buff.DurationRemaining = buff.DurationTotal;
                    buff.Stacks = 4;
                    buff.TickActionDelegate = simBuff =>
                    {
                        simBuff.LoggerSim.TickDebuff(simBuff.Time, buff);
                        simBuff.LoggerSim.FadeBuff(simBuff.Time, buff);
                        simBuff.RemoveBuff(Id);
                    };

                    sim.Buff(buff, EnumSim.SchoolType.Shadow, Id, false);
                }

                if (RandomSim.CheckChance(10 * sim.Player.GetTalentRank(Talents.Warlock_SoulLeech)))
                    sim.Heal(Talents.Warlock_SoulLeech, (int) (damage * 0.2f));
            };
        }
    }
}