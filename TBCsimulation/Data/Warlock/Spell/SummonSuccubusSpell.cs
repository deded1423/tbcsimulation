﻿using TBCsimulation.Data.Warlock.Pet;
using TBCsimulation.Simulation;

namespace TBCsimulation.Data.Warlock.Spell
{
    public partial class SummonSuccubusSpell : SpellsInfo.Spell
    {
        private void Init(FightSim sim)
        {
            ActionDelegate = delegate { sim.Pet = new SuccubusPet(); };
        }
    }
}