﻿using TBCsimulation.Data.TalentsInfo;
using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;

namespace TBCsimulation.Data.Warlock.Spell
{
    public partial class FelArmorSpell : SpellsInfo.Spell
    {
        private const int totalDuration = 30 * 60;


        private float healthValue = 1.2f;
        private int spelldamageValue = 120;

        private void Init(FightSim sim)
        {
            spelldamageValue =
                (int) (spelldamageValue * (1f + 0.1 * sim.Player.GetTalentRank(Talents.Warlock_DemonicAegis)));
            healthValue = (int) (healthValue * (1f + 0.1 * sim.Player.GetTalentRank(Talents.Warlock_DemonicAegis)));

            ActionDelegate = delegate
            {
                var buff = new BuffSim(Name, Id);
                buff.DurationTotal = totalDuration * 1000;
                buff.DurationRemaining = buff.DurationTotal;
                buff.TickActionDelegate = simBuff =>
                {
                    simBuff.LoggerSim.TickDebuff(simBuff.Time, buff);

                    simBuff.Player.SpellDamageMod -= spelldamageValue;

                    simBuff.LoggerSim.FadeDebuff(simBuff.Time, buff);
                    simBuff.RemoveDebuff(Id);
                };

                var hit = sim.Buff(buff, EnumSim.SchoolType.Shadow, Id, false);

                if (hit) sim.Player.SpellDamageMod += spelldamageValue;
            };
        }
    }
}