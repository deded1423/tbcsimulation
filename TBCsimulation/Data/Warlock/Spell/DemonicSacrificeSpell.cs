﻿using System;
using TBCsimulation.Data.TalentsInfo;
using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;

namespace TBCsimulation.Data.Warlock.Spell
{
    public partial class DemonicSacrificeSpell : SpellsInfo.Spell
    {
        private const int totalDuration = 30 * 60;
        private const int tickInterval = 4;
        private const float impFireValue = 1.15f;
        private const float voidwalkerHealthValue = 0.02f;
        private const float succShadowValue = 1.15f;
        private const float felhunterManaValue = 0.03f;
        private const float felguardShadowValue = 1.1f;
        private const float felguardManaValue = 0.02f;


        private void Init(FightSim sim)
        {
            ActionDelegate = delegate
            {
                BuffSim buff = null;

                if (sim.HasBuff(Talents.Warlock_DemonicSacrifice_Felguard))
                {
                    sim.RemoveBuff(Talents.Warlock_DemonicSacrifice_Felguard);
                    sim.RemoveBuffEvents(Talents.Warlock_DemonicSacrifice_Felguard);
                }

                if (sim.HasBuff(Talents.Warlock_DemonicSacrifice_Felhunter))
                {
                    sim.RemoveBuff(Talents.Warlock_DemonicSacrifice_Felhunter);
                    sim.RemoveBuffEvents(Talents.Warlock_DemonicSacrifice_Felhunter);
                }

                if (sim.HasBuff(Talents.Warlock_DemonicSacrifice_Imp))
                {
                    sim.RemoveBuff(Talents.Warlock_DemonicSacrifice_Imp);
                    sim.RemoveBuffEvents(Talents.Warlock_DemonicSacrifice_Imp);
                }

                if (sim.HasBuff(Talents.Warlock_DemonicSacrifice_Succubus))
                {
                    sim.RemoveBuff(Talents.Warlock_DemonicSacrifice_Succubus);
                    sim.RemoveBuffEvents(Talents.Warlock_DemonicSacrifice_Succubus);
                }

                if (sim.HasBuff(Talents.Warlock_DemonicSacrifice_Voidwalker))
                {
                    sim.RemoveBuff(Talents.Warlock_DemonicSacrifice_Voidwalker);
                    sim.RemoveBuffEvents(Talents.Warlock_DemonicSacrifice_Voidwalker);
                }

                switch (sim.Pet.Id)
                {
                    //Felguard - Touch of Shadow 35701
                    case PetSim.Warlock_Felguard:
                        buff = new BuffSim("Touch of Shadow", Talents.Warlock_DemonicSacrifice_Felguard);
                        buff.DurationTotal = totalDuration * 1000;
                        buff.DurationRemaining = buff.DurationTotal;
                        buff.TickInterval = tickInterval * 1000;
                        buff.TickActionDelegate = simBuff =>
                        {
                            simBuff.LoggerSim.TickDebuff(simBuff.Time, buff);

                            simBuff.ManaGained(buff.Id, (int) (simBuff.Player.GetMaxMana() * felguardManaValue));

                            buff.DurationRemaining -= buff.TickInterval;

                            if (buff.DurationRemaining > 0)
                            {
                                simBuff.EnqueueHeap(new HeapEventSim("Tick " + Name, buff.TickInterval, buff));
                            }
                            else
                            {
                                simBuff.LoggerSim.FadeDebuff(simBuff.Time, buff);
                                simBuff.RemoveDebuff(Id);
                            }
                        };
                        sim.Buff(buff, EnumSim.SchoolType.Shadow, Id, true);
                        break;
                    //Felhunter - Fel Energy 18792
                    case PetSim.Warlock_Felhunter:
                        buff = new BuffSim("Fel Energy", Talents.Warlock_DemonicSacrifice_Felhunter);
                        buff.DurationTotal = totalDuration * 1000;
                        buff.DurationRemaining = buff.DurationTotal;
                        buff.TickInterval = tickInterval * 1000;
                        buff.TickActionDelegate = simBuff =>
                        {
                            simBuff.LoggerSim.TickDebuff(simBuff.Time, buff);

                            simBuff.ManaGained(buff.Id, (int) (simBuff.Player.GetMaxMana() * felhunterManaValue));

                            buff.DurationRemaining -= buff.TickInterval;

                            if (buff.DurationRemaining > 0)
                            {
                                simBuff.EnqueueHeap(new HeapEventSim("Tick " + Name, buff.TickInterval, buff));
                            }
                            else
                            {
                                simBuff.LoggerSim.FadeDebuff(simBuff.Time, buff);
                                simBuff.RemoveDebuff(Id);
                            }
                        };
                        sim.Buff(buff, EnumSim.SchoolType.Shadow, Id, true);
                        break;
                    //Imp - Burning Wish 18789
                    case PetSim.Warlock_Imp:
                        buff = new BuffSim("Burning Wish", Talents.Warlock_DemonicSacrifice_Imp);
                        buff.DurationTotal = totalDuration * 1000;
                        buff.DurationRemaining = buff.DurationTotal;
                        buff.TickActionDelegate = simBuff =>
                        {
                            simBuff.LoggerSim.TickDebuff(simBuff.Time, buff);

                            simBuff.LoggerSim.FadeDebuff(simBuff.Time, buff);
                            simBuff.RemoveDebuff(Id);
                        };
                        sim.Buff(buff, EnumSim.SchoolType.Shadow, Id, false);
                        break;
                    //Succubus - Touch of Shadow 18791
                    case PetSim.Warlock_Succubus:
                        buff = new BuffSim("Touch of Shadow", Talents.Warlock_DemonicSacrifice_Succubus);
                        buff.DurationTotal = totalDuration * 1000;
                        buff.DurationRemaining = buff.DurationTotal;
                        buff.TickActionDelegate = simBuff =>
                        {
                            simBuff.LoggerSim.TickDebuff(simBuff.Time, buff);

                            simBuff.LoggerSim.FadeDebuff(simBuff.Time, buff);
                            simBuff.RemoveDebuff(Id);
                        };
                        sim.Buff(buff, EnumSim.SchoolType.Shadow, Id, false);
                        break;
                    //Voidwalker - Fel Stamina 18790
                    case PetSim.Warlock_Voidwalker:
                        buff = new BuffSim("Fel Stamina", Talents.Warlock_DemonicSacrifice_Voidwalker);
                        buff.DurationTotal = totalDuration * 1000;
                        buff.DurationRemaining = buff.DurationTotal;
                        buff.TickInterval = tickInterval * 1000;
                        buff.TickActionDelegate = simBuff =>
                        {
                            simBuff.LoggerSim.TickDebuff(simBuff.Time, buff);

                            simBuff.Heal(buff.Id, (int) (simBuff.Player.GetMaxHealth() * voidwalkerHealthValue));

                            buff.DurationRemaining -= buff.TickInterval;

                            if (buff.DurationRemaining > 0)
                            {
                                simBuff.EnqueueHeap(new HeapEventSim("Tick " + Name, buff.TickInterval, buff));
                            }
                            else
                            {
                                simBuff.LoggerSim.FadeDebuff(simBuff.Time, buff);
                                simBuff.RemoveDebuff(Id);
                            }
                        };
                        sim.Buff(buff, EnumSim.SchoolType.Shadow, Id, true);
                        break;
                    default:
                        throw new NotSupportedException();
                }
            };
        }
    }
}