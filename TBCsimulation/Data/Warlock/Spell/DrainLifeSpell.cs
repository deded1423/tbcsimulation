﻿using System;
using TBCsimulation.Data.SpellsInfo;
using TBCsimulation.Data.TalentsInfo;
using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;

namespace TBCsimulation.Data.Warlock.Spell
{
    public partial class DrainLifeSpell : SpellsInfo.Spell
    {
        private void Init(FightSim sim)
        {
            ActionDelegate = delegate
            {
                var damage = 108 + (int) Math.Floor(0.715 / 5 * sim.Player.GetSpellShadowDamage());

                if (sim.Player.HasTalent(Talents.Warlock_SoulSiphon))
                {
                    var stacks = (int) sim.GetParam(ParameterSim.NumberofAfflictionSpells);
                    foreach (var buff in sim.Debuffs)
                        if (buff.Id.Equals(SpellId.Warlock_CurseofAgony) ||
                            buff.Id.Equals(SpellId.Warlock_CurseofDoom) ||
                            buff.Id.Equals(SpellId.Warlock_CurseofExhaustion) ||
                            buff.Id.Equals(SpellId.Warlock_CurseofRecklessness) ||
                            buff.Id.Equals(SpellId.Warlock_CurseofTongues) ||
                            buff.Id.Equals(SpellId.Warlock_CurseofWeakness) ||
                            buff.Id.Equals(SpellId.Warlock_CurseoftheElements) ||
                            buff.Id.Equals(SpellId.Warlock_DeathCoil) ||
                            buff.Id.Equals(SpellId.Warlock_DrainLife) ||
                            buff.Id.Equals(SpellId.Warlock_DrainMana) ||
                            buff.Id.Equals(SpellId.Warlock_DrainSoul) ||
                            buff.Id.Equals(SpellId.Warlock_Fear) ||
                            buff.Id.Equals(SpellId.Warlock_HowlofTerror) ||
                            buff.Id.Equals(SpellId.Warlock_SeedofCorruption) ||
                            buff.Id.Equals(SpellId.Warlock_SiphonLife) ||
                            buff.Id.Equals(SpellId.Warlock_UnstableAffliction))
                            stacks++;

                    if (sim.Player.GetTalentRank(Talents.Warlock_SoulSiphon) == 1)
                        stacks = Math.Min(12, stacks);
                    else
                        stacks = Math.Min(15, stacks);

                    damage = (int) (damage *
                                    (1 + stacks * 0.02 * sim.Player.GetTalentRank(Talents.Warlock_SoulSiphon)));
                }

                if (RandomSim.CheckChance(2 * sim.Player.GetTalentRank(Talents.Warlock_Nightfall)))
                {
                    var buff = new BuffSim("Nightfall", Talents.Warlock_Nightfall);
                    buff.DurationTotal = 10 * 1000;
                    buff.DurationRemaining = buff.DurationTotal;
                    buff.TickActionDelegate = simBuff =>
                    {
                        simBuff.LoggerSim.TickDebuff(simBuff.Time, buff);
                        simBuff.LoggerSim.FadeBuff(simBuff.Time, buff);
                        simBuff.RemoveBuff(Id);
                    };

                    sim.Buff(buff, EnumSim.SchoolType.Shadow, Id, false);
                }

                //int hitChance = sim.Player.GetTalentRank(Talents.Suppression) * 2;

                damage = sim.Damage(EnumSim.DamageType.Spell, EnumSim.SchoolType.Shadow, Id, damage, 1, true);
                if (damage > 0) sim.Heal(Id, damage);
            };
        }
    }
}