﻿using System;
using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;

namespace TBCsimulation.Data.Warlock.Spell
{
    public partial class DrainSoulSpell : SpellsInfo.Spell
    {
        private void Init(FightSim sim)
        {
            ActionDelegate = delegate
            {
                var damage = 124 + (int) Math.Floor(2.143 / 5 * sim.Player.GetSpellShadowDamage());

                //int hitChance = sim.Player.GetTalentRank(Talents.Suppression) * 2;

                damage = sim.Damage(EnumSim.DamageType.Spell, EnumSim.SchoolType.Shadow, Id, damage, 1, true);
            };
        }
    }
}