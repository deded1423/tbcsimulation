﻿using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;

namespace TBCsimulation.Data.Warlock.Spell
{
    public partial class DemonSkinSpell : SpellsInfo.Spell
    {
        private const int totalDuration = 30 * 60;
        private const int armorValue = 120;
        private const float hp5Value = 5f;


        private void Init(FightSim sim)
        {
            ActionDelegate = delegate
            {
                var buff = new BuffSim(Name, Id);
                buff.DurationTotal = totalDuration * 1000;
                buff.DurationRemaining = buff.DurationTotal;
                buff.TickActionDelegate = simBuff =>
                {
                    simBuff.LoggerSim.TickDebuff(simBuff.Time, buff);

                    simBuff.Player.DefenseArmorMod -= armorValue;
                    simBuff.Player.HealthP5Mod -= hp5Value;

                    simBuff.LoggerSim.FadeDebuff(simBuff.Time, buff);
                    simBuff.RemoveDebuff(Id);
                };

                var hit = sim.Buff(buff, EnumSim.SchoolType.Shadow, Id, false);

                if (hit)
                {
                    sim.Player.DefenseArmorMod += armorValue;
                    sim.Player.HealthP5Mod += hp5Value;
                }
            };
        }
    }
}