﻿using System;
using TBCsimulation.Data.TalentsInfo;
using TBCsimulation.Simulation;

namespace TBCsimulation.Data.Warlock.Spell
{
    public partial class LifeTapSpell : SpellsInfo.Spell
    {
        private void Init(FightSim sim)
        {
            Mana = 0;
            ActionDelegate = delegate
            {
                var health = 582 + (int) Math.Floor(0.8 * sim.Player.GetSpellShadowDamage());
                sim.HealthLost(Id, health);
                health = (int) (health * (1 + 0.1 * sim.Player.GetTalentRank(Talents.Warlock_ImprovedLifeTap)));
                sim.ManaGained(Id, health);
            };
        }
    }
}