﻿using TBCsimulation.Data.TalentsInfo;
using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;

namespace TBCsimulation.Data.Warlock.Spell
{
    public partial class CurseoftheElementsSpell : SpellsInfo.Spell
    {
        private const int totalDuration = 300;
        private const int resistValue = 88;
        private const float damagetakenValue = 1.1f;


        private void Init(FightSim sim)
        {
            ActionDelegate = delegate
            {
                var buff = new BuffSim(Name, Id);
                buff.DurationTotal = totalDuration * 1000;
                buff.DurationRemaining = buff.DurationTotal;
                buff.TickActionDelegate = simBuff =>
                {
                    simBuff.LoggerSim.TickDebuff(simBuff.Time, buff);

                    simBuff.Boss.ResistanceArcaneMod += resistValue;
                    simBuff.Boss.ResistanceFireMod += resistValue;
                    simBuff.Boss.ResistanceFrostMod += resistValue;
                    simBuff.Boss.ResistanceShadowMod += resistValue;

                    simBuff.LoggerSim.FadeDebuff(simBuff.Time, buff);
                    simBuff.RemoveDebuff(Id);
                };

                var hitChance = sim.Player.GetTalentRank(Talents.Warlock_Suppression) * 2;

                var hit = sim.Debuff(buff, EnumSim.SchoolType.Shadow, Id, false, false, true,
                    sim.Player.GetSpellHitChance() + hitChance);

                if (hit)
                {
                    sim.Boss.ResistanceArcaneMod -= resistValue;
                    sim.Boss.ResistanceFireMod -= resistValue;
                    sim.Boss.ResistanceFrostMod -= resistValue;
                    sim.Boss.ResistanceShadowMod -= resistValue;
                }

                if (hit)
                {
                    if (sim.HasDebuff("Curse of Agony"))
                    {
                        sim.RemoveBuff("Curse of Agony");
                        sim.RemoveBuffEvents("Curse of Agony");
                    }

                    if (sim.HasDebuff("Curse of Doom"))
                    {
                        sim.RemoveBuff("Curse of Doom");
                        sim.RemoveBuffEvents("Curse of Doom");
                    }

                    if (sim.HasDebuff("Curse of Exhaustion"))
                    {
                        sim.RemoveBuff("Curse of Exhaustion");
                        sim.RemoveBuffEvents("Curse of Exhaustion");
                    }

                    if (sim.HasDebuff("Curse of Recklessness"))
                    {
                        sim.RemoveBuff("Curse of Recklessness");
                        sim.RemoveBuffEvents("Curse of Recklessness");
                    }

                    if (sim.HasDebuff("Curse of Tongues"))
                    {
                        sim.RemoveBuff("Curse of Tongues");
                        sim.RemoveBuffEvents("Curse of Tongues");
                    }

                    if (sim.HasDebuff("Curse of Weakness"))
                    {
                        sim.RemoveBuff("Curse of Weakness");
                        sim.RemoveBuffEvents("Curse of Weakness");
                    }
                }
            };
        }
    }
}