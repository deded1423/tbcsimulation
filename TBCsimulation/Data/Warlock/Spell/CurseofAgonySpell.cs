﻿using System;
using TBCsimulation.Data.SpellsInfo;
using TBCsimulation.Data.TalentsInfo;
using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;

namespace TBCsimulation.Data.Warlock.Spell
{
    public partial class CurseofAgonySpell : SpellsInfo.Spell
    {
        private const int totalDuration = 24;
        private const int tickInterval = 2;
        private const int ticks = 24;

        private void Init(FightSim sim)
        {
            ActionDelegate = delegate
            {
                var phase = 1;
                var phaseNumber = 0;
                var damage = 1356;
                if (sim.HasBuff(SpellId.Warlock_AmplifyCurse))
                    damage = (int) (damage * 1.5f);
                damage += (int) Math.Floor(1.2 * sim.Player.GetSpellShadowDamage());
                damage /= ticks;

                damage = (int) (damage * (1 + 0.01f * sim.Player.GetTalentRank(Talents.Warlock_Contagion)));

                var buff = new BuffSim(Name, Id);
                buff.DurationTotal = totalDuration * 1000;
                buff.DurationRemaining = buff.DurationTotal;
                buff.TickInterval = tickInterval * 1000;
                buff.TickActionDelegate = simBuff =>
                {
                    simBuff.LoggerSim.TickDebuff(simBuff.Time, buff);

                    simBuff.Damage(EnumSim.DamageType.Spell, EnumSim.SchoolType.Shadow, Id, damage * phase, 1, true);
                    phaseNumber += 1;

                    if (phaseNumber == 3)
                    {
                        phaseNumber = 0;
                        phase += 1;
                    }

                    buff.DurationRemaining -= buff.TickInterval;
                    if (buff.DurationRemaining > 0)
                    {
                        simBuff.EnqueueHeap(new HeapEventSim("Tick " + Name, buff.TickInterval, buff));
                    }
                    else
                    {
                        simBuff.LoggerSim.FadeDebuff(simBuff.Time, buff);
                        simBuff.RemoveDebuff(Id);
                    }
                };

                var before = sim.Debuffs.Find(b => { return b.Id.Equals(Id); });
                if (before != null)
                {
                    sim.RemoveBuffEvents(before);
                    sim.RemoveDebuff(before);
                }

                var hitChance = sim.Player.GetTalentRank(Talents.Warlock_Suppression) * 2;

                var hit = sim.Debuff(buff, EnumSim.SchoolType.Shadow, Id, false, true, true,
                    sim.Player.GetSpellHitChance() + hitChance);

                if (hit)
                {
                    if (sim.HasDebuff("Curse of Doom"))
                    {
                        sim.RemoveBuff("Curse of Doom");
                        sim.RemoveBuffEvents("Curse of Doom");
                    }

                    if (sim.HasDebuff("Curse of Exhaustion"))
                    {
                        sim.RemoveBuff("Curse of Exhaustion");
                        sim.RemoveBuffEvents("Curse of Exhaustion");
                    }

                    if (sim.HasDebuff("Curse of Recklessness"))
                    {
                        sim.RemoveBuff("Curse of Recklessness");
                        sim.RemoveBuffEvents("Curse of Recklessness");
                    }

                    if (sim.HasDebuff("Curse of the Elements"))
                    {
                        sim.RemoveBuff("Curse of the Elements");
                        sim.RemoveBuffEvents("Curse of the Elements");
                    }

                    if (sim.HasDebuff("Curse of Tongues"))
                    {
                        sim.RemoveBuff("Curse of Tongues");
                        sim.RemoveBuffEvents("Curse of Tongues");
                    }

                    if (sim.HasDebuff("Curse of Weakness"))
                    {
                        sim.RemoveBuff("Curse of Weakness");
                        sim.RemoveBuffEvents("Curse of Weakness");
                    }
                }
            };
        }
    }
}