﻿using System;
using TBCsimulation.Data.TalentsInfo;
using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;

namespace TBCsimulation.Data.Warlock.Spell
{
    public partial class IncinerateSpell : SpellsInfo.Spell
    {
        private void Init(FightSim sim)
        {
            Mana = (int) (Mana * (1f - 0.01f * sim.Player.GetTalentRank(Talents.Warlock_Cataclysm)));
            CastTime = (int) (CastTime * (1 - 0.02f * sim.Player.GetTalentRank(Talents.Warlock_Emberstorm)));

            ActionDelegate = delegate
            {
                var damage = RandomSim.GetInt(444, 514) +
                             (int) Math.Floor(
                                 (0.714 + 0.04 * sim.Player.GetTalentRank(Talents.Warlock_ShadowandFlame)) *
                                 sim.Player.GetSpellFireDamage());
                if (sim.HasDebuff(27215)) damage = RandomSim.GetInt(111, 129) + damage;

                damage = sim.Damage(EnumSim.DamageType.Spell, EnumSim.SchoolType.Fire, Id, damage, 1, false, true,
                    sim.Player.GetSpellHitChance(), true,
                    sim.Player.GetSpellFireCritChance(), sim.Player.GetSpellCritDamage() * 2);

                if (RandomSim.CheckChance(10 * sim.Player.GetTalentRank(Talents.Warlock_SoulLeech)))
                    sim.Heal(Talents.Warlock_SoulLeech, (int) (damage * 0.2f));
            };
        }
    }
}