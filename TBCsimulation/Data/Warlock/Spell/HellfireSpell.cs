﻿using System;
using TBCsimulation.Data.TalentsInfo;
using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;

namespace TBCsimulation.Data.Warlock.Spell
{
    public partial class HellfireSpell : SpellsInfo.Spell
    {
        private void Init(FightSim sim)
        {
            Mana = (int) (Mana * (1f - 0.01f * sim.Player.GetTalentRank(Talents.Warlock_Cataclysm)));

            ActionDelegate = delegate
            {
                var damage = 307 + (int) Math.Floor(2.143 / 15 * sim.Player.GetSpellFireDamage());
                sim.Damage(EnumSim.DamageType.Spell, EnumSim.SchoolType.Fire, Id, damage, 1, true, true,
                    sim.Player.GetSpellHitChance(),
                    true,
                    sim.Player.GetSpellFireCritChance(), sim.Player.GetSpellCritDamage() * 2);

                var health = 307 + (int) Math.Floor(1.638 / 15 * sim.Player.GetSpellFireDamage());
                sim.HealthLost(Id, health);
            };
        }
    }
}