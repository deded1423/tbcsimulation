﻿using System;
using TBCsimulation.Data.TalentsInfo;
using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;

namespace TBCsimulation.Data.Warlock.Spell
{
    public partial class DeathCoilSpell : SpellsInfo.Spell
    {
        private void Init(FightSim sim)
        {
            ActionDelegate = delegate
            {
                var damage = 526 + (int) Math.Floor(0.212 * sim.Player.GetSpellShadowDamage());

                var hitChance = sim.Player.GetTalentRank(Talents.Warlock_Suppression) * 2;

                damage = sim.Damage(EnumSim.DamageType.Spell, EnumSim.SchoolType.Shadow, Id, damage, 1, false, true,
                    sim.Player.GetSpellHitChance() + hitChance, true, sim.Player.GetSpellShadowCritChance(),
                    sim.Player.GetSpellCritDamage());
                if (damage > 0) sim.Heal(Id, damage);
            };
        }
    }
}