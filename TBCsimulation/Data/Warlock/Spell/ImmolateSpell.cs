﻿using System;
using TBCsimulation.Data.TalentsInfo;
using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;

namespace TBCsimulation.Data.Warlock.Spell
{
    public partial class ImmolateSpell : SpellsInfo.Spell
    {
        private const int totalDuration = 15;
        private const int tickInterval = 3;
        private const int ticks = totalDuration / tickInterval;


        private void Init(FightSim sim)
        {
            CastTime -= 100 * sim.Player.GetTalentRank(Talents.Warlock_Bane);

            ActionDelegate = delegate
            {
                var buff = new BuffSim(Name, Id);
                buff.DurationTotal = totalDuration * 1000;
                buff.DurationRemaining = buff.DurationTotal;
                buff.TickInterval = tickInterval * 1000;
                buff.TickActionDelegate = simBuff =>
                {
                    simBuff.LoggerSim.TickDebuff(simBuff.Time, buff);

                    var damageBuff = 615 + (int) Math.Floor(0.653f * simBuff.Player.GetSpellFireDamage());
                    damageBuff = damageBuff / ticks;
                    simBuff.Damage(EnumSim.DamageType.Spell, EnumSim.SchoolType.Fire, Id, damageBuff, 1, true);

                    if (buff.DurationRemaining > 0)
                    {
                        buff.DurationRemaining -= buff.TickInterval;
                        simBuff.EnqueueHeap(new HeapEventSim("Tick " + Name, buff.TickInterval, buff));
                    }
                    else
                    {
                        simBuff.LoggerSim.FadeDebuff(simBuff.Time, buff);
                        simBuff.RemoveDebuff(Id);
                    }
                };

                var damage = RandomSim.GetInt(331, 332) + (int) Math.Floor(0.198f * sim.Player.GetSpellFireDamage());
                var multiplier = 1 + 0.05f * sim.Player.GetTalentRank(Talents.Warlock_ImprovedImmolate);
                var crit = sim.Player.GetSpellFireCritChance() +
                           1 * sim.Player.GetTalentRank(Talents.Warlock_Devastation);
                var hit = sim.Damage(EnumSim.DamageType.Spell, EnumSim.SchoolType.Fire, Id, damage, multiplier, false,
                    true,
                    sim.Player.GetSpellHitChance(), true, crit, sim.Player.GetSpellCritDamage() * 2) != -1;

                if (hit)
                    sim.Debuff(buff, EnumSim.SchoolType.Fire, Id, false, true);
            };
        }
    }
}