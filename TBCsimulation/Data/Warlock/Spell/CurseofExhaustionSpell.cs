﻿using System;
using TBCsimulation.Data.TalentsInfo;
using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;

namespace TBCsimulation.Data.Warlock.Spell
{
    public partial class CurseofExhaustionSpell : SpellsInfo.Spell
    {
        private const int totalDuration = 12;
        private const float movementValue = 0.3f;


        private void Init(FightSim sim)
        {
            ActionDelegate = delegate
            {
                var buff = new BuffSim(Name, Id);
                buff.DurationTotal = totalDuration * 1000;
                buff.DurationRemaining = buff.DurationTotal;
                buff.TickActionDelegate = simBuff =>
                {
                    simBuff.LoggerSim.TickDebuff(simBuff.Time, buff);

                    //sim.Boss.GetMovementSpeed() += movementValue;

                    simBuff.LoggerSim.FadeDebuff(simBuff.Time, buff);
                    simBuff.RemoveDebuff(Id);
                };


                var hitChance = sim.Player.GetTalentRank(Talents.Warlock_Suppression) * 2;

                var hit = sim.Debuff(buff, EnumSim.SchoolType.Shadow, Id, false, false, true,
                    sim.Player.GetSpellHitChance() + hitChance);

                if (hit)
                {
                    if (sim.HasDebuff("Curse of Agony"))
                    {
                        sim.RemoveBuff("Curse of Agony");
                        sim.RemoveBuffEvents("Curse of Agony");
                    }

                    if (sim.HasDebuff("Curse of Doom"))
                    {
                        sim.RemoveBuff("Curse of Doom");
                        sim.RemoveBuffEvents("Curse of Doom");
                    }

                    if (sim.HasDebuff("Curse of Recklessness"))
                    {
                        sim.RemoveBuff("Curse of Recklessness");
                        sim.RemoveBuffEvents("Curse of Recklessness");
                    }

                    if (sim.HasDebuff("Curse of the Elements"))
                    {
                        sim.RemoveBuff("Curse of the Elements");
                        sim.RemoveBuffEvents("Curse of the Elements");
                    }

                    if (sim.HasDebuff("Curse of Tongues"))
                    {
                        sim.RemoveBuff("Curse of Tongues");
                        sim.RemoveBuffEvents("Curse of Tongues");
                    }

                    if (sim.HasDebuff("Curse of Weakness"))
                    {
                        sim.RemoveBuff("Curse of Weakness");
                        sim.RemoveBuffEvents("Curse of Weakness");
                    }
                }

                throw new NotImplementedException();
            };
        }
    }
}