﻿using TBCsimulation.Data.TalentsInfo;
using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;

namespace TBCsimulation.Data.Warlock.Spell
{
    public partial class DemonArmorSpell : SpellsInfo.Spell
    {
        private const int totalDuration = 30 * 60;


        private int armorValue = 660;
        private float hp5Value = 18f;
        private int resistValue = 18;

        private void Init(FightSim sim)
        {
            armorValue = (int) (armorValue * (1f + 0.1 * sim.Player.GetTalentRank(Talents.Warlock_DemonicAegis)));
            resistValue = (int) (resistValue * (1f + 0.1 * sim.Player.GetTalentRank(Talents.Warlock_DemonicAegis)));
            hp5Value = (int) (hp5Value * (1f + 0.1 * sim.Player.GetTalentRank(Talents.Warlock_DemonicAegis)));

            ActionDelegate = delegate
            {
                var buff = new BuffSim(Name, Id);
                buff.DurationTotal = totalDuration * 1000;
                buff.DurationRemaining = buff.DurationTotal;
                buff.TickActionDelegate = simBuff =>
                {
                    simBuff.LoggerSim.TickDebuff(simBuff.Time, buff);

                    simBuff.Player.DefenseArmorMod -= armorValue;
                    simBuff.Player.ResistanceShadowMod -= resistValue;
                    simBuff.Player.HealthP5Mod -= hp5Value;

                    simBuff.LoggerSim.FadeDebuff(simBuff.Time, buff);
                    simBuff.RemoveDebuff(Id);
                };

                var hit = sim.Buff(buff, EnumSim.SchoolType.Shadow, Id, false);

                if (hit)
                {
                    sim.Player.DefenseArmorMod += armorValue;
                    sim.Player.ResistanceShadowMod += resistValue;
                    sim.Player.HealthP5Mod += hp5Value;
                }
            };
        }
    }
}