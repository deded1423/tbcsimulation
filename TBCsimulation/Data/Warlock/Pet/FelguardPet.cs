﻿using TBCsimulation.Simulation;

namespace TBCsimulation.Data.Warlock.Pet
{
    public class FelguardPet : PetSim
    {
        public FelguardPet()
        {
            Id = 17252;
            Name = "Felguard";
        }
    }
}