﻿using TBCsimulation.Simulation;

namespace TBCsimulation.Data.Warlock.Pet
{
    public class FelhunterPet : PetSim
    {
        public FelhunterPet()
        {
            Id = 417;
            Name = "Felhunter";
        }
    }
}