﻿using TBCsimulation.Simulation;

namespace TBCsimulation.Data.Warlock.Pet
{
    public class VoidwalkerPet : PetSim
    {
        public VoidwalkerPet()
        {
            Id = 1860;
            Name = "Voidwalker";
        }
    }
}