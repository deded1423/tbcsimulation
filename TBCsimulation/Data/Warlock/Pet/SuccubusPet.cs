﻿using TBCsimulation.Simulation;

namespace TBCsimulation.Data.Warlock.Pet
{
    public class SuccubusPet : PetSim
    {
        public SuccubusPet()
        {
            Id = 1863;
            Name = "Succubus";
        }
    }
}