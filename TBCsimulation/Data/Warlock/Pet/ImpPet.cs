﻿using TBCsimulation.Simulation;

namespace TBCsimulation.Data.Warlock.Pet
{
    public class ImpPet : PetSim
    {
        public ImpPet()
        {
            Id = 416;
            Name = "Imp";
        }
    }
}