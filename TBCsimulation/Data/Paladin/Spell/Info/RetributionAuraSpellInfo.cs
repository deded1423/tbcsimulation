using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Paladin.Spell
{
    public partial class RetributionAuraSpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public RetributionAuraSpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=27150/retribution-aura
            Name = "Retribution Aura";
            Id = 27150;
            Level = 66;
            Rank = 6;
            Range = 0;

            Mana = -1;

            IsGCD = true;
            Channeled = false;
            CastTime = 0;
            CooldownTime = 0;

            School = EnumSim.SchoolType.Holy;
            SecondarySchool = EnumSim.SecondarySchoolType.Paladin_Retribution;
            Init(sim);
        }
    }
}