using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Paladin.Spell
{
    public partial class DivineShieldSpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public DivineShieldSpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=1020/divine-shield
            Name = "Divine Shield";
            Id = 1020;
            Level = 50;
            Rank = 2;
            Range = 0;

            Mana = 110;

            IsGCD = true;
            Channeled = false;
            CastTime = 0;
            CooldownTime = 300000;

            School = EnumSim.SchoolType.Holy;
            SecondarySchool = EnumSim.SecondarySchoolType.Paladin_Protection;
            Init(sim);
        }
    }
}