using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Paladin.Spell
{
    public partial class GreaterBlessingofKingsSpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public GreaterBlessingofKingsSpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=25898/greater-blessing-of-kings
            Name = "Greater Blessing of Kings";
            Id = 25898;
            Level = 60;
            Rank = 1;
            Range = 40;

            Mana = -1;

            IsGCD = true;
            Channeled = false;
            CastTime = 0;
            CooldownTime = 0;

            School = EnumSim.SchoolType.Holy;
            SecondarySchool = EnumSim.SecondarySchoolType.Paladin_Protection;
            Init(sim);
        }
    }
}