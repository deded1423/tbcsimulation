using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Paladin.Spell
{
    public partial class HammerofWrathSpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public HammerofWrathSpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=27180/hammer-of-wrath
            Name = "Hammer of Wrath";
            Id = 27180;
            Level = 68;
            Rank = 4;
            Range = 30;

            Mana = 440;

            IsGCD = false;
            Channeled = false;
            CastTime = 500000;
            CooldownTime = 6000;

            School = EnumSim.SchoolType.Holy;
            SecondarySchool = EnumSim.SecondarySchoolType.Paladin_Holy;
            Init(sim);
        }
    }
}