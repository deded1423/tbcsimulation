using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Paladin.Spell
{
    public partial class GreaterBlessingofSanctuarySpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public GreaterBlessingofSanctuarySpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=27169/greater-blessing-of-sanctuary
            Name = "Greater Blessing of Sanctuary";
            Id = 27169;
            Level = 70;
            Rank = 2;
            Range = 40;

            Mana = 360;

            IsGCD = true;
            Channeled = false;
            CastTime = 0;
            CooldownTime = 0;

            School = EnumSim.SchoolType.Holy;
            SecondarySchool = EnumSim.SecondarySchoolType.Paladin_Protection;
            Init(sim);
        }
    }
}