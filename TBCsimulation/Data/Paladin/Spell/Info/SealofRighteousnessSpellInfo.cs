using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Paladin.Spell
{
    public partial class SealofRighteousnessSpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public SealofRighteousnessSpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=27155/seal-of-righteousness
            Name = "Seal of Righteousness";
            Id = 27155;
            Level = 66;
            Rank = 9;
            Range = 0;

            Mana = 260;

            IsGCD = true;
            Channeled = false;
            CastTime = 0;
            CooldownTime = 0;

            School = EnumSim.SchoolType.Holy;
            SecondarySchool = EnumSim.SecondarySchoolType.Paladin_Holy;
            Init(sim);
        }
    }
}