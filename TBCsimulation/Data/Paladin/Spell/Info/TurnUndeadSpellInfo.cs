using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Paladin.Spell
{
    public partial class TurnUndeadSpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public TurnUndeadSpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=5627/turn-undead
            Name = "Turn Undead";
            Id = 5627;
            Level = 38;
            Rank = 2;
            Range = 20;

            Mana = 50;

            IsGCD = true;
            Channeled = false;
            CastTime = 1500;
            CooldownTime = 30000;

            School = EnumSim.SchoolType.Holy;
            SecondarySchool = EnumSim.SecondarySchoolType.Paladin_Holy;
            Init(sim);
        }
    }
}