using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Paladin.Spell
{
    public partial class BlessingofWisdomSpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public BlessingofWisdomSpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=27142/blessing-of-wisdom
            Name = "Blessing of Wisdom";
            Id = 27142;
            Level = 65;
            Rank = 7;
            Range = 30;

            Mana = 150;

            IsGCD = true;
            Channeled = false;
            CastTime = 0;
            CooldownTime = 0;

            School = EnumSim.SchoolType.Holy;
            SecondarySchool = EnumSim.SecondarySchoolType.Paladin_Holy;
            Init(sim);
        }
    }
}