using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Paladin.Spell
{
    public partial class BlessingofProtectionSpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public BlessingofProtectionSpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=10278/blessing-of-protection
            Name = "Blessing of Protection";
            Id = 10278;
            Level = 38;
            Rank = 3;
            Range = 30;

            Mana = -1;

            IsGCD = true;
            Channeled = false;
            CastTime = 0;
            CooldownTime = 300000;

            School = EnumSim.SchoolType.Holy;
            SecondarySchool = EnumSim.SecondarySchoolType.Paladin_Protection;
            Init(sim);
        }
    }
}