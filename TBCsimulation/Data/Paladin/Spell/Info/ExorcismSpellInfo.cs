using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Paladin.Spell
{
    public partial class ExorcismSpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public ExorcismSpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=27138/exorcism
            Name = "Exorcism";
            Id = 27138;
            Level = 68;
            Rank = 7;
            Range = 30;

            Mana = 340;

            IsGCD = true;
            Channeled = false;
            CastTime = 0;
            CooldownTime = 15000;

            School = EnumSim.SchoolType.Holy;
            SecondarySchool = EnumSim.SecondarySchoolType.Paladin_Holy;
            Init(sim);
        }
    }
}