using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Paladin.Spell
{
    public partial class JudgementoftheCrusaderSpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public JudgementoftheCrusaderSpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=21183/judgement-of-the-crusader
            Name = "Judgement of the Crusader";
            Id = 21183;
            Level = 6;
            Rank = 1;
            Range = 100;

            Mana = -1;

            IsGCD = false;
            Channeled = false;
            CastTime = 0;
            CooldownTime = 0;

            School = EnumSim.SchoolType.Holy;
            SecondarySchool = EnumSim.SecondarySchoolType.Paladin_Retribution;
            Init(sim);
        }
    }
}