using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Paladin.Spell
{
    public partial class SealofWisdomSpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public SealofWisdomSpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=27166/seal-of-wisdom
            Name = "Seal of Wisdom";
            Id = 27166;
            Level = 67;
            Rank = 4;
            Range = 0;

            Mana = 270;

            IsGCD = true;
            Channeled = false;
            CastTime = 0;
            CooldownTime = 0;

            School = EnumSim.SchoolType.Holy;
            SecondarySchool = EnumSim.SecondarySchoolType.Paladin_Holy;
            Init(sim);
        }
    }
}