using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Paladin.Spell
{
    public partial class GreaterBlessingofMightSpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public GreaterBlessingofMightSpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=27141/greater-blessing-of-might
            Name = "Greater Blessing of Might";
            Id = 27141;
            Level = 70;
            Rank = 3;
            Range = 40;

            Mana = 295;

            IsGCD = true;
            Channeled = false;
            CastTime = 0;
            CooldownTime = 0;

            School = EnumSim.SchoolType.Holy;
            SecondarySchool = EnumSim.SecondarySchoolType.Paladin_Retribution;
            Init(sim);
        }
    }
}