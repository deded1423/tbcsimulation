using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Paladin.Spell
{
    public partial class SealoftheMartyrSpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public SealoftheMartyrSpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=348700/seal-of-the-martyr
            Name = "Seal of the Martyr";
            Id = 348700;
            Level = 70;
            Rank = 1;
            Range = 0;

            Mana = 210;

            IsGCD = true;
            Channeled = false;
            CastTime = 0;
            CooldownTime = 0;

            School = EnumSim.SchoolType.Holy;
            SecondarySchool = EnumSim.SecondarySchoolType.Paladin_Retribution;
            Init(sim);
        }
    }
}