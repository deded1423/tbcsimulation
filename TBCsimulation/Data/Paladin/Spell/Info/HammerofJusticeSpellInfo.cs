using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Paladin.Spell
{
    public partial class HammerofJusticeSpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public HammerofJusticeSpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=10308/hammer-of-justice
            Name = "Hammer of Justice";
            Id = 10308;
            Level = 54;
            Rank = 4;
            Range = 10;

            Mana = 100;

            IsGCD = true;
            Channeled = false;
            CastTime = 0;
            CooldownTime = 1000;

            School = EnumSim.SchoolType.Holy;
            SecondarySchool = EnumSim.SecondarySchoolType.Paladin_Protection;
            Init(sim);
        }
    }
}