using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Paladin.Spell
{
    public partial class RighteousFurySpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public RighteousFurySpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=25780/righteous-fury
            Name = "Righteous Fury";
            Id = 25780;
            Level = 16;
            Rank = 1;
            Range = 0;

            Mana = -1;

            IsGCD = true;
            Channeled = false;
            CastTime = 0;
            CooldownTime = 0;

            School = EnumSim.SchoolType.Holy;
            SecondarySchool = EnumSim.SecondarySchoolType.Paladin_Protection;
            Init(sim);
        }
    }
}