using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Paladin.Spell
{
    public partial class FlashofLightSpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public FlashofLightSpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=27137/flash-of-light
            Name = "Flash of Light";
            Id = 27137;
            Level = 66;
            Rank = 7;
            Range = 40;

            Mana = 180;

            IsGCD = true;
            Channeled = false;
            CastTime = 1500;
            CooldownTime = 0;

            School = EnumSim.SchoolType.Holy;
            SecondarySchool = EnumSim.SecondarySchoolType.Paladin_Holy;
            Init(sim);
        }
    }
}