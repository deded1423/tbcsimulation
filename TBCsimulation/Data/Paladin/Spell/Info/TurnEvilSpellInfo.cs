using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Paladin.Spell
{
    public partial class TurnEvilSpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public TurnEvilSpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=10326/turn-evil
            Name = "Turn Evil";
            Id = 10326;
            Level = 52;
            Rank = 1;
            Range = 20;

            Mana = 75;

            IsGCD = true;
            Channeled = false;
            CastTime = 1500;
            CooldownTime = 30000;

            School = EnumSim.SchoolType.Holy;
            SecondarySchool = EnumSim.SecondarySchoolType.Paladin_Holy;
            Init(sim);
        }
    }
}