using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Paladin.Spell
{
    public partial class RedemptionSpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public RedemptionSpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=20773/redemption
            Name = "Redemption";
            Id = 20773;
            Level = 60;
            Rank = 5;
            Range = 30;

            Mana = -1;

            IsGCD = true;
            Channeled = false;
            CastTime = 10000;
            CooldownTime = 0;

            School = EnumSim.SchoolType.Holy;
            SecondarySchool = EnumSim.SecondarySchoolType.Paladin_Holy;
            Init(sim);
        }
    }
}