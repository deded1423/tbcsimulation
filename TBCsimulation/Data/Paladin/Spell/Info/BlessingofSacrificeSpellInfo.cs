using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Paladin.Spell
{
    public partial class BlessingofSacrificeSpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public BlessingofSacrificeSpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=27148/blessing-of-sacrifice
            Name = "Blessing of Sacrifice";
            Id = 27148;
            Level = 70;
            Rank = 4;
            Range = 30;

            Mana = 135;

            IsGCD = true;
            Channeled = false;
            CastTime = 0;
            CooldownTime = 30000;

            School = EnumSim.SchoolType.Holy;
            SecondarySchool = EnumSim.SecondarySchoolType.Paladin_Protection;
            Init(sim);
        }
    }
}