using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Paladin.Spell
{
    public partial class FireResistanceAuraSpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public FireResistanceAuraSpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=27153/fire-resistance-aura
            Name = "Fire Resistance Aura";
            Id = 27153;
            Level = 70;
            Rank = 4;
            Range = 0;

            Mana = -1;

            IsGCD = true;
            Channeled = false;
            CastTime = 0;
            CooldownTime = 0;

            School = EnumSim.SchoolType.Holy;
            SecondarySchool = EnumSim.SecondarySchoolType.Paladin_Protection;
            Init(sim);
        }
    }
}