using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Paladin.Spell
{
    public partial class ConsecrationSpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public ConsecrationSpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=27173/consecration
            Name = "Consecration";
            Id = 27173;
            Level = 70;
            Rank = 6;
            Range = 0;

            Mana = 660;

            IsGCD = true;
            Channeled = false;
            CastTime = 0;
            CooldownTime = 8000;

            School = EnumSim.SchoolType.Holy;
            SecondarySchool = EnumSim.SecondarySchoolType.Paladin_Holy;
            Init(sim);
        }
    }
}