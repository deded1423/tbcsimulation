using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Paladin.Spell
{
    public partial class SealoftheCrusaderSpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public SealoftheCrusaderSpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=27158/seal-of-the-crusader
            Name = "Seal of the Crusader";
            Id = 27158;
            Level = 61;
            Rank = 7;
            Range = 0;

            Mana = 210;

            IsGCD = true;
            Channeled = false;
            CastTime = 0;
            CooldownTime = 0;

            School = EnumSim.SchoolType.Holy;
            SecondarySchool = EnumSim.SecondarySchoolType.Paladin_Retribution;
            Init(sim);
        }
    }
}