using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Paladin.Spell
{
    public partial class HolyWrathSpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public HolyWrathSpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=27139/holy-wrath
            Name = "Holy Wrath";
            Id = 27139;
            Level = 69;
            Rank = 3;
            Range = 0;

            Mana = 825;

            IsGCD = true;
            Channeled = false;
            CastTime = 2000;
            CooldownTime = 1000;

            School = EnumSim.SchoolType.Holy;
            SecondarySchool = EnumSim.SecondarySchoolType.Paladin_Holy;
            Init(sim);
        }
    }
}