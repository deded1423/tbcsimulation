using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Paladin.Spell
{
    public partial class DivineProtectionSpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public DivineProtectionSpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=5573/divine-protection
            Name = "Divine Protection";
            Id = 5573;
            Level = 18;
            Rank = 2;
            Range = 0;

            Mana = 35;

            IsGCD = true;
            Channeled = false;
            CastTime = 0;
            CooldownTime = 300000;

            School = EnumSim.SchoolType.Holy;
            SecondarySchool = EnumSim.SecondarySchoolType.Paladin_Protection;
            Init(sim);
        }
    }
}