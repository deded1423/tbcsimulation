using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Paladin.Spell
{
    public partial class GreaterBlessingofWisdomSpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public GreaterBlessingofWisdomSpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=27143/greater-blessing-of-wisdom
            Name = "Greater Blessing of Wisdom";
            Id = 27143;
            Level = 65;
            Rank = 3;
            Range = 40;

            Mana = 310;

            IsGCD = true;
            Channeled = false;
            CastTime = 0;
            CooldownTime = 0;

            School = EnumSim.SchoolType.Holy;
            SecondarySchool = EnumSim.SecondarySchoolType.Paladin_Holy;
            Init(sim);
        }
    }
}