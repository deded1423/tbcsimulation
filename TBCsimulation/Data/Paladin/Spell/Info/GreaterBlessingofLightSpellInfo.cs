using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Paladin.Spell
{
    public partial class GreaterBlessingofLightSpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public GreaterBlessingofLightSpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=27145/greater-blessing-of-light
            Name = "Greater Blessing of Light";
            Id = 27145;
            Level = 69;
            Rank = 2;
            Range = 40;

            Mana = 360;

            IsGCD = true;
            Channeled = false;
            CastTime = 0;
            CooldownTime = 0;

            School = EnumSim.SchoolType.Holy;
            SecondarySchool = EnumSim.SecondarySchoolType.Paladin_Holy;
            Init(sim);
        }
    }
}