using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Mage.Spell
{
    public partial class ConjureManaJadeSpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public ConjureManaJadeSpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=3552/conjure-mana-jade
            Name = "Conjure Mana Jade";
            Id = 3552;
            Level = 38;
            Rank = 1;
            Range = 0;

            Mana = 800;

            IsGCD = true;
            Channeled = false;
            CastTime = 3000;
            CooldownTime = 0;

            School = EnumSim.SchoolType.Arcane;
            SecondarySchool = EnumSim.SecondarySchoolType.Mage_Arcane;
            Init(sim);
        }
    }
}