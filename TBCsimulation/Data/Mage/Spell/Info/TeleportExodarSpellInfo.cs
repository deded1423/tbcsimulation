using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Mage.Spell
{
    public partial class TeleportExodarSpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public TeleportExodarSpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=32271/teleport-exodar
            Name = "Teleport: Exodar";
            Id = 32271;
            Level = 20;
            Rank = 1;
            Range = 0;

            Mana = 120;

            IsGCD = true;
            Channeled = false;
            CastTime = 10000;
            CooldownTime = 0;

            School = EnumSim.SchoolType.Arcane;
            SecondarySchool = EnumSim.SecondarySchoolType.Mage_Arcane;
            Init(sim);
        }
    }
}