using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Mage.Spell
{
    public partial class FrostArmorSpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public FrostArmorSpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=7301/frost-armor
            Name = "Frost Armor";
            Id = 7301;
            Level = 20;
            Rank = 3;
            Range = 0;

            Mana = 170;

            IsGCD = true;
            Channeled = false;
            CastTime = 0;
            CooldownTime = 0;

            School = EnumSim.SchoolType.Frost;
            SecondarySchool = EnumSim.SecondarySchoolType.Mage_Frost;
            Init(sim);
        }
    }
}