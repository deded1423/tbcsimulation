using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Mage.Spell
{
    public partial class FireWardSpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public FireWardSpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=27128/fire-ward
            Name = "Fire Ward";
            Id = 27128;
            Level = 69;
            Rank = 6;
            Range = 0;

            Mana = 360;

            IsGCD = true;
            Channeled = false;
            CastTime = 0;
            CooldownTime = 30000;

            School = EnumSim.SchoolType.Fire;
            SecondarySchool = EnumSim.SecondarySchoolType.Mage_Fire;
            Init(sim);
        }
    }
}