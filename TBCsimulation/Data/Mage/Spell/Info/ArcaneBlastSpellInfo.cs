using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Mage.Spell
{
    public partial class ArcaneBlastSpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public ArcaneBlastSpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=30451/arcane-blast
            Name = "Arcane Blast";
            Id = 30451;
            Level = 64;
            Rank = 1;
            Range = 30;

            Mana = 195;

            IsGCD = true;
            Channeled = false;
            CastTime = 2500;
            CooldownTime = 0;

            School = EnumSim.SchoolType.Arcane;
            SecondarySchool = EnumSim.SecondarySchoolType.Mage_Arcane;
            Init(sim);
        }
    }
}