using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Mage.Spell
{
    public partial class RitualofRefreshmentSpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public RitualofRefreshmentSpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=43987/ritual-of-refreshment
            Name = "Ritual of Refreshment";
            Id = 43987;
            Level = 70;
            Rank = 1;
            Range = 30;

            Mana = -1;

            IsGCD = true;
            Channeled = true;
            CastTime = 1000;
            CooldownTime = 300000;

            School = EnumSim.SchoolType.Arcane;
            SecondarySchool = EnumSim.SecondarySchoolType.Mage_Arcane;
            Init(sim);
        }
    }
}