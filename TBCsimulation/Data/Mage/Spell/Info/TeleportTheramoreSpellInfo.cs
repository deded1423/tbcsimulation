using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Mage.Spell
{
    public partial class TeleportTheramoreSpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public TeleportTheramoreSpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=49359/teleport-theramore
            Name = "Teleport: Theramore";
            Id = 49359;
            Level = 35;
            Rank = 1;
            Range = 0;

            Mana = 120;

            IsGCD = true;
            Channeled = false;
            CastTime = 10000;
            CooldownTime = 0;

            School = EnumSim.SchoolType.Physical;
            SecondarySchool = EnumSim.SecondarySchoolType.Mage_Arcane;
            Init(sim);
        }
    }
}