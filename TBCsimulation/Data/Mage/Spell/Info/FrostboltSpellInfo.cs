using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Mage.Spell
{
    public partial class FrostboltSpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public FrostboltSpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=38697/frostbolt
            Name = "Frostbolt";
            Id = 38697;
            Level = 70;
            Rank = 14;
            Range = 30;

            Mana = 345;

            IsGCD = true;
            Channeled = false;
            CastTime = 3000;
            CooldownTime = 0;

            School = EnumSim.SchoolType.Frost;
            SecondarySchool = EnumSim.SecondarySchoolType.Mage_Frost;
            Init(sim);
        }
    }
}