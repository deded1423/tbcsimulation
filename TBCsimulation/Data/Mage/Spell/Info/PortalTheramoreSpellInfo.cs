using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Mage.Spell
{
    public partial class PortalTheramoreSpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public PortalTheramoreSpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=49360/portal-theramore
            Name = "Portal: Theramore";
            Id = 49360;
            Level = 35;
            Rank = 1;
            Range = 10;

            Mana = 850;

            IsGCD = true;
            Channeled = false;
            CastTime = 10000;
            CooldownTime = 1000;

            School = EnumSim.SchoolType.Physical;
            SecondarySchool = EnumSim.SecondarySchoolType.Mage_Arcane;
            Init(sim);
        }
    }
}