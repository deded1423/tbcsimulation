using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Mage.Spell
{
    public partial class IceArmorSpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public IceArmorSpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=27124/ice-armor
            Name = "Ice Armor";
            Id = 27124;
            Level = 69;
            Rank = 5;
            Range = 0;

            Mana = 630;

            IsGCD = true;
            Channeled = false;
            CastTime = 0;
            CooldownTime = 0;

            School = EnumSim.SchoolType.Frost;
            SecondarySchool = EnumSim.SecondarySchoolType.Mage_Frost;
            Init(sim);
        }
    }
}