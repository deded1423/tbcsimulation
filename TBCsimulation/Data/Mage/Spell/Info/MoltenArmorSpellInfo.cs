using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Mage.Spell
{
    public partial class MoltenArmorSpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public MoltenArmorSpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=30482/molten-armor
            Name = "Molten Armor";
            Id = 30482;
            Level = 62;
            Rank = 1;
            Range = 0;

            Mana = 630;

            IsGCD = true;
            Channeled = false;
            CastTime = 0;
            CooldownTime = 0;

            School = EnumSim.SchoolType.Fire;
            SecondarySchool = EnumSim.SecondarySchoolType.Mage_Fire;
            Init(sim);
        }
    }
}