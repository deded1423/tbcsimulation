using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Mage.Spell
{
    public partial class CounterspellSpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public CounterspellSpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=2139/counterspell
            Name = "Counterspell";
            Id = 2139;
            Level = 24;
            Rank = 1;
            Range = 30;

            Mana = 100;

            IsGCD = false;
            Channeled = false;
            CastTime = 0;
            CooldownTime = 24000;

            School = EnumSim.SchoolType.Arcane;
            SecondarySchool = EnumSim.SecondarySchoolType.Mage_Arcane;
            Init(sim);
        }
    }
}