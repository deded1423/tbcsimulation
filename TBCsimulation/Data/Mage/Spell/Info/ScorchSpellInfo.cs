using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Mage.Spell
{
    public partial class ScorchSpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public ScorchSpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=27074/scorch
            Name = "Scorch";
            Id = 27074;
            Level = 70;
            Rank = 9;
            Range = 30;

            Mana = 180;

            IsGCD = true;
            Channeled = false;
            CastTime = 1500;
            CooldownTime = 0;

            School = EnumSim.SchoolType.Fire;
            SecondarySchool = EnumSim.SecondarySchoolType.Mage_Fire;
            Init(sim);
        }
    }
}