using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Mage.Spell
{
    public partial class PortalExodarSpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public PortalExodarSpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=32266/portal-exodar
            Name = "Portal: Exodar";
            Id = 32266;
            Level = 40;
            Rank = 1;
            Range = 10;

            Mana = 850;

            IsGCD = true;
            Channeled = false;
            CastTime = 10000;
            CooldownTime = 1000;

            School = EnumSim.SchoolType.Arcane;
            SecondarySchool = EnumSim.SecondarySchoolType.Mage_Arcane;
            Init(sim);
        }
    }
}