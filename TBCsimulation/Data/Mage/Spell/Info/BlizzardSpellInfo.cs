using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Mage.Spell
{
    public partial class BlizzardSpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public BlizzardSpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=27085/blizzard
            Name = "Blizzard";
            Id = 27085;
            Level = 68;
            Rank = 7;
            Range = 30;

            Mana = 1645;

            IsGCD = true;
            Channeled = true;
            CastTime = 8000;
            CooldownTime = 0;

            School = EnumSim.SchoolType.Frost;
            SecondarySchool = EnumSim.SecondarySchoolType.Mage_Frost;
            Init(sim);
        }
    }
}