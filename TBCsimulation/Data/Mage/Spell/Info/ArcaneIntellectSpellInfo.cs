using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Mage.Spell
{
    public partial class ArcaneIntellectSpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public ArcaneIntellectSpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=27126/arcane-intellect
            Name = "Arcane Intellect";
            Id = 27126;
            Level = 70;
            Rank = 6;
            Range = 30;

            Mana = 700;

            IsGCD = true;
            Channeled = false;
            CastTime = 0;
            CooldownTime = 0;

            School = EnumSim.SchoolType.Arcane;
            SecondarySchool = EnumSim.SecondarySchoolType.Mage_Arcane;
            Init(sim);
        }
    }
}