using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Mage.Spell
{
    public partial class FlamestrikeSpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public FlamestrikeSpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=27086/flamestrike
            Name = "Flamestrike";
            Id = 27086;
            Level = 64;
            Rank = 7;
            Range = 30;

            Mana = 1175;

            IsGCD = true;
            Channeled = false;
            CastTime = 3000;
            CooldownTime = 0;

            School = EnumSim.SchoolType.Fire;
            SecondarySchool = EnumSim.SecondarySchoolType.Mage_Fire;
            Init(sim);
        }
    }
}