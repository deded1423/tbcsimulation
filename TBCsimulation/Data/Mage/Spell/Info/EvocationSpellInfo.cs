using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Mage.Spell
{
    public partial class EvocationSpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public EvocationSpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=12051/evocation
            Name = "Evocation";
            Id = 12051;
            Level = 20;
            Rank = 1;
            Range = 0;

            Mana = -1;

            IsGCD = true;
            Channeled = true;
            CastTime = 8000;
            CooldownTime = 480000;

            School = EnumSim.SchoolType.Arcane;
            SecondarySchool = EnumSim.SecondarySchoolType.Mage_Arcane;
            Init(sim);
        }
    }
}