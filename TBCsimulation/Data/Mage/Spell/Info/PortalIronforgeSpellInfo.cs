using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Mage.Spell
{
    public partial class PortalIronforgeSpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public PortalIronforgeSpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=11416/portal-ironforge
            Name = "Portal: Ironforge";
            Id = 11416;
            Level = 40;
            Rank = 1;
            Range = 10;

            Mana = 850;

            IsGCD = true;
            Channeled = false;
            CastTime = 10000;
            CooldownTime = 1000;

            School = EnumSim.SchoolType.Arcane;
            SecondarySchool = EnumSim.SecondarySchoolType.Mage_Arcane;
            Init(sim);
        }
    }
}