using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Mage.Spell
{
    public partial class AmplifyMagicSpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public AmplifyMagicSpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=33946/amplify-magic
            Name = "Amplify Magic";
            Id = 33946;
            Level = 69;
            Rank = 6;
            Range = 30;

            Mana = 600;

            IsGCD = true;
            Channeled = false;
            CastTime = 0;
            CooldownTime = 0;

            School = EnumSim.SchoolType.Arcane;
            SecondarySchool = EnumSim.SecondarySchoolType.Mage_Arcane;
            Init(sim);
        }
    }
}