using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Mage.Spell
{
    public partial class ConjureManaCitrineSpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public ConjureManaCitrineSpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=10053/conjure-mana-citrine
            Name = "Conjure Mana Citrine";
            Id = 10053;
            Level = 48;
            Rank = 1;
            Range = 0;

            Mana = 1130;

            IsGCD = true;
            Channeled = false;
            CastTime = 3000;
            CooldownTime = 0;

            School = EnumSim.SchoolType.Arcane;
            SecondarySchool = EnumSim.SecondarySchoolType.Mage_Arcane;
            Init(sim);
        }
    }
}