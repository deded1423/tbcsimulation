using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Mage.Spell
{
    public partial class IceBlockSpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public IceBlockSpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=45438/ice-block
            Name = "Ice Block";
            Id = 45438;
            Level = 30;
            Rank = 1;
            Range = 0;

            Mana = 15;

            IsGCD = true;
            Channeled = false;
            CastTime = 0;
            CooldownTime = 300000;

            School = EnumSim.SchoolType.Frost;
            SecondarySchool = EnumSim.SecondarySchoolType.Mage_Frost;
            Init(sim);
        }
    }
}