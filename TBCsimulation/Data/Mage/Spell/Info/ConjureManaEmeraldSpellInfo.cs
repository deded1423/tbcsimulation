using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Mage.Spell
{
    public partial class ConjureManaEmeraldSpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public ConjureManaEmeraldSpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=27101/conjure-mana-emerald
            Name = "Conjure Mana Emerald";
            Id = 27101;
            Level = 68;
            Rank = 1;
            Range = 0;

            Mana = 1670;

            IsGCD = true;
            Channeled = false;
            CastTime = 3000;
            CooldownTime = 0;

            School = EnumSim.SchoolType.Arcane;
            SecondarySchool = EnumSim.SecondarySchoolType.Mage_Arcane;
            Init(sim);
        }
    }
}