using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Mage.Spell
{
    public partial class TeleportShattrathSpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public TeleportShattrathSpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=35715/teleport-shattrath
            Name = "Teleport: Shattrath";
            Id = 35715;
            Level = 60;
            Rank = 1;
            Range = 0;

            Mana = 120;

            IsGCD = true;
            Channeled = false;
            CastTime = 10000;
            CooldownTime = 0;

            School = EnumSim.SchoolType.Arcane;
            SecondarySchool = EnumSim.SecondarySchoolType.Mage_Arcane;
            Init(sim);
        }
    }
}