using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Mage.Spell
{
    public partial class ConjureManaRubySpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public ConjureManaRubySpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=10054/conjure-mana-ruby
            Name = "Conjure Mana Ruby";
            Id = 10054;
            Level = 58;
            Rank = 1;
            Range = 0;

            Mana = 1470;

            IsGCD = true;
            Channeled = false;
            CastTime = 3000;
            CooldownTime = 0;

            School = EnumSim.SchoolType.Arcane;
            SecondarySchool = EnumSim.SecondarySchoolType.Mage_Arcane;
            Init(sim);
        }
    }
}