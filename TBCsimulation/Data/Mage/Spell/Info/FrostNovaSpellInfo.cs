using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Mage.Spell
{
    public partial class FrostNovaSpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public FrostNovaSpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=27088/frost-nova
            Name = "Frost Nova";
            Id = 27088;
            Level = 67;
            Rank = 5;
            Range = 0;

            Mana = 185;

            IsGCD = true;
            Channeled = false;
            CastTime = 0;
            CooldownTime = 25000;

            School = EnumSim.SchoolType.Frost;
            SecondarySchool = EnumSim.SecondarySchoolType.Mage_Frost;
            Init(sim);
        }
    }
}