using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Mage.Spell
{
    public partial class ArcaneExplosionSpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public ArcaneExplosionSpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=27082/arcane-explosion
            Name = "Arcane Explosion";
            Id = 27082;
            Level = 70;
            Rank = 8;
            Range = 0;

            Mana = 545;

            IsGCD = true;
            Channeled = false;
            CastTime = 0;
            CooldownTime = 0;

            School = EnumSim.SchoolType.Arcane;
            SecondarySchool = EnumSim.SecondarySchoolType.Mage_Arcane;
            Init(sim);
        }
    }
}