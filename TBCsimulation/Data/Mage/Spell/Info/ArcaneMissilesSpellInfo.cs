using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Mage.Spell
{
    public partial class ArcaneMissilesSpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public ArcaneMissilesSpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=38704/arcane-missiles
            Name = "Arcane Missiles";
            Id = 38704;
            Level = 70;
            Rank = 11;
            Range = 30;

            Mana = 785;

            IsGCD = true;
            Channeled = true;
            CastTime = 5000;
            CooldownTime = 0;

            School = EnumSim.SchoolType.Arcane;
            SecondarySchool = EnumSim.SecondarySchoolType.Mage_Arcane;
            Init(sim);
        }
    }
}