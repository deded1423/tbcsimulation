using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Mage.Spell
{
    public partial class TeleportDarnassusSpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public TeleportDarnassusSpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=3565/teleport-darnassus
            Name = "Teleport: Darnassus";
            Id = 3565;
            Level = 30;
            Rank = 1;
            Range = 0;

            Mana = 120;

            IsGCD = true;
            Channeled = false;
            CastTime = 10000;
            CooldownTime = 0;

            School = EnumSim.SchoolType.Arcane;
            SecondarySchool = EnumSim.SecondarySchoolType.Mage_Arcane;
            Init(sim);
        }
    }
}