using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Mage.Spell
{
    public partial class FireBlastSpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public FireBlastSpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=27079/fire-blast
            Name = "Fire Blast";
            Id = 27079;
            Level = 70;
            Rank = 9;
            Range = 20;

            Mana = 465;

            IsGCD = true;
            Channeled = false;
            CastTime = 0;
            CooldownTime = 8000;

            School = EnumSim.SchoolType.Fire;
            SecondarySchool = EnumSim.SecondarySchoolType.Mage_Fire;
            Init(sim);
        }
    }
}