﻿using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;

namespace TBCsimulation.Data.SpellsInfo
{
    public class ExampleSpell : Spell
    {
        private const int privateVar1 = 0;
        private const int privateVar2 = 0;

        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public ExampleSpell(FightSim sim)
        {
            Name = "Example";
            Id = 0;
            CastTime = 0;
            IsGCD = true;

            Mana = 0;
            Rank = 0;
            School = EnumSim.SchoolType.Physical;
            ActionDelegate = delegate { };
        }
    }
}