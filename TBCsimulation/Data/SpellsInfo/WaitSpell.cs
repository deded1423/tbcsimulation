﻿using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;

namespace TBCsimulation.Data.SpellsInfo
{
    public class WaitSpell : Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public WaitSpell(FightSim sim)
        {
            Name = "Wait";
            Id = 0;
            CastTime = 100;
            IsGCD = false;
            AffectedByHaste = false;

            Mana = 0;
            Rank = 0;
            School = EnumSim.SchoolType.Physical;
            ActionDelegate = delegate { };
        }
    }
}