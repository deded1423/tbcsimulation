﻿using System;
using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;

namespace TBCsimulation.Data.SpellsInfo
{
    public class Spell
    {
        public Action ActionDelegate;
        public bool AffectedByHaste = true;
        public int CastTime = 0;
        public bool Channeled = false;
        public int CooldownTime = 0;
        public int Id;
        public bool IsGCD = true;
        public int Level;

        public int Mana = 0;

        public string Name;
        public int Range;
        public int Rank = 0;
        public EnumSim.SchoolType School = EnumSim.SchoolType.Physical;
        public EnumSim.SecondarySchoolType SecondarySchool = EnumSim.SecondarySchoolType.None;

        public int TickTime = 0;

        //public new const string Name;
        //public new const int Id;
        //public new const int CastTime = 0;
        //public new const bool IsGCD = true;

        //public new const int Mana = 0;
        //public new const int Rank = 0;
        //public new const SchoolType School = SchoolType.Physical;

        //public new const Action<FightSim> ActionDelegate;


        public int GetCastTime(FightSim fightSim)
        {
            if (AffectedByHaste)
                return (int) (CastTime /
                              (1 + fightSim.Player.GetSpellHaste() + fightSim.Player.GetHaste() / 100)); // + lag;

            return CastTime; // + lag;
        }

        public int GetTickTime(FightSim fightSim)
        {
            if (AffectedByHaste)
                return (int) (TickTime /
                              (1 + fightSim.Player.GetSpellHaste() + fightSim.Player.GetHaste() / 100)); // + lag;

            return TickTime; // + lag;
        }
    }
}