//####### AUTOGENERATED #######

namespace TBCsimulation.Data.SpellsInfo
{
    public static partial class SpellId
    {
//####### AUTOGENERATED #######
        public const int Hunter_ArcaneResistance = 24510;

//####### AUTOGENERATED #######
        public const int Hunter_ArcaneShot = 27019;

//####### AUTOGENERATED #######
        public const int Hunter_AspectoftheBeast = 13161;

//####### AUTOGENERATED #######
        public const int Hunter_AspectoftheCheetah = 5118;

//####### AUTOGENERATED #######
        public const int Hunter_AspectoftheHawk = 27044;

//####### AUTOGENERATED #######
        public const int Hunter_AspectoftheMonkey = 13163;

//####### AUTOGENERATED #######
        public const int Hunter_AspectofthePack = 13159;

//####### AUTOGENERATED #######
        public const int Hunter_AspectoftheViper = 34074;

//####### AUTOGENERATED #######
        public const int Hunter_AspectoftheWild = 27045;

//####### AUTOGENERATED #######
        public const int Hunter_AutoShot = 75;

//####### AUTOGENERATED #######
        public const int Hunter_BeastLore = 1462;

//####### AUTOGENERATED #######
        public const int Hunter_CallPet = 883;

//####### AUTOGENERATED #######
        public const int Hunter_ConcussiveShot = 5116;

//####### AUTOGENERATED #######
        public const int Hunter_Disengage = 27015;

//####### AUTOGENERATED #######
        public const int Hunter_DismissPet = 2641;

//####### AUTOGENERATED #######
        public const int Hunter_DistractingShot = 27020;

//####### AUTOGENERATED #######
        public const int Hunter_EagleEye = 6197;

//####### AUTOGENERATED #######
        public const int Hunter_ExplosiveTrap = 27025;

//####### AUTOGENERATED #######
        public const int Hunter_EyesoftheBeast = 1002;

//####### AUTOGENERATED #######
        public const int Hunter_FeedPet = 6991;

//####### AUTOGENERATED #######
        public const int Hunter_FeignDeath = 5384;

//####### AUTOGENERATED #######
        public const int Hunter_FireResistance = 24464;

//####### AUTOGENERATED #######
        public const int Hunter_Flare = 1543;

//####### AUTOGENERATED #######
        public const int Hunter_FreezingTrap = 14311;

//####### AUTOGENERATED #######
        public const int Hunter_FrostResistance = 24478;

//####### AUTOGENERATED #######
        public const int Hunter_FrostTrap = 13809;

//####### AUTOGENERATED #######
        public const int Hunter_GreatStamina = 5049;

//####### AUTOGENERATED #######
        public const int Hunter_Growl = 14927;

//####### AUTOGENERATED #######
        public const int Hunter_HuntersMark = 14325;

//####### AUTOGENERATED #######
        public const int Hunter_ImmolationTrap = 27023;

//####### AUTOGENERATED #######
        public const int Hunter_KillCommand = 34026;

//####### AUTOGENERATED #######
        public const int Hunter_MendPet = 27046;

//####### AUTOGENERATED #######
        public const int Hunter_Misdirection = 34477;

//####### AUTOGENERATED #######
        public const int Hunter_MongooseBite = 36916;

//####### AUTOGENERATED #######
        public const int Hunter_MultiShot = 27021;

//####### AUTOGENERATED #######
        public const int Hunter_NaturalArmor = 24632;

//####### AUTOGENERATED #######
        public const int Hunter_NatureResistance = 24513;

//####### AUTOGENERATED #######
        public const int Hunter_RapidFire = 3045;

//####### AUTOGENERATED #######
        public const int Hunter_RaptorStrike = 27014;

//####### AUTOGENERATED #######
        public const int Hunter_RevivePet = 982;

//####### AUTOGENERATED #######
        public const int Hunter_ScareBeast = 14327;

//####### AUTOGENERATED #######
        public const int Hunter_ScorpidSting = 3043;

//####### AUTOGENERATED #######
        public const int Hunter_SerpentSting = 27016;

//####### AUTOGENERATED #######
        public const int Hunter_ShadowResistance = 24516;

//####### AUTOGENERATED #######
        public const int Hunter_SnakeTrap = 34600;

//####### AUTOGENERATED #######
        public const int Hunter_SteadyShot = 34120;

//####### AUTOGENERATED #######
        public const int Hunter_TameBeast = 1515;

//####### AUTOGENERATED #######
        public const int Hunter_TrackBeasts = 1494;

//####### AUTOGENERATED #######
        public const int Hunter_TrackDemons = 19878;

//####### AUTOGENERATED #######
        public const int Hunter_TrackDragonkin = 19879;

//####### AUTOGENERATED #######
        public const int Hunter_TrackElementals = 19880;

//####### AUTOGENERATED #######
        public const int Hunter_TrackGiants = 19882;

//####### AUTOGENERATED #######
        public const int Hunter_TrackHidden = 19885;

//####### AUTOGENERATED #######
        public const int Hunter_TrackHumanoids = 19883;

//####### AUTOGENERATED #######
        public const int Hunter_TrackUndead = 19884;

//####### AUTOGENERATED #######
        public const int Hunter_TranquilizingShot = 19801;

//####### AUTOGENERATED #######
        public const int Hunter_ViperSting = 27018;

//####### AUTOGENERATED #######
        public const int Hunter_Volley = 27022;

//####### AUTOGENERATED #######
        public const int Hunter_WingClip = 14268;
    }
}