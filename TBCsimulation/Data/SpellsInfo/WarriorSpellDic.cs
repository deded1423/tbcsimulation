//####### AUTOGENERATED #######

using System.Collections.Generic;

namespace TBCsimulation.Data.SpellsInfo
{
    public static partial class SpellId
    {
//####### AUTOGENERATED #######
        public static readonly Dictionary<int, string> WarriorDictionaryIdToName = new()
        {
            {25289, "Battle Shout"},

            {2457, "Battle Stance"},

            {18499, "Berserker Rage"},

            {2458, "Berserker Stance"},

            {2687, "Bloodrage"},

            {1161, "Challenging Shout"},

            {11578, "Charge"},

            {25231, "Cleave"},

            {469, "Commanding Shout"},

            {71, "Defensive Stance"},

            {25203, "Demoralizing Shout"},

            {676, "Disarm"},

            {25236, "Execute"},

            {25212, "Hamstring"},

            {30324, "Heroic Strike"},

            {25275, "Intercept"},

            {3411, "Intervene"},

            {5246, "Intimidating Shout"},

            {25266, "Mocking Blow"},

            {11585, "Overpower"},

            {6554, "Pummel"},

            {1719, "Recklessness"},

            {25208, "Rend"},

            {20230, "Retaliation"},

            {30357, "Revenge"},

            {29704, "Shield Bash"},

            {2565, "Shield Block"},

            {871, "Shield Wall"},

            {25242, "Slam"},

            {23920, "Spell Reflection"},

            {12678, "Stance Mastery"},

            {25225, "Sunder Armor"},

            {355, "Taunt"},

            {25264, "Thunder Clap"},

            {34428, "Victory Rush"},

            {1680, "Whirlwind"}
        };


//####### AUTOGENERATED #######
        public static readonly Dictionary<string, int> WarriorDictionaryNameToId = new()
        {
            {"Battle Shout", 25289},

            {"Battle Stance", 2457},

            {"Berserker Rage", 18499},

            {"Berserker Stance", 2458},

            {"Bloodrage", 2687},

            {"Challenging Shout", 1161},

            {"Charge", 11578},

            {"Cleave", 25231},

            {"Commanding Shout", 469},

            {"Defensive Stance", 71},

            {"Demoralizing Shout", 25203},

            {"Disarm", 676},

            {"Execute", 25236},

            {"Hamstring", 25212},

            {"Heroic Strike", 30324},

            {"Intercept", 25275},

            {"Intervene", 3411},

            {"Intimidating Shout", 5246},

            {"Mocking Blow", 25266},

            {"Overpower", 11585},

            {"Pummel", 6554},

            {"Recklessness", 1719},

            {"Rend", 25208},

            {"Retaliation", 20230},

            {"Revenge", 30357},

            {"Shield Bash", 29704},

            {"Shield Block", 2565},

            {"Shield Wall", 871},

            {"Slam", 25242},

            {"Spell Reflection", 23920},

            {"Stance Mastery", 12678},

            {"Sunder Armor", 25225},

            {"Taunt", 355},

            {"Thunder Clap", 25264},

            {"Victory Rush", 34428},

            {"Whirlwind", 1680}
        };
    }
}