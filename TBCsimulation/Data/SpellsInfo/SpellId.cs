﻿namespace TBCsimulation.Data.SpellsInfo
{
    public static partial class SpellId
    {
        public static int GetIdFromName(string name)
        {
            var id = -1;

            if (DruidDictionaryNameToId.TryGetValue(name, out id))
                return id;
            if (HunterDictionaryNameToId.TryGetValue(name, out id))
                return id;
            if (MageDictionaryNameToId.TryGetValue(name, out id))
                return id;
            if (PaladinDictionaryNameToId.TryGetValue(name, out id))
                return id;
            if (PriestDictionaryNameToId.TryGetValue(name, out id))
                return id;
            if (RogueDictionaryNameToId.TryGetValue(name, out id))
                return id;
            if (ShamanDictionaryNameToId.TryGetValue(name, out id))
                return id;
            if (WarlockDictionaryNameToId.TryGetValue(name, out id))
                return id;
            if (WarriorDictionaryNameToId.TryGetValue(name, out id))
                return id;
            if (RacialDictionaryNameToId.TryGetValue(name, out id))
                return id;
            return id;
        }

        public static string GetNameFromId(int id)
        {
            string name = null;
            if (DruidDictionaryIdToName.TryGetValue(id, out name))
                return name;
            if (HunterDictionaryIdToName.TryGetValue(id, out name))
                return name;
            if (MageDictionaryIdToName.TryGetValue(id, out name))
                return name;
            if (PaladinDictionaryIdToName.TryGetValue(id, out name))
                return name;
            if (PriestDictionaryIdToName.TryGetValue(id, out name))
                return name;
            if (ShamanDictionaryIdToName.TryGetValue(id, out name))
                return name;
            if (WarlockDictionaryIdToName.TryGetValue(id, out name))
                return name;
            if (WarriorDictionaryIdToName.TryGetValue(id, out name))
                return name;
            if (RacialDictionaryIdToName.TryGetValue(id, out name))
                return name;
            return name;
        }
    }
}