//####### AUTOGENERATED #######

namespace TBCsimulation.Data.SpellsInfo
{
    public static partial class SpellId
    {
//####### AUTOGENERATED #######
        public const int Priest_AbolishDisease = 552;

//####### AUTOGENERATED #######
        public const int Priest_BindingHeal = 32546;

//####### AUTOGENERATED #######
        public const int Priest_Chastise = 44047;

//####### AUTOGENERATED #######
        public const int Priest_ConsumeMagic = 32676;

//####### AUTOGENERATED #######
        public const int Priest_CureDisease = 528;

//####### AUTOGENERATED #######
        public const int Priest_DesperatePrayer = 25437;

//####### AUTOGENERATED #######
        public const int Priest_DevouringPlague = 25467;

//####### AUTOGENERATED #######
        public const int Priest_DispelMagic = 988;

//####### AUTOGENERATED #######
        public const int Priest_ElunesGrace = 2651;

//####### AUTOGENERATED #######
        public const int Priest_Fade = 25429;

//####### AUTOGENERATED #######
        public const int Priest_FearWard = 6346;

//####### AUTOGENERATED #######
        public const int Priest_Feedback = 25441;

//####### AUTOGENERATED #######
        public const int Priest_FlashHeal = 25235;

//####### AUTOGENERATED #######
        public const int Priest_GreaterHeal = 25314;

//####### AUTOGENERATED #######
        public const int Priest_Heal = 6064;

//####### AUTOGENERATED #######
        public const int Priest_HexofWeakness = 25470;

//####### AUTOGENERATED #######
        public const int Priest_HolyFire = 25384;

//####### AUTOGENERATED #######
        public const int Priest_InnerFire = 25431;

//####### AUTOGENERATED #######
        public const int Priest_LesserHeal = 2053;

//####### AUTOGENERATED #######
        public const int Priest_Levitate = 1706;

//####### AUTOGENERATED #######
        public const int Priest_ManaBurn = 25380;

//####### AUTOGENERATED #######
        public const int Priest_MassDispel = 32375;

//####### AUTOGENERATED #######
        public const int Priest_MindBlast = 25375;

//####### AUTOGENERATED #######
        public const int Priest_MindControl = 10912;

//####### AUTOGENERATED #######
        public const int Priest_MindSoothe = 25596;

//####### AUTOGENERATED #######
        public const int Priest_MindVision = 10909;

//####### AUTOGENERATED #######
        public const int Priest_PowerWordFortitude = 25389;

//####### AUTOGENERATED #######
        public const int Priest_PowerWordShield = 25218;

//####### AUTOGENERATED #######
        public const int Priest_PrayerofFortitude = 25392;

//####### AUTOGENERATED #######
        public const int Priest_PrayerofHealing = 25316;

//####### AUTOGENERATED #######
        public const int Priest_PrayerofMending = 33076;

//####### AUTOGENERATED #######
        public const int Priest_PrayerofShadowProtection = 39374;

//####### AUTOGENERATED #######
        public const int Priest_PrayerofSpirit = 32999;

//####### AUTOGENERATED #######
        public const int Priest_PsychicScream = 10890;

//####### AUTOGENERATED #######
        public const int Priest_Renew = 25315;

//####### AUTOGENERATED #######
        public const int Priest_Resurrection = 25435;

//####### AUTOGENERATED #######
        public const int Priest_ShackleUndead = 10955;

//####### AUTOGENERATED #######
        public const int Priest_Shadowfiend = 34433;

//####### AUTOGENERATED #######
        public const int Priest_Shadowguard = 25477;

//####### AUTOGENERATED #######
        public const int Priest_ShadowProtection = 25433;

//####### AUTOGENERATED #######
        public const int Priest_ShadowWordDeath = 32996;

//####### AUTOGENERATED #######
        public const int Priest_ShadowWordPain = 25368;

//####### AUTOGENERATED #######
        public const int Priest_Smite = 25364;

//####### AUTOGENERATED #######
        public const int Priest_Starshards = 25446;

//####### AUTOGENERATED #######
        public const int Priest_SymbolofHope = 32548;

//####### AUTOGENERATED #######
        public const int Priest_TouchofWeakness = 25461;
    }
}