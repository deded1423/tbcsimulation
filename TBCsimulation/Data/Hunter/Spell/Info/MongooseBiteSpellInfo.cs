using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Hunter.Spell
{
    public partial class MongooseBiteSpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public MongooseBiteSpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=36916/mongoose-bite
            Name = "Mongoose Bite";
            Id = 36916;
            Level = 70;
            Rank = 5;
            Range = 5;

            Mana = 80;

            IsGCD = true;
            Channeled = false;
            CastTime = 0;
            CooldownTime = 5000;

            School = EnumSim.SchoolType.Physical;
            SecondarySchool = EnumSim.SecondarySchoolType.Hunter_Survival;
            Init(sim);
        }
    }
}