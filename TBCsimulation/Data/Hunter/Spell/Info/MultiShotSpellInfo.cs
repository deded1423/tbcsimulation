using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Hunter.Spell
{
    public partial class MultiShotSpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public MultiShotSpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=27021/multi-shot
            Name = "Multi-Shot";
            Id = 27021;
            Level = 67;
            Rank = 6;
            Range = 35;

            Mana = 275;

            IsGCD = true;
            Channeled = false;
            CastTime = 0;
            CooldownTime = 10000;

            School = EnumSim.SchoolType.Physical;
            SecondarySchool = EnumSim.SecondarySchoolType.Hunter_Marksmanship;
            Init(sim);
        }
    }
}