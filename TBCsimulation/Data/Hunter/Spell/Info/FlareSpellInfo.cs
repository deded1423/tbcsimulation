using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Hunter.Spell
{
    public partial class FlareSpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public FlareSpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=1543/flare
            Name = "Flare";
            Id = 1543;
            Level = 32;
            Rank = 1;
            Range = 30;

            Mana = 50;

            IsGCD = true;
            Channeled = false;
            CastTime = 0;
            CooldownTime = 20000;

            School = EnumSim.SchoolType.Arcane;
            SecondarySchool = EnumSim.SecondarySchoolType.Hunter_Marksmanship;
            Init(sim);
        }
    }
}