using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Hunter.Spell
{
    public partial class TameBeastSpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public TameBeastSpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=1515/tame-beast
            Name = "Tame Beast";
            Id = 1515;
            Level = 10;
            Rank = 1;
            Range = 30;

            Mana = -1;

            IsGCD = true;
            Channeled = true;
            CastTime = 20000;
            CooldownTime = 0;

            School = EnumSim.SchoolType.Nature;
            SecondarySchool = EnumSim.SecondarySchoolType.Hunter_BeastMastery;
            Init(sim);
        }
    }
}