using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Hunter.Spell
{
    public partial class FrostTrapSpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public FrostTrapSpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=13809/frost-trap
            Name = "Frost Trap";
            Id = 13809;
            Level = 28;
            Rank = 1;
            Range = 0;

            Mana = 60;

            IsGCD = true;
            Channeled = false;
            CastTime = 0;
            CooldownTime = 30000;

            School = EnumSim.SchoolType.Frost;
            SecondarySchool = EnumSim.SecondarySchoolType.Hunter_Survival;
            Init(sim);
        }
    }
}