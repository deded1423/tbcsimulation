using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Hunter.Spell
{
    public partial class VolleySpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public VolleySpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=27022/volley
            Name = "Volley";
            Id = 27022;
            Level = 67;
            Rank = 4;
            Range = 35;

            Mana = 585;

            IsGCD = true;
            Channeled = true;
            CastTime = 6000;
            CooldownTime = 1000;

            School = EnumSim.SchoolType.Arcane;
            SecondarySchool = EnumSim.SecondarySchoolType.Hunter_Marksmanship;
            Init(sim);
        }
    }
}