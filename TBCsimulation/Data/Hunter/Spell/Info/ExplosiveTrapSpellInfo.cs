using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Hunter.Spell
{
    public partial class ExplosiveTrapSpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public ExplosiveTrapSpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=27025/explosive-trap
            Name = "Explosive Trap";
            Id = 27025;
            Level = 61;
            Rank = 4;
            Range = 0;

            Mana = 650;

            IsGCD = true;
            Channeled = false;
            CastTime = 0;
            CooldownTime = 30000;

            School = EnumSim.SchoolType.Fire;
            SecondarySchool = EnumSim.SecondarySchoolType.Hunter_Survival;
            Init(sim);
        }
    }
}