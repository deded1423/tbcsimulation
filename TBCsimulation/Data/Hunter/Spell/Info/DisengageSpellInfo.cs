using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Hunter.Spell
{
    public partial class DisengageSpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public DisengageSpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=27015/disengage
            Name = "Disengage";
            Id = 27015;
            Level = 62;
            Rank = 4;
            Range = 5;

            Mana = 205;

            IsGCD = true;
            Channeled = false;
            CastTime = 0;
            CooldownTime = 5000;

            School = EnumSim.SchoolType.Physical;
            SecondarySchool = EnumSim.SecondarySchoolType.Hunter_Survival;
            Init(sim);
        }
    }
}