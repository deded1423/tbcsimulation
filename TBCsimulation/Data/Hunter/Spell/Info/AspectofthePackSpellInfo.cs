using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Hunter.Spell
{
    public partial class AspectofthePackSpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public AspectofthePackSpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=13159/aspect-of-the-pack
            Name = "Aspect of the Pack";
            Id = 13159;
            Level = 40;
            Rank = 1;
            Range = 0;

            Mana = 100;

            IsGCD = true;
            Channeled = false;
            CastTime = 0;
            CooldownTime = 0;

            School = EnumSim.SchoolType.Nature;
            SecondarySchool = EnumSim.SecondarySchoolType.Hunter_BeastMastery;
            Init(sim);
        }
    }
}