using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Hunter.Spell
{
    public partial class CallPetSpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public CallPetSpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=883/call-pet
            Name = "Call Pet";
            Id = 883;
            Level = 10;
            Rank = 1;
            Range = 0;

            Mana = -1;

            IsGCD = true;
            Channeled = false;
            CastTime = 0;
            CooldownTime = 0;

            School = EnumSim.SchoolType.Physical;
            SecondarySchool = EnumSim.SecondarySchoolType.Hunter_BeastMastery;
            Init(sim);
        }
    }
}