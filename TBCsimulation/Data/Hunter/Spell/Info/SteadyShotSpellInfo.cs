using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Hunter.Spell
{
    public partial class SteadyShotSpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public SteadyShotSpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=34120/steady-shot
            Name = "Steady Shot";
            Id = 34120;
            Level = 62;
            Rank = 1;
            Range = 35;

            Mana = 110;

            IsGCD = true;
            Channeled = false;
            CastTime = 1000;
            CooldownTime = 0;

            School = EnumSim.SchoolType.Physical;
            SecondarySchool = EnumSim.SecondarySchoolType.Hunter_Marksmanship;
            Init(sim);
        }
    }
}