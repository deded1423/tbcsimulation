using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Hunter.Spell
{
    public partial class RapidFireSpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public RapidFireSpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=3045/rapid-fire
            Name = "Rapid Fire";
            Id = 3045;
            Level = 26;
            Rank = 1;
            Range = 0;

            Mana = 100;

            IsGCD = false;
            Channeled = false;
            CastTime = 0;
            CooldownTime = 300000;

            School = EnumSim.SchoolType.Arcane;
            SecondarySchool = EnumSim.SecondarySchoolType.Hunter_Marksmanship;
            Init(sim);
        }
    }
}