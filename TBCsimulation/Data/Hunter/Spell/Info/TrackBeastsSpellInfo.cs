using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Hunter.Spell
{
    public partial class TrackBeastsSpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public TrackBeastsSpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=1494/track-beasts
            Name = "Track Beasts";
            Id = 1494;
            Level = 1;
            Rank = 1;
            Range = 0;

            Mana = -1;

            IsGCD = true;
            Channeled = false;
            CastTime = 0;
            CooldownTime = 0;

            School = EnumSim.SchoolType.Physical;
            SecondarySchool = EnumSim.SecondarySchoolType.Hunter_Survival;
            Init(sim);
        }
    }
}