using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Hunter.Spell
{
    public partial class FeignDeathSpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public FeignDeathSpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=5384/feign-death
            Name = "Feign Death";
            Id = 5384;
            Level = 30;
            Rank = 1;
            Range = 0;

            Mana = 80;

            IsGCD = false;
            Channeled = false;
            CastTime = 0;
            CooldownTime = 30000;

            School = EnumSim.SchoolType.Physical;
            SecondarySchool = EnumSim.SecondarySchoolType.Hunter_Survival;
            Init(sim);
        }
    }
}