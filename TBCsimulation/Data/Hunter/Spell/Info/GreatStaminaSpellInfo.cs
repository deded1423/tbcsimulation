using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Hunter.Spell
{
    public partial class GreatStaminaSpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public GreatStaminaSpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=5049/great-stamina
            Name = "Great Stamina";
            Id = 5049;
            Level = 60;
            Rank = 10;
            Range = 100;

            Mana = -1;

            IsGCD = false;
            Channeled = false;
            CastTime = 0;
            CooldownTime = 0;

            School = EnumSim.SchoolType.Physical;
            SecondarySchool = EnumSim.SecondarySchoolType.Hunter_BeastTraining;
            Init(sim);
        }
    }
}