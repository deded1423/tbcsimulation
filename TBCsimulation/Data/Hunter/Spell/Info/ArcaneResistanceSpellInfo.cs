using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Hunter.Spell
{
    public partial class ArcaneResistanceSpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public ArcaneResistanceSpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=24510/arcane-resistance
            Name = "Arcane Resistance";
            Id = 24510;
            Level = 50;
            Rank = 4;
            Range = 100;

            Mana = -1;

            IsGCD = false;
            Channeled = false;
            CastTime = 0;
            CooldownTime = 0;

            School = EnumSim.SchoolType.Physical;
            SecondarySchool = EnumSim.SecondarySchoolType.Hunter_BeastTraining;
            Init(sim);
        }
    }
}