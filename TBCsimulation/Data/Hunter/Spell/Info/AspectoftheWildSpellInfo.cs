using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Hunter.Spell
{
    public partial class AspectoftheWildSpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public AspectoftheWildSpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=27045/aspect-of-the-wild
            Name = "Aspect of the Wild";
            Id = 27045;
            Level = 68;
            Rank = 3;
            Range = 0;

            Mana = 150;

            IsGCD = true;
            Channeled = false;
            CastTime = 0;
            CooldownTime = 0;

            School = EnumSim.SchoolType.Nature;
            SecondarySchool = EnumSim.SecondarySchoolType.Hunter_BeastMastery;
            Init(sim);
        }
    }
}