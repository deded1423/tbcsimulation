using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Hunter.Spell
{
    public partial class AspectoftheBeastSpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public AspectoftheBeastSpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=13161/aspect-of-the-beast
            Name = "Aspect of the Beast";
            Id = 13161;
            Level = 30;
            Rank = 1;
            Range = 0;

            Mana = 50;

            IsGCD = true;
            Channeled = false;
            CastTime = 0;
            CooldownTime = 0;

            School = EnumSim.SchoolType.Nature;
            SecondarySchool = EnumSim.SecondarySchoolType.Hunter_BeastMastery;
            Init(sim);
        }
    }
}