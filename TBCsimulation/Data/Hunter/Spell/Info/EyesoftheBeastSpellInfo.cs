using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Hunter.Spell
{
    public partial class EyesoftheBeastSpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public EyesoftheBeastSpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=1002/eyes-of-the-beast
            Name = "Eyes of the Beast";
            Id = 1002;
            Level = 14;
            Rank = 1;
            Range = 50000;

            Mana = 20;

            IsGCD = true;
            Channeled = false;
            CastTime = 2000;
            CooldownTime = 0;

            School = EnumSim.SchoolType.Nature;
            SecondarySchool = EnumSim.SecondarySchoolType.Hunter_BeastMastery;
            Init(sim);
        }
    }
}