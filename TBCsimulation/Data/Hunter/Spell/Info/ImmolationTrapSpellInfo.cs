using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Hunter.Spell
{
    public partial class ImmolationTrapSpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public ImmolationTrapSpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=27023/immolation-trap
            Name = "Immolation Trap";
            Id = 27023;
            Level = 65;
            Rank = 6;
            Range = 0;

            Mana = 305;

            IsGCD = true;
            Channeled = false;
            CastTime = 0;
            CooldownTime = 30000;

            School = EnumSim.SchoolType.Fire;
            SecondarySchool = EnumSim.SecondarySchoolType.Hunter_Survival;
            Init(sim);
        }
    }
}