using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Hunter.Spell
{
    public partial class ScorpidStingSpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public ScorpidStingSpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=3043/scorpid-sting
            Name = "Scorpid Sting";
            Id = 3043;
            Level = 22;
            Rank = 1;
            Range = 35;

            Mana = -1;

            IsGCD = true;
            Channeled = false;
            CastTime = 0;
            CooldownTime = 0;

            School = EnumSim.SchoolType.Nature;
            SecondarySchool = EnumSim.SecondarySchoolType.Hunter_Marksmanship;
            Init(sim);
        }
    }
}