using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Hunter.Spell
{
    public partial class ScareBeastSpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public ScareBeastSpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=14327/scare-beast
            Name = "Scare Beast";
            Id = 14327;
            Level = 46;
            Rank = 3;
            Range = 30;

            Mana = 75;

            IsGCD = true;
            Channeled = false;
            CastTime = 1500;
            CooldownTime = 30000;

            School = EnumSim.SchoolType.Nature;
            SecondarySchool = EnumSim.SecondarySchoolType.Hunter_BeastMastery;
            Init(sim);
        }
    }
}