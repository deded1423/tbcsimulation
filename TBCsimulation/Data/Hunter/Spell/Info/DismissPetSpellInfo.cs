using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Hunter.Spell
{
    public partial class DismissPetSpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public DismissPetSpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=2641/dismiss-pet
            Name = "Dismiss Pet";
            Id = 2641;
            Level = 10;
            Rank = 1;
            Range = 10;

            Mana = -1;

            IsGCD = true;
            Channeled = false;
            CastTime = 5000;
            CooldownTime = 0;

            School = EnumSim.SchoolType.Physical;
            SecondarySchool = EnumSim.SecondarySchoolType.Hunter_BeastMastery;
            Init(sim);
        }
    }
}