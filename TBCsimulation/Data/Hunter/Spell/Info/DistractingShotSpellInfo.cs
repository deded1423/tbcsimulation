using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Hunter.Spell
{
    public partial class DistractingShotSpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public DistractingShotSpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=27020/distracting-shot
            Name = "Distracting Shot";
            Id = 27020;
            Level = 69;
            Rank = 7;
            Range = 35;

            Mana = 140;

            IsGCD = true;
            Channeled = false;
            CastTime = 0;
            CooldownTime = 8000;

            School = EnumSim.SchoolType.Arcane;
            SecondarySchool = EnumSim.SecondarySchoolType.Hunter_Marksmanship;
            Init(sim);
        }
    }
}