using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Hunter.Spell
{
    public partial class SerpentStingSpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public SerpentStingSpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=27016/serpent-sting
            Name = "Serpent Sting";
            Id = 27016;
            Level = 67;
            Rank = 10;
            Range = 35;

            Mana = 275;

            IsGCD = true;
            Channeled = false;
            CastTime = 0;
            CooldownTime = 0;

            School = EnumSim.SchoolType.Nature;
            SecondarySchool = EnumSim.SecondarySchoolType.Hunter_Marksmanship;
            Init(sim);
        }
    }
}