using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Hunter.Spell
{
    public partial class EagleEyeSpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public EagleEyeSpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=6197/eagle-eye
            Name = "Eagle Eye";
            Id = 6197;
            Level = 14;
            Rank = 1;
            Range = 50000;

            Mana = 25;

            IsGCD = true;
            Channeled = true;
            CastTime = 1000;
            CooldownTime = 0;

            School = EnumSim.SchoolType.Arcane;
            SecondarySchool = EnumSim.SecondarySchoolType.Hunter_BeastMastery;
            Init(sim);
        }
    }
}