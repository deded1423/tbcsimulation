using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Hunter.Spell
{
    public partial class AspectoftheMonkeySpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public AspectoftheMonkeySpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=13163/aspect-of-the-monkey
            Name = "Aspect of the Monkey";
            Id = 13163;
            Level = 4;
            Rank = 1;
            Range = 0;

            Mana = 20;

            IsGCD = true;
            Channeled = false;
            CastTime = 0;
            CooldownTime = 0;

            School = EnumSim.SchoolType.Nature;
            SecondarySchool = EnumSim.SecondarySchoolType.Hunter_BeastMastery;
            Init(sim);
        }
    }
}