using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Hunter.Spell
{
    public partial class AspectoftheCheetahSpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public AspectoftheCheetahSpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=5118/aspect-of-the-cheetah
            Name = "Aspect of the Cheetah";
            Id = 5118;
            Level = 20;
            Rank = 1;
            Range = 0;

            Mana = 40;

            IsGCD = true;
            Channeled = false;
            CastTime = 0;
            CooldownTime = 0;

            School = EnumSim.SchoolType.Nature;
            SecondarySchool = EnumSim.SecondarySchoolType.Hunter_BeastMastery;
            Init(sim);
        }
    }
}