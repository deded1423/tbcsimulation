using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Hunter.Spell
{
    public partial class NaturalArmorSpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public NaturalArmorSpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=24632/natural-armor
            Name = "Natural Armor";
            Id = 24632;
            Level = 60;
            Rank = 10;
            Range = 100;

            Mana = -1;

            IsGCD = false;
            Channeled = false;
            CastTime = 0;
            CooldownTime = 0;

            School = EnumSim.SchoolType.Physical;
            SecondarySchool = EnumSim.SecondarySchoolType.Hunter_BeastTraining;
            Init(sim);
        }
    }
}