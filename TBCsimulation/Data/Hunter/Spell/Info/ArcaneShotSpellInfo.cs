using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Hunter.Spell
{
    public partial class ArcaneShotSpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public ArcaneShotSpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=27019/arcane-shot
            Name = "Arcane Shot";
            Id = 27019;
            Level = 69;
            Rank = 9;
            Range = 35;

            Mana = 230;

            IsGCD = true;
            Channeled = false;
            CastTime = 0;
            CooldownTime = 6000;

            School = EnumSim.SchoolType.Arcane;
            SecondarySchool = EnumSim.SecondarySchoolType.Hunter_Marksmanship;
            Init(sim);
        }
    }
}