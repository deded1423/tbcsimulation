using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Hunter.Spell
{
    public partial class KillCommandSpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public KillCommandSpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=34026/kill-command
            Name = "Kill Command";
            Id = 34026;
            Level = 66;
            Rank = 1;
            Range = 45;

            Mana = 75;

            IsGCD = false;
            Channeled = false;
            CastTime = 0;
            CooldownTime = 5000;

            School = EnumSim.SchoolType.Physical;
            SecondarySchool = EnumSim.SecondarySchoolType.Hunter_BeastMastery;
            Init(sim);
        }
    }
}