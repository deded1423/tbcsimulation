using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Warrior.Spell
{
    public partial class SunderArmorSpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public SunderArmorSpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=25225/sunder-armor
            Name = "Sunder Armor";
            Id = 25225;
            Level = 67;
            Rank = 6;
            Range = 5;

            Mana = -1;

            IsGCD = true;
            Channeled = false;
            CastTime = 0;
            CooldownTime = 0;

            School = EnumSim.SchoolType.Physical;
            SecondarySchool = EnumSim.SecondarySchoolType.Warrior_Protection;
            Init(sim);
        }
    }
}