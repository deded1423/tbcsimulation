using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Warrior.Spell
{
    public partial class BattleStanceSpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public BattleStanceSpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=2457/battle-stance
            Name = "Battle Stance";
            Id = 2457;
            Level = 1;
            Rank = 1;
            Range = 0;

            Mana = -1;

            IsGCD = false;
            Channeled = false;
            CastTime = 0;
            CooldownTime = 1000;

            School = EnumSim.SchoolType.Physical;
            SecondarySchool = EnumSim.SecondarySchoolType.Warrior_Arms;
            Init(sim);
        }
    }
}