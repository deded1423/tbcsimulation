using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Warrior.Spell
{
    public partial class RendSpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public RendSpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=25208/rend
            Name = "Rend";
            Id = 25208;
            Level = 68;
            Rank = 8;
            Range = 5;

            Mana = -1;

            IsGCD = true;
            Channeled = false;
            CastTime = 0;
            CooldownTime = 0;

            School = EnumSim.SchoolType.Physical;
            SecondarySchool = EnumSim.SecondarySchoolType.Warrior_Arms;
            Init(sim);
        }
    }
}