using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Warrior.Spell
{
    public partial class VictoryRushSpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public VictoryRushSpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=34428/victory-rush
            Name = "Victory Rush";
            Id = 34428;
            Level = 62;
            Rank = 1;
            Range = 5;

            Mana = -1;

            IsGCD = true;
            Channeled = false;
            CastTime = 0;
            CooldownTime = 0;

            School = EnumSim.SchoolType.Physical;
            SecondarySchool = EnumSim.SecondarySchoolType.Warrior_Fury;
            Init(sim);
        }
    }
}