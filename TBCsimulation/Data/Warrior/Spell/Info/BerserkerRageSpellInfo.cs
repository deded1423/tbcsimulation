using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Warrior.Spell
{
    public partial class BerserkerRageSpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public BerserkerRageSpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=18499/berserker-rage
            Name = "Berserker Rage";
            Id = 18499;
            Level = 32;
            Rank = 1;
            Range = 0;

            Mana = -1;

            IsGCD = true;
            Channeled = false;
            CastTime = 0;
            CooldownTime = 30000;

            School = EnumSim.SchoolType.Physical;
            SecondarySchool = EnumSim.SecondarySchoolType.Warrior_Fury;
            Init(sim);
        }
    }
}