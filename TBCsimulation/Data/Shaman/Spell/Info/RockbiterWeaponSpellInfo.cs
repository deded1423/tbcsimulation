using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Shaman.Spell
{
    public partial class RockbiterWeaponSpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public RockbiterWeaponSpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=25485/rockbiter-weapon
            Name = "Rockbiter Weapon";
            Id = 25485;
            Level = 70;
            Rank = 9;
            Range = 0;

            Mana = 205;

            IsGCD = true;
            Channeled = false;
            CastTime = 0;
            CooldownTime = 0;

            School = EnumSim.SchoolType.Nature;
            SecondarySchool = EnumSim.SecondarySchoolType.Shaman_Enhancement;
            Init(sim);
        }
    }
}