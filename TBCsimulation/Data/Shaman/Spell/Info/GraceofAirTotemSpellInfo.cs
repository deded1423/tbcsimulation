using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Shaman.Spell
{
    public partial class GraceofAirTotemSpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public GraceofAirTotemSpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=25359/grace-of-air-totem
            Name = "Grace of Air Totem";
            Id = 25359;
            Level = 60;
            Rank = 3;
            Range = 0;

            Mana = 310;

            IsGCD = true;
            Channeled = false;
            CastTime = 0;
            CooldownTime = 0;

            School = EnumSim.SchoolType.Nature;
            SecondarySchool = EnumSim.SecondarySchoolType.Shaman_Enhancement;
            Init(sim);
        }
    }
}