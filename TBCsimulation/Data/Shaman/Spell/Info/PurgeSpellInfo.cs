using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Shaman.Spell
{
    public partial class PurgeSpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public PurgeSpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=8012/purge
            Name = "Purge";
            Id = 8012;
            Level = 32;
            Rank = 2;
            Range = 30;

            Mana = -1;

            IsGCD = true;
            Channeled = false;
            CastTime = 0;
            CooldownTime = 0;

            School = EnumSim.SchoolType.Nature;
            SecondarySchool = EnumSim.SecondarySchoolType.Shaman_ElementalCombat;
            Init(sim);
        }
    }
}