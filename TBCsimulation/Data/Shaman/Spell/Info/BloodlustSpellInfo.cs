using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Shaman.Spell
{
    public partial class BloodlustSpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public BloodlustSpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=2825/bloodlust
            Name = "Bloodlust";
            Id = 2825;
            Level = 70;
            Rank = 1;
            Range = 0;

            Mana = 750;

            IsGCD = true;
            Channeled = false;
            CastTime = 0;
            CooldownTime = 600000;

            School = EnumSim.SchoolType.Nature;
            SecondarySchool = EnumSim.SecondarySchoolType.Shaman_Enhancement;
            Init(sim);
        }
    }
}