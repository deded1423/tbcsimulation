using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Shaman.Spell
{
    public partial class PoisonCleansingTotemSpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public PoisonCleansingTotemSpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=8166/poison-cleansing-totem
            Name = "Poison Cleansing Totem";
            Id = 8166;
            Level = 22;
            Rank = 1;
            Range = 0;

            Mana = -1;

            IsGCD = true;
            Channeled = false;
            CastTime = 0;
            CooldownTime = 0;

            School = EnumSim.SchoolType.Nature;
            SecondarySchool = EnumSim.SecondarySchoolType.Shaman_Restoration;
            Init(sim);
        }
    }
}