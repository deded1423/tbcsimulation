using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Shaman.Spell
{
    public partial class FrostResistanceTotemSpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public FrostResistanceTotemSpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=25560/frost-resistance-totem
            Name = "Frost Resistance Totem";
            Id = 25560;
            Level = 67;
            Rank = 4;
            Range = 0;

            Mana = 235;

            IsGCD = true;
            Channeled = false;
            CastTime = 0;
            CooldownTime = 0;

            School = EnumSim.SchoolType.Fire;
            SecondarySchool = EnumSim.SecondarySchoolType.Shaman_Enhancement;
            Init(sim);
        }
    }
}