using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Shaman.Spell
{
    public partial class StrengthofEarthTotemSpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public StrengthofEarthTotemSpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=25528/strength-of-earth-totem
            Name = "Strength of Earth Totem";
            Id = 25528;
            Level = 65;
            Rank = 6;
            Range = 0;

            Mana = 300;

            IsGCD = true;
            Channeled = false;
            CastTime = 0;
            CooldownTime = 0;

            School = EnumSim.SchoolType.Nature;
            SecondarySchool = EnumSim.SecondarySchoolType.Shaman_Enhancement;
            Init(sim);
        }
    }
}