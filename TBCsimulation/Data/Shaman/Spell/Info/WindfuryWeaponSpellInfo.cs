using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Shaman.Spell
{
    public partial class WindfuryWeaponSpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public WindfuryWeaponSpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=25505/windfury-weapon
            Name = "Windfury Weapon";
            Id = 25505;
            Level = 68;
            Rank = 5;
            Range = 0;

            Mana = 190;

            IsGCD = true;
            Channeled = false;
            CastTime = 0;
            CooldownTime = 0;

            School = EnumSim.SchoolType.Nature;
            SecondarySchool = EnumSim.SecondarySchoolType.Shaman_Enhancement;
            Init(sim);
        }
    }
}