using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Shaman.Spell
{
    public partial class FireElementalTotemSpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public FireElementalTotemSpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=2894/fire-elemental-totem
            Name = "Fire Elemental Totem";
            Id = 2894;
            Level = 68;
            Rank = 1;
            Range = 0;

            Mana = 680;

            IsGCD = true;
            Channeled = false;
            CastTime = 0;
            CooldownTime = 1200000;

            School = EnumSim.SchoolType.Fire;
            SecondarySchool = EnumSim.SecondarySchoolType.Shaman_ElementalCombat;
            Init(sim);
        }
    }
}