using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Shaman.Spell
{
    public partial class FireNovaTotemSpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public FireNovaTotemSpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=25547/fire-nova-totem
            Name = "Fire Nova Totem";
            Id = 25547;
            Level = 70;
            Rank = 7;
            Range = 0;

            Mana = 765;

            IsGCD = true;
            Channeled = false;
            CastTime = 0;
            CooldownTime = 15000;

            School = EnumSim.SchoolType.Fire;
            SecondarySchool = EnumSim.SecondarySchoolType.Shaman_ElementalCombat;
            Init(sim);
        }
    }
}