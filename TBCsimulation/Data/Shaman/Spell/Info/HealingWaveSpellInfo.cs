using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Shaman.Spell
{
    public partial class HealingWaveSpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public HealingWaveSpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=25396/healing-wave
            Name = "Healing Wave";
            Id = 25396;
            Level = 70;
            Rank = 12;
            Range = 40;

            Mana = 720;

            IsGCD = true;
            Channeled = false;
            CastTime = 3000;
            CooldownTime = 0;

            School = EnumSim.SchoolType.Nature;
            SecondarySchool = EnumSim.SecondarySchoolType.Shaman_Restoration;
            Init(sim);
        }
    }
}