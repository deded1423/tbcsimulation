using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Shaman.Spell
{
    public partial class FlametongueWeaponSpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public FlametongueWeaponSpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=25489/flametongue-weapon
            Name = "Flametongue Weapon";
            Id = 25489;
            Level = 64;
            Rank = 7;
            Range = 0;

            Mana = 180;

            IsGCD = true;
            Channeled = false;
            CastTime = 0;
            CooldownTime = 0;

            School = EnumSim.SchoolType.Fire;
            SecondarySchool = EnumSim.SecondarySchoolType.Shaman_Enhancement;
            Init(sim);
        }
    }
}