using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Shaman.Spell
{
    public partial class StoneclawTotemSpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public StoneclawTotemSpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=25525/stoneclaw-totem
            Name = "Stoneclaw Totem";
            Id = 25525;
            Level = 67;
            Rank = 7;
            Range = 0;

            Mana = 170;

            IsGCD = true;
            Channeled = false;
            CastTime = 0;
            CooldownTime = 30000;

            School = EnumSim.SchoolType.Nature;
            SecondarySchool = EnumSim.SecondarySchoolType.Shaman_ElementalCombat;
            Init(sim);
        }
    }
}