using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Shaman.Spell
{
    public partial class FlameShockSpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public FlameShockSpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=29228/flame-shock
            Name = "Flame Shock";
            Id = 29228;
            Level = 60;
            Rank = 6;
            Range = 20;

            Mana = 450;

            IsGCD = true;
            Channeled = false;
            CastTime = 0;
            CooldownTime = 6000;

            School = EnumSim.SchoolType.Fire;
            SecondarySchool = EnumSim.SecondarySchoolType.Shaman_ElementalCombat;
            Init(sim);
        }
    }
}