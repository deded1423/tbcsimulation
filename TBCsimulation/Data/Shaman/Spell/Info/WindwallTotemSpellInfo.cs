using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Shaman.Spell
{
    public partial class WindwallTotemSpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public WindwallTotemSpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=25577/windwall-totem
            Name = "Windwall Totem";
            Id = 25577;
            Level = 65;
            Rank = 4;
            Range = 0;

            Mana = 265;

            IsGCD = true;
            Channeled = false;
            CastTime = 0;
            CooldownTime = 0;

            School = EnumSim.SchoolType.Nature;
            SecondarySchool = EnumSim.SecondarySchoolType.Shaman_Enhancement;
            Init(sim);
        }
    }
}