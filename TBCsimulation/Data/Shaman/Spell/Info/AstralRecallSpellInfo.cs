using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Shaman.Spell
{
    public partial class AstralRecallSpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public AstralRecallSpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=556/astral-recall
            Name = "Astral Recall";
            Id = 556;
            Level = 30;
            Rank = 1;
            Range = 0;

            Mana = 150;

            IsGCD = true;
            Channeled = false;
            CastTime = 10000;
            CooldownTime = 900000;

            School = EnumSim.SchoolType.Nature;
            SecondarySchool = EnumSim.SecondarySchoolType.Shaman_Enhancement;
            Init(sim);
        }
    }
}