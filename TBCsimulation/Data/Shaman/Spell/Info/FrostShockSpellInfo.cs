using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Shaman.Spell
{
    public partial class FrostShockSpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public FrostShockSpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=25464/frost-shock
            Name = "Frost Shock";
            Id = 25464;
            Level = 68;
            Rank = 5;
            Range = 20;

            Mana = 525;

            IsGCD = true;
            Channeled = false;
            CastTime = 0;
            CooldownTime = 6000;

            School = EnumSim.SchoolType.Frost;
            SecondarySchool = EnumSim.SecondarySchoolType.Shaman_ElementalCombat;
            Init(sim);
        }
    }
}