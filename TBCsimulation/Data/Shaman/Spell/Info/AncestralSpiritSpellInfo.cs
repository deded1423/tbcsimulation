using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Shaman.Spell
{
    public partial class AncestralSpiritSpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public AncestralSpiritSpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=20777/ancestral-spirit
            Name = "Ancestral Spirit";
            Id = 20777;
            Level = 60;
            Rank = 5;
            Range = 30;

            Mana = -1;

            IsGCD = true;
            Channeled = false;
            CastTime = 10000;
            CooldownTime = 0;

            School = EnumSim.SchoolType.Nature;
            SecondarySchool = EnumSim.SecondarySchoolType.Shaman_Restoration;
            Init(sim);
        }
    }
}