using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Shaman.Spell
{
    public partial class ChainLightningSpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public ChainLightningSpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=25442/chain-lightning
            Name = "Chain Lightning";
            Id = 25442;
            Level = 70;
            Rank = 6;
            Range = 30;

            Mana = 760;

            IsGCD = true;
            Channeled = false;
            CastTime = 2000;
            CooldownTime = 6000;

            School = EnumSim.SchoolType.Nature;
            SecondarySchool = EnumSim.SecondarySchoolType.Shaman_ElementalCombat;
            Init(sim);
        }
    }
}