using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Shaman.Spell
{
    public partial class EarthShockSpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public EarthShockSpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=25454/earth-shock
            Name = "Earth Shock";
            Id = 25454;
            Level = 69;
            Rank = 8;
            Range = 20;

            Mana = 535;

            IsGCD = true;
            Channeled = false;
            CastTime = 0;
            CooldownTime = 6000;

            School = EnumSim.SchoolType.Nature;
            SecondarySchool = EnumSim.SecondarySchoolType.Shaman_ElementalCombat;
            Init(sim);
        }
    }
}