using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Shaman.Spell
{
    public partial class NatureResistanceTotemSpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public NatureResistanceTotemSpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=25574/nature-resistance-totem
            Name = "Nature Resistance Totem";
            Id = 25574;
            Level = 69;
            Rank = 4;
            Range = 0;

            Mana = 245;

            IsGCD = true;
            Channeled = false;
            CastTime = 0;
            CooldownTime = 0;

            School = EnumSim.SchoolType.Nature;
            SecondarySchool = EnumSim.SecondarySchoolType.Shaman_Enhancement;
            Init(sim);
        }
    }
}