using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Shaman.Spell
{
    public partial class EarthbindTotemSpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public EarthbindTotemSpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=2484/earthbind-totem
            Name = "Earthbind Totem";
            Id = 2484;
            Level = 6;
            Rank = 1;
            Range = 0;

            Mana = -1;

            IsGCD = true;
            Channeled = false;
            CastTime = 0;
            CooldownTime = 15000;

            School = EnumSim.SchoolType.Nature;
            SecondarySchool = EnumSim.SecondarySchoolType.Shaman_ElementalCombat;
            Init(sim);
        }
    }
}