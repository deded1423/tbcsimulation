using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Shaman.Spell
{
    public partial class HealingStreamTotemSpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public HealingStreamTotemSpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=25567/healing-stream-totem
            Name = "Healing Stream Totem";
            Id = 25567;
            Level = 69;
            Rank = 6;
            Range = 0;

            Mana = 95;

            IsGCD = true;
            Channeled = false;
            CastTime = 0;
            CooldownTime = 0;

            School = EnumSim.SchoolType.Frost;
            SecondarySchool = EnumSim.SecondarySchoolType.Shaman_Restoration;
            Init(sim);
        }
    }
}