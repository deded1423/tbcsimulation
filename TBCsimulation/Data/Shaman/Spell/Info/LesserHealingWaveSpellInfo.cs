using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Shaman.Spell
{
    public partial class LesserHealingWaveSpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public LesserHealingWaveSpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=25420/lesser-healing-wave
            Name = "Lesser Healing Wave";
            Id = 25420;
            Level = 66;
            Rank = 7;
            Range = 40;

            Mana = 440;

            IsGCD = true;
            Channeled = false;
            CastTime = 1500;
            CooldownTime = 0;

            School = EnumSim.SchoolType.Nature;
            SecondarySchool = EnumSim.SecondarySchoolType.Shaman_Restoration;
            Init(sim);
        }
    }
}