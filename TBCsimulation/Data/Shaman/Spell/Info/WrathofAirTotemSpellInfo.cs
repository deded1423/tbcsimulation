using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Shaman.Spell
{
    public partial class WrathofAirTotemSpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public WrathofAirTotemSpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=3738/wrath-of-air-totem
            Name = "Wrath of Air Totem";
            Id = 3738;
            Level = 64;
            Rank = 1;
            Range = 0;

            Mana = 320;

            IsGCD = true;
            Channeled = false;
            CastTime = 0;
            CooldownTime = 0;

            School = EnumSim.SchoolType.Physical;
            SecondarySchool = EnumSim.SecondarySchoolType.Shaman_Enhancement;
            Init(sim);
        }
    }
}