using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Shaman.Spell
{
    public partial class EarthElementalTotemSpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public EarthElementalTotemSpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=2062/earth-elemental-totem
            Name = "Earth Elemental Totem";
            Id = 2062;
            Level = 66;
            Rank = 1;
            Range = 0;

            Mana = 705;

            IsGCD = true;
            Channeled = false;
            CastTime = 0;
            CooldownTime = 1200000;

            School = EnumSim.SchoolType.Nature;
            SecondarySchool = EnumSim.SecondarySchoolType.Shaman_Enhancement;
            Init(sim);
        }
    }
}