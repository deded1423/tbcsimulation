using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Shaman.Spell
{
    public partial class GroundingTotemSpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public GroundingTotemSpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=8177/grounding-totem
            Name = "Grounding Totem";
            Id = 8177;
            Level = 30;
            Rank = 1;
            Range = 0;

            Mana = -1;

            IsGCD = true;
            Channeled = false;
            CastTime = 0;
            CooldownTime = 15000;

            School = EnumSim.SchoolType.Nature;
            SecondarySchool = EnumSim.SecondarySchoolType.Shaman_Enhancement;
            Init(sim);
        }
    }
}