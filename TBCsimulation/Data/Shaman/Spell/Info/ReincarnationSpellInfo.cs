using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Shaman.Spell
{
    public partial class ReincarnationSpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public ReincarnationSpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=20608/reincarnation
            Name = "Reincarnation";
            Id = 20608;
            Level = 30;
            Rank = 1;
            Range = 0;

            Mana = -1;

            IsGCD = false;
            Channeled = false;
            CastTime = 0;
            CooldownTime = 1000;

            School = EnumSim.SchoolType.Nature;
            SecondarySchool = EnumSim.SecondarySchoolType.Shaman_Restoration;
            Init(sim);
        }
    }
}