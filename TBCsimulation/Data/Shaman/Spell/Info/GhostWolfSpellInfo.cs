using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Shaman.Spell
{
    public partial class GhostWolfSpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public GhostWolfSpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=2645/ghost-wolf
            Name = "Ghost Wolf";
            Id = 2645;
            Level = 20;
            Rank = 1;
            Range = 0;

            Mana = 100;

            IsGCD = true;
            Channeled = false;
            CastTime = 2000;
            CooldownTime = 0;

            School = EnumSim.SchoolType.Nature;
            SecondarySchool = EnumSim.SecondarySchoolType.Shaman_Enhancement;
            Init(sim);
        }
    }
}