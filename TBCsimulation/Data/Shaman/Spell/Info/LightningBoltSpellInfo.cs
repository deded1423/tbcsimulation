using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Shaman.Spell
{
    public partial class LightningBoltSpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public LightningBoltSpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=25449/lightning-bolt
            Name = "Lightning Bolt";
            Id = 25449;
            Level = 67;
            Rank = 12;
            Range = 30;

            Mana = 300;

            IsGCD = true;
            Channeled = false;
            CastTime = 2500;
            CooldownTime = 0;

            School = EnumSim.SchoolType.Nature;
            SecondarySchool = EnumSim.SecondarySchoolType.Shaman_ElementalCombat;
            Init(sim);
        }
    }
}