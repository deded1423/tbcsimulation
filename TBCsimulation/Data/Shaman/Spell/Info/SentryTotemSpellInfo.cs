using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Shaman.Spell
{
    public partial class SentryTotemSpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public SentryTotemSpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=6495/sentry-totem
            Name = "Sentry Totem";
            Id = 6495;
            Level = 34;
            Rank = 1;
            Range = 0;

            Mana = 65;

            IsGCD = true;
            Channeled = false;
            CastTime = 0;
            CooldownTime = 0;

            School = EnumSim.SchoolType.Nature;
            SecondarySchool = EnumSim.SecondarySchoolType.Shaman_Enhancement;
            Init(sim);
        }
    }
}