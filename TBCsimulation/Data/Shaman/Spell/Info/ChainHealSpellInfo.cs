using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Shaman.Spell
{
    public partial class ChainHealSpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public ChainHealSpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=25423/chain-heal
            Name = "Chain Heal";
            Id = 25423;
            Level = 68;
            Rank = 5;
            Range = 40;

            Mana = 540;

            IsGCD = true;
            Channeled = false;
            CastTime = 2500;
            CooldownTime = 0;

            School = EnumSim.SchoolType.Nature;
            SecondarySchool = EnumSim.SecondarySchoolType.Shaman_Restoration;
            Init(sim);
        }
    }
}