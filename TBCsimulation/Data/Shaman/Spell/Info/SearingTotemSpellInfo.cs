using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Shaman.Spell
{
    public partial class SearingTotemSpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public SearingTotemSpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=25533/searing-totem
            Name = "Searing Totem";
            Id = 25533;
            Level = 69;
            Rank = 7;
            Range = 0;

            Mana = 205;

            IsGCD = true;
            Channeled = false;
            CastTime = 0;
            CooldownTime = 0;

            School = EnumSim.SchoolType.Fire;
            SecondarySchool = EnumSim.SecondarySchoolType.Shaman_ElementalCombat;
            Init(sim);
        }
    }
}