using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Shaman.Spell
{
    public partial class TranquilAirTotemSpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public TranquilAirTotemSpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=25908/tranquil-air-totem
            Name = "Tranquil Air Totem";
            Id = 25908;
            Level = 50;
            Rank = 1;
            Range = 0;

            Mana = -1;

            IsGCD = true;
            Channeled = false;
            CastTime = 0;
            CooldownTime = 0;

            School = EnumSim.SchoolType.Nature;
            SecondarySchool = EnumSim.SecondarySchoolType.Shaman_Restoration;
            Init(sim);
        }
    }
}