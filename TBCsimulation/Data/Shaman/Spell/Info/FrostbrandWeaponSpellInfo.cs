using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;


namespace TBCsimulation.Data.Shaman.Spell
{
    public partial class FrostbrandWeaponSpell : SpellsInfo.Spell
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public FrostbrandWeaponSpell(FightSim sim)

        {
//http://tbc.wowhead.com/spell=25500/frostbrand-weapon
            Name = "Frostbrand Weapon";
            Id = 25500;
            Level = 66;
            Rank = 6;
            Range = 0;

            Mana = 185;

            IsGCD = true;
            Channeled = false;
            CastTime = 0;
            CooldownTime = 0;

            School = EnumSim.SchoolType.Frost;
            SecondarySchool = EnumSim.SecondarySchoolType.Shaman_Enhancement;
            Init(sim);
        }
    }
}