﻿using System;
using System.Collections.Generic;
using TBCsimulation.Data.SpellsInfo;
using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;

namespace TBCsimulation.Report
{
    public class DataReport
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        private List<BuffReport> BuffReports = new(200);
        private List<BuffReport> DebuffReports = new(200);

        private List<EventReport> ReportEvents = new(1000);

        private List<SpellReport> SpellReports = new(200);

        public void AddEvent(EventReport eventReport)
        {
            ReportEvents.Add(eventReport);
        }

        public void PrintResults()
        {
            log.Info("######  SPELL REPORT  ######");
            foreach (var spellReport in SpellReports)
            {
                log.Info("Spell: " + IdUtils.GetNameFromId(spellReport.Id) + "(" + spellReport.Id + ")");
                log.Info("\t" + "Casts: " + spellReport.Casts);
                log.Info("\t" + "ManaUsed: " + spellReport.ManaUsed);
                log.Info("\t" + "Crits: " + spellReport.Crits);
                log.Info("\t" + "Misses: " + spellReport.Misses);
                log.Info("\t" + "Total Damage: " + spellReport.GetTotalDamage());
                log.Info("");
            }

            log.Info("");
            log.Info("");
            log.Info("");
            log.Info("######  BUFF REPORT  ######");
            foreach (var buffReport in BuffReports)
            {
                log.Info("Buff: " + IdUtils.GetNameFromId(buffReport.Id) + "(" + buffReport.Id + ")");
                log.Info("\t" + "Gained: " + buffReport.Gained);
                log.Info("\t" + "Lost: " + buffReport.Lost);
                log.Info("");
            }

            log.Info("");
            log.Info("");
            log.Info("");
            log.Info("######  DEBUFF REPORT  ######");
            foreach (var buffReport in DebuffReports)
            {
                log.Info("Buff: " + IdUtils.GetNameFromId(buffReport.Id) + "(" + buffReport.Id + ")");
                log.Info("\t" + "Gained: " + buffReport.Gained);
                log.Info("\t" + "Lost: " + buffReport.Lost);
                log.Info("");
            }
        }

        public void ProcessEvents()
        {
            SpellReport spellReport;
            BuffReport buffReport;
            foreach (var reportEvent in ReportEvents)
                switch (reportEvent.TypeReport)
                {
                    case TypeReport.ProcessEvent:
                        break;
                    case TypeReport.Cast:
                        spellReport = getSpellReport(((Spell) reportEvent.Data[0]).Id);
                        spellReport.Casts++;
                        break;
                    case TypeReport.MissSpell:
                        spellReport = getSpellReport((int) reportEvent.Data[0]);
                        spellReport.Misses++;
                        break;
                    case TypeReport.ResistSpell:
                        break;
                    case TypeReport.Debuff:
                        buffReport = getDebuffReport(((BuffSim) reportEvent.Data[0]).Id);
                        buffReport.Gained.Add(reportEvent.Time);
                        break;
                    case TypeReport.Buff:
                        buffReport = getBuffReport(((BuffSim) reportEvent.Data[0]).Id);
                        buffReport.Gained.Add(reportEvent.Time);
                        break;
                    case TypeReport.FadeDebuff:
                        buffReport = getDebuffReport(((BuffSim) reportEvent.Data[0]).Id);
                        buffReport.Lost.Add(reportEvent.Time);
                        break;
                    case TypeReport.FadeBuff:
                        buffReport = getBuffReport(((BuffSim) reportEvent.Data[0]).Id);
                        buffReport.Lost.Add(reportEvent.Time);
                        break;
                    case TypeReport.Damage:
                        break;
                    case TypeReport.Heal:
                        break;
                    case TypeReport.ManaSpend:
                        spellReport = getSpellReport((int) reportEvent.Data[0]);
                        spellReport.ManaUsed += (int) reportEvent.Data[1];
                        break;
                    case TypeReport.ManaGain:
                        break;
                    case TypeReport.HealthLost:
                        break;
                    case TypeReport.TriggerDamage:
                        spellReport = getSpellReport((int) reportEvent.Data[2]);
                        var beforeDamage = (int) reportEvent.Data[2];
                        var realDamage = (int) reportEvent.Data[9];
                        var crit = (bool) reportEvent.Data[10];

                        spellReport.Damages.Add(realDamage);
                        if (crit)
                            spellReport.Crits++;
                        break;
                    case TypeReport.TickBuff:
                        break;
                    case TypeReport.TickDebuff:
                        break;
                    case TypeReport.TriggerDebuff:
                        break;
                    case TypeReport.EndChannel:
                        break;
                    case TypeReport.TickChannel:
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
        }

        private SpellReport getSpellReport(int Id)
        {
            if (SpellReports.Exists(report => report.Id == Id)) return SpellReports.Find(report => report.Id == Id);

            var spellReport = new SpellReport(Id);
            SpellReports.Add(spellReport);
            return spellReport;
        }

        private BuffReport getBuffReport(int Id)
        {
            if (BuffReports.Exists(report => report.Id == Id)) return BuffReports.Find(report => report.Id == Id);

            var buffReport = new BuffReport(Id);
            BuffReports.Add(buffReport);
            return buffReport;
        }

        private BuffReport getDebuffReport(int Id)
        {
            if (DebuffReports.Exists(report => report.Id == Id)) return DebuffReports.Find(report => report.Id == Id);

            var buffReport = new BuffReport(Id);
            DebuffReports.Add(buffReport);
            return buffReport;
        }
    }
}