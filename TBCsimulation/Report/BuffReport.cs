﻿using System.Collections.Generic;

namespace TBCsimulation.Report
{
    public class BuffReport
    {
        private int _id;

        public List<int> Gained = new();
        public List<int> Lost = new();

        public BuffReport(int Id)
        {
            _id = Id;
        }

        public int Id => _id;
    }
}