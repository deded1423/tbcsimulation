﻿using System.Collections.Generic;
using System.Linq;

namespace TBCsimulation.Report
{
    public class SpellReport
    {
        private int _id;

        public SpellReport(int Id)
        {
            _id = Id;
        }

        public int Casts { get; set; }
        public int Misses { get; set; }
        public int ManaUsed { get; set; }

        public List<int> Damages { get; } = new();

        public int Crits { get; set; }

        public int Id => _id;

        public int GetTotalDamage()
        {
            return Damages.Sum();
        }
    }
}