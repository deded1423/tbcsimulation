﻿using System.Collections.Generic;

namespace TBCsimulation.Report
{
    public enum TypeReport
    {
        ProcessEvent,

        Cast,
        MissSpell,
        ResistSpell,

        Debuff,
        Buff,
        FadeDebuff,
        FadeBuff,

        Damage,
        Heal,
        ManaSpend,
        ManaGain,
        HealthLost,

        TriggerDamage,
        TickBuff,
        TickDebuff,
        TriggerDebuff,
        EndChannel,
        TickChannel,

        WaitForResource
    }

    public class EventReport : IReportEvent
    {
        public List<object> Data;
        public int Time;
        public TypeReport TypeReport;

        public EventReport(int time, TypeReport typeReport, List<object> data)
        {
            Time = time;
            TypeReport = typeReport;
            Data = data;
        }
    }
}