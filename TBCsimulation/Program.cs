﻿using System;
using System.Collections.Generic;
using System.IO;
using CommandLine;
using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;

namespace TBCsimulation
{
    internal class Program
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        private static void Main(string[] args)
        {
            Parser.Default.ParseArguments<Options>(args)
                .WithParsed(RunOptions)
                .WithNotParsed(HandleParseError);
        }

        private static void RunOptions(Options opts)
        {
            //handle options
            if (opts.DataFiles)
            {
                DataParser.CreateDataFiles();
                return;
            }

            var test = new FightSim();
            test.Params[ParameterSim.RotationString] = File.ReadAllText(opts.RotationFile);


            for (int i = 0; i < 10; i++)
            {
                var t = ObjectExtensions.Copy(test);
                t.Heap = new();
                t.Simulate();
                // t.PrintResults();
            }
        }

        private static void HandleParseError(IEnumerable<Error> errs)
        {
            foreach (var err in errs) log.Error(err);
        }

        private class Options
        {
            //https://github.com/commandlineparser/commandline/wiki
            [Option("datafiles", Default = false,
                HelpText = "Option to create he files from the data")]
            public bool DataFiles { get; set; }

            [Option("wowhead", Default = false,
                HelpText = "Option to download WowHead Information")]
            public bool DownloadWowHead { get; set; }

            [Option('r', "rotation", Default = @"F:\Proyectos\TBCsimulation\Rotation.json",
                HelpText = "Json file that has the rotation")]
            public string RotationFile { get; set; }
        }
    }
}