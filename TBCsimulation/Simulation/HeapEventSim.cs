﻿using System;
using TBCsimulation.Data.SpellsInfo;

namespace TBCsimulation.Simulation
{
    public class HeapEventSim : IComparable
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public BuffSim BuffSim;
        public string Name;
        public Spell Spell;

        public int Time;
        public bool triggerRotation = false;

        public HeapEventSim(string Name, int Time)
        {
            this.Time = Time;
            this.Name = Name;
        }

        public HeapEventSim(string Name, int Time, Spell Spell)
        {
            this.Time = Time;
            this.Name = Name;
            this.Spell = Spell;
        }

        public HeapEventSim(string Name, int Time, BuffSim BuffSim)
        {
            this.Time = Time;
            this.Name = Name;
            this.BuffSim = BuffSim;
        }

        public int CompareTo(object obj)
        {
            if (obj is HeapEventSim)
                return Time.CompareTo(((HeapEventSim) obj).Time);
            else
                throw new NotSupportedException();
        }

        public void ReduceTime(int TimeToReduce)
        {
            Time -= TimeToReduce;
        }

        public void DoAction(FightSim sim)
        {
            if (Spell != null)
                Spell.ActionDelegate();
            else if (BuffSim != null)
                BuffSim.TickActionDelegate(sim);
            else
                log.Error("Action returned null " + ToString());
        }

        public override string ToString()
        {
            return Name + " - " + Time.ToString();
        }
    }
}