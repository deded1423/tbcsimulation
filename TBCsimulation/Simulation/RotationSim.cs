﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;
using TBCsimulation.Data.Utils;
using System.Reflection;
using System.Text.RegularExpressions;
using static TBCsimulation.Data.SpellsInfo.SpellId;

namespace TBCsimulation.Simulation
{
    public class RotationSim
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(MethodBase.GetCurrentMethod()?.DeclaringType);

        public Func<FightSim, bool> Condition;

        public Type TypeSpell;

        public RotationSim(Type typeSpell, Func<FightSim, bool> condition)
        {
            TypeSpell = typeSpell;
            Condition = condition;
        }

        public static List<RotationSim> ParseRotation(FightSim sim, string Rotation)
        {
            if (Rotation.Equals(""))
            {
                log.Error("Rotation is empty");
                throw new ArgumentException();
            }

            var rotationSims = new List<RotationSim>();

            var json = JObject.Parse(Rotation);
            log.Debug("Processing rotation...");
            foreach (var spellrotation in json)
            {
                log.Debug("Key: " + spellrotation.Key);
                log.Debug("Value: " + spellrotation.Value);
                log.Debug("Getting type: " + spellrotation.Key.Replace(" ", "") + "Spell");
                var spellType =
                    Type.GetType(
                        "TBCsimulation.Data." + EnumSim.ParseWowClassToString(sim.Player.GetClass()) + ".Spell." +
                        spellrotation.Key.Replace(" ", "") + "Spell", true);
                var spellId = GetIdFromName(spellrotation.Key);
                if (spellId == -1)
                {
                    log.Error(spellrotation.Key + " is not found in the id list");
                    throw new ArgumentException();
                }

                var conditionFuncs = new List<Func<FightSim, bool>>();
                var conditionJson = (JProperty) spellrotation.Value.First;

                conditionFuncs.Add(delegate(FightSim fightSim)
                {
                    log.Debug("!HasCD of " + spellId);
                    return !fightSim.HasCD(spellId);
                });

                while (conditionJson != null)
                {
                    conditionFuncs.Add(
                        GetConditionFunc(sim, conditionJson.Name.ToLower(), (JArray) conditionJson.Value));

                    conditionJson = (JProperty) conditionJson.Next;
                }

                Func<FightSim, bool> finalFunc = delegate(FightSim fightSim)
                {
                    foreach (var func in conditionFuncs)
                        if (!func(fightSim))
                        {
                            log.Debug("Condition = false");
                            return false;
                        }
                        else
                        {
                            log.Debug("Condition = true");
                        }

                    return true;
                };
                rotationSims.Add(new RotationSim(spellType, finalFunc));
            }

            return rotationSims;
        }

        private static Func<FightSim, bool> GetConditionFunc(FightSim sim, string Name, JArray Param)
        {
            var num_args = Param.Count;

            //HasDebuff 
            if (Name.Equals("hasdebuff"))
            {
                log.Debug("Condition: HasDebuff");
                var spellName = Param[0].ToString();
                var debuff = GetIdFromName(spellName);
                log.Debug("Debuff = " + debuff);
                Func<FightSim, bool> func = null;
                if (Param.Count == 1)
                {
                    func = delegate(FightSim s)
                    {
                        log.Debug("HasDebuff of " + debuff);
                        return s.HasDebuff(debuff);
                    };
                }
                else if (Param.Count == 3)
                {
                    var number = int.Parse((string) Param[2]);
                    if (((string) Param[1]).ToLower().Equals("below"))
                    {
                        func = delegate(FightSim s)
                        {
                            log.Debug("HasDebuff bellow " + number + "of " + debuff);
                            if (!s.HasDebuff(debuff))
                                return false;
                            var time = 0;
                            var buffSim = s.GetDebuff(debuff);
                            foreach (var eventSim in s.Heap)
                                if (buffSim.Equals(eventSim.BuffSim))
                                {
                                    log.Debug("Time is: " + buffSim.DurationRemaining + " - (" + buffSim.TickInterval +
                                              " - " + eventSim.Time + ")");
                                    time = buffSim.DurationRemaining - (buffSim.TickInterval - eventSim.Time);
                                    return time < number;
                                }

                            log.Error("No buff event found");
                            throw new NotSupportedException();
                        };
                    }
                    else if (((string) Param[1]).ToLower().Equals("above"))
                    {
                        func = delegate(FightSim s)
                        {
                            log.Debug("HasDebuff above " + number + "of " + debuff);
                            if (!s.HasDebuff(debuff))
                                return false;
                            var time = 0;
                            var buffSim = s.GetDebuff(debuff);
                            foreach (var eventSim in s.Heap)
                                if (buffSim.Equals(eventSim.BuffSim))
                                {
                                    log.Debug("Time is: " + buffSim.DurationRemaining + " - (" + buffSim.TickInterval +
                                              " - " + eventSim.Time + ")");
                                    time = buffSim.DurationRemaining - (buffSim.TickInterval - eventSim.Time);
                                    return time > number;
                                }

                            log.Error("No buff event found");
                            throw new NotSupportedException();
                        };
                    }
                    else
                    {
                        log.Error("Paramenters of Rotation are incorrect");
                        throw new ArgumentException();
                    }
                }
                else
                {
                    log.Error("Paramenters of Rotation are incorrect");
                    throw new ArgumentException();
                }

                return func;
            }

            //!HasDebuff
            if (Name.Equals("!hasdebuff"))
            {
                log.Debug("Condition: !HasDebuff");
                var spellName = Param[0].ToString();
                var debuff = GetIdFromName(spellName);
                log.Debug("Debuff = " + debuff);
                Func<FightSim, bool> func = null;
                if (Param.Count == 1)
                {
                    func = delegate(FightSim s)
                    {
                        log.Debug("!HasDebuff of " + debuff);
                        return !s.HasDebuff(debuff);
                    };
                }
                else if (Param.Count == 3)
                {
                    var number = int.Parse((string) Param[2]);
                    if (((string) Param[1]).ToLower().Equals("below"))
                    {
                        func = delegate(FightSim s)
                        {
                            log.Debug("!HasDebuff bellow " + number + "of " + debuff);
                            if (!s.HasDebuff(debuff))
                                return true;
                            var time = 0;
                            var buffSim = s.GetDebuff(debuff);
                            foreach (var eventSim in s.Heap)
                                if (buffSim.Equals(eventSim.BuffSim))
                                {
                                    log.Debug("Time is: " + buffSim.DurationRemaining + " - (" + buffSim.TickInterval +
                                              " - " + eventSim.Time + ")");
                                    time = buffSim.DurationRemaining - (buffSim.TickInterval - eventSim.Time);
                                    return time > number;
                                }

                            log.Error("No buff event found");
                            throw new NotSupportedException();
                        };
                    }
                    else if (((string) Param[1]).ToLower().Equals("above"))
                    {
                        func = delegate(FightSim s)
                        {
                            log.Debug("!HasDebuff above " + number + "of " + debuff);
                            if (!s.HasDebuff(debuff))
                                return true;
                            var time = 0;
                            var buffSim = s.GetDebuff(debuff);
                            foreach (var eventSim in s.Heap)
                                if (buffSim.Equals(eventSim.BuffSim))
                                {
                                    log.Debug("Time is: " + buffSim.DurationRemaining + " - (" + buffSim.TickInterval +
                                              " - " + eventSim.Time + ")");
                                    time = buffSim.DurationRemaining - (buffSim.TickInterval - eventSim.Time);
                                    return time < number;
                                }

                            log.Error("No buff event found");
                            throw new NotSupportedException();
                        };
                    }
                    else
                    {
                        log.Error("Paramenters of Rotation are incorrect");
                        throw new ArgumentException();
                    }
                }
                else
                {
                    log.Error("Paramenters of Rotation are incorrect");
                    throw new ArgumentException();
                }

                return func;
            }

            //HasCD
            if (Name.Equals("hascd"))
            {
                log.Debug("Condition: HasCD");
                var spellName = Param[0].ToString();
                var cd = GetIdFromName(spellName);
                ;
                log.Debug("CD = " + cd);
                Func<FightSim, bool> func = null;
                if (Param.Count == 1)
                {
                    func = delegate(FightSim s)
                    {
                        log.Debug("CD of " + cd);
                        return s.HasCD(cd);
                    };
                }
                else if (Param.Count == 3)
                {
                    var number = int.Parse((string) Param[2]);
                    if (((string) Param[1]).ToLower().Equals("below"))
                    {
                        func = delegate(FightSim s)
                        {
                            log.Debug("CD bellow " + number + "of " + cd);
                            if (!s.HasCD(cd))
                                return true;
                            var time = s.GetCD(cd);
                            return time < number;
                        };
                    }
                    else if (((string) Param[1]).ToLower().Equals("above"))
                    {
                        func = delegate(FightSim s)
                        {
                            log.Debug("CD above " + number + "of " + cd);
                            if (!s.HasCD(cd))
                                return false;

                            var time = s.GetCD(cd);
                            return time > number;
                        };
                    }
                    else
                    {
                        log.Error("Paramenters of Rotation are incorrect");
                        throw new ArgumentException();
                    }
                }
                else
                {
                    log.Error("Paramenters of Rotation are incorrect");
                    throw new ArgumentException();
                }

                return func;
            }

            //!HasCD
            if (Name.Equals("!hascd"))
            {
                log.Debug("Condition: !HasCD");
                var spellName = Param[0].ToString();
                var cd = GetIdFromName(spellName);
                ;
                log.Debug("CD = " + cd);
                Func<FightSim, bool> func = null;
                if (Param.Count == 1)
                {
                    func = delegate(FightSim s)
                    {
                        log.Debug("!CD of " + cd);
                        return !s.HasCD(cd);
                    };
                }
                else if (Param.Count == 3)
                {
                    var number = int.Parse((string) Param[2]);
                    if (((string) Param[1]).ToLower().Equals("below"))
                    {
                        func = delegate(FightSim s)
                        {
                            log.Debug("!CD bellow " + number + "of " + cd);
                            if (!s.HasCD(cd))
                                return false;
                            var time = s.GetCD(cd);
                            return time > number;
                        };
                    }
                    else if (((string) Param[1]).ToLower().Equals("above"))
                    {
                        func = delegate(FightSim s)
                        {
                            log.Debug("!CD above " + number + "of " + cd);
                            if (!s.HasCD(cd))
                                return true;

                            var time = s.GetCD(cd);
                            return time < number;
                        };
                    }
                    else
                    {
                        log.Error("Paramenters of Rotation are incorrect");
                        throw new ArgumentException();
                    }
                }
                else
                {
                    log.Error("Paramenters of Rotation are incorrect");
                    throw new ArgumentException();
                }

                return func;
            }

            //Player Mana
            if (Name.Equals("playermana"))
            {
                var normal = @"(?<number>\d+)$";
                var percentage = @"^(?<number>\d+)%$";

                log.Debug("Condition: PlayerMana");
                if (num_args != 2 ||
                    !((string) Param[0]).ToLower().Equals("below") && !((string) Param[0]).ToLower().Equals("below"))
                {
                    log.Error("Params of Player Mana are incorrect " + Param);
                    throw new ArgumentException();
                }

                var param = Param[1].ToString();

                if (((string) Param[0]).ToLower().Equals("below"))
                {
                    log.Debug("Below");

                    if (Regex.IsMatch(param, normal))
                    {
                        var number = int.Parse(Regex.Match(param, normal).Groups["number"].Value);

                        if (number < 0)
                        {
                            log.Error("Params of Player Mana are incorrect (Number) " + number);
                            throw new ArgumentException();
                        }

                        Func<FightSim, bool> func = delegate(FightSim s)
                        {
                            log.Debug("PlayerMana Bellow " + param);
                            return s.Player.GetCurrentMana() < number;
                        };
                        return func;
                    }
                    else if (Regex.IsMatch(param, percentage))
                    {
                        var number = int.Parse(Regex.Match(param, percentage).Groups["number"].Value);

                        if (number < 0 || number > 100)
                        {
                            log.Error("Params of Player Mana are incorrect (Number) " + number);
                            throw new ArgumentException();
                        }

                        Func<FightSim, bool> func = delegate(FightSim s)
                        {
                            log.Debug("PlayerMana Bellow " + param + "%");
                            return (float) s.Player.GetCurrentMana() / (float) s.Player.GetMaxMana() * 100 < number;
                        };
                        return func;
                    }
                }
                else if (((string) Param[0]).ToLower().Equals("above"))
                {
                    log.Debug("Above");

                    if (Regex.IsMatch(param, normal))
                    {
                        var number = int.Parse(Regex.Match(param, normal).Groups["number"].Value);

                        if (number < 0)
                        {
                            log.Error("Params of Player Mana are incorrect (Number) " + number);
                            throw new ArgumentException();
                        }

                        Func<FightSim, bool> func = delegate(FightSim s)
                        {
                            log.Debug("PlayerMana Above " + param);
                            return s.Player.GetCurrentMana() > number;
                        };
                        return func;
                    }
                    else if (Regex.IsMatch(param, percentage))
                    {
                        var number = int.Parse(Regex.Match(param, percentage).Groups["number"].Value);

                        if (number < 0 || number > 100)
                        {
                            log.Error("Params of Player Mana are incorrect (Number) " + number);
                            throw new ArgumentException();
                        }

                        Func<FightSim, bool> func = delegate(FightSim s)
                        {
                            log.Debug("PlayerMana Above " + param + "%");
                            return (float) s.Player.GetCurrentMana() / (float) s.Player.GetMaxMana() * 100 > number;
                        };
                        return func;
                    }
                }
            }

            //Player Health
            if (Name.Equals("playerhealth"))
            {
                var normal = @"(?<number>\d+)$";
                var percentage = @"^(?<number>\d+)%$";

                log.Debug("Condition: PlayerHealth");
                if (num_args != 2 ||
                    !((string) Param[0]).ToLower().Equals("below") && !((string) Param[0]).ToLower().Equals("below"))
                {
                    log.Error("Params of Player Health are incorrect " + Param);
                    throw new ArgumentException();
                }

                var param = Param[1].ToString();

                if (((string) Param[0]).ToLower().Equals("below"))
                {
                    log.Debug("Below");

                    if (Regex.IsMatch(param, normal))
                    {
                        var number = int.Parse(Regex.Match(param, normal).Groups["number"].Value);

                        if (number < 0)
                        {
                            log.Error("Params of Player Health are incorrect (Number) " + number);
                            throw new ArgumentException();
                        }

                        Func<FightSim, bool> func = delegate(FightSim s)
                        {
                            log.Debug("PlayerHealth Bellow " + param);
                            return s.Player.GetCurrentHealth() < number;
                        };
                        return func;
                    }
                    else if (Regex.IsMatch(param, percentage))
                    {
                        var number = int.Parse(Regex.Match(param, percentage).Groups["number"].Value);

                        if (number < 0 || number > 100)
                        {
                            log.Error("Params of Player Health are incorrect (Number) " + number);
                            throw new ArgumentException();
                        }

                        Func<FightSim, bool> func = delegate(FightSim s)
                        {
                            log.Debug("PlayerHealth Bellow " + param + "%");
                            return (float) s.Player.GetCurrentHealth() / (float) s.Player.GetMaxHealth() * 100 <
                                   number;
                        };
                        return func;
                    }
                }
                else if (((string) Param[0]).ToLower().Equals("above"))
                {
                    log.Debug("Above");

                    if (Regex.IsMatch(param, normal))
                    {
                        var number = int.Parse(Regex.Match(param, normal).Groups["number"].Value);

                        if (number < 0)
                        {
                            log.Error("Params of Player Health are incorrect (Number) " + number);
                            throw new ArgumentException();
                        }

                        Func<FightSim, bool> func = delegate(FightSim s)
                        {
                            log.Debug("PlayerHealth Above " + param);
                            return s.Player.GetCurrentHealth() > number;
                        };
                        return func;
                    }
                    else if (Regex.IsMatch(param, percentage))
                    {
                        var number = int.Parse(Regex.Match(param, percentage).Groups["number"].Value);

                        if (number < 0 || number > 100)
                        {
                            log.Error("Params of Player Health are incorrect (Number) " + number);
                            throw new ArgumentException();
                        }

                        Func<FightSim, bool> func = delegate(FightSim s)
                        {
                            log.Debug("PlayerHealth Above " + param + "%");
                            return (float) s.Player.GetCurrentHealth() / (float) s.Player.GetMaxHealth() * 100 >
                                   number;
                        };
                        return func;
                    }
                }
            }

            //Boss Health
            if (Name.Equals("bosshealth"))
            {
                var normal = @"(?<number>\d+)$";
                var percentage = @"^(?<number>\d+)%$";

                log.Debug("Condition: BossHealth");
                if (num_args != 2 ||
                    !((string) Param[0]).ToLower().Equals("below") && !((string) Param[0]).ToLower().Equals("below"))
                {
                    log.Error("Params of Boss Health are incorrect " + Param);
                    throw new ArgumentException();
                }

                var param = Param[1].ToString();

                if (((string) Param[0]).ToLower().Equals("below"))
                {
                    log.Debug("Below");

                    if (Regex.IsMatch(param, normal))
                    {
                        var number = int.Parse(Regex.Match(param, normal).Groups["number"].Value);

                        if (number < 0)
                        {
                            log.Error("Params of Boss Health are incorrect (Number) " + number);
                            throw new ArgumentException();
                        }

                        Func<FightSim, bool> func = delegate(FightSim s)
                        {
                            log.Debug("BossHealth Bellow " + param);
                            return s.Boss.GetCurrentHealth() < number;
                        };
                        return func;
                    }
                    else if (Regex.IsMatch(param, percentage))
                    {
                        var number = int.Parse(Regex.Match(param, percentage).Groups["number"].Value);

                        if (number < 0 || number > 100)
                        {
                            log.Error("Params of Boss Health are incorrect (Number) " + number);
                            throw new ArgumentException();
                        }

                        Func<FightSim, bool> func = delegate(FightSim s)
                        {
                            log.Debug("BossHealth Bellow " + param + "%");
                            return (float) s.Boss.GetCurrentHealth() / (float) s.Boss.GetMaxHealth() * 100 < number;
                        };
                        return func;
                    }
                }
                else if (((string) Param[0]).ToLower().Equals("above"))
                {
                    log.Debug("Above");

                    if (Regex.IsMatch(param, normal))
                    {
                        var number = int.Parse(Regex.Match(param, normal).Groups["number"].Value);

                        if (number < 0)
                        {
                            log.Error("Params of Boss Health are incorrect (Number) " + number);
                            throw new ArgumentException();
                        }

                        Func<FightSim, bool> func = delegate(FightSim s)
                        {
                            log.Debug("BossHealth Above " + param);
                            return s.Boss.GetCurrentHealth() > number;
                        };
                        return func;
                    }
                    else if (Regex.IsMatch(param, percentage))
                    {
                        var number = int.Parse(Regex.Match(param, percentage).Groups["number"].Value);

                        if (number < 0 || number > 100)
                        {
                            log.Error("Params of Boss Health are incorrect (Number) " + number);
                            throw new ArgumentException();
                        }

                        Func<FightSim, bool> func = delegate(FightSim s)
                        {
                            log.Debug("BossHealth Above " + param + "%");
                            return (float) s.Boss.GetCurrentHealth() / (float) s.Boss.GetMaxHealth() * 100 > number;
                        };
                        return func;
                    }
                }
            }

            //Boss Timetodeath
            if (Name.Equals("bosstimetodeath")) throw new NotImplementedException();

            throw new NotImplementedException();
        }
    }
}