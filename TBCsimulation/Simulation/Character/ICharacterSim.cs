﻿using System;
using TBCsimulation.Data.Utils;

namespace TBCsimulation.Simulation
{
    public abstract class ICharacterSim
    {
        ////General
        public abstract int GetLevel();

        public abstract float GetMovementSpeed();
        public abstract int GetHasteRating();
        public abstract float GetHaste();

        ////Melee
        public abstract int GetAttackPower();
        //public abstract float GetMeleeCritChance();
        //public abstract float GetMeleeHitChance();
        //public abstract float GetMeleeAttackSpeedMH();
        //public abstract float GetMeleeAttackSpeedOH();
        //public abstract bool MeleeDualWield();

        ////Ranged
        //public abstract int GetRangedAttackPower();
        //public abstract float GetRangedCritChance();
        //public abstract float GetRangedHitChance();
        //public abstract float GetRangedAttackSpeed();

        ////Defense
        public abstract int GetDefenseArmor();

        public abstract float GetDefenseSpellHit();
        //public abstract int GetDefenseDefense();
        //public abstract float GetDefenseBlockChance();
        //public abstract int GetDefenseBlockValue();
        //public abstract float GetDefenseParryChance();
        //public abstract float GetDefenseDodgeChance();

        ////Health
        public abstract int GetCurrentHealth();
        public abstract bool SetCurrentHealth(int add);
        public abstract int GetMaxHealth();
        public abstract float GetHealthMP5();

        ////Mana
        public abstract int GetCurrentMana();
        public abstract bool SetCurrentMana(int add);
        public abstract int GetMaxMana();
        public abstract float GetManaMP5();
        public abstract float GetManaMP5Casting();


        ////Spell

        public abstract float GetSpellCritChance();
        public abstract float GetSpellHitChance();
        public abstract int GetSpellDamage();

        ////Resistance
        public abstract int GetResistanceArcane();
        public abstract int GetResistanceFire();
        public abstract int GetResistanceFrost();
        public abstract int GetResistanceHoly();
        public abstract int GetResistanceNature();
        public abstract int GetResistancePhysical();
        public abstract int GetResistanceShadow();

        public int GetResistance(EnumSim.SchoolType schoolType)
        {
            switch (schoolType)
            {
                case EnumSim.SchoolType.Arcane:
                    return GetResistanceArcane();
                case EnumSim.SchoolType.Fire:
                    return GetResistanceFire();
                case EnumSim.SchoolType.Frost:
                    return GetResistanceFrost();
                case EnumSim.SchoolType.Holy:
                    return GetResistanceHoly();
                case EnumSim.SchoolType.Nature:
                    return GetResistanceNature();
                case EnumSim.SchoolType.Physical:
                    return GetResistancePhysical();
                case EnumSim.SchoolType.Shadow:
                    return GetResistanceShadow();
                default:
                    throw new NotSupportedException();
            }
        }
    }
}