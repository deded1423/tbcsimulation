﻿using System;
using TBCsimulation.Data.Utils;

namespace TBCsimulation.Simulation
{
    public class BossSim : IBossSim
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public int CurrentHealth = 0;
        public int DefenseArmor = 7500;
        public int DefenseArmorMod = 0;
        public float DefenseSpellhit = 17f;
        public float DefenseSpellhitMod = 0f;
        public int Level = 73;
        public int MaxHealth = 100000;
        public int MaxHealthMod = 0;
        public int MeleeAttackPower = 0;
        public int MeleeAttackPowerMod = 0;
        public EnumSim.NpcType Npc = EnumSim.NpcType.Humanoid;
        public int ResistanceArcane = 0;
        public int ResistanceArcaneMod = 0;
        public int ResistanceFire = 0;
        public int ResistanceFireMod = 0;
        public int ResistanceFrost = 0;
        public int ResistanceFrostMod = 0;
        public int ResistanceHoly = 0;
        public int ResistanceHolyMod = 0;
        public int ResistanceNature = 0;
        public int ResistanceNatureMod = 0;
        public int ResistancePhysical = 0;
        public int ResistancePhysicalMod = 0;
        public int ResistanceShadow = 0;
        public int ResistanceShadowMod = 0;


        //General
        public override int GetLevel()
        {
            return Level;
        }

        public override EnumSim.NpcType GetRace()
        {
            return Npc;
        }

        //Melee
        public override int GetAttackPower()
        {
            return Math.Max(MeleeAttackPower + MeleeAttackPowerMod, 0);
        }

        //Defenses
        public override int GetDefenseArmor()
        {
            return Math.Max(DefenseArmor + DefenseArmorMod, 0);
        }

        public override float GetDefenseSpellHit()
        {
            switch (GetLevel())
            {
                case 75:
                    return 39;
                case 74:
                    return 28;
                case 73:
                    return 17;
                case 72:
                    return 6;
                case 71:
                    return 5;
                case 70:
                    return 4;
                case 69:
                    return 3;
                case 68:
                    return 2;
                case 67:
                    return 1;
                case 66:
                    return 0;
            }

            return 17;
        }

        public override int GetResistanceArcane()
        {
            return Math.Max(ResistanceArcane + ResistanceArcaneMod, 0);
        }

        public override int GetResistanceFire()
        {
            return Math.Max(ResistanceFire + ResistanceFireMod, 0);
        }

        public override int GetResistanceFrost()
        {
            return Math.Max(ResistanceFrost + ResistanceFrostMod, 0);
        }

        public override int GetResistanceHoly()
        {
            return Math.Max(ResistanceHoly + ResistanceHolyMod, 0);
        }

        public override int GetResistanceNature()
        {
            return Math.Max(ResistanceNature + ResistanceNatureMod, 0);
        }

        public override int GetResistancePhysical()
        {
            return Math.Max(ResistancePhysical + ResistancePhysicalMod, 0);
        }

        public override int GetResistanceShadow()
        {
            return Math.Max(ResistanceShadow + ResistanceShadowMod, 0);
        }

        public override int GetMaxHealth()
        {
            var health = MaxHealth + MaxHealthMod;
            return Math.Max(health, 0);
        }

        public override int GetCurrentHealth()
        {
            return Math.Max(CurrentHealth, 0);
        }

        public override bool SetCurrentHealth(int health)
        {
            if (health <= 0)
            {
                CurrentHealth = 0;
                return true;
            }

            if (health > GetMaxHealth())
            {
                CurrentHealth = GetMaxHealth();
                return false;
            }

            CurrentHealth = health;
            return false;
        }

        //#################################################
        //#################NOT IMPLEMENTED#################
        //#################################################

        public override float GetMovementSpeed()
        {
            throw new NotImplementedException();
        }

        public override int GetHasteRating()
        {
            throw new NotImplementedException();
        }

        public override float GetHaste()
        {
            throw new NotImplementedException();
        }

        public override float GetHealthMP5()
        {
            throw new NotImplementedException();
        }

        public override int GetCurrentMana()
        {
            throw new NotImplementedException();
        }

        public override bool SetCurrentMana(int add)
        {
            throw new NotImplementedException();
        }

        public override int GetMaxMana()
        {
            throw new NotImplementedException();
        }

        public override float GetManaMP5()
        {
            throw new NotImplementedException();
        }

        public override float GetManaMP5Casting()
        {
            throw new NotImplementedException();
        }

        public override float GetSpellCritChance()
        {
            throw new NotImplementedException();
        }

        public override float GetSpellHitChance()
        {
            throw new NotImplementedException();
        }

        public override int GetSpellDamage()
        {
            throw new NotImplementedException();
        }
    }
}