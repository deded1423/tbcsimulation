﻿using System.Collections.Generic;
using TBCsimulation.Data.Utils;

namespace TBCsimulation.Simulation
{
    public abstract class IPlayerSim : ICharacterSim
    {
        ////General

        ////Stats
        public abstract float GetStatsStregth();
        public abstract float GetStatsAgility();
        public abstract float GetStatsStamina();
        public abstract float GetStatsIntellect();
        public abstract float GetStatsSpirit();

        ////Melee

        ////Ranged

        ////Defense

        ////Health

        ////Mana

        ////Spell
        public abstract int GetSpellHealingPower();

        public abstract int GetSpellCritRating();

        public abstract int GetSpellHitRating();

        public abstract int GetSpellArcaneDamage();
        public abstract float GetSpellArcaneCritRating();
        public abstract float GetSpellArcaneCritChance();
        public abstract int GetSpellFireDamage();
        public abstract float GetSpellFireCritRating();
        public abstract float GetSpellFireCritChance();
        public abstract int GetSpellFrostDamage();
        public abstract float GetSpellFrostCritRating();
        public abstract float GetSpellFrostCritChance();
        public abstract int GetSpellHolyDamage();
        public abstract float GetSpellHolyCritRating();
        public abstract float GetSpellHolyCritChance();
        public abstract int GetSpellNatureDamage();
        public abstract float GetSpellNatureCritRating();
        public abstract float GetSpellNatureCritChance();
        public abstract int GetSpellPhysicalDamage();
        public abstract float GetSpellPhysicalCritRating();
        public abstract float GetSpellPhysicalCritChance();
        public abstract int GetSpellShadowDamage();
        public abstract float GetSpellShadowCritRating();
        public abstract float GetSpellShadowCritChance();

        ////Resistance
        public abstract int GetMaxEnergy();
        public abstract int GetCurrentEnergy();
        public abstract int GetRegenEnergy();
        public abstract int GetMaxRage();
        public abstract int GetCurrentRage();
        public abstract int GetRegenRage();
        public abstract bool SetCurrentEnergy(int add);
        public abstract bool SetCurrentRage(int add);
        public abstract EnumSim.ResourceType GetResource();
        public abstract EnumSim.RaceType GetRace();
        public abstract int GetRangedAttackPower();
        public abstract EnumSim.WowClass GetClass();
        public abstract Dictionary<int, int> GetTalents();
        public abstract int GetTalentRank(int Id);
        public abstract bool HasTalent(int Id);
        public abstract float GetManaSpiritCasting();
        public abstract float GetCombatManaRegenPercentage();
        public abstract float GetManaSpirit();
        public abstract float GetCritDamage();
        public abstract float GetSpellCritDamage();
        public abstract void SetClass(EnumSim.WowClass wowClass);
        public abstract void SetRace(EnumSim.RaceType race);
        public abstract float GetCombatHealthRegenPercentage();
        public abstract float GetSpellHaste();
        public abstract int GetSpellHasteRating();
        public abstract float GetBlockChance();
        public abstract int GetBlockValue();
        public abstract int GetBlockRating();
        public abstract float GetDodgeChance();
        public abstract int GetDodgeRating();
        public abstract float GetDefense();
        public abstract int GetDefenseRating();
        public abstract float GetResilience();
        public abstract int GetResilienceRating();
        public abstract int GetIgnoreArmor();
        public abstract int GetExpertiseRating();
        public abstract float GetExpertise();
        public abstract float GetHitChance();
        public abstract int GetHitRating();
        public abstract float GetCritChance();
        public abstract int GetCritRating();
        public abstract int GetSpellPenetration();
    }
}