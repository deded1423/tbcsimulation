﻿using TBCsimulation.Data.Utils;

namespace TBCsimulation.Simulation
{
    public abstract class IBossSim : ICharacterSim
    {
        ////General

        ////Stats

        ////Melee

        ////Ranged

        ////Defense

        ////Health

        ////Mana

        ////Spell

        ////Resistance
        public abstract EnumSim.NpcType GetRace();
    }
}