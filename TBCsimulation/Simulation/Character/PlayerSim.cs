﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Reflection;
using TBCsimulation.Data.Utils;

namespace TBCsimulation.Simulation
{
    [SuppressMessage("ReSharper", "MemberCanBePrivate.Global")]
    public partial class PlayerSim : IPlayerSim
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public float Agility = 0f;
        public float AgilityMod = 0f;
        public int ArcaneCritRating = 0;
        public int ArcaneCritRatingMod = 0;
        public int ArcaneDamage = 0;
        public int ArcaneDamageMod = 0;
        public float CombatManaRegenPercentage = 0;
        public float CombatManaRegenPercentageMod = 0;
        public float CombatHealthRegenPercentage = 0;
        public float CombatHealthRegenPercentageMod = 0;
        public float CritRatio = 22.08f;
        private int CurrentEnergy = 0;
        private int CurrentHealth = 0;
        private int CurrentMana = 0;
        private int CurrentRage = 0;
        public int DefenseArmor = 0;
        public int DefenseArmorMod = 0;
        public float DefenseSpellhit = 0f;
        public float DefenseSpellhitMod = 0f;
        public float ExpertiseRatio = 3.9425f;
        public int FireCritRating = 0;
        public int FireCritRatingMod = 0;
        public int FireDamage = 0;
        public int FireDamageMod = 0;
        public float FlatCritMod = 0f;
        public float FlatHasteMod = 0f;
        public float FlatSpellHasteMod = 0f;
        public float FlatSpellHitMod = 0f;
        public int FrostCritRating = 0;
        public int FrostCritRatingMod = 0;
        public int FrostDamage = 0;
        public int FrostDamageMod = 0;
        public int HasteRating = 0;
        public int HasteRatingMod = 0;
        public int SpellHaste = 0;
        public int SpellHasteMod = 0;
        public float HasteRatio = 15.77f;
        public int HealingPower = 0;
        public int HealingPowerMod = 0;
        public float HealthP5 = 0;
        public float HealthP5Mod = 0;
        public float HitRatio = 12.6f;
        public float DefenseRatio = 2.3640f;
        public int HolyCritRating = 0;
        public int HolyCritRatingMod = 0;
        public int HolyDamage = 0;
        public int HolyDamageMod = 0;
        public float Intellect = 0f;
        public float IntellectMod = 0f;
        public int Level = 70;
        public float ManaP5 = 0;
        public float ManaP5Mod = 0;
        public int MaxEnergy = 0;
        public int MaxEnergyMod = 0;
        public int MaxHealthMod = 0;
        public int MaxManaMod = 0;
        public int MaxRage = 0;
        public int MaxRageMod = 0;
        public float MovementSpeed = 1f;
        public float MovementSpeedMod = 0f;
        public int NatureCritRating = 0;
        public int NatureCritRatingMod = 0;
        public int NatureDamage = 0;
        public int NatureDamageMod = 0;
        public int PhysicalCritRating = 0;
        public int PhysicalCritRatingMod = 0;
        public int PhysicalDamage = 0;
        public int PhysicalDamageMod = 0;
        public int RangedAttackPower = 0;
        public int RangedAttackPowerMod = 0;
        public int RegenEnergy = 0;
        public int RegenEnergyMod = 0;
        public int RegenRage = 0;
        public int RegenRageMod = 0;
        public int ResistanceArcane = 0;
        public int ResistanceArcaneMod = 0;
        public int ResistanceFire = 0;
        public int ResistanceFireMod = 0;
        public int ResistanceFrost = 0;
        public int ResistanceFrostMod = 0;
        public int ResistanceHoly = 0;
        public int ResistanceHolyMod = 0;
        public int ResistanceNature = 0;
        public int ResistanceNatureMod = 0;
        public int ResistancePhysical = 0;
        public int ResistancePhysicalMod = 0;
        public int ResistanceShadow = 0;
        public int ResistanceShadowMod = 0;
        public int ShadowCritRating = 0;
        public int ShadowCritRatingMod = 0;
        public int ShadowDamage = 0;
        public int ShadowDamageMod = 0;
        public float SpellCritDamage = 0.5f;
        public float SpellCritDamageMod = 0;
        public int SpellCritRating = 0;
        public int SpellCritRatingMod = 0;
        public int SpellDamage = 0;
        public int SpellDamageMod = 0;
        public int SpellHitRating = 0;
        public int SpellHitRatingMod = 0;
        public float Spirit = 0f;
        public float SpiritMod = 0f;
        public float Stamina = 0f;
        public float StaminaMod = 0f;
        public float Stregth = 0f;
        public float StregthMod = 0f;


        public int SpellPenetration = 0;
        public int SpellPenetrationMod = 0;

        public float CritDamage = 1f;
        public float CritDamageMod = 0;
        public int CritRating = 0;
        public int CritRatingMod = 0;
        public int HitRating = 0;
        public int HitRatingMod = 0;
        public int AttackPower = 0;
        public int AttackPowerMod = 0;
        public int ExpertiseRating = 0;
        public int ExpertiseRatingMod = 0;
        public int IgnoreArmor = 0;
        public int IgnoreArmorMod = 0;

        public int ResilienceRating = 0;
        public int ResilienceRatingMod = 0;
        public int DefenseRating = 0;
        public int DefenseRatingMod = 0;
        public int DodgeRating = 0;
        public int DodgeRatingMod = 0;
        public int BlockRating = 0;
        public int BlockRatingMod = 0;
        public int BlockValue = 0;
        public int BlockValueMod = 0;


        private float baseSpellCritChance = 0;
        private float baseDodgeChance = 0;
        private float IntellectSpellCritRatio = 0;
        private float AgilityAttackPowerRatio = 0;
        private float AgilityRangedAttackPowerRatio = 0;
        private float AgilityDodgeRatio = 0;
        private float AgilityCritRatio = 0;
        private float StrengthAttackPowerRatio = 0;


        public EnumSim.ResourceType Resource = EnumSim.ResourceType.Mana;
        public EnumSim.RaceType Race = EnumSim.RaceType.Undead;
        public Dictionary<int, int> Talents = new();
        public EnumSim.WowClass WowClass = EnumSim.WowClass.Warlock;

        ////General
        public override void SetClass(EnumSim.WowClass wowClass)
        {
            WowClass = wowClass;
            switch (wowClass)
            {
                case EnumSim.WowClass.Druid:
                    Resource = EnumSim.ResourceType.Mana;
                    break;
                case EnumSim.WowClass.Hunter:
                    Resource = EnumSim.ResourceType.Mana;
                    break;
                case EnumSim.WowClass.Mage:
                    Resource = EnumSim.ResourceType.Mana;
                    break;
                case EnumSim.WowClass.Paladin:
                    Resource = EnumSim.ResourceType.Mana;
                    break;
                case EnumSim.WowClass.Priest:
                    Resource = EnumSim.ResourceType.Mana;
                    break;
                case EnumSim.WowClass.Rogue:
                    Resource = EnumSim.ResourceType.Energy;
                    break;
                case EnumSim.WowClass.Shaman:
                    Resource = EnumSim.ResourceType.Mana;
                    break;
                case EnumSim.WowClass.Warrior:
                    Resource = EnumSim.ResourceType.Rage;
                    break;
                case EnumSim.WowClass.Warlock:
                    Resource = EnumSim.ResourceType.Mana;
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(wowClass), wowClass, null);
            }
        }

        public override EnumSim.WowClass GetClass()
        {
            return WowClass;
        }

        public override Dictionary<int, int> GetTalents()
        {
            return Talents;
        }

        public override int GetTalentRank(int Id)
        {
            if (!Talents.ContainsKey(Id))
                return 0;
            return Talents[Id];
        }

        public override bool HasTalent(int Id)
        {
            return Talents.ContainsKey(Id);
        }

        public override int GetLevel()
        {
            return Math.Max(Level, 0);
        }

        public override EnumSim.ResourceType GetResource()
        {
            return Resource;
        }

        public override void SetRace(EnumSim.RaceType race)
        {
            Race = race;
        }

        public override EnumSim.RaceType GetRace()
        {
            return Race;
        }

        public override float GetHaste()
        {
            return Math.Max(GetHasteRating() / HasteRatio + FlatHasteMod, 0);
        }

        public override int GetHasteRating()
        {
            return Math.Max(HasteRating + HasteRatingMod, 0);
        }

        public override float GetSpellHaste()
        {
            return Math.Max(GetSpellHasteRating() / HasteRatio + FlatSpellHasteMod, 0);
        }

        public override int GetSpellHasteRating()
        {
            return Math.Max(SpellHaste + SpellHasteMod, 0);
        }

        public override float GetMovementSpeed()
        {
            return Math.Max(MovementSpeed + MovementSpeedMod, 0);
        }


        ////Stats
        public override float GetStatsAgility()
        {
            return Math.Max(Agility + AgilityMod, 0);
        }

        public override float GetStatsIntellect()
        {
            var intellect = Intellect + IntellectMod;
            if (Race.Equals(EnumSim.RaceType.Gnome))
                intellect = intellect * 1.05f;
            return Math.Max(+intellect, 0);
        }

        public override float GetStatsSpirit()
        {
            var spirit = Spirit + SpiritMod;
            spirit *= 1f - 0.01f * GetTalentRank(Data.TalentsInfo.Talents.Warlock_DemonicEmbrace);
            if (Race.Equals(EnumSim.RaceType.Human))
                spirit *= 1.1f;
            return Math.Max(spirit, 0);
        }

        public override float GetStatsStamina()
        {
            var stam = Stamina + StaminaMod;
            stam *= 1f + 0.03f * GetTalentRank(Data.TalentsInfo.Talents.Warlock_DemonicEmbrace);
            return Math.Max(stam, 0);
        }

        public override float GetStatsStregth()
        {
            return Math.Max(Stregth + StregthMod, 0);
        }

        ////Health
        public override int GetMaxHealth()
        {
            var health = 3310 + (int) (GetStatsStamina() * 10) + MaxHealthMod;
            health = (int) (health * (1f + 0.01f * GetTalentRank(Data.TalentsInfo.Talents.Warlock_FelStamina)));
            if (Race.Equals(EnumSim.RaceType.Tauren))
                health = (int) (health * (1f + 0.05f));
            return Math.Max(health, 0);
        }

        public override int GetCurrentHealth()
        {
            return Math.Max(CurrentHealth, 0);
        }

        public override bool SetCurrentHealth(int health)
        {
            if (health <= 0)
            {
                CurrentHealth = 0;
                return true;
            }

            if (health > GetMaxHealth())
            {
                CurrentHealth = GetMaxHealth();
                return false;
            }

            CurrentHealth = health;
            return false;
        }

        public override float GetHealthMP5()
        {
            float health;
            switch (WowClass)
            {
                case EnumSim.WowClass.Druid:
                    health = GetStatsSpirit() * 0.09f + 6.5f;
                    break;
                case EnumSim.WowClass.Hunter:
                    health = GetStatsSpirit() * 0.25f + 6f;
                    break;
                case EnumSim.WowClass.Mage:
                    health = GetStatsSpirit() * 0.10f + 6f;
                    break;
                case EnumSim.WowClass.Paladin:
                    health = GetStatsSpirit() * 0.25f + 6f;
                    break;
                case EnumSim.WowClass.Priest:
                    health = GetStatsSpirit() * 0.10f + 6f;
                    break;
                case EnumSim.WowClass.Rogue:
                    health = GetStatsSpirit() * 0.50f + 2f;
                    break;
                case EnumSim.WowClass.Shaman:
                    health = GetStatsSpirit() * 0.11f + 7f;
                    break;
                case EnumSim.WowClass.Warrior:
                    health = GetStatsSpirit() * 0.50f + 6f;
                    break;
                case EnumSim.WowClass.Warlock:
                    health = GetStatsSpirit() * 0.07f + 6f;
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            health *= GetCombatHealthRegenPercentage();
            return Math.Max(health + HealthP5 + HealthP5Mod, 0);
            //OOC
            //https://wow.allakhazam.com/wiki/HP_Regen_%28WoW%29
        }

        public override float GetCombatHealthRegenPercentage()
        {
            return Math.Max(Math.Min(CombatHealthRegenPercentage + CombatHealthRegenPercentageMod, 1), 0);
        }

        ////Mana
        public override int GetMaxMana()
        {
            var mana = 2335 + (int) (GetStatsIntellect() * 15) + MaxManaMod;
            mana = (int) (mana * (1f + 0.01f * GetTalentRank(Data.TalentsInfo.Talents.Warlock_FelIntellect)));
            return Math.Max(mana, 0);
        }

        public override int GetCurrentMana()
        {
            return Math.Max(CurrentMana, 0);
        }

        public override bool SetCurrentMana(int mana)
        {
            if (mana <= 0)
            {
                CurrentMana = 0;
                return true;
            }

            if (mana > GetMaxMana())
            {
                CurrentMana = GetMaxMana();
                return false;
            }

            CurrentMana = mana;
            return false;
        }

        public override float GetManaMP5()
        {
            return Math.Max(ManaP5 + ManaP5Mod + GetManaSpirit(), 0);
        }

        public override float GetManaMP5Casting()
        {
            return Math.Max(ManaP5 + ManaP5Mod + GetManaSpiritCasting(), 0);
        }

        public override float GetManaSpirit()
        {
            return Math.Max(0.04663576106305f * (float) Math.Sqrt(GetStatsIntellect()) * GetStatsSpirit(),
                0); //Regen_mp5 = 5 * 0.00932715221261 * sqrt(Int) * Spirit
        }

        public override float GetManaSpiritCasting()
        {
            return Math.Max(GetManaSpirit() * GetCombatManaRegenPercentage(), 0);
        }

        public override float GetCombatManaRegenPercentage()
        {
            return Math.Max(Math.Min(CombatManaRegenPercentage + CombatManaRegenPercentageMod, 1), 0);
        }

        public override int GetMaxEnergy()
        {
            var energy = MaxEnergy + MaxEnergyMod;
            return Math.Max(energy, 0);
        }

        public override int GetCurrentEnergy()
        {
            return Math.Max(CurrentEnergy, 0);
        }

        public override bool SetCurrentEnergy(int energy)
        {
            if (energy <= 0)
            {
                CurrentEnergy = 0;
                return true;
            }

            if (energy > GetMaxEnergy())
            {
                CurrentEnergy = GetMaxEnergy();
                return false;
            }

            CurrentEnergy = energy;
            return false;
        }

        public override int GetRegenEnergy()
        {
            var energy = RegenEnergy + RegenEnergyMod;
            return Math.Max(energy, 0);
        }

        public override int GetMaxRage()
        {
            var rage = MaxRage + MaxRageMod;
            return Math.Max(rage, 0);
        }

        public override int GetCurrentRage()
        {
            return Math.Max(CurrentRage, 0);
        }

        public override bool SetCurrentRage(int rage)
        {
            if (rage <= 0)
            {
                CurrentRage = 0;
                return true;
            }

            if (rage > GetMaxRage())
            {
                CurrentRage = GetMaxRage();
                return false;
            }

            CurrentRage = rage;
            return false;
        }

        public override int GetRegenRage()
        {
            var rage = RegenRage + RegenRageMod;
            return Math.Max(rage, 0);
        }


        ////Melee
        public override int GetAttackPower()
        {
            return (int) Math.Max(
                StrengthAttackPowerRatio * GetStatsStregth() + AgilityAttackPowerRatio * GetStatsAgility() +
                AttackPower + AttackPowerMod, 0);
        }

        public override float GetCritDamage()
        {
            return Math.Max(CritDamage + CritDamageMod, 0f);
        }

        public override int GetCritRating()
        {
            return (int) Math.Max(CritRating + CritRatingMod, 0);
        }

        public override float GetCritChance()
        {
            throw new NotImplementedException();
            // return (int) Math.Max(CritRating +CritRatingMod, 0);
        }

        public override int GetHitRating()
        {
            return (int) Math.Max(HitRating + HitRatingMod, 0);
        }

        public override float GetHitChance()
        {
            return (int) Math.Max(GetHitRating() / HitRatio, 0);
        }

        public override int GetExpertiseRating()
        {
            return (int) Math.Max(ExpertiseRating + ExpertiseRatingMod, 0);
        }

        public override float GetExpertise()
        {
            return (int) Math.Max(GetExpertiseRating() / ExpertiseRatio, 0);
        }

        public override int GetIgnoreArmor()
        {
            return (int) Math.Max(IgnoreArmor + IgnoreArmorMod, 0);
        }

        public override int GetResilienceRating()
        {
            return (int) Math.Max(ResilienceRating + ResilienceRatingMod, 0);
        }

        public override float GetResilience()
        {
            return (int) Math.Max(GetResilienceRating() / ExpertiseRatio, 0);
        }


        ////Ranged
        public override int GetRangedAttackPower()
        {
            return (int) Math.Max(
                GetStatsAgility() * AgilityRangedAttackPowerRatio + RangedAttackPower + RangedAttackPowerMod, 0);
        }


        ////Spell
        public override int GetSpellHealingPower()
        {
            return Math.Max(HealingPower + HealingPowerMod, 0);
        }

        public override int GetSpellCritRating()
        {
            return Math.Max(SpellCritRating + SpellCritRatingMod, 0);
        }

        public override float GetSpellCritChance()
        {
            var crit = GetSpellCritRating() / CritRatio + GetStatsIntellect() / IntellectSpellCritRatio +
                       baseSpellCritChance + FlatCritMod;
            crit += 1f * GetTalentRank(Data.TalentsInfo.Talents.Warlock_DemonicTactics);
            crit += 1f * GetTalentRank(Data.TalentsInfo.Talents.Warlock_Backlash);
            return Math.Max(crit, 0);
        }

        public override int GetSpellHitRating()
        {
            return Math.Max(SpellHitRating + SpellHitRatingMod, 0);
        }

        public override float GetSpellHitChance()
        {
            return Math.Max(GetSpellHitRating() / HitRatio + FlatSpellHitMod, 0);
        }

        public override int GetSpellDamage()
        {
            return Math.Max(SpellDamage + SpellDamageMod, 0);
        }

        public override float GetSpellCritDamage()
        {
            return Math.Max(SpellCritDamage + SpellCritDamageMod, 0);
        }

        public override int GetSpellArcaneDamage()
        {
            return Math.Max(ArcaneDamage + SpellDamage + ArcaneDamageMod + SpellDamageMod, 0);
        }

        public override float GetSpellArcaneCritRating()
        {
            return Math.Max(ArcaneCritRating + SpellCritRating + ArcaneCritRatingMod + SpellCritRatingMod, 0);
        }

        public override float GetSpellArcaneCritChance()
        {
            return Math.Max(GetSpellArcaneCritRating() / CritRatio + 1.701f + GetStatsIntellect() / 81.92f, 0);
        }

        public override int GetSpellFireDamage()
        {
            return Math.Max(FireDamage + SpellDamage + FireDamageMod + SpellDamageMod, 0);
        }

        public override float GetSpellFireCritRating()
        {
            return Math.Max(FireCritRating + SpellCritRating + FireCritRatingMod + SpellCritRatingMod, 0);
        }

        public override float GetSpellFireCritChance()
        {
            return Math.Max(GetSpellFireCritRating() / CritRatio + 1.701f + GetStatsIntellect() / 81.92f, 0);
        }

        public override int GetSpellFrostDamage()
        {
            return Math.Max(FrostDamage + SpellDamage + FrostDamageMod + SpellDamageMod, 0);
        }

        public override float GetSpellFrostCritRating()
        {
            return Math.Max(FrostCritRating + SpellCritRating + FrostCritRatingMod + SpellCritRatingMod, 0);
        }

        public override float GetSpellFrostCritChance()
        {
            return Math.Max(GetSpellFrostCritRating() / CritRatio + 1.701f + GetStatsIntellect() / 81.92f, 0);
        }

        public override int GetSpellHolyDamage()
        {
            return Math.Max(HolyDamage + SpellDamage + HolyDamageMod + SpellDamageMod, 0);
        }

        public override float GetSpellHolyCritRating()
        {
            return Math.Max(HolyCritRating + SpellCritRating + HolyCritRatingMod + SpellCritRatingMod, 0);
        }

        public override float GetSpellHolyCritChance()
        {
            return Math.Max(GetSpellHolyCritRating() / CritRatio + 1.701f + GetStatsIntellect() / 81.92f, 0);
        }

        public override int GetSpellNatureDamage()
        {
            return Math.Max(NatureDamage + SpellDamage + NatureDamageMod + SpellDamageMod, 0);
        }

        public override float GetSpellNatureCritRating()
        {
            return Math.Max(NatureCritRating + SpellCritRating + NatureCritRatingMod + SpellCritRatingMod, 0);
        }

        public override float GetSpellNatureCritChance()
        {
            return Math.Max(GetSpellNatureCritRating() / CritRatio + 1.701f + GetStatsIntellect() / 81.92f, 0);
        }

        public override int GetSpellPhysicalDamage()
        {
            return Math.Max(PhysicalDamage + SpellDamage + PhysicalDamageMod + SpellDamageMod, 0);
        }

        public override float GetSpellPhysicalCritRating()
        {
            return Math.Max(PhysicalCritRating + SpellCritRating + PhysicalCritRatingMod + SpellCritRatingMod, 0);
        }

        public override float GetSpellPhysicalCritChance()
        {
            return Math.Max(GetSpellPhysicalCritRating() / CritRatio + 1.701f + GetStatsIntellect() / 81.92f, 0);
        }

        public override int GetSpellShadowDamage()
        {
            return Math.Max(ShadowDamage + SpellDamage + ShadowDamageMod + SpellDamageMod, 0);
        }

        public override float GetSpellShadowCritRating()
        {
            return Math.Max(ShadowCritRating + SpellCritRating + ShadowCritRatingMod + SpellCritRatingMod, 0);
        }

        public override float GetSpellShadowCritChance()
        {
            return Math.Max(GetSpellShadowCritRating() / CritRatio + 1.701f + GetStatsIntellect() / 81.92f, 0);
        }

        public override int GetSpellPenetration()
        {
            return Math.Max(SpellPenetration + SpellPenetrationMod, 0);
        }

        ////Defenses
        public override int GetDefenseArmor()
        {
            return (int) Math.Max(GetStatsAgility() * 2 + DefenseArmor + DefenseArmorMod, 0);
        }

        public override int GetDefenseRating()
        {
            return Math.Max(DefenseRating + DefenseRatingMod, 0);
        }

        public override float GetDefense()
        {
            return Math.Max(GetDefenseRating() / DefenseRatio, 0);
        }

        public override int GetDodgeRating()
        {
            return Math.Max(DodgeRating + DodgeRatingMod, 0);
        }

        public override float GetDodgeChance()
        {
            // https://wowwiki-archive.fandom.com/wiki/Dodge?oldid=1217144
            throw new NotImplementedException();
            // return Math.Max(GetDodgeRating() / DodgeRatio+baseDodgeChance, 0);
        }

        public override int GetBlockRating()
        {
            return Math.Max(BlockRating + BlockRatingMod, 0);
        }

        public override float GetBlockChance()
        {
            throw new NotImplementedException();
            // return Math.Max(GetBlockRating() / BlockRatio, 0);
        }

        public override int GetBlockValue()
        {
            return Math.Max(BlockValue + BlockValueMod, 0);
        }

        public override float GetDefenseSpellHit()
        {
            return Math.Max(DefenseSpellhit + DefenseSpellhitMod, 0);
        }

        public override int GetResistanceArcane()
        {
            return Math.Max(ResistanceArcane + ResistanceArcaneMod, 0);
        }

        public override int GetResistanceFire()
        {
            return Math.Max(ResistanceFire + ResistanceFireMod, 0);
        }

        public override int GetResistanceFrost()
        {
            return Math.Max(ResistanceFrost + ResistanceFrostMod, 0);
        }

        public override int GetResistanceHoly()
        {
            return Math.Max(ResistanceHoly + ResistanceHolyMod, 0);
        }

        public override int GetResistanceNature()
        {
            return Math.Max(ResistanceNature + ResistanceNatureMod, 0);
        }

        public override int GetResistancePhysical()
        {
            return Math.Max(ResistancePhysical + ResistancePhysicalMod, 0);
        }

        public override int GetResistanceShadow()
        {
            return Math.Max(ResistanceShadow + ResistanceShadowMod, 0);
        }

        public void PrintStats()
        {
            log.Info("STATS OF PLAYER");
            foreach (FieldInfo fieldInfo in typeof(PlayerSim).GetFields())
            {
                log.Info("\t" + fieldInfo.Name + ": " + fieldInfo.GetValue(this));
            }
        }
    }
}