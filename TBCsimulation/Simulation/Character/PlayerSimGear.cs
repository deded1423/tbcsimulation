﻿using System;
using System.Collections.Generic;
using System.Linq;
using TBCsimulation.Data.Items;
using TBCsimulation.Data.Utils;

namespace TBCsimulation.Simulation
{
    public partial class PlayerSim : IPlayerSim
    {
        public Item Head = null;
        public List<Gem> HeadSockets = null;
        public Item Neck = null;
        public List<Gem> NeckSockets = null;
        public Item Shoulder = null;
        public List<Gem> ShoulderSockets = null;
        public Item Back = null;
        public List<Gem> BackSockets = null;
        public Item Chest = null;
        public List<Gem> ChestSockets = null;
        public Item Wrist = null;
        public List<Gem> WristSockets = null;
        public Item Hands = null;
        public List<Gem> HandsSockets = null;
        public Item Waist = null;
        public List<Gem> WaistSockets = null;
        public Item Legs = null;
        public List<Gem> LegsSockets = null;
        public Item Feet = null;
        public List<Gem> FeetSockets = null;
        public Item Finger1 = null;
        public List<Gem> Finger1Sockets = null;
        public Item Finger2 = null;
        public List<Gem> Finger2Sockets = null;
        public Item Trinket1 = null;
        public List<Gem> Trinket1Sockets = null;
        public Item Trinket2 = null;
        public List<Gem> Trinket2Sockets = null;
        public Item MainHand = null;
        public List<Gem> MainHandSockets = null;
        public Item OffHand = null;
        public List<Gem> OffHandSockets = null;
        public Item Ranged = null;
        public List<Gem> RangedSockets = null;

        public List<Item> GetAllGear()
        {
            return new()
            {
                Head,
                Neck,
                Shoulder,
                Back,
                Chest,
                Wrist,
                Hands,
                Waist,
                Legs,
                Feet,
                Finger1,
                Finger2,
                Trinket1,
                Trinket2,
                MainHand,
                OffHand,
                Ranged,
            };
        }

        public void SetGear(List<(string, string[])> items, List<(string, string[])> weapons)
        {
            Head = null;
            Neck = null;
            Shoulder = null;
            Back = null;
            Chest = null;
            Wrist = null;
            Hands = null;
            Waist = null;
            Legs = null;
            Feet = null;
            Finger1 = null;
            Finger2 = null;
            Trinket1 = null;
            Trinket2 = null;
            MainHand = null;
            OffHand = null;
            Ranged = null;

            Gem[] gems = null;
            foreach (var i in items)
            {
                Item item = Item.GetItem(ItemId.GetId(i.Item1), i.Item1);

                if (i.Item2 == null)
                {
                    gems = new Gem[0];
                }
                else
                {
                    gems = new Gem[i.Item2.Length];

                    for (int j = 0; j < i.Item2.Length; j++)
                    {
                        Gem gem = Gem.Gems[GemId.GetId(i.Item2[j])];
                        gems[j] = gem;
                    }
                }

                switch (item.Slot)
                {
                    case EnumSim.Slot.Waist:
                        if (Waist != null)
                        {
                            log.Fatal("Waist Item!!!");
                            throw new ArgumentOutOfRangeException();
                        }

                        WaistSockets = gems.ToList();
                        Waist = item;
                        break;
                    case EnumSim.Slot.Wrist:
                        if (Wrist != null)
                        {
                            log.Fatal("Wrist Item!!!");
                            throw new ArgumentOutOfRangeException();
                        }

                        WristSockets = gems.ToList();
                        Wrist = item;
                        break;
                    case EnumSim.Slot.Head:
                        if (Head != null)
                        {
                            log.Fatal("Head Item!!!");
                            throw new ArgumentOutOfRangeException();
                        }

                        HeadSockets = gems.ToList();
                        Head = item;
                        break;
                    case EnumSim.Slot.Chest:
                        if (Chest != null)
                        {
                            log.Fatal("Chest Item!!!");
                            throw new ArgumentOutOfRangeException();
                        }

                        ChestSockets = gems.ToList();
                        Chest = item;
                        break;
                    case EnumSim.Slot.Feet:
                        if (Feet != null)
                        {
                            log.Fatal("Feet Item!!!");
                            throw new ArgumentOutOfRangeException();
                        }

                        FeetSockets = gems.ToList();
                        Feet = item;
                        break;
                    case EnumSim.Slot.Hands:
                        if (Hands != null)
                        {
                            log.Fatal("Hands Item!!!");
                            throw new ArgumentOutOfRangeException();
                        }

                        HandsSockets = gems.ToList();
                        Hands = item;
                        break;
                    case EnumSim.Slot.Neck:
                        if (Neck != null)
                        {
                            log.Fatal("Neck Item!!!");
                            throw new ArgumentOutOfRangeException();
                        }

                        NeckSockets = gems.ToList();
                        Neck = item;
                        break;
                    case EnumSim.Slot.Shoulder:
                        if (Shoulder != null)
                        {
                            log.Fatal("Shoulder Item!!!");
                            throw new ArgumentOutOfRangeException();
                        }

                        ShoulderSockets = gems.ToList();
                        Shoulder = item;
                        break;
                    case EnumSim.Slot.Legs:
                        if (Legs != null)
                        {
                            log.Fatal("Legs Item!!!");
                            throw new ArgumentOutOfRangeException();
                        }

                        LegsSockets = gems.ToList();
                        Legs = item;
                        break;
                    case EnumSim.Slot.Trinket:
                        if (Trinket1 != null)
                        {
                            if (Trinket2 != null)
                            {
                                log.Fatal("Trinket Item!!!");
                                throw new ArgumentOutOfRangeException();
                            }

                            Trinket2Sockets = gems.ToList();
                            Trinket2 = item;
                            break;
                        }

                        Trinket1Sockets = gems.ToList();
                        Trinket1 = item;
                        break;
                    case EnumSim.Slot.Back:
                        if (Back != null)
                        {
                            log.Fatal("Back Item!!!");
                            throw new ArgumentOutOfRangeException();
                        }

                        BackSockets = gems.ToList();
                        Back = item;
                        break;
                    case EnumSim.Slot.Finger:
                        if (Finger1 != null)
                        {
                            if (Finger2 != null)
                            {
                                log.Fatal("Finger Item!!!");
                                throw new ArgumentOutOfRangeException();
                            }

                            Finger2Sockets = gems.ToList();
                            Finger2 = item;
                            break;
                        }

                        Finger1Sockets = gems.ToList();
                        Finger1 = item;
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }

                //Gems
                if (i.Item2 != null && i.Item2.Length > item.GetSocketCount())
                {
                    log.Fatal("More gems than can be socketed in " + item.Name);
                    throw new NotSupportedException();
                }

                if (!item.HasMetaSocket() && gems.Any((gem => gem.Socket == EnumSim.Socket.Meta)))
                {
                    log.Fatal("No meta sockets in" + item.Name);
                    throw new NotSupportedException();
                }
            }

            bool twoHand = false;
            Item weapon = Item.GetItem(ItemId.GetId(weapons[0].Item1), weapons[0].Item1);
            if (weapon == null || weapon.Slot != EnumSim.Slot.TwoHand)
            {
                Item offweapon = Item.GetItem(ItemId.GetId(weapons[1].Item1), weapons[1].Item1);
                OffHand = offweapon;
                gems = null;
                if (weapons[1].Item2 == null)
                {
                    gems = new Gem[0];
                }
                else
                {
                    gems = new Gem[weapons[1].Item2.Length];

                    for (int j = 0; j < weapons[1].Item2.Length; j++)
                    {
                        Gem gem = Gem.Gems[GemId.GetId(weapons[1].Item2[j])];
                        gems[j] = gem;
                    }
                }

                OffHandSockets = gems.ToList();
            }

            gems = null;
            if (weapons[0].Item2 == null)
            {
                gems = new Gem[0];
            }
            else
            {
                gems = new Gem[weapons[0].Item2.Length];

                for (int j = 0; j < weapons[0].Item2.Length; j++)
                {
                    Gem gem = Gem.Gems[GemId.GetId(weapons[0].Item2[j])];
                    gems[j] = gem;
                }
            }

            MainHand = weapon;
            MainHandSockets = gems.ToList();


            Item ranged = null;
            if (twoHand)
            {
                ranged = Item.GetItem(ItemId.GetId(weapons[1].Item1), weapons[1].Item1);
                gems = null;
                if (weapons[1].Item2 == null)
                {
                    gems = new Gem[0];
                }
                else
                {
                    gems = new Gem[weapons[1].Item2.Length];

                    for (int j = 0; j < weapons[1].Item2.Length; j++)
                    {
                        Gem gem = Gem.Gems[GemId.GetId(weapons[1].Item2[j])];
                        gems[j] = gem;
                    }
                }
            }
            else
            {
                ranged = Item.GetItem(ItemId.GetId(weapons[2].Item1), weapons[2].Item1);
                gems = null;
                if (weapons[2].Item2 == null)
                {
                    gems = new Gem[0];
                }
                else
                {
                    gems = new Gem[weapons[2].Item2.Length];

                    for (int j = 0; j < weapons[2].Item2.Length; j++)
                    {
                        Gem gem = Gem.Gems[GemId.GetId(weapons[2].Item2[j])];
                        gems[j] = gem;
                    }
                }
            }

            Ranged = ranged;
            RangedSockets = gems.ToList();

            if (MainHand != null && MainHand.Slot != EnumSim.Slot.MainHand && MainHand.Slot != EnumSim.Slot.OneHand &&
                MainHand.Slot != EnumSim.Slot.TwoHand)
            {
                log.Fatal("Main Weapon Error");
                throw new ArgumentOutOfRangeException();
            }

            if (OffHand != null && (MainHand.Slot == EnumSim.Slot.TwoHand ||
                                    (OffHand.Slot != EnumSim.Slot.OffHand && OffHand.Slot != EnumSim.Slot.OneHand)))
            {
                log.Fatal("Off Weapon Error");
                throw new ArgumentOutOfRangeException();
            }

            if (Ranged != null && Ranged.Slot != EnumSim.Slot.Ranged && Ranged.Slot != EnumSim.Slot.Relic &&
                Ranged.Slot != EnumSim.Slot.Thrown)
            {
                log.Fatal("Ranged Error");
                throw new ArgumentOutOfRangeException();
            }

            if (MainHand.Slot == EnumSim.Slot.TwoHand &&
                ((WowClass == EnumSim.WowClass.Mage && MainHand.ItemType != EnumSim.ItemType.Staff) ||
                 (WowClass == EnumSim.WowClass.Priest && MainHand.ItemType != EnumSim.ItemType.Staff) ||
                 (WowClass == EnumSim.WowClass.Warlock && MainHand.ItemType != EnumSim.ItemType.Staff) ||
                 (WowClass == EnumSim.WowClass.Rogue)))
            {
                log.Fatal("Twohand Error");
                throw new ArgumentOutOfRangeException();
            }

            var gear = GetAllGear();
            foreach (var item in gear)
            {
                if (item == null)
                    continue;
                if (item.Classes.Count != 0 && !item.Classes.Contains(WowClass))
                {
                    log.Fatal("Class not match " + item.Name);
                    throw new ArgumentOutOfRangeException();
                }

                if (item.Races.Count != 0 && !item.Races.Contains(Race))
                {
                    log.Fatal("Race not match " + item.Name);
                    throw new ArgumentOutOfRangeException();
                }

                if (item.ItemType == EnumSim.ItemType.Leather && (
                    WowClass == EnumSim.WowClass.Mage ||
                    WowClass == EnumSim.WowClass.Priest ||
                    WowClass == EnumSim.WowClass.Warlock))
                {
                    log.Fatal("Leather not match " + item.Name);
                    throw new ArgumentOutOfRangeException();
                }

                if (item.ItemType == EnumSim.ItemType.Mail && (
                    WowClass == EnumSim.WowClass.Mage ||
                    WowClass == EnumSim.WowClass.Priest ||
                    WowClass == EnumSim.WowClass.Warlock ||
                    WowClass == EnumSim.WowClass.Druid ||
                    WowClass == EnumSim.WowClass.Rogue))
                {
                    log.Fatal("Mail not match " + item.Name);
                    throw new ArgumentOutOfRangeException();
                }

                if (item.ItemType == EnumSim.ItemType.Plate && (
                    WowClass == EnumSim.WowClass.Mage ||
                    WowClass == EnumSim.WowClass.Priest ||
                    WowClass == EnumSim.WowClass.Warlock ||
                    WowClass == EnumSim.WowClass.Druid ||
                    WowClass == EnumSim.WowClass.Rogue ||
                    WowClass == EnumSim.WowClass.Hunter ||
                    WowClass == EnumSim.WowClass.Shaman))
                {
                    log.Fatal("Plate not match " + item.Name);
                    throw new ArgumentOutOfRangeException();
                }

                if (item.ItemType == EnumSim.ItemType.Axe && (
                    WowClass == EnumSim.WowClass.Druid ||
                    WowClass == EnumSim.WowClass.Mage ||
                    WowClass == EnumSim.WowClass.Priest ||
                    WowClass == EnumSim.WowClass.Rogue ||
                    WowClass == EnumSim.WowClass.Warlock
                ))
                {
                    log.Fatal("Axe not match " + item.Name);
                    throw new ArgumentOutOfRangeException();
                }

                if (item.ItemType == EnumSim.ItemType.Bow && (
                    WowClass == EnumSim.WowClass.Druid ||
                    WowClass == EnumSim.WowClass.Mage ||
                    WowClass == EnumSim.WowClass.Priest ||
                    WowClass == EnumSim.WowClass.Paladin ||
                    WowClass == EnumSim.WowClass.Warlock ||
                    WowClass == EnumSim.WowClass.Shaman
                ))
                {
                    log.Fatal("Bow not match " + item.Name);
                    throw new ArgumentOutOfRangeException();
                }

                if (item.ItemType == EnumSim.ItemType.Crossbow && (
                    WowClass == EnumSim.WowClass.Druid ||
                    WowClass == EnumSim.WowClass.Mage ||
                    WowClass == EnumSim.WowClass.Priest ||
                    WowClass == EnumSim.WowClass.Paladin ||
                    WowClass == EnumSim.WowClass.Warlock ||
                    WowClass == EnumSim.WowClass.Shaman
                ))
                {
                    log.Fatal("Crossbow not match " + item.Name);
                    throw new ArgumentOutOfRangeException();
                }

                if (item.ItemType == EnumSim.ItemType.Dagger && (
                    WowClass == EnumSim.WowClass.Druid ||
                    WowClass == EnumSim.WowClass.Mage ||
                    WowClass == EnumSim.WowClass.Priest ||
                    WowClass == EnumSim.WowClass.Warlock ||
                    WowClass == EnumSim.WowClass.Shaman
                ))
                {
                    log.Fatal("Dagger not match " + item.Name);
                    throw new ArgumentOutOfRangeException();
                }

                if (item.ItemType == EnumSim.ItemType.Gun && (
                    WowClass == EnumSim.WowClass.Druid ||
                    WowClass == EnumSim.WowClass.Mage ||
                    WowClass == EnumSim.WowClass.Priest ||
                    WowClass == EnumSim.WowClass.Paladin ||
                    WowClass == EnumSim.WowClass.Warlock ||
                    WowClass == EnumSim.WowClass.Shaman
                ))
                {
                    log.Fatal("Gun not match " + item.Name);
                    throw new ArgumentOutOfRangeException();
                }

                if (item.ItemType == EnumSim.ItemType.Idol && (
                    WowClass == EnumSim.WowClass.Hunter ||
                    WowClass == EnumSim.WowClass.Mage ||
                    WowClass == EnumSim.WowClass.Priest ||
                    WowClass == EnumSim.WowClass.Paladin ||
                    WowClass == EnumSim.WowClass.Warlock ||
                    WowClass == EnumSim.WowClass.Rogue ||
                    WowClass == EnumSim.WowClass.Shaman ||
                    WowClass == EnumSim.WowClass.Warrior
                ))
                {
                    log.Fatal("Idol not match " + item.Name);
                    throw new ArgumentOutOfRangeException();
                }

                if (item.ItemType == EnumSim.ItemType.Libram && (
                    WowClass == EnumSim.WowClass.Hunter ||
                    WowClass == EnumSim.WowClass.Mage ||
                    WowClass == EnumSim.WowClass.Priest ||
                    WowClass == EnumSim.WowClass.Druid ||
                    WowClass == EnumSim.WowClass.Warlock ||
                    WowClass == EnumSim.WowClass.Rogue ||
                    WowClass == EnumSim.WowClass.Shaman ||
                    WowClass == EnumSim.WowClass.Warrior
                ))
                {
                    log.Fatal("Libram not match " + item.Name);
                    throw new ArgumentOutOfRangeException();
                }

                if (item.ItemType == EnumSim.ItemType.Mace && (
                    WowClass == EnumSim.WowClass.Mage ||
                    WowClass == EnumSim.WowClass.Warlock
                ))
                {
                    log.Fatal("Mace not match " + item.Name);
                    throw new ArgumentOutOfRangeException();
                }

                if (item.ItemType == EnumSim.ItemType.Polearm && (
                    WowClass == EnumSim.WowClass.Druid ||
                    WowClass == EnumSim.WowClass.Mage ||
                    WowClass == EnumSim.WowClass.Warlock ||
                    WowClass == EnumSim.WowClass.Priest ||
                    WowClass == EnumSim.WowClass.Rogue ||
                    WowClass == EnumSim.WowClass.Shaman
                ))
                {
                    log.Fatal("Polearm not match " + item.Name);
                    throw new ArgumentOutOfRangeException();
                }

                if (item.ItemType == EnumSim.ItemType.Shield && (
                    WowClass == EnumSim.WowClass.Druid ||
                    WowClass == EnumSim.WowClass.Mage ||
                    WowClass == EnumSim.WowClass.Warlock ||
                    WowClass == EnumSim.WowClass.Hunter ||
                    WowClass == EnumSim.WowClass.Priest ||
                    WowClass == EnumSim.WowClass.Rogue
                ))
                {
                    log.Fatal("Shield not match " + item.Name);
                    throw new ArgumentOutOfRangeException();
                }

                if (item.ItemType == EnumSim.ItemType.Staff && (
                    WowClass == EnumSim.WowClass.Rogue
                ))
                {
                    log.Fatal("Staff not match " + item.Name);
                    throw new ArgumentOutOfRangeException();
                }

                if (item.ItemType == EnumSim.ItemType.Sword && (
                    WowClass == EnumSim.WowClass.Druid ||
                    WowClass == EnumSim.WowClass.Priest
                ))
                {
                    log.Fatal("Sword not match " + item.Name);
                    throw new ArgumentOutOfRangeException();
                }

                if (item.ItemType == EnumSim.ItemType.Thrown && (
                    WowClass == EnumSim.WowClass.Druid ||
                    WowClass == EnumSim.WowClass.Mage ||
                    WowClass == EnumSim.WowClass.Priest ||
                    WowClass == EnumSim.WowClass.Paladin ||
                    WowClass == EnumSim.WowClass.Warlock ||
                    WowClass == EnumSim.WowClass.Shaman
                ))
                {
                    log.Fatal("Thrown not match " + item.Name);
                    throw new ArgumentOutOfRangeException();
                }

                if (item.ItemType == EnumSim.ItemType.Totem && (
                    WowClass == EnumSim.WowClass.Hunter ||
                    WowClass == EnumSim.WowClass.Mage ||
                    WowClass == EnumSim.WowClass.Priest ||
                    WowClass == EnumSim.WowClass.Shaman ||
                    WowClass == EnumSim.WowClass.Warlock ||
                    WowClass == EnumSim.WowClass.Rogue ||
                    WowClass == EnumSim.WowClass.Shaman ||
                    WowClass == EnumSim.WowClass.Warrior
                ))
                {
                    log.Fatal("Totem not match " + item.Name);
                    throw new ArgumentOutOfRangeException();
                }

                if (item.ItemType == EnumSim.ItemType.Wand && (
                    WowClass == EnumSim.WowClass.Druid ||
                    WowClass == EnumSim.WowClass.Hunter ||
                    WowClass == EnumSim.WowClass.Rogue ||
                    WowClass == EnumSim.WowClass.Paladin ||
                    WowClass == EnumSim.WowClass.Warrior ||
                    WowClass == EnumSim.WowClass.Shaman
                ))
                {
                    log.Fatal("Wand not match " + item.Name);
                    throw new ArgumentOutOfRangeException();
                }

                if (item.ItemType == EnumSim.ItemType.FistWeapon && (
                    WowClass == EnumSim.WowClass.Warrior ||
                    WowClass == EnumSim.WowClass.Warlock ||
                    WowClass == EnumSim.WowClass.Mage ||
                    WowClass == EnumSim.WowClass.Priest
                ))
                {
                    log.Fatal("FistWeapon not match " + item.Name);
                    throw new ArgumentOutOfRangeException();
                }
            }
        }

        public void SetAllStats()
        {
            SetBaseStats();
            SetGearStats();
            SetGemsStats();
        }

        private void SetBaseStats()
        {
            int strength = 0;
            int agility = 0;
            int stamina = 0;
            int intellect = 0;
            int spirit = 0;

            switch (WowClass)
            {
                case EnumSim.WowClass.Druid:
                    strength = 76;
                    agility = 70;
                    stamina = 83;
                    intellect = 120;
                    spirit = 133;
                    baseSpellCritChance = 1.85f;
                    baseDodgeChance = -1.87f;
                    IntellectSpellCritRatio = 80;
                    AgilityAttackPowerRatio = 1;
                    AgilityRangedAttackPowerRatio = 0;
                    AgilityDodgeRatio = 14.7059f;
                    AgilityCritRatio = 25;
                    StrengthAttackPowerRatio = 2;
                    break;
                case EnumSim.WowClass.Hunter:
                    strength = 64;
                    agility = 151;
                    stamina = 108;
                    intellect = 77;
                    spirit = 83;
                    baseSpellCritChance = 3.6f;
                    baseDodgeChance = -5.45f;
                    IntellectSpellCritRatio = 80;
                    AgilityAttackPowerRatio = 0;
                    AgilityRangedAttackPowerRatio = 2;
                    AgilityDodgeRatio = 26.5f;
                    AgilityCritRatio = 40;
                    StrengthAttackPowerRatio = 1;
                    break;
                case EnumSim.WowClass.Mage:
                    strength = 33;
                    agility = 39;
                    stamina = 51;
                    intellect = 151;
                    spirit = 145;
                    baseSpellCritChance = 0.91f;
                    baseDodgeChance = 3.45f;
                    IntellectSpellCritRatio = 80;
                    AgilityAttackPowerRatio = 0;
                    AgilityRangedAttackPowerRatio = 0;
                    AgilityDodgeRatio = 25;
                    AgilityCritRatio = 25;
                    StrengthAttackPowerRatio = 2;
                    break;
                case EnumSim.WowClass.Paladin:
                    strength = 126;
                    agility = 77;
                    stamina = 120;
                    intellect = 83;
                    spirit = 89;
                    baseSpellCritChance = 3.336f;
                    baseDodgeChance = 0.65f;
                    IntellectSpellCritRatio = 80.05f;
                    AgilityAttackPowerRatio = 0;
                    AgilityRangedAttackPowerRatio = 0;
                    AgilityDodgeRatio = 25;
                    AgilityCritRatio = 25;
                    StrengthAttackPowerRatio = 2;
                    break;
                case EnumSim.WowClass.Priest:
                    strength = 39;
                    agility = 45;
                    stamina = 58;
                    intellect = 145;
                    spirit = 151;
                    baseSpellCritChance = 1.24f;
                    baseDodgeChance = 3.18f;
                    IntellectSpellCritRatio = 80;
                    AgilityAttackPowerRatio = 0;
                    AgilityRangedAttackPowerRatio = 0;
                    AgilityDodgeRatio = 25;
                    AgilityCritRatio = 25;
                    StrengthAttackPowerRatio = 2;
                    break;
                case EnumSim.WowClass.Rogue:
                    strength = 95;
                    agility = 181;
                    stamina = 89;
                    intellect = 39;
                    spirit = 58;
                    baseSpellCritChance = 0;
                    baseDodgeChance = -0.59f;
                    IntellectSpellCritRatio = 80;
                    AgilityAttackPowerRatio = 1;
                    AgilityRangedAttackPowerRatio = 2;
                    AgilityDodgeRatio = 25;
                    AgilityCritRatio = 40;
                    StrengthAttackPowerRatio = 1;
                    break;
                case EnumSim.WowClass.Shaman:
                    strength = 102;
                    agility = 64;
                    stamina = 114;
                    intellect = 108;
                    spirit = 120;
                    baseSpellCritChance = 2.2f;
                    baseDodgeChance = 1.67f;
                    IntellectSpellCritRatio = 80;
                    AgilityAttackPowerRatio = 0;
                    AgilityRangedAttackPowerRatio = 0;
                    AgilityDodgeRatio = 25;
                    AgilityCritRatio = 25;
                    StrengthAttackPowerRatio = 2;
                    break;
                case EnumSim.WowClass.Warrior:
                    strength = 145;
                    agility = 96;
                    stamina = 133;
                    intellect = 33;
                    spirit = 51;
                    baseSpellCritChance = 0;
                    baseDodgeChance = 0.75f;
                    IntellectSpellCritRatio = 80;
                    AgilityAttackPowerRatio = 0;
                    AgilityRangedAttackPowerRatio = 2;
                    AgilityDodgeRatio = 25;
                    AgilityCritRatio = 33;
                    StrengthAttackPowerRatio = 2;
                    break;
                case EnumSim.WowClass.Warlock:
                    strength = 51;
                    agility = 58;
                    stamina = 76;
                    intellect = 133;
                    spirit = 131;
                    baseSpellCritChance = 1.701f;
                    baseDodgeChance = 2.03f;
                    IntellectSpellCritRatio = 81.92f;
                    AgilityAttackPowerRatio = 0;
                    AgilityRangedAttackPowerRatio = 0;
                    AgilityDodgeRatio = 25;
                    AgilityCritRatio = 25;
                    StrengthAttackPowerRatio = 2;
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            switch (Race)
            {
                case EnumSim.RaceType.BloodElf:
                    strength += -3;
                    agility += 2;
                    stamina += -1;
                    intellect += 4;
                    spirit += -1;
                    break;
                case EnumSim.RaceType.Draenei:
                    strength += 1;
                    agility += -3;
                    stamina += -1;
                    intellect += 1;
                    spirit += 3;
                    break;
                case EnumSim.RaceType.Human:
                    strength += 0;
                    agility += 0;
                    stamina += 0;
                    intellect += 0;
                    spirit += 1;
                    break;
                case EnumSim.RaceType.NightElf:
                    strength += -3;
                    agility += 5;
                    stamina += -1;
                    intellect += 0;
                    spirit += 0;
                    break;
                case EnumSim.RaceType.Orc:
                    strength += 3;
                    agility += -3;
                    stamina += 2;
                    intellect += -3;
                    spirit += 3;
                    break;
                case EnumSim.RaceType.Dwarf:
                    strength += 2;
                    agility += -4;
                    stamina += 3;
                    intellect += -1;
                    spirit += -1;
                    break;
                case EnumSim.RaceType.Troll:
                    strength += 1;
                    agility += 2;
                    stamina += 1;
                    intellect += -4;
                    spirit += 3;
                    break;
                case EnumSim.RaceType.Gnome:
                    strength += -5;
                    agility += 3;
                    stamina += -1;
                    intellect += 4;
                    spirit += 0;
                    break;
                case EnumSim.RaceType.Undead:
                    strength += -1;
                    agility += -2;
                    stamina += 1;
                    intellect += -2;
                    spirit += 5;
                    break;
                case EnumSim.RaceType.Tauren:
                    strength += 5;
                    agility += -5;
                    stamina += 2;
                    intellect += -5;
                    spirit += 2;
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            Stregth = strength;
            Agility = agility;
            Stamina = stamina;
            Intellect = intellect;
            Spirit = spirit;
            DefenseSpellhit = 4f;
        }

        private void SetGearStats()
        {
            foreach (var item in GetAllGear())
            {
                foreach (var stat in item.Stats)
                {
                    ProcessStat(stat);
                }
            }
        }


        private void SetGemsStats()
        {
            foreach (var gem in HeadSockets)
            {
                foreach (var stat in gem.Stats)
                {
                    ProcessStat(stat);
                }
            }

            if (Head.HasSocketBonus(HeadSockets))
            {
                ProcessStat(Head.SocketBonus);
            }

            foreach (var gem in NeckSockets)
            {
                foreach (var stat in gem.Stats)
                {
                    ProcessStat(stat);
                }
            }

            if (Neck.HasSocketBonus(NeckSockets))
            {
                ProcessStat(Neck.SocketBonus);
            }

            foreach (var gem in ShoulderSockets)
            {
                foreach (var stat in gem.Stats)
                {
                    ProcessStat(stat);
                }
            }

            if (Shoulder.HasSocketBonus(ShoulderSockets))
            {
                ProcessStat(Shoulder.SocketBonus);
            }

            foreach (var gem in BackSockets)
            {
                foreach (var stat in gem.Stats)
                {
                    ProcessStat(stat);
                }
            }

            if (Back.HasSocketBonus(BackSockets))
            {
                ProcessStat(Back.SocketBonus);
            }

            foreach (var gem in ChestSockets)
            {
                foreach (var stat in gem.Stats)
                {
                    ProcessStat(stat);
                }
            }

            if (Chest.HasSocketBonus(ChestSockets))
            {
                ProcessStat(Chest.SocketBonus);
            }

            foreach (var gem in WristSockets)
            {
                foreach (var stat in gem.Stats)
                {
                    ProcessStat(stat);
                }
            }

            if (Wrist.HasSocketBonus(WristSockets))
            {
                ProcessStat(Wrist.SocketBonus);
            }

            foreach (var gem in HandsSockets)
            {
                foreach (var stat in gem.Stats)
                {
                    ProcessStat(stat);
                }
            }

            if (Hands.HasSocketBonus(HandsSockets))
            {
                ProcessStat(Hands.SocketBonus);
            }

            foreach (var gem in WaistSockets)
            {
                foreach (var stat in gem.Stats)
                {
                    ProcessStat(stat);
                }
            }

            if (Waist.HasSocketBonus(WaistSockets))
            {
                ProcessStat(Waist.SocketBonus);
            }

            foreach (var gem in LegsSockets)
            {
                foreach (var stat in gem.Stats)
                {
                    ProcessStat(stat);
                }
            }

            if (Legs.HasSocketBonus(LegsSockets))
            {
                ProcessStat(Legs.SocketBonus);
            }

            foreach (var gem in FeetSockets)
            {
                foreach (var stat in gem.Stats)
                {
                    ProcessStat(stat);
                }
            }

            if (Feet.HasSocketBonus(FeetSockets))
            {
                ProcessStat(Feet.SocketBonus);
            }

            foreach (var gem in Finger1Sockets)
            {
                foreach (var stat in gem.Stats)
                {
                    ProcessStat(stat);
                }
            }

            if (Finger1.HasSocketBonus(Finger1Sockets))
            {
                ProcessStat(Finger1.SocketBonus);
            }

            foreach (var gem in Finger2Sockets)
            {
                foreach (var stat in gem.Stats)
                {
                    ProcessStat(stat);
                }
            }

            if (Finger2.HasSocketBonus(Finger2Sockets))
            {
                ProcessStat(Finger2.SocketBonus);
            }

            foreach (var gem in Trinket1Sockets)
            {
                foreach (var stat in gem.Stats)
                {
                    ProcessStat(stat);
                }
            }

            if (Trinket1.HasSocketBonus(Trinket1Sockets))
            {
                ProcessStat(Trinket1.SocketBonus);
            }

            foreach (var gem in Trinket2Sockets)
            {
                foreach (var stat in gem.Stats)
                {
                    ProcessStat(stat);
                }
            }

            if (Trinket2.HasSocketBonus(Trinket2Sockets))
            {
                ProcessStat(Trinket2.SocketBonus);
            }

            foreach (var gem in MainHandSockets)
            {
                foreach (var stat in gem.Stats)
                {
                    ProcessStat(stat);
                }
            }

            if (MainHand.HasSocketBonus(MainHandSockets))
            {
                ProcessStat(MainHand.SocketBonus);
            }

            foreach (var gem in OffHandSockets)
            {
                foreach (var stat in gem.Stats)
                {
                    ProcessStat(stat);
                }
            }

            if (OffHand.HasSocketBonus(OffHandSockets))
            {
                ProcessStat(OffHand.SocketBonus);
            }

            foreach (var gem in RangedSockets)
            {
                foreach (var stat in gem.Stats)
                {
                    ProcessStat(stat);
                }
            }

            if (Ranged.HasSocketBonus(RangedSockets))
            {
                ProcessStat(Ranged.SocketBonus);
            }
        }

        private void ProcessStat(Stat stat)
        {
            switch (stat.Key)
            {
                case EnumSim.StatEnum.SpellCriticalStrikeRating:
                    SpellCritRating += stat.Value;
                    break;
                case EnumSim.StatEnum.SpellDamageandHealing:
                    SpellDamage += stat.Value;
                    HealingPower += stat.Value;
                    break;
                case EnumSim.StatEnum.Stamina:
                    Stamina += stat.Value;
                    break;
                case EnumSim.StatEnum.Manaper5sec:
                    ManaP5 += stat.Value;
                    break;
                case EnumSim.StatEnum.SpellHitRating:
                    SpellHitRating += stat.Value;
                    break;
                case EnumSim.StatEnum.ParryRating:
                    SpellCritRating += stat.Value;
                    break;
                case EnumSim.StatEnum.Strength:
                    Stregth += stat.Value;
                    break;
                case EnumSim.StatEnum.Agility:
                    Agility += stat.Value;
                    break;
                case EnumSim.StatEnum.Spirit:
                    Spirit += stat.Value;
                    break;
                case EnumSim.StatEnum.Intellect:
                    Intellect += stat.Value;
                    break;
                case EnumSim.StatEnum.CriticalStrikeRating:
                    CritRating += stat.Value;
                    break;
                case EnumSim.StatEnum.HitRating:
                    HitRating += stat.Value;
                    break;
                case EnumSim.StatEnum.HealingThirdSpellDamage:
                    HealingPower += stat.Value;
                    SpellDamage += (int) Math.Ceiling((double) stat.Value / 3);
                    break;
                case EnumSim.StatEnum.ResilienceRating:
                    ResilienceRating += stat.Value;
                    break;
                case EnumSim.StatEnum.AttackPower:
                    AttackPower += stat.Value;
                    break;
                case EnumSim.StatEnum.DefenseRating:
                    DefenseRating += stat.Value;
                    break;
                case EnumSim.StatEnum.DodgeRating:
                    DodgeRating += stat.Value;
                    break;
                case EnumSim.StatEnum.BlockRating:
                    BlockRating += stat.Value;
                    break;
                case EnumSim.StatEnum.BlockValue:
                    BlockValue += stat.Value;
                    break;
                case EnumSim.StatEnum.SpellDamage:
                    SpellDamage += stat.Value;
                    break;
                case EnumSim.StatEnum.HasteRating:
                    HasteRating += stat.Value;
                    break;
                case EnumSim.StatEnum.ExpertiseRating:
                    ExpertiseRating += stat.Value;
                    break;
                case EnumSim.StatEnum.Armor:
                    DefenseArmor += stat.Value;
                    break;
                case EnumSim.StatEnum.ArcaneSpellDamage:
                    ArcaneDamage += stat.Value;
                    break;
                case EnumSim.StatEnum.ShadowSpellDamage:
                    ShadowDamage += stat.Value;
                    break;
                case EnumSim.StatEnum.NatureSpellDamage:
                    NatureDamage += stat.Value;
                    break;
                case EnumSim.StatEnum.FireSpellDamage:
                    FireDamage += stat.Value;
                    break;
                case EnumSim.StatEnum.FrostSpellDamage:
                    FrostDamage += stat.Value;
                    break;
                case EnumSim.StatEnum.ShadowResistance:
                    ResistanceShadow += stat.Value;
                    break;
                case EnumSim.StatEnum.NatureResistance:
                    ResistanceNature += stat.Value;
                    break;
                case EnumSim.StatEnum.FireResistance:
                    ResistanceFire += stat.Value;
                    break;
                case EnumSim.StatEnum.FrostResistance:
                    ResistanceFrost += stat.Value;
                    break;
                case EnumSim.StatEnum.ArcaneResistance:
                    ResistanceArcane += stat.Value;
                    break;
                case EnumSim.StatEnum.Block:
                    throw new NotImplementedException();
                    break;
                case EnumSim.StatEnum.Healthper5sec:
                    HealthP5 += stat.Value;
                    break;
                case EnumSim.StatEnum.HolySpellDamage:
                    HolyDamage += stat.Value;
                    break;
                case EnumSim.StatEnum.RangedAttackPower:
                    RangedAttackPower += stat.Value;
                    break;
                case EnumSim.StatEnum.SpellHasteRating:
                    SpellHaste += stat.Value;
                    break;
                case EnumSim.StatEnum.IgnoreArmor:
                    IgnoreArmor += stat.Value;
                    break;
                case EnumSim.StatEnum.SpellPenetration:
                    SpellPenetration += stat.Value;
                    break;
                case EnumSim.StatEnum.WeaponDamage:
                    throw new NotImplementedException();
                    break;
                case EnumSim.StatEnum.AllResistance:
                    ResistanceArcane += stat.Value;
                    ResistanceFire += stat.Value;
                    ResistanceFrost += stat.Value;
                    ResistanceNature += stat.Value;
                    ResistanceShadow += stat.Value;
                    break;
                case EnumSim.StatEnum.SwordSkillRating:
                    throw new NotImplementedException();
                    break;
                case EnumSim.StatEnum.Fishing:
                    throw new NotImplementedException();
                    break;
                case EnumSim.StatEnum.MovementSpeed:
                    MovementSpeed += stat.Value;
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
    }
}