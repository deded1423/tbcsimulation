﻿namespace TBCsimulation.Simulation
{
    public static class ParameterSim
    {
        //0-1000 General
        public const int Iterations = 0;
        public const int TimeofFight = 1;
        public const int GCD = 2;
        public const int RotationString = 3;
        public const int InitialPet = 10;

        //1000-2000 Clases
        public const int NumberofAfflictionSpells = 1000;
        public const int DemonicSacrificePet = 1001;
    }
}