﻿using System;

namespace TBCsimulation.Simulation
{
    public static class RandomSim
    {
        private static readonly Random random = new();

        public static int GetInt(int min, int max)
        {
            return random.Next(min, max + 1);
        }

        public static float GetRNG()
        {
            return (float) random.NextDouble() * 100f;
        }

        /// <summary>
        /// Returns bool if the chance has hit (Over 100)
        /// </summary>
        /// <param name="chance"> Chance over 100</param>
        /// <returns></returns>
        public static bool CheckChance(float chance)
        {
            return (float) random.NextDouble() * 100f < chance;
        }
    }
}