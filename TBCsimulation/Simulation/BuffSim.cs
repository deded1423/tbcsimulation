﻿using System;

namespace TBCsimulation.Simulation
{
    public class BuffSim
    {
        public int DurationRemaining;
        public int DurationTotal;
        public int Id;
        public string Name;
        public int Stacks = 0;
        public Action<FightSim> TickActionDelegate = null;
        public int TickInterval = 0;

        public BuffSim(string Name, int Id)
        {
            this.Name = Name;
            this.Id = Id;
        }
    }
}