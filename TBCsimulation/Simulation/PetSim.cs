﻿namespace TBCsimulation.Simulation
{
    public class PetSim : IPetSim
    {
        public const int Warlock_Felguard = 17252;
        public const int Warlock_Felhunter = 417;
        public const int Warlock_Imp = 416;
        public const int Warlock_Succubus = 1863;
        public const int Warlock_Voidwalker = 1860;

        public int Id;
        public string Name;

        public PetSim()
        {
            Name = Name.Replace("", "");
        }
    }
}