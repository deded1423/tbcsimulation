﻿using TBCsimulation.Data.Utils;

namespace TBCsimulation.Simulation
{
    public interface IFightSim
    {
        public void Simulate();
        public void PrintResults();

        public int Heal(int idSource, int health);
        public int HealthLost(int idSource, int health);
        public int ManaSpend(int idSource, int mana);
        public int ManaGained(int idSource, int mana);

        public int Damage(EnumSim.DamageType damageType, EnumSim.SchoolType schoolType, int idSource, int damage,
            float multiplier,
            bool periodic);

        public int Damage(EnumSim.DamageType damageType, EnumSim.SchoolType schoolType, int idSource, int damage,
            float multiplier,
            bool periodic, bool canMiss,
            float hitChance);

        public int Damage(EnumSim.DamageType damageType, EnumSim.SchoolType schoolType, int idSource, int damage,
            float multiplier,
            bool periodic, bool canMiss,
            float hitChance, bool canCrit, float critChance, float critDamage);

        public bool Debuff(BuffSim buff, EnumSim.SchoolType schoolType, int idSource, bool isBinary);

        public bool Debuff(BuffSim buff, EnumSim.SchoolType schoolType, int idSource, bool isBinary, bool Ticks);

        public bool Debuff(BuffSim buff, EnumSim.SchoolType schoolType, int idSource, bool isBinary, bool Ticks,
            bool canMiss,
            float hitChance);

        public void RemoveDebuff(BuffSim buff);
        public void RemoveDebuff(string Name);
        public void RemoveDebuff(int Id);

        public bool HasDebuff(BuffSim buff);
        public bool HasDebuff(string Name);
        public bool HasDebuff(int Id);

        public bool Buff(BuffSim buff, EnumSim.SchoolType schoolType, int idSource, bool Ticks);

        public void RemoveBuff(BuffSim buff);
        public void RemoveBuff(string Name);
        public void RemoveBuff(int Id);

        public bool HasBuff(BuffSim buff);
        public bool HasBuff(string Name);
        public bool HasBuff(int Id);
    }
}