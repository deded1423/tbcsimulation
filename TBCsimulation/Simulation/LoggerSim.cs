﻿using System.Collections.Generic;
using System.IO;
using TBCsimulation.Data.SpellsInfo;
using TBCsimulation.Data.Utils;
using TBCsimulation.Report;

namespace TBCsimulation.Simulation
{
    public class LoggerSim
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public DataReport DataReport;

        public bool Debug = true;

        private StreamWriter writer;

        public LoggerSim(DataReport dataReport)
        {
            DataReport = dataReport;
        }

        public void Initialize()
        {
            if (writer != null)
            {
                log.Error("Trying to initalize 2 times logger");
                throw new IOException();
            }

            writer = new StreamWriter("Fight.fgh");
        }

        public void Close()
        {
            writer.Close();
        }

        public void Cast(int time, Spell spell)
        {
            DataReport.AddEvent(new EventReport(time, TypeReport.Cast, new List<object>()
            {
                spell
            }));

            writer.WriteLine(time + "\tCast " + spell.Name + " (" + spell.Id + ")");
            if (Debug)
                log.Debug(time + "\tCast " + spell.Name + " (" + spell.Id + ")");
        }

        public void ProcessEvent(int time, HeapEventSim heapEvent)
        {
            DataReport.AddEvent(new EventReport(time, TypeReport.ProcessEvent, new List<object>()
            {
                heapEvent
            }));

            writer.WriteLine(time + "\tProcessEvent " + heapEvent.Name + " - " + heapEvent.triggerRotation);
            if (Debug)
                log.Debug(time + "\tProcessEvent " + heapEvent.Name + " - " + heapEvent.triggerRotation);
        }

        public void TriggerDebuff(int time, BuffSim buff)
        {
            DataReport.AddEvent(new EventReport(time, TypeReport.TriggerDebuff, new List<object>()
            {
                buff
            }));

            writer.WriteLine(time + "\tTriggerBuff " + buff.Name);
            if (Debug)
                log.Debug(time + "\tTriggerBuff " + buff.Name);
        }

        public void TickBuff(int time, BuffSim buff)
        {
            DataReport.AddEvent(new EventReport(time, TypeReport.TickBuff, new List<object>()
            {
                buff
            }));

            writer.WriteLine(time + "\tTickBuff " + buff.Name);
            if (Debug)
                log.Debug(time + "\tTickBuff " + buff.Name);
        }

        public void TickDebuff(int time, BuffSim buff)
        {
            DataReport.AddEvent(new EventReport(time, TypeReport.TickDebuff, new List<object>()
            {
                buff
            }));

            writer.WriteLine(time + "\tTickDebuff " + buff.Name + " - Left: " + buff.DurationRemaining);
            if (Debug)
                log.Debug(time + "\tTickDebuff " + buff.Name + " - Left: " + buff.DurationRemaining);
        }

        public void Buff(int time, BuffSim buff)
        {
            DataReport.AddEvent(new EventReport(time, TypeReport.Buff, new List<object>()
            {
                buff
            }));

            writer.WriteLine(time + "\tBuff " + buff.Name + " (" + buff.Id + ")");
            if (Debug)
                log.Debug(time + "\tBuff " + buff.Name + " (" + buff.Id + ")");
        }

        public void Debuff(int time, BuffSim buff)
        {
            DataReport.AddEvent(new EventReport(time, TypeReport.Debuff, new List<object>()
            {
                buff
            }));

            writer.WriteLine(time + "\tDebuff " + buff.Name + " (" + buff.Id + ")");
            if (Debug)
                log.Debug(time + "\tDebuff " + buff.Name + " (" + buff.Id + ")");
        }

        public void FadeBuff(int time, BuffSim buff)
        {
            DataReport.AddEvent(new EventReport(time, TypeReport.FadeBuff, new List<object>()
            {
                buff
            }));

            writer.WriteLine(time + "\tFadeBuff " + buff.Name + " (" + buff.Id + ")");
            if (Debug)
                log.Debug(time + "\tFadeBuff " + buff.Name + " (" + buff.Id + ")");
        }

        public void FadeDebuff(int time, BuffSim buff)
        {
            DataReport.AddEvent(new EventReport(time, TypeReport.FadeDebuff, new List<object>()
            {
                buff
            }));

            writer.WriteLine(time + "\tFadeDebuff " + buff.Name);
            if (Debug)
                log.Debug(time + "\tFadeDebuff " + buff.Name);
        }

        public void TriggerDamage(int time, EnumSim.DamageType damageType, EnumSim.SchoolType schoolType, int idSource,
            int initialdamage,
            bool canMiss, float hitChance, bool canCrit, float critChance, float critDamage, int damage, bool crit)
        {
            DataReport.AddEvent(new EventReport(time, TypeReport.TriggerDamage, new List<object>()
            {
                damageType,
                schoolType,
                idSource,
                initialdamage,
                canMiss,
                hitChance,
                canCrit,
                critChance,
                critDamage,
                damage,
                crit
            }));

            writer.WriteLine(time + "\tTriggerDamage " + damageType + " " + schoolType + " (" + idSource + ") " +
                             initialdamage +
                             " Miss: " + canMiss + " " + hitChance + " Crit: " + canCrit + " " + critChance + " " +
                             critDamage + " Real: " + damage + " " + crit);
            if (Debug)
                log.Debug(time + "\tTriggerDamage " + damageType + " " + schoolType + " (" + idSource + ") " +
                          initialdamage +
                          " Miss: " + canMiss + " " + hitChance + " Crit: " + canCrit + " " + critChance + " " +
                          critDamage + " Real: " + damage + " " + crit);
        }

        public void MissSpell(int time, int idSource, float missChance)
        {
            DataReport.AddEvent(new EventReport(time, TypeReport.MissSpell, new List<object>()
            {
                idSource,
                missChance
            }));

            writer.WriteLine(time + "\tMissSpell (" + idSource + ") " + missChance);
            if (Debug)
                log.Debug(time + "\tMissSpell (" + idSource + ") " + missChance);
        }

        public void ResistSpell(int time, int idSource, float resistChance)
        {
            DataReport.AddEvent(new EventReport(time, TypeReport.ResistSpell, new List<object>()
            {
                idSource,
                resistChance
            }));

            writer.WriteLine(time + "\tResistSpell (" + idSource + ") " + resistChance);
            if (Debug)
                log.Debug(time + "\tResistSpell (" + idSource + ") " + resistChance);
        }

        public void Damage(int time, EnumSim.DamageType damageType, EnumSim.SchoolType schoolType, int idSource,
            int damage,
            bool isCrit)
        {
            DataReport.AddEvent(new EventReport(time, TypeReport.Damage, new List<object>()
            {
                damageType,
                schoolType,
                idSource,
                damage,
                isCrit
            }));

            writer.WriteLine(time + "\tDamage " + damageType + " " + schoolType + " (" + idSource + ") " + damage +
                             " Crit: " +
                             isCrit);
            if (Debug)
                log.Debug(time + "\tDamage " + damageType + " " + schoolType + " (" + idSource + ") " + damage +
                          " Crit: " +
                          isCrit);
        }

        public void Heal(int time, int idSource, int damage, bool isCrit)
        {
            DataReport.AddEvent(new EventReport(time, TypeReport.Heal, new List<object>()
            {
                idSource,
                damage,
                isCrit
            }));

            writer.WriteLine(time + "\tHeal (" + idSource + ") " + damage + " Crit: " + isCrit);
            if (Debug)
                log.Debug(time + "\tHeal (" + idSource + ") " + damage + " Crit: " + isCrit);
        }

        public void HealthLost(int time, int idSource, int damage)
        {
            DataReport.AddEvent(new EventReport(time, TypeReport.HealthLost, new List<object>()
            {
                idSource,
                damage
            }));

            writer.WriteLine(time + "\tHealthLost (" + idSource + ") " + damage);
            if (Debug)
                log.Debug(time + "\tHealthLost (" + idSource + ") " + damage);
        }

        public void ManaGain(int time, int idSource, int manaGained, int mana)
        {
            DataReport.AddEvent(new EventReport(time, TypeReport.ManaGain, new List<object>()
            {
                idSource,
                manaGained,
                mana
            }));

            writer.WriteLine(time + "\tManaGain (" + idSource + ") " + manaGained + " - " + mana);
            if (Debug)
                log.Debug(time + "\tManaGain (" + idSource + ") " + manaGained + " - " + mana);
        }

        public void ManaSpend(int time, int idSource, int manaSpend, int mana)
        {
            DataReport.AddEvent(new EventReport(time, TypeReport.ManaSpend, new List<object>()
            {
                idSource,
                manaSpend,
                mana
            }));

            writer.WriteLine(time + "\tManaSpend (" + idSource + ") " + manaSpend + " - " + mana);
            if (Debug)
                log.Debug(time + "\tManaSpend (" + idSource + ") " + manaSpend + " - " + mana);
        }

        public void WaitForResource(int time, int idSource, int mana)
        {
            DataReport.AddEvent(new EventReport(time, TypeReport.WaitForResource, new List<object>()
            {
                idSource,
                mana
            }));

            writer.WriteLine(time + "\tWaitForResource (" + idSource + ") " + mana);
            if (Debug)
                log.Debug(time + "\tWaitForResource (" + idSource + ") " + mana);
        }

        public void TickChannel(int time, Spell spell)
        {
            DataReport.AddEvent(new EventReport(time, TypeReport.TickChannel, new List<object>()
            {
                spell
            }));

            writer.WriteLine(time + "\tTickChannel " + spell.Name);
            if (Debug)
                log.Debug(time + "\tTickChannel " + spell.Name);
        }

        public void EndChannel(int time, Spell spell)
        {
            DataReport.AddEvent(new EventReport(time, TypeReport.EndChannel, new List<object>()
            {
                spell
            }));

            writer.WriteLine(time + "\tEndChannel " + spell.Name);
            if (Debug)
                log.Debug(time + "\tEndChannel " + spell.Name);
        }
    }
}