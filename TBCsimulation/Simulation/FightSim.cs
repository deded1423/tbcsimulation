﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Priority_Queue;
using TBCsimulation.Data.SpellsInfo;
using TBCsimulation.Data.TalentsInfo;
using TBCsimulation.Data.Utils;
using TBCsimulation.Data.Warlock.Pet;

namespace TBCsimulation.Simulation
{
    public class FightSim : IFightSim
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);

        public BossSim Boss;


        public List<BuffSim> Buffs = new();
        public Dictionary<int, int> CDs = new();
        public List<BuffSim> Debuffs = new();
        public bool EndFight = false;

        public SimplePriorityQueue<HeapEventSim> Heap = new();

        public LoggerSim LoggerSim = new(new Report.DataReport());
        public Dictionary<int, object> Params;
        public PetSim Pet;

        public PlayerSim Player;

        public List<RotationSim> Rotation = new();

        public int Time;
        public int TimeLastCast;

        public FightSim() : this(new PlayerSim(), new BossSim())
        {
        }

        public FightSim(PlayerSim Player) : this(Player, new BossSim())
        {
        }

        public FightSim(PlayerSim Player, BossSim Boss)
        {
            this.Player = Player;
            this.Boss = Boss;
            Params = new Dictionary<int, object>
            {
                {ParameterSim.Iterations, 1000},
                {ParameterSim.TimeofFight, 80 * 1000},
                {ParameterSim.GCD, 1500},
                {ParameterSim.RotationString, @""},


                {ParameterSim.NumberofAfflictionSpells, 0},
                {ParameterSim.DemonicSacrificePet, "Imp"}
            };

            switch (GetParam(ParameterSim.InitialPet))
            {
                case "Felguard":
                    Pet = new FelguardPet();
                    break;
                case "Felhunter":
                    Pet = new FelhunterPet();
                    break;
                case "Imp":
                    Pet = new ImpPet();
                    break;
                case "Succubus":
                    Pet = new SuccubusPet();
                    break;
                case "Voidwalker":
                    Pet = new VoidwalkerPet();
                    break;
                case null:
                    break;
                default:
                    throw new NotImplementedException();
            }
        }

        public void Simulate()
        {
            var timeEnd = (int) GetParam(ParameterSim.TimeofFight);

            Rotation = RotationSim.ParseRotation(this, GetParam(ParameterSim.RotationString) as string);
            LoggerSim.Initialize();

            SetHealth();

            ProcessRotation();


            while (!EndFight)
            {
                HeapEventSim eventSim = null;
                try
                {
                    eventSim = Heap.Dequeue();
                }
                catch (Exception e)
                {
                    log.Error("Error when dequeueing the queue " + Heap, e);
                }

                Debug.Assert(eventSim != null, nameof(eventSim) + " != null");
                ReduceHeapTime(eventSim.Time);
                Time += eventSim.Time;
                LoggerSim.ProcessEvent(Time, eventSim);

                try
                {
                    if (!eventSim.Name.Equals("GCD"))
                    {
                        if (eventSim.Spell != null && eventSim.Spell.GetType() != typeof(WaitSpell))
                        {
                            LoggerSim.Cast(Time, eventSim.Spell);
                            if (eventSim.Spell.Mana == -1)
                            {
                                log.Error("Mana is -1 of " + eventSim.Spell.Name);
                                throw new NotImplementedException();
                            }

                            if (ManaSpend(eventSim.Spell.Id, eventSim.Spell.Mana) != -1)
                            {
                                TimeLastCast = Time;
                                if (eventSim.Spell.CooldownTime > 0)
                                    AddCD(eventSim.Spell.Id, eventSim.Spell.CooldownTime);

                                eventSim.DoAction(this);
                            }
                            else
                            {
                                LoggerSim.WaitForResource(Time, eventSim.Spell.Id, eventSim.Spell.Mana);
                                eventSim.triggerRotation = false;
                                Spell spell = new WaitSpell(this);
                                EnqueueHeap(
                                    new HeapEventSim(spell.Name, spell.CastTime, spell) {triggerRotation = true});
                            }
                        }
                        else
                        {
                            //Buff
                            eventSim.DoAction(this);
                        }
                    }
                }
                catch (Exception e)
                {
                    log.Error("Error when executing the function " + eventSim.Spell + " " + eventSim.BuffSim, e);
                }

                if (eventSim.triggerRotation) ProcessRotation();

                EndFight = timeEnd < Time || EndFight;
            }

            LoggerSim.DataReport.ProcessEvents();
            LoggerSim.Close();
        }

        public void PrintResults()
        {
            LoggerSim.DataReport.PrintResults();
        }

        //#################
        //Fight Methods
        //#################
        public int Heal(int idSource, int health)
        {
            //Fel Armor
            if (HasBuff(28189)) health = (int) (health * 1.2f);

            LoggerSim.Heal(Time, idSource, health, false);
            Player.SetCurrentHealth(Player.GetCurrentHealth() + health);
            return health;
        }

        public int HealthLost(int idSource, int health)
        {
            LoggerSim.HealthLost(Time, idSource, health);
            if (Player.SetCurrentHealth(Player.GetCurrentHealth() - health))
            {
                log.Warn("Simulation Ended: Player CurrentHealth < 0");
                EndFight = true;
                return -1;
            }

            return health;
        }

        public int ManaSpend(int idSource, int mana)
        {
            LoggerSim.ManaSpend(Time, idSource, mana, Player.GetCurrentMana() - mana);
            if (Player.SetCurrentMana(Player.GetCurrentMana() - mana))
            {
                log.Warn("Current Mana is under 0!!!");
                return -1;
            }

            return mana;
        }

        public int ManaGained(int idSource, int mana)
        {
            LoggerSim.ManaGain(Time, idSource, mana, Player.GetCurrentMana() + mana);
            Player.SetCurrentMana(Player.GetCurrentMana() + mana);
            return mana;
        }

        public int Damage(EnumSim.DamageType damageType, EnumSim.SchoolType schoolType, int idSource, int damage,
            float multiplier,
            bool periodic)
        {
            return Damage(damageType, schoolType, idSource, damage, multiplier, periodic, false, 0, false, 0, 0);
        }

        public int Damage(EnumSim.DamageType damageType, EnumSim.SchoolType schoolType, int idSource, int damage,
            float multiplier,
            bool periodic, bool canMiss,
            float hitChance)
        {
            return Damage(damageType, schoolType, idSource, damage, multiplier, periodic, canMiss, hitChance, false, 0,
                0);
        }

        public int Damage(EnumSim.DamageType damageType, EnumSim.SchoolType schoolType, int idSource, int damage,
            float multiplier,
            bool periodic,
            bool canMiss,
            float hitChance, bool canCrit, float critChance, float critDamage)
        {
            return Damage(damageType, schoolType, idSource, damage, multiplier, periodic, canMiss, hitChance, canCrit,
                critChance, critDamage, out _);
        }

        public bool Buff(BuffSim buff, EnumSim.SchoolType schoolType, int idSource, bool Ticks)
        {
            if (Ticks)
            {
                if (HasBuff(buff.Name))
                {
                    var before = Buffs.Find(b => { return b.Name.Equals(buff.Name); });

                    before.DurationRemaining = buff.DurationRemaining;
                    before.DurationTotal = buff.DurationTotal;

                    RemoveBuffEvents(before);
                    EnqueueHeap(new HeapEventSim("Tick " + before.Name, before.TickInterval, buff));

                    LoggerSim.Buff(Time, buff);
                    return false;
                }
                else
                {
                    Debuffs.Add(buff);

                    EnqueueHeap(new HeapEventSim("Tick " + buff.Name, buff.TickInterval, buff));

                    LoggerSim.Buff(Time, buff);
                }
            }
            else
            {
                if (HasBuff(buff))
                {
                    var before = Buffs.Find(b => { return b.Name.Equals(buff.Name); });

                    before.DurationRemaining = buff.DurationRemaining;
                    before.DurationTotal = buff.DurationTotal;

                    RemoveBuffEvents(before);
                    EnqueueHeap(new HeapEventSim("Fade " + buff.Name, buff.DurationTotal, buff));

                    LoggerSim.Buff(Time, buff);
                    return false;
                }
                else
                {
                    Buffs.Add(buff);

                    EnqueueHeap(new HeapEventSim("Fade " + buff.Name, buff.DurationTotal, buff));

                    LoggerSim.Buff(Time, buff);
                }
            }

            return true;
        }

        public bool Debuff(BuffSim buff, EnumSim.SchoolType schoolType, int idSource, bool isBinary)
        {
            return Debuff(buff, schoolType, idSource, isBinary, false, false, 0);
        }

        public bool Debuff(BuffSim buff, EnumSim.SchoolType schoolType, int idSource, bool isBinary, bool Ticks)
        {
            return Debuff(buff, schoolType, idSource, isBinary, Ticks, false, 0);
        }

        public bool Debuff(BuffSim buff, EnumSim.SchoolType schoolType, int idSource, bool isBinary, bool Ticks,
            bool canMiss,
            float hitChance)
        {
            var missChance = Math.Max(1f, Boss.GetDefenseSpellHit() - hitChance);
            if (canMiss && RandomSim.CheckChance(missChance))
            {
                LoggerSim.MissSpell(Time, idSource, missChance);
                return false;
            }
            else
            {
                var resistChance = (float) Math.Min(350, Boss.GetResistance(schoolType)) / Player.GetLevel() / 5f *
                                   75f;
                if (isBinary && RandomSim.CheckChance(resistChance))
                {
                    LoggerSim.ResistSpell(Time, idSource, resistChance);
                    return false;
                }
                else
                {
                    if (Ticks)
                    {
                        if (HasDebuff(buff.Name))
                        {
                            var before = Debuffs.Find(b => { return b.Name.Equals(buff.Name); });

                            before.DurationRemaining = buff.DurationRemaining;
                            before.DurationTotal = buff.DurationTotal;

                            RemoveBuffEvents(before);
                            EnqueueHeap(new HeapEventSim("Tick " + before.Name, before.TickInterval, buff));

                            LoggerSim.Debuff(Time, buff);
                            return false;
                        }
                        else
                        {
                            Debuffs.Add(buff);

                            EnqueueHeap(new HeapEventSim("Tick " + buff.Name, buff.TickInterval, buff));

                            LoggerSim.Debuff(Time, buff);
                        }
                    }
                    else
                    {
                        if (HasDebuff(buff.Name))
                        {
                            var before = Debuffs.Find(b => { return b.Name.Equals(buff.Name); });

                            before.DurationRemaining = buff.DurationRemaining;
                            before.DurationTotal = buff.DurationTotal;

                            RemoveBuffEvents(before);
                            EnqueueHeap(new HeapEventSim("Fade " + buff.Name, buff.DurationTotal, buff));

                            LoggerSim.Debuff(Time, buff);
                            return false;
                        }
                        else
                        {
                            Debuffs.Add(buff);

                            EnqueueHeap(new HeapEventSim("Fade " + buff.Name, buff.DurationTotal, buff));

                            LoggerSim.Debuff(Time, buff);
                        }
                    }

                    return true;
                }
            }
        }

        public void RemoveBuff(BuffSim buff)
        {
            Buffs.RemoveAll(b => { return b.Equals(buff); });
        }

        public void RemoveBuff(string Name)
        {
            Buffs.RemoveAll(b => { return b.Name.Equals(Name); });
        }

        public void RemoveBuff(int Id)
        {
            Buffs.RemoveAll(b => { return b.Id == Id; });
        }

        public void RemoveDebuff(BuffSim buff)
        {
            Debuffs.RemoveAll(b => { return b.Equals(buff); });
        }

        public void RemoveDebuff(string Name)
        {
            Debuffs.RemoveAll(b => { return b.Name.Equals(Name); });
        }

        public void RemoveDebuff(int Id)
        {
            Debuffs.RemoveAll(b => { return b.Id == Id; });
        }

        public bool HasBuff(BuffSim buff)
        {
            foreach (var b in Buffs)
                if (buff.Equals(b))
                    return true;

            return false;
        }

        public bool HasBuff(string Name)
        {
            foreach (var b in Buffs)
                if (b.Name.Equals(Name))
                    return true;

            return false;
        }

        public bool HasBuff(int Id)
        {
            foreach (var b in Buffs)
                if (b.Id == Id)
                    return true;

            return false;
        }

        public bool HasDebuff(BuffSim buff)
        {
            foreach (var b in Debuffs)
                if (buff.Equals(b))
                    return true;

            return false;
        }

        public bool HasDebuff(string Name)
        {
            foreach (var b in Debuffs)
                if (b.Name.Equals(Name))
                    return true;

            return false;
        }

        public bool HasDebuff(int Id)
        {
            foreach (var b in Debuffs)
                if (b.Id == Id)
                    return true;

            return false;
        }

        public BuffSim GetBuff(string Name)
        {
            foreach (var b in Buffs)
                if (b.Name.Equals(Name))
                    return b;

            return null;
        }

        public BuffSim GetBuff(int Id)
        {
            foreach (var b in Buffs)
                if (b.Id == Id)
                    return b;

            return null;
        }

        public BuffSim GetDebuff(string Name)
        {
            foreach (var b in Debuffs)
                if (b.Name.Equals(Name))
                    return b;

            return null;
        }

        public BuffSim GetDebuff(int Id)
        {
            foreach (var b in Debuffs)
                if (b.Id == Id)
                    return b;

            return null;
        }

        public int Damage(EnumSim.DamageType damageType, EnumSim.SchoolType schoolType, int idSource, int damage,
            float multiplier,
            bool periodic, bool canMiss,
            float hitChance, bool canCrit, float critChance, float critDamage, out bool crit)
        {
            var initialdamage = damage;
            if (damageType == EnumSim.DamageType.Attack) throw new NotImplementedException();

            if (damageType == EnumSim.DamageType.Spell)
            {
                var missChance = Math.Max(1f, Boss.GetDefenseSpellHit() - hitChance);
                if (canMiss && RandomSim.CheckChance(missChance))
                {
                    //Miss
                    LoggerSim.MissSpell(Time, idSource, missChance);
                    crit = false;
                    LoggerSim.TriggerDamage(Time, damageType, schoolType, idSource, initialdamage, canMiss, hitChance,
                        canCrit,
                        critChance, critDamage, 0, crit);
                    return -1;
                }

                //No miss
                //https://youtu.be/7DXvTVfDN18?t=181 TODO
                var damageReduction =
                    1f - (float) Math.Min(350, Boss.GetResistance(schoolType) - Player.GetSpellPenetration()) /
                    Player.GetLevel() / 5f * 0.75f;

                damage = CalculateDamage(damageType, schoolType, idSource, damage, multiplier, periodic);
                damage = (int) Math.Floor(damage * damageReduction);

                if (canCrit && RandomSim.CheckChance(critChance))
                {
                    //Crit
                    damage = (int) Math.Floor(damage * (1 + critDamage));
                    LoggerSim.Damage(Time, damageType, schoolType, idSource, damage, true);
                    crit = true;
                    LoggerSim.TriggerDamage(Time, damageType, schoolType, idSource, initialdamage, canMiss, hitChance,
                        canCrit,
                        critChance, critDamage, damage, crit);
                    if (Boss.SetCurrentHealth(Boss.GetCurrentHealth() - damage))
                    {
                        log.Warn("Simulation Ended: Boss CurrentHealth < 0");
                        EndFight = true;
                    }

                    return damage;
                }

                //No crit
                LoggerSim.Damage(Time, damageType, schoolType, idSource, damage, false);
                crit = false;
                LoggerSim.TriggerDamage(Time, damageType, schoolType, idSource, initialdamage, canMiss, hitChance,
                    canCrit,
                    critChance, critDamage, damage, crit);
                if (Boss.SetCurrentHealth(Boss.GetCurrentHealth() - damage))
                {
                    log.Warn("Simulation Ended: Boss CurrentHealth < 0");
                    EndFight = true;
                }

                return damage;
            }

            throw new NotImplementedException();
        }

        private int CalculateDamage(EnumSim.DamageType damageType, EnumSim.SchoolType schoolType, int idSource,
            int damage,
            float multiplier, bool periodic)
        {
            switch (damageType)
            {
                case EnumSim.DamageType.Attack:
                    break;
                case EnumSim.DamageType.Spell:
                    break;
                case EnumSim.DamageType.Other:
                    break;
                default:
                    throw new NotSupportedException();
            }

            switch (schoolType)
            {
                case EnumSim.SchoolType.Arcane:
                    //Curse of the Elements
                    if (HasDebuff(SpellId.Warlock_CurseoftheElements))
                        damage = (int) (damage * 1.1f);
                    break;
                case EnumSim.SchoolType.Fire:
                    //Curse of the Elements
                    if (HasDebuff(SpellId.Warlock_CurseoftheElements))
                        damage = (int) (damage * 1.1f);

                    //Demonic Sacrifice Imp
                    if (HasBuff(Talents.Warlock_DemonicSacrifice_Imp))
                        damage = (int) (damage * 1.15);

                    //Emberstorm
                    multiplier += 0.02f * Player.GetTalentRank(Talents.Warlock_Emberstorm);
                    break;
                case EnumSim.SchoolType.Frost:
                    //Curse of the Elements
                    if (HasDebuff(SpellId.Warlock_CurseoftheElements))
                        damage = (int) (damage * 1.1f);
                    break;
                case EnumSim.SchoolType.Holy:
                    break;
                case EnumSim.SchoolType.Nature:
                    break;
                case EnumSim.SchoolType.Physical:
                    break;
                case EnumSim.SchoolType.Shadow:
                    //Curse of the Elements
                    if (HasDebuff(SpellId.Warlock_CurseoftheElements))
                        damage = (int) (damage * (1.1 + 0.01 * Player.GetTalentRank(Talents.Warlock_Malediction)));

                    //Demonic Sacrifice Felguard
                    if (HasBuff(Talents.Warlock_DemonicSacrifice_Felguard))
                        damage = (int) (damage * 1.1);

                    //Demonic Sacrifice Imp
                    if (HasBuff(Talents.Warlock_DemonicSacrifice_Succubus))
                        damage = (int) (damage * 1.15);

                    //Improved Shadow Bolt
                    if (HasBuff(Talents.Warlock_ImprovedShadowBolt) && !periodic)
                    {
                        damage = (int) (damage * (1 + 0.05 * Player.GetTalentRank(Talents.Warlock_ImprovedShadowBolt)));
                        var buff = Buffs.Find(b => b.Id == Talents.Warlock_ImprovedShadowBolt);
                        Debug.Assert(buff != null, nameof(buff) + " != null");
                        buff.Stacks--;
                        if (buff.Stacks == 0)
                        {
                            RemoveBuff(Talents.Warlock_ImprovedShadowBolt);
                            RemoveBuffEvents(Talents.Warlock_ImprovedShadowBolt);
                        }
                    }

                    //Shadow Mastery
                    multiplier += 0.02f * Player.GetTalentRank(Talents.Warlock_ShadowMastery);
                    break;
                default:
                    throw new NotSupportedException();
            }

            switch (Boss.GetRace())
            {
                case EnumSim.NpcType.Aberration:
                    break;
                case EnumSim.NpcType.Beast:
                    //Troll Racial
                    if (Player.GetRace().Equals(EnumSim.RaceType.Troll))
                        damage = (int) (damage * 1.05);
                    break;
                case EnumSim.NpcType.Critter:
                    break;
                case EnumSim.NpcType.Demon:
                    break;
                case EnumSim.NpcType.Dragon:
                    break;
                case EnumSim.NpcType.Elemental:
                    break;
                case EnumSim.NpcType.Giant:
                    break;
                case EnumSim.NpcType.Humanoid:
                    break;
                case EnumSim.NpcType.Mechanical:
                    break;
                case EnumSim.NpcType.Undead:
                    break;
                case EnumSim.NpcType.Uncategorized:
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }


            return (int) (damage * multiplier);
        }

        public void RemoveBuffEvents(string Name)
        {
            foreach (var evt in Heap)
                if (evt.BuffSim != null && evt.BuffSim.Name.Equals(Name))
                    Heap.Remove(evt);
        }

        public void RemoveBuffEvents(int Id)
        {
            foreach (var evt in Heap)
                if (evt.BuffSim != null && evt.BuffSim.Id == Id)
                    Heap.Remove(evt);
        }

        public void RemoveBuffEvents(BuffSim buff)
        {
            foreach (var evt in Heap)
                if (evt.BuffSim != null && evt.BuffSim.Equals(buff))
                    Heap.Remove(evt);
        }

        public bool HasCD(int Id)
        {
            return CDs.ContainsKey(Id);
        }

        public int GetCD(int Id)
        {
            return CDs[Id];
        }


        public object GetParam(int param)
        {
            object ret;
            Params.TryGetValue(param, out ret);
            return ret;
        }

        public void EnqueueHeap(HeapEventSim eventSim)
        {
            if (eventSim.triggerRotation)
                if (Heap.Any(sim => sim.triggerRotation))
                {
                    log.Error("2 Events with triggerRotation - " + Heap.First(sim => sim.triggerRotation).Name + " | " +
                              eventSim.Name);
                    throw new NotSupportedException();
                }

            Heap.Enqueue(eventSim, eventSim.Time);
        }

        //#################
        // PRIVATE METHODS
        //#################

        private void ReduceHeapTime(int time)
        {
            foreach (var e in Heap)
            {
                e.ReduceTime(time);
                Heap.UpdatePriority(e, e.Time);
            }

            foreach (var cd in new List<int>(CDs.Keys))
            {
                CDs[cd] = CDs[cd] - time;
                if (CDs[cd] <= 0) CDs.Remove(cd);
            }
        }

        private void AddCD(int id, int time)
        {
            if (!CDs.ContainsKey(id))
            {
                CDs.Add(id, time);
            }
            else
            {
                log.Error("Error when adding CD of " + id + " with time " + time);
                throw new NotSupportedException();
            }
        }

        private void SetHealth()
        {
            Player.SetCurrentHealth(Player.GetMaxHealth());
            Player.SetCurrentMana(Player.GetMaxMana());

            Boss.CurrentHealth = Boss.GetMaxHealth();

            var buff = new BuffSim("Regen", 0);
            buff.TickInterval = 2000;
            buff.TickActionDelegate = delegate(FightSim sim)
            {
                var health = (int) (sim.Player.GetHealthMP5() / 5 * 2);
                int mana;
                if (TimeLastCast + 5000 < Time)
                    mana = (int) (sim.Player.GetManaMP5() / 5 * 2);
                else
                    mana = (int) (sim.Player.GetManaMP5Casting() / 5 * 2);

                if (health > 0) sim.Heal(0, health);

                if (mana > 0) sim.ManaGained(0, mana);

                sim.EnqueueHeap(new HeapEventSim("Regen", 2000, buff));
            };
            EnqueueHeap(new HeapEventSim("Regen", 2000, buff));
        }

        private Spell GetSpellRotation()
        {
            Spell spell = null;

            log.Info("Getting Spell Rotation: ");
            foreach (var rotationSim in Rotation)
            {
                log.Info("Checking rotation: " + rotationSim.TypeSpell.Name);
                if (rotationSim.Condition(this))
                {
                    spell = Activator.CreateInstance(rotationSim.TypeSpell, this) as Spell;
                    if (spell.Mana <= Player.GetCurrentMana()) break;

                    log.Info("No mana: " + spell.Mana + "<=" + Player.GetCurrentMana());
                    spell = null;
                }
            }

            log.Info("Spell is " + spell?.Name);
            return spell;
        }

        private void ProcessRotation()
        {
            var spell = GetSpellRotation();

            if (spell == null)
            {
                spell = new WaitSpell(this);
                EnqueueHeap(new HeapEventSim(spell.Name, spell.CastTime, spell) {triggerRotation = true});
                return;
            }

            var gcd = Math.Max(1000,
                (int) Math.Floor((int) GetParam(ParameterSim.GCD) /
                                 (1 + Player.GetSpellHaste() + Player.GetHaste() / 100f)));
            var spellCastTime = spell.GetCastTime(this);

            if (spell.IsGCD != true)
            {
                EnqueueHeap(new HeapEventSim("Cast " + spell.Name, spellCastTime, spell) {triggerRotation = true});
            }
            else
            {
                if (spell.CastTime < gcd)
                {
                    EnqueueHeap(new HeapEventSim("Cast " + spell.Name, spellCastTime, spell));
                    EnqueueHeap(new HeapEventSim("GCD", gcd) {triggerRotation = true});
                }
                else
                {
                    if (spell.Channeled)
                    {
                        if (spell.TickTime == 0)
                        {
                            log.Error("TickTime = 0 of " + spell.Name);
                            throw new NotImplementedException();
                        }

                        var spellTickTime = spell.GetTickTime(this);
                        spellCastTime = spellCastTime / spellTickTime * spellTickTime;

                        var buff = new BuffSim(spell.Name, spell.Id);
                        buff.DurationTotal = spellCastTime;
                        buff.DurationRemaining = buff.DurationTotal;
                        buff.TickInterval = spell.GetTickTime(this);
                        buff.TickActionDelegate = sim =>
                        {
                            sim.LoggerSim.TickChannel(sim.Time, spell);

                            spell.ActionDelegate();
                            buff.DurationRemaining -= buff.TickInterval;

                            if (buff.DurationRemaining > 0)
                            {
                                sim.EnqueueHeap(new HeapEventSim("Tick " + spell.Name, buff.TickInterval, buff));
                            }
                            else
                            {
                                sim.LoggerSim.EndChannel(sim.Time, spell);
                                sim.Buffs.Remove(sim.Buffs.Find(b => b.Id == spell.Id));
                                EnqueueHeap(new HeapEventSim("GCD", 0) {triggerRotation = true});
                            }
                        };
                        LoggerSim.Cast(Time, spell);

                        var missChance = Math.Max(1f, Boss.GetDefenseSpellHit() - Player.GetSpellHitChance());
                        if (RandomSim.CheckChance(missChance))
                        {
                            LoggerSim.MissSpell(Time, spell.Id, missChance);
                            EnqueueHeap(new HeapEventSim("GCD", gcd) {triggerRotation = true});
                        }
                        else
                        {
                            EnqueueHeap(new HeapEventSim("Tick " + spell.Name, buff.TickInterval, buff));
                        }
                    }
                    else
                    {
                        EnqueueHeap(new HeapEventSim("Cast " + spell.Name, spellCastTime, spell)
                            {triggerRotation = true});
                    }
                }
            }
        }
    }
}