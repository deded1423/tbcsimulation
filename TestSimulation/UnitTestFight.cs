using System;
using System.Collections.Generic;
using System.IO;
using NUnit.Framework;
using TBCsimulation.Data.Utils;
using TBCsimulation.Simulation;

namespace TestSimulation
{
    public class UnitTestFight
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void TestFight()
        {
            var test = new FightSim();
            test.Params[ParameterSim.RotationString] = File.ReadAllText(@"F:\\Proyectos\\TBCsimulation\\Rotation.json");

            for (int i = 0; i < 10; i++)
            {
                var t = ObjectExtensions.Copy(test);
                t.Heap = new();
                t.Simulate();
                // t.PrintResults();
            }
        }

        [Test]
        public void TestPlayer()
        {
            var test = new FightSim();
            test.Params[ParameterSim.RotationString] = File.ReadAllText(@"F:\\Proyectos\\TBCsimulation\\Rotation.json");

            EnumSim.WowClass wowClass = EnumSim.WowClass.Warlock;
            EnumSim.RaceType race = EnumSim.RaceType.Undead;

            Dictionary<int, int> Talents = new();
            List<(string, string[])> items = new()
            {
                ("Spellstrike Hood", null),
                ("Brooch of Heightened Potential", null),
                ("Mana-Etched Spaulders", null),
                ("Sethekk Oracle Cloak", null),
                ("Frozen Shadoweave Robe", null),
                ("Crimson Bracers of Gloom", null),
                ("Mana-Etched Gloves", null),
                ("Girdle of Ruination", null),
                ("Spellstrike Pants", null),
                ("Frozen Shadoweave Boots", null),
                ("Ashyen's Gift", null),
                ("Sparking Arcanite Ring", null),
                ("Scryer's Bloodgem", null),
                ("Icon of the Silver Crescent", null),
            };

            List<(string, string[])> weapons = new()
            {
                ("Stormcaller", null),
                ("Lamp of Peaceful Radiance", null),
                ("Nether Core's Control Rod", null),
            };
            test.Player.SetClass(wowClass);
            test.Player.SetRace(race);
            test.Player.SetGear(items, weapons);
            test.Player.SetAllStats();
            test.Player.PrintStats();
        }
    }
}